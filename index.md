---
layout: home
---

This content now lives at [MakerForums](https://forum.makerforums.info/c/3d-printing/thing-printer)
and is continuing on there. Please go log in there with the same google
account you used for Google+ and you will still own all the content you
wrote.

This static site is out of date and not maintained now that
the content is available at makerforums.
