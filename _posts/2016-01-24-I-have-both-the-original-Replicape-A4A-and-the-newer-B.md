---
layout: post
title: "I have both the original Replicape A4A and the newer B"
date: January 24, 2016 18:18
category: "Discussion"
author: Vernon Barry (Jetguy)
---
I have both the original Replicape A4A and the newer B. With help from James Armstrong, I've got the latest B cape running and printing!!! My problem is, I want to get the old cape going in another printer using the latest Kamikaze image. I know for a fact the A4A was updated with the A4A EEPROM data and can read it out. I'm getting bone_capemgr bone_capemgr: slot #1: No cape found



However, I also cannot run this command

opkg install replicape-devicetree

 

I get

sudo: opkg: command not found



Anyway, just trying to understand the best way forward to get both board running similar images.





**Vernon Barry (Jetguy)**

---
---
**Elias Bakken** *January 24, 2016 18:24*

Kamikaze is Debian based, so the package manager is Apt. 

A4A had a DIP switch, the cape does not show up on any of the slots?

If you want to update the EEPROM on A4A, use the same procedure as for 00B3, but exchange the version to 0A4A: 

 wget [https://bitbucket.org/intelligentagent/replicape/raw/bf08295bbb5e98ce6bff60097fe9b78d96002654/eeprom/Replicape_0A4A.eeprom](https://bitbucket.org/intelligentagent/replicape/raw/bf08295bbb5e98ce6bff60097fe9b78d96002654/eeprom/Replicape_0A4A.eeprom)


---
**Elias Bakken** *January 24, 2016 18:25*

Also, great that you have the Rev B running! Post a video when you get the time :)




---
**Vernon Barry (Jetguy)** *January 24, 2016 18:39*

Thanks for the fast reply. I still get permission denied when trying to update the EEPROM. That said, I can read the EEPROM and read the file you just pointed me at, both are the same.

Question: I cannot find a diagram explaining the 2 switches in the A4A documentation page? maybe I missed it?



Here you can see me reading the EEPROM



debian@kamikaze:~$ sudo cat /sys/bus/i2c/drivers/at24/2-0054/at24-1/nvmem                                    ▒U3▒A0Replicape 3D printer cape0A4AIntelligent AgenBB-BONE-REPLICAP00000000001(


---
**Elias Bakken** *January 24, 2016 19:15*

Switches are standard for all capes,  selects EEPROM address. You shool be root when updating.


---
**Elias Bakken** *January 24, 2016 19:15*

But you should not need to update:)


---
**James Armstrong** *January 24, 2016 19:23*

Vernon, the workaround we had to do (which didn't make sense because we are using sudo) is to do a 

sudo chown debian:debian /sys/bus/i2c/drivers/at24/2-0054/at24-1/nvmem

then write the eeprom and then restart. I don't know why I could do this on my board without chown and just sudo but you could not on any of your boards.




---
**James Armstrong** *January 24, 2016 19:24*

I guess you could do su - and switch to root (I don't know what the password is).




---
**James Armstrong** *January 24, 2016 19:31*

**+Elias Bakken** a few things we noticed and I was planning on tracing down once I get my printer wired (working on it now) was that if you home and try to print the Z would raise back up. The fix was to add home_x = 0, home_y = 0, home_z = 0 (we are at mins for everything) in the config then it would work. I would think the home_? was just to set an offset after homing if needed and it would default to 0 if homing to min and default to max?? if homing to max. Should be a simple fix. 



Also I think I ran into this at home and at Vernon's, if you preheat manually then start a print it didn't seem to want to start, like it was not seeing that it was already at temperature. I didn't have time to try and duplicate it and will look into it once I get up and running.



Toggle didn't seem to want to do anything but would show the printer status (operational or not), I ran toggle from the prompt and noticed a lot of http 401 log messages anytime we tried to press a button.



Lastly, I don't know if there is currently an idle timeout that would disable heaters and steppers after xxx minutes of no activity but we enabled them and forgot and noticed they were still heating and steppers enabled some time later. If this is not a feature it would be a good one. I forked the repository with plans to start diving in and getting my feet wet soon. 



One thing I need to know so I can run the dev branch is what is the command line compile options you use to compile Redeem? The only reason I am asking is I tried it once and ended up with a mess. The sd image seemed to have stuff in /usr/lib/python but when I compiled it ended up putting in /usr/local and had two copies with conflicts. 


---
**James Armstrong** *January 24, 2016 19:42*

**+Elias Bakken** I think he needed to update the eeprom because we swapped it out when he was trying to get the B3 written since it didn't come programmed. Probably ended up writing over it and using it on the B3.




---
**Vernon Barry (Jetguy)** *January 24, 2016 19:51*

No, I got it working by doing nothing or something? I know, crazy!! The problem might be just understanding. When I booted, I was watching the console using a 3.3V FTDI cable so see all boot messages.

You see this

Starting kernel ...



[    3.158253] omap_voltage_late_init: Voltage driver support not added

[    3.167393] cpu cpu0: cpu0 clock notifier not ready, retry

[    3.322492] bone_capemgr bone_capemgr: slot #1: No cape found

[    3.382450] bone_capemgr bone_capemgr: slot #2: No cape found

[    3.442450] bone_capemgr bone_capemgr: slot #3: No cape found

Loading, please wait...



However, doing some poking around

debian@kamikaze:/$ dmesg|grep bone_cape

[    3.230831] bone_capemgr bone_capemgr: Baseboard: 'A335BNLT,0A5A,1613BBBK0797'

[    3.230855] bone_capemgr bone_capemgr: compatible-baseboard=ti,beaglebone-black - #slots=4

[    3.266999] bone_capemgr bone_capemgr: slot #0: 'Replicape 3D printer cape,0A4A,Intelligent Agen,BB-BONE-REPLICAP'

[    3.322492] bone_capemgr bone_capemgr: slot #1: No cape found

[    3.382450] bone_capemgr bone_capemgr: slot #2: No cape found

[    3.442450] bone_capemgr bone_capemgr: slot #3: No cape found

[    3.448386] bone_capemgr bone_capemgr: initialized OK.

[    3.469359] bone_capemgr bone_capemgr: slot #0: dtbo 'BB-BONE-REPLICAP-0A4A.dtbo' loaded; overlay id #0


---
*Imported from [Google+](https://plus.google.com/102668345821729808509/posts/WFhLeNxnHEn) &mdash; content and formatting may not be reliable*
