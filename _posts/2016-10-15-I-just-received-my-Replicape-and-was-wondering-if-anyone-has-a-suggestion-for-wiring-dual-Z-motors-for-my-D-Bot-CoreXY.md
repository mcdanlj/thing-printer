---
layout: post
title: "I just received my Replicape and was wondering if anyone has a suggestion for wiring dual Z motors for my D-Bot CoreXY"
date: October 15, 2016 02:54
category: "Discussion"
author: Luke Ingerman
---
I just received my Replicape and was wondering if anyone has a suggestion for wiring dual Z motors for my D-Bot CoreXY.





**Luke Ingerman**

---
---
**Jon Charnas** *October 15, 2016 08:16*

One on z the other on h in slave mode?


---
**Elias Bakken** *October 15, 2016 13:14*

There is a section about that in the wiki: [wiki.thing-printer.com - Redeem - Thing-Printer](http://wiki.thing-printer.com/index.php?title=Redeem#Slave_mode)


---
**Elias Bakken** *October 15, 2016 13:15*

Oh sorry, just realized you wanted two steppers on one driver? That is possible too, both in series and in parallel. 


---
**Luke Ingerman** *October 15, 2016 14:42*

Ok, so I have the option to drive the motors from two separate drivers (slave mode) or wiring the two motors to the same driver (serial or parallel). What are the Pros/Cons for each? 


---
**Jon Charnas** *October 15, 2016 14:44*

Same driver means you have less configuration to do. Slave means you can get more torque (full amperage for both motors)


---
**Luke Ingerman** *October 15, 2016 14:51*

I have tried two Arduino/RAMPS setups for my D-Bot in which the Z axis was driven by the same DRV8825 driver. The Z axis ran smoothly in this configuration but I never tried it with the additional weight of a print. I think the idea of having full torque available (slave mode) is a good idea and keep me from re wiring things for the time being.


---
**Jon Charnas** *October 15, 2016 14:52*

Yep. Sorry my answers are short - left hand is sown and bandaged up after a mishap with a knife and a squash.


---
**Luke Ingerman** *October 15, 2016 14:57*

Completely understandable. I've jabbed myself with a knife a few times cleaning up a print or when building my D-Bot.


---
**Ryan Carlyle** *October 15, 2016 15:00*

The "right" way to do it is separate drivers. After that, I would recommend serial wiring if you have a 24v PSU and the motors are identical. This is equivalent to driving each with 12v and they both get full current. If you have a 12v PSU, series wiring can seriously cut your top Z speed, and parallel is probably better. Downside to parallel wiring is that each motor gets half the current and torque. But it doesn't limit your speed as much. 


---
**Luke Ingerman** *October 15, 2016 15:03*

Thanks Ryan. I'm going to wire the Z axis two separate drivers to start with. If I have a need for dual extruders/hot ends down the road then I will need to try something else.


---
*Imported from [Google+](https://plus.google.com/114654865873386162214/posts/Ng1k3fBvG1P) &mdash; content and formatting may not be reliable*
