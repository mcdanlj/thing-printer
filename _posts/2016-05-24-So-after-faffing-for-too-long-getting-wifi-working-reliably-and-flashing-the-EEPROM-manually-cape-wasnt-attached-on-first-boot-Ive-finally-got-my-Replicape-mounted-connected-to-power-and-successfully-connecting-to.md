---
layout: post
title: "So after faffing for too long getting wifi working reliably, and flashing the EEPROM manually (cape wasn't attached on first boot), I've finally got my Replicape mounted, connected to power, and successfully connecting to"
date: May 24, 2016 21:34
category: "Discussion"
author: Marcos Scriven
---
So after faffing for too long getting wifi working reliably, and flashing the EEPROM manually (cape wasn't attached on first boot), I've finally got my Replicape mounted, connected to power, and successfully connecting to Octoprint. I was just manually testing the endstops - one thing that's not clear from the wiring diagram is why there's three poles on the end stop plugs? Is this so one has the option of normally open or normally closed? On my current setup my printer (Sanguinoulu electronics) uses normally open (middle pin), which seems like a bad idea!





**Marcos Scriven**

---
---
**Elias Bakken** *May 24, 2016 21:38*

NO/NC is configured in software. 


---
**Marcos Scriven** *May 24, 2016 21:43*

Ah, I was more referring to the physical wiring, but now I see that actually the first revision had only two connections per end stop, whereas the rev b has three. 


---
**Dejay Rezme** *May 26, 2016 00:58*

Maybe it is 5V or 12V for optical or magnetic switches?


---
**Marcos Scriven** *May 26, 2016 07:22*

So I'm having a strange issue with y-stop. 



The way I wired up my stops is connecting the COMMON and NC terminals of the switches to the first two terminals of stop plugs on the Replicape (E.g the pin with the square solder pad, and the one immediately next to it. I'm not sure how they are numbered, and I still don't know why there's three connections on the cape - whether you're using NC or NO, you're just closing a single circuit, you don't need to monitor the other pole, since its state is implied from the other - EDIT Unless you <b>do</b> monitor both, and if the software detects both the NC and NO poles are the same state, then the switch has failed?).



This seems to work fine for x and z, but with y I see nothing in the Octoprint console for a few seconds, then suddenly a whole bunch of ' 'Y stop hit!'  messages. Any ideas? I've physically checked the switch with a multimeter, and it's definitely 'closed' when not pressed, and 'open' when I press it.


---
**Marcos Scriven** *May 26, 2016 09:54*

In fact, I just plugged in my y switch to the y2 instead, and that works reliably! Does this mean a hardware error?


---
**Marcos Scriven** *May 26, 2016 10:17*

Hmm - but in doing that the x1 stopped working. Very strange. 



<b>EDIT</b> - Actually, scrap that - the x1 didn't stop working, I just dislodged it while taking out y1 to put into y2. So yes, I have x1, y2, z1 working - but something wrong with y1


---
**Elias Bakken** *May 26, 2016 15:54*

There is only one input pin, along with gnd and 5V for powering Hall effect end stops etc. 

is Y1 not working at all or are the events buffered in some way like you described. This might be a BBB rev issue that needs to be resolved by pin name/number override in the config...


---
**Marcos Scriven** *May 26, 2016 15:58*

My BBB rev is A5C - Output for Y1 seemeded sporadic. Sometimes nothing. Sometimes a whole bunch buffered. But it was difficult to tell for sure that every click was successfully buffered.


---
**Elias Bakken** *May 26, 2016 17:46*

There is a tool for checking the values that are passed between the two PRUs.

Could you run this on your BBB and check if the values change as you press the Y1 end stop? [https://bitbucket.org/intelligentagent/redeem/src/de9d0728f1c3c0a331c3b8134ea6e1921a3025cd/tools/endstop_test.py?at=master&fileviewer=file-view-default](https://bitbucket.org/intelligentagent/redeem/src/de9d0728f1c3c0a331c3b8134ea6e1921a3025cd/tools/endstop_test.py?at=master&fileviewer=file-view-default)


---
**Marcos Scriven** *May 26, 2016 17:51*

Thanks. I see no change as I press the y-stop, whilst plugged into Y1, running that:



[http://i.imgur.com/4CeqCrv.png](http://i.imgur.com/4CeqCrv.png)


---
**Elias Bakken** *July 16, 2016 11:00*

Make sure you are running the latest from develop for this, alternatively install the new Kamikaze 1.1. 



If you are getting an event in the console  of octoprint but not seeing the change in the testing script, it might be that the end stop is not configured right. Could send the result of an M574?


---
*Imported from [Google+](https://plus.google.com/117910047317867058880/posts/iqfpUPFVkxS) &mdash; content and formatting may not be reliable*
