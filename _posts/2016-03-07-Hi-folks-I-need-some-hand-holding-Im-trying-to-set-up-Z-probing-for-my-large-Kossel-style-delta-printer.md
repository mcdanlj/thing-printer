---
layout: post
title: "Hi, folks. I need some hand-holding: I'm trying to set up Z-probing for my large Kossel-style delta printer"
date: March 07, 2016 19:57
category: "Discussion"
author: Matti Airas
---
Hi, folks. I need some hand-holding: I'm trying to set up Z-probing for my large Kossel-style delta printer. I have a differential IR probe a la David Crocker and I've connected it to X2. It works great, I can do "M574 X2 x_cw,y_cw,z_cw" and it works just fine. What I don't understand, however, is how to get that working with G30. If I enable the endstop and do G30, the hotend just stops at the endstop instead of doing the reverse-reprobe dance. Heeeeelp.





**Matti Airas**

---
---
**Matti Airas** *March 07, 2016 21:35*

Nevermind. I was doing everything correctly but didn't notice that the G30 log messages were of level "debug". Also the reverse-reprobe dance is only performed with G30 Px S". I wonder is that optimal?



Once I did "M111 S10", I got the messages just fine.


---
**Jonathan Cohen** *May 16, 2018 23:33*

When I run G29C, the probe crashes Redeem.  Are there specific settings that you are using for the IR probe ?


---
*Imported from [Google+](https://plus.google.com/106643081727071984016/posts/BGAxVP2yThf) &mdash; content and formatting may not be reliable*
