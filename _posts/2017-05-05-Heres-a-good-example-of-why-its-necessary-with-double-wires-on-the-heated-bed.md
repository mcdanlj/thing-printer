---
layout: post
title: "Here's a good example of why it's necessary with double wires on the heated bed"
date: May 05, 2017 10:20
category: "Discussion"
author: Elias Bakken
---
Here's a good example of why it's necessary with double wires on the heated bed. They heat up! So use the double barrel connector in the bed and PSU if you have it!



<b>Originally shared by Matthew Del Rosso</b>



As I am a high school student, it is very rare that I get to mess around with industrial tech (I love tech lol ). But today I struck lucky and my dad brought home a FLIR thermal camera/thermometer  for me to mess around with.... well not really, he needs me to teach him how to use it. As soon as I saw it I immediately thought of making it watch my 3D printer. (For those of you that are interested) the pics below show an image takes from the thermal camera taking the instantaneous temps of 3 spots, the heatblock, power supply, and heated bed. What I found interesting was that the temp was rather consistently 60 degrees off from the temps shown by cura, I wonder what would sub for that difference? Anyways just though it was interesting to share with all of you.



(if this is spam let me know and I will remove it : )   ) 



![images/c79db7d3d310f8e21c507923b660b1c7.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/c79db7d3d310f8e21c507923b660b1c7.jpeg)
![images/b8ad66c0fa8fe20440870d888c2d5c31.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/b8ad66c0fa8fe20440870d888c2d5c31.jpeg)

**Elias Bakken**

---
---
**Step Cia** *May 05, 2017 15:37*

Or just do PLA and forget heat bed altogether :)


---
**Elias Bakken** *May 05, 2017 17:34*

Oh, i still do a few degrees on my buildtak, like 40


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/aiudL7ZPHxu) &mdash; content and formatting may not be reliable*
