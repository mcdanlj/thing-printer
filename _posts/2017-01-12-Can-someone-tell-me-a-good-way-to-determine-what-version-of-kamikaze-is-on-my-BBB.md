---
layout: post
title: "Can someone tell me a good way to determine what version of kamikaze is on my BBB?"
date: January 12, 2017 19:52
category: "Discussion"
author: Andrew A
---
Can someone tell me a good way to determine what version of kamikaze is on my BBB?  The flashing process is very inconsistant so i can't tell if it flashed correctly.  Im not getting the 4 lights at the end, but 'sometimes' it will turn itself off after flashing.  I can log into octoprint.  But i need to know if the flash really worked.



Secondly, im trying to go through the wifi steps in the wiki and that isn't working at all.  Even tho it wasn't mentioned i did eventually figuer out i needed to type "connmanctl" first.  however the first command after that does nothing.  scan wifi does nothing, services does nothing, agent on DOES work.  "connect wifi_dc85de828967_38303944616e69656c73_managed_psk"   doesn't work .



here's a screenshot towards the end showing some of the commands

![images/ff6f534bbd1429fdd79b9a021d02a613.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/ff6f534bbd1429fdd79b9a021d02a613.jpeg)



**Andrew A**

---
---
**Step Cia** *January 12, 2017 22:06*

Are you trying 2.1?


---
**Andrew A** *January 12, 2017 23:36*

no its the 2.0.8 or whatever 'stable' version.  i reflashed yet again, but recorded it with my camera and i got 4 solid lights and it turned itself off.  I also ftp'd into it and see files listed for the 10th month of 2016 so it appears the flash worked.



the wifi issue i can't figuer out still, i managed to hack in internet over USB to do the 'updating apt-get" part of the wiki, thinking maybe thats why the wifi wasn't working.  but still can't get it going.  i used nano to edit/follow this guide [elementztechblog.wordpress.com - Sharing internet using Network-Over-USB in BeagleBone Black](https://elementztechblog.wordpress.com/2014/12/22/sharing-internet-using-network-over-usb-in-beaglebone-black/)



and after editing the file the guide doesn't say anything else....i rebooted, tried to ping google, didn't work, so i dunno i have a wiki and a guide that doesn't work at all :(


---
**Step Cia** *January 12, 2017 23:41*

I read about the issue with wifi and opted to use ethernet for simplicity...


---
**Andrew A** *January 13, 2017 01:12*

well one of the main reasons i picked this board over all the others was because of wifi/octoprint.  I wanted to easily be able to have it setup in a different room without messy cords.  I seen the wiki pages and thought ok they have it all figuered out, just follow the steps and it will be a snap......i did my research and tried to find a wifi stick that was easily compatible, and seen links like this  [davenelson.com - BeagleBone Black WiFi Adapter (WNA1100) - Dave Nelson](http://davenelson.com/beaglebone-black-wifi-adapter-wna1100/)



now 2 days straight going through the 'arch wiki' and doing stuff like this [https://wiki.debian.org/ath9k](https://wiki.debian.org/ath9k) , and then realizing i have no idea how to install the firmware to the doogle after reading stuff like this [https://github.com/qca/open-ath9k-htc-firmware](https://github.com/qca/open-ath9k-htc-firmware) and i don't even know if im barking up the right or wrong tree.....



seriously thinking i should just sell this on ebay at this point, but i bent 4 pin's on the replicape doing all the reflashes (i know u can flash maybe with the cape on ,but the instructions say to hold the button which means having it off)



anyways this is all very very annoying and i havn't even gotten to the 'redeem' page yet on the wiki.........its going to take 4 years to get this thing running


---
**Jon Charnas** *January 13, 2017 07:05*

Also issues with USB dongles is why 2.1 RC was pushed forward. We're working on a way to get a version of kamikaze to be sent from a gcode command but it's not ready yet


---
**Andrew A** *January 13, 2017 20:18*

well here's the terminal where i tried, nothing happened and this is after a fresh flash and apt-get update and upgrade.  did i do something wrong trying to load them? 



last night after alot of tinkering i did manage to get the 'scan wifi' to work, and when i tried to connect it asked for a passphase but it wouldn't connect with the password.  I wasn't sure what i did to get it working that far so i decided to do the reflash and start from scratch because ill have to do this every time i update kamikaze.



basically what im thinking is the ath9k driver needed to be installed, and that let it work a little bit, but i have to get the 'firmware' part installed to really get it working and thats the part i don't know how to do.  Also isn't there supposed to be a debian GUI?  ive tried repeatedly to VNC into this and i keep getting a terminal instead of a GUI.  i was going to try installing the wifi  via the GUI :(



maybe i should try the 2.1 version?  or can someone walk me through getting the firmware file and putting it in the right folder? i can ftp into it but it wouldn't let me modify files, maybe i needed ftp in as root.  

![images/34f28ce19d2952b162a74a8bbbf9b1fb.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/34f28ce19d2952b162a74a8bbbf9b1fb.jpeg)


---
**Andrew A** *January 13, 2017 20:51*

ok i just tried 2.1RC,   2 mins setting it up and it worked instantly, that is more like it.  just hope i don't run into problems being RC.  The reason i didn't start out with this version is i read a few scary bugs in the comment section


---
**Step Cia** *January 13, 2017 20:53*

ok that's promising. I have been avoiding 2.1 as well


---
**Jon Charnas** *January 13, 2017 22:07*

So the bugs are purely fixable and nothing that should stop you from at least printing. And once you have redeem configured just save your [local.cfg](http://local.cfg) and when the final 2.1.0 comes out you can flash it without having to worry about how long it'll take to reconfigure everything.


---
**Andrew A** *January 13, 2017 22:22*

ok thanks for the help everyone


---
**Step Cia** *January 13, 2017 23:32*

Jon what if we never made any changes to local.cfg but instead we modified the printer config?


---
**Jon Charnas** *January 14, 2017 04:50*

Then take the settings you modified and put them in a [local.cfg](http://local.cfg) file. You really shouldn't modify the printer config directly, because it would get overwritten by a redeem update.


---
**Step Cia** *January 14, 2017 05:45*

I remember from earlier instruction, printer and local config can be modified but not default config



Can we add this to wiki?


---
**Jon Charnas** *January 14, 2017 05:55*

I thought it already was, but I'll make it more explicit, yes


---
**Step Cia** *January 14, 2017 05:57*

Yep if what you said is true then, I've been doing it wrong this whole time then....... More explicit please


---
**Daniel Kusz** *January 14, 2017 12:51*

But what is wrong to make another [printer.cfg](http://printer.cfg) (custom printer) file and upload it after reflashing?


---
**Jon Charnas** *January 14, 2017 12:54*

That's why I updated the wiki. If it's a printer.cfg that you created, it's fine. But if you're updating the settings for a pre-existing printer.cfg it will get over-written later. That's why if you're using any printer type previously defined in Redeem (core XY, cartesian, i3, mendelmax, etc.) you should only change settings in local.cfg and not in your local version of printer.cfg. The printer.cfg defines a type of printer, the local.cfg is specific to your hardware, if that makes sense?


---
**Daniel Kusz** *January 14, 2017 14:04*

Jon, I totally agree with you an with the files hierarchy. It works. If you build 3d printer from scratch with one of popular kinematics good practice I think, is to copy one of the printer.cfg and edit or make new with specific parameterers there. In this case you can make you specification changes in printer.cfg and save it on computer before reflash/update. After reflashing you need only to upload you specific printer.cfg. Updating existing printer.cfg you can do by the same - copying and making new specific printer.cfg. Then you don't need to use local.cfg - some another level in hierachy. With using local.cfg or only copied printer.cfg after update anyway you need to paste or upload your configuration or file. You always have something to upload or paste. Difference for me is that local.cfg can be edited directly in Octoprint without upload what is big advatage when you test your printer configurations. When it's done you can make approved printer.cfg and use it. This is my opinion when I use Replicape for the custom printer.


---
**Andrew A** *January 21, 2017 10:31*

ok so is there a reason why im not seeing any printer profiles in octoprint?  the stable version had the printer profiles like corexy ect, but i can't find them in this version.  



in settings under cura, there is a button to 'test' the cura folder, it says path does not exist.  i ftp in and look in this folder and i see nothing named cura..........







Also upon logging into octoprint it says there is an update to kamikaze, "Kamikaze: unknown

Installed: Commit a4485840f820ddf8c3a77a0df0eb1906eb322322

Available: Commit a77ee3aa250a304e2dc1a8d5d39169480fcd244e"



i try to install and it says



" Updating, please wait.

dd: failed to open '/dev/mmcblk0': Permission denied

dd: failed to open '/dev/mmcblk0': Permission denied

** Updating Kamikaze **

** install U-boot**



The update did not finish successfully. Please consult the log for details."









So i go back to the wiki page and copy/paste all the commands under "update packages" again.  Then i put the commands for "Manual installation of package feed "



i log back in to octoprint, the 'update' still doesn't work.  i look in curaengine, and the folder still fails the 'test', but now i have 3 printer profiles, kossel_mini_02, prusa_i3_02, thing_02.  But no corexy?  I do spot a maxcorexy in the redeem part.  



But i assume none of those cura profiles will work with corexy.  Maybe im wrong but it seems like something is just wrong or installed wrong?



I just relogged into octoprint and go to the curaengine and now the profiles are gone again......redeem profiles are still there, but magically the 3 that were in cura gone........seriously what is going on


---
**Andrew A** *January 21, 2017 10:35*

ok so i open the settings do nothing other than click save, and then the 3 profiles appear when i reopen settings......but the curaengine still seems to be missing? I mean it fails the test anyway


---
**Andrew A** *January 22, 2017 07:16*

So i had octoprint 'connecting' last night but this morning it wouldn't 'connect'........only said 'auto' and something about a serial connection.....which is strange since i was connecting via 'wifi' and if im connecting to octoprint surely i have a 'connection'.......



anyways yet another flash........apon boot, i do the update part of the wiki, but avoided the 'manual update' at the bottom of the wiki this time thinking maybe that broke it last night doing it.



so after the update i get into octoprint, in settings curaengine 'test' still fails no big surprize, i see 3 printer profiles cool....go to redeem, its empty, no profiles.  i restart redeem, save, reopen, cure is empty this time, and redeem has profiles.............seriously this is some shit.



so then i connect, it works, i play with my motion control for the first time, realize i need to slave a motor to another, i edit the local.cfg via octoprint and the little 'edit' button, save, didn't work.  so then i save it to my hard drive, edited it, and try to 'import' it.  i select the file, try to click the button and nothing happens........so i can't import files......i change the name try again, still nothing.



So then i think, ok maybe i just need to reboot, so i do that, log into octoprint, click connect.........now it won't connect again, usb too........jesus fkin christ................ive had nothing but trouble with this thing all its doing is wasting my time..........


---
**Jon Charnas** *January 22, 2017 11:25*

Ok, that is troubling. May I suggest you try to flash the latest Kamikaze 2.1.0 RC4 image? There's one minor annoyance with octoprint trying to update on every boot but it should be fixed quickly. You can download it from here: [github.com - Kamikaze2](https://github.com/goeland86/Kamikaze2/releases/tag/2.1.0-rc4)



Then there might be a bug in the octoprint_redeem plugin that won't list files unless you upload a blank local.cfg file. Sorry I was unavailable to get more involved in your debugging process before.



Also with this RC4 you should see in Octoprint directly which version you've installed (It says 2.1.0 RC4 in the octoprint header). You can change it in the settings of course, but there's an additional file you can read in /etc/kamikaze now that also contains the version and when the image was originally created. These two files will become part of the standard for future releases.


---
**Andrew A** *January 23, 2017 01:54*

 same problem still, i can connect on fresh install, but eventually it looses the ability to connect and ask's to do the manual serial port thing.  i tried usb, ethernet, and wifi........



i have 1 hunch im going to try so i am reflashing, and im not going to click the 'restart redeem' button in settings, ever.  and see if that is what is causing my issues.



if that fails the only thing i can think of is finding a way to compare my fresh install files to the files after it stops working, but im not sure how to do that.  maybe ill save the dmesg screen on first boot, then do a dmesg when its borked but not if that will catch it or not.



the sad thing that just dawned on me is i can't even sell this replicape because its not working


---
**Andrew A** *January 23, 2017 03:13*

ok so fresh flash, connected, played with motors a bit, i think powered down, once, maybe twice, and rechecked that everything worked and it did.  I then look inside cura and redeem options and they are empty, no printer profiles.  just the local.cfg and default.cfg.  local being blank.  default had some stuff in it.



so then i edit one line into the local.cfg, and then try to import a local.cfg, click save, power down, power up, can't connect......



and again, when i can't connect, my motors/fan on power supply is all making lots of noise and it never stops.  When things are working, that noise gets very quiet after about 30 second from powering on, so its like something is freezing up during boot.



i copied a before and after dmesg and will upload them, they do vary slightly afound "5.4" and also towards the very end, things get out of order so it gets hard to follow/compare.



also of note it says 'no cape found' somewhere in there, which obviously seems like a huge red flag, except its also in the one that was working



working [http://textuploader.com/d1af9](http://textuploader.com/d1af9)



<s>not working</s>[http://textuploader.com/d1af0](http://textuploader.com/d1af0)



[textuploader.com - Untitled Post](http://textuploader.com/d1af9)


---
**Jon Charnas** *January 23, 2017 04:54*

Ok, so, if you look in both, you'll see 4 lines where it's looking for the replicape (working and not-working files): [ 3.357152] bone_capemgr bone_capemgr: slot #0: 'Replicape 3D printer cape,00 - it only shows "no cape found" for slots 1-3, but that's normal. You only have one installed :)



My guess is what's happening is that the configuration for redeem is getting messed up. Can you look at what the output of 'systemctl status redeem -l -n 100' looks like on the not-working image? You should be seeing more information there about what happens when redeem tries to start and fails. It may even tell you which part of the configuration file doesn't work. Also you can look at the octoprint and redeem logs from the logs tab in the octoprint settings. The redeem log shows under the filename "plugin_redeem.log", that may be easier if you don't want to deal with the systemctl command, the log file will simply contain all the output from redeem, not just the last start's.



Other detail to check - when you flash initially, do you then select a base configuration as the favorite in the redeem settings in octoprint, after you uploaded the local.cfg? (Upload a file with just an empty line through the octoprint settings if the default configurations don't show, the available printer configurations should show up after that).



What would really help debug the setup is the output of redeem as explained above, whether you've selected a base printer configuration file for redeem to start working with, and the contents you add to local.cfg afterwards.



I'm really sorry you've had such a poor experience setting it up, because once you've started understanding how it works it's just easier and a pleasure to work with it. Hopefully we'll get you there soon!


---
**Andrew A** *January 23, 2017 07:16*

That was it.....it never occured to me that setting the local.cfg wrong would cause a 'connection' error.  Im super surprized i didn't find that during my researching that error.



The logs kept saying missing header, and then listed the the command i wrote.  So for others running into this problem if u are changing a setting for steppers for example you need to write [Steppers] first.  I was just putting the command in and assumed if i was doing it wrong it just wouldn't update that setting.



It might be good to put that local.cfg trick in the wiki under kamikaze or at the very top of the redeem wiki.  that whole dispearing thing made everything so much more confusing.  Infact it might be good to put all these 'quirks' into the wiki so people know ahead of time that all these things might happen to them, or what to do if it happens.



I mean some of this is me being dumb, maybe most of it even, but i tried searching and i seen it mentioned 5 or so times here in the community search but no fix for it. and on google itself its listed many many times in relation to octoprint but nothing to fix it.  I think i even recall ending up on octoprint's website and there wasn't anything listed about being dumb.  So yah please think of the dumb people :), im sure there's some kids out there too, i wish i had that excuse.



Seriously tho, octoprint needs to change that error from 'manually add serial port' to.....FIX YOUR LOCAL.CFG   lol........


---
**Jon Charnas** *January 23, 2017 07:20*

So the problem is that redeem and octoprint talk through a virtual serial port. This means that if redeem fails to start that connection is broken and octoprint can't really help diagnose it.



We're planning on adding syntax checking to the file upload pane in the octoprint plugin, but it's not ready yet.



Glad you going the error!


---
**Jon Charnas** *January 23, 2017 07:21*

And I'll be adding extra stuff to the wiki later today as well.


---
**Andrew A** *January 23, 2017 07:37*

ok thanks again, now for the easy part right, tweaking all the settings? lol


---
**Jon Charnas** *January 23, 2017 07:38*

Yep! We'll be here or on slack off you need more help :)


---
*Imported from [Google+](https://plus.google.com/106001037169639681932/posts/hX13F7H6WEA) &mdash; content and formatting may not be reliable*
