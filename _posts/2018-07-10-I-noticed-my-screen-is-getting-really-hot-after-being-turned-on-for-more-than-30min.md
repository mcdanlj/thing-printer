---
layout: post
title: "I noticed my screen is getting really hot after being turned on for more than 30min"
date: July 10, 2018 21:50
category: "Discussion"
author: Jean Le Flambeur
---
I noticed my screen is getting really hot after being turned on for more than 30min. Seems to be mostly on the side with the LEDs but it's definitely pretty warm to the touch. Since I'm using this as a battery powered FPV screen I'm worried about the power consumption and prolonged usage in an enclosed case... What is causing the heat? Is it a regulator for the LEDs?







**Jean Le Flambeur**

---
---
**Elias Bakken** *July 10, 2018 22:18*

I think the main reason for the heat from the pcb is the HDMI to DSI conversion in the main chip. There are also a couple of LDOs that will generate heat. The backlight will also generate a lot of heat (normal for lcds) which can be lowered at the expense of a lower brightness. 


---
*Imported from [Google+](https://plus.google.com/117821889871231142040/posts/ScP9iemhjP8) &mdash; content and formatting may not be reliable*
