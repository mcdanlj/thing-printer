---
layout: post
title: "So I'm building my second printer which is a hypercube 400 x 400 x 500"
date: July 13, 2017 05:01
category: "Discussion"
author: Rudy Rawlins
---
So I'm building my second printer which is a hypercube 400 x 400 x 500.  I'm using the beaglebone black and replicape with AC powered heat bed. Was wondering what is the minimum power supply wattage I would need for my setup.





**Rudy Rawlins**

---
---
**Jon Charnas** *July 13, 2017 05:43*

That's the heatbed which will be powered by AC - so not a problem for the replicape's PSU. 



The rest of the energy you're using will be limited. Depending on whether you've got a single or multi-extruder setup you have 30 to 40W per heater cartridge, and the current you'll put through your steppers. 



Assuming max current (1.2A), that's 1.2*5*12 = 72W just for the 5 steppers at 12V, or 144W at 24V. I would strongly advise for a 350W PSU if you're going with 24V, knowing the replicape has a 20A fuse on board. With 12V you could get away with 150 or 200W PSU. You'll need to account for almost 1.5A of current at 5V for the beaglebone itself (add some losses due to the voltage regulator). Then there's the accessories you might add, like LEDs, the fans, etc. which shouldn't be too much.


---
**Rudy Rawlins** *July 13, 2017 11:05*

Thanks for the info. I will buy a 24v 400w supply then taking into account I may use dual extruder and lighting.


---
**Jon Charnas** *July 13, 2017 11:20*

Keep in mind the fan outputs are on a 12V regulator - you'll always get 12V out of the fan pins ;)


---
**Rudy Rawlins** *July 13, 2017 12:03*

**+Jon Charnas** Thanks was wondering about that. But the extruder is 24V right?


---
**Rudy Rawlins** *July 14, 2017 13:21*

I want to use these end-stops on the replicape. Will they work? [https://www.aliexpress.com/item/6-pcs-lot-Optical-Endstop-Light-Control-Limit-Optical-Switch-for-3D-Printers-RAMPS-1-4/32434841034.html?spm=a2g0s.9042311.0.0.gf1zrt](https://www.aliexpress.com/item/6-pcs-lot-Optical-Endstop-Light-Control-Limit-Optical-Switch-for-3D-Printers-RAMPS-1-4/32434841034.html?spm=a2g0s.9042311.0.0.gf1zrt)




---
**Jon Charnas** *July 14, 2017 13:31*

I believe they will, as long as it's a simple "open/close" signal that's given it will work. I think the cape even has a triplet of pins for each endstop precisely for those who wanted to use optical endstops. Check the fritzing example for which order the pins should go into: [wiki.thing-printer.com - Replicape rev B - Thing-Printer](http://wiki.thing-printer.com/index.php?title=Replicape_rev_B#Fritzing_example)


---
**Rudy Rawlins** *July 14, 2017 13:54*

Thank much!




---
*Imported from [Google+](https://plus.google.com/107020357971886653051/posts/DcvLMwdeqd5) &mdash; content and formatting may not be reliable*
