---
layout: post
title: "I see the following in the redeem log file: 2016-02-18 04:19:14,660 root ERROR Alarm: Stepper Z I was actually printing and there was some issues with the extrusion and I stopped the print"
date: February 18, 2016 04:25
category: "Discussion"
author: Veridico Cholo
---
I see the following in the redeem log file:



2016-02-18 04:19:14,660 root         ERROR    Alarm: Stepper Z



I was actually printing and there was some issues with the extrusion and I stopped the print. After I stopped, I saw the above message in the Octoprint terminal and in the redeem log file.



The printer continues to move and heat up, etc. I haven;t tried printing again.

What's going on?





**Veridico Cholo**

---
---
**Elias Bakken** *February 18, 2016 11:50*

That is most likely too high current on Z. Try lowering it.


---
**Veridico Cholo** *February 18, 2016 13:47*

Thanks Elias, I'll try that. I have two Z motors connected and driven together, so I made the current higher (1.2 amps). Maybe I will disconnect one and try cloning it on the H driver.


---
**Veridico Cholo** *February 19, 2016 15:47*

Lowering the current fixed the alarm. Thanks Elias!


---
*Imported from [Google+](https://plus.google.com/109713994627754483735/posts/2RQssjKw8vk) &mdash; content and formatting may not be reliable*
