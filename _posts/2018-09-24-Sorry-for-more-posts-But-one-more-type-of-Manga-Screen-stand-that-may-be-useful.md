---
layout: post
title: "Sorry for more posts. But, one more type of Manga Screen stand that may be useful"
date: September 24, 2018 14:51
category: "Discussion"
author: Chris Deister
---
Sorry for more posts. But, one more type of Manga Screen stand that may be useful. These are simple easel-style braces with countersunk screw and screen mount holes. Two options: one that is taller with longer legs allowing portrait or landscape mounts, and another that looks nicer with shorter legs that just lets you mount in landscape. 



[https://www.thingiverse.com/thing:3117797](https://www.thingiverse.com/thing:3117797)





**Chris Deister**

---
---
**Jon Charnas** *September 24, 2018 14:55*

Feel free to share those as you like - they're really cool.


---
*Imported from [Google+](https://plus.google.com/106165570706316836863/posts/Ty2GWZ4Z84x) &mdash; content and formatting may not be reliable*
