---
layout: post
title: "The following video is after issuing G28 Z0 (the default octoprint home Z) Every direction works correctly when moved normally"
date: February 21, 2016 06:36
category: "Discussion"
author: Philip Acierto
---
The following video is after issuing G28 Z0 (the default octoprint home Z)



Every direction works correctly when moved normally. But it homes once.. that moves in the wrong direction away from the sensor, unlike the X and Y which come in a second time closer, slower.



What is worse is after it does this, I can no longer reduce the Z axis any more. I can +10mm but when I try to -1mm it doesnt move, almost like an end stop...



Below is my settings for the end stop:



end_stop_X2_stops =

end_stop_Y2_stops =

end_stop_Z1_stops =

end_stop_Z2_stops = z_neg



soft_end_stop_min_z = -0.5

soft_end_stop_max_z = 0.3



Those shouldn't be triggered except the inductive Z2, which is the expected. but look at the following after Homing Z and then moving the Z.. sure enough:



Send: G1 Z10.0000 F450

Recv: ok

Send: M119

Recv: ok X1: False, X2: True, Y1: False, Y2: True, Z1: True, Z2: True



Even though I moved it +10, the Z2 (the inductive sensor) is still triggering. Though physically, I can see it is not triggered, as there is a light on top of the senor that comes on when it is triggered.



Any ideas?




**Video content missing for image https://lh3.googleusercontent.com/-F0GTXSOiqF8/Vsla_-9nsUI/AAAAAAAAC7s/HXa4PoYvzOo/s0/VID_20160220_221452.mp4.gif**
![images/57873613023d93cf2ce7f751820138f6.gif](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/57873613023d93cf2ce7f751820138f6.gif)



**Philip Acierto**

---
---
**Elias Bakken** *February 21, 2016 12:56*

I think back off length can be set for z, could that be wrong? Is soft end stop right direction?


---
**Philip Acierto** *February 21, 2016 19:08*

The soft end stops are basically a clone of my X and Y, which are correct and behave as expected. Now the values may be a bit off as my steps per mm aren't very accurate, but the positive and negative values are correct as far as I can tell.



I don't remember seeing a back off value able to be set for Z homing (I did see one for probing in the macro).



I'm not any closer to figuring this one out yet..


---
**Veridico Cholo** *February 21, 2016 23:29*

Obvious question: Is your z axis inverted? since the platform goes down as z increases.

I have another printer like yours and in its firmware the z axis is inverted.

There is a setting in redeem for that: direction_z = -1


---
**Philip Acierto** *February 21, 2016 23:55*

It is inverted actually, which allows it to move up and down as expected.



It's only when homing Z it works wrong. Which is why I think it may be a bug in the homing code.


---
**Elias Bakken** *February 22, 2016 02:11*

Is z and h not the same direction? 


---
**Philip Acierto** *February 22, 2016 02:14*

I got rid of H now, rewired both the Z axis to one stepper motor. I put smaller motors at .8 amp each, instead of the larger ones I had before.



They are nema 23 driving true ball screws; I like over kill :) cnc status.



So we can eliminate the H axis issue/variable I was having before.


---
**Elias Bakken** *February 22, 2016 02:22*

Ok, but do you remember if they were opposite? If so that would be bug I guess.


---
**Philip Acierto** *February 22, 2016 02:36*

They were both set to -1.



When I removed the H stepper, I commented out the invert H, so now only invert Z is currently active.



Inverted Z is what is needed to bring the bed closer to the nozzel when lowering the Z value, and further away when increasing Z. Had to get it right in my head the raising the bed in the physical world needs to be really lowering the Z values.


---
**Philip Acierto** *February 23, 2016 00:28*

So did anything with Z leveling make it into the dev build? Or am I at a standstill for a while?




---
**Elias Bakken** *February 23, 2016 00:30*

I think we should do a "hotfix" for the probe bug and get that into mainline directly, unless you managed to get Daryl to include it? It will have to wait until I get back though...


---
*Imported from [Google+](https://plus.google.com/111974602640488998735/posts/c3yHtRiJ9uH) &mdash; content and formatting may not be reliable*
