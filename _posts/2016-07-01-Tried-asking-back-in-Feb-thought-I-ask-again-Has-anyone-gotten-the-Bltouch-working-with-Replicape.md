---
layout: post
title: "Tried asking back in Feb, thought I ask again: Has anyone gotten the Bltouch working with Replicape?"
date: July 01, 2016 18:07
category: "Discussion"
author: Philip Acierto
---
Tried asking back in Feb, thought I ask again:



Has anyone gotten the Bltouch working with Replicape? They released another version since last time I asked, so maybe more people have them? I have yet to buy one; waiting to see for sure they work. The config is what I'd be curious about.



Replicape has servo control and then a regular end stop, so I think it would work in theory.





**Philip Acierto**

---
---
**James Armstrong** *July 01, 2016 18:22*

Question, if this is the "new" BLTouch, from what I see is referred to as BLTouch-C or "Classic", then is it really a new model or just a new name? Classic infers an old or previous model.


---
**Philip Acierto** *July 01, 2016 18:26*

I know very little about the development of these sensors, I know that this white/clear version replaced the yellow version of old, but what was upgraded/fixed/replaced I do not know.



I just know I have a glass bed and my inductive sensor over aluminum tape is not the best solution for me, as I've discovered with bed probing malfunctions and adjusting distances while calibrating my steps_per_mm.



Having a physical button without a swing out arm, the BLtouch essentially, sounds like a great solution to me.


---
**Elias Bakken** *July 01, 2016 18:42*

Did I not see a video of the working a few weeks ago? I thought it was on this page, but I might be wrong...


---
**Philip Acierto** *July 01, 2016 20:32*

If you are certain you saw somewhere a Bltouch working with replicape, then that is pretty much what I Wanted to know, that it works. Getting spoon fed a config would be even nicer, but can't have everything :)



Thanks for tracking down my inductive sensor issue last night causing homing issues with g29 so that I can get by for now; it working at least re-energized my enthusiasm for the project.



This Bltouch sensor would be the next fix to eliminate jamming my print head into the bed when it isn't over my preset aluminum tape points.


---
**Elias Bakken** *July 01, 2016 20:37*

I found the video: 
{% include youtubePlayer.html id=MSm_5oW4wKA %}
[http://youtu.be/MSm_5oW4wKA](http://youtu.be/MSm_5oW4wKA) he does report a problem... **+Ville Jussila** did you solve this?


---
**Philip Acierto** *July 01, 2016 21:05*

I would love this sensor to work Elias; I don't know of anything that works better. Because of issues with inductive sensors and glass beds, I can't recommend that route. If swinging arm + micro switch was Level 1, the BLtouch with no physical arm swing with a mechanical button is Level 2.


---
**Ville Jussila** *July 01, 2016 21:22*

No i did not get it working, there is some strange problem with detecting pulse-alike end stop events. Did file a issue on bitbucket, as i believe it is some how related to PRU code.

[https://bitbucket.org/intelligentagent/redeem/issues/103/pru0-wont-stop-on-10ms-endstop-signal](https://bitbucket.org/intelligentagent/redeem/issues/103/pru0-wont-stop-on-10ms-endstop-signal)


---
**Ville Jussila** *July 01, 2016 21:34*

Or to be more precise redeem / replicape will detect those pulse-alike signals, but for some reason it does not break actions on PRU0. Othervise BLTouch is working right as it supposed, all "servo commands" are working etc.


---
**Elias Bakken** *July 01, 2016 21:44*

**+Ville Jussila** **+Philip Acierto** OK, thanks for clearing that up. 10ms might not be enough to test for end stop hit + delete all remaining steps in the buffer/path. Most likely this will need some manual mingling, ideally in the form of a plugin that can capture the event and set the end stop until it returns etc. I've been looking at something similar to this, since I am currently researching different probe mechanisms, for my upcoming video documenting the bed probing and a friend has a somewhat special solution using an accelerometer. If I get that working, a solution for BLtouch should not be too far away. 


---
**Ville Jussila** *July 01, 2016 21:51*

thanks, that sounds good, keep us posted.



If you need some help, i could spend some time on this, if it is python related.


---
**Andrew Dowling** *July 08, 2016 13:34*

Second some sort of workaround to get this working. I really want to use this touch probe.


---
**Per Hassel Sørensen** *February 25, 2017 11:03*

Is the short pulse (10 ms) from BLtouch now handled correctly by current software relase? 

I am asking as I plan on adding the BLtouch for auto levelling to my Podrap1 XXL cartesian dual extruder printer:

<iframe src="[https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FGoPodbike%2Fvideos%2F1363181500359218%2F&show_text=0&width=560](https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FGoPodbike%2Fvideos%2F1363181500359218%2F&show_text=0&width=560)" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>


---
*Imported from [Google+](https://plus.google.com/111974602640488998735/posts/fybpn82rZUU) &mdash; content and formatting may not be reliable*
