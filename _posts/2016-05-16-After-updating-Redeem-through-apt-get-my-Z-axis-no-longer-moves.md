---
layout: post
title: "After updating Redeem through apt-get my Z axis no longer moves"
date: May 16, 2016 23:42
category: "Discussion"
author: Chris Romey
---
After updating Redeem through apt-get my Z axis no longer moves.  I have most variables defined in my local.cfg file for my printer so nothing should have been overwritten.



What should I be looking at to troubleshoot this?  I'm not getting anything terribly useful out of the debug logs.  I can hear my steppers power up when the drivers power on from idle.



Does not seem to be an endstop issue, either:



Send: M119

Recv: ok X1: True, X2: False, Y1: True, Y2: False, Z1: False, Z2: False



Logs when running G28 Z0:



May 16 23:40:15 kamikaze redeem[634]: 05-16 23:40 root         DEBUG    homing ['Z']

May 16 23:40:15 kamikaze redeem[634]: 05-16 23:40 root         DEBUG    endstop active mask = 0b111111

May 16 23:40:15 kamikaze redeem[634]: 05-16 23:40 root         DEBUG    homing internal ['Z']

May 16 23:40:15 kamikaze redeem[634]: 05-16 23:40 root         DEBUG    Doing homing for Z

May 16 23:40:15 kamikaze redeem[634]: 05-16 23:40 root         DEBUG    Search: {'Z': -0.2} at 0.02 m/s, 0.5 m/s^2

May 16 23:40:15 kamikaze redeem[634]: 05-16 23:40 root         DEBUG    Backoff to: {'Z': 0.01}

May 16 23:40:15 kamikaze redeem[634]: 05-16 23:40 root         DEBUG    Fine search: {'Z': -0.012}

May 16 23:40:15 kamikaze redeem[634]: 05-16 23:40 root         DEBUG    Center: {'Z': -0.0}

May 16 23:40:16 kamikaze redeem[634]: 05-16 23:40 root         DEBUG    Enabling stepper Y

May 16 23:40:16 kamikaze redeem[634]: 05-16 23:40 root         DEBUG    Enabling stepper X

May 16 23:40:16 kamikaze redeem[634]: 05-16 23:40 root         DEBUG    Enabling stepper Z

May 16 23:40:16 kamikaze redeem[634]: 05-16 23:40 root         DEBUG    Enabling stepper E

May 16 23:40:16 kamikaze redeem[634]: 05-16 23:40 root         DEBUG    Enabling stepper H

May 16 23:40:16 kamikaze redeem[634]: 05-16 23:40 root         DEBUG    Coarse search done!

May 16 23:40:16 kamikaze redeem[634]: 05-16 23:40 root         DEBUG    endstop active mask = 0b111111

May 16 23:40:16 kamikaze redeem[634]: 05-16 23:40 root         DEBUG    Home: {'Z': 0.0}

May 16 23:40:16 kamikaze redeem[634]: 05-16 23:40 root         DEBUG    homing done for ['Z']

May 16 23:40:16 kamikaze redeem[634]: 05-16 23:40 root         INFO     Homing done.











**Chris Romey**

---
---
**Daryl Bond** *May 17, 2016 02:19*

Have you checked your soft endstop limits?


---
**Chris Romey** *May 17, 2016 02:42*

**+Daryl Bond** Yep, they're the same as before. :(



end_stop_Z1_stops = z_neg,h_neg



soft_end_stop_min_z = -0.5

soft_end_stop_min_h = -0.5



soft_end_stop_max_z = 0.3

soft_end_stop_max_h = 0.3




---
**Chris Romey** *May 17, 2016 06:45*

Well I've figured it out. I might be crazy but it looks like the syntax changed for setting slave steppers.



Old = slave_h = Z (from previous local.cfg which was working before updating redeem)

New = slave_z = H (updated after re-checking the wiki)


---
**Daryl Bond** *May 17, 2016 11:12*

Good to know you found it! There should have been a readout at the start of the log about the axis slaving, is that what pointed you in the right direction?


---
**Chris Romey** *May 17, 2016 18:30*

**+Daryl Bond** I wasn't seeing anything in the log files about stepper slaving that jumped out at me.  However, for investigative sake I flipped the syntax back to the original to see if anything changed in the logs.  Since both configs are valid from a config standpoint there were no errors.



Tiny crumbs of info! :)



slave_h = Z



May 17 18:21:07 kamikaze redeem[1554]: 05-17 18:21 root         DEBUG    Axis 2 is slaved to axis 4

May 17 18:21:07 kamikaze redeem[1554]: 05-17 18:21 root         DEBUG    Axis Y min steps/s = 400.0

May 17 18:21:07 kamikaze redeem[1554]: 05-17 18:21 root         DEBUG    Axis X min steps/s = 400.0

May 17 18:21:07 kamikaze redeem[1554]: 05-17 18:21 root         DEBUG    Axis Z min steps/s = 2000.0

May 17 18:21:07 kamikaze redeem[1554]: 05-17 18:21 root         DEBUG    Axis E min steps/s = 4014.4

May 17 18:21:07 kamikaze redeem[1554]: 05-17 18:21 root         DEBUG    Axis H min steps/s = 2000.0







slave_z = H



May 17 18:27:18 kamikaze redeem[1685]: 05-17 18:27 root         DEBUG    Axis 4 is slaved to axis 2

May 17 18:27:18 kamikaze redeem[1685]: 05-17 18:27 root         DEBUG    Axis Y min steps/s = 400.0

May 17 18:27:18 kamikaze redeem[1685]: 05-17 18:27 root         DEBUG    Axis X min steps/s = 400.0

May 17 18:27:18 kamikaze redeem[1685]: 05-17 18:27 root         DEBUG    Axis Z min steps/s = 2000.0

May 17 18:27:18 kamikaze redeem[1685]: 05-17 18:27 root         DEBUG    Axis E min steps/s = 4014.4

May 17 18:27:18 kamikaze redeem[1685]: 05-17 18:27 root         DEBUG    Axis H min steps/s = 2000.0




---
**Daryl Bond** *May 17, 2016 21:13*

Yeah, the info is there but it is pretty hard to spot! Do you want to raise a ticket on the repo asking for clearer logging of the slave settings?


---
*Imported from [Google+](https://plus.google.com/113335787317667734340/posts/3otoDd1QXwa) &mdash; content and formatting may not be reliable*
