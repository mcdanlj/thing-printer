---
layout: post
title: "aaaand it's printing calibration cube :) it started the print too high so it detached mid print"
date: January 04, 2017 07:17
category: "Discussion"
author: Step Cia
---
aaaand it's printing calibration cube :) it started the print too high so it detached mid print. not sure why it starts higher than expected After G29 triangulation hmmm


**Video content missing for image https://lh3.googleusercontent.com/-PJysGmk9oho/WGyhk53OQjI/AAAAAAABWiw/xKojBQJZ4D4rVcdV-O8m12K6A6kihBEZgCJoC/s0/20170103_225924.mp4**
![images/6f9764fd6983f7552f6d8ad28c25aa35.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/6f9764fd6983f7552f6d8ad28c25aa35.jpeg)



**Step Cia**

---
---
**Step Cia** *January 04, 2017 07:19*

haha in the video that creaking sound is from the printed pulley seems to be more amplified...


---
**Elias Bakken** *January 04, 2017 11:36*

G29 does not alter the end stop offset, so you need to adjust that manually! 


---
**Step Cia** *January 04, 2017 14:10*

Hmm ok I did not see that in the wiki... Is it under [probe]?


---
**Step Cia** *January 05, 2017 07:32*

in this case, my tip of hot end is actually = probe. do I still need offset?


---
**Step Cia** *January 06, 2017 16:35*

I think i figured it out... In the video clearly the z is not doing any active bed leveling while printing. I then added M561U (to update the bed matrix) and M500 at the end of my G29 code which seems to solve the issue. It is now actively changining z when printing :)



Should add those to the probe section of the wiki


---
**Andrew Dowling** *January 06, 2017 22:52*

Or you can use G92 Z-0.2 or whatever in your starting gcode to make things higher or lower.


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/3BnjQ9kajUL) &mdash; content and formatting may not be reliable*
