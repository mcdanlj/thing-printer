---
layout: post
title: "I am setting up the \"old\" RevA4 version with a BBB with the newest Kamikaze Ver 2.1.0"
date: August 20, 2017 18:35
category: "Discussion"
author: Franz Kainz
---
I am setting up the "old" RevA4 version with a BBB with the newest Kamikaze Ver 2.1.0. When I'd like to update the eeprom of the Replicape, I get an error message with the last command on the help (all others work):



 cat Replicape.eeprom > /sys/bus/i2c/drivers/at24/2-0054/nvmem/at24-1/nvmem



error message:

bash: cd: dr: No such file or directory



Any thoughts what the correct folder is, so that I can get the "old" Replicape to work?



many thanks and best regards,

Franz





**Franz Kainz**

---
---
**Jon Charnas** *August 20, 2017 18:51*

Hi Franz, normally you shouldn't update the EEPROM on the Rev A4 boards - only a specific subset of the B3 batch had incorrect EEPROM as far as I remember. **+Elias Bakken** can you confirm?


---
**Franz Kainz** *August 20, 2017 19:58*

Many thanks Jon, Elias advised to update the EEPROM, so not sure if this is still required. 


---
**Jon Charnas** *August 20, 2017 20:26*

Yeah as far as I can remember, it was for a specific batch of B3s, and at one point it was included in Kamikaze by default to do this, but we rolled it back because it presented issues for people with the A4 version.



Speaking of, that reminds me I need to talk to one of the redeem devs to make sure we didn't lose compatibility when he re-wrote some of the PRU firmwares...


---
**Elias Bakken** *August 22, 2017 11:22*

Yes, the location of the EEPROM file that can be cated has changed a lot, but  it's in /sys somewhere : )




---
**Franz Kainz** *August 28, 2017 21:23*

found it, this is the right command for the A4

cat Replicape.eeprom > /sys/bus/i2c/drivers/at24/2-0054/at24-1/nvmem



also an option to update the Wiki, cheers, Franz




---
*Imported from [Google+](https://plus.google.com/+FranzKainz/posts/fXjPUrq1ZdB) &mdash; content and formatting may not be reliable*
