---
layout: post
title: "Replicase is ready to print. House your Beaglebone + Replicape in style!"
date: March 14, 2018 22:15
category: "Discussion"
author: Michael K Johnson
---
<b>Replicase</b> is ready to print. House your Beaglebone + Replicape in style!



**+Elias Bakken** this is meant to be a fairly universally useful case for mounting at least on rectangular extrusion-based printers; there's nothing particularly relevant to the Tronxy X5S I made it for. Please feel free to add to any pointers to useful Replicape resources.



[https://www.thingiverse.com/thing:2826626](https://www.thingiverse.com/thing:2826626) (to be easy to find) and [https://github.com/johnsonm/Replicase](https://github.com/johnsonm/Replicase) (please fork and PR!)





**Michael K Johnson**

---
---
**Elias Bakken** *March 15, 2018 11:31*

Nice! I'll try to make one:)


---
*Imported from [Google+](https://plus.google.com/+MichaelKJohnson/posts/3iw63WNrLvr) &mdash; content and formatting may not be reliable*
