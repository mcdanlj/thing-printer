---
layout: post
title: "Replicape is home ... fast delivery to Slovakia, I like it..."
date: April 01, 2016 21:10
category: "Discussion"
author: Jozef Török
---
Replicape is home 😊...  fast delivery to Slovakia, I like it...  thank you

![images/75ab99f605d2ddebb8be09c23bcb0ac3.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/75ab99f605d2ddebb8be09c23bcb0ac3.jpeg)



**Jozef Török**

---
---
**Jozef Török** *April 01, 2016 22:14*

If you mean replicape then yes,...At first sight, product is great quality and looks amazing, and I believe that its functionality will be the equally amazing! 


---
**Elias Bakken** *April 01, 2016 22:15*

It's been Easter holiday this past week, so packages are probably going to be a bit delayed because of that unfortunately, but a lot were picked up on Tuesday, so 4-8 business days from then for people who ordered last week!


---
**Daniel Kusz** *April 02, 2016 06:11*

I'm waiting too for my replicape :-) I hope it will soon in Poland. 


---
**Daniel Kusz** *April 09, 2016 11:53*

What kind of delivery you had? Mail or FedEx? How long you are waiting for? My package was sent by mail on Monday and I'm still waiting.


---
**Jozef Török** *April 10, 2016 08:47*

Package was sent by mail and I waited cca 7 days.


---
*Imported from [Google+](https://plus.google.com/103177972435054977140/posts/a2LVHPP1YSY) &mdash; content and formatting may not be reliable*
