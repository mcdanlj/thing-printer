---
layout: post
title: "I nearly have my Kossel Mini dialed-in with the Replicape but haven't figured out the software endstop adjustment"
date: February 01, 2016 04:24
category: "Discussion"
author: Gregg Geschke
---
I nearly have my Kossel Mini dialed-in with the Replicape but haven't figured out the software endstop adjustment.



I was previously using a Printrboard with Repetier firmware and was using the M666 command to make small adjustments to the endstop position for each of the 3 columns. When homing the printer, it would locate the endstops, then back off each column by the M666 endstop adjustment value. That would be the home position.



The M666 command is implemented in the Redeem software, but I don't understand what is actually being adjusted. It changes the path_planner.center_offset values - but isn't that the center position in Cartesian space?



Also, in Repetier firmware, the values for the M666 command are given in mm units, but I think the Redeem M666 command is expecting meters.



Any thoughts? Thanks!





**Gregg Geschke**

---
---
**Jon Charnas** *February 01, 2016 08:24*

As to why the Redeem expects meters instead of mm, it's because Redeem uses only SI units, it's maybe not well highlited in the wiki though.


---
**Elias Bakken** *February 01, 2016 17:30*

I haven't used M666, but it looks very similar to M206. Try "M206?" for a long description, it is better documented. 

 


---
**Gregg Geschke** *February 03, 2016 05:20*

Thanks **+Elias Bakken**.



The M206 command doesn't seem to be implemented in the Redeem firmware.



I've played with M666 some more and figured out how to do the software endstop adjustment I was wanting, then copied them into the offset_[xyz] parameters in my local.cfg. It's working great now.



BTW, the  #replicape  is really quiet with the microstepping 8 setting. Thanks for such a nice design!


---
*Imported from [Google+](https://plus.google.com/101170487506966401331/posts/Ctpo2UVF3uG) &mdash; content and formatting may not be reliable*
