---
layout: post
title: "I received my manga screen 2 a few days ago and while it works great with mac and pc, i have problems getting it to display a correct image on Android"
date: June 14, 2018 09:56
category: "Discussion"
author: Philipp Luftensteiner
---
I received my manga screen 2 a few days ago and while it works great with mac and pc, i have problems getting it to display a correct image on Android.



As the documentation states the screen should work out of the box with an Odroid C2 running Android, i bought it for that particular reason.



While touchscreen and HDMI are working technically, the image appears 90 degree rotated and split in the middle, probably because of the 1080x1920 native resolution.



Theoretically i could build a custom rom, but i have no idea where to start.



I there a way to switch the resolution via serial interface?



Thank you!





**Philipp Luftensteiner**

---
---
**Elias Bakken** *June 14, 2018 14:51*

Yeah, there should be a way to rotate the image on Android. I've forgotten what file to edit, but you should be able to Google that with no reference to the screen if you have it working in portrait mode. 


---
**Philipp Luftensteiner** *June 14, 2018 19:27*

Hi elias, i made this image to show the problem, it looks the same on all android machines i tried (odroid c2, odroid xu4 and khadas vim). 



I saw EDID mentioned in the documentation, could that be of any help? 

![images/b3938408beef2ddf60bbc99bc3b80a83.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/b3938408beef2ddf60bbc99bc3b80a83.jpeg)


---
**Claudio Prezzi** *July 03, 2018 12:07*

I get the same problem on Raspberry Pi 3 B+


---
**BOT DINGO** *August 18, 2018 15:49*

same


---
**Elias Bakken** *August 21, 2018 08:56*

There are two problems with this picture, the resolution is wrong (1920x1080) instead of 1080x1920. Also the rotation is wrong. 


---
**BOT DINGO** *August 23, 2018 13:58*

**+Elias Bakken** I think Phillipp knows this, I also tried switching it to 1080x1920. In either configuration Portrait or Landscape the image will duplicate 2+ times. This is only on SBC boards for me, if I connect to a computer it displays correctly. Can you show us what the config text should look like for a raspberry pi? Following the directions on the wiki is what gives me the result above.


---
*Imported from [Google+](https://plus.google.com/+PhilippLuftensteiner/posts/TqCeNxjPnG1) &mdash; content and formatting may not be reliable*
