---
layout: post
title: "Hi. Anyone that can explain way the Z axis is hard to set up on my Mendel90"
date: April 02, 2016 10:35
category: "Discussion"
author: Jan M. Simonsen
---
Hi. Anyone that can explain  way the Z axis is hard to set up on my Mendel90. I set this int the cfg file.

offset_z = -0.224

travel_z = -0.224

But when the Z axis hit the enstop on top an moving down again it stops at 180mm i want it to move 224mm





**Jan M. Simonsen**

---
---
**Jan M. Simonsen** *April 03, 2016 06:01*

When i manualy move Z axis step/unit is correct. It move 10mm  when i want 10mm and same with 100mm. so i think that is correct.




---
**Elias Bakken** *April 24, 2016 12:57*

What is your home_x value? Set it to 0 for now.


---
**Jan M. Simonsen** *April 24, 2016 14:38*

The home value for all axis are 0


---
**Elias Bakken** *April 24, 2016 16:22*

If you set the value to something higher than -0.224, does it still stop at -0.180? Are you certain the steps pr mm is right? Could you try issuing a gcode M206? That should give you the currently set offset values.


---
**Jan M. Simonsen** *April 24, 2016 16:31*

as i can see the step pr mm is correct. If i change the value higher or lover it still stop at the same place. And value its same with M206


---
**Elias Bakken** *April 24, 2016 18:08*

Disregarding the option that an end stop is stopping the z-axis at that point and home_z is 0 and you have verified that the m206 gives you the same value that you have in your local.cfg, I am really at a loss. What does M114 say after homing? 


---
**Jan M. Simonsen** *April 24, 2016 18:22*

i my have solve the porblem. there was some old probing thing in the file that what not in use.




---
**Elias Bakken** *April 24, 2016 18:23*

Ok, let me know when you run into the next problem:)


---
*Imported from [Google+](https://plus.google.com/114134914189900792065/posts/9FgPzN7cRzc) &mdash; content and formatting may not be reliable*
