---
layout: post
title: "Delta printers are a start to insanity"
date: May 02, 2016 11:25
category: "Discussion"
author: Pieter Koorts
---
Delta printers are a start to insanity. So I got the FSR sensors working and they work a treat for levelling but my printer still does not seem to know what height it is. When starting a print job, whether I tell it to start at Z0.0 or Z1.0 (gcode) it still every time pushes the head into the bed until it hits the end stop. It does not start at the height I want it to and this causes the steppers to jump a step and everything is wildly off.



What am I doing wrong here because no matter what I try the head only stops when it hits the end stop fsr. Really like the Delta but drives me insane configuring it.





**Pieter Koorts**

---
---
**Pieter Koorts** *May 02, 2016 15:24*

How do you set the print height of a Delta printer. Is this the travel_* option? I figured out I currently have to set my starting height to Z35.7 in Cura which is actually where zero is supposed to be.


---
**Daryl Bond** *May 03, 2016 00:43*

How I have setup my delta. I will list the pertinent settings from my config and then go through them.



####################################



[Geometry]

axis_config = 3



travel_x = -0.5

travel_y = -0.5

travel_z = -0.5



offset_x = -0.3087

offset_y = -0.3114

offset_z = -0.3057



[Homing]

# Homing speed for the steppers in m/s

home_speed_x = 0.15

home_speed_y = 0.15

home_speed_z = 0.15



# homing backoff speed

home_backoff_speed_x = 0.025

home_backoff_speed_y = 0.025 

home_backoff_speed_z = 0.025



# homing backoff dist

home_backoff_offset_x = 0.01

home_backoff_offset_y = 0.01 

home_backoff_offset_z = 0.01



# Where should the printer go after homing

home_x = 0.0

home_y = 0.0

home_z = 0.25

home_e = 0.0



####################################



Now the explanation.



! axis_config

Just to make sure everyone knows these settings are for a delta!



! travel_* .

The travel_ option sets how far the axes are allowed to move when homing, so you want the specified travel_* to be slightly greater than the maximum distance that axis will ever have to travel. On my delta I have a build volume of height 0.3m and so I use travel_* = 0.5 to allow for if my effector is offset from the center giving one carriage that is lower than the others.



! offset_* and home_speed_* .

First, the ideal case where you have a perfectly dimensioned machine:

Imagine that you have centered your extruder on the bed and then moved it upwards until all the endstops are hit simultaneously. All of the offset_ values will be the same and are the distance, and direction, from the tip of the extruder to the surface of the bed.



Now my case with endstops that aren't perfectly aligned:

I, figuratively, perform the same operation as above but as long as my extruder is centered then only one endstop will be triggered because they aren't all at the same height. This would be, in my case, the z tower endstop as it is the lowest absolute value I have recorded in offset_* . If I then removed that endstop and kept raising the extruder, still aligned with the center of the bed, then the next endstop I hit would be x, and then y. The relative distance from the extruder tip to the bed when each endstop is hit is what is being recorded in offset_* . This isn't the actual procedure I followed to dial in my settings but it is the easiest way I could think of to explain the overall idea.



The reason the offset_* values are negative is because my home_speed_* value is positive. I have a positive home_speed_* meaning that my carriages will travel in the positive (up) direction to engage the endstops. When I hit the endstops I am telling the printer, with the offset_* value, that the bed is some distance in the opposite (negative, down) direction.



! home_backoff_speed_* .

The speed to use when performing the fine search.



! home_backoff_offset_* . 

How far to move away from the endstop before performing the fine search.



! home_* .

Where to go after the homing procedure is finished. I have used home_z = 0.25 so that my extruder is 250mm above the bed. Another example is if you had offset_x=offset_y=offset_z=-0.3 and you set home_z=0.3 then the carriages would remain at the endstops.



I hope that helps!




---
**Pieter Koorts** *May 03, 2016 09:06*

Wow that is some awesome detail. So if I understand correctly, offset_* in geometry is used to set the print height in a sense. I think that's where I got confused as the redeem wiki wording was a little cryptic. It states origin but what is the origin.... Makes sense now.


---
**Daryl Bond** *May 03, 2016 10:57*

Yeah, I think that offset_* is the critical value for your case.


---
**Jon Charnas** *May 03, 2016 19:55*

So actually I figured this out recently as well, but offset_* is telling redeem how far above the printbed each endstop switch is being activated at. Then, as you found, the home_z value basically defines where the carriages go after hitting the endstops (you can set home_x/y to something else, too). Then there's the travel_* which defines the maximum distance Redeem will move an axis in search of the endstop. I hope this clarifies it? If so I'll update the wiki a bit to make that part of the calibration clearer.


---
**Pieter Koorts** *May 04, 2016 20:17*

Okay so the printer knows what height it is not but it seems to be printing a 10mm high test cube at 12mm high so higher than it should be. I am pretty sure I have my steps right.



I use 0.9 degree steppers and 16 tooth Gt2 pulleys at whatever the max micro stepping is. Then in redeem I have steps_pr_mm_(xyz) set to 12.5 which should be correct. Am I just being dumb and missing something?


---
**Jon Charnas** *May 04, 2016 20:23*

Your steps per mm are correct from what I compute, but check your delta rod lengths. See [http://boim.com/DeltaUtil/CalDoc/Calibration.html](http://boim.com/DeltaUtil/CalDoc/Calibration.html) - an easy way to check is to set a height near the bed and move the head around (if you have any significant error in your rod lengths or radius, it'll show up pretty quickly)


---
**Daryl Bond** *May 05, 2016 00:13*

One way to check if your steps/mm are exact is to use a set of digital calipers following these steps:



1. Move the extruder up as high as it will go.

2. Open up your calipers some amount greater than 10mm so that the depth gauge part is sticking out the bottom.

3. Place the calipers upside down on the bed, directly below the extruder, and move the extruder down until it depresses the depth gauge just a little bit.

4. Zero the calipers.

5. Command the extruder to move down 10mm.

6. Check what displacement the calipers have measured.

7. Adjust the steps/mm value according to (new steps/mm) = (10mm / measured distance in mm) x (old steps/mm)



Repeat until you get exactly 10mm measured for 10mm commanded.



From the info you have given us your steps/mm should be correct but it can't hurt to check!


---
*Imported from [Google+](https://plus.google.com/100077479073911242630/posts/MFwNNaG2jCU) &mdash; content and formatting may not be reliable*
