---
layout: post
title: "I have 2 Replicapes and BBB's for sale"
date: May 04, 2017 15:58
category: "Discussion"
author: Bob McDowell
---
I have 2 Replicapes and BBB's for sale. They were used in a school project for my daughter. Any interest?





**Bob McDowell**

---
---
**Dan Cook** *May 04, 2017 16:09*

How much for all of it?


---
**Dan Cook** *May 04, 2017 17:52*

95roverd90@[gmail.com](http://gmail.com)


---
**Bob McDowell** *May 05, 2017 04:42*

Make me an offer. I am in Germany so shipping to Euro zone no problem, I will be in the states in mid July-August so could ship then.


---
**Klipper Pressure Advance** *May 09, 2017 22:24*

how old is the Relicape? and what revision are they ?


---
**Bob McDowell** *May 11, 2017 06:05*

The Replicapes are Rev B3 and B3A, the B3 was purchased in May and the B3a in November 2016. These were used for my daughters Computer Science project in school. The Beaglebones wer purchased at the same times.



Bob






---
**Klipper Pressure Advance** *May 11, 2017 06:08*

Do you know what shipping would be to Melbourne Australia?


---
**Bob McDowell** *May 11, 2017 06:11*

Looks like around 20€


---
**Klipper Pressure Advance** *May 11, 2017 06:15*

Thats approx $30 Australian for shipping ...ok thanks ...are you willing to sell just the B3A Replicape?


---
**Bob McDowell** *May 14, 2017 19:10*

Depends on what you are offering?


---
**Klipper Pressure Advance** *May 17, 2017 00:07*

well new including shipping to Australia is approx 108€ so I offer 50€ you pay shipping for B3A.


---
**Holger Rusch** *May 22, 2017 07:34*

Hello **+Bob McDowell**, is the B3A still on sale? I am in Germany, shipping would be easy. :)


---
**Bob McDowell** *May 22, 2017 10:32*

Yes it is. 50€ +shipping 


---
**Bob McDowell** *May 22, 2017 10:32*

Yes it is. 50€ +shipping 


---
**Holger Rusch** *May 22, 2017 10:46*

**+Bob McDowell** can you please contact me via email. holger at rusch dot name

thanks.


---
**Holger Rusch** *May 23, 2017 06:55*

**+Bob McDowell** did you miss it, or was my email address unclear?



holger<@>rusch<.>name



This should be clear.


---
**Bob McDowell** *May 24, 2017 16:15*

Holger 

My email

wannathermal@[outlook.com](http://outlook.com)


---
*Imported from [Google+](https://plus.google.com/107803003115083427930/posts/3jtWostYgC6) &mdash; content and formatting may not be reliable*
