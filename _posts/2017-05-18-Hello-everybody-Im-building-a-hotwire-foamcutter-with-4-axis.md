---
layout: post
title: "Hello everybody, Im building a hotwire foamcutter with 4 axis"
date: May 18, 2017 10:57
category: "Discussion"
author: Roel Jaspers
---
Hello everybody,



Im building a hotwire foamcutter with 4 axis.

A couple of months ago I've bought the replicape to do this and a BeagleBone black WireLess.

Finally I've got everything working.

The Kamikaze 2.1.0 didn't work on the BBW.

Happily GearheadRed did some good work to get things going on the BBW.

I've the the link down here for those who are looking.



I've hooked my motors to the replicape ( Axe X,Y,Z and E), tested them with some G1 code in the terminal and that works fine.



I know I have to setup a printe.cfg file for my specific mcnc machine.



Here's my question 1:

REDEEM is working with SI-units. How do I setup the specific info for my motors?



The motors I'm using are 17HS4401 with 200 steps per revolution.

These are connect to a threaded rods (m8) with a pitch of 0.75

What do I put in the config file in SI-units?



Question 2:

I wanted to setup the Wifi from the BBW by using nmtui.

NMTUI is reporting back that I'm not allowed to do this (No authorisation).

By googling I found out that you have to login as root.

I can only use the login ubuntu:temppwd, the root:kamikaze is not working in kamikaze 2.1.1RC.

How can I get the password for this version of the image?



Thanks for yor answers.



PS: anybody else uses the replicape for a cnc machine?

















**Roel Jaspers**

---
---
**Roel Jaspers** *May 18, 2017 20:54*

I found an answer to my second question which i'll shqre here:



In ubuntu 16.0.4 the root is disabled.

After login in with ubuntu:temppwd you have to this:

sudo -i

passwrd : temppwd

Now your in root and you can use nmtui



Worked perfectly for me and google is my friend






---
*Imported from [Google+](https://plus.google.com/117611496004196437423/posts/YQKL3SjSiyX) &mdash; content and formatting may not be reliable*
