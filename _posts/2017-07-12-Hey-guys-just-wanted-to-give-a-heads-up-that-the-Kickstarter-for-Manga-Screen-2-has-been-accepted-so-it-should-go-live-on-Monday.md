---
layout: post
title: "Hey guys, just wanted to give a heads up that the Kickstarter for Manga Screen 2 has been accepted, so it should go live on Monday!"
date: July 12, 2017 08:19
category: "Discussion"
author: Elias Bakken
---
Hey guys, just wanted to give a heads up that the Kickstarter for Manga Screen 2 has been accepted, so it should go live on Monday! There are a few early bird offers, so you there's a good chance to strike a deal! 

![images/ca54743272b61d63f927f9a1bff49fc7.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/ca54743272b61d63f927f9a1bff49fc7.png)



**Elias Bakken**

---
---
**Oyvind Dahl** *July 12, 2017 09:53*

Launch party in Oslo? =D


---
**Elias Bakken** *July 12, 2017 09:55*

**+Oyvind Dahl** yes! 8 o clock on Monday! You coming? Free Cava:D


---
**Oyvind Dahl** *July 12, 2017 10:35*

I'll be there!




---
**Mathieu Paquin** *July 14, 2017 21:09*

oh :D I'm quite exciited!




---
**Jon Charnas** *July 15, 2017 16:31*

I can't wait to see how much of your old plank is in the video ;)


---
**Elias Bakken** *July 15, 2017 16:32*

Not that much actually, but many new scenes from around the hackerspace : )


---
**John Pickens** *July 16, 2017 15:11*

What time GMT is it going live on Kickstarter?




---
**John Pickens** *July 16, 2017 15:12*

(Ready to swoop)




---
**Daniel Kusz** *July 17, 2017 08:37*

 Yes, What time campaign starts? 


---
**Elias Bakken** *July 17, 2017 08:38*

8 PM CET! That is 7 PM GMT and 3PM ET. 


---
**Dan Cook** *July 17, 2017 14:17*

Woohoo!


---
**Elias Bakken** *July 17, 2017 15:05*

Well actually 3 pm ET = 9 pm CET! But we will keep the New Yorkians happy and obey their time:)


---
**John Pickens** *July 18, 2017 03:27*

Thanks, Elias, I was able to swoop in and order one! Good luck!


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/WsCzkvaav93) &mdash; content and formatting may not be reliable*
