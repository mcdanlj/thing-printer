---
layout: post
title: "Just finding out about the Revolve project"
date: September 24, 2018 07:08
category: "Discussion"
author: Paul Boulay
---
Just finding out about the Revolve project. Lots of promise. In a 3d printer application, what would it take to implement the safe pause and restore on mains power fail / restore using this?





**Paul Boulay**

---
---
**Jon Charnas** *September 24, 2018 07:12*

Quite a lot actually. Unlike on Arduino or other dedicated realtime systems, revolve and replicape offload the step execution to dedicated processors. This means that you'd need to store not just the main system info to persistent memory, but also the realtime chips memory as well.


---
**Jon Charnas** *September 24, 2018 07:13*

There are upcoming development efforts that would allow for that, but as of yet that's not ready for testing.


---
*Imported from [Google+](https://plus.google.com/110750912795553997972/posts/G5RYMZ9fUqR) &mdash; content and formatting may not be reliable*
