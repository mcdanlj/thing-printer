---
layout: post
title: "Hi. So I have a Manga 2 1080..."
date: July 09, 2018 16:03
category: "Discussion"
author: Andy Williams
---
Hi. So I have a Manga 2 1080... I'm struggling really badly to get it work with my Pi.. I've changed the config.txt etc as per Claudio Prezzi's instructions but all I get on my screen is 3 partial desktops (the touch screen works which is great) If I plug my Pi into my TV I get fullscreen 1080, plug in Manga 2 in and I get as I said, 3 desktops.. Any help would be greatly appreciate. Works fine with my Windows 10 laptop but I want the Pi in my Peli case

![images/8b5e2d8e544367ae71bd7831ec39afd7.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/8b5e2d8e544367ae71bd7831ec39afd7.jpeg)



**Andy Williams**

---
---
**Claudio Prezzi** *July 10, 2018 09:59*

I got the same 3 partial desktops before I changed the config.

The code I provided does only work if your screen has the same EDID "IAG-MangaScreen2". You can check your EDID with the command "tvservice -n".


---
**James Armstrong** *August 08, 2018 01:28*

**+Claudio Prezzi** I have that ID, what is the code (I will continue to look in the meantime)




---
*Imported from [Google+](https://plus.google.com/103068870789972010594/posts/gNT65Sa5KsV) &mdash; content and formatting may not be reliable*
