---
layout: post
title: "I can't seem to run PID autotune with other parameters than the standard, 3 loops to 200C"
date: November 03, 2016 19:00
category: "Discussion"
author: Leo Södergren
---
I can't seem to run PID autotune with other parameters than the standard, 3 loops to 200C. 





**Leo Södergren**

---
---
**Elias Bakken** *November 03, 2016 19:28*

What is the command you are trying?


---
**Leo Södergren** *November 03, 2016 19:30*

M303, I've tried with only "C" defined and once with "E", "S" and "C" defined


---
*Imported from [Google+](https://plus.google.com/107257105792966201860/posts/EQJs7ufR5tg) &mdash; content and formatting may not be reliable*
