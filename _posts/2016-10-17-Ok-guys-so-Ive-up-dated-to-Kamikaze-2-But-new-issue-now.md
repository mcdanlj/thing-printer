---
layout: post
title: "Ok guys so Ive up dated to Kamikaze 2, But new issue now"
date: October 17, 2016 02:10
category: "Discussion"
author: Edwardo Garabito
---
Ok guys so Ive up dated to Kamikaze 2, But new issue now. None of the setting used to get my printer working before work now. 



Also Local.cfg does something weird. No matter profile i choose, (rostock max) Local.cfg fills its self with what looks like default settings, which cause redeem too overwrite my setting.   



Basically If i do a fresh install, there should be nothing in the Local.cfg.. right? 



After choosing my profile, restarting redeem, and then saving. Local.cfg gets filled with data that doesn't match any of my profiles. and if i change anything in the local.cfg file and save. It gets over written back to its original state.



I think this is why none of my settings are working now. 



on another note, when i restart redeem, shouldn't i be discounted and have to reconnect again? Because thats not what is happening. After restarting and saving. I manually disconnect and reconnect. not sure if im doing things wrong.    





**Edwardo Garabito**

---
---
**tizzledawg** *April 28, 2017 07:31*

I am also getting this. Hitting Restart Redeem doesn't kill the steppers or disconnect, im pretty sure Redeem isn't restarting - I have to manually remove power to get a config to take.


---
*Imported from [Google+](https://plus.google.com/115118943136595777855/posts/9PCxAgCoU8p) &mdash; content and formatting may not be reliable*
