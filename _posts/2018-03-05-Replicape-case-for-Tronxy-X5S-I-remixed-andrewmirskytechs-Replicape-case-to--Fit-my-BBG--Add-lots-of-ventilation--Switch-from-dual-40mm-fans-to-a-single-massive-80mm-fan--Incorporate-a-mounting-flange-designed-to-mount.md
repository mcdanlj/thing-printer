---
layout: post
title: "Replicape case for Tronxy X5S I remixed andrewmirskytech's Replicape case to:  Fit my BBG  Add lots of ventilation  Switch from dual 40mm fans to a single massive 80mm fan  Incorporate a mounting flange designed to mount"
date: March 05, 2018 11:52
category: "Discussion"
author: Michael K Johnson
---
<b>Replicape case for Tronxy X5S</b>



I remixed andrewmirskytech's Replicape case to:

⌷ Fit my BBG

⌷ Add lots of ventilation

⌷ Switch from dual 40mm fans to a single massive 80mm fan

⌷ Incorporate a mounting flange designed to mount on the Tronxy X5S in the same location as the original electronics



[https://github.com/johnsonm/Replicape_Case](https://github.com/johnsonm/Replicape_Case)





**Michael K Johnson**

---
---
**William Burton** *March 05, 2018 14:31*

Cool. I use that case for my tronxy x5s as well. Been meaning to reprint one with some mounting brackets. Now I guess I have no excuse. 



Question what amps did you end up using for your motors and what microsteping.  



I have kept mine between.5 and.7 mostly and either 3 or 4 microstepping


---
**Michael K Johnson** *March 05, 2018 17:34*

I haven't hooked it up yet. I was thinking about 1A but haven't thought it through yet. I put heatsinks on, 18B20 to measure, and big fan in place to support as high a current as I end up needing to use in practice. Looking forward to finishing first phase of moving off the provided control board soon... :)


---
**Michael K Johnson** *March 05, 2018 17:38*

Oh, and I realized maybe I will need washers because I don't have 5 or 6mm long screws to mount it. I should have thought of that in the model. :(


---
**William Burton** *March 05, 2018 19:35*

**+Michael K Johnson**. When you get it hooked up let me know. I have had my tronxy x5s running on replicape for a couple months now.  I changed out the hotend for an e3d and add a 24v power supply. That helped out greatly on the hot bed.  It's like my favorite 3d printer now.


---
**Michael K Johnson** *March 05, 2018 21:27*

I have 24V to the bed through spare MOSFET board for now. Was trying to go to dual extrusion with 24V heaters and running controller board at 24V but putting that off pending other changes first.


---
**Michael K Johnson** *March 06, 2018 00:14*

This case still needs some work.



For the bottom: The USB cutout isn't aligned perfectly. I don't understand how the original case worked for the power connectors; I had to slice away almost 2mm of the bottom case to make it fit. I'm not sure I put the flange where I want it; I have the power coming out the front and ethernet out the back, and that's not obviously the best solution. I might put the larger mounting flanges all around to have the option to explore and see what works best. (I can add thickness around the mounting holes while I'm at it.)



For the top: The 80mm fan didn't mount properly; I still don't have the right offset for the screws.



<i>sigh</i>


---
**Michael K Johnson** *March 06, 2018 00:44*

Drilling the holes out with a 3/32" bit made it <i>just barely</i> possible for me to use 3mm screws, with some difficulty.


---
**Michael K Johnson** *March 06, 2018 00:54*

I made all the changes I know about. I do not expect to have a chance to do test prints for at least a few days, but I pushed my updates to github so if you want to try them out and give me feedback, be my guest.


---
**Michael K Johnson** *March 06, 2018 01:08*

I begin to wonder if it would have been easier to redesign it from scratch... ☺


---
**Michael K Johnson** *March 07, 2018 03:41*

[forum.freecadweb.org - How to use the "Measure Distance" tool effectively? - FreeCAD Forum](https://forum.freecadweb.org/viewtopic.php?t=1956) shows me how to use FreeCAD to measure from STL. Part workbench, Part → Create Shape from Mesh, and now the measure tool is actually useful. I may or may not use that info to redesign from scratch. ☺


---
**Michael K Johnson** *March 08, 2018 01:39*

You can't remove the beaglebone from the case without prying the replicape off the beaglebone, but there's not enough room to comfortably hold the replicape to pull it out, so its quite tricky to remove. It also doesn't give any access to the microSD card slot.



I'm thinking that clips instead of screws would be a lot more convenient, at least for holding the beaglebone in place. That would make it easy to remove, giving sufficiently convenient access to the microSD card slot.


---
**Michael K Johnson** *March 10, 2018 03:45*

[https://github.com/johnsonm/Replicase](https://github.com/johnsonm/Replicase)


---
**Michael K Johnson** *March 10, 2018 20:12*

**+William Burton** it looks like the board that Tronxy shipped was set to 1V on all stepper driver trim pots, which doesn't help much because I don't know the sense resistor value on that board. With that setting, I got occasional layer shifts from skipped X/Y steps (say that six times fast), so I trimmed them up to 1.2V and am testing that now. The steppers are barely warm to the touch so far, so I don't think the current is very high. The Y gantry is rather stiff on my unit; maybe switching to openbuilds eccentric spacers with custom gantry plate would help? The acrylic gantry plates are clearly not a long term part anyway!



I'm doing the first test print of my designed from scratch "Replicase" replacement. I'm a little over 2mm along so it will be hours before I know what I will need to fix for the next iteration.


---
**Michael K Johnson** *March 14, 2018 22:26*

**+William Burton** [github.com - johnsonm/Replicase](https://github.com/johnsonm/Replicase) is ready to print, with mounting brackets! Though you'll have to change the fan size if you don't have a monster 80mm fan like me! ☺


---
*Imported from [Google+](https://plus.google.com/+MichaelKJohnson/posts/PpgyM4UsdPD) &mdash; content and formatting may not be reliable*
