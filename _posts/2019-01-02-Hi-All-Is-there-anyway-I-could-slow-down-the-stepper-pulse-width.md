---
layout: post
title: "Hi All, Is there anyway I could slow down the stepper pulse width?"
date: January 02, 2019 11:00
category: "Discussion"
author: CNC Beaker
---
Hi All,



Is there anyway I could slow down the stepper pulse width?



I have external stepper drivers and my X and Y need a on time of about 1-5uS and the extruder needs 8uS. 

Currently its running at 300nS.



Can I add anything to my local.cfg?





Cheers





**CNC Beaker**

---
---
**Jon Charnas** *January 24, 2019 12:44*

I'm not sure... I think you'd need to modify the PRU firmware code, or at least the PCA9685 frequency for this.


---
*Imported from [Google+](https://plus.google.com/111223679393405421993/posts/SJvS8zaV7mC) &mdash; content and formatting may not be reliable*
