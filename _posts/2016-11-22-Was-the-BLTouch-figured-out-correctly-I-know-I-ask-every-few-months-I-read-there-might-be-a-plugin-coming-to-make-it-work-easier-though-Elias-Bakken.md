---
layout: post
title: "Was the BLTouch figured out correctly? (I know I ask every few months) I read there might be a plugin coming to make it work easier though Elias Bakken ;"
date: November 22, 2016 02:36
category: "Discussion"
author: Philip Acierto
---
Was the BLTouch figured out correctly? (I know I ask every few months) I read there might be a plugin coming to make it work easier though **+Elias Bakken**; I would love to switch to it if getting it to work is possible now.





**Philip Acierto**

---
---
**Veridico Cholo** *December 05, 2016 14:39*

I added a BL touch a few weeks ago to another printer with a RUMBA board and it works great.



It is really a popular device a it would be great to get it working with Replicape and Redeem


---
**Furax Gromov** *December 21, 2016 16:58*

I started the tests with BLTouch. To drive it correctly I put these values:



[Endstops]

end_stop_delay_cycles = 500



[Servos]

servo_0_enable = True

servo_0_angle_init = 90

servo_0_angle_min =0

servo_0_angle_max = 180

servo_0_pulse_min = 0.0005

servo_0_pulse_max = 0.0025





M280 P0 S10 F5000 R ; Pin Down

M280 P0 S90 F5000 R ; Pin Up


---
**Philip Acierto** *December 22, 2016 04:02*

that is great info, I don't have a touch atm but if anyone else chimes in saying it works for them then I'll assume it is safe to go buy one and add it to my machine.



Appreciate it.


---
**Dan Cook** *August 20, 2017 16:18*

Furax Gromov, you are the man! I just installed a BLTouch and your configuration works perfectly! I'm going to add a How-To to our wiki, and I'll be sure to mention you!


---
*Imported from [Google+](https://plus.google.com/111974602640488998735/posts/UzDKcTTkaoi) &mdash; content and formatting may not be reliable*
