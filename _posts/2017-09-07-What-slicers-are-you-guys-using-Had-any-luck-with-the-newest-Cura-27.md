---
layout: post
title: "What slicers are you guys using? Had any luck with the newest Cura 2.7?"
date: September 07, 2017 13:46
category: "Discussion"
author: Tim Curtis
---
What slicers are you guys using? Had any luck with the newest Cura 2.7?





**Tim Curtis**

---
---
**Daniel Kusz** *September 07, 2017 13:54*

Yes, I used Cura 2.7 one time with simple print. Any troubles?


---
**Tim Curtis** *September 07, 2017 14:05*

I've been chasing z scars. Z bulges   and blobs of extra material. I just switched to a E3D aero extruder set up. This helped some but I still have issues. I'm thinking it's the newer cura slicer. I used to get great looking prints with my ramps 1.4 and old cura setup. I want to get the new cura dialed in because of the new features. Do our have any profiles that I can try to see to check against mine? 


---
**Daniel Kusz** *September 07, 2017 14:13*

Ok, I can help you and send you my profiles that was OK on Cura 2.6. I need to print something bigger to check are there any issues on 2.7. Last print was to fiat. Send me your email adress.


---
**Tim Curtis** *September 07, 2017 14:16*

timcurtis67@gmail.com

Thank you!


---
**Tim Curtis** *September 07, 2017 14:19*

I was using Cura 2.6 as well but with the same results. I am printing Carbon PETG with a .6 hardened nozzle. .3 layer 1.2 wall.



The parts come out great except for the random Z scars.


---
**Daniel Kusz** *September 07, 2017 14:37*

Here is my example from 2.6 with PLA. Do you have bigger scars\end points? 

![images/1a73441828fc0030572c8fcaf031084b.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/1a73441828fc0030572c8fcaf031084b.jpeg)


---
**Tim Curtis** *September 07, 2017 14:51*

Daniel,

I'll take a look when I get home. I'm at work right now and the link you

sent me is firewall blocked.

I'll send you a picture.



Thank you for any help you have. I'm trying to narrow this down to the

slicer (settings) or my replicape. I can't think it is the replicape but it

is one of the things I replaced in my printer. I have a duet wifi board for

my larger printer I am building. I hate to give up on this replicape board

so I am hoping I can get this thing dialed in.



Tim


---
**Daniel Kusz** *September 07, 2017 15:01*

I sent you my 0.2 profile on you email.


---
**Tim Curtis** *September 07, 2017 15:03*

Got it, Thank you. I'll compare it to what I was using.


---
**Daniel Kusz** *September 08, 2017 13:45*

Yestetday I have printed Yoda bust on Cura 2.7. I have blobs too. Is your printed stoped for a moment when causing blobs? My print was running at night and I didn't observe it. 

![images/6d5ed3d9ae5e70ce3a9d4293907c7117.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/6d5ed3d9ae5e70ce3a9d4293907c7117.jpeg)


---
**Tim Curtis** *September 11, 2017 11:06*

That is the same look I get on my parts too. I haven't observed any pauses while printing but  I don't watch the printer running enough. My prints run about 6 hours at a time. I'm wondering if some kind of pressure advance function would eliminate this issue. Does replicape/redeem have pressure advance settings? 


---
**Tim Curtis** *September 11, 2017 11:10*

I did change my retracts from 1mm to 2mm and increased the speed of retracts up to 45mms. This didn't change much.



Also I can say without a doubt that the blobs are not consistent on the prints. I have ran 45 prints thus far (I have to print 100 total) and no 2 parts are alike as far as blobs and even depressions (holes).... Bug in the software?



I do have a Duetwifi I could try to see if it makes a difference but I hate to tear down until I get through this print job. 


---
**Jon Charnas** *September 15, 2017 10:59*

Just as a follow-up, in case anyone is wondering: the slowdown issues tends to happen more on complex paths with very short G1 segments.  The cause of this is Octoprint's latency between receiving the redeem "ok" and sending the next G1. The whole development team is aware of the problem and is actively looking for solutions.



It is worth noting, however, that Redeem itself will not cause blobs if you simply read the Gcode file directly into Redeem's input (I won't document how to do that here because it's really not advisable, this was done purely for testing purposes). So we know that the issue comes from a layer above Redeem, and we are looking at alternatives, including, potentially, an alternative to OctoPrint if that should turn out to produce the best outcome for our users.


---
**Alex Carlson** *September 17, 2017 21:23*

**+Tim Curtis** Wiley is working on a pressure advance algorithm at the moment, it hasn't been pushed to the develop branch of redeem yet due to a significant bug still being present.


---
*Imported from [Google+](https://plus.google.com/106115959585163180149/posts/2C4TfAdbXBh) &mdash; content and formatting may not be reliable*
