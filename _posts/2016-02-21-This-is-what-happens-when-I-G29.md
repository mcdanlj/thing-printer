---
layout: post
title: "This is what happens when I G29"
date: February 21, 2016 06:11
category: "Discussion"
author: Philip Acierto
---
This is what happens when I G29. Movement works in all directions now, I rewired to have both z axis on same stepper driver.



How do I adjust the speed the movement is for probing?



Why does the x axis move when probing instead of the Z?


**Video content missing for image https://lh3.googleusercontent.com/-8-Tv3zcJzQ8/VslVLakyvFI/AAAAAAAAC8E/kpS_h1ZI5hw/s0/VID_20160220_213429.mp4.gif**
![images/e7e599da4a137e50232bf4955fb2f2dc.gif](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/e7e599da4a137e50232bf4955fb2f2dc.gif)



**Philip Acierto**

---
---
**Elias Bakken** *February 21, 2016 12:49*

Ah, that might be a bug actually. It thinks it's a delta when probing so it switches to Cartesian coordinate system and travels the same amount in x,y,z which on a delta is only z. 

Do all axes move when probing or just x and z?


---
**Christian Herget** *February 21, 2016 15:56*

For me all axis are moving and I always get more or less the same offset for all points, however I definitely messed up my leveling by placing a aluminum plate at one of the probe points.

That's wat I see on the console:

Recv: Found Z probe height 0.009 at (X, Y) = (10.0, 20.0)

Recv: Found Z probe height 0.009 at (X, Y) = (10.0, 160.0)

Recv: End stop Z2 hit!

Recv: Found Z probe height 0.009 at (X, Y) = (170.0, 100.0)



Thanks,

Christian


---
**Christian Herget** *February 21, 2016 17:24*

**+Elias Bakken** You were right, the file PathPlanner.py needs to be patched :)

Line 298 and 327 have to be changed from:

end   = (-z_dist, -z_dist, -z_dist, 0.0, 0.0, 0.0, 0.0, 0.0)

end   = (z_dist, z_dist, z_dist, 0.0, 0.0, 0.0, 0.0, 0.0)

to:

end   = (0.0, 0.0, -z_dist, 0.0, 0.0, 0.0, 0.0, 0.0)

end   = (0.0, 0.0, z_dist, 0.0, 0.0, 0.0, 0.0, 0.0)



However, this is only true for Cartesian, not sure if this will work with Core XY. 



Now auto bed leveling seems to work for me, but I'm still testing.﻿



Edit: It works, here is my G29 macro for a Prusa I3 with an inductive probe as a reference in case somebody is interested



[Macros]

G29 = 

    M561                ; Reset the bed level matrix

    M558 P3             ; Set probe type to Servo with switch

    

    M557 P0 X10  Y20    ; Set probe point 0

    M557 P1 X80  Y20    ; Set probe point 1

    M557 P2 X170 Y20    ; Set probe point 2

    M557 P3 X170 Y80    ; Set probe point 3

    M557 P4 X80  Y80    ; Set probe point 4

    M557 P5 X10  Y80    ; Set probe point 5

    M557 P6 X10  Y160   ; Set probe point 5

    M557 P7 X80  Y160   ; Set probe point 7

    M557 P8 X170 Y160   ; Set probe point 8



    M574 Z2 z_neg       ; Enable endstop Z2



    G28 X0 Y0           ; Home X Y



    G28 Z0              ; Home Z

    G0  Z8              ; Move Z up to allow space for probe

    G92 Z0              ; Reset Z height to 0



    G30 P0 S            ; Probe point 0

    G0 Z0               ; Move the Z up



    G30 P1 S            ; Probe point 1

    G0 Z0               ; Move the Z up



    G30 P2 S            ; Probe point 2

    G0 Z0               ; Move the Z up



    G30 P3 S            ; Probe point 3

    G0 Z0               ; Move the Z up



    G30 P4 S            ; Probe point 4

    G0 Z0               ; Move the Z up



    G30 P5 S            ; Probe point 5

    G0 Z0               ; Move the Z up



    G30 P6 S            ; Probe point 6

    G0 Z0               ; Move the Z up



    G30 P7 S            ; Probe point 7

    G0 Z0               ; Move the Z up



    G30 P8 S            ; Probe point 8

    G0 Z0               ; Move the Z up

    

    G28 X0 Y0           ; Home X Y

    

    G28 Z0              ; Home Z



    M574 Z2             ; Disable endstop Z2

    G92 Z0.3            ; Compensate probe offset

    G0  Z0              ; Move nozzle to the lowest position




---
**Elias Bakken** *February 21, 2016 17:25*

Yeah, just check what the coordinate system is, should be examples of that elsewhere in that file.


---
**Philip Acierto** *February 21, 2016 19:11*

Only Z needs to move when probing corexy so I see no problem with the change.



Also, is there a variable that handles the speed of the movement between probe locations? Nothing in the config seems to affect it, and the speed that it is moving, as seen in the video, is driving me nuts.﻿


---
**Philip Acierto** *February 22, 2016 19:34*

Is this a file that will be patched soon and rebuilt included in the next redeem release, or do I need to do this to my local build and maintain it myself?



Is there a link to how to rebuild redeem? Or is it simple as editing the file and restarting redeem?




---
**Christian Herget** *February 22, 2016 19:42*

**+Philip Acierto**​ I edit  the file with nano directly in the BBB, no compiling needed as  it is Python. Unfortunatly I cant remember the path, but you can do 

find -iname "PathPlanner.py"

to search for it.﻿


---
**Philip Acierto** *February 22, 2016 19:45*

neat, no compiling? Well that will make things easier then! That should at least get me up and running than, soon as I can figure out the Z probing issue on my other post!



Thanks for helping to solve this issue!


---
**Daryl Bond** *February 23, 2016 00:15*

Feel free to fork the Redeem repository ( [https://bitbucket.org/intelligentagent/redeem](https://bitbucket.org/intelligentagent/redeem) ) and contribute a fix! I was going to do something about it but I don't have a Cartesian bot to test it on. 



Try using:



[http://pastebin.com/raw/saPpU6Nv](http://pastebin.com/raw/saPpU6Nv)



This just does what was posted above but does a check for the different axis configurations. I am assuming that CoreXY and H-bot would be the same as Cartesian as the Z-axis is all that needs to move when probing?


---
**Philip Acierto** *February 23, 2016 00:38*

That would be correct. X and Y is where all the wonky magic happens for movement on the horizontal plane, but Z probing is vertical movement for the standard Cartesian systems, so the same across the board.



I haven't had time in the past few days to look at my code or test, but once I get another evening to sit down with it, I would be glad to test the proposed fix out on my CoreXY.



I was just hoping to have the homing G28 for my Z fixed first (separate thread), since that is used as part of the G29.



Once G28 is resolved I can hopefully confirm this fix for G29.


---
**Tim Curtis** *February 24, 2016 16:25*

I was trying to find my directories/files for redeem using the find command as Christian Herget directed. find -iname "PathPlanner.py" but I don't get any results. I am logged in to my BBB with putty. I logged in under root. Any idea what I am doing wrong?


---
**Elias Bakken** *February 24, 2016 16:54*

If you installed using apt, it should be in /usr/lib/python2.7/dist-packages/redeem (or something there abouts) :)


---
**Tim Curtis** *February 24, 2016 17:20*

I installed everything with the kamakazi image (Flashed). SO where would I find all the files that redeem operates from? I want to try making some tweeks. Even though I'll probably screw things up,. :)


---
**Elias Bakken** *February 24, 2016 17:21*

You should find them there!


---
**Tim Curtis** *February 24, 2016 19:19*

There Where? on my kamakazi flash disk? or on my BBB?


---
**Elias Bakken** *February 24, 2016 20:21*

Well, they are the same, rsynced, so after flashing you will find it in your BBB!


---
**Christian Herget** *February 26, 2016 21:09*

**+Tim Curtis** you have to run

<i>find -iname "PathPlanner.py"</i>

on your BBB in the root folder

<i>cd /</i>

this should give you:

<i>./usr/lib/python2.7/dist-packages/redeem/PathPlanner.py</i>

Which is the path to that file.


---
**Tim Curtis** *February 29, 2016 12:15*

I haven't looked again for it yet, I was away this past weekend. I'll check tonight. I did try that command before but I may not have been in the root folder. I'll give that a try tonight. Thanks for the info.


---
**Christian Herget** *February 29, 2016 12:20*

**+Tim Curtis** , **+Elias Bakken** included my patch for the file into the master branch of redeem so it should be fixed in upcoming packages ([https://bitbucket.org/intelligentagent/redeem/src/32e265e724151bae60637496fa4cc0f1662bb39e/redeem/PathPlanner.py?at=master](https://bitbucket.org/intelligentagent/redeem/src/32e265e724151bae60637496fa4cc0f1662bb39e/redeem/PathPlanner.py?at=master)).

Yes, you need to execute the <i>find</i> from the root directory.


---
**Tim Curtis** *February 29, 2016 14:50*

Will I be able to access those directories being logged in as "root"? Or do I have to create a user and login in that way?


---
**Daryl Bond** *March 01, 2016 00:28*

You can access <b>everything</b> when logged in as root!


---
*Imported from [Google+](https://plus.google.com/111974602640488998735/posts/8hZ2ADjdo6z) &mdash; content and formatting may not be reliable*
