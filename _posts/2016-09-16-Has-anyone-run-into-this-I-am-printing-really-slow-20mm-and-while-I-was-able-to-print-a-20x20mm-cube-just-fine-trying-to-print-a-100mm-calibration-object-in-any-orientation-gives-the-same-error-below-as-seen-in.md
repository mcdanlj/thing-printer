---
layout: post
title: "Has anyone run into this? I am printing really slow, 20mm, and while I was able to print a 20x20mm cube just fine, trying to print a 100mm calibration object, in any orientation, gives the same error below (as seen in"
date: September 16, 2016 00:22
category: "Discussion"
author: Philip Acierto
---
Has anyone run into this?



I am printing really slow, 20mm, and while I was able to print a 20x20mm cube just fine, trying to print a 100mm calibration object, in any orientation, gives the same error below (as seen in octoprint terminal):







"Communication timeout while printing, trying to trigger response from printer. Configure long running commands or increase communication timeout if that happens regularly on specific commands or long moves."







I downloaded a plugin that was supposed to send OK rec to by pass this, I turned up the time out to 60 seconds from the stock 30. I am not sure what to do here. Any ideas? Am I even right thinking this is a printing too slow thing?





**Philip Acierto**

---
---
**James Armstrong** *September 16, 2016 02:35*

I think other have had this problem and Elias just posted that they think extending the stepper watchdog timeout fixes the problem. If you are not of the slack channel you should sign up. 


---
**Philip Acierto** *September 16, 2016 03:01*

I've hopped on the freenode IRC a few times to get my printer this far, not sure I've heard about the slack channel, if that is different.



Was a walk through or patch in the works?


---
**James Armstrong** *September 16, 2016 03:02*

I think it shows at the top of the freenode Welcome and on the wiki. 


---
**James Armstrong** *September 16, 2016 03:03*

[thing-printer.com - Thing-Printer.com ‹ SlackBot Invitation](http://www.thing-printer.com/wp-login.php?action=slack-invitation)


---
**James Armstrong** *September 16, 2016 03:42*

**+Philip Acierto** I think one of the issues is a simple configuration change from what I remember. 


---
**Philip Acierto** *September 16, 2016 04:06*

alright, I'll dig around for it. I didn't see it in the config but I would hope its something simple to change like that.


---
**Elias Bakken** *September 16, 2016 12:34*

Yes, increase the timeout in OctoPrint as well as the stepper watchdog timeout in Redeem.


---
**Philip Acierto** *September 18, 2016 20:26*

**+Elias Bakken** I replied to the slack channel as well, but:



adjusting the watchdog timeout and octoprint doesn't seem to be fixing my slow move issues.  I am fairly certain the issue is still timeout related because if I speed up my print speed, it no longer happens (though a longer movement input will still trip it)



I have tried turning up the timeout_seconds = 500, put it up to 1000, and 5000. I've changed Communication timeout to 500, 5000, and nothing seems to change. The print head moves, and completes the last movement sucesffuly, but never return Recv: ok, so the next move is never issues.







Send: N867 G1 X43.440 Y87.563 E184.9654*80

Recv: ok

Send: N868 G1 X83.533 Y127.656 E186.8513*107

Recv: ok

Recv: ok

Send: N869 M105*32

Send: N870 G1 X82.967 Y127.656 E186.8701*111

Recv: ok T:187.0/190.0 T1:0.0/0.0 T0:187.0/190.0 B:0.0/0.0 @:255.0

Send: N871 G1 X43.440 Y88.129 E188.7293*95

Communication timeout while printing, trying to trigger response from printer. Configure long running commands or increase communication timeout if that happens regularly on specific commands or long moves.







Any other ideas?





![images/48758004139be02c0012807e1b63e79b.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/48758004139be02c0012807e1b63e79b.png)


---
**Philip Acierto** *September 18, 2016 20:26*





![images/1b33e121a082f00c475b93b55a06705c.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/1b33e121a082f00c475b93b55a06705c.png)


---
**Philip Acierto** *September 18, 2016 21:17*

Here is a video and the terminal output (with it hanging at the end for about 5 mins before displaying the timeout error)



Send: N1068 G1 X113.223 Y124.536 E8.9527*107

Recv: ok

Send: N1069 G1 X113.788 Y124.536 E8.9715*109

Recv: ok

Send: N1070 G1 X116.908 Y127.656 E9.1183*96

Recv: ok

Send: N1071 G1 X117.474 Y127.656 E9.1371*105

Recv: ok

Send: N1072 G1 X114.354 Y124.536 E9.2839*110

Recv: ok

Send: N1073 G1 X114.920 Y124.536 E9.3027*96

Recv: ok

Send: N1074 G1 X118.040 Y127.656 E9.4494*105

Recv: ok

Send: N1075 G1 X118.606 Y127.656 E9.4682*105

Recv: ok

Send: N1076 G1 X115.486 Y124.536 E9.6150*97

Recv: ok

Send: N1077 G1 X116.051 Y124.536 E9.6338*97

Recv: ok

Send: N1078 G1 X119.171 Y127.656 E9.7806*99

Recv: ok

Send: N1079 G1 X119.737 Y127.656 E9.7994*108

Recv: ok

Send: N1080 G1 X116.479 Y124.399 E9.9526*98

Recv: ok

Send: N1081 G1 X116.843 Y124.197 E9.9665*110

Communication timeout while printing, trying to trigger response from printer. Configure long running commands or increase communication timeout if that happens regularly on specific commands or long moves.

![images/7e5bec906f93a3ef0a8839ed67464c5b](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/7e5bec906f93a3ef0a8839ed67464c5b)


---
*Imported from [Google+](https://plus.google.com/111974602640488998735/posts/PHgizZggpG8) &mdash; content and formatting may not be reliable*
