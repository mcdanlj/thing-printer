---
layout: post
title: "A brand new Replicape revision folks, and I'm looking for some muscle to beta test the next round of boards in a few weeks :)"
date: May 08, 2015 15:02
category: "Discussion"
author: Elias Bakken
---
A brand new Replicape revision folks, and I'm looking for some muscle to beta test the next round of boards in a few weeks :)





**Elias Bakken**

---
---
**James Armstrong** *May 08, 2015 15:09*

I'm game. Let me know the details or what I need to do to get on the list.


---
**James Armstrong** *May 08, 2015 15:12*

Are the modes of the TMC2100 going to be configurable? I have heard that they are not suited for 3d printing with some settings (something about not having enough current drive at some speeds the printers hit).


---
**Elias Bakken** *May 08, 2015 15:16*

Cool! The stealthChop mode is not for 3D-printing, but the SpreadCycle should work well!


---
**Clarence Lee** *May 12, 2015 06:04*

Is there an option or jumper to switch to the stealthChop mode? Under some condition, it's still usable on kossel mini motion anyway.


---
**Elias Bakken** *May 12, 2015 07:19*

**+Clarence Lee**​ yes, it is available.


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/TEh2bfLf8FQ) &mdash; content and formatting may not be reliable*
