---
layout: post
title: "The redeem info has been updated. Still a few quirks, but much better than before"
date: January 28, 2014 19:46
category: "Discussion"
author: Elias Bakken
---
The redeem info has been updated. Still a few quirks, but much better than before. I've also back-ported some of the code from A4 to A3 since there are some people with A3 capes: [http://wiki.thing-printer.com/index.php?title=Redeem](http://wiki.thing-printer.com/index.php?title=Redeem)





**Elias Bakken**

---


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/TvKZXr6P2A5) &mdash; content and formatting may not be reliable*
