---
layout: post
title: "Okay I am a little bit confused with delta calibration?"
date: August 19, 2016 08:51
category: "Discussion"
author: Mathias Giacomuzzi
---
Okay I am a little bit confused with delta calibration? So I followed the guide on this page.



>>[http://minow.blogspot.co.at/index.html#6142825731059141013](http://minow.blogspot.co.at/index.html#6142825731059141013)



>>Now run script A in RepetierHost / click button A in Pronterface.  The >>carriage should move up to home, then down to a point in front of tower A.  >>The proper height should be just above the print surface, with clearance >>space for a single sheet of paper to slide between.  Adjust the end-stop >>screw only for tower A if necessary, then repeat the test by running the >>script or clicking the button.  Repeat until the paper slides between the print >>surface with only slight drag.



>>With tower A adjusted, move on to tower B, and then to tower C.  When >>tower C is adjusted, you should be able to go back and successfully repeat >>the test on tower A without having to readjust.



I tried this with redeem but I have some strange behavior. I do not adjust the endstop screw I thought instead of that I use the M206 command to modifie the x, y, and z offset.



But after I calibrate tower A then tower B and then tower C... I switch back to tower A and then I need a readjust ... so its a never ending story I always need to readjust something.



Is M206 not for that usage?



Or does someone know whats going wrong here?



thanks













**Mathias Giacomuzzi**

---


---
*Imported from [Google+](https://plus.google.com/104869072343043179738/posts/iwJoNvaySH2) &mdash; content and formatting may not be reliable*
