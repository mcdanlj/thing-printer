---
layout: post
title: "i have my printer moving fine on every axis using octoprint, I can home as well"
date: April 10, 2016 12:42
category: "Discussion"
author: Ken Runner
---
i have my printer moving fine on every axis using octoprint, I can home as well.



When I try to print it seems that every axis is trying to go to max so I have to shutdown the machine.  When I go into terminal and enter G0 X1 of G1 X1 I get the same result, every axis will hit max and I don't have endstops for the max so it will just hit the end of it's travel and try to continue.



Any suggestions?





**Ken Runner**

---
---
**Pieter Koorts** *April 10, 2016 12:45*

Have you tried setting a maximum on travel_(x,y,z) and on homing. This might limit travel distance but not sure if this affects repeated retries.



So if your rod travel is up to 200mm, try setting it to 180mm and then start working from there to correct it.


---
**Ken Runner** *April 10, 2016 14:07*

Adding in values for travel_x,y,z and offset_x,y,z seemed to fix it, Thanks!


---
*Imported from [Google+](https://plus.google.com/117074313801696052366/posts/98MhbB1ijHH) &mdash; content and formatting may not be reliable*
