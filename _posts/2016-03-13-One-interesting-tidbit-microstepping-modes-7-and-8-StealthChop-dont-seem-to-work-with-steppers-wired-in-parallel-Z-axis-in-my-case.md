---
layout: post
title: "One interesting tidbit - microstepping modes 7 and 8 (StealthChop) don't seem to work with steppers wired in parallel (Z axis in my case)"
date: March 13, 2016 16:36
category: "Discussion"
author: Ante Vukorepa
---
One interesting tidbit - microstepping modes 7 and 8 (StealthChop) don't seem to work with steppers wired in parallel (Z axis in my case).





**Ante Vukorepa**

---
---
**Elias Bakken** *March 13, 2016 16:37*

Can you use H as a slave driver? 




---
**Ante Vukorepa** *March 13, 2016 17:13*

Yup, but i need to do some rewiring first (stepper cabling hidden inside the makerslide alu extrusions the whole thing is built of).


---
**Marcos Scriven** *May 30, 2016 17:19*

First question - do you not mean in series? Second question, this does work for me, albeit slower. In what way doesn't it work for you?


---
**Ante Vukorepa** *May 30, 2016 17:35*

Parallel, not series. And they don't move, they just twitch.


---
**Ante Vukorepa** *May 30, 2016 17:35*

(regardless of the current)


---
**Marcos Scriven** *May 30, 2016 20:10*

Any particular reason you put them in parallel rather than series? The former requires double the current right?


---
**Ante Vukorepa** *May 30, 2016 20:54*

Yeah, but there's plenty of current. I don't remember the details anymore, but after some testing and back-of-the-envelope math, i think i concluded there's less artifacting due to microstepping in a parallel arrangement, than a series one.


---
*Imported from [Google+](https://plus.google.com/105486978634901788220/posts/Vr3S7QEodv4) &mdash; content and formatting may not be reliable*
