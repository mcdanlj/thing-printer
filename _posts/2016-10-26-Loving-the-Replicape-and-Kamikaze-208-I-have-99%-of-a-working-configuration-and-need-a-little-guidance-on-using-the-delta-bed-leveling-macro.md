---
layout: post
title: "Loving the Replicape and Kamikaze 2.0.8! I have 99% of a working configuration and need a little guidance on using the delta bed leveling macro"
date: October 26, 2016 22:54
category: "Discussion"
author: Jim Nofsinger
---
Loving the Replicape and Kamikaze 2.0.8!  I have 99% of a working configuration and need a little guidance on using the delta bed leveling macro.  



Currently using the example for G29 (below).  The printer goes through the motions and properly triggers the zprobe.  What it doesn't do for me is raise the probe between points and sets a smaller probe radius.  My probe hangs down too low to use 100% of radius.  How do I raise the probe between points?  is there away to set the bed probe radius?  



I attached my local.cfg it is for a Kickstarter Delta by Billy Zelsnack the Polygon 2060.  Printer is awesome btw ;)



g29 = 

	M557 P0 X0     Y100   Z25 ; Set probe points

	M557 P1 X86.6  Y50    Z25

	M557 P2 X86.6  Y-50   Z25

	M557 P3 X0     Y-100  Z25

	M557 P4 X-86.6 Y-50   Z25

	M557 P5 X-86.6 Y50    Z25

	M557 P6 X0     Y0     Z25

	G28                      ; Home steppers

	G30 P6 S

	G30 P6 S

	G30 P0 S

	G30 P0 S

	G30 P1 S

	G30 P1 S

	G30 P2 S

	G30 P2 S

	G30 P3 S

	G30 P3 S

	G30 P4 S

	G30 P4 S

	G30 P5 S

	G30 P5 S

	G30 P6 S

	G30 P6 S





**Jim Nofsinger**

---
---
**Elias Bakken** *October 27, 2016 00:15*

Hi! Love the enthusiasm! Did you try the "G29C" G-code? It is a special G-code to generate a G29 macro. Try "G29C?" to get a description of the alternatives for the command!


---
**Jim Nofsinger** *October 27, 2016 22:36*

doh!  didn't even process that command properly.... thanks!  



I ran the setup G29C and did get a better radius.  For some reason my printer thinks it is a slope though :(



[http://review.thing-printer.com/?key=RPA52HF3IHS6K1TL3AJ1](http://review.thing-printer.com/?key=RPA52HF3IHS6K1TL3AJ1)



in Smoothie i had 

Arm length is 270.

Arm radius is 145.75

I measured the Hez at 65.31



I put in my config

hez = 0.06531

l = 0.27

r = 0.14575



and it produces the sloped results, any suggestions on what could be off?  did I not convert the radius correctly? ie smoothie radius does not equal replicape radius?



here is what the G29C produced (when run it doesn't hit half the bed of course)



g29 = M561; (RFS) Reset bed matrix

	M557 P0 X+50.00 Y+0.00 Z30.0

	M557 P1 X+35.36 Y+35.36 Z30.0

	M557 P2 X+0.00 Y+50.00 Z30.0

	M557 P3 X-35.36 Y+35.36 Z30.0

	M557 P4 X-50.00 Y+0.00 Z30.0

	M557 P5 X-35.36 Y-35.36 Z30.0

	M557 P6 X-0.00 Y-50.00 Z30.0

	M557 P7 X+35.36 Y-35.36 Z30.0

	M557 P8 X+37.50 Y+0.00 Z30.0

	M557 P9 X+26.52 Y+26.52 Z30.0

	M557 P10 X+0.00 Y+37.50 Z30.0

	M557 P11 X-26.52 Y+26.52 Z30.0

	M557 P12 X-37.50 Y+0.00 Z30.0

	M557 P13 X-26.52 Y-26.52 Z30.0

	M557 P14 X-0.00 Y-37.50 Z30.0

	M557 P15 X+26.52 Y-26.52 Z30.0

	M557 P16 X+25.00 Y+0.00 Z30.0

	M557 P17 X+17.68 Y+17.68 Z30.0

	M557 P18 X+0.00 Y+25.00 Z30.0

	M557 P19 X-17.68 Y+17.68 Z30.0

	M557 P20 X-25.00 Y+0.00 Z30.0

	M557 P21 X-17.68 Y-17.68 Z30.0

	M557 P22 X-0.00 Y-25.00 Z30.0

	M557 P23 X+17.68 Y-17.68 Z30.0

	M557 P24 X+12.50 Y+0.00 Z30.0

	M557 P25 X+8.84 Y+8.84 Z30.0

	M557 P26 X+0.00 Y+12.50 Z30.0

	M557 P27 X-8.84 Y+8.84 Z30.0

	M557 P28 X-12.50 Y+0.00 Z30.0

	M557 P29 X-8.84 Y-8.84 Z30.0

	M557 P30 X-0.00 Y-12.50 Z30.0

	M557 P31 X+8.84 Y-8.84 Z30.0

	M557 P32 X+0.00 Y+0.00 Z30.0

	    G32 ; Undock probe

	    G28 ; Home steppers

	    G30 P0 S F3000; Probe point 0

	    G30 P1 S F3000; Probe point 1

	    G30 P2 S F3000; Probe point 2

	    G30 P3 S F3000; Probe point 3

	    G30 P4 S F3000; Probe point 4

	    G30 P5 S F3000; Probe point 5

	    G30 P6 S F3000; Probe point 6

	    G30 P7 S F3000; Probe point 7

	    G30 P8 S F3000; Probe point 8

	    G30 P9 S F3000; Probe point 9

	    G30 P10 S F3000; Probe point 10

	    G30 P11 S F3000; Probe point 11

	    G30 P12 S F3000; Probe point 12

	    G30 P13 S F3000; Probe point 13

	    G30 P14 S F3000; Probe point 14

	    G30 P15 S F3000; Probe point 15

	    G30 P16 S F3000; Probe point 16

	    G30 P17 S F3000; Probe point 17

	    G30 P18 S F3000; Probe point 18

	    G30 P19 S F3000; Probe point 19

	    G30 P20 S F3000; Probe point 20

	    G30 P21 S F3000; Probe point 21

	    G30 P22 S F3000; Probe point 22

	    G30 P23 S F3000; Probe point 23

	    G30 P24 S F3000; Probe point 24

	    G30 P25 S F3000; Probe point 25

	    G30 P26 S F3000; Probe point 26

	    G30 P27 S F3000; Probe point 27

	    G30 P28 S F3000; Probe point 28

	    G30 P29 S F3000; Probe point 29

	    G30 P30 S F3000; Probe point 30

	    G30 P31 S F3000; Probe point 31

	    G30 P32 S F3000; Probe point 32

	    G31 ; Dock probe

	    M561 U; (RFS) Update the matrix based on probe data

	    M561 S; Show the current matrix

	    M500; (RFS) Save data

	



Thanks!

Jim



[review.thing-printer.com - Tools Bed probing Config Key Store Wiki Review is a collection of ...](http://review.thing-printer.com/?key=RPA52HF3IHS6K1TL3AJ1)


---
**Jon Charnas** *November 12, 2016 08:40*

What I did is I ignored the hez factor and subtracted it from the Delta radius, works better, I think there's a bug left in that piece of code


---
*Imported from [Google+](https://plus.google.com/118269177478098269592/posts/bWpyUaBHCf8) &mdash; content and formatting may not be reliable*
