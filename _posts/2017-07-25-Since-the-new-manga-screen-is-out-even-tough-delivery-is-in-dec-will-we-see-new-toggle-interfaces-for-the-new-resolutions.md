---
layout: post
title: "Since the new manga screen is out (even tough delivery is in dec): will we see new toggle interfaces for the new resolutions?"
date: July 25, 2017 11:00
category: "Discussion"
author: Leo Södergren
---
Since the new manga screen is out (even tough delivery is in dec): will we see new toggle interfaces for the new resolutions?





**Leo Södergren**

---
---
**Leo Södergren** *July 25, 2017 11:00*

and maybe rotated interfaces?


---
**Jon Charnas** *July 25, 2017 11:18*

I believe there's already a full HD interface in the works on the develop branch, but I'm not sure what you mean by rotated interfaces?


---
**Leo Södergren** *July 25, 2017 11:27*

**+Jon Charnas**​ Ok will check it out. For portrait setups, instead of landscape


---
**Elias Bakken** *July 25, 2017 11:32*

You specify rotation in the config file, that works already! Yes, there is a 1080p interface that looks OK, just a few minor cosmetic things. I'll make a 720p version as well: )




---
**Daniel Kusz** *July 25, 2017 12:08*

What about visible stl's?


---
*Imported from [Google+](https://plus.google.com/107257105792966201860/posts/domtv6RRb9P) &mdash; content and formatting may not be reliable*
