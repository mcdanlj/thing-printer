---
layout: post
title: "Replicase  OpenSCAD model of click-together Replicape case I became frustrated with remixing Replicape_Case and started over with the BBB SRM and a micrometer and modeled a case that has lots of ventilation and is designed"
date: March 10, 2018 03:43
category: "Discussion"
author: Michael K Johnson
---
<b>Replicase</b> — OpenSCAD model of click-together Replicape case



I became frustrated with remixing Replicape_Case and started over with the BBB SRM and a micrometer and modeled a case that has lots of ventilation and is designed to click together. I haven't printed it yet. Looking for feedback on the design.



The code is ugly in places and the parameter names are awful, but that's what search and replace is for. I think the basic design will make me happier, though I expect to have to print it three or four times before I'm satisfied.



[https://github.com/johnsonm/Replicase](https://github.com/johnsonm/Replicase)





**Michael K Johnson**

---
---
**Michael K Johnson** *March 10, 2018 03:44*

![images/c5acdc8ba0e95ef94bf4c40af4b5a67f.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/c5acdc8ba0e95ef94bf4c40af4b5a67f.png)


---
**Dave Posey** *March 10, 2018 16:33*

Nicely done design.  I would love to see how it prints and works for you.


---
**Michael K Johnson** *March 10, 2018 16:38*

I added labels for all the Replicape connectors and fixed the clip length on the top of the case so it should actually clip together. Still haven't printed it.


---
**Michael K Johnson** *March 10, 2018 16:40*

**+Dave Posey** I'll definitely be following up when printed, either in this thread or another post. I'm sure there will be more tweaks after I print it!


---
**Michael K Johnson** *March 10, 2018 20:55*

Not yet 5mm of Z into the print, I realized that I was right that no plan survives first contact with the enemy. The mounting flanges opposite the origin were 4mm (two shell thicknesses) more narrow than I intended. Not a killer for this first print and I'm going to let it complete so that I can test-fit everything.


---
**Michael K Johnson** *March 11, 2018 05:14*

**+Dave Posey** First print complete, and found fixes and tweaks. TL;DR—fixes made and pushed, ready for next iteration. If you are adventurous, it's ready to print. Good chance that any additional fit problems can be addressed with knife and/or sandpaper.



Critical were fit problems. One design error was that the standoffs were simply misplaced by one shell thickness. Additionally, the clips on the standoffs were so thick that it was impossible to clip the board in at all, and the clips holding the shells together had a similar problem and two of the clips broke off the top half. The font was too narrow to print well horizontally, reducing legibility, and a few labels were a few mm away from what looks like optimal placement.



Overall none of these problems were difficult to deal with. This was a better first prototype than I expected for a design with this many constraints.


---
**Michael K Johnson** *March 12, 2018 10:55*

Ethernet jack doesn't quite fit and the clips being the full width of the case make them hard to insert (in the "addressed with knife") category, and the top clips aren't strong enough (hard to add material with a knife). Next on the list...


---
**Michael K Johnson** *March 13, 2018 11:09*

I thought I had dealt with everything for my overnight print, but no. I am making enough changes that I expect a few more prototypes before this is worth anyone else printing it.


---
**Michael K Johnson** *March 14, 2018 10:25*

Last print had only a problem with labels printing not quite like I meant, and I snapped a clip off again, so current files have the thickest supports I can print, and I updated the README with instructions to not break the clips. All the critical dimensions worked, though.


---
**Michael K Johnson** *March 14, 2018 21:49*

I'm declaring victory for now. It clipped together, the labels match, it all seems to work fine.


---
**Michael K Johnson** *March 14, 2018 21:50*

I had to print the bottom five times and the top six times before I was satisfied with the design.


---
*Imported from [Google+](https://plus.google.com/+MichaelKJohnson/posts/L5AiPhMR2vb) &mdash; content and formatting may not be reliable*
