---
layout: post
title: "Hello everyone! I am nearly complete on my configuration of the new Kamikaze 2.0.8 version"
date: October 21, 2016 17:32
category: "Discussion"
author: Jim Nofsinger
---
Hello everyone!



I am nearly complete on my configuration of the new Kamikaze 2.0.8 version.  Only outstanding item so far, is where do the traditional Z probe get plugged into?  I have a micro switch on the end of my delta nozzle.  





Thanks,

Jim



(looking forward to printing this weekend)





**Jim Nofsinger**

---
---
**Elias Bakken** *October 21, 2016 19:13*

If your probe requires 12V, put it in the Z2 connector. If not (microswiches usually don't) you can use which ever end stop input is free. You have to configure it in software anyways! 


---
*Imported from [Google+](https://plus.google.com/118269177478098269592/posts/VSMjAFqg8Qm) &mdash; content and formatting may not be reliable*
