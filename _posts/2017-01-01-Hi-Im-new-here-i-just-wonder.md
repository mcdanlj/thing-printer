---
layout: post
title: "Hi I'm new here. i just wonder"
date: January 01, 2017 12:25
category: "Discussion"
author: Per Arne Kristiansen
---
Hi I'm new here. 

i just wonder. how do you connect bltouch (auto leveling sensor) to Replicape 

and how to program it ?





**Per Arne Kristiansen**

---
---
**Andrew Dowling** *January 01, 2017 14:27*

Here you go. This isn't all my work, but it works.

edit config. 

(start config info)

[Servos]

 servo_0_enable = True

servo_0_channel = P9_14

servo_0_angle_init = 90

servo_0_angle_min = 0

servo_0_angle_max = 180

servo_0_pulse_min = 0.0006

servo_0_pulse_max = 0.0024



servo_1_enable = True

servo_1_channel = P9_16

servo_1_angle_init = 90

servo_1_angle_min = 0

servo_1_angle_max = 180

servo_1_pulse_min = 0.0006

servo_1_pulse_max = 0.0024



[Macros]



G29 = (yeah, you will have to custom make this probably. sample below)

M561                ; Reset the bed level matrix

    M558 P0             ; Set probe type to Servo with switch

    M557 P0 X10 Y20     ; Set probe point 0

    M557 P1 X10 Y180    ; Set probe point 1

    M557 P2 X180 Y100   ; Set probe point 2

    G28 X0 Y0           ; Home X Y



    G28 Z0              ; Home Z

    G0 Z12              ; Move Z up to allow space for probe

    G32                 ; Undock probe

    G92 Z0              ; Reset Z height to 0

    G30 P0 S            ; Probe point 0

    G0 Z0               ; Move the Z up

    G31                 ; Dock probe



    G28 Z0              ; Home Z

    G0 Z12              ; Move Z up to allow space for probe

    G32                 ; Undock probe

    G92 Z0              ; Reset Z height to 0

    G30 P1 S            ; Probe point 1

    G0 Z0               ; Move the Z up

    G31                 ; Dock probe



    G28 Z0              ; Home Z

    G0 Z12              ; Move Z up to allow space for probe

    G32                 ; Undock probe

    G92 Z0              ; Reset Z height to 0

    G30 P2 S            ; Probe point 2

    G0 Z0               ; Move the Z up

    G31                 ; Dock probe



    G28 X0 Y0           ; Home X Y



G32 =    

	M280 P0 S10

	G4 S1





G31 =    

	M280 P0 S90

	G4 S1

![images/4c9b7890a35ef3e013e76d27e246cfa7.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/4c9b7890a35ef3e013e76d27e246cfa7.png)


---
**Andrew Dowling** *January 01, 2017 14:32*

G28 Z does not act as expected with bltouch (at least with cartasian printers). I have mentioned the issue. Do not use G28 Z with cartasian unless your printer likes crashing the head into the bed. PM me for a workaround on slack. pr1mus285 username.


---
**Per Arne Kristiansen** *January 01, 2017 14:36*

**+Andrew Dowling** 

Hi thank you i havent recived the card neither the bltouch yet but I want to be prepared :) 


---
**Per Arne Kristiansen** *January 07, 2017 10:25*

When I connect it like this everything goes black..... 


---
**Andrew Dowling** *January 08, 2017 16:22*

Does it turn red with just the 3 pin connector connected?




---
**Per Arne Kristiansen** *January 08, 2017 17:40*

Yes 


---
**Andrew Dowling** *January 09, 2017 04:04*

and then when you connect the white and black and it just turns off?


---
*Imported from [Google+](https://plus.google.com/100860112688228391754/posts/8Ca2V7o8LGn) &mdash; content and formatting may not be reliable*
