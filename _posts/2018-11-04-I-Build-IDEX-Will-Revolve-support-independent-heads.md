---
layout: post
title: "I Build IDEX. Will Revolve support independent heads?"
date: November 04, 2018 18:33
category: "Discussion"
author: Pawel Misiak
---
I Build IDEX. Will Revolve support independent heads?





**Pawel Misiak**

---
---
**Jon Charnas** *November 04, 2018 18:44*

Revolve will support 6 onboard steppers + headers for 2 external steppers. Redeem supports addressing those axes already as far as I know. Whether or not you can make a macro that changes which stepper is used to move the X axis, I don't know, but I suspect that it should be feasible.


---
*Imported from [Google+](https://plus.google.com/117613851420139606503/posts/ieAuQbEwPE2) &mdash; content and formatting may not be reliable*
