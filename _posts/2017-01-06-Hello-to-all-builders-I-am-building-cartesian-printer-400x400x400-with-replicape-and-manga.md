---
layout: post
title: "Hello to all builders! I am building cartesian printer 400x400x400 with replicape and manga"
date: January 06, 2017 08:01
category: "Discussion"
author: Michal Tůma
---
Hello to all builders! I am building  cartesian printer 400x400x400 with replicape and manga. 

Steppers for X and Y going now only in one direction. For reverse direction i hear only noise (that current goes in stepper). Few days ago it was working. When i switch the connector and connect Y stepper to Z driver or X stepper to Z driver, steppers are turning both directions. So i think problem should be on replicape or redeem. Any idea where can be problem?





**Michal Tůma**

---
---
**Andrew Dowling** *January 06, 2017 22:49*

Sounds like you may have shorted out a stepper motor, and possibly damaged the stepper motor driver. If you tell the motors to spin at a slow speed either direction, and then force the axis to move manually, does it move in a jerky fasion?


---
**Michal Tůma** *January 09, 2017 13:37*

Shorted out stepper motor by wrong wiring or how it could happen? i had 3 times made wiring, so maybe i made second time some mistake in wiring, but now it is conected corectly.

Force the axis manualy when is moving and have jerky move, do you mean like missing steps? Move  axis with bigger force that motor do? Yes clockwise it does, when i tell it to go counterclockwise, it only hold position. Please tell me that i dont have to buy new replicape :-/


---
**Michal Tůma** *January 10, 2017 08:53*

After another flashing kamikaze, stepper motors are working both sides. Still don't know what was exactly wrong... Now i will keep in mind to do not change default.cfg :)


---
**Dan Cook** *January 18, 2017 20:37*

It may have been the travel distance in the config... I had the same problem when I was first building my printer. If it "thought" that it didn't have enough room to move a certain distance, it would just make a low noise until I M18'd...


---
*Imported from [Google+](https://plus.google.com/116142004932985250467/posts/JwCJDJbNtRN) &mdash; content and formatting may not be reliable*
