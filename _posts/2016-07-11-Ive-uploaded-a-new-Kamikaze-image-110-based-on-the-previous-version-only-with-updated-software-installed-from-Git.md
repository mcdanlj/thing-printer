---
layout: post
title: "I've uploaded a new Kamikaze image (1.1.0), based on the previous version, only with updated software installed from Git"
date: July 11, 2016 15:15
category: "Discussion"
author: Elias Bakken
---
I've uploaded a new Kamikaze image (1.1.0), based on the previous version, only with updated software installed from Git. This should have the latest Redeem, Toggle and OctoPrint with plugins. What's interesting is that it uses the same method for generating the image as with Kamikaze 2.0 which is still in the works. Feel free to test it out! [http://wiki.thing-printer.com/index.php?title=Kamikaze#Kamikaze_1.1.0](http://wiki.thing-printer.com/index.php?title=Kamikaze#Kamikaze_1.1.0)





**Elias Bakken**

---
---
**Chris Romey** *July 11, 2016 15:47*

Most excellent!  Thank you very much.  Will give this a go after I get home from work.


---
**Daniel Kusz** *July 15, 2016 20:10*

I installed the new Kamikaze. Toggle and Octoprint work very well. Also, the heat works much better. The only problem I see in Redeem - I think that there are problems in the reading cfg files. X and Y axes are reversed and do not move at the defined distance.  Changes in printer.cfg not give results. Do any of you install a new Redeem / Kamikaze? What are your experiences?


---
**Elias Bakken** *September 19, 2016 13:31*

The 1.1 series of images is an effort towards moving to Git installation of Redeem, since it is still under heavy development. V2.0 will not have it as a deb package. 



The error you are seeing is likely due to redeem not starting. Is the cape connected and powered through the power connector? Is the EEPROM updated? 


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/cH1RbqFTdJ4) &mdash; content and formatting may not be reliable*
