---
layout: post
title: "Just wondering how to disable Toggle on Kamikaze?"
date: January 31, 2016 00:18
category: "Discussion"
author: Daryl Bond
---
Just wondering how to disable Toggle on Kamikaze? I like to view a scrolling log file ($ journalctl -f) but haven't been able to disable Toggle so that the command line is always visible. I've tried disabling through systemctl but it seems that octoprint keeps bringing it back? Many thanks!





**Daryl Bond**

---
---
**Elias Bakken** *January 31, 2016 00:22*

systemctl disable toggle


---
**Daryl Bond** *January 31, 2016 00:24*

I could swear I already tried that, but it seems to have worked this time! Thanks Elias :)


---
**Elias Bakken** *January 31, 2016 00:28*

After a reboot, yes. If not: systemctl stop toggle


---
**Daryl Bond** *January 31, 2016 02:14*

I have commented out the line "After=redeem.service octoprint.service" in /lib/systemd/system/toggle.service



As far as I can tell this should prevent the loading of Toggle after Redeem or Octoprint?


---
*Imported from [Google+](https://plus.google.com/104347555452272558593/posts/aryjY6wLxyB) &mdash; content and formatting may not be reliable*
