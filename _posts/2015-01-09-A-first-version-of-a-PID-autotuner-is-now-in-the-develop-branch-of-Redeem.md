---
layout: post
title: "A first version of a PID autotuner is now in the develop branch of Redeem"
date: January 09, 2015 14:08
category: "Discussion"
author: Elias Bakken
---
A first version of a PID autotuner is now in the develop branch of Redeem. Please test and give feedback!





**Elias Bakken**

---
---
**Jimmy Edwards** *January 09, 2015 18:38*

The two things I had to learn the hard way with PID loops was.

1. You need a tunable sample rate.  No sense sampling something something a 1000 times a second when the feedback changes very slowly.

2. Integrator wined up is bad.  See #1.  If you are sampling too fast you can build up way too much demand for change in one direction.  Which will damp out any change needed in the opposite direction.  Solution is to zero built up integrator demand when the feedback begins to demand a change in opposite direction.



I Have not had a chance to test it.  But in my experience this is a common problem.


---
**Elias Bakken** *January 09, 2015 23:56*

1. The Tuning sample rate is the same as the PID sample rate in my case. Is that OK?

2. Since the integral is 0 during tuning, this should not be a problem.


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/NUAcB41bAoo) &mdash; content and formatting may not be reliable*
