---
layout: post
title: "Hello. New to the 3D Printing thing and I've assembled a FT Kossel kit and replacing the RAMPs with the Replicape and repurposed BBB"
date: September 15, 2017 23:45
category: "Discussion"
author: Chad Hinton
---
Hello.  New to the 3D Printing thing and I've assembled a FT Kossel kit and replacing the RAMPs with the Replicape and repurposed BBB.  I've re-assembled the stepper motor cables to be in alignment with the image on the wiki.  



I'm having more difficulty determining the wire placement for the end-stops.  The cables were wired straight through for the RAMPs but the image indicates the middle and an outer wire swap places.  How do tell which wire goes where?





**Chad Hinton**

---
---
**Chad Hinton** *September 16, 2017 00:09*

bah!  Scroll down and RTFM some more and there's the answer.  never mind.


---
**Dan Cook** *September 16, 2017 00:22*

👍


---
*Imported from [Google+](https://plus.google.com/109766677766216016353/posts/EPgweRJt5Gg) &mdash; content and formatting may not be reliable*
