---
layout: post
title: "I had a question about homing. After I complete a homing routine all of the axis go to zero"
date: July 24, 2017 16:25
category: "Discussion"
author: Tim Curtis
---
I had a question about homing. After I complete a homing routine all of the axis go to zero. They go the amount I put in from the home switch to zero.  That's good except for the Z axis. I wanted it to go to around 20mm above zero. So far the only way I could get it work was to have my Z zero set about 20mm away from the true zero (bed face). I found a place farther down in the config file that looks like I can tell the axis where to go after homing but it doesn't seem to follow that part? Any suggestions?





**Tim Curtis**

---
---
**Tim Curtis** *July 24, 2017 16:47*

I should add that I have a Cartesian printer.


---
**Elias Bakken** *July 24, 2017 17:19*

What does home_z say? Try to check M114 after you have done a homing. If it is not 0, 0, 0 you should check home_*




---
**Tim Curtis** *July 24, 2017 18:33*

I'll check what the home_z says when I get home but I had it set to .2346. That is the distance from the z endstop switch to Z zero (face of bed). On my machine it is 234.6mm of Z travel. So I set the home_z to .200 and after homing the table stops 34.6mm away from the nozzle. That ok but then I have to G92 my Z another 34.6 mm to get my zero where I need it to be.



I guess my question is can I set my z zero in the correct location (home_z .2346) but only have the z actually move to about 20mm away from the nozzle? Meaning move to Z 20.0. Or how do I have the Z not move at all after homing? The problem comes if  I change a nozzle or switch to my volcano hot end I risk the nozzle crashing into the bed after homing if the length is different.



 I did do a M114 check and the zero's were all set where they should be, ie middle of the table in X and Y. 


---
**Tim Curtis** *July 24, 2017 18:35*

My goal is to add a switch on the bed to probe with the nozzle and have it set the z zero automatically. That way I can switch nozzles and not worry about crashes...


---
**Elias Bakken** *July 24, 2017 18:48*

Could it be the soft end stops messing with you? I don't think it is, but you could check it. 




---
**Tim Curtis** *July 24, 2017 18:51*

I will post my config file later tonight to see if you can help me sort this homing out. It works great other than the Z axis.


---
**Dave Posey** *July 24, 2017 20:57*

Check your offset_* settings also.



When I first brought up my system, I was trying to set this parameter.   When I had it set and it was in conflict with your z_home setting I saw "weird" homing effects.  I also had the side effect of not being able to utilize my entire print bed.






---
**Tim Curtis** *July 25, 2017 11:34*

[System]



# CRITICAL=50, # ERROR=40, # WARNING=30,  INFO=20,  DEBUG=10, NOTSET=0

loglevel =  20



# Plugin to load for redeem, comma separated (i.e. HPX2Max,plugin2,plugin3)

plugins = 



# Machine type is used by M115 

# to identify the machine connected. 

machine_type = Genesis 250



[Geometry]

# 0 - Cartesian

# 1 - H-belt

# 2 - Core XY

# 3 - Delta

axis_config = 0



# Set the total length each axis can travel 

travel_x = 0.252

travel_y = 0.170

travel_z = 0.200



#   This affects the homing endstop searching length.

#   travel_* can be left undefined.

#   It will be determined by soft_end_stop_min/max_

<b># ...</b>





<b># Define the origin in relation to the endstops</b>

<b>offset_x = 0.126</b>

<b>offset_y = 0.085</b>

<b>offset_z = 0.2</b>



<b>#   It will be determined by home_speed and soft_end_stop_min/max_</b>

# offset_x = 0.0

# ...



bed_compensation_matrix = 

		1.0, 0.0, 0.0,

		0.0, 1.0, 0.0,

		0.0, 0.0, 1.0





# Stepper e is ext 1, h is ext 2

[Steppers]



microstepping_x = 7

microstepping_y = 7

microstepping_z = 7

microstepping_e = 5

microstepping_h = 5

microstepping_a = 3

microstepping_b = 3

microstepping_c = 3



current_x = 0.75

current_y = 0.75

current_z = 0.75

current_e = 0.5

current_h = 0.5

current_a = 0.5

current_b = 0.5

current_c = 0.5



# steps per mm:

#   Defined how many stepper full steps needed to move 1mm.

#   Do not factor in microstepping settings.

#   For example: If the axis will travel 10mm in one revolution and

#                angle per step in 1.8deg (200step/rev), steps_pr_mm is 20.

steps_pr_mm_x = 3.921569

steps_pr_mm_y = 3.921569

steps_pr_mm_z = 50.0

steps_pr_mm_e = 2.6813

steps_pr_mm_h = 2.6813

steps_pr_mm_a = 6.0

steps_pr_mm_b = 6.0

steps_pr_mm_c = 6.0



backlash_x = 0.0

backlash_y = 0.0

backlash_z = 0.0

backlash_e = 0.0

backlash_h = 0.0

backlash_a = 0.0

backlash_b = 0.0

backlash_c = 0.0





# Which steppers are enabled

in_use_x = True

in_use_y = True

in_use_z = True

in_use_e = True

in_use_h = True

in_use_a = False

in_use_b = False

in_use_c = False



# Set to -1 if axis is inverted

direction_x =  1

direction_y =  1

direction_z =  1

direction_e =  1

direction_h =  1

direction_a =  1

direction_b =  1

direction_c =  1



# Set to True if slow decay mode is needed 0 = False

slow_decay_x = 0

slow_decay_y = 0

slow_decay_z = 0

slow_decay_e = 0

slow_decay_h = 0

slow_decay_a = 0

slow_decay_b = 0

slow_decay_c = 0



dac_channel_x = 



[Planner]



# size of the path planning cache

move_cache_size = 1024



# time to wait for buffer to fill, (ms)

print_move_buffer_wait = 250



# if total buffered time gets below (min_buffered_move_time) then wait for (print_move_buffer_wait) before moving again, (ms)

min_buffered_move_time = 100



# total buffered move time should not exceed this much (ms)

max_buffered_move_time = 1000



acceleration_x = 0.2

acceleration_y = 0.2

acceleration_z = 0.1

acceleration_e = 0.5

acceleration_h = 0.5

acceleration_a = 0.5

acceleration_b = 0.5

acceleration_c = 0.5



maxjerk_xy = 15

maxjerk_z  = 2

maxjerk_eh = 5



# Max speed for the steppers in m/s

max_speed_x = 1.0

max_speed_y = 1.0

max_speed_z = 0.5

max_speed_e = 1.0

max_speed_h = 1.0

max_speed_a = 0.2

max_speed_b = 0.2

max_speed_c = 0.2



e_axis_active = True





[Cold-ends]

path = /sys/bus/w1/devices/28-000002e34b73/w1_slave

connect-therm-E-fan-0 = False

connect-therm-E-fan-1 = False

connect-therm-E-fan-2 = False

connect-therm-E-fan-3 = False

connect-therm-H-fan-0 = False

connect-therm-H-fan-1 = False

connect-therm-H-fan-2 = False

connect-therm-H-fan-3 = False

connect-therm-HBP-fan-0 = False

connect-therm-HBP-fan-1 = False

connect-therm-HBP-fan-2 = False

connect-therm-HBP-fan-3 = False



add-fan-0-to-M106 = True

add-fan-1-to-M106 = True

add-fan-2-to-M106 = True

add-fan-3-to-M106 = True



[Heaters]

# For list of available temp charts, look in temp_chart.py



temp_chart_E = SEMITEC-104GT-2

pid_p_E = 0.0383

pid_i_E = 71.3004

pid_d_E = 5.1443

ok_range_E = 4.0

path_adc_E = /sys/bus/iio/devices/iio:device0/in_voltage4_raw

mosfet_E = 5

onoff_E = False

prefix_E = T0



temp_chart_H = SEMITEC-104GT-2

pid_p_H = 0.0383

pid_i_H = 71.3004

pid_d_H = 5.1443

ok_range_H = 4.0

path_adc_H = /sys/bus/iio/devices/iio:device0/in_voltage5_raw

mosfet_H = 3

onoff_H = False

prefix_H = T1



temp_chart_A = B57560G104F

pid_p_A = 283.37

pid_i_A = 55.0

pid_d_A = 366.26

ok_range_A = 4.0

path_adc_A = /sys/bus/iio/devices/iio:device0/in_voltage1_raw

mosfet_A = 11

onoff_A = False

prefix_A = T2



temp_chart_B = B57560G104F

pid_p_B = 0.1

pid_i_B = 0.3

pid_d_B = 0.0

ok_range_B = 4.0

path_adc_B = /sys/bus/iio/devices/iio:device0/in_voltage2_raw

mosfet_B = 12

onoff_B = False

prefix_B = T3



temp_chart_C = B57560G104F

pid_p_C = 0.1

pid_i_C = 0.3

pid_d_C = 0.0

ok_range_C = 4.0

path_adc_C = /sys/bus/iio/devices/iio:device0/in_voltage3_raw

mosfet_C = 13

onoff_C = False

prefix_C = T4



temp_chart_HBP = B57560G104F

pid_p_HBP = 0.1024

pid_i_HBP = 71.7485

pid_d_HBP = 5.1911

ok_range_HBP = 4.0

path_adc_HBP = /sys/bus/iio/devices/iio:device0/in_voltage6_raw

mosfet_HBP = 4

onoff_HBP = False

prefix_HBP = B



[Endstops]

# Which axis should be homed. 

has_x = True

has_y = True

has_z = True

has_e = False

has_h = False

has_a = False

has_b = False

has_c = False



inputdev = /dev/input/event0



# Invert =

#   True means endstop is connected as Normally Open (NO) or not connected

#   False means endstop is connected as Normally Closed (NC) 

invert_X1 = True

invert_X2 = False

invert_Y1 = True

invert_Y2 = False

invert_Z1 = True

invert_Z2 = False



pin_X1 = GPIO3_21

pin_X2 = GPIO0_30

pin_Y1 = GPIO1_17

pin_Y2 = GPIO1_19

pin_Z1 = GPIO0_31

pin_Z2 = GPIO0_4



keycode_X1 = 112

keycode_X2 = 113

keycode_Y1 = 114

keycode_Y2 = 115

keycode_Z1 = 116

keycode_Z2 = 117



# If one endstop is hit, which steppers and directions are masked.

#   The list is comma separated and has format

#     x_cw = stepper x clockwise (independent of direction_x)

#     x_ccw = stepper x counter clockwise (independent of direction_x)

#     x_neg = setpper x negative direction (affected by direction_x)

#     x_pos = setpper x positive direction (affected by direction_x)

#   Steppers e and h (and a, b, c for reach) can also be masked.

#   

#   For a list of steppers to stop, use this format: x_cw, y_ccw

#   For Simple XYZ bot, the usual practice would be

#     end_stop_X1_stops = x_neg, end_stop_X2_stops = x_pos, ...

#   For CoreXY and similar, two steppers should be stopped if an end stop is hit.

#     similarly for a delta probe should stop x, y and z.

end_stop_X1_stops = x_ccw

end_stop_Y1_stops = y_ccw

end_stop_Z1_stops = z_ccw

end_stop_X2_stops =

end_stop_Y2_stops =

end_stop_Z2_stops =



soft_end_stop_min_x = -0.126

soft_end_stop_min_y = -0.085

soft_end_stop_min_z = 0.0





soft_end_stop_max_x = 0.126

soft_end_stop_max_y = 0.085

soft_end_stop_max_z = 0.2346





[Homing]



# Homing speed for the steppers in m/s

#   Search to minimum ends by default. Negative value for searching to maximum ends.

home_speed_x = -0.1

home_speed_y = -0.1 

home_speed_z = -0.02

home_speed_e = 0.01

home_speed_h = 0.01

home_speed_a = 0.01

home_speed_b = 0.01

home_speed_c = 0.01



# homing backoff speed                

home_backoff_speed_x = 0.01

home_backoff_speed_y = 0.01

home_backoff_speed_z = 0.01

home_backoff_speed_e = 0.01           

home_backoff_speed_h = 0.01           

home_backoff_speed_a = 0.01           

home_backoff_speed_b = 0.01           

home_backoff_speed_c = 0.01           

                                      

# homing backoff dist                             

home_backoff_offset_x = 0.01                                                          

home_backoff_offset_y = 0.01                                                              

home_backoff_offset_z = 0.01          

home_backoff_offset_e = 0.01          

home_backoff_offset_h = 0.01  

home_backoff_offset_a = 0.01  

home_backoff_offset_b = 0.01  

home_backoff_offset_c = 0.01



# Where should the printer goes after homing

#   home_* can be left undefined. It will stay at the end stop.





# ...


---
**Tim Curtis** *July 25, 2017 11:37*

That is my config file.

Down at the bottom it says "#   home_* can be left undefined. It will stay at the end stop."



I don't have anything defined in that section but the printer still moves to XYZ zero after homing?



It moves whatever I put in the "define origin after end stops section."


---
*Imported from [Google+](https://plus.google.com/106115959585163180149/posts/6rWowqCNu8J) &mdash; content and formatting may not be reliable*
