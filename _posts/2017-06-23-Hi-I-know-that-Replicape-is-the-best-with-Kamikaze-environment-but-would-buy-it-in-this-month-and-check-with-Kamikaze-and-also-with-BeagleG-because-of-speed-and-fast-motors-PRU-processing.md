---
layout: post
title: "Hi, I know that Replicape is the best with Kamikaze environment, but would buy it in this month and check with Kamikaze and also with BeagleG (because of speed and fast motor's PRU processing)"
date: June 23, 2017 13:31
category: "Discussion"
author: Radek Biskupski
---
Hi,

I know that Replicape is the best with Kamikaze environment, but would buy it in this month and check with Kamikaze and also with BeagleG (because of speed and fast motor's PRU processing).

And here's my question: does Replicape comes with .dts file or any PIN mapping file? I'm new in this area and I think I could be wrong or my thinking process is mistaked, but need something like that to configure it with BeagleG.

Thx





**Radek Biskupski**

---
---
**Jon Charnas** *June 23, 2017 13:44*

So - Replicape is designed to work with Redeem exclusively. I don't know if it would work with BeagleG - Redeem does exactly what BeagleG does, but has the pin outputs already set for the Replicape.



There is a .dts file for the Replicape, but so far as I know you're the first to try BeagleG with it... Any reason you prefer using BeagleG to the open solution made for the Replicape?



Look at the Kamikaze/Umikaze repos on github to find where we grab the .dts files from (older script is all in one: [https://github.com/eliasbakken/Kamikaze2/blob/master/make-kamikaze-2.0.8.sh](https://github.com/eliasbakken/Kamikaze2/blob/master/make-kamikaze-2.0.8.sh))

[github.com - Kamikaze2](https://github.com/eliasbakken/Kamikaze2/blob/master/make-kamikaze-2.0.8.sh)


---
**Radek Biskupski** *June 23, 2017 14:49*

Jon,

BeagleG was just the first clear-code and well documented open source I saw after fighting with Machinekit. It amazed me with very fast moves and wide spectrum interfaces and sockets easy to integrated with web platforms.

Regarding Kamikaze - no, it not exactly would be second in my test. In my opinion Kamikaze is full environment solution and less work need to be done from my side. If I'll be able to change GUI and configure some parameters like laser's head temp, lamp current and others for my needs, then will not take tests with BeagleG even, because integration could take more time that Kamikaze configuration :)

With lasers there are many different things which should be controlled, but hopefully they could be connected to Replicape inputs and presented on GUI, I hope so :|


---
*Imported from [Google+](https://plus.google.com/100171647175055988434/posts/ixHp2F9T3ER) &mdash; content and formatting may not be reliable*
