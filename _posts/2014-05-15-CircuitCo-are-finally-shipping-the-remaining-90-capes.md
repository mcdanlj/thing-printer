---
layout: post
title: "CircuitCo are finally shipping the remaining 90 capes"
date: May 15, 2014 13:18
category: "Discussion"
author: Elias Bakken
---
CircuitCo are finally shipping the remaining 90 capes. We've agreed that I do the testing of the capes to speed things along. Here is the UPS tracking number: 1Z A66 E64 0496 231819





**Elias Bakken**

---
---
**Elias Bakken** *May 15, 2014 13:19*

[http://wwwapps.ups.com/WebTracking/track](http://wwwapps.ups.com/WebTracking/track)


---
**Elias Bakken** *May 15, 2014 13:19*

Should be here Friday!


---
**James Armstrong** *May 15, 2014 14:14*

Around the world and back by the time it gets to me.


---
**Elias Bakken** *May 15, 2014 16:41*

James, did you not receive your order yet? I sent yours on May 6. via mail.


---
**James Armstrong** *May 15, 2014 18:12*

Not yet. Was there any tracking number? I do remember getting an email about it shipping now that I think of it, just no tracking.


---
**James Armstrong** *May 15, 2014 18:14*

Found the email. May 6th via international delivery. Address looks right.


---
**Elias Bakken** *May 15, 2014 18:17*

But still nothing? **+Charles Steinkuehler**  has gotten his..


---
**James Armstrong** *May 15, 2014 19:09*

Not as of yesterday. When did he receive his and what state is he in?


---
**James Armstrong** *May 15, 2014 22:58*

Just got it in the mail today. 


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/YbyotKz2z6e) &mdash; content and formatting may not be reliable*
