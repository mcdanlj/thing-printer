---
layout: post
title: "will this be an issue? at 0.5A my motor is louder than how I would like it to sound which is more silent at 0.75A"
date: August 14, 2016 06:18
category: "Discussion"
author: Step Cia
---
will this be an issue? at 0.5A my motor is louder than how I would like it to sound which is more silent at 0.75A

![images/441c436b1ffe8c1d57ee1c403d06ace6.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/441c436b1ffe8c1d57ee1c403d06ace6.jpeg)



**Step Cia**

---
---
**Daniel Kusz** *August 14, 2016 06:40*

Did you change microstepping for 7 or 8 - this is a silent mode for motors. Then you can use more current. Use some fan directly on replicape above 0.5A.


---
**Step Cia** *August 14, 2016 21:44*

thx for the info I changed it to 7 and keep 500mA decay to 4. now it sounds much more silent :) 



Although I still get this "stepper Fault - Stepper E" warning pop up not sure how to get rid of it...


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/4mmomtReSWc) &mdash; content and formatting may not be reliable*
