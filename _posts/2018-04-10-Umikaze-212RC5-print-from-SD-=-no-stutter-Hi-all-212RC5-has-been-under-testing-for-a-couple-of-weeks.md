---
layout: post
title: "Umikaze 2.1.2RC5 + \"print from SD\" = no stutter Hi all, 2.1.2RC5 has been under testing for a couple of weeks"
date: April 10, 2018 22:14
category: "Discussion"
author: Jon Charnas
---
Umikaze 2.1.2RC5 + "print from SD" = no stutter



Hi all, 2.1.2RC5 has been under testing for a couple of weeks. I'll release 2.1.2 final this weekend, after rebuilding the image to include OctoPrint 1.3.7.



If you're excited about getting your stutter to finally disappear, read the "print from SD" page linked below.



Other improvements will be listed in the release notes. I apologize for the delay in getting this released, work's been drowning me.



Cheers,

Jon





**Jon Charnas**

---


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/DQYSmxMSq7V) &mdash; content and formatting may not be reliable*
