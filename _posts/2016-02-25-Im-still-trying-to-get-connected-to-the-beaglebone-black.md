---
layout: post
title: "I'm still trying to get connected to the beaglebone black"
date: February 25, 2016 19:50
category: "Discussion"
author: Matthew Palmore
---
I'm still trying to get connected to the beaglebone black. i flashed kamikazie and then connected it to my comp, i dont get any octoprint network to show up so im not sure where im suppose to go from here





**Matthew Palmore**

---
---
**Matthew Palmore** *February 25, 2016 19:54*

ive tried to use putty to communicate with the bbb but i cant even type in the command window


---
**Tim Curtis** *February 25, 2016 20:10*

So you can't connect even with putty? When you connect with putty do you see a prompt on the black terimnal window that say "login" or anything like that? Also are you using a windows pc? At the very least when you plug in the BBB with the USB adaptor cable Windows should pop up and show BBB as a thumb drive with files on it. Even without replicape attached to the BBB. Did you get the pop up window? If not check to make sure the USB port on your computer is working.


---
**Tim Curtis** *February 25, 2016 20:11*

Also in putty are you using the IP 192.168.7.2 (for the USB adapter cable).


---
**Matthew Palmore** *February 25, 2016 20:18*

yea, i put in the correct ip, pressed save and it opens up a terminal window but after the terminal window pops up nothing happens. 



i do get the bbb shown up as a thumb drive. I'm using a windows computer that is running windows 10




---
**Tim Curtis** *February 25, 2016 20:27*

Sounds like a Windows 10 issue. I read somewhere about issues with putty and Windows 10. I still use Windows 7 myself. Do you have access to another computer with WIN7 or 8 to test on? As soon as you plug the BBB into the USB port you will get the pop up window like a thumb drive. You will find the drivers for windows loaded on the BBB. They get installed onto your pc and then putty should work. But you need to get to the drivers first. Is anyone else on here using Windows 10? Maybe they can help you.


---
**Tim Curtis** *February 25, 2016 20:29*

Sorry I missed reading where you get the thumb drive. Open the drive and click on the file named "Start.html" A page will open in your browser. Scroll down to the area listing the drivers to install. Install them and then try using putty. It should work then.


---
**Tim Curtis** *February 25, 2016 20:32*

Here is a link to tutorial videos on the BBB. This guy helped me a ton. Great wealth of information. 

[http://www.toptechboy.com/beaglebone-black](http://www.toptechboy.com/beaglebone-black)



Check out the second video for connecting with putty.


---
**Matthew Palmore** *February 25, 2016 20:54*

when typing the 192.168.7.2 ip adress into my browser nothing pops up. i have a feeling that could be the issue. how do i go about fixing that?




---
**Tim Curtis** *February 25, 2016 21:01*

If you have the correct drivers installed from your BBB then type in [192.168.7.2:5000](http://192.168.7.2:5000) and ocotprint should open.


---
**Daryl Bond** *February 25, 2016 22:30*

If you are having problems connecting then I would also suggest connecting a monitor to the BBB and working the problem from both ends. Try with ethernet and wireless as well. I have had issues with networking over USB but never chased it down because I use wireless now. A powered USB board is useful for connecting peripherals like wireless dongles. 


---
*Imported from [Google+](https://plus.google.com/103569827835970105061/posts/Pt2h7id6G7X) &mdash; content and formatting may not be reliable*
