---
layout: post
title: "I do find my Delta printer quite amusing at times"
date: May 08, 2016 09:27
category: "Discussion"
author: Pieter Koorts
---
I do find my Delta printer quite amusing at times. Input one thing, output something different. Tinkering so far in redeem all movements are now smooth and accurate while measuring with a vernier but oddly my 10x10 test cube comes out angled. Originally thought it was motor current as I run low current for testing and to save me print head in case I cause the machine to ram but even upping the current still has this effect.



Is this a normal occurrence of step skipping on the steppers or something else someone has encountered? Image of test cube is on its side.

![images/32edbf1901e9ed3c9292f4697ae12db1.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/32edbf1901e9ed3c9292f4697ae12db1.jpeg)



**Pieter Koorts**

---
---
**Daryl Bond** *May 08, 2016 09:51*

Based on my experience I would say definitely missing steps. What microstepping are you running? If you are using 8 then it will give you super quiet and smooth movement but isn't the strongest setting. I am using 6 which is a bit noisier (hissing) but doesn't skip for me.


---
**Daryl Bond** *May 08, 2016 09:53*

Good on you for getting to the pushing plastic stage!


---
**Pieter Koorts** *May 08, 2016 10:48*

Sorry Daryl, going to have to apologise for wasting your time. This is purely my fault. I will blame it on long calibration and not enough coffee.



The issue: I have FSR sensors for levelling however I noticed during printing the sensor trigger LED seems to just flash ever so slightly which means it was triggering while printing causing the steppers to stop for a few milliseconds.



I tried another print by turning off the FSR during printing and the print came out looking really good now. So I need to edit my config and make sure the FSR is disabled during printing each time so that it does not false trigger as my printer tries to smoosh plastic down.



The working print was done at 1.0 amp and micro stepping at 8 but steppers do warm up some what, so might test at a lower current.



Mmm... G+ does not allow adding images after the fact :-(


---
**Daryl Bond** *May 08, 2016 10:57*

Good to hear that it is all ok! 



You are probably already aware but the option you are after is "homing_only_endstops". I'm glad to know that this option is going to be useful for FSR usage.



Yeah, the no pictures after that first post is annoying...


---
**Elias Bakken** *May 08, 2016 14:43*

1A is probably too high, 0.5 is more like it for the deltas. You should have seen some messages appear in the log though, since end stop triggers are logged as info.


---
**Pieter Koorts** *May 08, 2016 14:57*

Before my mishap with the FSR sensor I originally thought "lets try 1 amp" just to rule it out since my carriage, effector and rods are all metal so have some weight to them. Putting that on 0.9 steppers I was not fully sure if the holding torque would be enough. Alas, the end result was me not paying attention.


---
**Daryl Bond** *May 08, 2016 22:59*

I have given up on 0.9 steppers on my printer. I'm not sure what their torque rating was but it wasn't enough! Couldn't print any faster than about 30mm/s without losing steps. Now I use the strongest 1.8 I could get. I can't really detect any difference in print quality after the change either.


---
**Pieter Koorts** *May 09, 2016 08:30*

I kind of like 0.9 steppers and the ones I have are not too weak. They may require more current to achieve more hold but 44N.cm is pretty decent for a delta printer. I am not expecting super crazy speeds with anything between 50mm/s and 100mm/s being plenty for this machine. I only have a e3d lite6 hotend which is not known for its blazing fast performance.


---
*Imported from [Google+](https://plus.google.com/100077479073911242630/posts/5wysCBieJsd) &mdash; content and formatting may not be reliable*
