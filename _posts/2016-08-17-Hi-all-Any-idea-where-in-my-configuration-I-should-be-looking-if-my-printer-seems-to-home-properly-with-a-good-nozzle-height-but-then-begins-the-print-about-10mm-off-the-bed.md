---
layout: post
title: "Hi all. Any idea where in my configuration I should be looking if my printer seems to home properly with a good nozzle height, but then begins the print about 10mm off the bed?"
date: August 17, 2016 17:09
category: "Discussion"
author: Tristan Dolik
---
Hi all. Any idea where in my configuration I should be looking if my printer seems to home properly with a good nozzle height, but then begins the print about 10mm off the bed?





**Tristan Dolik**

---
---
**Daniel Kusz** *August 17, 2016 17:29*

Is your Z axis inverted? If yes,  change it back and change homing endstop if necessary. After this, inverted z axis control change in Octoprint. 


---
**Tristan Dolik** *August 17, 2016 19:32*

Worked a treat. Thank you!


---
*Imported from [Google+](https://plus.google.com/118346741979795305129/posts/LsYDNLgdY9h) &mdash; content and formatting may not be reliable*
