---
layout: post
title: "?? so G29 with S is not save but G30 with S is save ??"
date: January 04, 2017 23:47
category: "Discussion"
author: Step Cia
---
?? so G29 with S is not save

but G30 with S is save ??





**Step Cia**

---
---
**Elias Bakken** *January 05, 2017 02:43*

Perhaps, there is no real standard for the gcode interface, so each code has its own semantics. S means simulate for one and save for the other. 


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/de2emJPUxxc) &mdash; content and formatting may not be reliable*
