---
layout: post
title: "Success! The Probe routine (G29) now seems to be working great!"
date: June 15, 2016 00:23
category: "Discussion"
author: Elias Bakken
---
Success! The Probe routine (G29) now seems to be working great! I think my WIP DIY probing effector might have some pretty bad accuracy across the bed, but OK precision. I've committed my changes and so I think it's time for a minor bump due to this. Perhaps let it simmer for a bit and get some feedback : )

![images/ebc2ea5cdb2fbefbd5ba86dda7e2058e.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/ebc2ea5cdb2fbefbd5ba86dda7e2058e.png)



**Elias Bakken**

---
---
**Elias Bakken** *June 15, 2016 17:32*

You can generate a new G29 macro by running G29C. Right now it makes a polar style probe pattern around 0, but it might serve as a starting point.


---
**Elias Bakken** *June 15, 2016 17:38*

For some reason the documentation on that Gcode does not work, but take a look at the source code and you shoudl be able to ficure it out: [https://bitbucket.org/intelligentagent/redeem/src/0a184a5a0eba7a37f9146b9873d4b2dcbcadb4db/redeem/gcodes/G29.py?at=master&fileviewer=file-view-default](https://bitbucket.org/intelligentagent/redeem/src/0a184a5a0eba7a37f9146b9873d4b2dcbcadb4db/redeem/gcodes/G29.py?at=master&fileviewer=file-view-default)


---
**Elias Bakken** *June 16, 2016 19:25*

Yeah, I've discovered this design flaw as well. Perhaps it would be better to have a pull-down resistor on the end stop inputs...



I use the 12V on my sensor which does work, since the pull-up is actually 47K (also a design flaw), so the current going into the 5V regulator is very small.



The input to the BBB is buffered, so it should handle up to 15V. My suggestion is to just connect it to 12V and give it a shot, should work fine!


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/JS6CEQqPxCy) &mdash; content and formatting may not be reliable*
