---
layout: post
title: "I can move X, Y and Z in both directions to Octoprint, X and Y both home ok, Z will not home, but sits and whines"
date: February 03, 2016 05:55
category: "Discussion"
author: Ken Runner
---
I can move X, Y and Z in both directions to Octoprint, X and Y both home ok, Z will not home, but sits and whines.  The Z axis is 2 steppers wired in parallel connected to threaded rods for the axis.  The printer is a old Printrbot style machine.  I can move Z down and hit the end stop with no problem, but using home on Octoprint causes the above issue, any ideas?





**Ken Runner**

---
---
**Elias Bakken** *February 03, 2016 10:42*

It might be the speed of the z-axis. Try lowering homing speed for z.


---
**James Armstrong** *February 03, 2016 14:27*

Yes, I had this problem on mine also, moving using arrows on octoprint worked (I think because there is a setup that sets the feedrate for moves on the gui) but G28 would whine and not move because it was using the config file feedrate.




---
**Ken Runner** *February 07, 2016 22:17*

After further testing I adjusted the values of home_speed for z, but that didn't prevent the issue.  The only change that kept the issue from occuring was to change the Z axis steps per mm to a lesser value.  My calculated value for Z is 141.7, but I had problems with any value over about 85.  Any ideas?


---
**Hitbox Alpha** *February 08, 2016 22:33*

I am also getting this though i'm using 125 steps. i've tried turning speeds and feeds both up and down to no luck.



Do you have a probe on your? i know i do and i'm using Z2 for the endstop


---
**Ken Runner** *February 09, 2016 05:25*

no probe, just using z1 as the neg endstop


---
*Imported from [Google+](https://plus.google.com/117074313801696052366/posts/UimAn5LRDFt) &mdash; content and formatting may not be reliable*
