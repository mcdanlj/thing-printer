---
layout: post
title: "which slicer for redeem? Hello it would be great if there is any recommendation concerning slicing"
date: July 26, 2016 08:45
category: "Discussion"
author: Mathias Giacomuzzi
---
which slicer for redeem?



Hello it would be great if there is any recommendation concerning slicing.



Dose someone share settings for slicr3 or the new cura? 



 





**Mathias Giacomuzzi**

---
---
**Jon Charnas** *July 26, 2016 09:01*

Hi Mathias,

Redeem is just like any other firmware - it doesn't need special slicer settings - your printer does! Calibrating slicer settings is long and tedious, but not dependent on the printer's software, but rather the printer mechanics (nozzle size, plastic melting point, inertia of the print head when moving at speed, etc.). Spend time testing out different settings to see what they do. Take the simple calibration cube and slice it on your desktop with different settings, upload a gcode file to octoprint and see how it prints.


---
**Mathias Giacomuzzi** *July 26, 2016 12:10*

Hey Jon,



So that is all clear ... I know that. The reason why I asked was I just want to learn from others :-) So I already tried your configs with repitierhost.  But I would like to use cura and slic3r for some more tests.



With the built in profile in octoprint i printed the test cube which came out pretty well. Okay the dimension is 20.13 x 20.13 x 20.28. So what could be the issue here?


---
**Jon Charnas** *July 26, 2016 12:20*

Hi Mathias, for the oversize issues, I would suggest you lower your X/Y jerk values. For the height oversize, I wonder if your 0 is touching the bed or with 0.2 mm space? Because if you start printing at 0.2mm, the printer'll move to 0.2+20mm upwards... The first layer would then be 0.4mm.



Repetier uses CuraEngine by the way, the new Cura has an incompatible configuration schema with the older ones, so Octoprint and Repetier will be slightly slow in incorporating those changes.



I personally have never used slic3r successfully - though I haven't tried very hard after trying with CuraEngine. You could start off by copying the settings from the Repetier profiles into the new Cura and Slic3r as well. There is also a 2 week demo of Simplify3D you can try ($149 for the software), and Craftware which is still in beta test.



There are probably a few I've forgotten. KISSlicer was popular for a while, though again I've never had much success or tried very hard to get it working...


---
**James Brassill** *July 28, 2016 18:20*

I have been trying to use Slic3r since I have about a dozen filament configs made. I have tried using the same start code from when I had RAMPs but all it does is home X&Y then home Z and start moving extremely slow towards the center of the bed. I have also tried my Cura start code and it will start to print but it does so about 10mm off the bed.


---
**Jon Charnas** *July 28, 2016 18:27*

James, check your calibration of redeem then. I've had no issues printing from Cura, and I can't imagine why it wouldn't from Slic3r either. The g-code instructions are the same as what you'd feed to a Marlin firmware.


---
**James Brassill** *July 28, 2016 18:36*

Cura prints alright with everything being dimensionally correct. I will make a new post with the start code comparisons between cura and slic3r. I am sure the issue lies in there somewhere. 


---
*Imported from [Google+](https://plus.google.com/104869072343043179738/posts/RC6NyWfRm5G) &mdash; content and formatting may not be reliable*
