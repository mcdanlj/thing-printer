---
layout: post
title: "Heating and homing tests using an iPad"
date: February 21, 2016 00:36
category: "Discussion"
author: Andy Tomascak
---
Heating and homing tests using an iPad. I let the magic smoke out of ext1 during a rewire, so this isn't quite a stock install anymore. :)



My dog Cato (sporting a full-body Mohawk) makes a cameo at the end. :)


**Video content missing for image https://lh3.googleusercontent.com/-DwkKMkJ4V8E/VskGj9S8coI/AAAAAAAAD68/l7u-_F2raR8/s0/VID_20160220_165919153.mp4.gif**
![images/8c4424b3e329ed3cd33003689d044e1d.gif](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/8c4424b3e329ed3cd33003689d044e1d.gif)



**Andy Tomascak**

---
---
**Elias Bakken** *February 21, 2016 01:09*

Haha! Great! Love it! If you send me your rostock max config file, I can include it in the debian package and you can simply use local.cfg to override the extruder PWM channel. That is the purpose of the local file, local quirks :)


---
**Andy Tomascak** *February 21, 2016 01:27*

That was my intention! I was lazy though and only changed my main config, so let me separate out my "tweaks" so that the rostock_max.cfg is clean. I'll email it to you as soon as I get the bugs ironed out.


---
**Robert Moser** *February 21, 2016 04:46*

Those steppers sound a lot different than the one I'm using!



Sorry about the magic smoke.  Hot-air SMD rework stations are cheap on ebay!  :D


---
**Tim Curtis** *February 22, 2016 19:23*

Is the tablet in the video wifi'd into redeem or is it hooked up direct (wired)?


---
**Andy Tomascak** *February 22, 2016 19:47*

The tablet is just an iPad. It's not running anything special, just OctoPrint thru the web interface using wifi. I could have done the same with my phone! (but it would have been much harder to see! )


---
*Imported from [Google+](https://plus.google.com/110080975040288455209/posts/XtWGE6vD1Yb) &mdash; content and formatting may not be reliable*
