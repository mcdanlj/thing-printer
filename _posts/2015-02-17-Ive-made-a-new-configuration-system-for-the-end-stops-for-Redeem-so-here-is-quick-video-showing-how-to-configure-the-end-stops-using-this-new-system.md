---
layout: post
title: "I've made a new configuration system for the end stops for Redeem, so here is quick video showing how to configure the end stops using this new system"
date: February 17, 2015 16:37
category: "Discussion"
author: Elias Bakken
---
I've made a new configuration system for the end stops for Redeem, so here is quick video showing how to configure the end stops using this new system. Please note that when updating to the latest redeem, the standard is now that none of the steppers are masked by any of the end stops. So do not home until you have updated the config!





**Elias Bakken**

---
---
**James Armstrong** *February 17, 2015 18:12*

Is that your fan noise I hear? I was just wondering because for some reason my steppers whine a lot which they didn't do with my other printer board and I use the DRV8825's on my core xy and they are pretty much silent. I have tried various current and mode settings.


---
**Elias Bakken** *February 17, 2015 18:15*

The one setting that is unavailable on Replicape is mixed mode. This is the mode that is standard on the Pololu boards for instance.. But yes, my fan is also on on my power source. You can get mixed mode, but you need to cut some wires on the PCB :)


---
**James Armstrong** *February 17, 2015 18:17*

Ok. That is the mode I use on my other drivers. I think I will give that a try.


---
**Elias Bakken** *February 17, 2015 18:18*

Yes, me too! Next rev will definitely have the mixed mode as a setting, if I choose DRV8825s again!


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/YHXR93LQZ14) &mdash; content and formatting may not be reliable*
