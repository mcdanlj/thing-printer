---
layout: post
title: "Hello everyone! Could anyone take some time to check out an issue I currently have with the replicape"
date: June 04, 2016 22:53
category: "Discussion"
author: Adel Alouani
---
Hello everyone!

Could anyone take some time to check out an issue I currently have with the replicape. I currently can't connect to redeem via octoprint because I keep getting this error message: 



Machine State: Error: Failed to autodetect serial port, please set it manually.



I tried looking up what extra serial port I could add and added /dev/octoprint_1



but that wasn't it.



Below is a small snipet of the logs for redeem state and a log of octoprint and redeem. It says something about not being able to get a profile. Please have a look, I tried reinstalling redeem and octoprint but no success. I find the redeem status pretty weird at the end with it saying ValueError: invalid literal for float(): 1.0 #before 0.1



Here's a pastebin:

[http://pastebin.com/7mPFTgTc](http://pastebin.com/7mPFTgTc)



Thanks for reading.

Cheers :)





**Adel Alouani**

---
---
**James Armstrong** *June 04, 2016 23:42*

Looks like a #0.1 as a comment in that config line is maybe confusing the issue. 


---
**Adel Alouani** *June 05, 2016 00:09*

Ah yes, there was an extra comment at the end of a line in a config file. Log says redeem ready now, but I still can't connect through the serial port :/


---
**Adel Alouani** *June 05, 2016 00:51*

Got it to connect after trying to add /dev/octoprint_1 manually after. Lesson learned: Don't write wrong code on the .cfg files. Thanks :)


---
*Imported from [Google+](https://plus.google.com/+AdelAlouani/posts/Bfze3XKjRqi) &mdash; content and formatting may not be reliable*
