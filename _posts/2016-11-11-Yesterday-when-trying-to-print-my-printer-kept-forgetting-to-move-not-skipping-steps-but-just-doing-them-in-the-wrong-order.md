---
layout: post
title: "Yesterday when trying to print my printer kept forgetting to move, not skipping steps but just doing them in the wrong order"
date: November 11, 2016 07:24
category: "Discussion"
author: Leo Södergren
---
Yesterday when trying to print my printer kept forgetting to move, not skipping steps but just doing them in the wrong order. Is there any quick fix, like changing the planner (look ahead)?





**Leo Södergren**

---
---
**Jon Charnas** *November 11, 2016 07:36*

Uh... That's the first time I hear this kind of problem. What redeem are you running? The latest from git master, git develop?


---
**Jon Charnas** *November 11, 2016 07:36*

Also is it a delta?


---
**Leo Södergren** *November 11, 2016 07:37*

lastest avalible from the wiki.


---
**Leo Södergren** *November 11, 2016 07:38*

No ultimaker style


---
**Jon Charnas** *November 11, 2016 07:53*

Mmk, so the version from kamikaze 2.0.8 then. Does the printer move in the right direction when you do manual moves?


---
**Leo Södergren** *November 11, 2016 07:57*

Yeah everything works fine, except for the strange moves(and that M303 behaves strangley)


---
**Jon Charnas** *November 11, 2016 08:12*

Try installing redeem from git. Using ssh as root, go to/usr/src/redeem and run in order "git pull", "python [http://setup.py](http://setup.py) clean install", "cp configs/* /etc/redeem", "systemctl restart redeem" and try again


---
**Leo Södergren** *November 11, 2016 08:13*

Thanks will try when i get home and share my results.


---
**Elias Bakken** *November 11, 2016 11:27*

It might be a slicer issue that is appearing. Which slicer are you using? And are you getting the same results using the built in Cura?


---
**Leo Södergren** *November 11, 2016 11:44*

Would be strange if it was a slicer issue since running the same file gives different results


---
**Leo Södergren** *November 11, 2016 14:02*

**+Jon Charnas** did you mean "python [setup.py](http://setup.py) clean install"?




---
**Jon Charnas** *November 11, 2016 14:03*

Yes, sorry


---
**Jon Charnas** *November 11, 2016 14:04*

Not sure why the Android app added an http in front


---
**Leo Södergren** *November 11, 2016 14:07*

Didn't seem to make any difference, I'll try a different slicer


---
**Leo Södergren** *November 11, 2016 14:54*

I turned up the stepper current and everything works now. Seems like the printer was symmetrically missing steps, It skipped the same amount of steps in one direction as the other. 



This picture is shows how it was printing before, looks like the part is supposed to look like that since the skipped step is only on one part of the print, very strange.

![images/9495c232d4fc47b7802da5bb47bad9ad.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/9495c232d4fc47b7802da5bb47bad9ad.jpeg)


---
**Pieter Koorts** *November 11, 2016 17:38*

Is this a case of it printing and then every now and then just stops moving for a second or so and then continues printing?


---
**Leo Södergren** *November 11, 2016 21:43*

**+Pieter Koorts** No it looked like it was correcting itself.


---
**Pieter Koorts** *November 11, 2016 21:48*

**+Leo Södergren** the reason I ask is because when I left the debug log enabled I found my printer stuttering on certain moves with high gcode line counts. I think the beagle bone was too busy writing logs to feed gcode to the rest of the program.


---
**Leo Södergren** *November 11, 2016 22:00*

**+Pieter Koorts** I'm just running the log at lvl 20 seems to run smoothly even with small accelerations.


---
*Imported from [Google+](https://plus.google.com/107257105792966201860/posts/PXo5N5oVB29) &mdash; content and formatting may not be reliable*
