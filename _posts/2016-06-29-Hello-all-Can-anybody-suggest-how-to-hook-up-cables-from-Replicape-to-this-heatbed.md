---
layout: post
title: "Hello all, Can anybody suggest how to hook up cables from Replicape to this heatbed?"
date: June 29, 2016 18:28
category: "Discussion"
author: Daniel Kusz
---
Hello all,



Can anybody suggest how to hook up cables from Replicape to this heatbed?



[http://reprap.org/wiki/File:RRD-Mk2b-dual-power_blk.jpg](http://reprap.org/wiki/File:RRD-Mk2b-dual-power_blk.jpg)





**Daniel Kusz**

---
---
**James Armstrong** *June 29, 2016 22:07*

Depends on the power supply voltage you are using. I use this bed. For 12v you solder +12v to pad 1 and run replicape hotbed- to pad 2 and 3 (you can solder a short wire between 2 and 3 or run two ground wires back to the replicape hotbed terminal. 


---
*Imported from [Google+](https://plus.google.com/103041373788524358316/posts/H1i2DigdAbX) &mdash; content and formatting may not be reliable*
