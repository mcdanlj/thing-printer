---
layout: post
title: "Greetings, I have a BBBW. Should I be using umikaze or kamikaze?"
date: September 05, 2017 13:50
category: "Discussion"
author: Steve Kappes
---
Greetings, I have a BBBW. Should I be using umikaze or kamikaze?

Thanks,





**Steve Kappes**

---
---
**Jon Charnas** *September 05, 2017 14:03*

Hi Steve, Definitely Umikaze 2.1.1 - Kamikaze 2.1.0 and before doesn't support the BBBW. You can find the image of the still-being-released 2.1.1 image here: [drive.google.com - drive.google.com/open?id=0B_1DFGwNMXzPQXdNa2pZNXlRZE0](https://drive.google.com/open?id=0B_1DFGwNMXzPQXdNa2pZNXlRZE0) (this will be posted to github as soon as the release notes are ready)


---
**Steve Kappes** *September 05, 2017 14:44*

Thanks, I've been reading everything I can find but still unsure.


---
**Jon Charnas** *September 05, 2017 14:46*

Like I said, the 2.1.1 release is in a "everything is ready but the documentation" - so, yeah, you're gonna feel like some stuff might be missing ;)


---
**Steve Kappes** *September 05, 2017 14:55*

No problem, I appreciate the clarity. Thanks,


---
*Imported from [Google+](https://plus.google.com/116146659082281745739/posts/i1aLRuXKpTu) &mdash; content and formatting may not be reliable*
