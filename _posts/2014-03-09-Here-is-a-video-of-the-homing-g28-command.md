---
layout: post
title: "Here is a video of the homing (g28) command"
date: March 09, 2014 19:39
category: "Discussion"
author: Elias Bakken
---
Here is a video of the homing (g28) command. Still a bit ruff round the edges, but it works:)

![images/70d18e49a86b2cfde3c8aa505ecefa50.gif](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/70d18e49a86b2cfde3c8aa505ecefa50.gif)



**Elias Bakken**

---


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/b3rpCgPBHSQ) &mdash; content and formatting may not be reliable*
