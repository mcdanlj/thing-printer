---
layout: post
title: "Hello! Just ordered a Replicape and I'm trying to test some of the electronics I have on hand before it arrives"
date: February 18, 2017 23:12
category: "Discussion"
author: Matouš Vrba
---
Hello! Just ordered a Replicape and I'm trying to test some of the electronics I have on hand before it arrives. I have a MAX31820PARMCR+ sensor, which is a One-Wire temp sensor from MAXIM and I am trying to make it work with my Beaglebone (I have a BB Green Wireless). The BB is running Kamikaze. The problem is that I cannot see the sensor when running "ls /sys/bus/w1/devices/". One reason might be that the sensor only supports parasitic-power mode, where it sucks power from the DQ line, which the driver does not account for.



I cannot find where the one-wire protocol is implemented in the Kamikaze distribution - it seems that most standard tutorials on how to run one-wire on BB either presume that the device will be listed in "/sys/devices/w1_bus_master1" (link to tutorial: [http://www.bonebrews.com/temperature-monitoring-with-the-ds18b20-on-a-beaglebone-black/](http://www.bonebrews.com/temperature-monitoring-with-the-ds18b20-on-a-beaglebone-black/)) or that there exists a file "/sys/devices/bone_capemgr.9/slots" (for example this tutorial: [https://interactingobjects.com/ds18b20-temperature-sensor-on-a-beaglebone-black-running-ubuntu/](https://interactingobjects.com/ds18b20-temperature-sensor-on-a-beaglebone-black-running-ubuntu/)). Neither of these seem to correspond with what is running on my Kamikaze BB (the folders do not even exist). Also when I rum lsmod, there is no w1-gpio nor w1-therm as suggested by some of the tutorials.



So how is the one-wire implemented? How can I modify it? Any suggestions on getting this working?



Thanks,

 Matouš





**Matouš Vrba**

---
---
**Matouš Vrba** *February 22, 2017 20:36*

tl;dr:

So again - where do I find the OneWire driver and how do I modify it?



So I delved a bit deeper into the Device tree setup on the Kamikaze and it turns out that for some reason instead of the standard path "/sys/devices/bone_capemgr.9/slots" the slots file is located in 

"/sys/devices/platform/bone_capemgr/" for some reason. There is also a file "BB-W1-P9.12-00A0.dtbo" board file located in "/lib/firmware/", so I loaded it by running "echo BB-W1-P9.12 > /sys/devices/platform/bone_capemgr/slots" and it successfully created the slot and a device in "/sys/bus/w1/devices/" called "w1_bus_master1". So it seems to be working somehow. But no device for the sensor connected to my board appears, only some garbage with nonsense names like "00-4000000" etc., which would point at the sensor not communicating properly, but still trying to do so. I have tried connecting the sensor to P9.22 as implied by the pinout image (on [http://wiki.thing-printer.com/index.php?title=Replicape](http://wiki.thing-printer.com/index.php?title=Replicape)) as well as P9.12 as implied by the name of the board file and neither worked.



I am suspecting that the OneWire driver itself has to be modified in order to support the parasitic mode properly, but I cannot find it. It has cost me a lot of time to get even this far and I would really appreciate some help or at least some directions.


---
**Elias Bakken** *February 22, 2017 21:34*

This is how it's done in Redeem: [bitbucket.org - intelligentagent / Redeem 
  / source  / redeem / Redeem.py
 — Bitbucket](https://bitbucket.org/intelligentagent/redeem/src/0dc7929320eec5f4573579aab4fb31ddaab8bbbd/redeem/Redeem.py?at=master&fileviewer=file-view-default#Redeem.py-258)




---
**Elias Bakken** *February 22, 2017 21:36*

From memory, I think only DS18B20 has been tested, but it should give a starting point. Realy simple code for those. 


---
**Matouš Vrba** *February 24, 2017 18:01*

Thanks for your answer, the code you linked to will certainly be useful. However I think that I will have to look into the linux onewire driver itself. I found some forum threads discussing onewire with parasitic mode on the RasPi and they got it working but is seems that Kamikaze uses a different version of the driver.


---
*Imported from [Google+](https://plus.google.com/114713703406364073556/posts/7Qh7z1cq6Mb) &mdash; content and formatting may not be reliable*
