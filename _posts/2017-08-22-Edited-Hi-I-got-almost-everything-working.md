---
layout: post
title: "Edited: Hi, I got almost everything working :)"
date: August 22, 2017 03:53
category: "Discussion"
author: Aditya Gandhi
---
Edited:

Hi, I got almost everything working :). Just the last steps, for  Calibration.



I am printing calibration cubes, they are off by about .5 mm, what changes should I make to fix this



Another query regarding, Z-Probe &  G29 macro  Question on a Prusa-i3 clone, with Induction sensor



I am having issues, with this process, when printing I can see one side of the board having issues, with calibration. Seems like the values set automatically are not correct? 

I have about .5-.7 mm extra distance between the two extreme on Y-axis while printing



My config: [https://pastebin.com/255BTR1P](https://pastebin.com/255BTR1P) 



I am trying to get G29 Macro working,  when I run the G29 macro, from my config. I see that there is a config for Delta printers added to my local.cfg which is generated, os this correct?



This is what is added to my local.cfg



[Geometry]

travel_y = 0.19

travel_z = 0.2

travel_h = 0.2

offset_z = 0.0

offset_h = 0.0

bed_compensation_matrix = [[1.0, 0.0, -0.0015206510288527629], [0.0, 1.0, -0.006057611774593304], [0.0015229669541757136, 0.006094530778882017, 1.0]]



[Delta]

hez = 0.0601

l = 0.322

r = 0.175

ae = 0.02032

be = 0.02032

ce = 0.02032



















**Aditya Gandhi**

---
---
**Dan Cook** *August 22, 2017 12:42*

As far as the [Delta] section showing up in your local.cfg, you should be ok... you can add this little entry to your Geometry section to be certain:



[Geometry]

axis_config = 0



As far as tweaking your steps for calibration cubes... if your Y axis is printing just a little wide, try backing the "steps_pr_mm_y = 6.25" to something like "6" and try printing another cube. You should be able to use the two cubes to calculate a fairly close steps_pr_mm... (and the same for your other two axes)... good luck! 


---
**Phil Turner** *August 22, 2017 17:04*

On calibration for print size, take your expected size and divide that by your measured size, then multiply that by your steps per millimeter for that axis, to get an adjusted value for steps_per_mm



For example, if you have a 40mm test cube, and Y is measuring 40.5:

40/40.5=0.9876

0.9876*6.25=6.1725



You would then set your steps_per_mm_y = 6.1725

and try another print test. repeat as needed, and be sure you're measuring the proper axis (I've gotten my calibration cubes turned and tried tweaking my X axis when accidentally measuring my Y axis on the test cube).




---
**Aditya Gandhi** *August 23, 2017 00:46*

Thanks will try this and update 


---
*Imported from [Google+](https://plus.google.com/116302896773894703093/posts/f7NwCGBRCop) &mdash; content and formatting may not be reliable*
