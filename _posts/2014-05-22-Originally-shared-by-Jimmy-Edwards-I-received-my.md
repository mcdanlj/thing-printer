---
layout: post
title: "Originally shared by Jimmy Edwards I received my !!!"
date: May 22, 2014 18:17
category: "Discussion"
author: Jimmy Edwards
---
<b>Originally shared by Jimmy Edwards</b>



I received my  #Replicape  !!!

The package looked scary when it arrived in the mail...

But the Board shows no sign of damage.  She is a beauty.  

I'm so glad that I bet on Elias Bakken and the "thing" crew.

Now I have to get to work on building my 3D printer.



![images/480519f9aa6bafaefa67a57ee0001f7f.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/480519f9aa6bafaefa67a57ee0001f7f.jpeg)
![images/59603ae7ba12608dc0f8af9ad3ab9507.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/59603ae7ba12608dc0f8af9ad3ab9507.jpeg)
![images/5a1867f09e2eeec3d2db7fea165c5c5e](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/5a1867f09e2eeec3d2db7fea165c5c5e)

**Jimmy Edwards**

---
---
**Elias Bakken** *May 22, 2014 18:23*

Fan-freaking-tastic James! Yes, it did look busted, but none of the packages have had any faults so far! If you want to use the Thing Image, make sure you do a

opkg update

opkg install replicape-device-tree

from the fresh image.


---
**Jimmy Edwards** *May 22, 2014 18:38*

Thank you So Much for the VooDoo that you do Elias !!!  I know that it takes a LOT of work to go from VaporWare to HardWare.


---
**James Armstrong** *May 22, 2014 18:51*

Yeah, my box looked the same but all was intact also.


---
**Elias Bakken** *August 17, 2014 13:14*

**+James Edwards** Any progress with your cape?


---
**Jimmy Edwards** *August 18, 2014 17:56*

I've come down with a severe case of summer time procrastination.  I'm plugging away on the assembly of a printer frame.  As soon as I get the Stepper motors mounted I'll fire up the Cape and try to make things move.  This is more of a winter project,,, something to do when stuck inside.


---
**Elias Bakken** *August 18, 2014 18:32*

Hehe, Okay! : )


---
*Imported from [Google+](https://plus.google.com/+JamesEdwards780/posts/EqG8dKYxPvS) &mdash; content and formatting may not be reliable*
