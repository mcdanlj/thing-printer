---
layout: post
title: "Elias Bakken any reason we can't checkout and use the standard octoprint?"
date: January 26, 2016 13:41
category: "Discussion"
author: James Armstrong
---
**+Elias Bakken** any reason we can't checkout and use the standard octoprint? The version on the image does not let you update it. Is all the special Redeem stuff just a plugin or did you modify octoprint just for Redeem?





**James Armstrong**

---
---
**Elias Bakken** *January 26, 2016 14:22*

It's Octoprint from the master branch, but it seems it has been changed slightly in the packaging process... The versioning of Octoprint is very automated and not that straight forward to understand, but it looks like it tags the commit with something. I'm sending a cape to **+Gina Häußge**​ so hopefully she can help shed some light on that in return:)


---
**James Armstrong** *January 26, 2016 14:48*

Right, the plugin returns are like [?25 and don't get processes as a success but they still install (after removing and installing pip and playing the folder permissions game on the python dist-packages folder). 

 


---
*Imported from [Google+](https://plus.google.com/+JamesArmstrong3/posts/LU2QscNjBf2) &mdash; content and formatting may not be reliable*
