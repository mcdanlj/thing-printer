---
layout: post
title: "So playing with my replicape today. While having it open all was working all okay and motors were moving"
date: April 03, 2016 20:24
category: "Discussion"
author: Pieter Koorts
---
So playing with my replicape today. While having it open all was working all okay and motors were moving. Now once the printer is all together, on first start the motors do their singing and then nothing. I can login to the BBB and I can access the web interface but it seems no serial port exists even though it did while spread out all over the desk.



It is able to see the cape so not sure what the hell is happening :(



[    3.267160] bone_capemgr bone_capemgr: slot #0: 'Replicape 3D printer cape,00B3,Intelligent Agen,BB-BONE-REPLICAP'





**Pieter Koorts**

---
---
**Jon Charnas** *April 03, 2016 20:46*

can you see if redeem is running? "systemctl status redeem -l " will show you something like "active" or "failed" near the top of the output.


---
**Pieter Koorts** *April 03, 2016 20:48*

Redeem is listed as active running but weird output as well



   CGroup: /system.slice/redeem.service

           `-301 /usr/bin/python /usr/bin/redeem



Apr 03 20:33:06 kamikaze redeem[301]: load_entry_point('Redeem==1.1.5', 'console_scripts', 'redeem')()

Apr 03 20:33:06 kamikaze redeem[301]: File "/usr/lib/python2.7/dist-packages/redeem/Redeem.py", line 568, in main

Apr 03 20:33:06 kamikaze redeem[301]: r = Redeem()

Apr 03 20:33:06 kamikaze redeem[301]: File "/usr/lib/python2.7/dist-packages/redeem/Redeem.py", line 190, in <i>_init_</i>

Apr 03 20:33:06 kamikaze redeem[301]: stepper.set_decay(printer.config.getint("Steppers", "slow_decay_" + name))

Apr 03 20:33:06 kamikaze redeem[301]: File "/usr/lib/python2.7/ConfigParser.py", line 359, in getint

Apr 03 20:33:06 kamikaze redeem[301]: return self._get(section, int, option)

Apr 03 20:33:06 kamikaze redeem[301]: File "/usr/lib/python2.7/ConfigParser.py", line 356, in _get

Apr 03 20:33:06 kamikaze redeem[301]: return conv(self.get(section, option))

Apr 03 20:33:06 kamikaze redeem[301]: ValueError: invalid literal for int() with base 10: 'True'


---
**Jon Charnas** *April 03, 2016 20:56*

Yes, that looks like a case of expecting a numerical value and instead reading in a "True" for that line... Double check your local.cfg file. And if you've installed from the git repository, don't forget to copy configs/*.cfg to /etc/redeem (it won't overwrite local.cfg) to have the proper syntax and values in those files which can otherwise cause issues.


---
**Pieter Koorts** *April 03, 2016 21:24*

This is the stock kamikaze image which was working while in pieces but now all back together I get no serial. The only difference is I have hooked up the end-stops. I think I will disconnect the end-stops and update the system files just in case.


---
**Pieter Koorts** *April 04, 2016 19:41*

So, got redeem all working again and motors moving. Weird issue, when homing the sliders only move up a bit then down but never all the way up to the end-stops. I keep homing it will eventually get to the top but then I can only move down about 10 and it won't go further. Any tips on getting a Delta printer setup on the replicape?


---
**Pieter Koorts** *April 04, 2016 23:18*

Not to worry. I feel like such a numpty for choosing the wrong printer type. Need more coffee.


---
*Imported from [Google+](https://plus.google.com/100077479073911242630/posts/3rXJ5xarNh5) &mdash; content and formatting may not be reliable*
