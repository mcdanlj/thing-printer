---
layout: post
title: "Looking for help from anyone running a webcam on their Replicape"
date: September 19, 2017 11:08
category: "Discussion"
author: Tim Curtis
---
Looking for help from anyone running a webcam on their Replicape.

Mine works fine by default with the Umikaze flash with this listed in the line "[http://kamikaze.local:8080/?action=stream](http://kamikaze.local:8080/?action=stream)"



It works fine on my wired Ethernet desktop, I see the webcam image just fine. But I can't see the stream on my tablet or phone. I also can't see it from my remote session while outside my network on another desktop computer. I can control my printer just fine, just no webcam feed.



I thought maybe the kamikaze.local part was messing it up so I tried something like this "[http://127.0.0.1:8080/?action=stream](http://127.0.0.1:8080/?action=stream)" Using the IP of my Octoprint (not the example I listed). When I try it this way I lose the webcam feature all together? No errors show up, just no picture?



I know the IP is correct because I have it set as a fixed IP address and it is how I log into Octoprint.



Anyone else run into this?





**Tim Curtis**

---
---
**Jon Charnas** *September 19, 2017 11:29*

So you need to point the webcam feed using the IP address within octoprint for it to work on machines which can't resolve kamikaze.local. It's basically a DNS/port forwarding issue to access it from outside your network.



I run a Fritzbox modem with a dyn-dns like service. So what I did is I setup port forwarding for port 80 for Octoprint, and for port 8080 for the mjpg-streamer video.



My entries are basically :

external-dns:80 -> beagleboneIP:80

external-dns:8080 -> beagleboneIP:8080



and if you can access the external DNS name from within your network, you can actually set the webcam URL in octoprint to be

[http://external-dns:8080/?action=stream](http://external-dns:8080/?action=stream)



This has worked for me and I'm able to see the webcam in OctoPrint from outside my network.


---
**Tim Curtis** *September 19, 2017 11:55*

**+Jon Charnas**, Thank you. I will try it that way.



I use a DNS service so I should be able to do what you are suggesting.



Question, My Octoprint is set to port 5000 (internally) and I feed it out through port 80 on my router. So would I use these 2 lines instead?

external-dns:5000 -> beagleboneIP:80 (for Octoprint)

external-dns:8080 -> beagleboneIP:8080 (for webcam)


---
**Jon Charnas** *September 19, 2017 12:04*

No, I'd assume it would be

external-dns:80 -> beagleboneIP:5000 if that's what you want to do. However I think the Beaglebone redirects automatically and internally from port 80 to 5000, so try it with a redirect from 80:80. Unless you're using port 5000 externally.


---
**Tim Curtis** *September 19, 2017 12:09*

OK Thanks for the explanation..


---
**Jon Charnas** *September 19, 2017 20:13*

**+Tim Curtis** did you get it working? I'm curious


---
**Tim Curtis** *September 19, 2017 20:14*

Haven't gotten home to try yet. I'll let you know by the morning USA east coast.


---
**Tim Curtis** *September 20, 2017 11:06*

**+Jon Charnas** I haven't gotten it working yet. But its a problem with my router. Port forwarding works for Octoprint, I just have to get the web stream part working. I'll update when I get it working in case anyone else is using a comcast internet router.


---
**Tim Curtis** *September 20, 2017 12:38*

The problem is my router only lets me pick the device I want to port forward. It doesn't let me specify the device port that I want to forward out. Meaning I can only pick my BBB to port forward. I can't pick 10.0.X.X:5000 or 10.0.X.X:8080. There isn't any place to type in the port to read from? I'm contacting Comcast to see what they say. 


---
**Jon Charnas** *September 20, 2017 12:40*

I wish I could help, but in this case, yeah, you need to talk to Comcast. I'm using a FritzBox modem myself, but I live in Switzerland. And from what I remember of my time in the US, Comcast is notorious (among others) for making their appliances basically as useless as they can.


---
*Imported from [Google+](https://plus.google.com/106115959585163180149/posts/L9ZBA59h8de) &mdash; content and formatting may not be reliable*
