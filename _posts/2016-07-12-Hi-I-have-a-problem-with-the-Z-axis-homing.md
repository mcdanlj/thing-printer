---
layout: post
title: "Hi, I have a problem with the Z axis homing"
date: July 12, 2016 15:08
category: "Discussion"
author: Daniel Kusz
---
Hi,

I have a problem with the Z axis homing. When I want to perform a homing Z-axis stepper is not working properly: does not rotate, but makes noise. Only after a few seconds turns several times with a great noise but not going to endstop. I tried to change the current and micro-steps with no effect. Z2 endstop is not connected and is defined as True. Where to find the cause? Can it be some conflict between max. speed for Z axis in printer.cfg (0.02) and homing speed for Z axis in default.cfg  (0.1)? This is my printer.cfg for The Thing:



[System]



[Geometry]

# Thing has H-belt

axis_config = 1



# Set the total length each axis can travel 

travel_x = 0.25

travel_y = 0.25

travel_z = 0.3



# Define the origin in relation to the endstops

offset_x = 0.0

offset_y = 0.0

offset_z = 0.0



# Stepper e is ext 1, h is ext 2

[Steppers]

microstepping_x = 7

microstepping_y = 7

microstepping_z = 7

microstepping_e = 7

microstepping_h = 7



current_x = 0.7	

current_y = 0.7

current_z = 0.9

current_e = 0.9

current_h = 0.9



steps_pr_mm_x = 5.0

steps_pr_mm_y = 5.0

steps_pr_mm_z = 100.0

steps_pr_mm_e = 6.0

steps_pr_mm_h = 6.0



# Set to -1 if axis is inverted

direction_x =  -1

direction_y =  1

direction_z =  -1

direction_e =  -1

direction_h =  1



# Set to True if slow decay mode is needed

slow_decay_x = 1

slow_decay_y = 1

slow_decay_z = 1

slow_decay_e = 1

slow_decay_h = 1



[Cold-ends]

connect-therm-E-fan-0 = True

connect-therm-H-fan-1 = True



[Planner]

# Max speed for the steppers in m/s

max_speed_x = 0.2

max_speed_y = 0.2

max_speed_z = 0.02

max_speed_e = 0.2

max_speed_h = 0.2



[Heaters]

# For list of available temp charts, look in temp_chart.py

# Epcos 10 K

temp_chart_E = B57560G104F

pid_p_E = 0.05

pid_i_E = 0.01

pid_d_E = 0.01

ok_range_E = 2.0



# Epcos 10 K

temp_chart_H = B57560G104F

pid_p_H = 0.05

pid_i_H = 0.01

pid_d_H = 0.01

ok_range_H = 2.0



# Epcos 100 K

temp_chart_HBP = B57560G104F

pid_p_HBP = 0.05

pid_i_HBP = 0.01

pid_d_HBP = 0.01

ok_range_HBP = 4.0



# Invert = False means endtop is connected as Normaly Open (NO)

[Endstops]

invert_X1 = True

invert_X2 = True

invert_Y1 = True

invert_Y2 = True

invert_Z1 = True

invert_Z2 = True



end_stop_X1_stops = x_cw, y_cw

end_stop_X2_stops = x_ccw, y_ccw

end_stop_Y1_stops = y_ccw, x_cw

end_stop_Y2_stops = y_cw, x_ccw 

end_stop_Z1_stops = z_cw



[Homing]

home_x = 0.1

home_y = 0.1





**Daniel Kusz**

---
---
**Ryan Carlyle** *July 12, 2016 19:51*

First thing I would check is that both coils are wired properly. (Loose crimps, broken wiring, etc.) It sounds like only one coil is receiving current from the driver.


---
**Daniel Kusz** *July 12, 2016 19:56*

OK I check it out. I would add only that the engine is running normally outside the function Homing Z. Problems arise only when the Z axis homing operation begins.


---
**Ryan Carlyle** *July 12, 2016 19:59*

Oh, if it runs normally outside homing, then that's probably not a wiring issue :-)



Other things to consider are homing way too fast, or hitting a resonant frequency for the motor when you home. Different homing speeds may help.


---
*Imported from [Google+](https://plus.google.com/103041373788524358316/posts/1krdi15uT8E) &mdash; content and formatting may not be reliable*
