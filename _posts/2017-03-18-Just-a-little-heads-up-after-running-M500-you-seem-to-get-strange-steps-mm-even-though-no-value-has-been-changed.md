---
layout: post
title: "Just a little heads up, after running M500 you seem to get strange steps/mm even though no value has been changed"
date: March 18, 2017 12:17
category: "Discussion"
author: Leo Södergren
---
Just a little heads up, after running M500 you seem to get strange steps/mm even though no value has been changed. 





**Leo Södergren**

---
---
**Jon Charnas** *March 18, 2017 13:04*

On what version of Kamikaze/Redeem? I know there are several users on 2.1.0 with master and you're the first report I've had of this...


---
**Leo Södergren** *March 18, 2017 13:09*

I updated to the latest ~2 weeks ago ( so i guess i have the latest?). I also got some delta settings even though the printer is Cartesian. 


---
**Jon Charnas** *March 18, 2017 13:22*

The delta settings part isn't new, but it's not going to be a problem for your configuration either. I think it's a bug in the octoprint_redeem plugin more so than in Redeem, but I can't remember. I've seen discussions on that topic in Slack though.


---
**Elias Bakken** *March 18, 2017 19:36*

Hmm, as far as I know, the M500 does not change the steps pr mm values for the steppers, so I think something else must have been altered. 


---
**Leo Södergren** *March 18, 2017 21:54*

Here's my local.cfg file before M500

![images/923d9f0ef13130cb9654124b8aabd336.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/923d9f0ef13130cb9654124b8aabd336.png)


---
**Leo Södergren** *March 18, 2017 21:58*

And after. Last time my E-stepper values got changed which made the stepper just skip steps since the acceleration became to high.

![images/df2f739f057f84f450e0501deb4dcb16.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/df2f739f057f84f450e0501deb4dcb16.png)


---
*Imported from [Google+](https://plus.google.com/107257105792966201860/posts/5zuq6erbrtS) &mdash; content and formatting may not be reliable*
