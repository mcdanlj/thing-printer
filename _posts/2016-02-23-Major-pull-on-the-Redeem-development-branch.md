---
layout: post
title: "Major pull on the Redeem development branch!"
date: February 23, 2016 00:26
category: "Discussion"
author: Elias Bakken
---
Major pull on the Redeem development branch! **+Daryl Bond** has done some big work on moving the delta calculations into C++. This has been merged into the development branch and needs testing before being accepted into the main branch. Feel free to test and comment, both for Cartesian, CoreXY and delta users! I'll be in NY all week, so sadly I wont be able to join the merge party, but I'll be there in spirit :)





**Elias Bakken**

---
---
**Daryl Bond** *February 23, 2016 01:57*

I second the "please test" request. Any comments are very welcome as well :)



The update should also allow for shorter move buffer times, due to faster processing of move commands, so try setting "max_buffered_move_time" to a lower value. This should give a more responsive printer when you are trying to pause or cancel a print job.


---
**Jon Charnas** *February 24, 2016 10:18*

Daryl, I'll check it out as soon as I can get my local.cfg properly figured out :) In the meantime, for those who want to try it out but haven't yet figured out how to install redeem from source: [https://bitbucket.org/intelligentagent/redeem/overview](https://bitbucket.org/intelligentagent/redeem/overview)


---
**Daryl Bond** *February 24, 2016 11:00*

Thanks Jon, good luck with the .cfg!



Just a heads up that I may have already found a bug :( I seem to be losing Z height on my delta as I print. For example I air print (no extrusion, bed out of the way) and after a layer or two I end up lower than I started! I'm trying to track it down but just a heads up that this is the 'develop' branch and so there is a chance that it isn't working properly just yet.



Edit: Fingers crossed but I may have been jumping to conclusions. I think it was actually my 'flying extruder' pulling on my carriages too tightly and causing missed steps. So a hardware fault rather than software. Phew!


---
**Daryl Bond** *March 06, 2016 22:44*

**+Flo Ri Han**​ you're right, I haven't seen it yet, I'll get onto it ASAP! Thanks for letting me know.


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/Px96ByhQmdf) &mdash; content and formatting may not be reliable*
