---
layout: post
title: "Baseline PID Autotune. What you see in this picture is the result of running an M303 with no additional parameters on a Hexagon hot end with no fan"
date: May 13, 2016 15:06
category: "Discussion"
author: Elias Bakken
---
Baseline PID Autotune. What you see in this picture is the result of running an M303 with no additional parameters on a Hexagon hot end with no fan. That causes a 3 cycle Åström-Hägglund (relay) tuning around 100 degrees. Following that, the temperature is set at 180 degrees. The overshoot is too high and there is some ringing after. 

![images/b4a1edaee6cd871e54c9b65f5ba995c8.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/b4a1edaee6cd871e54c9b65f5ba995c8.png)



**Elias Bakken**

---
---
**Jon Charnas** *May 19, 2016 09:55*

Stupid question maybe, but wouldn't you want the PID autotune to be run around the usual printing temperature you're going to be running at? So if you print PLA/ABS around 210/220C, don't autotune at 100C but at 220, no?


---
**Elias Bakken** *May 19, 2016 10:09*

Yes, but the auto tune was set for 100 by default. 


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/WrGHbRQEdYp) &mdash; content and formatting may not be reliable*
