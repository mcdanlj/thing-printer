---
layout: post
title: "Hello all I have had my replicape for some time but had put it up waiting for documentation and a community to come about"
date: July 28, 2016 23:45
category: "Discussion"
author: Olahf84
---
Hello all I have had my replicape for some time but had put it up waiting for documentation and a community to come about. I am really glad to see that it has happened.

My question is has anyone developed a config for a core xy design that utilizes a duel z axis that uses the slave h feature? And if so could you please upload it. I have been banging my head against the wall trying to get this board to work. I come from the repetier side of things and this operates similar but different.

Thank you for any help you can provide.





**Olahf84**

---
---
**Elias Bakken** *July 29, 2016 12:41*

The easiest way to figure out how to set up a (quite exotic) printer like this is to experiment with g-codes. Start with the latest kamikaze image, play around with the g-codes for stepper direction, travel, end stops, slave mode etc. g/m-codes are self documenting by adding a "?" at the end, or have a look at the wiki for a complete list. Is it a rev a or b board?


---
**Olahf84** *July 29, 2016 14:40*

Hello Elias, I had an email conversation with you awhile ago when I was having connection issues with the board and 1.1 has seemed to fixed that issue. I am running the rev b board. I have a working heated bed and the extruder is heating so that is all in order. I have a feeling my problem lies with the motor and end stops communicating with each other. For some reason only one of my motors seems to be working when I tell it to move in a direction and switching motors yields the opposite movements. I am using the maxcore xy config as my base but it seems like it is operating like a Cartesian printer.


---
**Philip Acierto** *July 29, 2016 20:14*

Just speaking from experience, I could never get my CoreXY to home correctly with slave H. I ended up wiring both motors to one stepper driver and upped the current. Since doing that my Z has acted much better.


---
**Olahf84** *July 30, 2016 01:35*

That's how I originally had the z wired up but I had been using a ramps board with separate connectors so I figured I would just try the Slave function. My main issue was just getting the x and y to move correctly in Manuel control and to home. Those functions worked before with the config I had but not after 1.1. With 1.1 does the default override the printer specific config? 


---
**Olahf84** *July 30, 2016 05:28*

I was able to get manual control of my x and y axis working but I have a stepper fault on my z now for some reason. The x and y still will ram into the end stops with no regard for them.




---
**Jon Charnas** *July 30, 2016 08:05*

**+Angelo Cochenour**: make sure you checked that you're not running your steppers above 0.6A without cooling - driver faults usually happen because of they're overheating, though there was one case where it occured because of a voltage drop. Remember that the unit for "stepper current" is not a fraction, but actual amps. The maximum current supported is 1.2A (with active cooling!), the maximum safe current without cooling is between 0.5 and 0.6A, depending on how the 'cape is ventilated passively.


---
**Olahf84** *July 30, 2016 14:15*

Thanks Jon I will check that my x and y are set to .5 and seem to like there. I will back the amps back on the other steppers I had them at the default 1 and had it set to 1.5 on my dual motor z axis. I have a 120mm fan blowing across the drivers as well.


---
**Olahf84** *July 31, 2016 05:29*

Finally big break through tonight I have x y homing and my z sensor is detecting my aluminum print bed. But when I run the G29 code I get a horrible noise from my z motors like they are slipping. Every thing is tightened down and the current is set to 1.0.




---
**Luke Ingerman** *October 17, 2016 02:01*

Angelo Cochenour would you be willing to share your config? I am trying to setup my D-Bot CoreXY after having problems with two different Arduino/RAMPS setups and trying to 'learn' Redeem now. I have the Replicape Rev B3A.


---
*Imported from [Google+](https://plus.google.com/101872482691679577480/posts/SgqL6PjVosC) &mdash; content and formatting may not be reliable*
