---
layout: post
title: "So it finally arrived in the UK and now I am trying to wire up a set of FSR pressure sensors to the replicape"
date: April 27, 2016 18:24
category: "Discussion"
author: Pieter Koorts
---
So it finally arrived in the UK and now I am trying to wire up a set of FSR pressure sensors to the replicape. Trying to be ultra cautious and not blow something up but how would this hook to the replicape z-min endstop?



The FSR board gives a constant 3.3v when NOT triggered on the sense pin but when triggered (pressing on it) then it goes to zero volts. Measuring the z-min pins on the replicape one pin shows 5v so I am assuming the other is ground.



Would the sense pin simply go to the 5volt pin on z-min and the two grounds link? Rev1.2 is the board I am using from here [https://github.com/johnsl/fsr_endstop](https://github.com/johnsl/fsr_endstop) which seems to indicate 3 pin endstop connections.



EDIT: I am just full of fail today, was looking at the Rev A board and not Rev B. Still is one of the pins on the 3pin socket the best for the sense output? [http://wiki.thing-printer.com/index.php?title=Replicape_rev_B](http://wiki.thing-printer.com/index.php?title=Replicape_rev_B)





**Pieter Koorts**

---
---
**Elias Bakken** *April 27, 2016 19:30*

Your board can probably run on 5V as well, in which case you can simply use any end stop. But double check that.


---
**Pieter Koorts** *April 27, 2016 20:39*

Okay so after holding my breath and not blowing anything. I have used the inductive end but should work with the others. The FSR board sense goes to yellow(sense) on the replicape and the positive and negative to red and black. The FSR's work a treat and super sensitive. The hot end just touches the glass and it immediately triggers.



Only use the 5 volt on the inductive plug if you use it. Ignore the 12 volt pin.


---
**Elias Bakken** *April 27, 2016 22:44*

Great! Post a video!




---
**Pieter Koorts** *April 27, 2016 22:46*

Just need to tidy it up a little, wires every where and looks like a birds nest. Will post a video of it soon.


---
**Pieter Koorts** *May 04, 2016 20:02*

I know its still a wire mess but it did look worse before this. Replicape is not in the shot as it is underneath the printer.




{% include youtubePlayer.html id=2eWT3e-1BXc %}
[https://www.youtube.com/watch?v=2eWT3e-1BXc](https://www.youtube.com/watch?v=2eWT3e-1BXc)


---
**Elias Bakken** *May 04, 2016 21:15*

Nice!


---
**Mathias Giacomuzzi** *August 06, 2016 23:23*

Hello Pieter, Do you have any video? Is FSR working with replicape?




---
**Step Cia** *September 16, 2016 05:58*

Nice to know FSR is working with replicape. How many FSR did you end up using?


---
**Pieter Koorts** *September 16, 2016 06:00*

Being a delta it is 3 under the bed with the effector pressing down.


---
**Step Cia** *September 16, 2016 06:03*

I'm actually surprise that it triggers almost instantly in your case which gives me hope for mine :) did you have to turn the FSR off while printing?


---
**Pieter Koorts** *September 16, 2016 10:05*

The movement can cause issues so the FSR is only used while checking. It is set in firmware to be ignored while printing.


---
**Step Cia** *September 18, 2016 06:05*

If you have the FSR already pinched before the nozzle pushed the bed, does it cause any confusion where the controller think that the FSR has been triggered from the get go?


---
**Pieter Koorts** *September 18, 2016 06:53*

**+Step Cia** the weight of the bed just provides a light preload so helps triggering. The binder clips are actually not pushing down on the glass but just holding the glass centred.


---
**Step Cia** *September 18, 2016 12:56*

Thx i was wondering how much per loading to apply it seems the more the better as it became more sensitive. Is there a setting in redeem to tell how much preloading of the FSR or redeem doesn't care if this is FSR?


---
**Pieter Koorts** *September 18, 2016 12:59*

The replicape is just using the standard triggering like a micro-switch because i use a separate trigger board for the FSR sensors. 



[http://cdn2.bigcommerce.com/server5100/9oviwe/products/389/images/1096/fsr_controller__28817.1447184109.1280.1280.jpg?c=2](http://cdn2.bigcommerce.com/server5100/9oviwe/products/389/images/1096/fsr_controller__28817.1447184109.1280.1280.jpg?c=2)



The jumpers adjust the sensitivity and if you need ultra precise triggering then the firmware can be reflashed.


---
**Step Cia** *September 18, 2016 18:13*

Ok I didn't realize separate trigger board for FSR is needed.... I'll try to look for it thx


---
**Pieter Koorts** *September 18, 2016 18:15*

Not exactly "needed" but like inductive probes it save a whole headache having it. (E.g) having it built into the replicape for inductive probes.


---
**Step Cia** *September 24, 2016 17:19*

Just got the same FSR board. Where did you put your jumper? The lower number should indicate more sensitive right?


---
**Pieter Koorts** *September 24, 2016 17:34*

**+Step Cia** [https://github.com/JohnSL/FSR_Endstop](https://github.com/JohnSL/FSR_Endstop)



the link is to the github design and readme. down the bottom of the page it shows the jumper options.


---
*Imported from [Google+](https://plus.google.com/100077479073911242630/posts/Sy3DYMHiTbK) &mdash; content and formatting may not be reliable*
