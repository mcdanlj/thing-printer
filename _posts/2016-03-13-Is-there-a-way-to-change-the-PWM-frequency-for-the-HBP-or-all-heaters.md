---
layout: post
title: "Is there a way to change the PWM frequency for the HBP or all heaters?"
date: March 13, 2016 21:35
category: "Discussion"
author: Ante Vukorepa
---
Is there a way to change the PWM frequency for the HBP or all heaters? The PWM is making my heated bed sing me the songs of its people. Very loudly.





**Ante Vukorepa**

---
---
**Elias Bakken** *March 13, 2016 21:40*

Yes, but not in the config at the present. You have to look in the initialization og Redeem: /usr/lib/python2.7/dist-packages/redeem/Redeem.py There should be a line with pwm frequency, but keep in mind that this also affects the DAC. You can also add an issue to Redeem to get that setting read from the config.


---
**Elias Bakken** *March 13, 2016 21:42*

You can also consider setting it to on-off control, if it is a large heat block.


---
**Ante Vukorepa** *March 13, 2016 21:51*

Ah! Cool. I'll set it to on-off, i think that should be just fine. It's a PCB bed, but for some reason, it's insanely loud. I'm guessing it's an accidental resonance, because it gets order of magnitude quieter when i touch the bed terminals.


---
**Chris Romey** *March 18, 2016 18:45*

Was this the noise you were getting? 
{% include youtubePlayer.html id=lffWafdt92I %}
[https://www.youtube.com/watch?v=lffWafdt92I](https://www.youtube.com/watch?v=lffWafdt92I)



My heater started to sing after putting a 200W silicone heater in place of the reprap pcb.


---
**Ante Vukorepa** *March 18, 2016 19:01*

Very similar, yes. Loud as hell. I've since switched to on-off mode, it keeps the temps perfectly stable anyways, since the bed (at least mine) has a very high "momentum".


---
**Marcos Scriven** *June 02, 2016 09:51*

Ahh - onoff_HBP silent bliss, thanks.


---
*Imported from [Google+](https://plus.google.com/105486978634901788220/posts/R7nkDC4WAQP) &mdash; content and formatting may not be reliable*
