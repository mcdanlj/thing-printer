---
layout: post
title: "In case you missed it, I'm adding a link to the Review Thomas Sanladerer did on Replicape"
date: March 21, 2015 09:36
category: "Discussion"
author: Elias Bakken
---
In case you missed it, I'm adding a link to the Review Thomas Sanladerer did on Replicape. He loves the hardware, but feels the firmware is lacking. I agree with him and so getting a working Redeem version 1.0 is a big priority now as well as documenting the features available. 





**Elias Bakken**

---
---
**Jimmy Edwards** *March 21, 2015 14:41*

How can the community help?


---
**Elias Bakken** *March 21, 2015 18:10*

Updating the wiki, either thing-printer wiki or the rep-rap wiki would be great!



[http://wiki.thing-printer.com/index.php?title=Redeem](http://wiki.thing-printer.com/index.php?title=Redeem)



[http://reprap.org/wiki/Redeem](http://reprap.org/wiki/Redeem)


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/XCrrTbScaj4) &mdash; content and formatting may not be reliable*
