---
layout: post
title: "Does anyone know what the max current for the heated bed mosfet is?"
date: February 22, 2016 19:12
category: "Discussion"
author: Tim Curtis
---
Does anyone know what the max current for the heated bed mosfet is? My bed heater pulls 180 watts. I Have one of the new Rev B boards.





**Tim Curtis**

---
---
**Elias Bakken** *February 22, 2016 19:22*

240W is the current limitation imposed by the 20A fuse, but both the mosfet and the tracks will handle more than that if properly ventilated. 


---
**Elias Bakken** *February 22, 2016 19:22*

This is for 12V. For 24V you can double that. 


---
**Tim Curtis** *February 22, 2016 19:24*

Ok Thanks Elias, I am using 12 volts and pulling 15 amps. Just wanted to make sure.


---
*Imported from [Google+](https://plus.google.com/106115959585163180149/posts/AU2nZt5LNyz) &mdash; content and formatting may not be reliable*
