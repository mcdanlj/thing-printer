---
layout: post
title: "I'm re-posting a discussion on implementing the M109 from reprap forums:"
date: February 11, 2014 13:23
category: "Discussion"
author: Elias Bakken
---
I'm re-posting a discussion on implementing the M109 from reprap forums: [http://forums.reprap.org/read.php?146,303976](http://forums.reprap.org/read.php?146,303976)





**Elias Bakken**

---
---
**James Armstrong** *February 11, 2014 15:04*

Look at the Repetier code, it may give some insight. Looks like they wait for the current buffer to drain (complete all moves) then set the temperature and wait in a loop for the temperature to be hit and report the current temperature every second.



[https://github.com/repetier/Repetier-Firmware/blob/master/src/ArduinoAVR/Repetier/Commands.cpp#L928](https://github.com/repetier/Repetier-Firmware/blob/master/src/ArduinoAVR/Repetier/Commands.cpp#L928)


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/bityFnLq4bw) &mdash; content and formatting may not be reliable*
