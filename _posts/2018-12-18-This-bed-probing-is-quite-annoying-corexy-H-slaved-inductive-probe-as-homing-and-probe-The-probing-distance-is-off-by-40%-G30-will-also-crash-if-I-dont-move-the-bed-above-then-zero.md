---
layout: post
title: "This bed probing is quite annoying. (corexy, H slaved, inductive probe as homing and probe) The probing distance is off by +40% G30 will also crash if I don't move the bed above, then zero"
date: December 18, 2018 16:46
category: "Discussion"
author: Derek Kuraitis
---
This bed probing is quite annoying.

(corexy, H slaved, inductive probe as homing and probe)

The probing distance is off by +40%

G30 will also crash if I don't move the bed above, then zero.

g0 z5, g92

I'm trying to go through the code. I'm a novice at programming but I want this bed leveling working! :D

I guess it's still possible I've made a mistake in config somewhere..





**Derek Kuraitis**

---
---
**Elias Bakken** *December 18, 2018 16:54*

It's difficult to make a G29 that works for all printers, so it needs to be very flexible. That also makes it a bit difficult to configure, but it should be possible with some trial and error. 


---
**Jon Charnas** *December 18, 2018 17:42*

Check G29.2 to generate a probing routine that works for you.


---
**Derek Kuraitis** *December 18, 2018 17:45*

I just about made one but I guess I have to fudge the offsets to get it to work. I was just using the default 3 points but was hoping to solve the odd: distance moved is not equal to distance probed. The default 10mm = -14.14mm probed distance


---
**Derek Kuraitis** *December 18, 2018 17:46*

I'm updating everything right now, see if that changes anything. I'm reading as much as I can on github


---
**Jon Charnas** *December 18, 2018 17:57*

That's in your steps per millimeter settings you want to change it


---
**Derek Kuraitis** *December 18, 2018 18:27*

I'm using m114 as reference. They should be exactly the same, steps_per_mm wouldn't matter. In debug it says 4000 steps, which is correct.

steps per mm * micro * 10mm

25*16*10


---
**Derek Kuraitis** *December 18, 2018 19:04*

Well.... Good news. The RC branch fixed it. 2.2.dev0+69.rc.g7637fc8

If I move 5, it reads -4.94mm. Perfect.


---
**Jon Charnas** *December 18, 2018 23:19*

Be aware that the RC branch is getting more updates and we'll try to put out an image with it soon!


---
**Derek Kuraitis** *December 19, 2018 10:44*

Yeah, I saw that the endstop alarms has been changed too. I had used two optical endstops to stop the rotation of Z and H so I can level the bed. One side of the bed slouches. Now I cant because it pauses moves. There's so many printers with dual Z motors but It's always a hassle..


---
**Derek Kuraitis** *December 19, 2018 10:45*

Maybe I'll find some way to use the inductive probe to level it.. More experimenting..


---
**Jon Charnas** *December 19, 2018 10:48*

Well, you can still do that if Z & H are not slaved... Z1_stops = z_ccw, Z2_stops=h_ccw should work?



There's an M-code command to activate slaving too - maybe make a custom G29 routine that decouples H & Z, sets the Z1 and Z2 triggers as above, then re-slave and "standardize" your Z1 and Z2 triggers?


---
**Derek Kuraitis** *December 19, 2018 11:40*

Yeah, I might do that. I also just bought some bearing blocks. I was just thinking about belting the two leadscrews together and sneak it under the bed so I don't have to suffer syncing the the two motors. It'll save a lot of time too I bet. I kinda wish there was a bed compensation that would sync the two motors based on the readings it got and some geometry inputs.


---
*Imported from [Google+](https://plus.google.com/114326755084967855066/posts/74HurTcoBv8) &mdash; content and formatting may not be reliable*
