---
layout: post
title: "Updated redeem (apt update) today and the printer decided it was going to keep the motors non-stop ramming into the bed"
date: May 05, 2016 17:16
category: "Discussion"
author: Pieter Koorts
---
Updated redeem (apt update) today and the printer decided it was going to keep the motors non-stop ramming into the bed. Luckily current was low so no damage but noticed this error in logs.



homing_only_endstops = self.printer.config.get('Endstops', 'homing_only_endstops')

ConfigParser.NoOptionError: No option 'homing_only_endstops' in section: 'Endstops'



temperature_charts.load(printer.config.get("System", "data_path"))

ConfigParser.NoOptionError: No option 'data_path' in section: 'System'



Is "homing_only_endstops" a required option now or have I inadvertently stumbled on a bug?





**Pieter Koorts**

---
---
**Daryl Bond** *May 05, 2016 21:46*

It sounds like your default cfg file hasn't been updated correctly. Should the package in the feed be checked **+Elias Bakken**? You could try doing the update again (apt-get update; apt-get install redeem) or you could replace your /etc/redeem/default.cfg with this one: [https://bitbucket.org/intelligentagent/redeem/raw/b767c9b5328b37ca5ea4657fdaa9781b0bcaa430/configs/default.cfg](https://bitbucket.org/intelligentagent/redeem/raw/b767c9b5328b37ca5ea4657fdaa9781b0bcaa430/configs/default.cfg)


---
**Pieter Koorts** *May 05, 2016 21:48*

Yeah, just was replying when your message popped up. I copied the new default.cfg from the bitbucket repo so it looks like it has not filtered through the feed. Though not sure if it should but I certainly don't touch default.cfg out of habit and just override.


---
**James Armstrong** *May 06, 2016 18:37*

One thing I notice is redeem hates when new configs are added but not found. Seems like it could maybe use defaults for the new variable then log, popup or show on toggle that you need to edit your config to use the new features that were added instead of just stopping. At least I think I remember redeem just ending if a configuration entry is not found. Maybe gracefully log and end if it is a critical entry but a non-critical new config can use defaults and continue.




---
**Daryl Bond** *May 07, 2016 01:12*

I think what you described is already implemented to some degree. There is a default configuration that holds a setting for every option. This will catch everything that is configurable and set a default value. If an option is added to Redeem then this default obviously needs to be updated as well. In this case it appears that the update of the default cfg didn't happen at the same time as the rest of Redeem was updated. The problem here seems, to me, to be more of not having the installation/update procedure being as robust as it should have been.


---
*Imported from [Google+](https://plus.google.com/100077479073911242630/posts/DGVvxfeutNg) &mdash; content and formatting may not be reliable*
