---
layout: post
title: "Elias Bakken what is the best place to get the absolute latest BBB image with replicape?"
date: January 14, 2016 16:33
category: "Discussion"
author: James Armstrong
---
**+Elias Bakken** what is the best place to get the absolute latest BBB image with replicape? I have the new B board and want to get it downloaded. I remember seeing a mention recently of trying to get a new image with the latest kernel going.





**James Armstrong**

---
---
**Elias Bakken** *January 14, 2016 22:26*

James, I'm putting the final touches on the image now. I'll upload as soon as I have verified it working!


---
**Elias Bakken** *January 14, 2016 23:30*

You can grab the latest image from here if you want, I'll work on the instructions for this: [http://kamikaze.thing-printer.com/images/](http://kamikaze.thing-printer.com/images/)


---
**James Armstrong** *January 15, 2016 00:46*

Ok. Got the new image and for some reason it will not flash the MMC. Verified the user boot button is held down for a few seconds after the 4 led's come on. We are trying and older image now as we don't have a good uEnv to compare to. 


---
**Elias Bakken** *January 15, 2016 00:56*

Hm.. Ok, I'll try to download the file that was uploaded and flash it myself to see what is wrong, but it's 2 o'clock here, so it might have to wait until tomorrow!


---
**James Armstrong** *January 15, 2016 01:16*

Never mind. Apparently the led pattern is deferent than I am use to while burning. The cylon pattern is while writing where before it use to flash a few led's for a long time. 


---
**Elias Bakken** *January 15, 2016 01:38*

Yeah, it's pretty slow to start, but if you have a Manga screen, you can connect it to get a splash screen during flashing.


---
**James Armstrong** *January 15, 2016 01:59*

Ok. Next question. With the December image we saw the login prompt with HDMI but when we went to the latest image the BBB seems to be in some sort of loop after the kamakazi splash goes away (led's D2/D3 would toggle a few times the D4 would blink on then repeat) but we had no HDMI. 


---
**Elias Bakken** *January 15, 2016 02:07*

It should halt after a few minutes after  flashing. All leds solid.


---
**James Armstrong** *January 15, 2016 02:10*

This is after the flash and removal of the SD. The HDMI screen shows the boot splash then it goes away and the monitor goes into sleep mode. We can't access the terminal to get the IP address. This is with the B3 cape plugged in. Without the cape the led's change but (I think it constantly reboots without the cape). 


---
**Elias Bakken** *January 15, 2016 02:24*

Hm... ill have to flash it myself to see if there is a difference between my flasher and the one I uploaded. It could be a Uboot panic due to wrong memory access... can't know without a serial connected.


---
**James Armstrong** *January 15, 2016 02:36*

The 12-23-15 image gives us a login so we will play with that for now. 


---
**Dave Posey** *January 15, 2016 05:06*

Has anyone tried the latest image with an original BeagleBone Rev A5?  



I can't seem to get mine to boot a Kamikaze image.  



I've tried all 3 of the posted images and they act the same way.  I get a flash of all 4 LED above the USB connector and then it never does anything else.



Update:  I have tried the standard BeagleBone Jessie, Debian 8.2  image and that boots fine.  


---
**Elias Bakken** *January 15, 2016 13:08*

James, I've tried the procedure and it works for me! I've written down what to expect.

[http://wiki.thing-printer.com/index.php?title=Kamikaze#Flashing_procedure](http://wiki.thing-printer.com/index.php?title=Kamikaze#Flashing_procedure)


---
**James Armstrong** *January 15, 2016 14:01*

Yes, we figured out the boot process and are past that. The kernel seemed to go weird with the latest image and does not show anything on the screen after the boot splash, the screen ends up going to sleep. Blinking lights and no ethernet or anyway to see what the ip is. With the 12-23-2015 image we get past the boot splash and get the login prompt and all is good except octoprint would;t find any serial ports but we left it at that point. No terminal on the latest image unless there is a configuration setting that is wrong that keeps the screen from showing.


---
**James Armstrong** *January 15, 2016 14:41*

Ok. Here are some logs from the latest image. It does boot and I get a prompt using the serial port only. The (hdmi) screen just does not ever come on. The dmesg log shows something about a display service not being found.



[https://www.dropbox.com/s/3ii9hw39n17emgt/redeem-010515.zip?dl=0](https://www.dropbox.com/s/3ii9hw39n17emgt/redeem-010515.zip?dl=0)



and ps dump:

[https://www.dropbox.com/s/3plv0mluxpkyy1u/ps-010516.rtf?dl=0](https://www.dropbox.com/s/3plv0mluxpkyy1u/ps-010516.rtf?dl=0)



Looks like octoprint and toggle are running but not redeem. I will run that manually and see what it complains about.


---
**James Armstrong** *January 15, 2016 15:34*

debian@kamikaze:/usr/lib/python2.7/dist-packages/redeem$/usr/bin/redeem

Traceback (most recent call last):

  File "/usr/bin/redeem", line 9, in <module>

    load_entry_point('Redeem==1.1.4', 'console_scripts', 'redeem')()

  File "/usr/lib/python2.7/dist-packages/pkg_resources.py", line 356, in load_entry_point

    return get_distribution(dist).load_entry_point(group, name)

  File "/usr/lib/python2.7/dist-packages/pkg_resources.py", line 2476, in load_entry_point

    return ep.load()

  File "/usr/lib/python2.7/dist-packages/pkg_resources.py", line 2190, in load

    ['__name__'])

  File "/usr/lib/python2.7/dist-packages/redeem/Redeem.py", line 39, in <module>

    from Stepper import *

  File "/usr/lib/python2.7/dist-packages/redeem/Stepper.py", line 28, in <module>

    from ShiftRegister import ShiftRegister

  File "/usr/lib/python2.7/dist-packages/redeem/ShiftRegister.py", line 40, in <module>

    spi.bpw = 8

AttributeError: 'NoneType' object has no attribute 'bpw'


---
**James Armstrong** *January 15, 2016 15:43*

Does this mean anything:



[    3.228528] bone_capemgr bone_capemgr: Baseboard: 'A335BNLT,000C,2314BBBK0772'

[    3.228555] bone_capemgr bone_capemgr: compatible-baseboard=ti,beaglebone-black - #slots=4

[    3.267020] bone_capemgr bone_capemgr: Invalid signature 'ffffffff' at slot 0

[    3.274208] bone_capemgr bone_capemgr: slot #0: No cape found

[    3.332473] bone_capemgr bone_capemgr: slot #1: No cape found

[    3.392470] bone_capemgr bone_capemgr: slot #2: No cape found

[    3.452471] bone_capemgr bone_capemgr: slot #3: No cape found

[    3.458303] bone_capemgr bone_capemgr: initialized OK.


---
**James Armstrong** *January 15, 2016 15:44*

**+Elias Bakken** did you ever get a slack channel going?  I see a [replicape.slack.com](http://replicape.slack.com) but don't have access.


---
**James Armstrong** *January 15, 2016 18:05*

FYI, I had to make the B3 EEPROM image and write to the NVRAM before the BBB would recognize the replicape. It would appear that the eeprom was not written on it or something. Now I am at least getting:



[    3.267032] bone_capemgr bone_capemgr: slot #0: 'Replicape 3D printer cape,00B3,Intelligent Agen,BB-BONE-REPLICAP'



and 



root       297     1  3 18:02 ttyGS0   00:00:06 /usr/bin/python /usr/bin/redeem


---
**James Armstrong** *January 15, 2016 18:05*

still working on why HDMI isn't working.


---
**James Armstrong** *January 15, 2016 23:35*

So far two of the new replicapes from the factory are coming without the EEPROM flashed and redeem crashes until it is written. One person has been unsuccessful writing the EEPROM like I was. Always gets permission problem even with sudo no matter what we try. 


---
*Imported from [Google+](https://plus.google.com/+JamesArmstrong3/posts/AP8vJjxnTBU) &mdash; content and formatting may not be reliable*
