---
layout: post
title: "I am looking again at BLTouch, and found there is now a wiki page on it: The information is very confusing, it says it does work on development branch, but next lines it says it doesn't work?"
date: January 18, 2018 23:43
category: "Discussion"
author: Philip Acierto
---
I am looking again at BLTouch, and found there is now a wiki page on it:



[http://wiki.thing-printer.com/index.php?title=BLTouch](http://wiki.thing-printer.com/index.php?title=BLTouch)



The information is very confusing, it says it does work on development branch, but next lines it says it doesn't work? Says you can't home Z, but you can probe... so how does a Cartesian (CoreXY personally) printer use this?



I am using Umikaze 2.1.2rc3, so does that mean I can just buy a bltouch, set it up according to the wiki, and homing will work for me now? Or is arguably the best probe on the market still not fully implemented?





**Philip Acierto**

---
---
**Jon Charnas** *February 08, 2018 15:45*

As far as I can tell, the wiki instructions should let you make it work. I think there's a few devs on the Replicape Slack that have one or have used one, so the support is there, but the instructions are probably out of date.


---
**Philip Acierto** *February 09, 2018 21:01*

"On Replicape/Redeem, BLTouch is support for probing but not homing because it only sends pulses. Homing is theoretically possible but currently not implemented - feel free to file an issue at the issue tracker if this is something you could use."



that was the misleading part, yet the paragraph above says with the new PRU firmware it "should" work, then this comes directly afterwards.



If it can probe, but not home... that makes it pretty useless for a Cartesian, where we home on Z min, not max like a delta. The probe and home uses the same end stop switch (in this case BL Touch if I could figure out how to make it work), so we can't have one but not the other.


---
*Imported from [Google+](https://plus.google.com/111974602640488998735/posts/1d1LmaiLmBv) &mdash; content and formatting may not be reliable*
