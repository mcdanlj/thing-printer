---
layout: post
title: "I have a manga screen 2(1080x1920). and Hook up to my RPi 3 running retropie 4.4"
date: July 04, 2018 01:50
category: "Discussion"
author: 박세희
---
I have a manga screen 2(1080x1920). and Hook up to my RPi 3 running retropie 4.4. I managed to get my manga screen 2 to work with full resolution. but I cannot change resolution with configurations below(Same with last version of raspbian image.)



hdmi_cvt=1080 1920 60 5 0 0 0

hdmi_group=2

hdmi_mode=87

display_rotate = 1



I can only get it to work with 



hdmi_timings=1080 1 100 10 60 1920 1 4 2 4 0 0 0 60 0 144000000 3

display_rotate = 1



I can just change the resolution with framebuffer_width/height on the raspbian. 



But on the retropie, framebuffer_width/height doesn't work for me.





**박세희**

---


---
*Imported from [Google+](https://plus.google.com/103586355598270852902/posts/i6hFHdESqgR) &mdash; content and formatting may not be reliable*
