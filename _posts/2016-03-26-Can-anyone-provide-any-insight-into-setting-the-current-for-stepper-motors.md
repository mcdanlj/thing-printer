---
layout: post
title: "Can anyone provide any insight into setting the current for stepper motors?"
date: March 26, 2016 02:02
category: "Discussion"
author: William Burton
---
Can anyone provide any insight into setting the current for stepper motors?  I have a Rated Current/Phase of 1.5A for the extruder

And for the X, Y, Z Rated Current/Phase is 2.8A







**William Burton**

---
---
**James Armstrong** *March 26, 2016 02:37*

It really should be explained better in the file because it isn't what you think. I believe the drivers can only handle 1.2 amps (not sure) and it isn't a one to one current setting. I think it is RMS or half the current because .5 works just fine where I would normally use 1.2


---
**William Burton** *March 27, 2016 19:58*

With the drivers max of 2.5A but RMS is 1.2A, your are saying the max value in the config files should never be above 1.2?  and the value you are setting in the configuration file is the current/phase value of the motor in question. Thanks again for the information. 


---
**James Armstrong** *March 27, 2016 23:31*

The question should be answered by Elias. I'm just saying from what I can tell by a few comments and from the fact that if I use more than .5 my motors get really hot when I would typically use more than .5 on other drivers. 


---
**James Armstrong** *March 27, 2016 23:33*

    def set_current_value(self, i_rms):

        """ Current chopping limit (This is the value you can change) """

        self.current_value = i_rms

        

        r_sense = 0.1020              # External resistors + internal

        sqrt_2 = 1.41421356237



        v_iref = 2.5*(i_rms/1.92)

        if(v_iref > 2.5):

            logging.warning("Current ref for stepper "+[self.name](http://self.name)+" above limit (2.5 V). Setting to 2.5 V")

            v_iref = 2.5

        logging.debug("Setting votage to "+str(v_iref)+" for "+[self.name](http://self.name))

        self.dac.set_voltage(v_iref)




---
*Imported from [Google+](https://plus.google.com/110903600885855713529/posts/jdsKUB2waZY) &mdash; content and formatting may not be reliable*
