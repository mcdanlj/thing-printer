---
layout: post
title: "Is anyone using the \"email notification when print job complete\" set up in octoprint?"
date: September 05, 2017 14:28
category: "Discussion"
author: Tim Curtis
---
Is anyone using the "email notification when print job complete" set up in octoprint? If so, is there anything special to do on the BBB to get it working?





**Tim Curtis**

---
---
**Jon Charnas** *September 05, 2017 14:40*

Not on the BBB, no. You'll need to get all the email settings in OctoPrint's settings tab for it correct though. Check with your email provider and the official plugin's FAQs how to go about that?


---
**Tim Curtis** *September 05, 2017 14:43*

Okay, Thanks I will check the FAQs. Is there a particular version plugin that you recommend or use?


---
*Imported from [Google+](https://plus.google.com/106115959585163180149/posts/6ETThoV76vs) &mdash; content and formatting may not be reliable*
