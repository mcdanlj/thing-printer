---
layout: post
title: "Hey all, Don't know if this is the right place to post or not but what's the average time for shipping for the Replicape?"
date: October 12, 2017 09:38
category: "Discussion"
author: CNC Beaker
---
Hey all,

 Don't know if this is the right place to post or not but what's the average time for shipping for the Replicape?

I am in Australia so I understand it will take longer but i'm at 5 weeks is this normal?

Cheers





**CNC Beaker**

---
---
**Jon Charnas** *October 12, 2017 09:50*

It seems long, yeah, but could customs be getting in your way? I pinged Elias to take a look at it. But you'd be better emailing him about that issue directly. Use the "get in touch" form in his website for that. He should get back to you in a few hours, I think he's still getting over the jetlag from his trip to Colorado


---
**CNC Beaker** *October 12, 2017 10:15*

Thanks Jon, 



Not sure about the customs thing.

Did try the "get in touch" form but no reply. I'm sure there are many emails he has to respond to, will hold out.


---
**Elias Bakken** *October 12, 2017 11:20*

Hi CNC Beaker! What is your order number, I'll check that status!




---
**CNC Beaker** *October 13, 2017 07:20*

Thanks Elias for getting back to me.

Order Number is 2273.

Cheers


---
**Daniel Tan** *October 17, 2017 10:59*

G'day, just for reference my order was sent via Asendia priority, got to me in just under 3 weeks. I am in Melbourne if that matters.


---
**CNC Beaker** *November 06, 2017 10:55*

Thanks all, now have my replicape. Its time to print..


---
*Imported from [Google+](https://plus.google.com/111223679393405421993/posts/17XNN2jA8PD) &mdash; content and formatting may not be reliable*
