---
layout: post
title: "I've done some work on filament sensors"
date: April 23, 2016 16:27
category: "Discussion"
author: Elias Bakken
---
I've done some work on filament sensors. The electronics and software part is working at least, but the mechanics is not yet great. Better solutions welcome : )

[http://www.thing-printer.com/filament-sensor-3d-printer-replicape/](http://www.thing-printer.com/filament-sensor-3d-printer-replicape/)





**Elias Bakken**

---
---
**dstevens lv** *April 23, 2016 18:25*

Packaging the assembly for a direct drive/direct mount extruder is going to be challenging but for a Bowden it's not too far from being able to implement.


---
**Elias Bakken** *April 23, 2016 18:32*

**+dstevens lv** I guess the rotary encoder could be a replacement for the radial bearing pushing against the filament in a direct drive extruder.


---
**Daniel Kruger** *April 23, 2016 21:40*

For the encoders I've bought 111688538683 of flE-Bay to play with. How about soft silicone tube pushed over the encoder shaft with an adjustable spring tension 608 bearing idler like a wade's extruder. For direct drive, a Bowden tube secured to the sensor mechanism and to the extruder should work. I would love to have a woking system like this for my replicape.


---
**Javier Murcia** *April 27, 2016 01:02*

the problem with measuring at the spool is that as the spool is used, it rotates more quickly for the same ammount of filament.



how about this? [http://hackaday.com/2015/06/16/prevent-failed-prints-with-a-filament-speed-sensor/](http://hackaday.com/2015/06/16/prevent-failed-prints-with-a-filament-speed-sensor/)



also, i've seen one tiny sensor wich i couldn't find anymore. It was to samll (maybe 3mm dia) rubber rollers, and one had a magnet glued. Put that inside a little box wich hold them appart at the correct distance, trow in a hall sensor, and you have a jam sensor. i'll try to find it, since it was the simplest and most compact setup i've ever seen.


---
**tizzledawg** *July 30, 2017 03:37*

Is there anything in the future for encoders on the XY steppers? Like maybe a daughter board which if a skipped step has occured, then pass it to redeem to correct it?


---
**Elias Bakken** *July 30, 2017 08:35*

**+tizzledawg** BeagleBone has support for rotary encoders in hardware (counting steps) which I think might be necessary in order to not spend too much resources on the encoders. Or one could go for stepper drivers with support in hardware. 


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/dM7CECAZ3ia) &mdash; content and formatting may not be reliable*
