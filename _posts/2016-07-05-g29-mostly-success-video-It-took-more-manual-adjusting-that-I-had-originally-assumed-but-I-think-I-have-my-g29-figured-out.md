---
layout: post
title: "g29 mostly success video It took more manual adjusting that I had originally assumed, but I think I have my g29 figured out"
date: July 05, 2016 20:30
category: "Discussion"
author: Philip Acierto
---
g29 mostly success video



It took more manual adjusting that I had originally assumed, but I think I have my g29 figured out. I have to do manual probe movements to keep the speed controlled, and I had to manually set the probe points to account for the offset of the probe to the nozzle tip since the WORKING X/Y probe offset is applied to the probe points based on the nozzle position.



If anyone has any questions please feel free to ask, hopefully my comments in the macro help answer the easy one.



Do note my compensation matrix isn't perfect yet, the nozzle height still seems to vary while printing, but I think that is a bug still being worked on as this is still in a developmental/experimental phase. My X/Y is not calibrated yet as my bed is 425mm in itself and the probe points are only as high as 300s, but I am still working towards a first print and calibration cube to get it closer.





[Probe]

length = 0.04              ; distance to probe on Z axis

speed = 0.01              ; no idea doesn't affect anything

accel = 0.1                  ; no idea doesn't affect anything

offset_x = -0.003       ; offset of nozzle to probe point

offset_y = -0.085       ; offset of nozzle to probe point

offset_z = 0.000         ; no idea doesn't affect anything



[Macros]

g29 = 

	M561                	                ; Reset the bed level matrix

	M558 P3                                  ; Set probe type to Servo with switch

	M557 P0 X80 Y85		        ; Set probe point 0 (Bottom Left)

	M557 P1 X80 Y220    	        ; Set probe point 1 (Middle Left)

	M557 P2 X80 Y340	                ; Set probe point 2 (Top Left)

	M557 P3 X230 Y340	        ; Set probe point 3 (Top Middle)

	M557 P4 X380 Y340	        ; Set probe point 4 (Top Right)

	M557 P5 X380 Y220        	; Set probe point 5 (Middle Right)

	M557 P6 X380 Y85        	        ; Set probe point 6 (Bottom Right)

	M557 P7 X230 Y85              	; Set probe point 7 (Bottom Middle)

	M574 Z2 z_neg                       ; Enable endstop Z2 (since inductive sensor probes past nozzle height it has to be disabled under normal conditions)

	G92 Z0                                    ; Reset Z height to 0 so it guarantees positive movement

	G0 Z10 F800                           ; Move Z up to allow space for probe

	G28 X0 Y0          		        ; Home X Y

	G0 X77 Y0 F20000	                ; Move the X Y to safe place to Home Z

	G28 Z0              	                ; Home Z

	G0 Z10 F800                           ; Move Z up to allow space for probe

	G92 Z0                                    ; Reset Z height to 0

	G0 X77 Y0 F20000	        	; Move the X Y manually to Probe Point 0

	G30 P0 S                  	        ; Probe point 0

	G1 Z0 F800                             ; Move the Z up for safe movement

	G0 X77 Y135 F20000		; Move the X Y manually to Probe Point 1

	G30 P1 S                                 ; Probe point 1

	G1 Z0 F800                             ; Move the Z up for safe movement

	G0 X77 Y255 F20000		; Move the X Y manually to Probe Point 2

	G30 P2 S                                 ; Probe point 2

	G0 Z0 F800                             ; Move the Z up for safe movement

	G0 X227 Y255 F20000		; Move the X Y manually to Probe Point 3

	G30 P3 S                                 ; Probe point 3

	G1 Z0 F800                             ; Move the Z up for safe movement

	G0 X377 Y255 F20000		; Move the X Y manually to Probe Point 4

	G30 P4 S                                 ; Probe point 4

	G1 Z0 F800                             ; Move the Z up for safe movement

	G0 X377 Y135 F20000		; Move the X Y manually to Probe Point 5

	G30 P5 S                                 ; Probe point 5

	G1 Z0 F800                             ; Move the Z up for safe movement

	G0 X377 Y0 F20000		; Move the X Y manually to Probe Point 6

	G30 P6 S                                 ; Probe point 6

	G1 Z0 F800                             ; Move the Z up for safe movement

	G0 X227 Y0 F20000		; Move the X Y manually to Probe Point 7

	G30 P7 S                                 ; Probe point 7

	G0 X70 Y70 F20000                ; Speed up moving to near home position

	M574 Z2                                  ; Disable endstop Z2

	G92 Z13.5                               ; Compensate probe offset (and also adding 10mm probe height we set above)

	G0 Z0 F800                             ; Move nozzle to the lowest position

	M561 U                                    ; Apply Bed Compensation Matrix (the whole point of this macro)


**Video content missing for image https://lh3.googleusercontent.com/-LUuWys7buVg/V3wY6Z3tUMI/AAAAAAAAEWE/o4G5Fy28_Bw2hrsCIzaGgcx6RauW-3Ung/s0/VID_20160705_121304.mp4.gif**
![images/37f50a9793f772b8cd347bb81ebd3674.gif](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/37f50a9793f772b8cd347bb81ebd3674.gif)



**Philip Acierto**

---


---
*Imported from [Google+](https://plus.google.com/111974602640488998735/posts/cccvMseFPFq) &mdash; content and formatting may not be reliable*
