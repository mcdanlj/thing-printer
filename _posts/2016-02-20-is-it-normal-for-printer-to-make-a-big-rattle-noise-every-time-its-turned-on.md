---
layout: post
title: "is it normal for printer to make a big rattle noise every time its turned on?"
date: February 20, 2016 21:58
category: "Discussion"
author: Hauvert Pacheco
---
is it normal for printer to make a big rattle noise every time its turned on? also my z axis goes up and down just fine pressing the manual button but when i click home it makes a nasty noise again. any idea why? i also can't seem to ssh into my board. every time i do i get this 

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!

Someone could be eavesdropping on you right now (man-in-the-middle attack)!

It is also possible that a host key has just been changed.

The fingerprint for the RSA key sent by the remote host is

51:bd:d2:e1:fb:ea:1f:4e:84:c2:a0:7d:79:01:10:50.

Please contact your system administrator.

Add correct host key in /Users/admin/.ssh/known_hosts to get rid of this message.

Offending RSA key in /Users/admin/.ssh/known_hosts:3

RSA host key for 192.168.1.11 has changed and you have requested strict checking





**Hauvert Pacheco**

---
---
**James Armstrong** *February 20, 2016 22:00*

Edit your .ssh/knownhosts and go to the line mentioned and delete it. If you ssh'd into a system with the same IP address it will remember it and complain. 


---
**Hauvert Pacheco** *February 20, 2016 22:07*

how can i get to that file? i searched for it but can not find it.

must i always delete every time i want to ssh into it? 


---
**James Armstrong** *February 20, 2016 22:08*

What computer OS are you ssh'ing from?


---
**Hauvert Pacheco** *February 20, 2016 22:10*

mac os 10.9.5


---
**Hauvert Pacheco** *February 20, 2016 22:11*

also, after raising the usteps all the way up theres no more high pitch noise. but my stepper motors are real hot! is that bad?


---
**James Armstrong** *February 20, 2016 22:12*

From the command prompt type in:



vi .ssh/known_hosts (and then enter)

Press :3 (then enter)

Press dd (two d's) the the line should go away. 

Press :wq (then enter)



Try the ssh again. 


---
**Hauvert Pacheco** *February 20, 2016 22:17*

i types vi ssh/knownhosts and got this...



~                                                                               

~                                                                               

~                                                                               

~                                                                               

~                                                                               

~                                                                               

~                                                                               

~                                                                               

~                                                                               

~                                                                               

~                                                                               

~                                                                               

~                                                                               

~                                                                               

~                                                                               

~                                                                               

~                                                                               

~                                                                               

~                                                                               

~                                                                               

~                                                                               

~                                                                               

-- INSERT --


---
**Hauvert Pacheco** *February 20, 2016 22:18*

i tried typing vi. ssh/knownhosts, exactly what you typed but it said command not found

so i tried various combinations and this was the only one that worked. vi ssh/knownhosts


---
**James Armstrong** *February 20, 2016 22:41*

Sorry, I editors the comment right after, use known_hosts, also your current is set too high in your config. You need to figure the correct settings but I would set them all to 0.3 right now until you figure it out. 


---
**James Armstrong** *February 20, 2016 22:42*

**+Hauvert Pacheco** that is 



vi   .ssh/known_hosts


---
**Veridico Cholo** *February 20, 2016 23:25*

**+Hauvert Pacheco** My motors have a current spec of 1.2 Amps per phase, I started setting them to half that: 0.6 

I ended up with x,y set to 0.8, z set to 1.0 (i have two z motors connected together driven by the z driver alone. if I set the z to 1.2 amps, I get an Alarm from redeem) and E up to 1.0



Microstepping for x,y and z is 8 and for e it's 7


---
**Hauvert Pacheco** *February 21, 2016 00:03*

**+James Armstrong** so i delete it the txt but how can i save? if i close the window and try to ssh the same warning appears. i must not be saving it or deleting it right


---
**Andy Tomascak** *February 21, 2016 00:27*

Read this: [http://labs.hoffmanlabs.com/node/1116](http://labs.hoffmanlabs.com/node/1116) to help understand ssh. What you are seeing is caused by flashing the BBB with a new image, resulting in a "new" computer that ssh doesn't trust by default. You can reduce the security (not really recommended) or simply delete the old entry.



As others have said, your motors are running way too much current. Running them hot for too long will damage them.


---
**James Armstrong** *February 21, 2016 01:48*

I'm pretty sure the current is half what you would normally use or at least in RMS. I am running 1.5 amp motors with the 0.5 setting. Elias would have to chime in. I don't think the setting is exactly 1 to 1 (like you said, I would normally run 1 amp or so but entering in 1 is way too high for this setup). 


---
**Veridico Cholo** *February 21, 2016 02:11*

Yes, the current, decay and the microsteps for these drivers is a bit new to me. Specially the microsteps. Being used to the Arduino boards with Pololu drivers and Marlin firmware, I usually do 1/16 micro stepping like everyone else and that's it. These drivers setting of 8, which is 1/16 microstepping, Stealth chop and interpolation down to 256 microsteps is totally new to me.

It's the only setting that eliminates the motors from making a high pitch hissing noise.

It seems to be working fine, though. I have been running a test print for 6 hours and the steppers are warm but not unreasonably. Any guidance about current/microsteps/decay from Elias is always welcome!


---
**Andy Tomascak** *February 21, 2016 02:25*

Try this: [http://dkc1.digikey.com/us/en/tod/STMicroelectronics/StepperFundamentals_NoAudio/Stepper_Fundamentals_NoAudio.html](http://dkc1.digikey.com/us/en/tod/STMicroelectronics/StepperFundamentals_NoAudio/Stepper_Fundamentals_NoAudio.html)


---
**Hauvert Pacheco** *February 21, 2016 23:15*

i wasn't able to delete with via terminal but i was able to find the ~/.ssh/known_hosts file and i simply edit it with textedit and it worked. now i just need to figure out how to change the current


---
**Andy Tomascak** *February 21, 2016 23:19*

It's in /etc/redeem/. How did you set the current so high in the first place??


---
**Hauvert Pacheco** *February 21, 2016 23:23*

too be honest i don't know how i could have. i was able ssh the first time but did nothing because it was still the beagle board. once i flashed it couldn't figure out how to ssh again because i kept getting that warning. 


---
**Andy Tomascak** *February 21, 2016 23:33*

All the .cfg files are in /etc/redeem. If you see your printer in there, great! Otherwise you will need to build your own .cfg. To do this, make a copy of default.cfg and rename it to <your_printer_type>.cfg. Then make any changes you need. Next, I'd delete out any parts that you didn't change so that your config stays clean. Finally, if you have any personalizations for your specific printer, create another file called "local.cfg" and put them in that.



Basically, it works like this: default.cfg is used to set all settings. "<your_printer_type>.cfg" is then used to override any settings in default.cfg. "Local.cfg" is then used to override any settings that your printer needs that's beyond what is in "<your_printer_type>.cfg".



Example: I have the same default.cfg as you. But I have a rostock_max.cfg that has the override settings for that type of printer (delta, arm lengths, etc). Finally, I blew up my ext1 circuit and had to do a mod to get my printer working again. That mod is in local.cfg because it only pertains to my specific printer, not all rostocks.﻿


---
**Andy Tomascak** *February 21, 2016 23:41*

The setting you will want to override is "current_x = 0.5" for your "x" stepper. If it's running hot, try "current_x = 0.3". You will want to do this with each stepper (current_y, current_z, etc.) Restart Redeem after you make changes.


---
**Tim Curtis** *February 22, 2016 18:11*

Veridico Cholo, Did you still use the Steps per mm value for the 1/16 micro step or did you recalculate for 256 steps when using setting number 8? I too am using 1/16 micro step. When I try either setting 4 or 8 my axis moves way too much for what is programmed. I am using the same micro step numbers and setting that I have used on this printer for a few years now but the replicape doesn't apply them correctly for me.


---
**Andy Tomascak** *February 22, 2016 18:14*

Steps_per_mm (in the cfg) is calculated without regard to microstepping. Do the math for whole steps, the software will compensate for whatever microstepping you choose.



If you are using old settings from Repetier/Marlin/etc, you are likely off by a LOT. 


---
**Tim Curtis** *February 22, 2016 19:05*

Andy, Thanks for the info. So I need to divide all my setting by16 since my numbers were for 1/16 micro steps originally. That makes sense because when I called for a 1 mm move I moved about 16 mm. Thanks again.


---
**Andy Tomascak** *February 22, 2016 19:45*

You should multiplying, not dividing. :)



(1/16 step) * 16 = whole step



Sorry, just realized it depends on how you are doing the calculation. Number for most printers will be between 3 and 6, if that helps. Depends on gearing of course.


---
**Tim Curtis** *February 23, 2016 12:10*

Andy,  I got it dialed in last night. I'm set on 8 and I took my original steps per mm that I used on my ramps and divided it by 16 (I was using 1/16 micro steps). Now I am dead on with movements. 1 mm = 1 mm, 10 mm = 10 mm, ect... I am very pleased with how quiet these boards are. I'll be messing with speeds tonight to find where my limit is.


---
*Imported from [Google+](https://plus.google.com/116125412087866821266/posts/D85DG7ZEAF2) &mdash; content and formatting may not be reliable*
