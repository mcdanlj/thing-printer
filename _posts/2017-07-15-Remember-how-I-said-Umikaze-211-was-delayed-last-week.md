---
layout: post
title: "Remember how I said Umikaze 2.1.1 was delayed last week?"
date: July 15, 2017 16:28
category: "Discussion"
author: Jon Charnas
---
Remember how I said Umikaze 2.1.1 was delayed last week?



After months of trying to sort out how to get Redeem and octoprint to work on the same image for the standard Beaglebone black and the wireless version, I finally found the fix last night. As such, I believe it's now safe to assume that we have a working image for Beaglebone black Wireless enthusiasts, though it's still a release-candidate. 



I welcome anyone wanting to test the new image, but be warned that it runs the develop branch of redeem. This is good news for people on CoreXY and Delta printers. 



It's bad news for folks who carefully tuned their accelerations and speeds. The PRU firmware was re-written in C to better count steps, meaning if you say 80mm/s you're now getting 80mm/s instead of the approximation you had previously. Acceleration is generally stronger, and you'll need to lower those and re-tune with this new PRU firmware. 



Included is also Octoprint 1.3.4 (latest & greatest from Gina), Slic3r is pre-installed, though not the the octoprint slic3r plugin (yet), which you should be able to install via the octoprint plugins menu. 



You'll find the release candidate image here: [https://github.com/goeland86/Umikaze2/releases/tag/2.1.1-rc3](https://github.com/goeland86/Umikaze2/releases/tag/2.1.1-rc3)





**Jon Charnas**

---
---
**Elias Bakken** *July 15, 2017 16:39*

Yay! 


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/aXPcHRpduo6) &mdash; content and formatting may not be reliable*
