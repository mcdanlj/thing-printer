---
layout: post
title: "Is a Replicape needed for a connection with octoprint?"
date: July 10, 2015 17:12
category: "Discussion"
author: Pascal Wieland
---
Is a Replicape needed for a connection with octoprint? I don't have any, because i have an external stepper driver.



Octoprint shows me only the AUTO interface. I can't find any HOWTO for the connection to redeem, e. g.



Is it normal that the local.cfg not exists?



Sorry for the stupid questions, i'm a printing noob... :/





**Pascal Wieland**

---
---
**Elias Bakken** *July 12, 2015 10:13*

local.cfg does not exist by default. That is right.

If only Auto exists, Redeem is not running probably.

Replicape is not needed, but you have to apply the overlay manually.


---
*Imported from [Google+](https://plus.google.com/113006560065419555185/posts/5pJLaN9yyAW) &mdash; content and formatting may not be reliable*
