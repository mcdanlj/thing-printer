---
layout: post
title: "Heres the log of redeem and the result of updating /boot/uEnv.txt with the A4A model"
date: May 18, 2016 06:27
category: "Discussion"
author: IonicBasher
---
Heres the log of redeem and the result of updating /boot/uEnv.txt with the A4A model. 

Also as you can see writing the eeprom is not working. Do I have to clear it first to allow space. If so, how? Searched to no avail.



![images/4268ae6af9132f585a2253bf96b92b82.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/4268ae6af9132f585a2253bf96b92b82.png)
![images/6aefbd462a31eb37d5652f8f58fb623c.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/6aefbd462a31eb37d5652f8f58fb623c.png)

**IonicBasher**

---
---
**Elias Bakken** *May 18, 2016 09:16*

Looks like the file was not the right format... Can you have a look at the file and make sure it's not a http 404 or something? :)


---
**IonicBasher** *May 19, 2016 22:00*

Nope automatically root user for kamikaze image.

However the link was not right as Elias mentioned. It was a valid link however it led to the bitbucket repository. We needed the raw eeprom. Correct link for wget:



[https://bitbucket.org/intelligentagent/replicape/raw/bf08295bbb5e98ce6bff60097fe9b78d96002654/eeprom/Replicape_0A4A.eeprom](https://bitbucket.org/intelligentagent/replicape/raw/bf08295bbb5e98ce6bff60097fe9b78d96002654/eeprom/Replicape_0A4A.eeprom)



After this all worked well.


---
*Imported from [Google+](https://plus.google.com/100874739223986876085/posts/HNCknSqDCRb) &mdash; content and formatting may not be reliable*
