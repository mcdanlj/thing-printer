---
layout: post
title: "Hello all! I am new to 3D printing and I have a Prusia I3 steel I have built"
date: June 06, 2016 06:51
category: "Discussion"
author: Bob McDowell
---
Hello all! I am new to 3D printing and I have a Prusia I3 steel I have built. I originally had a SAV MK1 board that I was abe to get up and printing before I "let the magic smoke out"! Now I have purchased an BBB and Replicape. Elias has answered some questions through email, thanks Elias. Of course I have some more!

1. I have dual Z steppers, can I just tie the leads together with an 2-1 adapter from the local hobby store?

I have a windows enviroment , my Linux skills are appalling, and would like to access Redeem on a windows desktop through my network. I can get to Octoprint without a problem. Can you point me to some info about accessing Redeem.

Bob





**Bob McDowell**

---
---
**Bob McDowell** *June 06, 2016 10:48*

Ok, I got my Replicape temporarily hooked up to the printer with one extruder and 1fan. All seems to work except I have a little end stop and direction problem that I will work out.

Now I still need to access the config files in Redeem and work out the dual Z steppers.

For the config files I need to figure out how to access and edit the files from windows. Also, I can no longer access the BBB from a usb connection. Any ideas for that. I can access SSH from my desktop.


---
**Elias Bakken** *June 06, 2016 11:49*

Hi! If you only have one extruder, you might be better off using the H stepper driver as a slave, that way you don't need any additional hardware. If you do have dual extrusion, get the splitter cable. 



If you are on Windows, you can use Putty to get ssh access to the board. That way you can upgrade redeem to the latest version and perhaps more importantly upgrade the python-octoprint-redeem plugin that allows you to edit the config file from OctoPrint. 


---
**Bob McDowell** *June 06, 2016 14:18*

Fantastic response. Thanks. I will try and let you know. I do have dual extruders so I will go with the splitter.


---
**Bob McDowell** *June 08, 2016 05:10*

Ok, I have my printer working wth the Replicape and man is themovement smoother and quieter than with the SAV MK1 board. Nice! But now I have to learn how to configure in Redeem. Is there a step by step "Redeem for Dummy's"! My Linux skills are lacking so I don't even understand how toget to and edit files. My particular problem is setting home. Does anyone have a Prusia I3 cfg I can use.

Bob


---
**Bob McDowell** *June 08, 2016 05:20*

I also postd this last entry in the forums.


---
*Imported from [Google+](https://plus.google.com/107803003115083427930/posts/cWuJryJcGF2) &mdash; content and formatting may not be reliable*
