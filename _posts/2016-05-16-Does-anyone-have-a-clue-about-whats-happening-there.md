---
layout: post
title: "Does anyone have a clue about what's happening there?"
date: May 16, 2016 11:37
category: "Discussion"
author: Javier Murcia
---
Does anyone have a clue about what's happening there? 

I'm talking about the heat bed, with these sudden spikes or valleys. The heat bed is a enormous sheet of aluminum, so it's imposible that it cools that fast, and it's static (since it's a delta) so I don't think the cable problem is also pretty unlikely. 

![images/83fc224df5ea5056b712b0c339288128.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/83fc224df5ea5056b712b0c339288128.png)



**Javier Murcia**

---
---
**Pieter Koorts** *May 16, 2016 11:40*

Mmmmm... I had this the other day. Can't remember if it was hot end or bed but I had a large fan on so assumed that when it turned off the heaters it just cooled too fast. I am using git master though.


---
**Javier Murcia** *May 16, 2016 11:44*

I'm having this problem with the bed, and I don't have any fans in it. Also even with the fan, I don't think it could fall 33c in less than a second, but appart from that I'm clueless 


---
**Elias Bakken** *May 16, 2016 11:50*

This is almost certainly your heater being loose or your cable about to fail! 


---
**Javier Murcia** *May 16, 2016 13:55*

The heater is a silicone mat glued to the aluminum build surface, and the sensor is integrated in the silicone mat. 



Im going to check the wiring, since sometimes my aluminum extrusion profiles sometimes cut the cables insulator. I suppose if I check the resistance of the ntc I would see an abrupt change when this occurs, am I wrong? 


---
**Elias Bakken** *May 16, 2016 14:51*

It depends on your multi meter Javier, low costs use longer to settle, but you will probably see the same thing yes.


---
**Daryl Bond** *May 17, 2016 02:21*

I had the same symptoms just before the leads to my hot end thermistor broke. 


---
*Imported from [Google+](https://plus.google.com/+JavierMurciaDiaz/posts/1Cz8XMfNDDX) &mdash; content and formatting may not be reliable*
