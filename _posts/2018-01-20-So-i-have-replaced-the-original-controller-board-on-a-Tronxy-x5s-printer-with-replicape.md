---
layout: post
title: "So i have replaced the original controller board on a Tronxy x5s printer with replicape"
date: January 20, 2018 18:58
category: "Discussion"
author: William Burton
---
So i have replaced the original controller board on a Tronxy x5s printer with replicape.  I have it working great for me, with one exception.  When printing circular items, the printer pauses for a second or two every so often.  This leads to little bumps of extra plastic appearing where the pauses occurred.  This only seems to happen on circular prints from what i can tell.  Anyone have this issue or any recommendations on how to correct?  



I have Replicape Rev B and version 2.0.5 installed.



Other changes to the Tronxy x5s i have made are swapping out the hot end for a E3D 24v all metal hot end, adding a 24v power supply and 24v hotbed.   









**William Burton**

---
---
**Jon Charnas** *January 20, 2018 23:24*

Hi **+William Burton**, so the pauses on circular prints is a well understood phenomenon, if not super well explained. Basically Redeem was optimized too much, and has a relatively short buffer. For circular moves, where each circle is a bunch of tiny straight lines, there's a problem with Octoprint feeding each line fast enough. It's been acknowledged on the OctoPrint side to some extent, and we've managed to get "Print from SD" emulation working in the latest develop version of Redeem/Octoprint with Umikaze 2.1.2-rc. 



I wouldn't recommend that if you're not ready to face some dragons down in the code.



What I heartily recommend is a) upgrading to Umikaze 2.1.1 - lots of new features in Redeem, and b) playing around with the move buffer size in the [Planner] configuration. Increasing that buffer will help with the circles, but increasing it too much will make your fans turn on too early or too late, which is also being worked on now.


---
**William Burton** *January 21, 2018 03:37*

Sorry actually i do have 2.1.1 installed.  I will look at the configuration changes you mentioned.  Thanks again.  


---
**Jon Charnas** *February 08, 2018 15:44*

Any luck with your Tronxy?


---
*Imported from [Google+](https://plus.google.com/110903600885855713529/posts/HXQMMzhXRC9) &mdash; content and formatting may not be reliable*
