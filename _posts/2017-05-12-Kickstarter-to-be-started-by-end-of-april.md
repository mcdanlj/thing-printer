---
layout: post
title: "Kickstarter to be started by end of april"
date: May 12, 2017 20:58
category: "Discussion"
author: Mathieu Paquin
---
Kickstarter to be started by end of april. What is up with that manga screen?





**Mathieu Paquin**

---
---
**Jon Charnas** *May 12, 2017 21:34*

There was a delay getting the prototyping parts, the delay is probably on the order of 2-3 weeks


---
**Mathieu Paquin** *May 14, 2017 04:46*

ohhhh. Ok thx for the update. I was looking at the website but it was still saying end of april. I was scared the project was dead :S




---
**Elias Bakken** *May 15, 2017 12:54*

Not at all! Just quiet before the storm : ) Working on the KS video now!


---
*Imported from [Google+](https://plus.google.com/112278304600785903296/posts/jPLWEBuR6Qw) &mdash; content and formatting may not be reliable*
