---
layout: post
title: "Question, what's your favorite / most used slicer with the Replicape?"
date: February 09, 2017 14:16
category: "Discussion"
author: Jon Charnas
---
Question, what's your favorite / most used slicer with the Replicape?



This is to help decision making for the next Kamikaze release as Octoprint's just released support for slic3r. 



Please help decide whether Kamikaze includes both Slic3r and Cura or only one or the other with documentation in the wiki for alternatives.





**Jon Charnas**

---
---
**Jon Charnas** *February 09, 2017 14:48*

That's what the 3rd and 4th options are for ;)


---
**Daniel Kusz** *February 09, 2017 14:53*

I like Cura more because of interface and slice speed, but slic3r is very good software too. Build-in slicer should work in Toggle with pre defined profiles, so you can use printer as standalone device without desktop computer. Look at the Zeus printer from AIO ROBOTICS. 


---
**Benjamin Liedblad** *February 12, 2017 07:24*

Maybe not possible, but why not both? 

I'm new to OctoPrint, but in playing with it for 20 minutes, thought I saw that it it possible to install plugins... could the Kamikaze flasher include all slicers supported by OctoPrint and just let the end user choose which OctoPrint slicer plugin to use?




---
**Jon Charnas** *February 12, 2017 08:53*

So yes, nothing's stopping the use of both Slic3r and Cura onboard. But I'm trying to figure out if this is something that would actually be used. We're trying to keep the image lightweight, and both Cura and Slic3r have different dependency chains which could eat into the spare storage on the BBB. This poll is to determine if users prefer one or the other, or if it would be best to include both.


---
**Jon Charnas** *February 15, 2017 10:36*

Ok, so, looks like the results are actually pretty even. We'll try to include both options in 2.1.1 or later releases.


---
**Henrik Söderholtz** *February 20, 2017 09:04*

For me Slic3r has been nothing but trouble on OS X, so I have no confidence for that software, and I find Cura's slicer does a much better job of getting efficient prints to cut print times. Loving the 2.4.0 beta right now!


---
**Phil Mally** *March 05, 2017 10:23*

Simplify 3D




---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/6MCMGTFAEr4) &mdash; content and formatting may not be reliable*
