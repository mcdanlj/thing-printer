---
layout: post
title: "I read somewhere that if your stepper are making a high pitch noise that it's not good"
date: February 20, 2016 03:08
category: "Discussion"
author: Hauvert Pacheco
---
I read somewhere that if your stepper are making a high pitch noise that it's not good. How can I fix this?  It seems the only my x stepper is making noise. Thing I could have wires it wrong or should I change anything? The x is the only one making high pitch noise.





**Hauvert Pacheco**

---
---
**James Armstrong** *February 20, 2016 03:14*

Change the micro steps to 6 or 7 in the config. 


---
**Veridico Cholo** *February 20, 2016 03:46*

I had to experiment with the microsteps and the current settings. Even on micro step 7 I had noise. I had to go to microstep 8 and now all motors are super quiet. 


---
**Hauvert Pacheco** *February 20, 2016 04:09*

Okay awesome thanks! 


---
*Imported from [Google+](https://plus.google.com/116125412087866821266/posts/a3FZt8gcSnu) &mdash; content and formatting may not be reliable*
