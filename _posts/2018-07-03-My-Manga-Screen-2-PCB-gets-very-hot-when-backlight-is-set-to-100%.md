---
layout: post
title: "My Manga Screen 2 PCB gets very hot when backlight is set to 100%"
date: July 03, 2018 13:29
category: "Discussion"
author: Claudio Prezzi
---
My Manga Screen 2 PCB gets very hot when backlight is set to 100%. Is this normal and ok to use, or is there anything to do like active cooling for using 100% backlight?





**Claudio Prezzi**

---
---
**Elias Bakken** *July 03, 2018 14:56*

The back of the pcb has been measured at 66 deg C with normal use, which I think is fine. There are no electrolytic caps that can dry out.


---
*Imported from [Google+](https://plus.google.com/+ClaudioPrezzi/posts/eYenHBNFNhZ) &mdash; content and formatting may not be reliable*
