---
layout: post
title: "The trailer is out! Originally shared by Elias Bakken The Manga Screen 2 trailer!"
date: June 12, 2017 07:45
category: "Discussion"
author: Elias Bakken
---
The trailer is out!



<b>Originally shared by Elias Bakken</b>



The Manga Screen 2 trailer! I made a small project using the new Manga Screen 2, then I filmed it and now it is on YouTube! Watch it til the end, it's cozy.





**Elias Bakken**

---
---
**Daniel Kusz** *June 12, 2017 18:31*

Elias! You are alive! BTW great news and good luck. Will be there some Toggle update for new screen?


---
**Elias Bakken** *June 12, 2017 18:36*

**+Daniel Kusz** Yes! I've made a 1080p version of Toggle, but I want to work some more in it before delivery. 


---
**Daniel Kusz** *June 12, 2017 18:41*

This will complete the works: D


---
**Mathieu Paquin** *June 19, 2017 01:20*

Teasing a bit more ;)


---
**Mathieu Paquin** *June 26, 2017 23:08*

I just saw that you were going to make 2 different size! This is going to be sooo perfect for my project. Is it going to be dimmable like the previous version? 


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/AqQQDHY21yc) &mdash; content and formatting may not be reliable*
