---
layout: post
title: "Working on a mount for the BBB replicape"
date: December 15, 2016 17:02
category: "Discussion"
author: Step Cia
---
Working on a mount for the BBB replicape. Made a bunch of side outlet for 4mm sleeving cable. Hopefully the space inside is enough to hide all the cables needed to connect to the cape. The screenshot is the more developed version :)



![images/4546684b38133573ae46c37f416c2297.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/4546684b38133573ae46c37f416c2297.png)
![images/f15a053661c6e1937b92cf04f4c09d44.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/f15a053661c6e1937b92cf04f4c09d44.jpeg)

**Step Cia**

---
---
**Jon Charnas** *December 15, 2016 17:15*

Nice but I'm curious how you're planning on cooling the stepper drivers


---
**Step Cia** *December 15, 2016 17:47*

Thought about it but haven't come up with solution yet... If one were to apply heatsink would it be directly at top of the chip or at the bottom where the silver rectangular are?


---
**Elias Bakken** *December 15, 2016 23:52*

Trinamic does not use much heat sinks. Forced cooling works wonders.


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/VRbByinFBgi) &mdash; content and formatting may not be reliable*
