---
layout: post
title: "I made a quick Manga Screen 2 mount for attaching to the end of 4040 (or 2020) extrusions"
date: September 24, 2018 12:25
category: "Discussion"
author: Chris Deister
---
I made a quick Manga Screen 2 mount for attaching to the end of 4040 (or 2020) extrusions. Nothing fancy, but does have countersunk holes on both sides. Thought it might be helpful for someone. 



[https://www.thingiverse.com/thing:3117555](https://www.thingiverse.com/thing:3117555)









**Chris Deister**

---
---
**Elias Bakken** *September 24, 2018 12:48*

Oh cool!


---
**Chris Deister** *September 24, 2018 14:53*

Thanks for the screen. Beyond the printer, I had been looking into sourcing these types of screens and dealing with the electronics, you did, as I needed a small 60 fps 1080p screen for visual neuroscience experiments. We are piloting some experiments soon. You saved me a ton of work, which may have gone nowhere. 


---
*Imported from [Google+](https://plus.google.com/106165570706316836863/posts/Yt9ZGaj7qjY) &mdash; content and formatting may not be reliable*
