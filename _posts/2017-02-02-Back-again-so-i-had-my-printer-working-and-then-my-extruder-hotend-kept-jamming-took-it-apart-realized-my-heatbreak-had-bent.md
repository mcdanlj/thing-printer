---
layout: post
title: "Back again, so i had my printer working, and then my extruder/hotend kept jamming, took it apart, realized my heatbreak had bent"
date: February 02, 2017 11:57
category: "Discussion"
author: Andrew A
---
Back again, so i had my printer working, and then my extruder/hotend kept jamming, took it apart, realized my heatbreak had bent.



So i assumed it was just me fiddling with the settings and had gotten the hotend too close to the bed.  i put a new hotend and nozzle in, do the card/paper leveling trick, hit home multiple times to double check its right.......i hit print, it homes, and then my nozzle is jammed into the bed!!!!!!!



why would it throw my nozzle into the bed only during print??????? but not home too?????



THEN, i can't get my soft endstops working right, they were working at one point, but then i swapped my x axis and this hotend thing happened and im not sure where it got messed up but nomatter what i do i can't get them to work like they are supposed to.  the only way i can get it to print is to put a high number on both the min and max, its almost as if it is adding them together????  if i drop the max below .25 it won't move at all after homing, if i set it at .25 it will run into walls......and i did try positive and negatives but not all combinations of them.  but i swear its as if all my settings are changing on me or the effects of said settings change.



at one point i did manage to get it to stop it somewhere along the bed , but when i went to increase the number to get it to travel further it wouldn't work.



now back to this bed issue, that is what really is annoying me right now, the endstops i feel like maybe i can figuer those out with some more tinkering, but the bed problem simply does not make any sense at all.  I home, its perfect level, i hit print nozzle into bed.....i hit pause, hit home....perfect level again.....what?????





**Andrew A**

---
---
**Andrew A** *February 02, 2017 11:57*

[Steppers]



microstepping_x = 6

microstepping_y = 6

microstepping_z = 6

microstepping_e = 6

microstepping_h = 6



current_x = 0.5

current_y = 0.5

current_z = 0.5

current_e = 0.5

current_h = 0.5



steps_pr_mm_x = 10.0

steps_pr_mm_y = 10.0

steps_pr_mm_z = 50.0

steps_pr_mm_e = 26.15

steps_pr_mm_h = 50.0



in_use_h = True

slave_z = H

direction_z =  -1

direction_h =  -1

[Endstops]



invert_X1 = False

invert_X2 = True

invert_Y1 = False

invert_Y2 = True

invert_Z1 = False

invert_Z2 = True

end_stop_Z1_stops = z_ccw, h_ccw

end_stop_X1_stops = x_ccw, y_ccw

end_stop_Y1_stops = x_ccw, y_cw



soft_end_stop_min_x = -0.2

soft_end_stop_min_y = -0.2

soft_end_stop_min_z = -0.2

soft_end_stop_max_x = 0.27

soft_end_stop_max_y = 0.27

soft_end_stop_max_z = 0.2





[Heaters]



sensor_E = SEMITEC-104GT-2



[Cold-ends]



connect-therm-E-fan-0 = True

add-fan-0-to-M106 = True

[Planner]



max_speed_x = 0.3

max_speed_y = 0.3

max_speed_z = 0.03

max_speed_e = 0.2

max_speed_h = 0.03



acceleration_x = 3.0

acceleration_y = 3.0

acceleration_z = 0.5

acceleration_e = 3.0

acceleration_h = 0.5



[Homing]



home_speed_x = 0.1

home_speed_y = 0.1 

home_speed_z = 0.1

home_speed_e = 0.01

home_speed_h = 0.1



home_x = 0.22

home_y = 0.16

home_z=  0.003



[Geometry]

travel_x = -0.4

travel_y = -0.4

travel_z = 0.4




---
**Andrew A** *February 02, 2017 11:59*

oh and during the print my bed started going up instead of down.....quiet interesting since it was printing down yesterday......


---
**Daniel Kusz** *February 02, 2017 12:07*

Is your Z axis inverted? 


---
**Daniel Kusz** *February 02, 2017 12:09*

I see yes. Make direction_z=1 and invert it in Octoprint. Next try to print.


---
**Andrew A** *February 03, 2017 06:52*

that switched the controls in octoprint so down actually makes the bed go down.  when i click print, the print starts, and the bed drops like 5x what i set it at, i click pause, hit 'home' and it goes to where i set it.......................................stupid............so again why does 'printing' put it at a 'different' distance than 'home' button..................



so then i resume print, and after the first layer, nozzle into bed..............im hoping to fix this issue before i ruin my last heatbreak


---
**Daniel Kusz** *February 03, 2017 06:57*

Why you have home_z = 0.003? In your configuration there is no space before "=". Please check this detail. 


---
**Andrew A** *February 03, 2017 08:49*

ill check that but i think i just remembered something that i was doing with my previous setup and i forgot to do it the next day and caused my problems.



When i upload a file, and slice it, it tells me to 'select a slicing profile', nowhere in the guides did i see anything about how to create one, so i just picked one to use, regardless all the profiles show the same 'command'.



Under advanced is this command

G1 Z15.0 F{travel_speed}                ; move the platform down 15mm



and i was deleting it yesterday,  can someone tell me why this is in all the slicer profiles? because to me it seems very strange to drop the z 15mm......i guess in a delta it might make sense for some complex reason, but its in all the profiles.





heck it still doesn't make sense to me, and i can't even download a profile to edit one, so i can't import my own, and it sure is annoying having to remove that line every time.  unless its to make sure the nozzle doesn't hit and its 'supposed' to go back up afterwords



anyways hopefully that 'space' fixes that i guess, on a side note, if u look at the line under the perusa profile it says that line moves it 15mm, but the command is actually for 5mm lol



"G1 Z5.0 F{travel_speed} "



be back in another 30mins-hour after i test these settings


---
**Andrew A** *February 03, 2017 13:20*

so 4 hours later and im back at the config i posted with the -1 direction.



what appears to be happening, and im really tired so this could all be wrong, but if i leave the original 'perusa advanced settings' alone, it will home, lower the bed, then raise the bed, and somewhere in all of that i lose my bed height settings which jams my nozzle into the bed or puts it so close it can't put out extrusion.



so what i have to do is disable those "z axis" options in 'advanced gcode', then "home" BEFORE i print...............so  i got it working, i think, but its some stupid hack job that will drive me crazy, and i lose the nice little feature of moving the bed out of the way when the x crosses over it.



so in other words when the z axis options on, it homes, lowers the bed, then parks itself on the endstop essentially which makes no sense.



i guess it would be fine if i had my endstops zero'd in perfect....but then that is hard, and would make all these settings pointless.  i think there was one gcode that said send z to 0, maybe i just need to remove that, im too tired but ill try that i guess tomorrow.......



i kind of wish they never included any slicer profiles, i had to do the stupid redeem code by hand, i would have saved 3 days of headaches if they forced me to write the slicer profile by hand


---
**Andrew A** *February 04, 2017 10:13*

Edit: Nvm ive confirmed im mentally retarded


---
**Andrew Dowling** *February 04, 2017 11:28*

Also check the local.config file (on redeem settings page - you can edit that in the browser). Sometimes an M500 command likes to write something in there that you didn't intend. Check that - it overwrites EVERYTHING else. Delete anything in there that might be giving you pains.




---
**Andrew A** *February 04, 2017 12:32*

i deleted the homing commands and just leveled my bed to my endstop, and after fiddling around i realized a bolt was lose on the z axis, so every time i homed thats why i kept getting a different result :(.........dam ghosts in the machine i swear.  but yah i guess building a 3d printer is alot more frustrating than i had imagined.  oh well, mid life printer build crisis over, for now



yah so how do i stop the redeem going blank in octoprint settings?  they say the 'first' time you just have to upload a blank local.cfg, and then it will show up after that, however nearly every time i shut down, and then reboot its blank again, i am using the shutdown command in ocotprint before i power down.  also i have a print estimate time of 28 minutes, but for some reason below that its saying 2 hours?  do the octoprint mm/s override the redeem settings? i can't quiet tell if changing the mm/s in octoprint is actually speeding it up or not


---
**Andrew A** *February 07, 2017 15:39*

nvm its doing it again, it homes, goes to the home_z, but then it goes to the endstop for some reason that i can't figuer out.  what setting is telling it to go to the endstop?  i checked and the printer is printing in the correct direction, im using corexy with no probe, using home_z to adjust nozzle distance from the bed, using perusa profile, and my printer profile with my bed size and z axis inverted, which i did because i also inverted it in redeem to try and fix this problem



i just need to know why the bed would move after homing and going to home_z, why does it go to the endstop, what setting is doing that?


---
**Andrew A** *February 07, 2017 19:13*

fixed with offset_z


---
*Imported from [Google+](https://plus.google.com/106001037169639681932/posts/5NLL3oC4DYp) &mdash; content and formatting may not be reliable*
