---
layout: post
title: "I can not download files local.cfg to Redeem and Toggle - respectively 500: Internal Server Error, 404: Not Found"
date: May 16, 2016 19:58
category: "Discussion"
author: Daniel Kusz
---
I can not download files local.cfg to Redeem and Toggle - respectively 500: Internal Server Error, 404: Not Found. Is reinstall solve this problem and the problem of Toggle?





**Daniel Kusz**

---
---
**Elias Bakken** *May 16, 2016 20:13*

You might have to make the file: touch /etc/toggle/local.cfg


---
**Elias Bakken** *May 16, 2016 20:15*

Also, make sure the user octo can read and write to it: chmod 777 /etc/toggle/local.cfg


---
**Daniel Kusz** *May 17, 2016 18:09*

OK, done it ;) Great, I can see models and can run stepper :D


---
**Daniel Kusz** *May 17, 2016 20:51*

Another two small issues: I can't import the file. cfg for Toggle and Control Z axis from screen moves always 10 mm regardless of the setting.﻿


---
**IonicBasher** *May 18, 2016 06:30*

Same here unable to upload any files to toggle or redeem.


---
**Elias Bakken** *May 18, 2016 09:18*

I think that is also a permission issue. The catalog must be writable for octo


---
**Daniel Kusz** *May 18, 2016 14:02*

chmod 777 /etc/toggle ? 


---
**Elias Bakken** *May 18, 2016 14:03*

That is the quick and dirty solution, yes.


---
*Imported from [Google+](https://plus.google.com/103041373788524358316/posts/hA9kh7KEA5c) &mdash; content and formatting may not be reliable*
