---
layout: post
title: "Hello, I have a problem of speed using replicape on corexy"
date: April 21, 2017 10:02
category: "Discussion"
author: David Slade
---
Hello,

I have a problem of speed using replicape on corexy. Using branch master. I recently take time to confirm the issue with some users, different models of corexy/controllers.

For example, Kid buu ([http://www.thingiverse.com/thing:1682656](http://www.thingiverse.com/thing:1682656)) with a 0.6mm nozzle and 0.225 layer the estimated time is 3h15, and the real print time is near 6h!

On a I3, same model, same speed , 0.4 nozzle 0.19 layer, estimated time 3h58, real print time 4h20.

With a more little model, Super Saiyan Marvin, to have a correct estimated print time I need to push speed to 140%





**David Slade**

---
---
**Tan Tuan TRAN** *April 21, 2017 12:00*

Pareil chez moi :(


---
*Imported from [Google+](https://plus.google.com/110933325899665289288/posts/AyMmfat6jgW) &mdash; content and formatting may not be reliable*
