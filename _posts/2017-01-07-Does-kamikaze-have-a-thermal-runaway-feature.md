---
layout: post
title: "Does kamikaze have a thermal runaway feature?"
date: January 07, 2017 19:06
category: "Discussion"
author: KairuFPV “Deltym”
---
Does kamikaze have a thermal runaway feature? What are the safety features that are implemented?





**KairuFPV “Deltym”**

---
---
**Elias Bakken** *January 07, 2017 19:34*

There are several safety features implemented to handle error conditions on the thermistors. In fact, you should get a notification if a condition occurs directly in OctoPrint. See this for more info: [thing-printer.com - Some new safety features in Redeem - Thing-Printer.com](http://www.thing-printer.com/some-new-safety-features-in-redeem/)


---
*Imported from [Google+](https://plus.google.com/108846290422025907415/posts/A8gwJwMhGXB) &mdash; content and formatting may not be reliable*
