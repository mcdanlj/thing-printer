---
layout: post
title: "Henrik Peiss This is what my connection information looks like on Octoprint"
date: August 16, 2014 16:30
category: "Discussion"
author: Elias Bakken
---
**+Henrik Peiss** This is what my connection information looks like on Octoprint. Tried a reboot, everything looks good with the latest image..

![images/4007f7b35d648b3e62783ed9f99863ad.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/4007f7b35d648b3e62783ed9f99863ad.png)



**Elias Bakken**

---
---
**James Armstrong** *August 16, 2014 23:24*

I just borrowed I duplicator4 to have something to try the Replicape board in. I got tired of boy being able to join in the fun. 


---
**Elias Bakken** *August 16, 2014 23:42*

Nice!


---
**Henrik Peiss** *August 17, 2014 08:47*

Seems like it is a config file problem, because redeem isn't starting after config files mod. That seems to be the reason why Octoprint isn't able to connect to the virtual port offered by redeem. What is the best way for solving the config file problem: look on redeem.py and the config file in parallel?


---
**Elias Bakken** *August 17, 2014 09:37*

I expanded on the web ui yesterday, so you can edit config files and restart redeem from there. Ill package it and upload once I get in to work!


---
**Elias Bakken** *August 17, 2014 13:12*

**+Henrik Peiss** could you try the new UI?

opkg update

opkg install thing-frontend

systemctl restart lighttpd


---
**Henrik Peiss** *August 17, 2014 15:21*

Sounds great. 👍 

Will be back at my BBB in a few hours. Will post my findings. 


---
**James Armstrong** *August 17, 2014 19:49*

Is this image the one on the wiki with the date of 2014-06 (BBB flasher image)? Or are you talking about a package image?



--

James Armstrong


---
**Henrik Peiss** *August 17, 2014 20:02*

**+James Armstrong** use the last one

[http://feeds.thing-printer.com/images/](http://feeds.thing-printer.com/images/)


---
**James Armstrong** *August 21, 2014 17:27*

What's the newer "debrew" flasher images compared to the others?


---
**Elias Bakken** *August 21, 2014 17:29*

They are for my coffe machine, don't use those : ) But I'm making a fresh image now, so hold on a few minutes and you can get the latest!


---
**James Armstrong** *August 21, 2014 17:35*

Thanks. I borrowed a Duplicator4 to use as a test bed for the replicape since all I had was my corexy (not going to mess with that yet) and a delta.


---
**Elias Bakken** *August 21, 2014 17:38*

Ok, todays image should be ready for download.


---
**Henrik Peiss** *August 24, 2014 11:13*

**+James Armstrong** Did you have succes with the duplicator4? Assuming it has more or less the same hardware as the CTC bot I can use, I assume problems on getting proper reading from the hotend sensor (which are labeled as Thermocouples on the CTC board)? **+Elias Bakken** mentioned on one of my CTC-pics [1] that there is no implementation of the thermocouples yet.



[1]: [https://plus.google.com/+HenrikPeiss/posts/WiyukVAoo6w/p/pub?pid=6047009200196412162&oid=101273030243942030155](https://plus.google.com/+HenrikPeiss/posts/WiyukVAoo6w/p/pub?pid=6047009200196412162&oid=101273030243942030155)


---
**James Armstrong** *September 07, 2014 05:05*

So I have the duplicator4 all modified and the Replicape installed. I have a question on the configuration. This is setup for x and y max endstops and z min. Homing should also home to x max y max and z min. I cannot get it to both move correctly with manual controls and home the right way. If I inverse the axis directions it will then move towards the endstop but not move the correct way whine manually moving. I could get it to somewhat work by moving all the endstops to min but that is not what I want since that will also make all movements backwards.





Is there a home to min or max setting In the configuration? I only see homing x enabled or disabled  no choice to set to min or max. Is min always assumed.  


---
**James Armstrong** *September 07, 2014 14:09*

I think I found what needs to be changed in the code (will require config setting). I'm going to try and implement homing to max to get my feet wet. 


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/jaicpCEm1uh) &mdash; content and formatting may not be reliable*
