---
layout: post
title: "Just a question regarding the Replicape power supply limits"
date: January 22, 2017 09:06
category: "Discussion"
author: tizzledawg
---
Just a question regarding the Replicape power supply limits. Can this board be run off 28 volts?



I just got hit by 2 power interuptions during a long print which really pissed me off and now I'm thinking of running this printer off a 24v battery, however if I am to float charge it, the board will be getting closer to 28 volts. Would this be ok?





**tizzledawg**

---
---
**Pieter Koorts** *January 22, 2017 13:09*

If you are not using a heated bed(power) you could try a dc to dc converter so that any variation in source voltage would just get filtered out.


---
**Elias Bakken** *January 22, 2017 13:54*

The Max input on the step down converter is 24V, so no... 


---
**tizzledawg** *January 23, 2017 08:46*

Thanks. I might try using a 18v battery then and isolate with a diode so it can kick in straight away if the 24 volt supply drops out.


---
**Jon Charnas** *January 26, 2017 11:01*

**+tizzledawg** why not get a computer-UPS unit? They make units that aren't too pricey with several kWh providing direct AC (110 or 240V depending on where you live). I'm not sure how the print would be affected if you suddenly drop from 24 to 18V... probably negative impacts since temp on bed and heater will drop a bit.


---
**Vernon Barry (Jetguy)** *January 31, 2017 03:38*

Actually there is a specific DC style UPS board that could work for many low powered printers (not the heated bed, but at least the mechanics and fans) [http://www.mini-box.com/OpenUPS?sc=8&category=981](http://www.mini-box.com/OpenUPS?sc=8&category=981)

- Intelligent UPS, USB interface

- SMBUS slave and I2C master

- Buck-Boost Architecture. Min input of 6v up to 30V

- Programable output 6-24V

- Default output is 12V

- Input/Output current 6A/10A peak

- Charge between 6-30V, 3A

- Voltage and current limits

- Li-Ion, Li-Po, LIFEPO4***, Lead Acid

- 1, 2S, 3S, 4S, 5S, 6S

- Balancing up to 6S

- Coulomb counting (fuel gauge)

- Motherboard ON/OFF pulse control*




---
**Vernon Barry (Jetguy)** *January 31, 2017 03:42*

Again, you could absolutely use one of those, a couple of standard gel cell 12V batteries and even program it to send  a loss of power to send your printer into a pause mode until power returns. Yes, a bit expensive but so is a standard UPS and this is hugely efficient compared to converting to AC and then back to DC for no good reason. They also make a much simpler no frills 12V unit for SLA batteries [http://www.mini-box.com/picoUPS-120-12V-DC-micro-UPS-battery-backup?sc=8&category=1264](http://www.mini-box.com/picoUPS-120-12V-DC-micro-UPS-battery-backup?sc=8&category=1264)


---
*Imported from [Google+](https://plus.google.com/108496831286068938419/posts/Whz1sGkZVvX) &mdash; content and formatting may not be reliable*
