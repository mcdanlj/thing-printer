---
layout: post
title: "I was just looking at the \"Configuring a printer on Redeem with Marlin values\" section of ."
date: May 26, 2016 12:53
category: "Discussion"
author: Marcos Scriven
---
I was just looking at the "Configuring a printer on Redeem with Marlin values" section of [http://wiki.thing-printer.com/index.php?title=Marlin_to_redeem](http://wiki.thing-printer.com/index.php?title=Marlin_to_redeem).



I'm probably missing something, but it seemed to complicate something that seemed simple to me. A few thoughts:



1) It talks about '1/32 microstep, and you have 18-tooth GT-2 pulleys' being an example, but then quotes '200' steps/mm, whereas presumably it should read something like 177.77?  So when it says example, I think it should say 'In this format'



2) It says to use the calculator to find the number of teeth on your pulley - but one can just physically count them :)



3) Even then, it's recommending finding the number of teeth, by changing until the steps/mm matches your Marlin config. But if you know your Marlin config, you can simply divide it by your microstepping? In my case it was simply 64 steps/mm in Marlin, become 64/16 = 4 steps/mm in Redeem?



4) Is the Wiki possible to login to and suggest changes? I like Github Page for this reason, can just suggest things by way of a PR.





**Marcos Scriven**

---
---
**Elias Bakken** *May 26, 2016 15:50*

I've sent you a wiki login!


---
*Imported from [Google+](https://plus.google.com/117910047317867058880/posts/5kYDkDne7j4) &mdash; content and formatting may not be reliable*
