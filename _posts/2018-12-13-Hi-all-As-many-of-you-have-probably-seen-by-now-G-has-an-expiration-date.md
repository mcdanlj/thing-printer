---
layout: post
title: "Hi all, As many of you have probably seen by now, G+ has an expiration date"
date: December 13, 2018 12:59
category: "Discussion"
author: Jon Charnas
---
Hi all,



As many of you have probably seen by now, G+ has an expiration date. So as to make sure that we don't lose a good communication medium when this G+ community eventually gets shut down, what would be your preferred replacement tool/location?





**Jon Charnas**

---
---
**Jon Charnas** *December 13, 2018 15:32*

Some folks on Slack have deleted their G+ accounts and can't vote anymore. I will tally the counts of what they report to me here (and update this comment):

Facebook: 0

Reprap.org: 2

Dedicated forum: 1

MeWe.com: 0

Other: 0


---
**Chris Deister** *December 13, 2018 17:52*

Discord is worth considering. 


---
**Jon Charnas** *December 13, 2018 17:55*

**+Chris Deister** True, but we already have a Slack channel for instant contact. Self-invitation link is on the thing-printer in the Support menu.


---
**Michael K Johnson** *December 13, 2018 20:09*

Running a one-off forum will be extra work, is an information security risk if it's not maintained well, and will reduce participation versus a more general context...



I'd say that moving to another walled garden is about the worst choice, whether that is Facebook or MeWe. Out of the frying pan into the fire...


---
**Jon Charnas** *December 14, 2018 06:41*

**+Michael K Johnson** The main problem is that Google+ will be shutting down, and what I'm asking for is what do we replace this community with? While I'm not a fan of walled gardens, I do believe in having a location for the community to share. We also have a slack team, but it doesn't serve the same purpose, in my view at least.


---
**Michael K Johnson** *December 14, 2018 11:11*

If a walled garden is OK, make it one that doesn't hide content behind login, so that those who don't want to or have specific reasons not to authenticate to it don't have to. That rules out at least Facebook and MeWe. Consider a dedicated subreddit as an alternative. Easy to set up, publicly searchable, people have heard of it, relatively low barrier to entry.


---
**Jon Charnas** *December 14, 2018 11:24*

**+Michael K Johnson** that is what I would call the reprap forum area in that case. Your concerns are well noted. Again, it is an optional area of communication. We have a very active (~400 members) slack for replicape and Manga Screen which is a versatile option. But again, walled garden kind of initiative.


---
**Michael K Johnson** *December 14, 2018 11:48*

**+Jon Charnas** yeah, that's why I voted reprap! I still think it's the best idea. I was just mentioning reddit as the garden with the most open walls. ;)


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/8soBLWRAkFW) &mdash; content and formatting may not be reliable*
