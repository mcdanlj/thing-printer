---
layout: post
title: "I design the cover for Replicape and BBB"
date: August 24, 2016 20:20
category: "Discussion"
author: Daniel Kusz
---
I design the cover for Replicape and BBB. Not finished yet. Ultimately, I want to put it at the bottom of the printer The Thing with bringing the airflow of the fan.  It will also simply version to safely store or move the unit (pocket 3D printer brain). After finishing I will put designs on Thingiverse.﻿

![images/e7f383d02ee727b463de9fe9fed60b7d.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/e7f383d02ee727b463de9fe9fed60b7d.jpeg)



**Daniel Kusz**

---
---
**Dan Cook** *September 06, 2016 14:18*

I was looking for one of these the other day. I wonder if mounting a fan to the top or bottom would work well for cooling... may still need some heat sinks.


---
**Daniel Kusz** *September 06, 2016 14:21*

There will be two fans. I've done printed prototype but I need to make improvements and print final version. Soon on Thingiverse. 


---
**Henrik Söderholtz** *January 26, 2017 11:08*

Did you ever get this up on Thingiverse?


---
**Daniel Kusz** *January 26, 2017 11:37*

Yes :-) 

[https://www.thingiverse.com/thing:1789792](https://www.thingiverse.com/thing:1789792)


---
**Henrik Söderholtz** *January 26, 2017 17:18*

Turns out I just asked you for the files... ^^


---
*Imported from [Google+](https://plus.google.com/103041373788524358316/posts/ZsgoBBcvfRr) &mdash; content and formatting may not be reliable*
