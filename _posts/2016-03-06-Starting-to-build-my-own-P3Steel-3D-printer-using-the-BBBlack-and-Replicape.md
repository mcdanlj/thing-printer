---
layout: post
title: "Starting to build my own P3Steel 3D printer using the BBBlack and Replicape"
date: March 06, 2016 14:46
category: "Discussion"
author: Kenneth Tan Fotografie
---
Starting to build my own P3Steel 3D printer using the BBBlack and Replicape. 

Need some pointers on the software side.

Can you advice on what image/software for the BB?

Or are there preconfigured images that cover all bases that I could download?



Having the frame lasercut this week and assembling the BOM right now.

Have the BBBlack and Replicape in house already









**Kenneth Tan Fotografie**

---
---
**Elias Bakken** *March 06, 2016 14:47*

This is what you need: [http://wiki.thing-printer.com/index.php?title=Kamikaze](http://wiki.thing-printer.com/index.php?title=Kamikaze)




---
**Jon Charnas** *March 06, 2016 15:04*

Also, if you're coming from Marlin/Repetier-type firmware, read this guide on how to re-learn how to do your settings with this page (still under construction): [http://wiki.thing-printer.com/index.php?title=Marlin_to_redeem](http://wiki.thing-printer.com/index.php?title=Marlin_to_redeem)


---
**Kenneth Tan Fotografie** *March 06, 2016 15:44*

I'm coming from nowhere. Total newbee. Thanx Jon! Elias, going to implement your Wiki! thanx 


---
*Imported from [Google+](https://plus.google.com/101666459971170251704/posts/MaksLuzKzFP) &mdash; content and formatting may not be reliable*
