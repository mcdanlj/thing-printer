---
layout: post
title: "I'm playing with the Manga Screen 2 connected to a raspberry pi 3 and noticed that for some reason the FPS is limited to 20"
date: July 09, 2018 07:56
category: "Discussion"
author: Jean Le Flambeur
---
I'm playing with the Manga Screen 2 connected to a raspberry pi 3 and noticed that for some reason the FPS is limited to 20. I'm using a QT application that works at 60 FPS on the official RPI display but on the MS2 it's limited to 20 for some reason.

It seems the vsync happens every 50ms, because if I use a swapInterval of 1, I get 20 FPS. With 2 (so a swap every 2 vsyncs) I get 10 FPS and so on.

Any clues?

My hdmi timings are these:

1080 1 100 10 60 1920 1 4 2 4 0 0 0 60 0 144000000 3



I also notice tearing when the console text scrolls and even that is overall slow.





**Jean Le Flambeur**

---
---
**Andy Williams** *July 09, 2018 16:27*

Hi Jean.. What did you do to get your Pi working ? Did you change config.txt etc ? 


---
**Elias Bakken** *July 09, 2018 23:47*

There should be several framerates supported in the edid and by the screen, 60 being the highest. Could you try it on a computer for debugging this? It should give you a list of supported framerates more easily to experiment with. The tearing I cannot do anything with I think. There is a sync pin coming from the screen, but there is no way to convey that back to the computer. 


---
**Jean Le Flambeur** *July 10, 2018 21:47*

**+Elias Bakken** The problem is caused by the display_rotation in the config.txt: when rotating by 90 or 270 degrees the FPS drops to 20. The issue is detailed here: [github.com - Poor GPU performance (with OpenVG) in Portrait Mode · Issue #403 · raspberrypi/firmware](https://github.com/raspberrypi/firmware/issues/403)




---
**Elias Bakken** *July 10, 2018 21:49*

Oh... I was not aware of that. Crap. 


---
**Elias Bakken** *July 10, 2018 21:54*

Can you rotate the content in the app using EGL? I'm doing that in my app Toggle. I'm not sure if this is an issue with BBB though. 


---
*Imported from [Google+](https://plus.google.com/117821889871231142040/posts/dt1GdLfsJu2) &mdash; content and formatting may not be reliable*
