---
layout: post
title: "The new standard it seems : ) Originally shared by Prusa 3D by Josef Prusa"
date: September 20, 2016 16:28
category: "Discussion"
author: Elias Bakken
---
The new standard it seems : )



<b>Originally shared by Prusa 3D by Josef Prusa</b>





**Elias Bakken**

---
---
**Jon Charnas** *September 20, 2016 16:44*

Won't be standard if it's still using an Arduino firmware... There are more Marlin forks than there are in my grandmother's silverware. 


---
**Daniel Kusz** *September 20, 2016 16:45*


{% include youtubePlayer.html id=Kfe_84FGJ8c %}
[youtube.com - Review: The Original Josef Prusa i3 MK2 🏅 It doesn't get any better than this!](https://www.youtube.com/watch?v=Kfe_84FGJ8c)


---
**Jon Charnas** *September 21, 2016 09:21*

That's what the bed probing and bed leveling is for, no?


---
**Jon Charnas** *September 21, 2016 09:43*

No, if your Z is not orthogonal, it would assume that the surface is not level and compensate for it - it's what the bed leveling matrix does... How it works if the gantry is also tilted at a 20 degree angle though, I don't know.


---
**Jon Charnas** *September 21, 2016 11:08*

Err... Right, I see what you mean, because your first reference point would always be at the same layer height.


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/76zPGaGe5DJ) &mdash; content and formatting may not be reliable*
