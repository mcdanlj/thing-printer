---
layout: post
title: "I found that due to job reasons I no longer have time for 3D printing and now resort to shipping my stuff off to have it printed elsewhere, which is at the same times much less smelly, at least for ABS ;-) Would anyone be"
date: November 05, 2018 12:27
category: "Discussion"
author: Karl Pitrich
---
I found that due to job reasons I no longer have time for 3D printing and now resort to shipping my stuff off to have it printed elsewhere, which is at the same times much less smelly, at least for ABS ;-)



Would anyone be interested in a set of BBB + Replicape + Manga Screen 2 5,9", the latter just tested 10mins once)? I can also offer lots of other 3D printing accessories, several RAMPS, motors, sensors, mechanics, etc.



Items are located in Germany, shipping via DHL possible.







**Karl Pitrich**

---
---
**Brian Jacoby** *November 18, 2018 23:07*

Hi Karl, G+ makes it amazingly hard to contact you directly.  Interested in your Replicape+BBB setup.


---
**Karl Pitrich** *November 26, 2018 13:19*

**+Brian Jacoby** I've sent a hangout chat invitation, also could not figure out how to message you, G+, RIP :)


---
*Imported from [Google+](https://plus.google.com/+KarlPitrichOfficial/posts/hGxjHsegfBi) &mdash; content and formatting may not be reliable*
