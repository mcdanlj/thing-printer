---
layout: post
title: "What does \" Alarm: Stepper Y\" means?"
date: April 02, 2016 18:37
category: "Discussion"
author: Terabyte Johnson
---
What does " Alarm: Stepper Y" means? I can't get my steppers working properly, but I don't know if my settings are wrong, or it's thinking that endstops are triggered and does not let them move.

Steppers seem to work somehow when connected to E or H, but when connected to X,Y,Z they do not. My endstops are normally closed, either "true" or "false" does not seem to make a difference. Increasing Redeem log level did not elaborate on what the error is. Below is a typical output of Redeem.

Thanks.



2016-04-02 18:22:21,972 root         INFO     Found Replicape rev. 00B3

2016-04-02 18:22:22,162 root         INFO     Delta calibration calculated. Current settings:

2016-04-02 18:22:22,168 root         INFO     Column A(X): A_radial=0.0mm A_tangential=0.0mm

2016-04-02 18:22:22,173 root         INFO     Column B(Y): B_radial=0.0mm B_tangential=0.0mm

2016-04-02 18:22:22,178 root         INFO     Column C(Z): C_radial=0.0mm C_tangential=0.0mm

2016-04-02 18:22:22,183 root         INFO     Radius (r) = 198.25mm  Rod Length (L)= 290.8mm

2016-04-02 18:22:22,252 root         INFO     Cooler connects therm E with fan 1

2016-04-02 18:22:22,259 root         INFO     Added fan 0 to M106/M107

2016-04-02 18:22:22,267 root         INFO     Added fan 3 to M106/M107

2016-04-02 18:22:22,582 root         INFO     Stepper watchdog started, timeout 60 s

2016-04-02 18:22:22,587 root         INFO     Ethernet bound to port 50000

2016-04-02 18:22:22,755 root         INFO     Pipe octoprint open. Use '/dev/octoprint_1' to communicate with it

2016-04-02 18:22:22,927 root         INFO     Pipe toggle open. Use '/dev/toggle_1' to communicate with it

2016-04-02 18:22:23,100 root         INFO     Pipe testing open. Use '/dev/testing_1' to communicate with it

2016-04-02 18:22:23,263 root         INFO     Pipe testing_noret open. Use '/dev/testing_noret_1' to communicate with it

2016-04-02 18:22:23,297 root         INFO     Watchdog started, refresh 30 s

2016-04-02 18:22:23,312 root         INFO     Redeem ready

2016-04-02 18:22:23,292 root         INFO     Alarm: Operational

2016-04-02 18:22:44,695 root         INFO     Coordinate system set to 0

2016-04-02 18:23:08,094 root         ERROR    Alarm: Stepper Y

2016-04-02 18:23:10,306 root         ERROR    Alarm: Stepper Z







**Terabyte Johnson**

---
---
**James Armstrong** *April 02, 2016 21:50*

What is your stepper current set to in the config? That alarm usually means the stepper driver has a fault such as over heating. The current setting does not appear to be a 1to1 current. I use .5 which seems more like 1amp to the motors. 


---
**James Armstrong** *April 02, 2016 23:58*

Well it looks like you hadn't even started printing so it is probably not a heat issue. 


---
**Veridico Cholo** *April 03, 2016 00:00*

The same thing happened to me. The motor kept  moving but the alram message kept showing up. As **+James Armstrong** says, lowering the current works


---
**Terabyte Johnson** *April 03, 2016 01:13*

Yeah, at this point, I wish it was the heat issue:) Thanks, lowering the current worked with mixed results. At first, it seemed that it was going to work, but then it still showed alarms. I've tried .7, .5, .3 and even 1mA with same result. Not sure why, but I have only Y and Z alarms. Although Y and Z do power up, they do not move. Reinstalling of Redeem and using defaults did not change anything.


---
**Jon Charnas** *April 03, 2016 20:53*

That is odd... However correction - the current value is normally in amperes - 1 means 1A. I could be wrong but that's what I remember from discussions on #replicape on IRC.


---
**Terabyte Johnson** *April 03, 2016 21:58*

**+Jon Charnas** Thanks Jon, yes 1 in the xxxx.cfg means 1A, I did not want to type 0.001, so I've used 1mA instead.


---
**James Armstrong** *April 03, 2016 22:29*

I wonder why when I use anything over .5-.7 my motors get extremely hot and they are rated for 1.5 amps. My Z had to be set to .08 and it is a .45amp motor and it would melt my bracket when set much higher 


---
**James Armstrong** *April 03, 2016 22:31*

It is irms however is that makes a difference. 


---
**Terabyte Johnson** *April 03, 2016 23:48*

To make sure I did not miss anything obvious, I would like to share what I did. I have BBB Rev C (from Element 14) with Replicape Rev 00B3. I've installed Kamikaze from February 16 2016. I use two different stepper models one from Rostock Max V2.4 [http://www.automationtechnologiesinc.com/products-page/3d-printer/nema17-stepper-motor-kl17h247-150-4a-for-3d-printer](http://www.automationtechnologiesinc.com/products-page/3d-printer/nema17-stepper-motor-kl17h247-150-4a-for-3d-printer) and another from [http://www.omc-stepperonline.com/3d-printer-nema-17-stepper-motor-59ncm84ozin-2a-17hs192004s-p-18.html](http://www.omc-stepperonline.com/3d-printer-nema-17-stepper-motor-59ncm84ozin-2a-17hs192004s-p-18.html) The result is the same "Error: Alarm Stepper ...." regardless of the current specified in the config. 



I assume that pinout on replicape goes 1,2 for the 1st coil, and 3,4 for the 2nd coil (A2 A1, B1 B2), which moves my steppers in the opposite direction, so I have connectors flipped (Blk, Grn, Red, Blu or Red, Grn, Yel, Blu, going from the +12 V power connectors' side).



Judging by what I see on a scope, driver produces something when move commands are issued, but they may or may not move the stepper. Sometimes I see alarms even without a stepper connected.






---
**Terabyte Johnson** *April 04, 2016 02:56*

**+James Armstrong** Well, if it is rms in the config, what is the current steppers are rated at? I believe it is also rms! (if it were DC, rms would be the same as the peak current). So using the proper current in the config should not be a problem. However, TMC2100 driver is rated at 1.2A rms continous, and 2.5A rms short time. Thus, as long as the current stays under 1A rms we should have no problems, right? This does not explain, why a stepper rated at 0.45A requires 0.08A in the config. The driver senses high internal temperature (could reach over 100C), short to ground (2-3V), and some 5V undervoltage conditions.



Normally, the current is determined by the driver setting, and as long as it is under about 1A RMS it should not trigger errors. Unless there is something else that is triggering the alarms. It would be great to find out. Although my problem may be something other than the current setting, I see that the stepper current settings came up several times, and it would be nice if somebody provided a little more detailed explanation


---
*Imported from [Google+](https://plus.google.com/107971138403694534981/posts/aef1kjT8dZz) &mdash; content and formatting may not be reliable*
