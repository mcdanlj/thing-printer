---
layout: post
title: "Elias Bakken , while you weren't on it was discussed that the Kamikaze image has an empty default root password, and ssh root login is not disabled"
date: January 27, 2016 21:58
category: "Discussion"
author: Jon Charnas
---
**+Elias Bakken**, while you weren't on #replicape it was discussed that the Kamikaze image has an empty default root password, and ssh root login is <b>not</b> disabled. I think this is something that needs to be fixed in the next release, and people using the current version need to know about the potential risk this poses as well if connected on the open internet.





**Jon Charnas**

---
---
**James Armstrong** *January 27, 2016 22:00*

Didn't know there was an irc channel, I'll have to get on it. There are a few other minor issues with the latest image, mainly with octoprint, pip and permissions that I can figure my way around but should probably be fixed. 


---
**Jon Charnas** *January 27, 2016 22:09*

Yep, on freenode. Channel title is "secret replicape club". So, woops, I gave it away.


---
**Elias Bakken** *January 28, 2016 13:56*

Yes, this image is very open. I'll be happy to close that gap if you want and push out a new image. 


---
**Elias Bakken** *January 28, 2016 13:58*

This has been the default setup for the standard Debian images for a long time, so I've just copied that. 


---
**Jon Charnas** *January 28, 2016 14:05*

Doesn't necessarily deserve a new image by itself as long as people are aware of the risk - which not everyone seemed to be. Fix it for next release, maybe?


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/1oJkhTqaytD) &mdash; content and formatting may not be reliable*
