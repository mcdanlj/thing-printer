---
layout: post
title: "Manga Screen seems to be wonky. Less than half the screen responds to touch, and the side looks weird"
date: June 19, 2016 23:29
category: "Discussion"
author: Alexander Borsi
---
Manga Screen seems to be wonky. Less than half the screen responds to touch, and the side looks weird.



I was instructed by Elias to run a command of:



cat /sys/devices/platform/ocp/4830e000.lcdc/drm/card0/card0-HDMI-A-1/edid | parse-edid



And when I do I get "Partial Read... Try Again"



What does this mean and/or how do I fix it?





![images/73cfe2f25830ed83b90e0049c08186df.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/73cfe2f25830ed83b90e0049c08186df.jpeg)
![images/6db653a070a9165f80d0a365d1b8795a.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/6db653a070a9165f80d0a365d1b8795a.jpeg)

**Alexander Borsi**

---
---
**Elias Bakken** *June 27, 2016 19:27*

For some reason the EDID is not read right. This is either due to a bad cable (I get around 50% working cables, terrible) or a loose HDMI connector. Do you have a way to test it with a different cable? An A to D cable can be used with computer.


---
**Alexander Borsi** *June 30, 2016 03:04*

I hooked it up to a RPI2 and it shows up, and I can rotate the screen around, but I can't get the EDID info on it in RPI land.


---
**Alexander Borsi** *July 07, 2016 03:14*

Still broken. make get_edid blows up and errors out.


---
*Imported from [Google+](https://plus.google.com/102644518510093053332/posts/TTYzBZmif6r) &mdash; content and formatting may not be reliable*
