---
layout: post
title: "Preparing for Z probing with switch: Servo range is 180 degrees"
date: November 09, 2016 17:15
category: "Discussion"
author: Daniel Kusz
---
Preparing for Z probing with switch:




{% include youtubePlayer.html id=Pw6EZ56lE60 %}
[https://www.youtube.com/watch?v=Pw6EZ56lE60&feature=em-upload_owner](https://www.youtube.com/watch?v=Pw6EZ56lE60&feature=em-upload_owner)



 Servo range is 180 degrees. Any ideas why its going only in about 170 degrees range? I use M280 P0 S180 command.





**Daniel Kusz**

---
---
**Jon Charnas** *November 09, 2016 19:07*

Different servo brands have different pwm settings for neutral. Maybe it needs to be adjusted?


---
**Elias Bakken** *November 09, 2016 19:15*

You can set the pulse length in the settings file, so if it works with a different controller, you can recreate that with redeem!


---
*Imported from [Google+](https://plus.google.com/103041373788524358316/posts/c8cyfJfyw4C) &mdash; content and formatting may not be reliable*
