---
layout: post
title: "Hello, I am quite new to 3D printing and redeem"
date: July 17, 2016 12:26
category: "Discussion"
author: Mathias Giacomuzzi
---
Hello,



I am quite new to 3D printing and redeem. So the last days I was able to set up a kossel-minimax ([https://github.com/mosfet/kossel-minimax](https://github.com/mosfet/kossel-minimax)).



So I had a few problems or task which I not really clear how to solve that.



1) How setup to the extruder the right way concerning speed and resolution. So for the resolution I tried to extrude 50mm as an example and measured the actual extrusion. So I corrected the steps_pr_mm_e as log as I get the correct length. For the speed I found out that the speed is set in the printer profile. But how fast or how slow should the speed be? Are there any recommendations. I also think the speed should be changed for different different nozzel diameter – right? So I tried a little bit and I taught 51mm/min was okay for a 0.4mm nozzel for me but I am no sure. So is there any formula or table around how to set up that correctly?



2) End stop

Does someone knows a good end stop? Is it better to use an mechanical or optical/capacitive solution? If they the switch point is not exactly the same – what are the right config values to correct them for each joint. Should I use offset_x, offset_y and offset_z for that?



3) Z axis

So I on a cnc milling machine the normal way before each job is to set the part zero-point. So for the z-axis I jog to that position and use a touch off feature to set the home position to 0 for that axis.

So how to solve that in redeem – without a z probe? By the way does someone knows a good z probe for redeem? Should I use a little offset between the bed and the nozzel for printing?



4) Is there any video around how to use a probe the correct way.



Thanks for the help





**Mathias Giacomuzzi**

---
---
**Jon Charnas** *July 17, 2016 15:23*

Hi Mathias, for your extruder setup, I would recommend that you look at what volume per second you're going to be extruding. From there use the filament diameter to get the cubic mm / second needed, and set your maximum extruder speed to about 10% more. Remember you're only setting the upper limits in Redeem. Your slicer will be managing the changes in extruder speeds as needed.



For endstops, either mechanical or optical are equivalent. I have a Kossel Mini with mechanical endstops and they work perfectly fine. And yes, the offset_x, offset_y and offset_z values are ideal for setting the proper heights, but lookup M206 and M500 in the Redeem wiki page - it will let you interactively change and save the offsets to get the perfectly level values you want for a delta.



For Z height calibration - that's where your endstops come in. I don't have a Z-probe on my Kossel and I print perfectly fine, every time. It took me a good 2 weeks to calibrate the endstop offsets and print radius value properly, but on a Delta-style printer, the bed doesn't move relative to the endstops, ever. So endstops define where your reference point is for the machine. That said, most probes are either inductive (if you have a metal plate as bed) or a mechanical switch (same as endstop usually). As far as I know Elias is writing the documentation on Z-probe still, as he recently changed the code to better support auto-leveling.


---
**Mathias Giacomuzzi** *July 17, 2016 17:33*

Hello Jon,  



Thanks for your reply, do you have any good guide for calibration?


---
**Jon Charnas** *July 17, 2016 17:39*

Hi Mathias,



Yes, there's a link to what kind of movement you observe the print head doing when values aren't set correctly in the wiki's Redeem Delta section: [http://wiki.thing-printer.com/index.php?title=Redeem#Delta](http://wiki.thing-printer.com/index.php?title=Redeem#Delta) For evaluating nozzle height above the bed, I recommend a business card, and do all these tests with nozzle and bed cold first. Then last step is to do it when both at printing temp, to see if there's any deformation due to heat expansion.


---
**Mathias Giacomuzzi** *July 17, 2016 17:53*

Hello Jon,



Concerning:  



>>I would recommend that you look at what >>volume per second you're going to be >>extruding. From there use the filament >>diameter to get the cubic mm / second >>needed, and set your maximum extruder >>speed to about 10% more



Okay I understand but I have no idea what volume per second I need or would have. Do you have any example here what you exactly mean?



thanks


---
**Jon Charnas** *July 18, 2016 07:24*

Honestly I wouldn't bother looking too deeply into it - if you're not extruding at light speeds you're ok. Take the max speed of your motion platform and apply the reduction ratio to it (i.e. if your extruder is reducted 5:1, divide max speed of X by 5 and you'll have a good upper limit of how fast your motor can go). That's how I defined the limits on mine - pushing PLA through the nozzle at those speeds works just fine for me - but the slicer will do flow calculations and adjust as needed for the prints. Typically it's much slower than full speed.


---
*Imported from [Google+](https://plus.google.com/104869072343043179738/posts/LjT72pW3Dhg) &mdash; content and formatting may not be reliable*
