---
layout: post
title: "Here are my start code comparisons. Slic3r that doesn't work now, but worked on RAMPS/Marlin G21 G91 G28 ;"
date: July 28, 2016 19:38
category: "Discussion"
author: James Brassill
---
Here are my start code comparisons.



Slic3r that doesn't work now, but worked on RAMPS/Marlin

G21

G91

G28 ; home all axes

G29

G1 Z5 F5000 ; lift nozzle



Cura that works with Redeem/Replicape

G21        ;metric values

G90        ;absolute positioning

M82        ;set extruder to absolute mode

M107       ;start with the fan off

G28 X0 Y0  ;move X/Y to min endstops

G28 Z0     ;move Z to min endstops

G1 Z15.0 F{travel_speed} ;move the platform down 15mm

G92 E0                  ;zero the extruded length

G1 F200 E3              ;extrude 3mm of feed stock

G92 E0                  ;zero the extruded length again

G1 F{travel_speed}

;Put printing message on LCD screen

M117 Printing...





Though I think the issue has something to do with that relative vs absolute extruder distances. I might try slic3r with no start gcode and put the cura start gcode in octoprint and see what happens.





**James Brassill**

---
---
**Jon Charnas** *July 28, 2016 19:53*

Here's my start G-Code:

; Default start code

; M80 ; Start ATX PSU if not already on.

G28 ; Home extruder

; G1 Z200 F4000 ; bring the head down before starting to move, disable when using G29

; G29 ; auto probe

M107 ; Turn off fan

G90 ; Absolute positioning

; M82 ; Extruder in absolute mode

{IF_BED}M190 S{BED}

; Activate all used extruder

{IF_EXT0}M104 T0 S{TEMP0}

G92 E0 ; Reset extruder position

M116 ; Wait for all temperatures set to be reached.

{IF_EXT0}M109 T0 S{TEMP0}



As you can see, I also have a G90 and G92... Maybe try to remove the G21? This is also ported over from my ramps...


---
**James Brassill** *July 28, 2016 19:55*

I will give that a try tonight, thanks!


---
**James Brassill** *July 29, 2016 03:00*

**+Jon Charnas** You have been very helpful, a mix of your start code and my cura code placed in Octoprints start section worked to get slic3r going. I am not sure why placing said same code in slic3rs start code section caused the printer to move so slow.


---
**Jon Charnas** *July 31, 2016 15:27*

I have no idea either - glad it worked!


---
*Imported from [Google+](https://plus.google.com/+JamesBrassill/posts/Xi9BWFj7ygv) &mdash; content and formatting may not be reliable*
