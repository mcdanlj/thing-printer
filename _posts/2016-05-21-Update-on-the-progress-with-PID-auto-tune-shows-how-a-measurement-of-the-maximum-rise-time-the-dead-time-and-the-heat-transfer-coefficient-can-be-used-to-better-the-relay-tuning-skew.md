---
layout: post
title: "Update on the progress with PID auto-tune shows how a measurement of the maximum rise time, the dead time and the heat transfer coefficient can be used to better the relay tuning skew"
date: May 21, 2016 17:11
category: "Discussion"
author: Elias Bakken
---
Update on the progress with PID auto-tune shows how a measurement of the maximum rise time, the dead time and the heat transfer coefficient can be used to better the relay tuning skew. The tuning is completed on the -16 min mark here, and the testing shows that the P and I-values are perhaps too low. But the biggest problem with this tuning method is that it does not work reliably in the face of forced convection, IE a fan blowing on it. Even though the pre-calibration and tuning setup only works without a fan blowing, it should serve as a starting point for determining the right values for the PID and then move on to making it more robust.  

![images/663e8c118109851f170ea079c8f4a6f1.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/663e8c118109851f170ea079c8f4a6f1.png)



**Elias Bakken**

---
---
**Ryan Carlyle** *May 21, 2016 20:49*

I would argue that having a fan blowing on the hot end is bad hardware design and should be addressed in hardware, not software... But it's pretty common for people to run auto-tune with the hot end airflow at max. Does that work here?


---
**Elias Bakken** *May 21, 2016 21:59*

I've not tried having a fan blowing on the hot end, but I've tried having active cooling on the cold part of the hot end and that also makes the calculations wrong. From what I can tell, the calculations for both max heat rate and cooling constant seems right, but the set point power value is not right. Overshoots by about 25 degrees every time.


---
**Ryan Carlyle** *May 21, 2016 22:19*

What kind of hot end are you using for these tests?


---
**Elias Bakken** *May 21, 2016 22:21*

I've got a hexagon hot end that I'm testing with now, but I've got an E3D that can be hooked up as well. Actually, let me do that right away...


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/4XmXbY9PVRA) &mdash; content and formatting may not be reliable*
