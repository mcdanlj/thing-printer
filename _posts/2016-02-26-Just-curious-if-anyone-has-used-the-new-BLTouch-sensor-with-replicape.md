---
layout: post
title: "Just curious if anyone has used the new BLTouch sensor with replicape?"
date: February 26, 2016 00:34
category: "Discussion"
author: Philip Acierto
---
Just curious if anyone has used the new BLTouch sensor with replicape? Seems straight forward and I believe would work as it only needs a servo and end stop connection. Might work much more reliably than my inductive sensor on a glass bed...





[https://www.indiegogo.com/projects/bltouch-auto-leveling-sensor-for-3d-printers#/story](https://www.indiegogo.com/projects/bltouch-auto-leveling-sensor-for-3d-printers#/story)





**Philip Acierto**

---
---
**_-KaT-_ “KaTZWorld”** *February 26, 2016 01:46*

I have one on a ramps board and love it... I have the replicape and have looked at hooking up also looks pretty easy it's the configuration that has me stumped 


---
**Andrew Dowling** *June 08, 2016 01:29*

Did you manage to get your probe to deploy? I couldn't even get the probe to deploy and retract. I had it hooked to the 5v and ground pins via jumpers for power. I attached what acts as the servo data to first the end stop and then directly to the GPIO pin second. Also, I could never get Zmin to trigger, even though I attached the output to the Z1.

If you got further than me, could you get me a snapshot of your config file and something on your wiring?


---
**_-KaT-_ “KaTZWorld”** *June 08, 2016 01:45*

I have shelved this till the delta I got coming get here ... then I will try again but for now I am working with ramsp based board and bltouch setups with no problem


---
**Andrew Dowling** *June 09, 2016 11:44*

That's too bad. I got a delta I built custom I want to use the sensor on. somewhere between the wiring and the not really defined required servo timing, I simply didn't get anything above and beyond powering up the sensor.


---
*Imported from [Google+](https://plus.google.com/111974602640488998735/posts/NTE7DCryhvj) &mdash; content and formatting may not be reliable*
