---
layout: post
title: "I get around 15-20s of stepper noise at startup"
date: March 16, 2017 08:40
category: "Discussion"
author: Henrik Söderholtz
---
I get around 15-20s of stepper noise at startup. Is this normal? The wiki says about 3s. It's quite (read very) annoying. =) Any way to get rid of it?





**Henrik Söderholtz**

---
---
**Henrik Söderholtz** *March 16, 2017 22:32*

What's the hardware fix? I believe I have B3. Patchable?


---
**Sergey Kirillov** *March 30, 2017 14:40*

I'm having same problem. It takes even more than 20s in my case and it is quite annoying. Is there anything I can do to fix it?


---
*Imported from [Google+](https://plus.google.com/103752299402481876381/posts/g76Dvw512s1) &mdash; content and formatting may not be reliable*
