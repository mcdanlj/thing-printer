---
layout: post
title: "So I just picked up one of these screens and I'm not sure where to start with getting the touch screen to play nice"
date: July 24, 2017 21:00
category: "Discussion"
author: seekerofthetruths
---
[https://www.amazon.com/gp/product/B01ID5BQTC/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B01ID5BQTC/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1)   So I just picked up one of these screens and I'm not sure where to start with getting the touch screen to play nice.  The display works real nice once I set the screen rotation but I have no clue how to update the drivers. If someone could point me in the right direction I would be very appreciative. And yes I know the new Manga screen is coming out but i need instant gratification :P





**seekerofthetruths**

---
---
**seekerofthetruths** *July 24, 2017 22:49*

If it helps any it is supposed to work with Angstrom out of the box..


---
**Leo Södergren** *July 25, 2017 10:54*

Might be the older style waveshare touchscreens, if so you cant do much except try to make your own driver. Try plugging it in to your computer and see if i registers as a bar-code-scanner or a touch screen

[waveshare.com - 7inch HDMI LCD (B) - Waveshare Wiki](http://www.waveshare.com/wiki/7inch_HDMI_LCD_%28B%29)


---
**Leo Södergren** *July 25, 2017 10:55*

This is supposed to be a fix but i'm not smart enough to get it to work. [github.com - derekhe/waveshare-7inch-touchscreen-driver](https://github.com/derekhe/waveshare-7inch-touchscreen-driver)


---
*Imported from [Google+](https://plus.google.com/111487005800269310962/posts/7USVaAcUF64) &mdash; content and formatting may not be reliable*
