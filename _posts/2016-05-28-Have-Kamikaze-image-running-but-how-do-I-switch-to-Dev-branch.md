---
layout: post
title: "Have Kamikaze image running, but how do I switch to Dev branch?"
date: May 28, 2016 10:31
category: "Discussion"
author: Børre Evensen
---
Have Kamikaze image running, but how do I switch to Dev branch?

And the temp for hotted and heated its showing, but i can't heat it up.

What have I missed?





**Børre Evensen**

---
---
**Elias Bakken** *May 28, 2016 10:59*

To use the Develop branch you have to clone the redeem git repo, change branch and install the software from there. Is the led for the heater coming on when you set a temperature?


---
**Børre Evensen** *May 28, 2016 11:47*

That was just what I did with the Develop branch, and it hung on the noise part on boot. Have fixed that now.

Just found out that the hotend and bed heat, but Octoprint dont show it. Its standing at Off there. Have tried to refresh the browser.


---
**Elias Bakken** *May 28, 2016 12:25*

Can you see what gets returned in the M105 calls in the octoprint terminal?


---
**Børre Evensen** *May 28, 2016 15:20*

The Terminal dont update, and I can't send commands. So its a octoprintproblem


---
**Elias Bakken** *May 28, 2016 15:48*

Is this the standard version of OctoPrint that comes with Kamikaze or did you install a different version?


---
**Børre Evensen** *May 28, 2016 16:05*

Its the default, with a naggy update message. Running from a Beaglebone (white) and SD.


---
**Jon Charnas** *May 28, 2016 19:35*

You may want to install octoprint through pip to get the latest version (stable). It's fixed a number of things.


---
**Børre Evensen** *May 28, 2016 19:46*

Great, do you know any easy guide on this pip install?


---
**Jon Charnas** *May 28, 2016 19:55*

This is what I came up with - but it's not ideal really, it still has issues updating octoprint plugins automatically.


---
**Jon Charnas** *May 28, 2016 19:57*

[https://github.com/eliasbakken/kamikaze/issues/1](https://github.com/eliasbakken/kamikaze/issues/1)


---
**Jon Charnas** *May 28, 2016 19:58*

Sorry I forgot to put the link earlier :/


---
**Børre Evensen** *May 28, 2016 20:02*

Thank you :)


---
**Børre Evensen** *May 29, 2016 13:22*

I try to run your script, but it stop after it have downloaded from github.

Tried then to run rest of commands manual, but it stil on same version.

Set the checkout folder to "/home/octo/OctoPrint/" and run update to 1.2.11

It says Update successful, restart required

After restart its still "Installed: 1.2.8.post0.dev0+gdedadbc"

This is on a new kamikaze image


---
**Jon Charnas** *May 29, 2016 14:22*

Edit /etc/systemd/system/multi-user.target.wants/octoprint.service to point to /usr/local/bin/octoprint instead of /usr/bin/octoprint the latter is the one installed by the .deb package.


---
**Eric Trombly** *May 08, 2017 18:55*

The script stops because the Octoprint directory already exists, you need to delete it first. Just tried this method and it works.


---
*Imported from [Google+](https://plus.google.com/103356570987486549903/posts/Vt29R4xtf2B) &mdash; content and formatting may not be reliable*
