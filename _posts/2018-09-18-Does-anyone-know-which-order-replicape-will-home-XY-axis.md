---
layout: post
title: "Does anyone know which order replicape will home XY axis?"
date: September 18, 2018 16:44
category: "Discussion"
author: Erich Zeimantz
---
Does anyone know which order replicape will home XY axis? Additionally, is there a way to change the order (can't seem to find an answer in the docs)? 



I'm finishing up building a coreXY and have it mechanically built where Y needs to home before X. If not, then X will miss it's switch and the block will go off of it's rail. I did this to limit the moving wires on the X carriage. I can change the setup if needed but prefer to keep it.





**Erich Zeimantz**

---
---
**Uday Kumar Adusumilli** *September 18, 2018 17:36*

I have built a hypercube. first it homes x, then y.



Please do note that my left motor is connected to X stepper and my right motor to Y stepper. 


---
**Erich Zeimantz** *September 18, 2018 17:59*

**+Uday Kumar Adusumilli** That is what I was a bit worried about. I may end up redoing my end stops all together to fix it but was hoping for a different solution as I know other boards have ways to modify the sequence.


---
**Uday Kumar Adusumilli** *September 18, 2018 21:58*

I forgot to mention before, but my direction x and direction y are also inverted in local.cfg. But I dont think it really matters for which axis moves first.


---
**Uday Kumar Adusumilli** *September 20, 2018 07:15*

Sorry, cannot be of more help. Looked into the config, but cant see how to change the order




---
**Erich Zeimantz** *September 20, 2018 19:46*

Thanks for the help **+Uday Kumar Adusumilli**! I found a different setup that will eliminate the issue. Just waiting for new switches to arrive.


---
*Imported from [Google+](https://plus.google.com/115381714321898310646/posts/9hJrhLZia8h) &mdash; content and formatting may not be reliable*
