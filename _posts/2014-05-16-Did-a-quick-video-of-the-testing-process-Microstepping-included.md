---
layout: post
title: "Did a quick video of the testing process. Microstepping included"
date: May 16, 2014 15:31
category: "Discussion"
author: Elias Bakken
---
Did a quick video of the testing process. Microstepping included.

![images/ec5a7a1915df1cecd60461587f43d915.gif](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/ec5a7a1915df1cecd60461587f43d915.gif)



**Elias Bakken**

---
---
**Jimmy Edwards** *May 16, 2014 16:24*

Way to Go Elias it Looks Great !!!

I just got a bunch of parts in from #RobotDigg.com Shaft couplers, gears, belts, and linear bearings.  Good prices,,, but very long lead times out of China.  I guess that is to be expected when you select the lowest price for delivery.

I should be ready to go by the time my #Replicape arrives for my #BeagleBoneBlack.


---
**Elias Bakken** *May 16, 2014 16:26*

Charles and James Armstrong have gotten theirs so yours should not be far behind!


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/97rvLBUC8LQ) &mdash; content and formatting may not be reliable*
