---
layout: post
title: "Here is a short movie with homing routine on The Thing printer"
date: July 12, 2016 21:31
category: "Discussion"
author: Daniel Kusz
---
Here is a short movie with homing routine on The Thing printer. First: Z axis homing doesn't work after X and Y. Second: At the end strange behavior of the stepper during only Z axis homing. Today I have no idea what is causing this.




{% include youtubePlayer.html id=2o5-gUtTAuM %}
[https://www.youtube.com/watch?v=2o5-gUtTAuM](https://www.youtube.com/watch?v=2o5-gUtTAuM)





**Daniel Kusz**

---
---
**James Armstrong** *July 12, 2016 22:58*

Sounds like maybe the homing speeds are set too high for Z. I think mine are at 0.007 and backoff speed is 0.01


---
**James Armstrong** *July 12, 2016 22:59*

Also, if you move z down first then try and home does it attempt to home it? Maybe you don't have a backoff distance and if it is already hitting the end stop it will not hom


---
**Philip Acierto** *July 13, 2016 04:55*

I know that sound all too well. It is a z stepper motor movement speed that is too fast. Just slow it down til it moves as expected.


---
**Daniel Kusz** *July 13, 2016 08:14*

Thank you so much for help! I changed the speed at 0.005 and now works as it should :)


---
**Elias Bakken** *July 13, 2016 09:36*

Just a small efficiency thing, but I think you can home x and y simultaneously. Does that not work?


---
**Daniel Kusz** *July 13, 2016 11:15*

**+Elias Bakken** Yes, homing the X and Y running simultaneously. Now the homing Z also works. Only wonder how to do the homing of all axes in one cycle like on your video. But I still recognize the printer to work with Replicape and I have to learn more.


---
**James Armstrong** *July 13, 2016 11:57*

**+Daniel Kusz** does issuing a G28 with no parameters home all or does it still use a config option for the order?


---
**Elias Bakken** *July 13, 2016 13:58*

A clean G28 should home all axes that have an end stop.


---
**Daniel Kusz** *July 13, 2016 22:28*

**+James Armstrong** Now I'm using .cfg files.


---
*Imported from [Google+](https://plus.google.com/103041373788524358316/posts/UgRWbYdUKX1) &mdash; content and formatting may not be reliable*
