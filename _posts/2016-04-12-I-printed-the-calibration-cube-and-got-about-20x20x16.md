---
layout: post
title: "I printed the calibration cube and got about 20x20x16"
date: April 12, 2016 05:48
category: "Discussion"
author: Ken Runner
---
I printed the calibration cube and got about 20x20x16.  I tried changing the steps_pr_mm_z to multiple values, but don't seem to be getting the change in the Z axis that I would expect.  Is there a procedure for calibration?





**Ken Runner**

---
---
**Ken Runner** *April 15, 2016 16:16*

Turns out I just needed to keep trying, I eventually found a value that was about 20 steps-per-mm higher than my calculated value for z


---
**Daryl Bond** *April 17, 2016 01:29*

Good to hear that you got it sorted!


---
*Imported from [Google+](https://plus.google.com/117074313801696052366/posts/SBvyDDhHeie) &mdash; content and formatting may not be reliable*
