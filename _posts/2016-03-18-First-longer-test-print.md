---
layout: post
title: "First longer test print"
date: March 18, 2016 02:20
category: "Discussion"
author: Ante Vukorepa
---
First longer test print.



![images/18c7122bc7a0b3179498b2ca3c2f113b.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/18c7122bc7a0b3179498b2ca3c2f113b.jpeg)
![images/af223332e1230975708ca3b1efd02d5b.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/af223332e1230975708ca3b1efd02d5b.jpeg)
![images/cb1ec7e46f20d9a77566b52c5f5443c5.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/cb1ec7e46f20d9a77566b52c5f5443c5.jpeg)
![images/cec984a354f28258bdaeb4aa4ed25a60.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/cec984a354f28258bdaeb4aa4ed25a60.jpeg)

**Ante Vukorepa**

---
---
**Ante Vukorepa** *March 18, 2016 17:37*

ORD Bot Hadron that's been through 4 years of changes and modifications, D3D hotend and extruder, 0.4mm nozzle  (at the moment, i think), Oriental Motor steppers, 10mm/s perimeters, 20mm/s infill, extrusion width 0.3mm, 2 perimeters, 15% infill, 0.2117mm layers.


---
**Ante Vukorepa** *March 18, 2016 17:38*

Some comparison prints from 2 years ago: [http://www.thingiverse.com/make:69010](http://www.thingiverse.com/make:69010)


---
*Imported from [Google+](https://plus.google.com/105486978634901788220/posts/GHbzGv6tzqT) &mdash; content and formatting may not be reliable*
