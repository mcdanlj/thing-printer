---
layout: post
title: "Are these fans all 12v? I have 5v fans for cooling part where can I connect these to?"
date: August 25, 2016 16:50
category: "Discussion"
author: Step Cia
---
Are these fans all 12v? I have 5v fans for cooling part where can I connect these to? Thx

![images/cc7072146ddc4a81eb0acf59ebfec6f9.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/cc7072146ddc4a81eb0acf59ebfec6f9.png)



**Step Cia**

---
---
**James Armstrong** *August 25, 2016 17:01*

I believe those are all 12V, but the board switches the low side (GND) so if you make your own connector and go from the fan - and then run the power wire to a 5V source, it should work except a possible problem from the 12V going through a current limiting resistor then through an LED that may end up putting a small 12v signal on the fan- when it is not turned on. 




---
**Elias Bakken** *August 26, 2016 09:58*

There is 5V on the pins on the other side. The end stop pins. 


---
**Step Cia** *August 26, 2016 19:12*

k thanks are there setting somewhere I need to change if I were to use end stop pin for part cooling fans? I'd like to be able to control to cooling fans like first layer off and then 100% for bridging etc etc


---
**Elias Bakken** *August 27, 2016 09:51*

Most likely your slicer will produce M106 commands to control that fan. There are settings which allow you to choose which fans are affected by the M106. Like **+James Armstrong** says, the mosfet switches on the low side, so you should be able to use the GND pin (low) from the fan output and the +5V (high) from the end stop. 


---
**Daniel Kusz** *August 28, 2016 19:15*

About using fan outputs I'm thinking about connecting beeper to one of them that will signalize end of the print :)


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/Dawrn56xBZ8) &mdash; content and formatting may not be reliable*
