---
layout: post
title: "so trying to setup corexy, i use the config file for it thats in octoprint 'maxcorexy'"
date: January 28, 2017 07:52
category: "Discussion"
author: Andrew A
---
so trying to setup corexy, i use the config file for it thats in octoprint 'maxcorexy'.  

i make a pretty basic local config



[Steppers]



microstepping_x = 6

microstepping_x = 6

microstepping_x = 6

microstepping_x = 6

microstepping_x = 6



current_x = 0.5

current_y = 0.5

current_z = 0.5

current_e = 0.5

current_h = 0.5



steps_pr_mm_x = 10.0

steps_pr_mm_y = 10.0

steps_pr_mm_z = 50.0

steps_pr_mm_e = 26.15

steps_pr_mm_h = 50.0



in_use_h = True

slave_h = Z



[Endstops]



invert_X1 = False

invert_X2 = False

invert_Y1 = False

invert_Y2 = False

invert_Z1 = False

invert_Z2 = False



end_stop_Z1_stops = z_cw, h_cw

end_stop_Z2_stops = z_ccw, h_ccw



[Heaters]



sensor_E = SEMITEC-104GT-2



[Planner]



max_speed_x = 0.3

max_speed_y = 0.3

max_speed_z = 0.03

max_speed_e = 0.2

max_speed_h = 0.03



acceleration_x = 3.0

acceleration_y = 3.0

acceleration_z = 0.5

acceleration_e = 3.0

acceleration_h = 0.5



[Homing]



home_speed_x = 0.1

home_speed_y = 0.1 

home_speed_z = 0.1

home_speed_e = 0.01

home_speed_h = 0.1







So first of all, only the left and forward buttons work on the 'manual control', the up,down, right, back, does not.



i did alot of tweeking/changes, somewhere in there i got the down to work but the slave h did not move with z.....



eventually i just deleted everything and have just this



[Steppers]

current_x = 0.5

current_y = 0.5

current_z = 0.5

current_e = 0.5

current_h = 0.5



and again there's a bunch of buttons on the manual control not working



these motors run at 0.9 , and which buttons work and don't work changes, so this seems to be a problem with setup somewhere other than current but maybe im wrong.  i tried looking in the logs but nothing stood out to me anyway.  the only strange thing is in terminal on bootup it says number of extruders 2, regardless if i have in_use_h = True   or false, maybe that doesn't mean anything.



i do restart redeem after each local.cfg edit and connect.



also somewhere during all of that testing, when i pressed for example left, i would get movements that shouldn't have happened.



i feel like im missing something really obvious again, but also feel like just loading the corexy profile should have gotten me in the 'ballpark' of mostly working, but its still mostly 'not'





**Andrew A**

---
---
**Dan Cook** *January 28, 2017 13:00*

Are you using endstops? Also in the first [local.cfg](http://local.cfg) you listed, you have 5 microstepping entries for "x"... although that may not have any effect.



I had to get my printer set up by using the terminal. M18 the steppers and move everything to center position... command small movements to make sure you're moving in the right direction... get your coordinate lengths set up correctly for soft end stop min/max... that was the hardest part for me.



You may want to try the Slack chat to get some real time help... unfortunately I'm still getting started myself, and I don't want to mislead you.



Dan








---
**Andrew A** *January 28, 2017 23:50*

im using 3 endstops, i assumed thats all i really needed, altho im questioning this a little bit with the z axis as one side of my bed has slop in the lead screw block, so when its powered off the bed will go lower on that side, so i assume its going to be impossible to get each side the same height.



anyways i didn't see a way to disable the endstops, maybe i have to write an entire config file and not have the commands in it for x2,z2,y2?


---
**Dan Cook** *January 29, 2017 00:15*

Well, (maybe I'm wrong here, so I apologize in advance)... in my case, until my X,Y,Z hit their respective end stop, the printer doesn't know where it is initially. You need to tell the printer in your [local.cfg](http://local.cfg) where each endstop is located... check the [Endstop] section in the default/printer configs and you will definitely need to set up the soft end stops for your printer... you'll also need to tinker with the [Homing] section.






---
**Andrew A** *January 29, 2017 04:16*

[Endstops]



invert_X1 = False

invert_X2 = True

invert_Y1 = False

invert_Y2 = True

invert_Z1 = False

invert_Z2 = True



so that got my x and y working perfect but my z wouldn't move



i removed these lines

in_use_h = True

slave_h = Z



and z worked....so when i enslave the H motor, it stops z from working



i swapped the cables and the H motor does work on the Z plug.



so basically i just need to get the slave mode to work, i tried putting 



invert_h1=False 

invert_h2=True

but it did nothing



not sure how to get slave mode to work :(


---
**Andrew A** *January 29, 2017 04:18*

i also tried to send a g1 h10 command and nothing happens with just 

in_use_h= True


---
**Andrew A** *January 29, 2017 04:28*

hmmm ok so they had it listed 2 different ways in the redeem wiki, what i really needed was slave_z = H grumble grumble 



"# A stepper controller can operate in slave mode, 

# meaning that it will mirror the position of the 

# specified stepper. Typically, H will mirror Y or Z, 

# in the case of the former, write this: slave_h = Y."



"The syntax for selecting which axis is the master and which the slave is:

I want to slave H to Z (H follows everything Z does) then you use "slave_z = H". "


---
*Imported from [Google+](https://plus.google.com/106001037169639681932/posts/XfpZy4zGBtu) &mdash; content and formatting may not be reliable*
