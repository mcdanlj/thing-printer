---
layout: post
title: "I am just getting started with my replicape"
date: February 16, 2018 00:36
category: "Discussion"
author: Eric Testerman
---
I am just getting started with my replicape. I have used ramps and marlin several times before, but new to replicape. I have 12v from atx(PS) wired to the replicape as shown in the diagrams. I get a power led on the replicape and that is it. The beaglebone does not power up. It is my understanding that the cape should be passing 5v to the beaglebone directly. Not sure what to troubleshoot here. My power out form the ATX is good and I have confirmed polarity several times.





**Eric Testerman**

---
---
**Jon Charnas** *February 16, 2018 04:06*

Ok which power LED? The one by the fuse or the one at the other end of the cape? The LED by the fuse indicates a blown fuse...


---
**Eric Testerman** *February 16, 2018 04:50*

It was the LED at the end of the cape that stayed on. I started over from scratch and it is running now. I have a config loaded as well as good feedback and control via octoprint. Thermosisters are reading good, endstops check good, and fan control is up. Tomorrow motors and hot ends. Thanks. I assume the board recognized the error whatever it was and would not power up fully?


---
**Jon Charnas** *February 16, 2018 07:22*

Possibly, though that's the first time I've heard of it. I have seen cases where a couple of pins out of place when mating the cape on to the BBB will lead to this, but usually that's very obvious. The less obvious is if a pin actually folds itself flat between the connector and the cape, and that's happened a few times as well. If you have difficulties with your motors or heaters, double check that.


---
*Imported from [Google+](https://plus.google.com/117049942869161331647/posts/Wk7Tn8oAhc8) &mdash; content and formatting may not be reliable*
