---
layout: post
title: "Hello everyone I have recently picked up finishing the design of the reach B0, as I am found myself needing it"
date: February 23, 2017 09:11
category: "Discussion"
author: Theodor Andersen
---
Hello everyone

I have recently picked up finishing the design of the reach B0, as I am found myself needing it. though seeing as there are more people in the world than me, I thought, I would ask you guys, the community what you actually want on the board.



Disclaimer: this poll is only to gain an idea of what I should put on the board.



(This won't be mass produced, though I'll publish the final design, and you can do what you want with it.)





**Theodor Andersen**

---
---
**Per Hassel Sørensen** *February 23, 2017 09:49*

I would suggest that you add necessary peripherals to support:

A) Minimum 3 pcs of TMC2100, preferably as many as fits - cost can be limited by not installing all of them

B) Minimum of three extra servo channels so we get a total of 5, for future use (f.ex. build plate adjustment, Z-probe movement, filament changer, filament feeder, build plate carousel for series production, enclosure air vent control etc)

C) 4-port powered USB hub - can be added at very low cost and no use of header signals  


---
**Theodor Andersen** *March 30, 2017 11:52*

Some of these things have turned out to be incredibly hard to accomplish, we will have an update up on the project within the next few days


---
*Imported from [Google+](https://plus.google.com/117630610652060224381/posts/iaTDh3Q9yNW) &mdash; content and formatting may not be reliable*
