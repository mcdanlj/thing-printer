---
layout: post
title: "Dusted off my old CoreXY for something to connect the new replicape to"
date: January 19, 2016 03:06
category: "Discussion"
author: James Armstrong
---
Dusted off my old CoreXY for something to connect the new replicape to. Need to track down the rattle. **+Elias Bakken** is there any way to make the printer home to X/Y max? It kept wanting to home to min so I had to flip the stepper direction and endstops to be able to play with it. Only X and y connected until I rewire. 

![images/150f400029903bef0ec45145ff9b78f9.gif](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/150f400029903bef0ec45145ff9b78f9.gif)



**James Armstrong**

---
---
**Elias Bakken** *January 20, 2016 02:19*

Use negative values for homing. For coreXY, you can also specify some exotic rules for the end stops, so you can home x and y at the same time. 


---
*Imported from [Google+](https://plus.google.com/+JamesArmstrong3/posts/fYqyxCfa3ce) &mdash; content and formatting may not be reliable*
