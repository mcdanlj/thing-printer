---
layout: post
title: "I have been trying to get the Manga Screen 2 to work with an installation of Android Things on Raspberry Pi 3, and I am unable to do it"
date: July 04, 2018 00:29
category: "Discussion"
author: John Brumbaugh
---
I have been trying to get the Manga Screen 2 to work with an installation of Android Things on Raspberry Pi 3, and I am unable to do it.  Has any one tried to do it and had any luck?



The screen works for me in that it will show the Android Things interface, but it is not rotated correctly and it repeats the screen three times.  Unfortunately, I don't think I have the ability to edit the configuration like stated for a regular Raspberry Pi 3.



Any thoughts?





**John Brumbaugh**

---
---
**Jean Le Flambeur** *July 09, 2018 07:59*

I think the config.txt file is always present, regardless of what OS you have installed. It's read by the firmware. Did you check if you have a boot partition with a config.txt file on it?


---
**BOT DINGO** *August 18, 2018 05:09*

I have this same issue :(


---
*Imported from [Google+](https://plus.google.com/103207816779941793103/posts/QoWV6RNDbwR) &mdash; content and formatting may not be reliable*
