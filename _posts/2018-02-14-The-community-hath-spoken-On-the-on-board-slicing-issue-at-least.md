---
layout: post
title: "The community hath spoken! On the on-board slicing issue, at least"
date: February 14, 2018 09:54
category: "Discussion"
author: Jon Charnas
---
The community hath spoken! On the on-board slicing issue, at least.



Like mentioned in the last comment there, I'll be removing the slicers from the default image, but build documentation and scripts to make it trivial for users who want one or both to install them.



Now, the second poll that I hinted at last time. I've hit a bit of a road-block with Umikaze 2.1.2. I was hoping to be able to introduce in-place upgrade scripts, that would backup your Redeem, OctoPrint, Toggle and network configurations while upgrading the rest of your image. It's taking longer than I want to get it done.



So. Please tell me what you'd prefer:





**Jon Charnas**

---
---
**Jon Charnas** *February 15, 2018 20:12*

Hmm. Interesting spread so far. The good news for the impatient is that I may have a fix for the in-place ugprade scripts. The bad news is that there's apparently a lot more changes to Redeem than I thought, so I need to go pick an older commit that is known to work well.


---
**Jon Charnas** *February 26, 2018 06:48*

So the image back up scripts now work. I'm waiting for the devs to stabilize redeem before making an image. The tests I built were using the development branch, which is not ready to ship out yet.


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/d3R1WDuZwXi) &mdash; content and formatting may not be reliable*
