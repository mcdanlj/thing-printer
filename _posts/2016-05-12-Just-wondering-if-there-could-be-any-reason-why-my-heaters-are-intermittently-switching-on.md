---
layout: post
title: "Just wondering if there could be any reason why my heaters are intermittently switching on?"
date: May 12, 2016 09:33
category: "Discussion"
author: Daryl Bond
---
Just wondering if there could be any reason why my heaters are intermittently switching on? I had noticed previously that my H heater seemed to be always on, albeit at low power indicated by the brightness of the LED, but this didn't bother me as it wasn't connected. Then the heated bed started turning on randomly for about 0.5s. Any ideas?









**Daryl Bond**

---
---
**Elias Bakken** *May 12, 2016 09:58*

The PWM chip has a single enable pin, so either all outputs are enabled or they are all disabled. There is a pull up resistor on the enable pin that should cause all outputs to be disabled if the pin is floating, but it is under dimensioned. Once redeem is running, the enable pin is set low, meaning enabled. Disabled outputs on the PWM should be pulled low by pull down resistors, but they are also under dimensioned, so there is a dim light some times. If it lights up for 0.5 seconds, most likely it is a software thing causing it. You should try to enable  the debug line for the mosfets and see if it outputs anything else than 0.


---
**Daryl Bond** *May 12, 2016 10:09*

Thanks! I'll look into that as soon as it has finished a print.


---
*Imported from [Google+](https://plus.google.com/104347555452272558593/posts/N5533QoKH3z) &mdash; content and formatting may not be reliable*
