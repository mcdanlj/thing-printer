---
layout: post
title: "I've just run an apt-get update/upgrade in Kamikaze, and now Octoprint isn't starting"
date: May 26, 2016 07:29
category: "Discussion"
author: Marcos Scriven
---
I've just run an apt-get update/upgrade in Kamikaze, and now Octoprint isn't starting. It needs netifaces==0.10, but /usr/lib/python2.7/dist-packages/ contains 0.10.4. 



A few questions about this:



1) The various Python apps on Kamikaze appear to use the common Python env, rather than having their own. Is that correct, and if so, how are you managing dependency conflicts?

2) Because Elias is packaging Octoprint himself, the 'update Octoprint' functionality in Octoprint itself is broken

3) An aside - but would you consider a more traditional forum like support channel? I find Google+ to not be so easily broweable? I know there's a forum on thing-printer, but it seems unused...





**Marcos Scriven**

---
---
**Marcos Scriven** *May 26, 2016 09:16*

I got this working again by 'pip install' the bits octoprint suddenly said it didn't have.  But it'd still be nice to know the answers to my questions.


---
**Elias Bakken** *May 26, 2016 10:28*

There is an effort under way to make a new image that does not rely on Debian packages for the user packages, so that would resolve that issue. The forum is missing a sign-up button, I'll add that:)


---
**Marcos Scriven** *May 26, 2016 11:12*

**+Elias Bakken** Since you've mentioned a new image - another couple of questions if I may: 



1) Will you continue to support BBBs with 2GB eMMCs? Already Kamikaze doesn't leave me a lot of room. 



2) Is the new image you're talking about anything to do with your 'Kamikaze2 - simplified build' GitHub repo?


---
**Elias Bakken** *May 26, 2016 11:26*

Yes, It will be based on the new BBB IoT image. Are you having space issues already? I think there should be about 1.5 GB left.


---
**Marcos Scriven** *May 26, 2016 11:30*

df -h shows: /dev/mmcblk1p2  1.7G  1.5G   90M  95% /



I flashed from BBB-eMMC-flasher-kamikaze-2016-02-16-4gb.img and have not installed anything extra except linux-headers and a dkms wifi driver. And even before that I checked and it wasn't that much more space.


---
**Elias Bakken** *May 26, 2016 14:01*

Here is mine: 

/dev/mmcblk0p2  3.4G  1.6G  1.6G  51% /


---
**Elias Bakken** *May 26, 2016 14:02*

BBB has 4 GB. Are you guys using a 2 GB sdcard? 




---
**Marcos Scriven** *May 26, 2016 14:12*

Only BBB rev c has 4GB. I have an A5C [http://elinux.org/Beagleboard:BeagleBoneBlack#Revision_C_.28Production_Version.29](http://elinux.org/Beagleboard:BeagleBoneBlack#Revision_C_.28Production_Version.29)


---
**Elias Bakken** *May 26, 2016 14:16*

Ah, that explains it.

I'll see what I can do, but the current image is already pretty stripped down and being put together from the ground up. Pretty bare bone. Starting from a standard image will limit that even further.


---
**Marcos Scriven** *May 26, 2016 14:26*

What do you mean by the 'standard image'. The console images here were a lot smaller: [https://rcn-ee.com/rootfs/2016-05-12/flasher/](https://rcn-ee.com/rootfs/2016-05-12/flasher/)  are a lot smaller than the ones with desktop managers installed. Unless you need that for Manga/Toggle?


---
**Elias Bakken** *May 26, 2016 15:23*

In the next version I'm thinking of starting from the IoT image, haven't looked at the size yet, though...


---
*Imported from [Google+](https://plus.google.com/117910047317867058880/posts/ayd2wpuqyR3) &mdash; content and formatting may not be reliable*
