---
layout: post
title: "G29 leveling oddness. When I home the Delta printer all the way to the endstops and then issue G0 Z0 or G1 Z0, the effector will go straight down to the bed and stay in the middle all the way down"
date: April 07, 2016 20:58
category: "Discussion"
author: Pieter Koorts
---
G29 leveling oddness. When I home the Delta printer all the way to the endstops and then issue G0 Z0 or G1 Z0, the effector will go straight down to the bed and stay in the middle all the way down. However when using G29, one of the steppers travel slightly faster placing the effector in the wrong place off-center when probing. 



My step speed, current and per mm is all the same for X, Y and Z.



The G29 macro is quite simple with:

reset (M561), 

set probe point (M557), 

home (G28), 

do probe (G30),



Has anyone encountered this before. After 30 minutes of WTF I am a little lost with this oddity.





**Pieter Koorts**

---
---
**Daryl Bond** *April 08, 2016 07:18*

Do you have an offset (offset_x, offset_y) specified for your probe? If one/any of these are non-zero the effector is offset by that amount so that the probe is located where the extruder nozzle would be for the coordinate you are probing at.


---
**Pieter Koorts** *April 08, 2016 12:19*

Ah yes, that makes sense. Other printer boards first zero and then offset so this just confused me at first. Maybe a wiki update regarding this for someone that has wiki access.



Another thing, can the probe actuation height be set so that it lifts higher while probing each spot? My probing seems to work now but the lift is just enough to actuate the switch while the push-pin seems to still touch the bed so when it moves the pin drags on the bed and sometimes sticks.



Similar to Repetier you can set the probe z-offset but also the actuation pressure. For example, my microswitch clicks with 0.4mm of movement in each direction.


---
**Daryl Bond** *April 08, 2016 12:34*

I am using the develop branch so I'm not 100% sure if the following will apply to you, but you can try it anyway! To probe from higher up you can set the Z location of the probe point to be greater i.e. "M557 P0 X10 Y10 Z50". You may also need to increase the probe "length" config option so that the probing action doesn't 'time-out' before hitting the bed. So in this example you might use length=0.06. Remember that gcodes are in mm and config settings are in m. Hope this helps!


---
**Pieter Koorts** *April 08, 2016 13:15*

Thanks Daryl, I seem to slowly be getting into my head how the Replicape works compared to my old 8 bit things. I should probably volunteer to edit documentation based on my thousand questions being tossed into this group.


---
*Imported from [Google+](https://plus.google.com/100077479073911242630/posts/KwzJpNcatJg) &mdash; content and formatting may not be reliable*
