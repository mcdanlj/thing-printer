---
layout: post
title: "I have a old replicape a4a who I have changed the pin outs on to get them vertical"
date: April 15, 2016 13:45
category: "Discussion"
author: Børre Evensen
---
I have a old replicape a4a who I have changed the pin outs on to get them vertical. Now mine Y axis dont work. So I have obviously overestimated my soldering skills. Is it possible to use the H-axis as Y-axis? Can I switch the GPIOs in any config or something?





**Børre Evensen**

---
---
**Elias Bakken** *April 15, 2016 13:47*

Yes, configure the h as a slave to the y, and just leave the y unconnected.


---
**Børre Evensen** *April 15, 2016 13:53*

Thank you Elias, you are a lifesaver. We can blame Jensa, he learned me to solder :D


---
*Imported from [Google+](https://plus.google.com/103356570987486549903/posts/7iYxM7JaWxR) &mdash; content and formatting may not be reliable*
