---
layout: post
title: "Elias Bakken is there a way to update the 12-23-15 kamakazi image and upgrade the redeem package from the develop branch?"
date: January 17, 2016 02:03
category: "Discussion"
author: James Armstrong
---
**+Elias Bakken** is there a way to update the 12-23-15 kamakazi image and upgrade the redeem package from the develop branch? I could not find the way to switch from main to develop in the apt settings and when I tried to compile from source it out everything in /use/local and I have conflicts with the old one from the image. 



Also, when Redeem starts all the steppers engage. Is there a setting that keeps that from happening? 



Related to the previous, I go into octoprint and click the disable motors and nothing happens (command goes out and sends ok). The only way to disable the motors is to actually move them first the the disable command works (this is why I was trying to update to the dev branch for testing). 



Finally, a big issue that we also discovered with the BBP1S (fastbot) board and also exists with the replicape is that is you enable the heaters then reset the board, the heaters stay engaged until Redeem starts and if that fails for some reason the heaters will be stuck on. They fixed it with a software release but I am not sure how. It would seem like it would have to be done in the cape initialization or in the uboot section to cover possible hangs and the heater staying on. 





**James Armstrong**

---
---
**Elias Bakken** *January 17, 2016 02:26*

The new U-boot disables the steppers. I'll have to check what the issue with enabled heaters is, but that should definitely be a part of the unit procedure. Are you sure you cannot get the latest image to work? There should be a reboot happening on first boot, but never later.


---
**Elias Bakken** *January 17, 2016 02:28*

You cold get the latest redeem package from the feed, just look at the instructions for Kamikaze, there is a new feed. 


---
**James Armstrong** *January 17, 2016 03:03*

I'll check the new image again. There were many issues i ran into, something about pip and a python function that was removed in the newer versions but was still referenced. I'll have to pull up my browsing history and see. I know what ever it was if I reverted to something like 2.3.0 of the module vs something .5.6 it fixed it because the function was in there. 



(Ignore the blah blah of the last sentence, here is what one error was related to)



[https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=744145](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=744145)



Also, is the latest image fixed where octoprint can be updated? The December image says octoprint needs to be manually updated. 






---
**James Armstrong** *January 17, 2016 04:34*

**+Elias Bakken** the other issue with the latest image is HDMI does not work. It makes finding the IP address a pain without looking at DHCP server or connecting the serial port. Any thoughts on why? Is it something in the uEnv.txt?


---
**James Armstrong** *January 17, 2016 04:44*

Redeem runs, Octoprint runs, no HDMI, on going to Octoprint Plugins you get:



The pip command could not be found. Please configure it manually. No installation and uninstallation of plugin packages is possible while pip is unavailable.



which has to do with:



  File "/usr/bin/pip", line 9, in <module>

    load_entry_point('pip==1.5.6', 'console_scripts', 'pip')()

  File "/usr/lib/python2.7/dist-packages/pkg_resources.py", line 356, in load_entry_point

    return get_distribution(dist).load_entry_point(group, name)

  File "/usr/lib/python2.7/dist-packages/pkg_resources.py", line 2476, in load_entry_point

    return ep.load()

  File "/usr/lib/python2.7/dist-packages/pkg_resources.py", line 2190, in load

    ['__name__'])

  File "/usr/lib/python2.7/dist-packages/pip/__init__.py", line 74, in <module>

    from pip.vcs import git, mercurial, subversion, bazaar  # noqa

  File "/usr/lib/python2.7/dist-packages/pip/vcs/mercurial.py", line 9, in <module>

    from pip.download import path_to_url

  File "/usr/lib/python2.7/dist-packages/pip/download.py", line 25, in <module>

    from requests.compat import IncompleteRead

ImportError: cannot import name IncompleteRead


---
**James Armstrong** *January 17, 2016 05:06*

Octoprint plugins can't be installed with new image (permissions error and other issues).


---
**Elias Bakken** *January 17, 2016 13:35*

Ok, ill have a look at pip and the permissions today. You mean HDMI with screens other than Manga Screen I assume? 


---
**James Armstrong** *January 17, 2016 14:06*

**+Elias Bakken** correct. I don't have the manga connected yet. Is it a simple config Line in the uEnv? Now that I have two BBB boards configured, one with each version, I may compare the two config files. 


---
**Elias Bakken** *January 17, 2016 15:43*

As for pip, this should do the job as a work around: 

apt-get remove python-pip

easy_install pip




---
**Elias Bakken** *January 17, 2016 15:44*

Ok, I might have to go back to the stock u-boot, only having the stepper disable function...


---
**James Armstrong** *January 17, 2016 18:25*

**+Elias Bakken** is it normal for the steppers to really whine badly (high frequency). Using default config and have the current set correctly. Played with the decays through octoprint and it didn't really make much of a change. I have changed the default config to play with the decay modes. 


---
**Elias Bakken** *January 17, 2016 19:08*

One of two things is probably happening. Either the steppers are in "power save mode". This will cause a loud noise, and should happen only for 3-4 seconds during boot. If you are hearing it constantly with latest Redeem, then you are probably using very small steppers. They tend to be noisy, even worse than other drivers.


---
**James Armstrong** *January 17, 2016 19:37*

standard Kyson nema17's. I will record it later.


---
**Elias Bakken** *January 17, 2016 21:02*

try to set microstepping to 7 or 8 and see if that fixes the problem! That is the "quiet mode"


---
*Imported from [Google+](https://plus.google.com/+JamesArmstrong3/posts/AZUjQXa33NU) &mdash; content and formatting may not be reliable*
