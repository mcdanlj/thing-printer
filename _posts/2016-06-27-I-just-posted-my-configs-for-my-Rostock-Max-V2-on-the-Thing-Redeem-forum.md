---
layout: post
title: "I just posted my configs for my Rostock Max V2 on the Thing Redeem forum"
date: June 27, 2016 18:08
category: "Discussion"
author: Curt Page
---
I just posted my configs for my Rostock Max V2 on the Thing Redeem forum. They have been working for me.



![images/f317a67d5568a18637f47ab1d718a5bd.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/f317a67d5568a18637f47ab1d718a5bd.jpeg)
![images/f1067866f504ee628da14d8ef9cb744d.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/f1067866f504ee628da14d8ef9cb744d.jpeg)
![images/180d60e4137c7e3a0be1d7e7abc1154e.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/180d60e4137c7e3a0be1d7e7abc1154e.jpeg)

**Curt Page**

---
---
**Elias Bakken** *June 27, 2016 19:11*

Great! Should I include it as a config in Redeem? 


---
**Curt Page** *June 27, 2016 19:11*

That would be great 


---
**Elias Bakken** *June 27, 2016 19:18*

Done! Only in develop for now though. 


---
**Elias Bakken** *June 27, 2016 19:21*

But it looks like the platform is missing from Toggle! What has happened? 


---
**Curt Page** *June 27, 2016 19:36*

Toggle hasn't displayed the models since the last deb. package update. I've been running the main branch to get up and running. 


---
**Curt Page** *June 27, 2016 19:37*

All the other features of toggle work 


---
**Andy Tomascak** *June 30, 2016 03:00*

Oh, another Rostock Max user! :D I'll have to see if our configs are similar...


---
**Andy Tomascak** *June 30, 2016 03:33*

Okay, not as similar as I had hoped. But it looks like yours is printing okay! :)


---
*Imported from [Google+](https://plus.google.com/100516727423004370656/posts/NKhADcs9QRT) &mdash; content and formatting may not be reliable*
