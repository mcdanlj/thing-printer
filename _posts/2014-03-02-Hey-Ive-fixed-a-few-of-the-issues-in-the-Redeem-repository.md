---
layout: post
title: "Hey! I've fixed a few of the issues in the Redeem repository"
date: March 02, 2014 19:51
category: "Discussion"
author: Elias Bakken
---
Hey! I've fixed a few of the issues in the Redeem repository. Feel free to add moar: [https://bitbucket.org/intelligentagent/redeem/issues?status=new&status=open](https://bitbucket.org/intelligentagent/redeem/issues?status=new&status=open)





**Elias Bakken**

---


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/F95aGds1qx2) &mdash; content and formatting may not be reliable*
