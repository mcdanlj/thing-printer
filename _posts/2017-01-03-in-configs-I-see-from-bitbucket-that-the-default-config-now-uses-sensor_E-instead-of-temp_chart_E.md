---
layout: post
title: "in configs, I see from bitbucket that the default config now uses \"sensor_E\" instead of \"temp_chart_E\""
date: January 03, 2017 00:03
category: "Discussion"
author: Step Cia
---
in configs, I see from bitbucket that the default config now uses "sensor_E" instead of "temp_chart_E". I tried changing my config to use sensor_E but all I get is the temp reading is off. and when I use temp_chart_E, it gives me temperature reading. 



which one is correct, sensor_E or temp_chart_E? 





**Step Cia**

---
---
**Elias Bakken** *January 03, 2017 00:22*

In the latest Redeem, it should be sensor_E. Do you get any warning or errors during redeem start-up? 


---
**Step Cia** *January 03, 2017 00:27*

I didn't see any warning. Everything was fresh install from kamikaze 2.0.8 flash. The only thing I carried over from version 1 was my printer config which still has temp_chart_E...


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/WA2hmUGF7tM) &mdash; content and formatting may not be reliable*
