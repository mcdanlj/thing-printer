---
layout: post
title: "Umikaze 2.1.2 RC 4 - finally! Ok, ok, normally it's \"3rd time's the charm\"..."
date: March 17, 2018 23:11
category: "Discussion"
author: Jon Charnas
---
Umikaze 2.1.2 RC 4 - finally!



Ok, ok, normally it's "3rd time's the charm"... but in this case, I do hope version 4 will fix all problems that have been run into up until now.



Because of new versions of the Linux kernel and other packages on ubuntu were really messing things up, I basically stripped Umikaze 2.1.1 of the older Redeem version and re-installed the latest one. I also added the <b>in place upgrade</b> capability, which means if you are running Kamikaze 2.1.0 or Umikaze 2.1.1, it will backup your network configuration, OctoPrint configurations folder, Redeem and Toggle local.cfg files.



I decided to go along this path because this puts Redeem's "print to SD" feature out in the wild for more folks to use. Documentation on how to properly use it will follow within the week, either as a Youtube screen capture or as a screenshot-based page on the Thing-Printer wiki.



DISCLAIMER: Dragons ahead!



This a Release Candidate, meaning not everything is final and bugs are suspected. Please file any bugs you find on the github repository! There may also still be the occasional feature enhancement added (i.e. files to be added during the backup, removal of unneeded files, etc.) before the final release.





**Jon Charnas**

---
---
**Lars Dunemark** *March 31, 2018 14:41*

It it safe to run apt upgrade or will it break things?


---
**Jon Charnas** *March 31, 2018 14:58*

It can break the kernel installation, be careful


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/LKE1f7UMRmB) &mdash; content and formatting may not be reliable*
