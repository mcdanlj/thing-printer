---
layout: post
title: "Hi guys, had trouble with flashing the EEPROM on the RevB3 board, and do not get Octoprint to connect"
date: January 18, 2017 00:52
category: "Discussion"
author: Jan Vik
---
Hi guys, had trouble with flashing the EEPROM on the RevB3 board, and do not get Octoprint to connect.  Get the eeprom flash and saving it seems fine, but " cat Replicape_00B3.eeprom > /sys/bus/i2c/drivers/at24/2-0054/at24-1/nvmem"    comes back "bash: /sys/bus/i2c/drivers/at24/2-0054/at24-1/nvmem: No such file or directory"



Is the file directory address in the wiki wrong ? 



Here `s the log from my ssh cmd :

root@kamikaze:~# sudo <s>s</s>

<s>root@kamikaze:~# </s>[https://bitbucket.org/intelligentagent/replicape/raw/bf08295bbb5e98ce6bff60097fe9b78d96002654/eeprom/Rep](https://bitbucket.org/intelligentagent/replicape/raw/bf08295bbb5e98ce6bff60097fe9b78d96002654/eeprom/Rep)<s>                                                                                                                    licape_00B3.eeprom</s>

<s>bash: </s>[https://bitbucket.org/intelligentagent/replicape/raw/bf08295bbb5e98ce6bff60097fe9b78d96002654/eeprom/Replicape_00B3](https://bitbucket.org/intelligentagent/replicape/raw/bf08295bbb5e98ce6bff60097fe9b78d96002654/eeprom/Replicape_00B3)<s>                                                                                                                    .eeprom: No such file or directory</s>

<s>root@kamikaze:~#  wget </s>[https://bitbucket.org/intelligentagent/replicape/raw/bf08295bbb5e98ce6bff60097fe9b78d96002654/eepr](https://bitbucket.org/intelligentagent/replicape/raw/bf08295bbb5e98ce6bff60097fe9b78d96002654/eepr)<s>                                                                                                                    om/Replicape_00B3.eeprom</s>

<s>--2017-01-18 00:38:29-</s>  [https://bitbucket.org/intelligentagent/replicape/raw/bf08295bbb5e98ce6bff60097fe9b78d96002654/ee](https://bitbucket.org/intelligentagent/replicape/raw/bf08295bbb5e98ce6bff60097fe9b78d96002654/ee)                                                                                                                    prom/Replicape_00B3.eeprom

Resolving [bitbucket.org](http://bitbucket.org) ([bitbucket.org](http://bitbucket.org))... 104.192.143.1, 104.192.143.3, 104.192.143.2, ...

Connecting to [bitbucket.org](http://bitbucket.org) ([bitbucket.org](http://bitbucket.org))|104.192.143.1|:443... connected.

HTTP request sent, awaiting response... 200 OK

Length: 244 [application/octet-stream]

Saving to: 'Replicape_00B3.eeprom.2'



Replicape_00B3.eeprom.2        100%[=================================================>]     244  --.-KB/s    in 0s



2017-01-18 00:38:30 (10.0 MB/s) - 'Replicape_00B3.eeprom.2' saved [244/244]



root@kamikaze:~# cat Replicape_00B3.eeprom.2 > /sys/bus/i2c/drivers/at24/2-0054/at24-1/nvmem

bash: /sys/bus/i2c/drivers/at24/2-0054/at24-1/nvmem: No such file or directory

root@kamikaze:~# cat Replicape_00B3.eeprom > /sys/bus/i2c/drivers/at24/2-0054/at24-1/nvmem

bash: /sys/bus/i2c/drivers/at24/2-0054/at24-1/nvmem: No such file or directory

root@kamikaze:~# root@kamikaze:~# cat Replicape_00B3.eeprom > /sys/bus/i2c/drivers/at24/2-0054/at24-1/nvmem

bash: /sys/bus/i2c/drivers/at24/2-0054/at24-1/nvmem: No such file or directory

root@kamikaze:~# bash: /sys/bus/i2c/drivers/at24/2-0054/at24-1/nvmem: No such file or directory

bash: bash:: command not found

root@kamikaze:~# root@kamikaze:~# cat Replicape_00B3.eeprom > /sys/bus/i2c/drivers/at24/2-0054/at24-1/nvmem

bash: /sys/bus/i2c/drivers/at24/2-0054/at24-1/nvmem: No such file or directory

root@kamikaze:~# bash: /sys/bus/i2c/drivers/at24/2-0054/at24-1/nvmem: No such file or directory

bash: bash:: command not found

root@kamikaze:~#









**Jan Vik**

---
---
**Dan Cook** *January 18, 2017 20:30*

Are you using an older BBB that only has 2gb of onboard RAM? If so, you'll need to boot from the SD card per instructions on the wiki.


---
**Jan Vik** *January 19, 2017 20:41*

Sorry Dan, do not think I follow you here. If  you think about the boot of the Kamikaze image, that`s sure done from the SD card. The wiki mention some Version B3`s being in need of flashing the eeprom, nothing mentioned about the BBB there. 



I`got all up and running, done the update on the Kamikaze thru the Octoprint Software Update interface. Can restart Toggle and Redeem from Octoprint, but can not get connection to printer. Every attemp ends in "The was an unhandled error while talking to the printer. Due to that OctoPrint disconnected. Error: Failed to autodetect serial port, please set it manually."     



Running "systemctl status" for  Redeem 

comes back : "root@kamikaze:~# systemctl status -n 100 redeem

● redeem.service - The Replicape Daemon

   Loaded: loaded (/lib/systemd/system/redeem.service; enabled; vendor preset: e

   Active: inactive (dead)

lines 1-3/3 (END)

● redeem.service - The Replicape Daemon

   Loaded: loaded (/lib/systemd/system/redeem.service; enabled; vendor preset: enabled)

   Active: inactive (dead) "



Started from scratch and flashed the BBB multiple times, gotten updates, upgrades, restarted Redeem and Toggle each time, but no luck....


---
**Dan Cook** *January 19, 2017 20:44*

Oh, gotcha... that's odd, and above my level of experience. Hopefully someone can chime in on it either here or the slack chat.


---
**Andrew A** *January 22, 2017 07:32*

i am getting this same problem posted it in my other thread because i didn't want to create a bunch of things and clutter, but i don't think it has anything to do with you updating the eeprom.  do a fresh kamikaze flash, and on your first boot, log into octoprint and see if u can connect, my guess is u can, the second you reboot it all goes to shit.  octoprint's print config files for redeem and cura magically appear and dispear depending on its mood, and whatever is causing that is most likely tied into the connection working and 'magically' not working.  i had a somewhat related issue with the wifi not working on the stable version, the drivers were supposed to be there, the wifi stick i bought was supposed to be 100% compatible and for some 'magic' reason it wasn't working, only works on the RC version.  and in the cureengine the folder has a test button, and the test always fails.......its almost like there's a bunch of files missing......it sure would be nice if there was some kind of program to compare whats on our BBB to what is 'supposed" to be on it, or better yet if it all just worked like it was supposed to



this is all just really sad






---
**Andrew A** *January 22, 2017 10:30*

So i 'might' be onto something, my thought was files were missing, but maybe its all just 'permissions' issues.



so with that in mind, first thing was i tried to find my serial port name, which i didn't really have much luck with, but to try yourself type 



lsusb

dmesg



then i took whatever names looked right and added them to octoprint manually in settings.



after that i nano /etc/group  and added ubuntu,octo to the uusp and reboot.  all of these i ended up with names under auto to try to connect to, some of them tried to connect but failed, others said permission denied.....then i did a reboot and then suddenly they all said permission denied, and another time after that they wouldn't even try to connect.......see its like someone is screwing with me i swear, how does the software do something different every time???


---
**Andrew A** *January 23, 2017 07:25*

just to clarify your problem, after a fresh flash without touching ANY settings, you can or cann't connect in octoprint?  Also i might be wrong but i don't think u need to do any of the updates in the wiki under kamikaze, i mean try a fresh flash without updates, u can always update later...i wasted sooooooo much time doing those updates and i really didn't need to do any of it for my problem.



I finally figuered out what was causing my issue, my local.cfg wasn't setup right. whew that was easy.....


---
*Imported from [Google+](https://plus.google.com/116826138544868285121/posts/L8TGGYcx6YF) &mdash; content and formatting may not be reliable*
