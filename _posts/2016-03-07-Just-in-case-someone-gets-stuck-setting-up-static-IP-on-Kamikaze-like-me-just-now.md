---
layout: post
title: "Just in case someone gets stuck setting up static IP on Kamikaze (like me just now)"
date: March 07, 2016 22:27
category: "Discussion"
author: Ante Vukorepa
---
Just in case someone gets stuck setting up static IP on Kamikaze (like me just now).



Angstrom Linux seems to use connman to manage networking. Editing /etc/network/interfaces is a no go. Manually editing connman's settings is a no-no.



It should be done through connmanctl. You can run it in interactive mode (neat because it has autocomplete).



Then you can do this:



config <service> --ipv4 manual <ip address> <netmask> <gateway>



You can list services with "services".





**Ante Vukorepa**

---
---
**Ante Vukorepa** *March 07, 2016 22:31*

Erh. Meant Debian, not Angstrom. But  i believe Kamikaze inherited Connman from Angstrom distro.


---
**Jon Charnas** *March 22, 2016 08:50*

And Angstrom is a debian-derivative as well. It all converges eventually ;) The point about connman vs /etc/network/interfaces is important though.


---
*Imported from [Google+](https://plus.google.com/105486978634901788220/posts/5aapKXq5E58) &mdash; content and formatting may not be reliable*
