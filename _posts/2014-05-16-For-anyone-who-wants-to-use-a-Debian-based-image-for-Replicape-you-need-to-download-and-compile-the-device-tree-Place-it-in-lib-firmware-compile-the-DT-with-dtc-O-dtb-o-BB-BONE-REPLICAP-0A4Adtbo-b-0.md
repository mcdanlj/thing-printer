---
layout: post
title: "For anyone who wants to use a Debian based image for Replicape, you need to download and compile the device tree: Place it in /lib/firmware/ compile the DT with: dtc -O dtb -o BB-BONE-REPLICAP-0A4A.dtbo -b 0 -@"
date: May 16, 2014 16:47
category: "Discussion"
author: Elias Bakken
---
For anyone who wants to use a Debian based image for Replicape, you need to download and compile the device tree:



[https://bitbucket.org/intelligentagent/replicape/src/efa48a03b533f45cc241543f255e86edafde8122/Device_tree/3.8/?at=Rev-A4](https://bitbucket.org/intelligentagent/replicape/src/efa48a03b533f45cc241543f255e86edafde8122/Device_tree/3.8/?at=Rev-A4)



Place it in /lib/firmware/



compile the DT with:

dtc -O dtb -o BB-BONE-REPLICAP-0A4A.dtbo -b 0 -@ BB-BONE-REPLICAP-0A4A.dts﻿





**Elias Bakken**

---
---
**Elias Bakken** *May 16, 2014 17:57*

If you want to use Redeem, you also need tty0tty: [https://github.com/freemed/tty0tty](https://github.com/freemed/tty0tty)



Thanks to **+James Armstrong** for finding this out!


---
**Daryl Bond** *September 27, 2014 03:53*

A further note for the device tree on debian. If you are still having issues after doing the above, as I did, have a look at:



[http://elinux.org/Beagleboard:BeagleBoneBlack_Debian#Loading_custom_capes](http://elinux.org/Beagleboard:BeagleBoneBlack_Debian#Loading_custom_capes)



In brief add "CAPE=BB-BONE-REPLICAP:0A4A" (without the "") to /etc/default/capemgr. Note the : between the cape name and the version. Otherwise it will try to load version 00A0.


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/LzM3P72XKJP) &mdash; content and formatting may not be reliable*
