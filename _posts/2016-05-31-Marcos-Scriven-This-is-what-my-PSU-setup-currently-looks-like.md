---
layout: post
title: "Marcos Scriven This is what my PSU setup currently looks like"
date: May 31, 2016 21:57
category: "Discussion"
author: Andy Tomascak
---
**+Marcos Scriven**

This is what my PSU setup currently looks like. (Sorry for the poor pics!) The IEC switch allows mains power to the PSU, and the (unseen) "main" switch on the front allows PSU power to the BBB/Replicape. Again, the two stage power is there mostly for safety and my own peace-of-mind. :)



![images/5450776d153a4cfaff218d36c46df744.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/5450776d153a4cfaff218d36c46df744.jpeg)
![images/107849df006074dab592291df10bfdf7.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/107849df006074dab592291df10bfdf7.jpeg)

**Andy Tomascak**

---
---
**Marcos Scriven** *June 01, 2016 12:11*

Ah, thanks for sharing that. I don't have space under my printer, so was thinking something like this: [http://www.thingiverse.com/thing:1402107](http://www.thingiverse.com/thing:1402107)



Also, I just tried the SMPS - works fine, but the fan is noisy as hell, which is a shame considering the electronics can driver the steppers so much more quietly. 



Do you trim the voltage to 12.00V without any load? I find if I do that, when the heat bed comes on it dips to 11.71, then once warm operates around 11.9


---
**Marcos Scriven** *June 01, 2016 12:18*

Although I kept the fan from the old PSU, so maybe could try this: [https://images-na.ssl-images-amazon.com/images/I/41CqVYav-lL._SL256_.jpg](https://images-na.ssl-images-amazon.com/images/I/41CqVYav-lL._SL256_.jpg)


---
**Andy Tomascak** *June 01, 2016 15:57*

Either my fan isn't as noisy, or I don't notice it in the enclosure. I didn't mess with the voltage trim other than to make sure it was getting a full 12v under load. The voltage regulator on the Replicape is capable of dealing with any overage.


---
*Imported from [Google+](https://plus.google.com/110080975040288455209/posts/DFgAbv2md6M) &mdash; content and formatting may not be reliable*
