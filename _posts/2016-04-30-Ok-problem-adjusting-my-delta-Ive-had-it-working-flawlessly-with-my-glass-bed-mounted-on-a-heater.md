---
layout: post
title: "Ok, problem adjusting my delta. I've had it working flawlessly with my glass bed mounted on a heater"
date: April 30, 2016 13:26
category: "Discussion"
author: Jon Charnas
---
Ok, problem adjusting my delta. I've had it working flawlessly with my glass bed mounted on a heater.



But I've been selected as a super-user for the Fleks3D build plate. Now I need to recalibrate my Z height to account for the 3.5mm thickness of the plate I'm clipping on top.



I thought it was as simple as just shrinking the home_z value in the [Homing] parameters... I'm running on the latest develop git checkout. Am I missing something?





**Jon Charnas**

---
---
**Elias Bakken** *April 30, 2016 13:38*

That should do it. You can also use m206 for minor adjustments. Redeem 1.2.2 should be in the feed now, same as master branch. 


---
**Jon Charnas** *April 30, 2016 14:03*

Ok, I'll try the master branch instead of develop.


---
**Jon Charnas** *April 30, 2016 15:46*

Still not working as I'd expect. What happens is that it goes up to the endstops, then comes down to the Z height specified in home_z... Instead of bringing the build plate up when modifying it, I'm pushing the assembly down. Any idea why this might be?


---
**Jon Charnas** *April 30, 2016 17:35*

**+Elias Bakken**: here's my local.cfg, if you see anything wrong in it:

[http://pastebin.ca/3587034](http://pastebin.ca/3587034)


---
**Jon Charnas** *April 30, 2016 18:04*

Ok I found it. The Z height of endstops is defined in [Geometry] with offset_x values... Which in my case were in kossel_mini.cfg instead of local.cfg (scary to think that my Z-height wasn't defined locally)


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/Px6iiCjd4Tp) &mdash; content and formatting may not be reliable*
