---
layout: post
title: "Elias Bakken I see the Redeem code supports a filament sensor"
date: January 19, 2016 20:52
category: "Discussion"
author: James Armstrong
---
**+Elias Bakken** I see the Redeem code supports a filament sensor. I am currently using the Tunnell sensor that does all the work but I like the idea of the code being in control since it knows when it should be extruding. The replicape doesn't have any extra i/o and I see it is using the event subsystem so is it setup to use mouse or something? 





**James Armstrong**

---
---
**Elias Bakken** *January 19, 2016 22:03*

It's very experimental, but it does work . I'm using two end stops as input for a rotary encoder. I'm not sure how to integrate it into octoprint, though. Currently there is an Mcode for seeing the "drift", but I want a continous feedback like with temperature.  Would be great to calibrate tension on the extruder I think! 


---
*Imported from [Google+](https://plus.google.com/+JamesArmstrong3/posts/D1Y9vSkcGAu) &mdash; content and formatting may not be reliable*
