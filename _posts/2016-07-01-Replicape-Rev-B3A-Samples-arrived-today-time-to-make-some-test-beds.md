---
layout: post
title: "Replicape Rev B3A! Samples arrived today, time to make some test beds:)"
date: July 01, 2016 14:07
category: "Discussion"
author: Elias Bakken
---
Replicape Rev B3A! Samples arrived today, time to make some test beds:) 

![images/c9c66df7ac3865319d8bed698d2d1ee4.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/c9c66df7ac3865319d8bed698d2d1ee4.jpeg)



**Elias Bakken**

---
---
**Jon Charnas** *July 01, 2016 14:17*

Cool! What's new? 


---
**Daniel Kusz** *July 01, 2016 14:18*

Exactly, what's new?﻿


---
**Elias Bakken** *July 01, 2016 14:20*

No start-up noise, cooler caps, no crooked fan connectors, higher value pull-downs, a boot-hole and a header for the remaining pwm channels. Mostly nice to have features, but worth fixing for the next batch!:)


---
**James Armstrong** *July 01, 2016 16:50*

Nice. Looks good.


---
**James Armstrong** *July 01, 2016 16:52*

The new stepper driver chip they used on the new Duet Wifi looks nice, have you tested those out any? TMC2660. A bit larger so they would not fit on your board.


---
**Ryan Carlyle** *July 01, 2016 17:25*

Yeah, I'm very happy with the 2660 on the Duet Wifi.  They're doing SPI control for setup and regular dir/step pulses. I don't know offhand what the differences are with the other TMCs though. 


---
**Pieter Koorts** *July 01, 2016 17:32*

Or if you want a semi drop in replacement the tmc2130 which like the tmc2660 also has stall guard and other features but the same current capacity of the tmc2100


---
**Philip Acierto** *July 01, 2016 18:08*

Does this mean all of the current Rev B will be getting a fix for the steppers making noise at start up soon?


---
**Elias Bakken** *July 01, 2016 18:12*

Philip, there is not much to be done for B3 to help the start-up noise. Best case is that the second whine disappears, but Imo it's not a great concern. I'd rather get a new kamikaze out for one, so we don't have to get ssh access to update the software! My $0.02:)


---
**Philip Acierto** *July 01, 2016 18:30*

I'd prefer easier updating as well, but figure I ask.


---
**Andrew A** *July 14, 2016 06:45*

maybe if its just simple parts upgrade, you could sell a little upgrade kit for the DIY solder people for the main issues solved?  Like those caps look like the could be replaced in about 5 minutes with newer ones for example.  people always like having the newest and best, i just bought a replicape a few weeks back so a little ouch lol.



anyways, im still building my 3d printer so im not even sure how important these upgrades are so maybe its a non concern for most people


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/gZ1XWRPAU5P) &mdash; content and formatting may not be reliable*
