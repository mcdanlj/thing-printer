---
layout: post
title: "I heard going with a higher voltage than 12v can solve the whine problem with the TMC2100 drivers so I made a mod to my HPB and fitted the new 24v E3D v6"
date: May 28, 2016 09:37
category: "Discussion"
author: tizzledawg
---
I heard going with a higher voltage than 12v can solve the whine problem with the TMC2100 drivers so I made a mod to my HPB and fitted the new 24v E3D v6. Using a 24v 300W (400W peak) Audio power supply.



<b>Sad face</b>  Still noisy. I can change the pitch using the decay but I guess my hearing range is still pretty good.



 I don't want to go stealthchop because of the decrease in accuracy so my next move is pulling apart the steppers and applying a resin or glue to the windings so see if that helps to stop them singing - do only one first though.



Any other thoughts?





**tizzledawg**

---
---
**Elias Bakken** *May 28, 2016 10:29*

Change steppers!


---
**Ryan Carlyle** *May 28, 2016 17:03*

I don't have a simulator built for TMC2100 choppers yet, unfortunately... do you guys know if it's high-inductance motors or low-inductance motors that whine? Or does it seem pretty random?


---
**Elias Bakken** *May 28, 2016 18:14*

It appears that larger steppers are more quiet than smaller, but it is most likely related to the inductance.


---
**tizzledawg** *May 31, 2016 09:10*

So I went ahead to see what adding Lacquer to the windings would do (MG4226 Corona dope because I had some at work) and it did help a little but that tone just cuts through. I played a little around and found setting the decay to 0 helped ALOT. What does the scale of decay mean? It confused me a little in the documentation and how it translates to the TMC2100 datasheet wasn't quite clear.


---
*Imported from [Google+](https://plus.google.com/108496831286068938419/posts/gA1s7Wskso1) &mdash; content and formatting may not be reliable*
