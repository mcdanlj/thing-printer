---
layout: post
title: "Is going to be back up at any time in the near future?"
date: May 11, 2017 20:42
category: "Discussion"
author: Jed Kivi
---
Is [thing-printer.com](http://thing-printer.com) going to be back up at any time in the near future?



Is there a mirror or something like that?





**Jed Kivi**

---
---
**Jon Charnas** *May 11, 2017 20:54*

So it's up to the host to bring it back up. They freaked out and shut off the website over a log file that they thought meant the site was hacked. They had a decently long checklist of things to correct before bringing it back up, which as far as I can tell Elias has completed. Now they're being slow about restoring the website... Unless **+Elias Bakken**​ chimes in with more details that's as far as anyone knows.


---
**Elias Bakken** *May 11, 2017 20:59*

There is one wordpress plugin they still claim needs to be fixed, so I'll get that done tomorrow! 


---
**Jed Kivi** *May 11, 2017 22:56*

Thanks, I just bought a replicape and I'm looking forward to trying things out


---
*Imported from [Google+](https://plus.google.com/114249510761763649417/posts/MkNUr1MiDbN) &mdash; content and formatting may not be reliable*
