---
layout: post
title: "What ever happened with the online configuration editor the old Redeem had (via the web)?"
date: January 25, 2016 19:58
category: "Discussion"
author: James Armstrong
---
What ever happened with the online configuration editor the old Redeem had (via the web)? Now it seems anytime the printer config file needs editing you have to ssh into the board. 





**James Armstrong**

---
---
**Elias Bakken** *January 25, 2016 21:29*

You can upload the config file from your computer. Go to the "redeem" plugin page in Octoprint. I want to get rid of the lighttpd all together eventually, and do everything through Octoprint. I've been thinking of an extra tab for online config editing, but would like a better interface than a simple file editor... 


---
*Imported from [Google+](https://plus.google.com/+JamesArmstrong3/posts/W9fonpi3xy3) &mdash; content and formatting may not be reliable*
