---
layout: post
title: "I haven't wired my printer up to the replicape yet"
date: January 20, 2016 16:56
category: "Discussion"
author: Matt LaRussa
---
I haven't wired my printer up to the replicape yet.  I wanted to get the OS ready before adding another variable.  On boot, the octoprint service fails to start:

Jan 20 10:47:50 kamikaze octoprint[455]: Traceback (most recent call last):

Jan 20 10:47:50 kamikaze octoprint[455]: File "/usr/bin/octoprint", line 5, in <module>

Jan 20 10:47:50 kamikaze octoprint[455]: from pkg_resources import load_entry_point

Jan 20 10:47:50 kamikaze octoprint[455]: File "build/bdist.linux-armv7l/egg/pkg_resources/__init__.py", line 3130, in <module>

Jan 20 10:47:50 kamikaze octoprint[455]: File "build/bdist.linux-armv7l/egg/pkg_resources/__init__.py", line 3116, in _call_aside

Jan 20 10:47:50 kamikaze octoprint[455]: File "build/bdist.linux-armv7l/egg/pkg_resources/__init__.py", line 3143, in _initialize_master_working_set

Jan 20 10:47:50 kamikaze octoprint[455]: File "build/bdist.linux-armv7l/egg/pkg_resources/__init__.py", line 644, in _build_master

Jan 20 10:47:50 kamikaze octoprint[455]: File "build/bdist.linux-armv7l/egg/pkg_resources/__init__.py", line 657, in _build_from_requirements

Jan 20 10:47:50 kamikaze octoprint[455]: File "build/bdist.linux-armv7l/egg/pkg_resources/__init__.py", line 830, in resolve

Jan 20 10:47:50 kamikaze octoprint[455]: pkg_resources.DistributionNotFound: The 'netifaces==0.10' distribution was not found and is required by OctoPrint

Jan 20 10:47:50 kamikaze systemd[1]: octoprint.service: main process exited, code=exited, status=1/FAILURE

Jan 20 10:47:50 kamikaze systemd[1]: Unit octoprint.service entered failed state.



I verified netifaces is installed:

root@kamikaze:/var/log# pip show netifaces

---

Metadata-Version: 1.1

Name: netifaces

Version: 0.10.4

Summary: Portable network interface information.

Home-page: [https://bitbucket.org/al45tair/netifaces](https://bitbucket.org/al45tair/netifaces)

Author: Alastair Houghton

Author-email: alastair@alastairs-place.net

License: MIT License

Location: /usr/lib/python2.7/dist-packages





**Matt LaRussa**

---
---
**Elias Bakken** *January 20, 2016 16:59*

Is this the latest Kamikaze? What date is the image? 


---
**Matt LaRussa** *January 20, 2016 17:22*

It's the 1/5/16 build.


---
**James Armstrong** *January 20, 2016 18:12*

That is the version I am using with no to little issues. Some permission issues and a pip module error that only seemed to affect plugins. Did you do a apt-get update / upgrade? I think I tried that once and it broke stuff. 


---
**Matt LaRussa** *January 20, 2016 19:28*

I did several apt upgrades.  I didn't see it mentioned that we couldn't patch the OS.


---
**James Armstrong** *January 20, 2016 19:39*

Not that we can't but the image is so new something may be changing with the updates. Try again without doing an OS upgrade. I have two running from the same image. The only thing I have stayed away from us the is upgrade because I saw issues the first time I tried. 


---
**Elias Bakken** *January 20, 2016 19:51*

Ah, OK, that explains it! Hm... Is there a way to "lock" python packages? Octoprint is pretty particular about the  libraries, so an upgrade will most likely break things yes. But we do need to find a way to let people upgrade. 


---
**Matt LaRussa** *January 20, 2016 20:10*

I might change directions and go with a jessie install then install the Thing-Printer binaries.  As long as I can get the replicape to init, getting the third party tools running shouldn't be a problem.


---
**Matt LaRussa** *January 20, 2016 20:47*

Not knocking Kamikaze, BTW.


---
**James Armstrong** *January 20, 2016 21:12*

I thought kamakazi was  the latest Debian Jessie kernel. Maybe I read that wrong. 


---
**Elias Bakken** *January 20, 2016 21:13*

Sure! Feel free to do that! The big difference is related to getting Toggle to run, so if you do not need that, it might be a good idea. You also need Octoprint and Cura Engine, though.


---
**Matt LaRussa** *January 21, 2016 03:32*

Does this mean the cape is loaded?

debian@beaglebone:~$ dmesg|grep bone_capemgr

[    3.913660] bone_capemgr bone_capemgr: Baseboard: 'A335BNLT,00C0,5214BBBK1111'

[    3.913690] bone_capemgr bone_capemgr: compatible-baseboard=ti,beaglebone-black - #slots=4

[    3.955848] bone_capemgr bone_capemgr: slot #0: 'Replicape 3D printer cape,00B3,Intelligent Agen,BB-BONE-REPLICAP'

[    4.011206] bone_capemgr bone_capemgr: slot #1: No cape found

[    4.071286] bone_capemgr bone_capemgr: slot #2: No cape found

[    4.131175] bone_capemgr bone_capemgr: slot #3: No cape found

[    4.137266] bone_capemgr bone_capemgr: initialized OK.

[    5.151340] bone_capemgr bone_capemgr: loader: failed to load slot-0 BB-BONE-REPLICAP:00B3 (prio 0)

[   18.947436] bone_capemgr bone_capemgr: part_number 'BB-BONE-REPLICAP', version '0A4A'

[   18.947476] bone_capemgr bone_capemgr: slot #4: override

[   18.947495] bone_capemgr bone_capemgr: Using override eeprom data at slot 4

[   18.947512] bone_capemgr bone_capemgr: slot #4: 'Override Board Name,0A4A,Override Manuf,BB-BONE-REPLICAP'

[   19.089518] bone_capemgr bone_capemgr: slot #4: dtbo 'BB-BONE-REPLICAP-0A4A.dtbo' loaded; overlay id #0



I tried to fix the prio 0 error by installing device-tree-overlays from the  thing repo, followed by enabling the cape in uEnv.txt and running update-initramfs.  NoGo.




---
**James Armstrong** *January 21, 2016 03:36*

Which board do you have? It looks like it saw a B3 and an override A4. 


---
**Matt LaRussa** *January 21, 2016 03:50*

I have a B3.  I just noticed that override..



debian@beaglebone:/sys/devices/platform/bone_capemgr$ cat slots

 0: P-----  -1 Replicape 3D printer cape,00B3,Intelligent Agen,BB-BONE-REPLICAP

 1: PF----  -1 

 2: PF----  -1 

 3: PF----  -1 

 4: P-O-L-   0 Override Board Name,0A4A,Override Manuf,BB-BONE-REPLICAP

﻿




---
**Matt LaRussa** *January 21, 2016 03:55*

I had the 0A4A listed in /etc/default/capemgr.  I changed it to 00B3 and after a reboot got the following:



debian@beaglebone:~$ dmesg|grep bone_cape

[    3.913409] bone_capemgr bone_capemgr: Baseboard: 'A335BNLT,00C0,5214BBBK1111'

[    3.913440] bone_capemgr bone_capemgr: compatible-baseboard=ti,beaglebone-black - #slots=4

[    3.955807] bone_capemgr bone_capemgr: slot #0: 'Replicape 3D printer cape,00B3,Intelligent Agen,BB-BONE-REPLICAP'

[    4.011160] bone_capemgr bone_capemgr: slot #1: No cape found

[    4.071240] bone_capemgr bone_capemgr: slot #2: No cape found

[    4.131134] bone_capemgr bone_capemgr: slot #3: No cape found

[    4.137221] bone_capemgr bone_capemgr: initialized OK.

[    5.151321] bone_capemgr bone_capemgr: loader: failed to load slot-0 BB-BONE-REPLICAP:00B3 (prio 0)

[   15.737269] bone_capemgr bone_capemgr: part_number 'BB-BONE-REPLICAP', version '00B3'

[   15.737303] bone_capemgr bone_capemgr: slot #4: override

[   15.737322] bone_capemgr bone_capemgr: Using override eeprom data at slot 4

[   15.737339] bone_capemgr bone_capemgr: slot #4: 'Override Board Name,00B3,Override Manuf,BB-BONE-REPLICAP'





and....



debian@beaglebone:~$ cat /sys/devices/platform/bone_capemgr/slots

 0: P-----  -1 Replicape 3D printer cape,00B3,Intelligent Agen,BB-BONE-REPLICAP

 1: PF----  -1 

 2: PF----  -1 

 3: PF----  -1 




---
**James Armstrong** *January 21, 2016 03:56*

This is mine. 



0: P-<s>-L</s>   0 Replicape 3D printer cape,00B3,Intelligent Agen,BB-BONE-REPLICAP

 1: PF----  -1 

 2: PF----  -1 

 3: PF----  -1 

debian@kamikaze:/sys/devices/platform/bone_capemgr$ 



Although we had problems writing the eeprom on my friends board but finally got it written and he showed an override on #4 but it was a B3 on both slots so it didn't seem to cause a problem as it said the board was already loaded. 


---
**James Armstrong** *January 21, 2016 03:58*

    3.228549] bone_capemgr bone_capemgr: compatible-baseboard=ti,beaglebone-black - #slots=4

[    3.267032] bone_capemgr bone_capemgr: slot #0: 'Replicape 3D printer cape,00B3,Intelligent Agen,BB-BONE-REPLICAP'

[    3.322517] bone_capemgr bone_capemgr: slot #1: No cape found

[    3.382478] bone_capemgr bone_capemgr: slot #2: No cape found

[    3.442478] bone_capemgr bone_capemgr: slot #3: No cape found

[    3.448413] bone_capemgr bone_capemgr: initialized OK.

[    3.470839] bone_capemgr bone_capemgr: slot #0: dtbo 'BB-BONE-REPLICAP-00B3.dtbo' loaded; overlay id #0


---
**James Armstrong** *January 21, 2016 03:59*

And my uEvn.txt



~                                                                                                                                                                                            #Docs: [http://elinux.org/Beagleboard:U-boot_partitioning_layout_2.0](http://elinux.org/Beagleboard:U-boot_partitioning_layout_2.0)



uname_r=4.1.6-bone15

##uuid=

dtb=am335x-boneblack-replicape.dtb

cmdline=coherent_pool=1M quiet consoleblank=0 fbcon=rotate:1 init=/lib/systemd/systemd



#In the event of edid real failures, uncomment this next line:

#cmdline=coherent_pool=1M quiet consoleblank=0 fbcon=rotate:1 init=/lib/systemd/systemd video=HDMI-A-1:1024x768@60e

cmdline=coherent_pool=1M quiet consoleblank=0 fbcon=rotate:1 init=/lib/systemd/systemd video=HDMI-A-1



##Example v3.8.x

#cape_disable=capemgr.disable_partno=

#cape_enable=capemgr.enable_partno=



##Example v4.1.x

#cape_disable=bone_capemgr.disable_partno=

#cape_enable=bone_capemgr.enable_partno=



cmdline=coherent_pool=1M quiet init=/lib/systemd/systemd consoleblank=0 fbcon=rotate:1



##enable BBB: eMMC Flasher:

#cmdline=init=/opt/scripts/tools/eMMC/init-eMMC-flasher-v3.sh consoleblank=0 fbcon=rotate:1



uuid=a540a1f5-ca80-4447-b31e-985536fb7cf8




---
**Matt LaRussa** *January 21, 2016 04:10*

i think the overlay is the problem...

bone_capemgr bone_capemgr: loader: failed to load slot-0 BB-BONE-REPLICAP:00B3 (prio 0)



I tried to interpret the Angstrom fix for Debian by installing the only package in the thing printer repo that mentions overlays.  Doesn't look like it worked.


---
**James Armstrong** *January 21, 2016 04:35*

Have you tried the new 01-05-2016 kamikazi image? That is what I am running. It has a few minor issues with a pip module that keeps octoprint from updating plugins. 


---
**Matt LaRussa** *January 21, 2016 06:13*

I gave up on the debian build.  Back to kamikaze.  Recognizes the cape, but octoprint times out when trying to reach it over the network.  operate seems to work fine.


---
**Matt LaRussa** *January 21, 2016 06:13*

I just have to remember to not use apt-get update/upgrade/dist-upgrade.


---
**James Armstrong** *January 21, 2016 13:22*

**+Matt LaRussa** Try using the ip:5000 instead of the kamikaze.local/octoprint




---
**Jon Charnas** *January 22, 2016 13:41*

Quick lookup of how to block a package update in an apt-get system: [http://askubuntu.com/questions/18654/how-to-prevent-updating-of-a-specific-package](http://askubuntu.com/questions/18654/how-to-prevent-updating-of-a-specific-package)



I hope this will help you guys figure out the pip/python/octoprint stuff, I'm hoping to get my Replicape and BBB soon.


---
*Imported from [Google+](https://plus.google.com/+MattLaRussa/posts/eQ1P7Ar3jNf) &mdash; content and formatting may not be reliable*
