---
layout: post
title: "Is there a way to have my replicape cooling fan only run when the steppers are enabled?"
date: August 07, 2017 15:17
category: "Discussion"
author: Tim Curtis
---
Is there a way to have my replicape cooling fan only run when the steppers are enabled?



If not can I just add a thermistor on or near the stepper driver and use the H channel?



I've been printing with the replicape for a week now and it is working awesome....







**Tim Curtis**

---
---
**Dave Posey** *August 07, 2017 22:59*

I'd add a M106 command to your startup and stop scripts in cura.



If in doubt,  configure Cura for the desktop, tweak the gcode scripts under advanced.



Then use Cura export to generate an ini file that you can then load into the Cura settings in Octoprint.


---
**Tim Curtis** *August 08, 2017 11:18*

Thanks Dave, I can try that.


---
**Tim Curtis** *August 14, 2017 16:00*

It works to put M106 P3 S255 in my print program, the only thing I have run into is that the M107's that are in my program to stop my cooling fan also stop my driver fan. Once I figure out how to make Cura write M107 P1 or M106 P1 S0 then it won't shut off my driver fan.


---
*Imported from [Google+](https://plus.google.com/106115959585163180149/posts/97rZcyJdHbn) &mdash; content and formatting may not be reliable*
