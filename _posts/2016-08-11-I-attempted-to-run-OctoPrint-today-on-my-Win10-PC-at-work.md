---
layout: post
title: "I attempted to run OctoPrint today on my Win10 PC at work"
date: August 11, 2016 20:33
category: "Discussion"
author: Dan Cook
---
I attempted to run OctoPrint today on my Win10 PC at work. I made it through Step: 6 from ([https://github.com/foosel/OctoPrint/wiki/Setup-on-Windows](https://github.com/foosel/OctoPrint/wiki/Setup-on-Windows)) and could not get Octoprint to start from using Step: 7 "C:\Octoprint>octoprint"



The first time I tried, I did have to allow permissions for my network, but after the second through fifteenth attempts, the command screen gave the typical "wall of text" but I never got a window for OctoPrint to pop up.



Any suggestions?



Thanks!





**Dan Cook**

---
---
**Elias Bakken** *August 11, 2016 21:28*

OctoPrint runs on the BeagleBone, no need to install any software! Or am I missing something? 


---
**Dan Cook** *August 11, 2016 22:48*

Elias, I may have misunderstood what i saw... I was reading about OctoPrint and saw a guide to "Setup on Windows"... I thought I'd give it a shot... it was working great up to the last step, lol

[https://github.com/foosel/OctoPrint/wiki/Setup-on-Windows](https://github.com/foosel/OctoPrint/wiki/Setup-on-Windows)

I was just assuming there would have to be some kind of software installed on the PC to control the printer from my PC's browser.



I'm a civil engineer... I can build you a bridge, but when it comes to this stuff it's a little tricky for me.


---
**Elias Bakken** *August 11, 2016 22:50*

Hehe, well rest assured, once you have the printer set up (there is the issue of flashing the eMMC) all units with a browser in your network should have access to the printer, no software needed!


---
**Dan Cook** *August 11, 2016 22:57*

OK, cool... I'll order the BBB today and start cramming all of this info, lol... is it possible to work with the BBB before I have the Replicate?


---
**Elias Bakken** *August 11, 2016 23:06*

Yes!


---
**Dan Cook** *August 15, 2016 16:32*

BBB should be here tomorrow and I'm not entirely sure if there is a micro sd card included. I'm going to pick one up at Best Buy and get it loaded with Kamikaze. Are there any suggestions/requirements as far as capacity/speed/class? 


---
**Elias Bakken** *August 15, 2016 18:19*

4GB og you can find it!


---
**Dan Cook** *August 17, 2016 13:18*

I have BBB and flashed SD Card ready with Kamikaze 1.1.0... I just want to verify if it is possible to flash the BBB with Kamikaze before I have the Replicape installed, and if there is any functionality with the software.


---
**Elias Bakken** *August 17, 2016 19:37*

Yes! That works fine


---
*Imported from [Google+](https://plus.google.com/105892126556694282818/posts/GkU44dhtfng) &mdash; content and formatting may not be reliable*
