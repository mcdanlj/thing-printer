---
layout: post
title: "I've printed an enclosure for the new 24v power supply I purchased ( designed for audio power amps so very clean rails)"
date: May 12, 2016 08:37
category: "Discussion"
author: tizzledawg
---
I've printed an enclosure for the new 24v power supply I purchased ( designed for audio power amps so very clean rails). I had also got a E3D 24v hotend but forgot about the HPB which is still 12v. 



Is there a way to limit the  HPB output to 50% duty so it is effectively still 12v?





**tizzledawg**

---
---
**Elias Bakken** *May 12, 2016 08:39*

That has been on my block for a while, I have that same problem, but it has not been prioritized. Should be a very easy fix, though!


---
**Ryan Carlyle** *May 12, 2016 14:33*

You need 25% duty to run a 12v bed on 24v. It's also very hard on the power supply and insanely dangerous if the PWM controller freezes up. 


---
**Elias Bakken** *May 12, 2016 15:36*

I think 50% is duty cycle is right, but you are right about the PSU or rather the decoupling caps. 


---
**Ryan Carlyle** *May 12, 2016 15:56*

It's a purely resistive load and a 12v bed has 1/4th the resistance of an equivalent 24v bed. For the sake of easy math, say it's a 120w heatbed. With a 12v PSU, that will be 10A and 1.2 ohm resistance. If you then put a 24v PSU on it, you'll get 20A and 480w instantaneous power. To regulate the average power back to the bed's rating of 120w, you need a 25% duty cycle.



I would also personally size the PSU and wiring for the full current draw at 100% duty cycle, to be safe. There are a LOT of things that can go wrong when you use excessive voltage on resistive heaters.


---
**Elias Bakken** *May 12, 2016 16:09*

Sorry, you are right! Brainfart.


---
**tizzledawg** *May 13, 2016 08:04*

Been bashing my head about this but I have come to agree as well. 



What is the bit depth of the PWM for the heaters? If the Replicape is working at 25% duty what kind of resolution is left going from 0 - 25%?


---
**Elias Bakken** *May 13, 2016 09:09*

It's 12 bit, do there is still a fair bit left


---
**Dejay Rezme** *May 15, 2016 20:50*

I'm gonna need this as well. Redeem currently doesn't use PWM to control the heater outputs?


---
**tizzledawg** *May 17, 2016 11:56*

I believe it does use PWM. It would be hard for the PID control loop to work if it were only on and off. ( I know PWM is only on and off but at a relatively high frequency :P ).


---
**Dejay Rezme** *May 17, 2016 12:37*

**+tizzledawg** Ah thanks. Well my bed heater is 12V / 120W. That should mean 1.2Ω and 10 amps at 12V and 20 ampere at 24V. So that would be way too much even with PWM correct?

Damnit going with 24V was a huge mistake for a beginner lol.


---
**tizzledawg** *May 18, 2016 12:33*

12v is more common for sure but I think the advantages of 24v outweigh the hassle. I just bought a 24v heatbed on ebay so I guess I'll find out soon enough.


---
*Imported from [Google+](https://plus.google.com/108496831286068938419/posts/7GCFRyjQM6V) &mdash; content and formatting may not be reliable*
