---
layout: post
title: "The M109 blocks other commands bug has been there for a while, but now that I actually came around to fixing it, it was gone"
date: January 06, 2015 15:27
category: "Discussion"
author: Elias Bakken
---
The M109 blocks other commands bug has been there for a while, but now that I actually came around to fixing it, it was gone. Can someone please verify this?

[https://bitbucket.org/intelligentagent/redeem/issue/18/m109-blocks-all-other-commands](https://bitbucket.org/intelligentagent/redeem/issue/18/m109-blocks-all-other-commands)





**Elias Bakken**

---
---
**James Armstrong** *January 06, 2015 15:45*

Could I just run the package updater and get all the new changes?


---
**Elias Bakken** *January 06, 2015 15:54*

It depends on what you want. My latest commits have been to the develop branch, but the Redeem package is created from Master. If it's been a long time since you updated, you should do an update of Redeem at least. But if you want Bed probing and compensation, you need to install from git. I'll try to merge soon, but it would be nice with some beta-testing first : )


---
**James Armstrong** *January 21, 2015 22:40*

FYI, I backed your Manga screen. Can't wait. 


---
**Elias Bakken** *January 21, 2015 22:44*

  Awesome! :)


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/aETQQ2YTdBN) &mdash; content and formatting may not be reliable*
