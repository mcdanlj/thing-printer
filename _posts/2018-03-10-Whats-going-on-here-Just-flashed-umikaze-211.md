---
layout: post
title: "What's going on here? Just flashed umikaze 2.1.1"
date: March 10, 2018 17:48
category: "Discussion"
author: Derek Kuraitis
---
What's going on here? Just flashed umikaze 2.1.1. I'm completely new to this. I could probably figure it out, I just need a point in the right direction. Thankyou!

![images/23b6f45eeb7190f290cc708f3000b3d3.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/23b6f45eeb7190f290cc708f3000b3d3.jpeg)



**Derek Kuraitis**

---
---
**Jon Charnas** *March 10, 2018 17:59*

The SPI device failure is because the SPI device is the Replicape. Install it on top and that will go away. The video0 error is because it's looking for a webcam. The Webcam issue is not a blocking problem, but the SPI device will be problematic.



As for why you're seeing this instead of Toggle, it's because in 2.1.1 Toggle is known to have a memory leak - meaning it consumes continually more RAM until it crashes the system. We disabled it by default, but it's still there.



You can re-enable it with "systemctl enable toggle" and it will show at the next boot. Or start it in your current session with "systemctl start toggle" to enable it here. We recommend activating toggle only if you're not going to be running the printer continuously for more than 24 hours, or it'll become a problem.



Remember to edit /etc/toggle/local.cfg with the OctoPrint API key and to choose the right theme for your screen (resolution and aspect-ratio).



To make the screen display in landscape mode instead, make sure you disable the console_rotation setting in /etc/toggle/local.cfg (overwrite the rotation setting with a 0) and take out the console or framebuffer rotation from /boot/uEnv.txt for the console to show in landscape mode.


---
**Elias Bakken** *March 10, 2018 18:01*

Hi! This is not necessarily wrong, It depends on what you expect. You should run with the cape on though, in order for Redeem to start up.


---
**Derek Kuraitis** *March 10, 2018 19:53*

ah ok. It seems I'm just impatient. It's still working on something 2hrs in.


---
**Derek Kuraitis** *March 10, 2018 19:58*

well I can ping it, so I guess Ill continue with the rest thankyou.


---
**Jon Charnas** *March 10, 2018 21:18*

**+Derek Kuraitis** can you specify what it's working on even 2 hours in?


---
**Derek Kuraitis** *March 11, 2018 13:00*

I forgot to write it down, and I can only scroll up a few pages. It was cleaning directories I believe.


---
**Marcus Lövstrand** *March 11, 2018 16:26*

Will the memory leak of Toggle be fixed in 2.1.2?


---
**Jon Charnas** *March 11, 2018 16:28*

I will do my best to ensure that it is. At the moment I haven't had the occasion of testing it though two or three folks have at one point said they would tackle it. Since I'm still building RC images at the moment I'm not super-concerned about that detail for now, but the script enables Toggle by default.


---
*Imported from [Google+](https://plus.google.com/114326755084967855066/posts/DgrMUGHNbJE) &mdash; content and formatting may not be reliable*
