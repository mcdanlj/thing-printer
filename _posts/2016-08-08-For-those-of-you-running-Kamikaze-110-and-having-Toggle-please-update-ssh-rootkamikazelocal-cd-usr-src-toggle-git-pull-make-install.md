---
layout: post
title: "For those of you running Kamikaze 1.1.0 and having Toggle, please update: ssh root@kamikaze.local cd /usr/src/toggle/ git pull make install"
date: August 08, 2016 14:00
category: "Discussion"
author: Elias Bakken
---
For those of you running Kamikaze 1.1.0 and having Toggle, please update: 

[https://bitbucket.org/intelligentagent/toggle/issues/19/toggle-fails-to-establish-communication](https://bitbucket.org/intelligentagent/toggle/issues/19/toggle-fails-to-establish-communication)



ssh root@kamikaze.local

cd /usr/src/toggle/

git pull

make install





**Elias Bakken**

---


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/gbLzL2XegBE) &mdash; content and formatting may not be reliable*
