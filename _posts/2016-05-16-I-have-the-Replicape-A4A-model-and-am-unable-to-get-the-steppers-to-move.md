---
layout: post
title: "I have the Replicape A4A model and am unable to get the steppers to move"
date: May 16, 2016 12:04
category: "Discussion"
author: IonicBasher
---
I have the Replicape A4A model and am unable to get the steppers to move. I know the wiring is correct because it works fine with machinekit.

It says in the redeem log it is being "INFO     Found Replicape rev. 00B3".

Is this the problem? I have gotten the steppers to hum abit but no more even with messing with the different configurations and microsteps/current.



I also tried the quad extruder setup because I believed the A4A was used but also didnt work.



Any help appreciated.





**IonicBasher**

---
---
**Elias Bakken** *May 16, 2016 12:12*

Yes, the EEPROM has been overwritten when you flashed with Kamikaze. Just update the EEPROM with A4 blob and reboot!


---
**IonicBasher** *May 16, 2016 12:40*

I forgot to mention that I tried updating EEPROM and also updating redeem to 1.2.2. Just to make sure I tried updating EEPROM again as per debian instructions on Replicape A4A page. Still loading as 00B3 on redeem log.

Any other notable differences in setting up a model A4A from revB?


---
**IonicBasher** *May 16, 2016 22:53*

Is there a line I can comment out to halt the use of the B3 EEPROM or manually switch to the A4A?


---
**Elias Bakken** *May 16, 2016 23:31*

See these instructions,

[http://wiki.thing-printer.com/index.php?title=Replicape_rev_B#Debian](http://wiki.thing-printer.com/index.php?title=Replicape_rev_B#Debian)

but exchange for A4A


---
**IonicBasher** *May 17, 2016 04:57*

I tried with:

wget [https://bitbucket.org/intelligentagent/replicape/src/bf08295bbb5e98ce6bff60097fe9b78d96002654/eeprom/Replicape_00A4.eeprom](https://bitbucket.org/intelligentagent/replicape/src/bf08295bbb5e98ce6bff60097fe9b78d96002654/eeprom/Replicape_00A4.eeprom)



cat Replicape_00B3.eeprom > /sys/bus/i2c/drivers/at24/2-0054/at24-1/nvmem



Did not work. 

I also tried editing /boot/uEnv.txt to include A4A but also did not work.



The A4A model does not seem to be tested with Debian to be reliable. I will go back to Angrstrom image for now.


---
**IonicBasher** *May 17, 2016 04:58*

*cat Replicape_00A4.eeprom > /sys/bus/i2c/drivers/at24/2-0054/at24-1/nvmem


---
**Elias Bakken** *May 17, 2016 10:50*

No, the A4 definitivt world better with Debian. What whent wrong with the update? 


---
*Imported from [Google+](https://plus.google.com/100874739223986876085/posts/W7FhYPQCaFV) &mdash; content and formatting may not be reliable*
