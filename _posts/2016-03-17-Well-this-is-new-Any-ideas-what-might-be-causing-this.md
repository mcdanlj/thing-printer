---
layout: post
title: "Well this is new... Any ideas what might be causing this?"
date: March 17, 2016 20:43
category: "Discussion"
author: Chris Romey
---
Well this is new...



Any ideas what might be causing this?  Improper PID loop?

![images/7f0147fd6fb608126d5a0d284dc4114b.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/7f0147fd6fb608126d5a0d284dc4114b.png)



**Chris Romey**

---
---
**Elias Bakken** *March 17, 2016 21:36*

Did it just go off like that? After much time? Could you post your redeem log somewhere in a pastebin?


---
**Ante Vukorepa** *March 17, 2016 22:09*

That looks suspiciously like some kind of an accumulating overflow.


---
**Chris Romey** *March 18, 2016 01:22*

Elias, it was about an hour in to the print.  I will get the redeem log of the print for you.  Looking through it at the moment but I'm not seeing anything very useful.  There's an M104 S205 where I bump the temp down then back to 210, which seemed to level the temps back out.


---
**Chris Romey** *March 18, 2016 01:29*

Here's the redeem log. [http://files.mrzood.com/plugin_redeem.log](http://files.mrzood.com/plugin_redeem.log)


---
**Elias Bakken** *March 18, 2016 07:31*

Without having looked through the log, what is your I on the PID?


---
**Chris Romey** *March 18, 2016 15:39*

pid_i_E = 0.01


---
**Chris Romey** *March 18, 2016 17:11*

For what it's worth, I'm using the E3D v6 hot-end PID chart since that's the hot end I'm using.



# E3D v6 Hot end

temp_chart_E = SEMITEC-104GT-2

pid_p_E = 0.1

pid_i_E = 0.01

pid_d_E = 0.3

ok_range_E = 4.0


---
**Chris Romey** *March 18, 2016 20:00*

I let the thermals do their own thing and this is how it went: [https://drive.google.com/file/d/0B4vlYd5QDwgBY2JIc3pPcjdxd1E/view?usp=sharing](https://drive.google.com/file/d/0B4vlYd5QDwgBY2JIc3pPcjdxd1E/view?usp=sharing)


---
**Elias Bakken** *March 18, 2016 20:18*

I have not seen this before. I've always had rock steady temperatures using E3D ith similar settings... I'm at a loss.


---
**Ante Vukorepa** *March 18, 2016 20:22*

Some kinda cumulative rounding error in the PID loop, maybe?


---
**Chris Romey** *March 18, 2016 22:15*

I'll see if I can get some more useful info from the logs...


---
**Jon Charnas** *March 22, 2016 08:48*

**+Chris Romey** did you try running an M303 to get new PID settings? I've found that the default for E3D are ok, but M303 fine-tunes it and avoids this kind of over-heating over long term.


---
**Chris Romey** *March 24, 2016 19:02*

Finally have some time to play around with the PID tuning.  While my PID is way off, it's no longer doing the long over/undershoot.  Still some more tuning to go.


---
**Chris Romey** *March 24, 2016 19:06*

**+Elias Bakken** are there any special considerations I should be taking with the M303 values that are returned when putting them in my local.cfg? 


---
**Elias Bakken** *March 24, 2016 19:08*

Use m500 to store the values. Are you on devel? I saw an error retired with m500 on devel just today.


---
**Chris Romey** *March 24, 2016 19:36*

I'm on:

FIRMWARE_VERSION:1.1.8~Raw Deal


---
**Elias Bakken** *March 24, 2016 20:27*

Should be ok then, but take a look in the local.cfg to make sure it was stored.


---
*Imported from [Google+](https://plus.google.com/113335787317667734340/posts/JfiXLAQFLaL) &mdash; content and formatting may not be reliable*
