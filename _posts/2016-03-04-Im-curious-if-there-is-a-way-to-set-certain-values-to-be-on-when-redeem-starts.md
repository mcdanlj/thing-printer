---
layout: post
title: "I'm curious if there is a way to set certain values to be on when redeem starts?"
date: March 04, 2016 11:55
category: "Discussion"
author: Tim Curtis
---
I'm curious if there is a way to set certain values to be on when redeem starts? Like start up routines? Like having Fan 3 on ar start up. I use fan 3 for my lights.





**Tim Curtis**

---
---
**Elias Bakken** *March 04, 2016 14:25*

Hm... not automatically, but there is a way to do it. Redeem sets up a couple of pipes that you can echo commands to with a boot script. Out this in a boot-script: 

echo "M106 P3 S255" > /dev/testing_noret_1




---
**Tim Curtis** *March 04, 2016 14:56*

Thanks, I'll give that a try.


---
**Jarek Szczepański** *March 04, 2016 18:49*

in Octoprint's settings there is a tab "GCODE Scripts" - use a field called "After connection to printer is established" (which happens automagically :) ) and put your gcode there




---
*Imported from [Google+](https://plus.google.com/106115959585163180149/posts/QrgjwUmq2G2) &mdash; content and formatting may not be reliable*
