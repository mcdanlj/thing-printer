---
layout: post
title: "I have started setting up my replicape on my old printer that was running ramp 1.4 before"
date: July 24, 2017 12:31
category: "Discussion"
author: Tim Curtis
---
I have started setting up my replicape on my old printer that was running ramp 1.4 before. Everything is going well except for the autotune.



I tried tuning the bed with the command M303 E-1 P1 because I have a high power bed heater.  Anyway the first cycle went great as far as it heated to 100C and the cycled to cool. Then it started reheating again but was trying to heat to 200C? I thought maybe it would kick off at 100C but it climbed past 100 so I shut it off.



Did I do something wrong or is that a bug? I'm using the new Kamakazi 2.01 image.



I did try a M303 S60 E-1 but it seemed like it was heating very slowly.





**Tim Curtis**

---
---
**Jon Charnas** *July 24, 2017 12:53*

I generally run M303 S65 E-1 C8 to PID the bed when powered via mosfet, but I'm not sure if the additional parameter will help or not. Are you using the mosfet or an SSR to control the bed (bang-bang in RAMPS speak)


---
**Tim Curtis** *July 24, 2017 13:09*

Hi John, Thank you for the info.

I'm using the mosfet. My bed draws about 15 amps max. I used an SSR on my old ramps board but wanted to just use the mosfet on this one.

I'll try running the command the way you said tonight. I just wasn't sure why autotune would run at100C the first cycle and then jump to 200C for the next cycle. I can see 200 for the hotend but not a bed.



I did add my PID settings from my ramps board into my config file on the replicape but that didn't seem to work out.


---
**Jon Charnas** *July 24, 2017 13:12*

Yeah PID settings vary by software/hardware platform as the frequency at which loops run and how long they take to complete are different... Normally setting the temp should be ok. But if you ran M303 without an S setting, the default is, I think 200 degrees (we may want to change that so it detects when the heatbed is selected instead of a hotend).


---
**Holger Rusch** *July 25, 2017 02:38*

Try M303? to get detailed help on the command. That is: add a question mark. 


---
**Tim Curtis** *July 25, 2017 11:18*

Thank you for the tip Holger.



Jon, I was able to get the tuning working by entering the heat (S) parameter. Thank you again for the info.


---
**Jon Charnas** *July 25, 2017 11:19*

Glad it's working for you!


---
**Holger Rusch** *July 25, 2017 11:20*

do you really need pid for the bed? is it that powerfull? most ppl should be safe with bitbanging for the bed.




---
*Imported from [Google+](https://plus.google.com/106115959585163180149/posts/8Wy6PdpR7yh) &mdash; content and formatting may not be reliable*
