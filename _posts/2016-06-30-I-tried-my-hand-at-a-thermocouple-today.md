---
layout: post
title: "I tried my hand at a thermocouple today"
date: June 30, 2016 21:52
category: "Discussion"
author: Elias Bakken
---
I tried my hand at a thermocouple today. Turns out it was super simple when using a linear converter. The biggest challenge was adding some more ADC channels to the DT. 



![images/19d566a0b8a39c8f92499bcb1a68e619.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/19d566a0b8a39c8f92499bcb1a68e619.jpeg)



**Elias Bakken**

---
---
**tizzledawg** *August 07, 2016 04:54*

Can you add details about how you got this working please?

I have a Rev B3 board with a E3D V6 at the moment but I am looking at what is needed for a thermocouple to expand the temp range for more exotic filaments. 



It looks like you need a interface board to convert the mV thermocouple output to a 0-5 analog output for the replicape but what happens on the "firmware" side to scale this to the correct temps?



Thanks


---
**Elias Bakken** *August 07, 2016 16:52*

Hi! You need to implement the class for the sensor you have. Is it pt100? You might need to divide the voltage down so it fits in the 0-1.8V range as well.


---
**tizzledawg** *August 08, 2016 08:25*

I haven't got any parts yet. I prefer to work it all out before buying stuff if possible.



I've never done anything in python so might need a bit of help there.


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/2ohX1MDAJET) &mdash; content and formatting may not be reliable*
