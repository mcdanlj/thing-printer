---
layout: post
title: "So my board isn't being recognized by the BBB"
date: February 06, 2016 21:26
category: "Discussion"
author: Hitbox Alpha
---
So my board isn't being recognized by the BBB. and i can not flash the EEPROM on the replicape. Any ideas? 





**Hitbox Alpha**

---
---
**James Armstrong** *February 06, 2016 21:39*

Can't flash as in why? Permissions problem? We had that and had to temporarily change ownership of the NVRAM device file to debian user then Flash it. 


---
**Hitbox Alpha** *February 06, 2016 21:44*

if i try to flash the third line comes up with this error "cat: write error: Connection timed out"


---
**Hitbox Alpha** *February 06, 2016 21:45*

i think it is a permissions problem. how would i change ownership? i'm very new to linux 


---
**James Armstrong** *February 06, 2016 21:55*

That doesn't sound like the error we had. I'm going by memory here. 



sudo chown debian:debian nvmem 



Then you do the:



cat Redeem????(insert EEPROM file name) > nvmem


---
**Hitbox Alpha** *February 06, 2016 21:57*

hmmm the sudo command give this "chown: cannot access 'nvmem': No such file or directory" so i don't think thats it


---
**James Armstrong** *February 06, 2016 22:00*

I left all the directory info off. You can enter it in before the nvmem or cd to the directory first. It 



sudo chown debian:debian /sys/bus/i2c/devices/2-0054/at24-1/nvmem



cat Replicape.eeprom > /sys/bus/i2c/devices/2-0054/at24-1/nvmem


---
**Hitbox Alpha** *February 06, 2016 22:06*

stilll nothing. i'm very lost now


---
**James Armstrong** *February 06, 2016 22:08*

What did that return? I assume you have the EEPROM file for the B3 to write? There is an IRC channel that a few people hangs out in. I'm not home right now so I'm answering what I remember. What error did you get doing that?


---
**Hitbox Alpha** *February 07, 2016 00:13*

on the second command now 

cat: Replicape.eeprom: No such file or directory


---
**James Armstrong** *February 07, 2016 00:19*

Did you compile that file with the EEPROM build instructions? If not I can post the file. It maybe on the bitbucket repository of the wiki. Let me check there first. 


---
**Hitbox Alpha** *February 07, 2016 00:19*

no i did not. please if you can post the file


---
**James Armstrong** *February 07, 2016 00:22*

commands to download it from the wiki then write it:



 sudo -s

 wget [https://bitbucket.org/intelligentagent/replicape/raw/bf08295bbb5e98ce6bff60097fe9b78d96002654/eeprom/Replicape_00B3.eeprom](https://bitbucket.org/intelligentagent/replicape/raw/bf08295bbb5e98ce6bff60097fe9b78d96002654/eeprom/Replicape_00B3.eeprom)
 

cat Replicape_00B3.eeprom > /sys/bus/i2c/drivers/at24/2-0054/at24-1/nvmem


---
**Hitbox Alpha** *February 07, 2016 00:24*

yes i've tried to use that commands. on the third line i get this error "cat: write error: Connection timed out" 


---
**James Armstrong** *February 07, 2016 00:26*

I wonder if it isn't seeing the board at all? You do have the Replicape plugged in?


---
**Dave Posey** *February 07, 2016 00:26*

Try this command to download the eeprom image:

wget [https://bitbucket.org/intelligentagent/replicape/raw/bf08295bbb5e98ce6bff60097fe9b78d96002654/eeprom/Replicape_00B3.eeprom](https://bitbucket.org/intelligentagent/replicape/raw/bf08295bbb5e98ce6bff60097fe9b78d96002654/eeprom/Replicape_00B3.eeprom)



Then in the 2nd command from James, replace Replicape.eeprom with Replicape_00B3.eeprom


---
**Hitbox Alpha** *February 07, 2016 00:26*

it does not. using "dmesg" it comes back with no capes attached


---
**Dave Posey** *February 07, 2016 00:27*

The above is from:  

[http://wiki.thing-printer.com/index.php?title=Replicape_rev_B#Update_EEPROM](http://wiki.thing-printer.com/index.php?title=Replicape_rev_B#Update_EEPROM)




---
**James Armstrong** *February 07, 2016 00:29*

the board is plugged on the right way right? just a stupid question. The cutout should be above the ethernet jack


---
**Dave Posey** *February 07, 2016 00:31*

Good point James.  Also, make sure you are providing power to the Replicape power connector.  The BeagleBone power connector will not work.


---
**James Armstrong** *February 07, 2016 00:31*

try this:



i2cdetect -y -r 2



See what it returns. Someone posted that on the IRC channel.


---
**Hitbox Alpha** *February 07, 2016 00:31*

yes it is. power is being supplied though the cape using a ATX power supply. i am connected though the Ethernet jack using bitvise to SSH in


---
**James Armstrong** *February 07, 2016 00:32*

I thought I was able to detect the board and flash over usb power but that was almost a month ago so I could be wrong. I know the PWM chip will  not work over usb power.


---
**Hitbox Alpha** *February 07, 2016 00:34*

i can flash the kamikaze image just fine. but the BBB is not picking up the replicape as a cape


---
**Dave Posey** *February 07, 2016 00:43*

Zachary, try and read back the EEPROM and tell us what it says.

cat /sys/bus/i2c/drivers/at24/2-0054/at24-1/nvmem



and ctrl+c to break the command when it gets to the "junk" 



It should start like:

▒U3▒A0Replicape 3D printer cape00B3Intelligent AgenBB-BONE-REPLICAP00000000001(




---
**James Armstrong** *February 07, 2016 00:45*

By flash I mean write the replicape eeprom.


---
**Hitbox Alpha** *February 07, 2016 00:45*

cat: /sys/bus/i2c/drivers/at24/2-0054/at24-1/nvmem: Connection timed out


---
**James Armstrong** *February 07, 2016 00:46*

I'm going to guess he gets a timeout.


---
**Hitbox Alpha** *February 07, 2016 00:51*

i don't even know how to get to the EEPROM?


---
**James Armstrong** *February 07, 2016 01:14*

must be a bad board or something. The eeprom is the 8 pin chip next to the T0 or rest thermistor input. Take the Replicape off and make sure no pins bent and missed the socket when it was plugged in.


---
**Hitbox Alpha** *February 07, 2016 01:15*

i have reset the board several times. each time all pins were seated 


---
**Hitbox Alpha** *February 07, 2016 01:16*

how would i go about checking the board?


---
**James Armstrong** *February 07, 2016 01:24*

Did you do the i2cdetect command? It looks like the PCA (PWM) chip is on the I2C buss also. Maybe we can see if it is detecting that at all. Post results of the command:



i2cdetect -y -r 2


---
**Hitbox Alpha** *February 07, 2016 01:25*

     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f

00:          -- -- -- -- -- -- -- -- -- -- -- -- --

10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

50: -- -- -- -- UU UU UU UU -- -- -- -- -- -- -- --

60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

70: -- -- -- -- -- -- -- --


---
**James Armstrong** *February 07, 2016 01:26*

I'm assuming that is showing something at 54,55,56,57 on the I2C addresses. I think the eeprom is at 54. I will have to fire mine up and see what it shows.


---
**James Armstrong** *February 07, 2016 01:29*

Here is mine. Pretty much the same except for something maybe on 70:



     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f

00:          -- -- -- -- -- -- -- -- -- -- -- -- --

10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

50: -- -- -- -- UU UU UU UU -- -- -- -- -- -- -- --

60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

70: 70 -- -- -- -- -- -- --


---
**James Armstrong** *February 07, 2016 01:30*

never mind. Docs say UU means probing was skipped because it was in use by a driver


---
**Hitbox Alpha** *February 07, 2016 01:30*

anyone know what that 70 means


---
**James Armstrong** *February 07, 2016 01:34*

how about trying this command and seeing what is returned on 00: 0-3



sudo i2cdump -f 2 0x54 (answer yes)


---
**James Armstrong** *February 07, 2016 01:42*

Doesn't look good. Should be numbers on 0-3 and ff on the rest. X probably means it couldn't read or talk to the chip. 


---
**Hitbox Alpha** *February 07, 2016 01:45*

So how do I get it to talk to the chip?


---
**Elias Bakken** *February 07, 2016 17:06*

70  is the PWM chip. Are you powering from USB?




---
**Hitbox Alpha** *February 07, 2016 17:23*

I got it to work. Kinda. I pulled the Cape off and used some canned air to clean out the sockets. After that the BBB connected. But for some reason all the mosfets would not turn off


---
*Imported from [Google+](https://plus.google.com/110719820264542745151/posts/QJyA4MVnfMi) &mdash; content and formatting may not be reliable*
