---
layout: post
title: "Haven't been using manga screen for a while"
date: December 29, 2016 15:07
category: "Discussion"
author: Step Cia
---
Haven't been using manga screen for a while. I hooked it up yesterday after hitting the update button in octoprint which it says it is updating toggle to latest. So I assume I have the latest toggle version. But the screen is not responsive it lags quite a bit. i did ass the api key to local config. Is it just my manga screen? Or is anyone else silky smooth when navigating with finger?





**Step Cia**

---
---
**Daniel Kusz** *December 29, 2016 15:39*

Hi Step,

Here is my working Manga Screen:


{% include youtubePlayer.html id=kHgRXv7yuRM %}
[youtube.com - Manga screen test](https://www.youtube.com/watch?v=kHgRXv7yuRM)

 My only problem is the very low feedrate for X and Y. Except this I think it works very fine.


---
**Elias Bakken** *December 29, 2016 18:27*

Is it a very complex model? Try a calibration cube and see how that goes? 




---
**Step Cia** *December 29, 2016 18:55*

Ok that looks good. Mine has the box empty with no model and the temp and navigation button can't be pressed. I kept tapping them and it just not responsive. I have 2 manga screen and swapping them give the same issue. So this might be related to setup? Something not correctly install in the software? 


---
**Daniel Kusz** *December 29, 2016 19:29*

Perhaps this is due to problems in the installation **+Elias Bakken** You mean my FEEDRATE? It happens at idle runs beyond printing. Printing works ok.


---
**Elias Bakken** *December 29, 2016 20:58*

If you can move the object, you should be able to switch to temp and jog as well. It's not always super responsive, so you might have to try a few times. **+Daniel Kusz**, the feed rate is set in OctoPrint, is it not? If not I think it must be hard coded :/


---
**Step Cia** *December 30, 2016 08:11*

Here's mine

![images/29db087a3c76f34e55c1487907ff88dd](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/29db087a3c76f34e55c1487907ff88dd)


---
**Elias Bakken** *January 03, 2017 19:25*

Ah, Step I see that you have a white line in the side there. That means either a faulty HDMI cable or a faulty EEPROM chip on the screen. Instead of getting 800x480 resolution you are getting a 640x480, which probably also throws the touch screen off... you could try to force a resolution and see if that helps. Google Beaglebone Black Custom HDMI resolution.


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/HdtKEPxy7JM) &mdash; content and formatting may not be reliable*
