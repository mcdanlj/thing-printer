---
layout: post
title: "So im having a new issue this time with the end stops"
date: October 16, 2016 03:56
category: "Discussion"
author: Edwardo Garabito
---
So im having a new issue this time with the end stops. 

As soon as i do a homing the end stops get hit twice then the machines gets disconnected from the server. 



I can see the LEDs flashing still on the board, And the motors sound like they are running. But after a couple of minutes it shuts down, the motors, but the LEDs stay on, and i can reconnect again. 



I get this error in the terminal every time it happens. 



Send: G91

Recv: ok

Send: G28 Z0

Unexpected error while reading serial port, please consult octoprint.log for details: SerialException: 'read failed: [Errno 5] Input/output error' @ comm.py:_readline:1618

Changing monitoring state from 'Operational' to 'Offline: SerialException: 'read failed: [Errno 5] Input/output error' @ comm.py:_readline:1618'

Connection closed, closing down monitor





In my Redeem log files everything looks normal, too me at least. 

The warning about a usb gadget serial does concern me, but to be honest im not sure what that even refers too. 



2016-10-16 03:15:03,843 root         INFO     Found Replicape rev. 00B3

2016-10-16 03:15:04,090 root         INFO     Cooler connects therm E with fan 1

2016-10-16 03:15:04,096 root         INFO     Added fan 0 to M106/M107

2016-10-16 03:15:04,116 root         INFO     Added fan 3 to M106/M107

2016-10-16 03:15:04,593 root         INFO     Stepper watchdog started, timeout 500 s

2016-10-16 03:15:04,607 root         WARNING  USB gadget serial not available as /dev/ttyGS0

2016-10-16 03:15:04,611 root         INFO     Ethernet bound to port 50000

2016-10-16 03:15:04,778 root         INFO     Pipe octoprint open. Use '/dev/octoprint_1' to communicate with it

2016-10-16 03:15:04,952 root         INFO     Pipe toggle open. Use '/dev/toggle_1' to communicate with it

2016-10-16 03:15:05,113 root         INFO     Pipe testing open. Use '/dev/testing_1' to communicate with it

2016-10-16 03:15:05,284 root         INFO     Pipe testing_noret open. Use '/dev/testing_noret_1' to communicate with it

2016-10-16 03:15:05,308 root         INFO     Alarm: Operational

2016-10-16 03:15:05,316 root         INFO     Watchdog started, refresh 30 s

2016-10-16 03:15:05,320 root         INFO     Redeem ready

2016-10-16 03:20:18,784 root         WARNING  Heater time update large: H temp: 0.0 time delta: 0.253078937531

2016-10-16 03:20:18,779 root         WARNING  Heater time update large: E temp: 23.8441449244 time delta: 0.252753973007

2016-10-16 03:20:19,000 root         WARNING  Heater time update large: HBP temp: 22.0505634333 time delta: 0.503028154373

2016-10-16 03:21:50,676 root         INFO     End Stop Y1 hit!

2016-10-16 03:21:51,897 root         INFO     End Stop Z1 hit!

2016-10-16 03:21:53,473 root         INFO     End Stop X1 hit!

2016-10-16 03:22:04,134 root         INFO     End Stop Z1 hit!

2016-10-16 03:22:04,369 root         INFO     End Stop Y1 hit!

2016-10-16 03:22:04,456 root         INFO     End Stop X1 hit!

2016-10-16 03:20:42,584 root         INFO     -- Logfile configured --

2016-10-16 03:20:43,180 root         INFO     Found Replicape rev. 00B3

2016-10-16 03:20:43,421 root         INFO     Cooler connects therm E with fan 1

2016-10-16 03:20:43,427 root         INFO     Added fan 0 to M106/M107

2016-10-16 03:20:43,443 root         INFO     Added fan 3 to M106/M107

2016-10-16 03:20:43,930 root         INFO     Stepper watchdog started, timeout 500 s

2016-10-16 03:20:43,935 root         WARNING  USB gadget serial not available as /dev/ttyGS0

2016-10-16 03:20:43,940 root         INFO     Ethernet bound to port 50000

2016-10-16 03:20:44,112 root         INFO     Pipe octoprint open. Use '/dev/octoprint_1' to communicate with it

2016-10-16 03:20:44,288 root         INFO     Pipe toggle open. Use '/dev/toggle_1' to communicate with it

2016-10-16 03:20:44,450 root         INFO     Pipe testing open. Use '/dev/testing_1' to communicate with it

2016-10-16 03:20:44,612 root         INFO     Pipe testing_noret open. Use '/dev/testing_noret_1' to communicate with it

2016-10-16 03:20:44,633 root         INFO     Alarm: Operational

2016-10-16 03:20:44,651 root         INFO     Watchdog started, refresh 30 s

2016-10-16 03:20:44,655 root         INFO     Redeem ready





**Edwardo Garabito**

---
---
**Edwardo Garabito** *October 16, 2016 04:15*

Heres a video of whats happening

![images/8c2b9cfb5a308ce3b23e66f9fb5541a3](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/8c2b9cfb5a308ce3b23e66f9fb5541a3)


---
**Elias Bakken** *October 16, 2016 12:54*

It's difficult to say from the video, but it looks like your bbb is turning off. It stops blinking and then it turns back on, which would be consistent with being able to do things again after 5 minutes. 


---
**Elias Bakken** *October 16, 2016 12:55*

Can you see if that is happening?


---
**Edwardo Garabito** *October 16, 2016 13:00*

The bbb stays powered. You can see the leds still flashing on the system after I try to home it. In very puzzled. Ill try a shorter wireing connection to see if that'll fix the an issue with interference may be. But I'm defiantly a bit stumped. 


---
**Elias Bakken** *October 16, 2016 13:04*

It looks like there is no heartbeat on the leds around 2:40, only power.


---
**Edwardo Garabito** *October 16, 2016 14:07*

You are right the heart beat does stop, But the light from the Ethernet port and another led does stay on. What do yo think could be the source of this? Wheres the 1st place yo would check? 



Ill shoot another short video later today giving a better view of wiring and what goes on. With e better quality camera  


---
**Elias Bakken** *October 16, 2016 14:35*

I know that earlier versions of Kamikaze has had one of the end stops wired up as the "on/off" button, so maybe this is what you are seeing. But I think this is a part of  bigger issue where some installations are not succeeding as they should! I'll spend some time researching this today!


---
**Elias Bakken** *October 16, 2016 14:37*

Meanwhile, try to press each of the end stop buttons and see which end stop is wired as the on/off button. It might take up to a minute to perform a shut down. When you find the end stop that is the culprit, switch it out with a different one!






---
**Edwardo Garabito** *October 16, 2016 14:48*

So i should press and hold each end stop until it possibly shuts down the system? Ill try that.



Then ill do a fresh install of Kamikaze and re wire my system. and see if that helps.

 


---
**Elias Bakken** *October 16, 2016 16:08*

You only have to press it briefly and then see if the board shuts down. 


---
**Edwardo Garabito** *October 16, 2016 17:22*

Ok ill try that. And if it doesn't? What I should try next? 


---
**Edwardo Garabito** *October 16, 2016 21:53*

So I went a head and tried the end stop hold, and nothing happened. trying each one individually had no response too the machine. 



Even hitting all 3 manually at the same time did not shut the machine off. Also the BBB stays powered on, in the video it was hard too see but the machine does stayed powered the full time. From Disconnect too re connection everything has power.



I only get removed from the server only when i tell the machine too home its self. 



Im going to try a fresh install now of kamikaze

. 


---
**Elias Bakken** *October 20, 2016 10:36*

How did it go with the fresh install?


---
*Imported from [Google+](https://plus.google.com/115118943136595777855/posts/3Jhseckos3Z) &mdash; content and formatting may not be reliable*
