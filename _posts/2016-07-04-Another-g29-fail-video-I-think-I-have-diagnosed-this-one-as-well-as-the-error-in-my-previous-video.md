---
layout: post
title: "Another g29 fail video. I think I have diagnosed this one, as well as the error in my previous video"
date: July 04, 2016 00:15
category: "Discussion"
author: Philip Acierto
---
Another g29 fail video.



I think I have diagnosed this one, as well as the error in my previous video. The speed between probe points is faster than my printer can go. It became more apparent and repeatable as I lowered the current on my steppers. The homing speed you can see is quite slow and safe as it heads towards the end stops, but the speed between probe points is violent and over running the steppers, causing the weird movement seen before I had to quickly unplug the power to avoid the bed crashing into my extruder.



The speed of this movement between points is even faster then the movement speed I have set for regular movement. So wherever the movement speed is set for those probe points is not controlled by a user setting anywhere I can see.



This is a problem.


**Video content missing for image https://lh3.googleusercontent.com/-yrMuQ6W0LFA/V3mpvljiFxI/AAAAAAAAERo/uboz4i_fWYEmw5hyUdwm9zCwTDidfQqfw/s0/fail%252Bg29.mp4.gif**
![images/d6a078efd77df78b7da7a15ef79ed061.gif](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/d6a078efd77df78b7da7a15ef79ed061.gif)



**Philip Acierto**

---
---
**Philip Acierto** *July 04, 2016 00:20*

My current configuration:



[System]

machine_type = DinCore_XY

loglevel = 20



[Geometry]

axis_config = 2



travel_x = 0.48

travel_y = 0.46



[Steppers]

microstepping_x = 6

microstepping_y = 6

microstepping_z = 8

microstepping_e = 8

current_x = 0.18

current_y = 0.18

current_z = 0.6

current_e = 0.08

steps_pr_mm_x = 8.0

steps_pr_mm_y = 8.0

steps_pr_mm_z = 20.0

steps_pr_mm_e = 28.19



direction_e =  -1



[Cold-ends]

connect-therm-e-fan-0 = True



[Heaters]

pid_p_e = 0.142762682705

pid_i_e = 0.002279643636

pid_d_e = 2.23512825109

pid_p_hbp = 1.58068224052

pid_i_hbp = 0.0329308800107

pid_d_hbp = 18.9681868862



[Endstops]

invert_x1 = False

invert_x2 = False

invert_y1 = False

invert_y2 = False

invert_z1 = False

invert_z2 = False

end_stop_x1_stops = x_cw, y_cw

end_stop_x2_stops =

end_stop_y1_stops = x_cw, y_ccw

end_stop_y2_stops =

end_stop_z1_stops =

end_stop_z2_stops = z_cw

soft_end_stop_min_x = -0.46

soft_end_stop_min_y = -0.46

soft_end_stop_min_z = -0.1

soft_end_stop_max_x = 0.46

soft_end_stop_max_y = 0.33

soft_end_stop_max_z = 0.3



[Planner]

max_speed_x = 2.0

max_speed_y = 2.0

max_speed_z = 0.09

max_speed_e = 1.5

acceleration_x = 0.8

acceleration_y = 0.8

acceleration_z = 0.2

acceleration_e = 2.0



[Homing]

home_speed_x = 0.06

home_speed_y = 0.06

home_speed_z = 0.016

home_backoff_speed_x = 0.04

home_backoff_speed_y = 0.04

home_backoff_speed_z = 0.008

home_backoff_offset_x = 0.01

home_backoff_offset_y = 0.01

home_backoff_offset_z = 0.02

home_x = .08

#home_y = .065



[Probe]

#length = 0.01

speed = 0.05

accel = 0.1

offset_x = -0.01

offset_y = -0.80

offset_z = -0.005



[Macros]

g29 = 

	M561                	                ; Reset the bed level matrix

	M558 P3                                  ; Set probe type to Servo with switch

	M557 P0 X88 Y0		        ; Set probe point 0 (Bottom Left)

	M557 P1 X88 Y270	                ; Set probe point 1 (Top Left)

	M557 P2 X380 Y120        	; Set probe point 2 (Middle Right)

        G92 Z0                                    ; Reset Z height to 0

        G0 Z25                                    ; Move Z up to allow space for probe

	G28 X0 Y0          		        ; Home X Y

	G0 X88 Y0			        ; Move the X Y to safe place to Home Z

	G28 Z0              	                ; Home Z

	G30 P0 S                  	        ; Probe point 0

	G30 P1 S                                 ; Probe point 1

	G30 P2 S                                 ; Probe point 2

        G0 Z25                                    ; Move Z up to allow space

	G28 X0 Y0                               ; Home X Y






---
*Imported from [Google+](https://plus.google.com/111974602640488998735/posts/5RhBLrdYTFg) &mdash; content and formatting may not be reliable*
