---
layout: post
title: "So what is the feasibility of implementing a VL6180X sensor to Z-probe the bed ?"
date: May 10, 2017 13:10
category: "Discussion"
author: Jonathan Cohen
---
So what is the feasibility of implementing a VL6180X sensor to Z-probe the bed ? How would that compare to IR sensors vs inductive sensors vs piezo sensors ?  Accuracy ? Reproducible  ?   VL6180X uses I2C at a fixed address.  





**Jonathan Cohen**

---
---
**Jon Charnas** *May 10, 2017 13:28*

While feasible, I'm not sure i2c is low-latency enough to be able to stop the steppers in time to very accurately detect the bed height... I use Force Sensing Resistors (not piezo)and I had to turn my probe speed down to 200mm/min to get an accurate measurement with those hooked up on the endstop (so directly on the IO for the PRU with a lot less latency than the i2c bus)


---
**Theodor Andersen** *May 10, 2017 17:28*

It should be possible...

but you'll be forced to go really really slow(0,1mm/s, might not even be slow enough) to account for the latency. but other than that you just need to tell redeem to use that i2c address, as a probe.


---
**Jonathan Cohen** *May 10, 2017 17:55*

Okay, so more so an  academic exercise but not practical in the least

 (Probably nice for another project then !).  I saw a nice adaption of piezo disks inserted just above the extruder but it required some slight modifications <s>but</s> that probably has some issues as well...


---
**Jon Charnas** *May 10, 2017 17:57*

Hmm, the few examples phew I saw were force sending resistors instead of piezo disks... They work well, wether on the effector or the bed. Use a JohnSL board to hook it up as an endstop and you're good


---
**Elias Bakken** *May 10, 2017 17:58*

I think the best way would be to set a threshold through the I2C bus and then use that to trigger an end stop. The good thing about this is that it does not require any additional pins since I2C is already available and broken out. Should allow for fast probing as well. 

To implement this, it's best to make a plugin for Redeem. This interface is still pretty experimental, but it's ideal for cases such as this!




---
**Jonathan Cohen** *May 10, 2017 18:12*

The Piezo requires it's own board and experimental.  

[thingiverse.com - MK III Piezo Hotend z-probe for e3d v6 + Magnetic Delta Piezo Effector by DjDemonD](http://www.thingiverse.com/thing:2069480)


---
*Imported from [Google+](https://plus.google.com/112731031525284419110/posts/2ht2ALBFSbL) &mdash; content and formatting may not be reliable*
