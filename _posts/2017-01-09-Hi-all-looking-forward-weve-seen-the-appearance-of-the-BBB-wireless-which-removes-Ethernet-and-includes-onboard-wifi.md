---
layout: post
title: "Hi all, looking forward, we've seen the appearance of the BBB wireless, which removes Ethernet and includes onboard wifi"
date: January 09, 2017 20:45
category: "Discussion"
author: Jon Charnas
---
Hi all, looking forward, we've seen the appearance of the BBB wireless, which removes Ethernet and includes onboard wifi. While a great innovation, it does pose a challenge for kamikaze development. In order to include support for the wireless edition (and potential future derivatives of the BBB), would you prefer having:





**Jon Charnas**

---
---
**Step Cia** *January 10, 2017 01:05*

I saw the word complex and vote the other one right away :)


---
**Jon Charnas** *January 10, 2017 06:28*

It's more complex for the development on the flasher side of things, and may end up with a larger image size if we have to put support for both in one.



The difference comes in at the kernel and kernel overlay levels as far as I can tell, but it's still in preliminary testing.


---
**Sabin Iacob** *January 14, 2017 22:51*

board specific, there are many changes needed and I'm not so sure they can be fit in a single image without lots of duct tape; source: I got it working: [github.com - BBB Wireless not supported · Issue #20 · goeland86/Kamikaze2](https://github.com/goeland86/Kamikaze2/issues/20)


---
**Jon Charnas** *January 22, 2017 12:25*

Thanks to everyone who voted. I think the preference between a board specific and unified edition of Kamikaze 2.1+ is quite a narrow split.



Here's what I know so far timeline-wise: 2.1.0 will <b>not</b> have a BBBW edition or support included. There is an ongoing effort with a beta version available (thanks to **+Sabin Iacob**, gearheadred and manbehindthemadness on slack) for those who need it so far if you follow the github issue: [github.com - BBB Wireless not supported · Issue #20 · goeland86/Kamikaze2](https://github.com/goeland86/Kamikaze2/issues/20)






---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/DkT4tqqzGgH) &mdash; content and formatting may not be reliable*
