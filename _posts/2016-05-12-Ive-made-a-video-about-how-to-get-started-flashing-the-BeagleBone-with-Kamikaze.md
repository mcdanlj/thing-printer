---
layout: post
title: "I've made a video about how to get started flashing the BeagleBone with Kamikaze"
date: May 12, 2016 15:38
category: "Discussion"
author: Elias Bakken
---
I've made a video about how to get started flashing the BeagleBone with Kamikaze. Is this a good way to provide documentation or is it better with written documentation instead? 

[http://www.thing-printer.com/flashing-beaglebone-black-etcher-io/](http://www.thing-printer.com/flashing-beaglebone-black-etcher-io/)





**Elias Bakken**

---
---
**Javier Murcia** *May 16, 2016 14:40*

Just a quick note, you could mention in the wiki page that although the flasher image requires a 4gb sd card, these images could be burned into a beaglebone black with 2GB eMMC (although you will end with arround 200mb free only!) 


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/SNaz2Tca9mj) &mdash; content and formatting may not be reliable*
