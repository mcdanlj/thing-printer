---
layout: post
title: "Hi, Here are files to print cover for Replicape with BBB: Any comments welcome"
date: September 25, 2016 12:31
category: "Discussion"
author: Daniel Kusz
---
Hi,

Here are files to print cover for Replicape with BBB:

[http://www.thingiverse.com/thing:1789792](http://www.thingiverse.com/thing:1789792)



Any comments welcome. Let me know how was printing and is everything OK with assembling or fitting to electronics.





**Daniel Kusz**

---
---
**John Dammeyer** *October 29, 2016 16:35*

Beagle and Replicape don't fit.  Don't try an build it.  The lid has a wide enough opening for the connectors and the circuit board.  The base of the box doesn't.

![images/2de53f0ed3d4343a938db06786b83a57.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/2de53f0ed3d4343a938db06786b83a57.jpeg)


---
**Daniel Kusz** *October 29, 2016 18:20*

Please reading the instruction on Thingiverse! You need hexagonal distance sleeves.


---
*Imported from [Google+](https://plus.google.com/103041373788524358316/posts/NmRR3ezSg92) &mdash; content and formatting may not be reliable*
