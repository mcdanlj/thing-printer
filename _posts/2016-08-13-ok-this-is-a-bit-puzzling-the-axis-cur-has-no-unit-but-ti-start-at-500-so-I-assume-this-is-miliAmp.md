---
layout: post
title: "ok this is a bit puzzling. the axis cur has no unit but ti start at 500 so I assume this is miliAmp"
date: August 13, 2016 22:09
category: "Discussion"
author: Step Cia
---
ok this is a bit puzzling. the axis cur has no unit but ti start at 500 so I assume this is miliAmp. at 500 the motor make too loud noise so I up it to about 750 and it sounds good but If I restart octoprint this value goes back to 500 again. how can I save it?



I tried updating the default.cfg but it did't change anything

![images/f7bcc1ce0cb8f3306d8916cfda6de0d3.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/f7bcc1ce0cb8f3306d8916cfda6de0d3.jpeg)



**Step Cia**

---
---
**Jon Charnas** *August 13, 2016 22:56*

Octoprint doesn't display the values from the config file. So if you save values of 750mA then restart octoprint, you'll see 500mA displayed again. The plugin's defaults is 500mA to display, but what you set in the configuration file is what is used, unless you hit "set" in octoprint. And even then it won't be saved for the next restart of redeem. You need to manually enter M500 in the terminal tab to save changes.



And please don't edit default.cfg - edit local.cfg to override values. Default.cfg will change with redeem upgrades, local.cfg won't.


---
**Step Cia** *August 14, 2016 02:54*

Thanks for the explanation I did saw thr warning not to change default.cfg but I'm not sure I saw any explanation. Will try local then


---
**Step Cia** *August 14, 2016 05:52*

ok I tried to download local.cfg so I can make edit but it gives me "internal error"... looks like I need to learn how to SSH and edit directly


---
**Andrew Dowling** *August 16, 2016 04:56*

Try making a file for the redeem plugin. Call it something like StepCiaPrinter1.cfg, and edit the things you need to in there. Be sure to select it as "favorite" under the redeem plugin in the settings menu. Next comment is literally what is in my printerbot play's .cfg file.


---
**Andrew Dowling** *August 16, 2016 04:56*

[System]



machine_type = Printrbot Play



[Watchdog]

enable_watchdog = True



[Geometry]



axis_config = 0

offset_x = 0.001

travel_x = 0.1

offset_y = 0.001

travel_y = 0.2

travel_z = 0.116

offset_z = 0.0



[Steppers]



microstepping_x = 6

microstepping_y = 6

microstepping_z = 6

microstepping_e = 6



steps_pr_mm_x = 5.0

steps_pr_mm_y = 7.14

steps_pr_mm_z = 125.98

steps_pr_mm_e = 30.625



direction_x =  1

direction_y =  1

direction_z =  -1



current_z = 0.7

current_e = 0.8

current_y = 0.8

current_x = 0.8



slow_decay_x = 1

slow_decay_y = 1

slow_decay_z = 1

slow_decay_e = 1



[Heaters]



# E3D v6 Hot end

temp_chart_E = SEMITEC-104GT-2

pid_p_E = 0.0313458514797

pid_i_E = 50.1149661064

pid_d_E = 3.61579842038

ok_range_E = 5.0

max_temp_E = 298.0



temp_chart_HBP = B57560G104F

pid_p_HBP = 0.028372370413

pid_i_HBP = 146.180718708

pid_d_HBP = 10.5469494017

ok_range_HBP = 7.0

max_temp_HPB = 140



[Cold-ends]



connect-therm-E-fan-0 = True

therm-e-fan-0-target_temp = 150



[Endstops]



has_x = True

has_y = True

has_z = True



end_stop_X2_stops = x_cw

end_stop_Y1_stops = y_cw

end_stop_Z2_stops = z_ccw



invert_X2 = False

invert_Y1 = False

invert_Z2 = False



invert_X1 = True

invert_Y2 = False

invert_Z1 = True



[Homing]



home_speed_x = 0.7

home_speed_y = 0.7

home_speed_z = 0.008



home_backoff_speed_x = 0.01

home_backoff_speed_y = 0.01

home_backoff_speed_z = 0.004



home_backoff_offset_x = 0.006   

home_backoff_offset_y = 0.006   

home_backoff_offset_z = 0.003   



[Probe]

length = 0.01



[Servos]



servo_0_enable = False



[Planner]



move_cache_size = 1024

min_buffered_move_time = 100

max_buffered_move_time = 1000



max_speed_z = 0.008

max_speed_x = 1.1

max_speed_y = 1.2

max_speed_e = 1.2



acceleration_x = 0.6

acceleration_y = 1

acceleration_z = 0.030

acceleration_e = 1.5



max_jerk_x = 0.04

max_jerk_y = 0.06

max_jerk_z = 0.006

max_jerk_e = 0.03



[Homing]



home_x = 0

home_y = 0

home_z = 0



[Macros]

G29 =

    M561                ; Reset the bed level matrix

    M574 Z2 z_ccw

    

    M558 P1

    

    M557 P0 X40 Y80    ; Set probe point

    M557 P1 X60 Y80    ; Set probe point

    M557 P2 X60 Y120    ; Set probe point

    M557 P3 X40 Y120    ; Set probe point

    

    M557 P4 X1 Y80    ; Set probe point

    M557 P5 X20 Y80    ; Set probe point

    M557 P6 X20 Y120    ; Set probe point

    M557 P7 X1 Y120    ; Set probe point

    

    M557 P8 X80 Y80    ; Set probe point

    M557 P9 X99 Y80   ; Set probe point

    M557 P10 X99 Y120    ; Set probe point

    M557 P11 X80 Y120    ; Set probe point



    M557 P12 X1  Y1     ; Set probe point

    M557 P13 X20  Y1     ; Set probe point

    M557 P14 X20  Y40     ; Set probe point

    M557 P15 X1  Y40     ; Set probe point

    

    M557 P16 X40  Y1     ; Set probe point

    M557 P17 X60  Y1     ; Set probe point

    M557 P18 X60  Y40     ; Set probe point

    M557 P19 X40  Y40     ; Set probe point

    

    M557 P20 X99  Y1     ; Set probe point

    M557 P21 X80  Y1     ; Set probe point

    M557 P22 X80  Y40     ; Set probe point

    M557 P23 X99  Y40     ; Set probe point



    M557 P24 X1 Y199    ; Set probe point

    M557 P25 X20 Y199    ; Set probe point

    M557 P26 X20 Y160    ; Set probe point

    M557 P27 X1 Y160    ; Set probe point

    

    M557 P28 X40 Y199    ; Set probe point

    M557 P29 X60 Y199    ; Set probe point

    M557 P30 X60 Y160    ; Set probe point

    M557 P31 X40 Y160    ; Set probe point



    M557 P32 X99 Y199   ; Set probe point

    M557 P33 X80 Y199   ; Set probe point

    M557 P34 X80 Y160   ; Set probe point

    M557 P35 X99 Y160   ; Set probe point



    M500

    

    

    

    G0 Z8    



    G28 X0 Y0           ; Home X Y

    G92 Y0 X0           ; Reset zero pos

    

    G0 X50 Y100 F8000       ; Move to bed center

    G28 Z0              ; Home Z

    G0 Z5               ; Move Z up to allow space for probe

    G92 Z0              ; Zero the Z axis

    

    G1 X40 Y80 F8000

    G30 P0 F250 S            ; Probe point 0

    G0 Z0

    

    G1 X60 Y80 F8000

    G30 P1 F250 S            ; Probe point 1

    G0 Z0

    

    G1 X60 Y120 F8000

    G30 P2 F250 S            ; Probe point 2

    G0 Z0

    

    G1 X40 Y120 F8000

    G30 P3 F250 S            ; Probe point 3

    G0 Z0

    

    G1 X1 Y80 F8000

    G30 P4 F250 S            ; Probe point 4

    G0 Z0

    

    G1 X20 Y80 F8000

    G30 P5 F250 S            ; Probe point 5

    G0 Z0

    

    G1 X20 Y120 F8000

    G30 P6 F250 S            ; Probe point 6

    G0 Z0

    

    G1 X1 Y120 F8000

    G30 P7 F250 S            ; Probe point 7

    G0 Z0

    

    G1 X80 Y80 F8000

    G30 P8 F250 S            ; Probe point 8

    G0 Z0

    

    G1 X99 Y80 F8000

    G30 P9 F250 S            ; Probe point 9

    G0 Z0



    G1 X99 Y120 F8000

    G30 P10 F250 S            ; Probe point 10

    G0 Z0

    

    G1 X80 Y120 F8000

    G30 P11 F250 S            ; Probe point 11

    G0 Z0

    

    G1 X1  Y1 F8000

    G30 P12 F250 S            ; Probe point 12

    G0 Z0

    

    G1 X20  Y1 F8000

    G30 P13 F250 S            ; Probe point 13

    G0 Z0

    

    G1 X20  Y40 F8000

    G30 P14 F250 S            ; Probe point 14

    G0 Z0

    

    G1 X1  Y40 F8000

    G30 P15 F250 S            ; Probe point 15

    G0 Z0

    

    G1 X40  Y1 F8000

    G30 P16 F250 S            ; Probe point 16

    G0 Z0

    

    G1 X60  Y1 F8000

    G30 P17 F250 S            ; Probe point 17

    G0 Z0

    

    G1 X60  Y40 F8000

    G30 P18 F250 S            ; Probe point 18

    G0 Z0

    

    G1 X40  Y40 F8000

    G30 P19 F250 S            ; Probe point 19

    G0 Z0

    

    G1 X99  Y1 F8000

    G30 P20 F250 S            ; Probe point 20

    G0 Z0



    G1 X80  Y1 F8000

    G30 P21 F250 S            ; Probe point 21

    G0 Z0

    

    G1 X80  Y40 F8000

    G30 P22 F250 S            ; Probe point 22

    G0 Z0

    

    G1 X99  Y40 F8000

    G30 P23 F250 S            ; Probe point 23

    G0 Z0

    

    G1 X1 Y199 F8000

    G30 P24 F250 S            ; Probe point 24

    G0 Z0

    

    G1 X20 Y199 F8000

    G30 P25 F250 S            ; Probe point 25

    G0 Z0

    

    G1 X20 Y160 F8000

    G30 P26 F250 S            ; Probe point 26

    G0 Z0

    

    G1 X1 Y160 F8000

    G30 P27 F250 S            ; Probe point 27

    G0 Z0

    

    G1 X40 Y199 F8000

    G30 P28 F250 S            ; Probe point 28

    G0 Z0

    

    G1 X60 Y199 F8000

    G30 P29 F250 S            ; Probe point 29

    G0 Z0

    

    G1 X60 Y160 F8000

    G30 P30 F250 S            ; Probe point 30

    G0 Z0

    

    G1 X40 Y160 F8000

    G30 P31 F250 S            ; Probe point 31

    G0 Z0

    

    G1 X99 Y199 F8000

    G30 P32 F250 S            ; Probe point 32

    G0 Z0

    

    G1 X80 Y199 F8000

    G30 P33 F250 S            ; Probe point 33

    G0 Z0

    

    G1 X80 Y160 F8000

    G30 P34 F250 S            ; Probe point 34

    G0 Z0

    

    G1 X99 Y160 F8000

    G30 P35 F250 S            ; Probe point 35

    G0 Z0

    

    

    M561 U

    M561 S

    M500

    





    M574 Z2              ; Disable Probe


---
**Step Cia** *August 16, 2016 05:19*

Thanks I noticed you set your current higher than 0.5 did you not get the stepper fault warning like in my screenshot?


---
**Andrew Dowling** *August 16, 2016 06:04*

In stepper mode 6 it runs cooler. 7 and 8 are voltage controlled not current controlled, which makes them quieter, but also they have no torque and it is more work (heat) for the stepper motor driver. Also depends on the stepper motors. The openbuilds branded ones run better than a lot of other things out there. I added heat sinks to the stepper circuits on the board. Lets be honest, even in stepper mode 6,  the replicape is quieter than just about any other board out there.


---
**Andrew Dowling** *August 16, 2016 06:06*

Oh, and I am running at 24 volts. Always use the higest voltage you can with stepper motors. Inductance and higher frequencies and all that stuff..


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/XG3jAgrnrqb) &mdash; content and formatting may not be reliable*
