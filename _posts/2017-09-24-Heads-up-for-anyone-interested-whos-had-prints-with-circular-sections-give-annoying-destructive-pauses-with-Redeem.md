---
layout: post
title: "Heads up for anyone interested who's had prints with circular sections give annoying/destructive pauses with Redeem!"
date: September 24, 2017 21:00
category: "Discussion"
author: Jon Charnas
---
Heads up for anyone interested who's had prints with circular sections give annoying/destructive pauses with Redeem!



It's been identified as a likely suspect that the problem is a delay between OctoPrint on Umikaze receiving an "ok" from Redeem and it sending the next G-code command. (100ms between lines sent)



There is, luckily, a bit of a workaround. Redeem devs have helpfully implemented G2 and G3. G2 and G3 are arc movement commands, i.e. move along a partially circular curve instead of a straight line. This means, that if you're printing circles, it becomes a single G2 or G3 line instead of several dozens of G1 lines.



To generate G2 and G3 commands, if your slicer can't do it (and we're not yet aware of which slicer incorporates this natively yet, please share if you do know of one!), there's a handy program on Github that should do the trick for you.



Granted the problem is that you need to first switch Redeem to the develop branch instead of using the master or staging branches. But. Instructions to do so are here:

[http://wiki.thing-printer.com/index.php?title=Umikaze#To_switch_to_the_develop_branch](http://wiki.thing-printer.com/index.php?title=Umikaze#To_switch_to_the_develop_branch)



As always, catch us here or on the Replicape Slack if you have questions or troubles!





**Jon Charnas**

---
---
**Jon Charnas** *September 24, 2017 21:04*

As usual though, on the develop branch the warning "there be dragons here" applies. We're happy to help fix things, but if something breaks, know that it'll help us if you tell us something broke. Post issues in tickets on the (new) Redeem Github [github.com - redeem](https://github.com/intelligent-agent/redeem/issues)


---
**Eric Smith** *September 25, 2017 08:41*

I was just upgrading from RAMPS to Replicape, specifically because Octoprint cannot keep the print buffer full on detailed curves through the serial connection. Will this be a problem going forward into the future? I was assuming the connection from Octoprint to Redeem was not just another serial connection. If it is, the only answer seems to be using Arc Moves. Printing from SD cards on RAMPS can easily keep the print buffer full, but Octoprint can't save gcode to the RAMPS Sd card at an acceptable speed. Does Octoprint+Redeem have a similar ability to buffer or save the Gcode, so octoprint does not have to feed it gcode in real-time? And at a reasonable transfer speed from Octo to Redeem?



Sorry for the spam of questions. I saw your post and realized the Replicape I'm ordering might have the same problem I already have.


---
**Jon Charnas** *September 25, 2017 09:45*

We are looking to resolve the issue with regards to octoprint speeds by stimulating "SD card printing" in redeem (using the files uploaded in octoprint so no need to write to SD), and afaik there will also be a heavy refactoring of the octoprint communication later on in the 1.4 releases that should allow us to make it even faster.



It's ongoing work and the arc commands are currently just a bandaid until a more permanent and solid fix is in place.


---
**Eric Smith** *September 25, 2017 18:13*

Thanks for the info. I know Octoprint is the biggest culprit with how many cycles are used in pySerial. Hopefully the refactoring will lead to some real gains. Meanwhile,  I'll be looking forward to the simulated SD card printing.


---
**Tim Curtis** *September 27, 2017 15:25*

**+Jon Charnas**, I think the real fix is to have the slicer "filter" the extruder paths into true arcs before the program is even written. That how CAM systems have worked for years. Its called tool path filtering. You set the tolerance for how close you want to stay to the original path and the program gets shortened dramatically. The parts look much better and the machine (printer) runs so much smoother.-



 I guess the real trick is how do you get the slicer folks to buy into adding scripts or functions to filter a sliced path? Public requests maybe?


---
**Jon Charnas** *September 27, 2017 15:49*

Public Requests would certainly help. But the flip side of this is that most 8-bit printer controllers can't really handle proper arc calculations, especially with delta printers... The computations take a really long time with the limited processing capacity those boards have, so it may unfortunately be one of those "niche" features we may have to live without for the time being. The script does exactly what you say though, and there are instructions to add it as a post-processor to Simplify3D and Cura... It's not complicated, just a bit long to run.


---
**tizzledawg** *June 30, 2018 02:44*

So dropped a laser on my printer and using inkscape + plugin ([https://jtechphotonics.com/?page_id=2012](https://jtechphotonics.com/?page_id=2012)) and it spits out these G2, G3 and G4 commands.



I'm not a fan up updating software when stuff ain't broke so is the Dev version fairly stable?

[jtechphotonics.com - Inkscape Laser Plug-In &#x7c; J Tech Photonics, Inc.](https://jtechphotonics.com/?page_id=2012)


---
**Jon Charnas** *June 30, 2018 05:16*

**+tizzledawg** the code is stable, the difficulty is in packaging an image due to kernel images, though we're getting there. There's a beta image floating around that seems to work


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/egKMpY5M3Ze) &mdash; content and formatting may not be reliable*
