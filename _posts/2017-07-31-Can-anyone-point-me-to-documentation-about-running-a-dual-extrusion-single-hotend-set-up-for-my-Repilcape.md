---
layout: post
title: "Can anyone point me to documentation about running a dual extrusion single hotend set up for my Repilcape?"
date: July 31, 2017 12:00
category: "Discussion"
author: Tim Curtis
---
Can anyone point me to documentation about running a dual extrusion single hotend set up for my Repilcape? Like for a E3D CYCLOPS set up?



BTW, I've been printing with the Repilcape over the past weekend and I must say, It works very well.





**Tim Curtis**

---
---
**Jon Charnas** *July 31, 2017 12:14*

Hi Tim, there's no real documentation on how to do it really - Redeem assumes by default that the H stepper is for T1, so you can swap filament extrusions with T0 and T1, making sure you have a proper H config for the extruder stepper and you should be relatively good to go. At least that's how I did it on my attempt at single-nozzle/dual extrusion with too long bowden tubes...


---
**Tim Curtis** *July 31, 2017 12:35*

What do I type in to have a proper H config? I have the E and H steppes enabled, But how do I set up that Tool 0 and Tool 1 use the same hotend?


---
**Jon Charnas** *July 31, 2017 12:38*

That's all you need. The rest happens in the Slicer and Octoprint GUI. Redeem doesn't actually check for nozzle temperature before activating a stepper, even if it's an extruder. Flaw? Probably. In this case though, it simplifies matters for this use-case.


---
**Tim Curtis** *July 31, 2017 13:10*

Okay Jon, Thanks for the info. I'll mess around with my slicer to get it working.


---
*Imported from [Google+](https://plus.google.com/106115959585163180149/posts/K7bp4oxW6PC) &mdash; content and formatting may not be reliable*
