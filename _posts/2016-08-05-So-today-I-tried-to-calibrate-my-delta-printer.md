---
layout: post
title: "So today I tried to calibrate my delta printer"
date: August 05, 2016 16:45
category: "Discussion"
author: Mathias Giacomuzzi
---
So today I tried to calibrate my delta printer. I had some issues with the dimensions of my prints. So I printed a gauge holder to measure the bed. So first I tried to calibrate the x, y, an z end stop offset. For that I use M206 and M500. And I calibrate on these Points:



G0 X0.00 Y100.00 Z0

G0 X86.60 Y-50.00 Z0

G0 X-86.60 Y-50.00 Z0



After that I would like to calibrate the zero point



G0 X0 Y0 Z0 for that I taught I can change the z height here when changing the R value. So I really tried some different R settings but nothing changed in the Z position on the zero point. S does someone can tell me what I am doing wrong here?



Thanks



 





**Mathias Giacomuzzi**

---


---
*Imported from [Google+](https://plus.google.com/104869072343043179738/posts/68AqGwgPcJo) &mdash; content and formatting may not be reliable*
