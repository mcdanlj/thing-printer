---
layout: post
title: "Unable to get \"connect-therm-E-fan-0 = True\" to actually fire up Fan 0 when the extruder reaches 60c"
date: March 10, 2016 06:23
category: "Discussion"
author: Chris Romey
---
Unable to get "connect-therm-E-fan-0 = True" to actually fire up Fan 0 when the extruder reaches 60c.  Is there something else I'm missing in the config?





**Chris Romey**

---
---
**Chris Romey** *March 10, 2016 06:53*

Ah ha! I've figured it out.  I was confused at the [Cold-ends] portion of the config file.  Works like a charm now!


---
*Imported from [Google+](https://plus.google.com/113335787317667734340/posts/R8Tji2zdn1T) &mdash; content and formatting may not be reliable*
