---
layout: post
title: "Is there a way to get the replicape/redeem to work out the end-stop offset?"
date: May 15, 2016 14:09
category: "Discussion"
author: Pieter Koorts
---
Is there a way to get the replicape/redeem to work out the end-stop offset? I have to my best set the offsets for the towers (by screws on the  carriage) but it seems that I still have some angular movement. I was wondering if redeem can do this yet by counting steps before end-stops are hit and reporting in millimetres (or metres)?





**Pieter Koorts**

---
---
**Daryl Bond** *May 17, 2016 02:25*

The G33 routine may help you.



[https://bitbucket.org/intelligentagent/redeem/src/de9d0728f1c3c0a331c3b8134ea6e1921a3025cd/redeem/gcodes/G33.py?at=master&fileviewer=file-view-default](https://bitbucket.org/intelligentagent/redeem/src/de9d0728f1c3c0a331c3b8134ea6e1921a3025cd/redeem/gcodes/G33.py?at=master&fileviewer=file-view-default)


---
*Imported from [Google+](https://plus.google.com/100077479073911242630/posts/C4t1cc4mDVy) &mdash; content and formatting may not be reliable*
