---
layout: post
title: "Hi everyone, Today I connected the fan to the first connector on the Replicape"
date: June 01, 2016 20:02
category: "Discussion"
author: Daniel Kusz
---
Hi everyone,

Today I connected the fan to the first connector on the Replicape. After starting the fan from Octoprint every time it works only for 1-2 seconds or less (LED shows the same). So it is OK or it should work until I turn it off from the Octoprint? Second question: how to handle the second fan for second hotend? Can I run second fan from Octo? I plan two hotends with two fans 30x30. Maybe everything will work correctly when printing starts? For now, yet I can not see, because I'm waiting for housing and other mechanical parts and do not try to start normal printing - testing electronics only.





**Daniel Kusz**

---
---
**Elias Bakken** *June 02, 2016 10:20*

Most likely that fan is connected to a thermistor, so it starts automatically once the thermistor goes beyond 60. You can control which fans are started with an empty M106 fan by using "add-fan-to-m106" in the config or using the P index directly. 


---
**Daniel Kusz** *June 02, 2016 15:54*

**+Elias Bakken** 

connect-therm-E-fan-0 = True

connect-therm-H-fan-1 = True

add-fan-0-to-M106 = True

add-fan-1-to-M106 = True



Should it be like this?


---
**Elias Bakken** *June 02, 2016 16:11*

No, if a fan is connected to a thermistor, you cannot also start it with an M106. My recommendation is to only use the two first lines there. Then try to raise the temp to 70 degrees to see that the fan turns on as expected.


---
**Elias Bakken** *June 02, 2016 16:14*

But if you have a fan that you want to turn on on the first layer, use the add-fan... setting.


---
*Imported from [Google+](https://plus.google.com/103041373788524358316/posts/GaWxW5SRnYf) &mdash; content and formatting may not be reliable*
