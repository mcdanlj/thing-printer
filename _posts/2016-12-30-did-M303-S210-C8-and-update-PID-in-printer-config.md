---
layout: post
title: "did M303 S210 C8 and update PID in printer config"
date: December 30, 2016 08:31
category: "Discussion"
author: Step Cia
---
did M303 S210 C8 and update PID in printer config. this is what I get when I run my hotend at 210c. it appear to fluctuate +-4deg c this is **+Shai Schechter** mini hotend which uses ceramic heater. It heats up really really fast. but I'm used to large heater block and this rapid fluctuation is new to me. Is this normal?

![images/8beb9c36bd32d3bd276796a3953f836f.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/8beb9c36bd32d3bd276796a3953f836f.jpeg)



**Step Cia**

---
---
**Step Cia** *December 30, 2016 17:47*

Actually after further reading, it appear I need to decrease Ki and increase Kd = "less agitation from cumulative error, and more gliding into alignment with the target:..."


---
**Elias Bakken** *December 30, 2016 18:31*

It looks as though the C8 value is not being respected. That seems like a bug. I tried it in master just now, only 4 cycles...




---
**Step Cia** *December 30, 2016 18:52*

glad you caught that I didn't even realize it was only doing 4 bumps instead of 8...


---
**Step Cia** *December 30, 2016 21:26*

Is it possible that this setting cause the heater to bounce +- 4 deg c? I'm going to try to lower this number to say 0.5 and run the PID tune again.

![images/6eec0cef3fc05f338629c1d728e795ac.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/6eec0cef3fc05f338629c1d728e795ac.jpeg)


---
**Step Cia** *December 31, 2016 06:40*

nope. decreasing ok range does not affect anything... I tried different values combination changing the the PID numbers increasing and decreasing, it appear not to have any meaningful affect that is to reduce the rapid oscillation...



is there a way to reduce the power to the heater hot end? I think the heater might be delivering too much power?

![images/5b60b2cce8f7c64ee7c47807f2947a97.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/5b60b2cce8f7c64ee7c47807f2947a97.jpeg)


---
**Elias Bakken** *January 01, 2017 16:59*

There is no way to limit the Max power to the heater right now, but lowering the p value should definitely help you with the ringing.


---
**Step Cia** *January 01, 2017 19:04*

I was testing changing D value manually to extreme from 0 to 150 but it didn't change the graph... It should ease the oscillation but the graph appear unchanged... I'd it possible that my setting in printer config gets ignored just like the cycle number?


---
**Step Cia** *January 01, 2017 19:05*

Forgot to attached this graph

![images/16706a571d5c64c53b84a76dbef52183.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/16706a571d5c64c53b84a76dbef52183.jpeg)


---
**Daniel Kusz** *January 03, 2017 21:39*

**+Elias Bakken** I have the same situation - only 4 cycles. 


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/LJsbPhSczoB) &mdash; content and formatting may not be reliable*
