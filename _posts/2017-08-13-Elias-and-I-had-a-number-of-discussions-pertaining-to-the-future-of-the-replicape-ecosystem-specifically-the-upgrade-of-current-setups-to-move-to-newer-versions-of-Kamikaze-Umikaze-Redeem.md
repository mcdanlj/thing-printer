---
layout: post
title: "Elias and I had a number of discussions pertaining to the future of the replicape ecosystem, specifically the upgrade of current setups to move to newer versions of Kamikaze/Umikaze/Redeem"
date: August 13, 2017 21:03
category: "Discussion"
author: Jon Charnas
---
Elias and I had a number of discussions pertaining to the future of the replicape ecosystem, specifically the upgrade of current setups to move to newer versions of Kamikaze/Umikaze/Redeem.



One big change is in the works for Redeem thanks to Andrew Wiley, who re-wrote the PRU firmwares in C code, making them both more readable and more accurate. However this change means that any printer running a Kamikaze 2.1.0 or older image will not be able to install the redeem update. Additional software from TI is needed for the PRU firmwares to compile with this.



So here's the question for you: what version of Kamikaze/Redeem are you currently running? Note that for the 2.0.8 / master and 2.1.0 / master entries I mean you're running the redeem version that shipped with the Kamikaze image!



If you're running a 2.1.0 image with a recent (less than 2 months) develop checkout of redeem, please tick the Umikaze box. The updated branches really means code that is less than 2 months old.



We'll come up with a smooth transition instruction set to accompany the release notes for 2.1.1, including a guideline on how to adjust your speed settings if you are using the master branch. (See first comment as to why this matters.)





**Jon Charnas**

---
---
**Jon Charnas** *August 13, 2017 21:05*

The new PRU firmware in C from Andrew has better ramp-up and down for acceleration, meaning the whole acceleration sequence is more accurate in timing, and this in effect means that if you had (like me) a 35mm/s retract before, the new PRU firmware will be a good 50% faster in effect with the same settings. I'll not delve in the details here and I encourage you to ask @wiley on the Slack directly if you want to understand it better, but we'll try to put together some general guidelines as to which values to modify and by how much so that your printer behaves the same in the end, such that you don't need to worry about re-calibrating the printer after the upgrade.


---
**Janne Grunau** *August 13, 2017 21:19*

Kamikaze 2.1.0 with a recent develop branch checkout. "Beta testing" instructions on the wiki there good enough except that I ran out of space on emmc during the PRU toolchains installation. Resolved by switching to SD.


---
**Step Cia** *August 14, 2017 01:21*

Still rocking 2.0.8 

![images/8d5068724e960ea8aa342a4020969505.gif](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/8d5068724e960ea8aa342a4020969505.gif)


---
**Tim Curtis** *August 14, 2017 15:56*

I'm running 2.1.0 master. I would be interested in updating to the new version. Is it available now or soon? 


---
**Jon Charnas** *August 14, 2017 16:32*

There's a release-candidate for 2.1.1 **+Tim Curtis**, so far all the feedback's been positive, but I want to add a few more bells & whistles to it before calling it a proper release. They're marked as "pre-releases" here:

[github.com - Umikaze2](https://github.com/goeland86/Umikaze2/releases/)


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/Km1FyXsBBZo) &mdash; content and formatting may not be reliable*
