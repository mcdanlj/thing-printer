---
layout: post
title: "Connectors for Replicape. These are the ones I use: Contacts: 952-2159-ND Thermistors: 952-2228-ND Steppers: 609-2339-ND End-stops: 952-2227-ND These are the ones that should be used: Steppers: A99615-ND Thermistors:"
date: February 04, 2014 17:19
category: "Discussion"
author: Elias Bakken
---
Connectors for Replicape. These are the ones I use: 

Contacts: 952-2159-ND

Thermistors: 952-2228-ND

Steppers: 609-2339-ND

End-stops: 952-2227-ND



These are the ones that should be used: 

Steppers:  A99615-ND

Thermistors: A99614-ND

End-stops: A99613-ND

Contacts: A100453CT-ND 





**Elias Bakken**

---
---
**Elias Bakken** *February 04, 2014 17:19*

Digi-key part-nrs. 


---
**Koen Kooi** *October 01, 2014 12:01*

609-2339-ND is too tall, it hits the PCB. It it safe to dremel down that part of the PCB so that the male connector is at the edge instead of 2mm behind it?


---
**Elias Bakken** *October 01, 2014 12:55*

Oh no! Do you have a picture? There is only GND on both sides of the PCB is you are talking about the stepper connectors, so it should be OK to dremel.


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/H5kEXodvLNQ) &mdash; content and formatting may not be reliable*
