---
layout: post
title: "If someone wants to tell me what is going on I'd love that"
date: February 09, 2016 23:50
category: "Discussion"
author: Hitbox Alpha
---
If someone wants to tell me what is going on I'd love that. Motors are on separate drivers. Problem seems to follow the z driver. 


**Video content missing for image https://lh3.googleusercontent.com/-o2-OM_GUW7Q/Vrp7KDnAJgI/AAAAAAAAAD8/lOq1hNsGurI/s0/gplus430452166.mp4.gif**
![images/179d8d5f2e3d27afc6c2426e5b99e54a.gif](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/179d8d5f2e3d27afc6c2426e5b99e54a.gif)



**Hitbox Alpha**

---
---
**Elias Bakken** *February 10, 2016 00:30*

I'm betting it has to do with the end stop configuration. Probably a wrong inversion of the z end stop coupled with it only stopping one motor. Try something to the effect of 

M574 z1 x_neg,h_neg 

I'm assuming you have configured h as slave to z here.


---
**Hitbox Alpha** *February 10, 2016 00:34*

but this isn't a homing move it's a simple G01 not a G28. the motors are only be told to move in one direction at any time.


---
**Elias Bakken** *February 10, 2016 00:36*

The end stop will be active no matter the move, unless you explictly disable it with the M574 command. 


---
**Hitbox Alpha** *February 10, 2016 01:28*

So would I have to disable and reable the end stop when I want to use them?


---
**Elias Bakken** *February 10, 2016 13:43*

No, but it looks like something is stopping a motor in mid air, so that leads me to think that the end stop is not configured right. M119 will give you the status of the end stops. It can also be used to invert the end stops if they are non-inverted. 


---
**Hitbox Alpha** *February 10, 2016 22:30*

I'm more of file guy as apposed to just issuing gcode commands. the M574 command sets the high/low state of the switch as well as if it is min or max. i have set these in local.cfg, and added the h_neg to both the Z2 and Z1 endstops



M119 returned this "Recv: ok X1: False, X2: False, Y1: False, Y2: False, Z1: False, Z2: False" which is accurate for the position of the printer. being 100mm x 100mm and a unknown hight (since i can't get it to z home)   


---
**Hitbox Alpha** *February 11, 2016 00:43*

switching the slaved driver makes it so that H motor just buzzes and the Z motor work freely



I also get a "Alarm: Stepper H" repeating in my terminal window 


---
**Elias Bakken** *February 11, 2016 16:17*

The current might be too high then on H, try lowering it.


---
**Hitbox Alpha** *February 11, 2016 19:45*

I have both set to 1.2. But I've moved them down to 0.5 with the same result 


---
*Imported from [Google+](https://plus.google.com/110719820264542745151/posts/TmBD5y6mAK9) &mdash; content and formatting may not be reliable*
