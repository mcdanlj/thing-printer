---
layout: post
title: "Hey Thing-printer People! I wonder, does the Replicape have, or if will Revolve will have, free and exposed step/dir pins that Hangprinter can connect its four Mechaduinos to?"
date: June 22, 2018 12:47
category: "Discussion"
author: Torbjörn Ludvigsen
---
Hey Thing-printer People!

I wonder, does the Replicape have, or if will Revolve will have, free and exposed step/dir pins that Hangprinter can connect its four Mechaduinos to?



Also, is it fit for evolving a crazy tool head changing setup upon? How many (motor) pins do I have, and how accessible are they?





**Torbjörn Ludvigsen**

---
---
**Jeremiah Coley** *June 22, 2018 16:42*

I don't have an answer to you question, but wanted to say great design on the Hangprinter.


---
**Torbjörn Ludvigsen** *June 23, 2018 12:07*

**+Jeremiah Coley** Thanks!


---
**Tim Curtis** *June 27, 2018 10:47*

I'm pretty sure my replicape has pins for 3 more steppers giving you a total of 8 drives possible. There was a design for an expansion board called a "reach" with the extra stepper drivers on it but I've never seen one for sale anywhere.


---
*Imported from [Google+](https://plus.google.com/116979551833471281068/posts/X8XZRoFBEgK) &mdash; content and formatting may not be reliable*
