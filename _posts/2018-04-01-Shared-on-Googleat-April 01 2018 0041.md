---
layout: post
title: "Shared on April 01, 2018 00:41...\n"
date: April 01, 2018 00:41
category: "Discussion"
author: James Armstrong
---




![images/f018899061e68834680e08c2e69d418a.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/f018899061e68834680e08c2e69d418a.jpeg)
![images/3d169e3282be8740c849d4aba8d18ee9.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/3d169e3282be8740c849d4aba8d18ee9.jpeg)

**James Armstrong**

---
---
**Elias Bakken** *April 01, 2018 00:47*

Please don't show this to **+Ray Kholodovsky** 


---
**Ray Kholodovsky (Cohesion3D)** *April 01, 2018 01:37*

Yeah, I thought I noticed a divot at that spot... 


---
**Step Cia** *April 01, 2018 06:28*

![images/f8808f972027fbdbc5e61e1f2cfbe48d.gif](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/f8808f972027fbdbc5e61e1f2cfbe48d.gif)


---
**René Jurack** *April 01, 2018 09:30*

**+Elias Bakken** is onto something. I was beginning to wonder, how long it would take someone to find out the real reason why the DICE is build that sturdy 😆


---
**Jeremiah Coley** *June 17, 2018 11:28*

It slices it dices and now it opens bottles as well. 😀


---
*Imported from [Google+](https://plus.google.com/+JamesArmstrong3/posts/T6PW4g44Gd7) &mdash; content and formatting may not be reliable*
