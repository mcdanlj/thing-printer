---
layout: post
title: "ok no matter what the current I set, I keep on getting this error"
date: August 16, 2016 16:21
category: "Discussion"
author: Step Cia
---
ok no matter what the current I set, I keep on getting this error. Currently I only have one Nema 17 hooked up to stepper Z so I'm not sure why I'm even getting Stepper E fault...

![images/5d975915fc767601f66461f4c1c2a393.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/5d975915fc767601f66461f4c1c2a393.jpeg)



**Step Cia**

---
---
**Elias Bakken** *August 16, 2016 17:51*

Are you sure the stepper is wired correctly? If the stepper is wired wrong, that happens...


---
**Step Cia** *August 16, 2016 18:13*

at the moment I only have 1 Nema 17 connected to stepper Z but the fault says stepper E so I'm not sure if this is motor wiring issue...


---
**Elias Bakken** *August 16, 2016 20:43*

Do you not have anything connected on that port?


---
**Step Cia** *August 16, 2016 20:53*

no all other port are empty and not connected except stepper Z and I was able to move stepper Z and play around with acceleration and speed but everytime it moves, this warning always pop up


---
**Step Cia** *August 16, 2016 23:00*

well I made it go away by disabling all other stepper setting it to false except Z... move the z motor and no warning pop up. but I don't think this is my solution...


---
**Step Cia** *August 17, 2016 00:02*

I turned all steppers back to True and plug in another NEMA 17 to stepper E. it would not move... but when I plugged it to X and Y it moves. so all other steppers can move except E



it appear something physical not software issue


---
**Elias Bakken** *August 17, 2016 08:23*

Yes, this sounds like a physical issue. I'll send you a new one.


---
**Step Cia** *August 17, 2016 15:32*

Thx! much appreciated


---
**Elias Bakken** *August 19, 2016 20:03*

**+Step Cia**, are you OK to wait for the new batch of boards to finish manufacturing? You should be able to use the board as is until then, just use the H-driver instead of the E and disable the E. 


---
**Step Cia** *August 19, 2016 20:58*

that's fine thx


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/U9vZtzSpni2) &mdash; content and formatting may not be reliable*
