---
layout: post
title: "Replicape case prototypes in progress"
date: September 06, 2016 18:53
category: "Discussion"
author: Daniel Kusz
---
Replicape case prototypes in progress. 

![images/ec3ff95e0cb7d56b70a06d70a67a258c.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/ec3ff95e0cb7d56b70a06d70a67a258c.jpeg)



**Daniel Kusz**

---
---
**Dan Cook** *September 07, 2016 14:45*

Looking good!


---
**Daniel Kusz** *September 25, 2016 11:51*

Hi,

Here are files to print:

[thingiverse.com - Replicape + BBB cover by Bombel](http://www.thingiverse.com/thing:1789792)



Let me know how was printing and is everything OK with assembling or fitting to electronics.


---
*Imported from [Google+](https://plus.google.com/103041373788524358316/posts/HH8H27Nt5yk) &mdash; content and formatting may not be reliable*
