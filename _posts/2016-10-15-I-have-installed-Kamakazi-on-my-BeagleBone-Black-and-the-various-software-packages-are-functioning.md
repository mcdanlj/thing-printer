---
layout: post
title: "I have installed Kamakazi on my BeagleBone Black and the various software packages are functioning"
date: October 15, 2016 14:55
category: "Discussion"
author: Luke Ingerman
---
I have installed Kamakazi on my BeagleBone Black and the various software packages are functioning. Can I update OctoPrint to the latest version using their software update function or will this break things within Kamakazi/Redeem?





**Luke Ingerman**

---
---
**Elias Bakken** *October 15, 2016 15:03*

You can update to the latest, if you are on Kamikaze 2.0!


---
**Jon Charnas** *October 15, 2016 15:03*

Try the updater. If it fails nothing bad will happen, you still have the old version. Is it kamikaze 1 or 2?


---
**Luke Ingerman** *October 15, 2016 15:05*

I will have to check the version of Kamikaze I installed. I did it two weeks ago and have been traveling for work since then.


---
**Jon Charnas** *October 15, 2016 15:07*

just remember if its kamikaze 2 you need to check that the git checkout folder in the octoprint settings is /usr/src/OctoPrint/


---
*Imported from [Google+](https://plus.google.com/114654865873386162214/posts/gZdaZkko2tE) &mdash; content and formatting may not be reliable*
