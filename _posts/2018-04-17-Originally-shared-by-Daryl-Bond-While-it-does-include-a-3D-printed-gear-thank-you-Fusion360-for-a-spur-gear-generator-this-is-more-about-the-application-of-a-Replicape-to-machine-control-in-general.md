---
layout: post
title: "Originally shared by Daryl Bond While it does include a 3D printed gear (thank you Fusion360 for a spur gear generator!) this is more about the application of a Replicape to machine control in general"
date: April 17, 2018 10:52
category: "Discussion"
author: Daryl Bond
---
<b>Originally shared by Daryl Bond</b>



While it does include a 3D printed gear (thank you Fusion360 for a spur gear generator!) this is more about the application of a Replicape to machine control in general. 



My wifes grandfather passed his old Boxford lathe to me on his passing and it has been a fantastic, if somewhat old and worn, tool. It currently is having issues with current leakage (tingly fingers at 240V anyone?!) and so while the ancient motor is out to get looked at I thought I would address another issue. The gear-train for driving the leadscrew is pretty much shagged and I only have one of the change gears. It is also an imperial machine and I only do metric. The leadscrew itself is fine and so I thought I would look at driving it with a stepper motor. This allows for power feed on finishing passes and possibly, with a rotary encoder on the main spindle, threading operations in the future. So I implemented what is shown below. It is a NEMA17 with 5.18:1 gearbox driving a 20 tooth to 80 tooth (4:1) gear train, total reduction of approx. 21:1. Combined with the reduction of the leadscrew this gives around 1300 full steps/mm. At this early stage I am running the rev. A4A Replicape on a 12V power supply and it stalls out at around 150-200 mm/min. 



My next task is to test it when actually cutting. As my main motor is currently broken this could take a while. I suspect, however, that I will need a NEMA23 and higher driving voltage to make it perform at a more acceptable level.


**Video content missing for image https://lh3.googleusercontent.com/-ihm9dnJOs-g/WtXR3ouF2iI/AAAAAAAAApg/5ta2GUYWiJ4KS5GEO6HDoEp1h5d3xosSQCJoC/s0/20180417_202213.mp4**
![images/cd07c39a1666e668ad00386a2da8e39f.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/cd07c39a1666e668ad00386a2da8e39f.jpeg)



**Daryl Bond**

---
---
**Michael K Johnson** *April 17, 2018 11:48*

I would expect you to need a good encoder on the spindle and a dedicated PRU to make thread cutting work. For cutting metric threads, I expect an additional change gear would be better.


---
**Daryl Bond** *April 17, 2018 22:09*

**+Michael K Johnson** all I really need for threadcutting is to accurately know the spindle RPM and be able to drive the leadscrew fast enough to keep up. For that a single pulse per rev. is all that is required (see Mach3). I'm not sure what you mean by needing another change gear? All gearing ratios, within the limits of the drive, can be handled electronically.


---
**Michael K Johnson** *April 17, 2018 22:28*

I'm new to this, I didn't think pulse per rev was sufficient resolution. I was wrong, I guess!



Separate change gear would be the alternative to stepper motor, using the normal gear train with different gearing to cut metric threads. Just saying that there is a mechanical alternative to missed steps leading to tool crashes. :)


---
**Daryl Bond** *April 17, 2018 22:37*

**+Michael K Johnson** Oh, yes, I pulled off the mechanical drive train because it is pretty much broken at the forward/reverse selector arrangement. Having the current arrangement with correct programming is basically considered an electronic gearbox.



Obviously higher resolution than single pulse per rev would be great but isn't really necessary for constant speed operation.


---
*Imported from [Google+](https://plus.google.com/104347555452272558593/posts/G7idTHmMaWh) &mdash; content and formatting may not be reliable*
