---
layout: post
title: "Attention all beta-testers! Umikaze 2.1.2 RC1 is now officially available for testing!"
date: December 03, 2017 03:06
category: "Discussion"
author: Jon Charnas
---
Attention all beta-testers!



Umikaze 2.1.2 RC1 is now officially available for testing! Feature highlights:

- Forced root password change (securing IoT devices)

- "Print from SD" to eliminate pauses during prints

- Keep your Redeem, Toggle, Octoprint & NetworkManager settings!



Please attentively read the release notes at the download link, with new functionalities there are always a few bugs left... We know of some but this is too good for you guys to not have for the holidays. We'll fix the annoyances early next year. Happy Printing!



[https://github.com/intelligent-agent/Umikaze/releases/tag/v2.1.2rc1](https://github.com/intelligent-agent/Umikaze/releases/tag/v2.1.2rc1)





**Jon Charnas**

---
---
**Elias Bakken** *December 03, 2017 11:11*

I'll update Revolve, haha! 


---
**Jon Charnas** *December 04, 2017 07:00*

You may wanna wait on that - looks like the upgrade from 2.1.1 to 2.1.2rc is breaking SSH and file permissions somewhere... I'll be fixing it this week.


---
**tizzledawg** *January 13, 2018 09:07*

The "Print from SD through Octoprint" - i'm guessing that's new since 2.1.0?



I've got my printer hammering at 150mm/sec prints and it shits itself  when doing a heap of corners with a radius - say a gear with fillets between teeth.



Will 2.1.2 fix this or should I be increasing the move_cache_size in Redeem profile


---
**Jon Charnas** *January 13, 2018 09:19*

2.1.2 with print-from-sd should be fixing that without touching the move_cache_size. If you want to keep printing smooth without trouble before the 2.1.2 final is ready, increase the move_cache_size for now.


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/1V4gg9PbB7F) &mdash; content and formatting may not be reliable*
