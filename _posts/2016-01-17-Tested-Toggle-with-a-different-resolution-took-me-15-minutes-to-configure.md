---
layout: post
title: "Tested Toggle with a different resolution, took me 15 minutes to configure"
date: January 17, 2016 19:30
category: "Discussion"
author: Elias Bakken
---
Tested Toggle with a different resolution, took me 15 minutes to configure. If anyone has a resolution they want packaged in the deb/repo, just make a pull request. 

![images/1b511f48227c3d2a4bce8b8adc631b7f.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/1b511f48227c3d2a4bce8b8adc631b7f.jpeg)



**Elias Bakken**

---
---
**Matt LaRussa** *January 21, 2016 21:14*

Is there a flag to rotate the screen?  I have a screen that it is working on, just 90 degrees rotated.


---
**Elias Bakken** *January 21, 2016 21:15*

Look in /etc/toggle/default.cfg 


---
**Matt LaRussa** *January 21, 2016 21:15*

Thanks!


---
**Jon Charnas** *January 23, 2016 08:38*

Elias, how much work is it to setup a new configuration for toggle?


---
**Andy Tomascak** *February 05, 2016 20:37*

I'd also like to know how to setup a new configuration! :)


---
**Elias Bakken** *February 05, 2016 20:38*

Not much work to set up for a new resolution, but a bit trial and error:)


---
**Andy Tomascak** *February 05, 2016 20:43*

Using the style.css? I'm not sure what files to start with...


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/ddMTNeXYEfp) &mdash; content and formatting may not be reliable*
