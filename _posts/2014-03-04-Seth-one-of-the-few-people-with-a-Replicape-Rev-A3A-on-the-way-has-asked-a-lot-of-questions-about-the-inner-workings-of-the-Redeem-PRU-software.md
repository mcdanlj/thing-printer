---
layout: post
title: "Seth, one of the few people with a Replicape (Rev A3A) on the way has asked a lot of questions about the inner workings of the Redeem/PRU software"
date: March 04, 2014 19:14
category: "Discussion"
author: Elias Bakken
---
Seth, one of the few people with a Replicape (Rev A3A) on the way has asked a lot of questions about the inner workings of the Redeem/PRU software. Today I recorded some G1-motions. Here are the results. A Step happens on rising flank. 



![images/aa7a93341d4f17d0b099567b82913adb.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/aa7a93341d4f17d0b099567b82913adb.jpeg)
![images/0ded1ac85f48d4e82d8746837911c9dc.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/0ded1ac85f48d4e82d8746837911c9dc.jpeg)
![images/667c1ee5618b4ba35ebaa3dafeac0d7b.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/667c1ee5618b4ba35ebaa3dafeac0d7b.jpeg)

**Elias Bakken**

---
---
**James Armstrong** *March 04, 2014 20:47*

what speed? What are the two signal pins you are looking at (x / y step pins?) One thing we wanted to see is how the steps are generated on fast moves. The arduino stuff is doing double and quad stepping in the interrupt and does not produce smooth stepping. I don't have a image handy but it would be a bunch of quick back to back pulses then a gap and repeats.


---
**Elias Bakken** *March 04, 2014 21:23*

Yes, x and y steps.


---
**James Armstrong** *March 07, 2014 17:44*

Did you ever get around to sampling some higher speed steps? We are looking to compare to a know problem with the arduino / Atmel based products.


---
**Elias Bakken** *March 08, 2014 11:28*

No, I'm right in the middle of merging with the work of Methieu: [https://bitbucket.org/zittix/redeem/overview](https://bitbucket.org/zittix/redeem/overview)



He has a higher level in PRU implementation of the step procedure, so the duty cycles are not symmetrical like in these pictures. I'll post something once I have it working!


---
**James Armstrong** *March 08, 2014 14:42*

Ok. That is what we are looking for. Most firmware isn't symmetrical once it gets past a certain speed because they do 4 steps in a single interrupt because it is too fast for each step to be clocked on its own timer. 


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/MuUH9RvfJsG) &mdash; content and formatting may not be reliable*
