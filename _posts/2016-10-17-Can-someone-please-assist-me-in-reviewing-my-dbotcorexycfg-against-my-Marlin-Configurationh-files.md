---
layout: post
title: "Can someone please assist me in reviewing my dbotcorexy.cfg against my Marlin Configuration.h files?"
date: October 17, 2016 01:37
category: "Discussion"
author: Luke Ingerman
---
Can someone please assist me in reviewing my dbotcorexy.cfg against my Marlin Configuration.h files? I am building a D-Bot CoreXY printer and I was having issues with my Arduino/RAMPS setup in which my XY would randomly move diagonally. I felt this could be due to the CoreXY kinematics and cheap Arduino/RAMPS boards. I have purchased and configured/wired a BBB/Replicape setup but now I am having issues where the motors make noise, but do not move. My motors are 42BYGHW811P1 models.



Any assistance would be appreciated.







**Luke Ingerman**

---
---
**Luke Ingerman** *October 17, 2016 01:38*

And here is my dbotcorexy.cfg file.





[drive.google.com - dbotcorexy.cfg - Google Drive](https://drive.google.com/file/d/0B3dl-8-2xS0gN0FVbkotOG41Wnc/view?usp=sharing)


---
**Simon-Gabriel Gervais** *October 21, 2016 23:25*

i try replicape for the first time yesterday whit slight modification to your config file and its work your issue is probably hardware 


---
**Luke Ingerman** *October 22, 2016 04:21*

**+Simon-Gabriel Gervais** what version of the Replicape are you using and what version of Kamikaze?


---
**Luke Ingerman** *October 22, 2016 04:24*

**+Simon-Gabriel Gervais** can you verify the motor wiring as being Rev B3: 1, 2, 3, 4 = OA2, OA1, OB1, OB2 where pin 1 is the closest to the power terminals (furthest from the Ethernet port).


---
**Simon-Gabriel Gervais** *October 22, 2016 17:52*

im using the revb3 whit kamikaze 2.0.8 i use this motor [http://www.automationtechnologiesinc.com/products-page/3d-printer/nema17-stepper-motor-kl17h247-150-4a-for-3d-printer](http://www.automationtechnologiesinc.com/products-page/3d-printer/nema17-stepper-motor-kl17h247-150-4a-for-3d-printer)



wire order is :red green yellow blue from the power side.

hope its clear enough to help you



[automationtechnologiesinc.com - High-Torque Stepper Motor, Stepper Motor, Driver, Stepper Motor kit, DC Servo Motor, DC Servo Motor kit, Stepper Motor Power Supply, CNC Router, Spindle, and other Components. Stepper Motor &#x7c; Stepper Motor Driver &#x7c; CNC Router &#x7c; Laser Machine &#x7c; 3D Printers For Sale](http://www.automationtechnologiesinc.com/products-page/3d-printer/nema17-stepper-motor-kl17h247-150-4a-for-3d-printer)


---
*Imported from [Google+](https://plus.google.com/114654865873386162214/posts/jcQarVfueKp) &mdash; content and formatting may not be reliable*
