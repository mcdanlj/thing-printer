---
layout: post
title: "Hi guys, Sorry for the quiet, I promised a new image for Thanksgiving and..."
date: November 25, 2017 18:42
category: "Discussion"
author: Jon Charnas
---
Hi guys,



Sorry for the quiet, I promised a new image for Thanksgiving and... then work happened. Working in a startup is a lot of fun, though the workloads tend to be a bit less predictable than I'd like, so I unfortunately didn't have as much time as I wanted to build a new Umikaze with all the features we wanted.



BUT! I finally have a bit of room to breathe, and we've managed to get the develop branch to a stable state today (I'm printing from SD with it now), and as I promised a new image, well, I'm building what will basically be 2.1.1b. It won't have the in-place upgrade (i.e. you'll still have to backup/restore configs), but it will have a few new spiffy things.



First off, OctoPrint 1.3.5. You can normally upgrade it on your side already so not a big bonus. But I'm also going to be building with Redeem from the develop branch as it is now, so you can actually get the "Print from SD" functionality in your hands with this.



There are a few caveats and issues still, tracked on the Redeem github. But it's not a showstopper, and the problems are minor and affect file management, not printing.



Another new thing is going to be the mandatory password change. If you google anything about IoT, it's that it's crazy insecure with hardcoded passwords etc. Well, we're doing our part by forcing you to change the default password upon first login.



Finally this image will try to be slimmer, so you'll have more space for those big GCode files and timelapse videos for super long prints.



I expect to have the image ready sometime in the next 10 hours or so.



Cheers,

Jon





**Jon Charnas**

---
---
**Jon Charnas** *November 27, 2017 19:59*

Alright the image is ready, but I didn't manage to free up as much space as I'd have liked... We're at 2.3GB used space


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/iXZjNTCLGvJ) &mdash; content and formatting may not be reliable*
