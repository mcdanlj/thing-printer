---
layout: post
title: "Hey guys, looks like the new and shiny Kamikaze 2.0.7 is not working so great"
date: October 17, 2016 02:05
category: "Discussion"
author: Elias Bakken
---
Hey guys, looks like the new and shiny Kamikaze 2.0.7 is not working so great. I'll try to get a replacement out tomorrow, but for now stick with 1.1 until it is fixed! 





**Elias Bakken**

---
---
**Elias Bakken** *October 18, 2016 19:40*

Ok, V2.0.8 is now available. If you have 2.0.7 installed, please update! 


---
**Luke Ingerman** *October 20, 2016 00:25*

Is there a way of upgrading without a clean install/flash of the BBB?


---
**Elias Bakken** *October 20, 2016 00:26*

Unfortunately not since the image itself seems corrupted!


---
**Luke Ingerman** *October 21, 2016 04:01*

Has anyone updated to 2.0.8 yet? Any problems? I just burned the image to my microSD card and will flash my printer tomorrow after work. Hope all goes well and I get it up and running.


---
**Daniel Kusz** *October 21, 2016 13:34*

I want to try but after burning image there is something wrong with uSD card - no Kamikaze folder, wrong format of files. Have you successfully burned it? ﻿


---
**Luke Ingerman** *October 21, 2016 14:02*

Honestly, I burned it using dd via the terminal on my MacBook Pro. It doesn't mount the drive afterwards so I need to go downstairs and review the uSD on my Ubuntu box. Will do this tonight after work.


---
**Elias Bakken** *October 21, 2016 14:05*

There should be only one partition, **+Daniel Kusz**, called rootfs.

Not sure what has happened on the mac. 




---
**Daniel Kusz** *October 21, 2016 15:25*

After burning can't even look at the content of the card and the partition does not have any name. Kamikaze from 02/16/2016 burns correctly. I am using the win32 disk imager and etcher with no effect. Today I'll try another card. I format uSD card with Windows diskpart to NTFS.


---
**Luke Ingerman** *October 22, 2016 04:19*

I have been able to flash my BBB/Replicape and run through the Kamikaze wiki setup steps. I am having problems with my local.cfg settings not appearing in OctoPrint.



Example: My local.cfg shows current_x = 0.9 but OctoPrint interface shows 500 reflecting the default.cfg setting of current_x = 0.5.



My System commands are also erring out so I have to stop/start/restart OctoPrint via command line from another computer.


---
**Daniel Kusz** *October 22, 2016 06:22*

This is normal that you have always 0.5A in Octoprint. The same situation with other settings. Octoprint doesn't show changes in local.cfg﻿ or printer.cfg


---
**Elias Bakken** *October 22, 2016 12:28*

Yes, the control tab can not read values back:(


---
**Elias Bakken** *October 22, 2016 12:31*

**+Daniel Kusz** its ext4, which will not show you anything on a Windows computer.. If you get the right MD5 and verification complete in Etcher, you should be ok.


---
**Daniel Kusz** *October 22, 2016 12:34*

**+Elias Bakken** Thanks for the reply Elias! With previous versions that did not happen and I was confused.


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/E1xEQysBjTE) &mdash; content and formatting may not be reliable*
