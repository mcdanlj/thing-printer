---
layout: post
title: "Trying to do Delta calibrations and during movement the print exibits concave movement"
date: April 05, 2016 23:01
category: "Discussion"
author: Pieter Koorts
---
Trying to do Delta calibrations and during movement the print exibits concave movement. As the effector moves towards the pillars the z-probe gets further and further from the bed. So bad that on the outer each of the bed there is a difference of 20mm in probe height.



This is sort of a conversion from Repetier so all measurements are known but I have checked other things like rod length as well.



No matter how much I adjust the rod length value or the radius the concave movement does not seem to decrease at all. It just remains the same. I am doing this through the G-code terminal on Octoprint - M665 L0.198 R0.160 but no effect even though it acknowledges the command.



For example, my delta numbers (z height is about 185mm)



[Delta]

Hez = 0.060

L   = 0.198

r   = 0.160



# effector offset

Ae  = 0.036

Be  = 0.036

Ce  = 0.036



# carriage offset

A_radial = 0.033

B_radial = 0.033

C_radial = 0.033



Any tips on calibrating Delta printers? I am basing most of my config off the kossel_mini settings with adjustments.





**Pieter Koorts**

---
---
**Elias Bakken** *April 05, 2016 23:04*

Are you using the development branch? There has been a lot of work there, but I am experiencing similar behavior to what you are describing. Are you using G33/34 or G29?


---
**Pieter Koorts** *April 05, 2016 23:06*

Just following the Redeem wiki and doing manual X/Y movements in Octoprint at the moment. The movement is so far out I thought bed levelling won't help much. I am using the latest download of Kamikaze if that helps.


---
**Pieter Koorts** *April 06, 2016 19:46*

**+Elias Bakken** okay so getting somewhere but the math to me seems odd. My radius taken from Repetier is 160mm and this on Repetier travelled more or less (within 1mm) straight along the print bed.



I assumed that figure would work with Redeem but I thought nothing was changing when lowering the value. Apparently I did not go far enough. I had to set my radius all the way down to 100mm to get to 1 or 2mm deviation. The difference between 160mm and 100mm seems a bit much error even for human eyeballing the ruler no?



I was more wondering if the calculation that Redeem does for movement does not have a formula error somewhere?



At this moment though if I make my radius incredibly smaller than the print actually is it does seem to sort of do straight travelling.


---
**Elias Bakken** *April 06, 2016 21:18*

I think there is a problem with radial values. Try setting radial values to 0 and combining it in the effector offsets.


---
**Pieter Koorts** *April 06, 2016 22:59*

That looks to have solved it. Putting all the offsets on the effector and zero on the radial makes my Repetier measurements apply perfectly.


---
**Jon Charnas** *April 07, 2016 21:06*

Hmmm, wiki needs an update on the delta page then until this is fixed.


---
**Jon Charnas** *April 08, 2016 21:27*

Done. [http://wiki.thing-printer.com/index.php?title=Redeem#Delta](http://wiki.thing-printer.com/index.php?title=Redeem#Delta) -> see the note in the text.


---
*Imported from [Google+](https://plus.google.com/100077479073911242630/posts/C2dubTjDeMG) &mdash; content and formatting may not be reliable*
