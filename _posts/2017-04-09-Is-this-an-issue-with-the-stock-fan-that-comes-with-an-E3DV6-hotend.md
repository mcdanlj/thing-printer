---
layout: post
title: "Is this an issue with the stock fan that comes with an E3DV6 hotend"
date: April 09, 2017 17:28
category: "Discussion"
author: Jonathan Cohen
---
Is this an issue with the stock fan that comes with an E3DV6 hotend.  When I connect it, I only hear a hum and it does not spin up.  If I connect a few other fans to any of the fan connectors, they are perfectly fine.  As a test, if I connect the E3D V6 fan to a battery it is okay.  I migrated from a different setup where the mains supplied 12 volts for fans that were on all the time.  Any ideas why this occurs ?  



Also, is there a way to use a non Manga touchscreen and have the sizing correct (and the touchscreen calibrated) other than enabling a mouse ?  





**Jonathan Cohen**

---
---
**Jon Charnas** *April 09, 2017 18:30*

Ok, so, first check to do with the E3D fan is that you're sending it full PWM output. (M106 S255 typically, if it's not connected by default to a temperature sensor, which it might be. If so, it'll only turn on when the hotend reaches 60C) Another thing to check is the default_pwm value, that might also be an issue. 



As to non-Manga screens, there are several users on Slack that have managed it, and normally the options in toggle's .cfg file should let you specify rotation to work with a different screen as well.


---
**Jonathan Cohen** *April 09, 2017 21:08*

What is the specific call to set the default pwm for a fan output, e.g #1 ? I am using M106 for a another fan that works fine and I need the hot end fams to run all the time.


---
**Jon Charnas** *April 09, 2017 21:44*

it's an entry you'll want to override in your local.cfg file - check the redeem wiki entry about fans. Let me know if it's not there and I'll update it ASAP.


---
**Jonathan Cohen** *April 09, 2017 22:16*

There is only a entry about PWM and fans for M141.  I did not see a specific entry in the wiki about designating PWM setting for each fan output.  My local.cfg is empty as I have not customized it yet.  I am clear as to how to implement full power to the hot-end fans though.  


---
**Jon Charnas** *April 11, 2017 06:38*

Sorry for the delay - I haven't been able to even look at my printer the last couple of days because I was busy. Indeed the wiki doesn't mention the default_pwm value setting. I will try to find time tonight to fix that. In the meantime I suggest you look at default.cfg to know what the variable's name is and you can override it in your local.cfg.


---
**Jonathan Cohen** *April 11, 2017 11:03*

Okay. The only parameter that I could see in the default config is "default-fan-0-value = 0.0" under [fans].  If that's the case it would be 1.0 for fully on ?  Does the override occur for each individual section of the .cfg file,  such that local > mymachine > default.  Or, is there a different call.  


---
**Jon Charnas** *April 11, 2017 11:05*

Ah, **+Elias Bakken** can you confirm the value? From memory it should be 255 (as it's a PWM value that should be passed in).



Yes, the hierarchy as you describe it is correct, any value in local.cfg will override the same value from printer.cfg, which overrides the value from default.cfg. As a general rule, it's preferred to edit local.cfg, because a new redeem version will likely modify the printer.cfg values.


---
**Elias Bakken** *April 11, 2017 11:08*

I wish I could confirm Jon, but that pull request is from someone else. The gcode is 255 ( which is the de facto standard), but the class interface is 1.0, so I would guess the latter. Either way it should get capped.


---
**Dan Cook** *April 11, 2017 13:46*

Aye, "M106 S255 Px" (x being 0,1,2,or3)... also check the light (on the Replicape) corresponding to whichever fan you're using... if it's not lit after using M106 for that specific fan, that particular fan is probably set up for a temp. You can also set up fan commands in the "GCODE Macros" section of Octoprint... If I'm not mistaken, fan 2 (P1) is set up as the default fan output for your hotend...


---
**Jon Charnas** *April 11, 2017 13:53*

P0 in redeem is set by default to turn on with the hotend temp reaching 60C for sure. **+Elias Bakken** you mean the config file default value was something added in through someone else's PR?


---
**Jonathan Cohen** *April 11, 2017 14:05*

So, what is the primary setting that ensures  that a fan is set to max power/speed/frequency (due to the fact that the stock E3D fans do not handle PWM) ?  


---
**Dan Cook** *April 11, 2017 14:07*

I have two stock fans that are working perfectly fine... M106 S255 P0/1/2/3 should do the trick... unless it's a 24v fan?


---
**Jonathan Cohen** *April 11, 2017 14:18*

So, in my machine config I should have "add-fan-0-to-M106 = True" for the hotend fan and then have M106 S255 P0 in the local.cfg ?  When Cura slices, does it implement the correct fan, e.g. fan-1 which is cooling a print ?


---
**Dan Cook** *April 11, 2017 14:32*

As far as my setup goes, Cura is using fan 2 (P1) as the cooling fan, and it seems to be working correctly if you use part cooling... it kicks in after the first layer. If you're manually starting your fans, I'd use the GCODE Macros section in Octoprint... it can start the fan when you start the printer or start a print...



But... it's nice to have the fans automatically power on/off if you're not sticking around.



* Sorry about my confusion with the default fan 1/fan 2 in Redeem... 


---
**Jon Charnas** *April 11, 2017 14:33*

Cura will use M106 S<> and not specify a specific fan, which is what you

want for a print - so if you add the fan0 to M106 command, yes, the G-code

from cura will work with that :)


---
**Dan Cook** *April 11, 2017 14:39*

So that's why it just works from Cura :-) I have the other fans 0,2,3 set up within Redeem/Octoprint, so I thought I got lucky that Cura was flipping my cooling fan on after that first layer.


---
**Jon Charnas** *April 11, 2017 14:41*

Normally you can specify the add-fan-0-to-M106 = False and there's a

connect-therm-E-fan-1 =True that makes fan 1 stay on for the hotend


---
**Jon Charnas** *April 11, 2017 14:42*

What I mean is, if you specify those in your local.cfg, you can specify

which fan is activated by M106 and which by extruder temp - without having

to worry about which is which in Gcode.



On Tue, Apr 11, 2017 at 4:40 PM, Jon Charnas <*<b>***</b>@<b>**</b>> wrote:



> Normally you can specify the add-fan-0-to-M106 = False and there's a

> connect-therm-E-fan-1 =True that makes fan 1 stay on for the hotend

>


---
**Jonathan Cohen** *April 11, 2017 14:45*

Okay.  Ill give some of these suggestions a go and see what happens...


---
**Dan Cook** *April 12, 2017 12:36*

Did you get it working Jonathan?


---
**Jonathan Cohen** *April 12, 2017 14:00*

Got bogged down working up a mount for a fan to cool the stepper drivers.  Previously I had made a custom mount for a CRAMPS board but the geometry of the Replicape is completely different.  I see from posts that the Trinamic need cooling so I wanted to rig up a fan "temporarily" for that.  Later, Ill implement a better mounting system.  I'll know this evening if it works or not (West of the Atlantic)


---
**Dan Cook** *April 12, 2017 14:03*

Gotcha, I'm on the West side as well... half of my printer is temporary stuff that works well enough, that I move on to something else... good luck with it!


---
**Jonathan Cohen** *April 19, 2017 05:11*

Nope.  I could not get the E3D fan to turn on unless I hook it up directly to my power supply. Tried both Fan 0 and Fan 1 outputs and the light comes on for either but not the fan.  I know the fan is engaged since there is some resistance.  Must be a funny setting.  Dan, you said that you are using a E3D with no problem,  can you post the file so that I can compare to what I have been trying ?


---
**Jon Charnas** *April 19, 2017 05:56*

That's strange because I'm using the supplied e3d fan as well


---
**Jonathan Cohen** *April 19, 2017 12:59*

I don't understand it as well.  The stock fans are fine, worked on my other board. Even if I assign the extruder fan to M106 and then send a gcode, M106 S255 P0, the fan status light comes on but the fan does not spin up.  I tried this with two different fan connectors.  Are there any other calls that set the fan to run at maximum?  Jon, what is in your configuration files ?  Do you mess with the default.cfg ? (or is that static)   


---
**Dan Cook** *April 19, 2017 13:01*

Maybe a silly question, but have you tried flipping the +/- wires from the fan going to the Replicape? 


---
**Jonathan Cohen** *April 19, 2017 13:04*

LOL !   I have it (-) (+) with the 12V towards the RJ45 connector.  


---
**Jon Charnas** *April 19, 2017 13:33*

The default.cfg is static and is only modified when redeem is updated. If

you've made any changes there, I'd recommend you move them instead to

local.cfg.



I use the standard default.cfg and the kossel_mini.cfg linked to

printer.cfg. My local.cfg contains the following section for fans:



[Cold-ends]

connect-therm-e-fan-1 = False

connect-therm-e-fan-3 = True

add-fan-1-to-m106 = True

add-fan-3-to-m106 = False


---
*Imported from [Google+](https://plus.google.com/112731031525284419110/posts/e68oPWqPvN2) &mdash; content and formatting may not be reliable*
