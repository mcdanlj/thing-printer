---
layout: post
title: "I'm litteraly looking everyday on the mangascreen webpage"
date: March 20, 2017 14:20
category: "Discussion"
author: Mathieu Paquin
---
I'm litteraly looking everyday on the mangascreen webpage. I'm very curious to know what is the version2. Is it going to be bigger? smaller? higher density of pixel? IPS screen or TFT. Power consumption, etc. There is absolutly zero info about what it's going to be. End of march is coming soon. We're suppose to get details end of march. If only I could get some info to calm me down





**Mathieu Paquin**

---
---
**Mathieu Paquin** *March 20, 2017 14:30*

well, I have found here some information. Here is what I got currently, 1080p, and first iteration of the V2 was 5.9" . There is also a picture of the screen. I very excited, if it exactly like that, it would fit my project very very well :)


---
**Jon Charnas** *March 20, 2017 14:33*

That's what the plan is for now. Power consumption and other details will be given by Elias when he's finished building a few of the prototype boards himself. End of March is normally the launch of the crowd funding for the manga screen 2


---
**Elias Bakken** *March 20, 2017 15:38*

I was working on getting it to run on an odroid over the weekend, while waiting for the touch panel samples. Not quite there yet... The touch panels are really what is keeping the project from being launched. The screen has IPS yes. Synaptics touch chip, the works:)![images/e52bd914360a2ef5e030fd118aa9efa5.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/e52bd914360a2ef5e030fd118aa9efa5.jpeg)


---
**Mathieu Paquin** *March 20, 2017 15:47*

this looks promising :D




---
**Daniel Kusz** *March 20, 2017 21:11*

What about UI/toggle? Some new version with complete features for 3d printer? 


---
**Jon Charnas** *March 20, 2017 21:12*

**+Daniel Kusz** Toggle patches are still needed - I'd have gotten started with that if I'd had any breathing room in the past 4 months, but I am still fighting to find time for some basic Kamikaze patches that are even more critical...


---
*Imported from [Google+](https://plus.google.com/112278304600785903296/posts/8y2y1Ni6gH9) &mdash; content and formatting may not be reliable*
