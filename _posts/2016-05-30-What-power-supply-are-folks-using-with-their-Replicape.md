---
layout: post
title: "What power supply are folks using with their Replicape?"
date: May 30, 2016 17:26
category: "Discussion"
author: Marcos Scriven
---
What power supply are folks using with their Replicape? My PSU is a few years old now, a cheap 500W Apline ATX that came with my kit. It worked ok with a Melzi board but when printing in full swing on Replicape it's hissing and whining, and cuts out for a few minutes. I'm thus considering a 12v 30a LED SMPS (since I don't yet want upgrade everything else to 24v). I'm not sure if I'm having issues due to the drivers being less efficient in some way, or if the BBB is taking it over the edge. 





**Marcos Scriven**

---
---
**Andy Tomascak** *May 30, 2016 19:30*

Some older computer PSUs have their power spread across several rails, so even though it's a 500W PSU, you may only be getting half (for example) through the 12V rail you are using. Newer PSUs have moved away from this design due to the crazy power-hungry GPUs now available.



Personally, I'm using a cheap 12V 30A SMPS I got on Amazon, and it's worked fine so far. For ~$30 or whatever, it certainly worth trying.


---
**Marcos Scriven** *May 31, 2016 07:49*

Amazon (UK) is where I was thinking of getting one from. How did you wire yours up? Did you enclose it? Connect directly to mains or add an IEC plug? Multiple wires or just two thicker ones?


---
**Andy Tomascak** *May 31, 2016 16:46*

Mine is enclosed and I did wire up an IEC plug. For safety reasons alone this is a good idea! My IEC plug is wired with a switch, and I also have a second lit switch on the front (for main power, just like most computers). I can post a picture if that's helpful!


---
**Marcos Scriven** *May 31, 2016 21:06*

I think what was happening with mine is the load was just a little higher on the 12V rail (definitely only one on mine), tipping the 5v rail over the balance. Rather than faff with adding more wasteful and ugly resistors on the 5v, gone ahead and bought the SMPS. I see there's a design on Thingiverse for mounting an IED plug to one of these - scavenged the socket and switch from the PSU to reuse. 


---
**Jon Charnas** *June 01, 2016 13:01*

For what it's worth, my PSU is a 550W ATX as well, and I'm powering everything through the 12V rail. PSU's probably the quietest part of my printer. Second are the steppers, third the fans and loudest are the fan PWM interrupts...


---
**Marcos Scriven** *June 01, 2016 20:00*

So I've returned the £20 LED supply, even the terminals looks a bit corroded, and I've seen YouTube videos on how bad it is inside. I got instead a Corsair VS550 - which was £36. Quite a bit more, but can take plenty on its single 12v rail, and doesn't need any loads on the other rails either for power on or balancing. 

Nice and quiet too, save for a bit of resonant 'singing' along with the stepper drivers. Wonder if an iron core around the 12V connection might help reduce that. 


---
*Imported from [Google+](https://plus.google.com/117910047317867058880/posts/2eMTXwZKv6p) &mdash; content and formatting may not be reliable*
