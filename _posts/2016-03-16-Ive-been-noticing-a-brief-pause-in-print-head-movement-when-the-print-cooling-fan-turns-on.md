---
layout: post
title: "I've been noticing a brief pause in print head movement when the print cooling fan turns on"
date: March 16, 2016 20:34
category: "Discussion"
author: Chris Romey
---
I've been noticing a brief pause in print head movement when the print cooling fan turns on.  Anyone else experiencing this behavior? 





**Chris Romey**

---
---
**Elias Bakken** *March 16, 2016 21:09*

Ah, is that when it happens? I've noticed a small pause at some point, but never been able to pin point it. There is a very elaborate sync system designed for starting stuff even with lots of buffering, but M106 does not seem to use it...


---
**Elias Bakken** *March 16, 2016 21:10*

[https://bitbucket.org/intelligentagent/redeem/src/32e265e724151bae60637496fa4cc0f1662bb39e/redeem/gcodes/M106_M107.py?at=master&fileviewer=file-view-default](https://bitbucket.org/intelligentagent/redeem/src/32e265e724151bae60637496fa4cc0f1662bb39e/redeem/gcodes/M106_M107.py?at=master&fileviewer=file-view-default)




---
**Chris Romey** *March 16, 2016 23:15*

Yeah, that's when I've mostly noticed it.  First layer is normal with the fan off. Then, when the gcode tells redeem to turn on the fan, it pauses, the fan comes on, then it resumes printing.


---
**Jon Charnas** *March 17, 2016 08:29*

Hmm. I think I've also had pauses when M106 changes the fan speed - because I've had inexplicable pauses well after the fan's started, but changing fan speeds would explain that well.


---
**Ante Vukorepa** *March 17, 2016 22:02*

Unrelated, but a somewhat similar symptom. I've noticed small pauses (stutters) in Z movement when homing. They happen every 10cm or so and are not related to mechanics in any way. They're not perceivable by eye, but you can hear them quite well.


---
**Elias Bakken** *March 17, 2016 22:50*

That is a limitation on the shared memory, it can only go so far before needing rebuffering.


---
**Ante Vukorepa** *March 17, 2016 23:33*

Ah. Thought it was something like that. Haven't looked at the code much (beyond basics) so excuse me if this is an idiotic question - is the buffer circular? If yes, any reason it's not filled while it is being emptied (separate threads)?


---
**Elias Bakken** *March 17, 2016 23:49*

The buffer is circular, but no feedback is given from the PRU within one path segment. For longer segments they have to be stitched. It can be handled more elegantly by using shorter segments I guess, or expand the buffer beyond the current 40k.


---
**Ante Vukorepa** *March 18, 2016 00:40*

Ah.


---
**Jon Charnas** *March 18, 2016 09:11*

Elias, how much memory can we play with on that buffer? Is there a physical limitation at all? I understand that a bigger buffer means slower reaction time when you're interrupting a print, but would a "buffer flush" when you're cancelling a print work to restore responsiveness?


---
**Elias Bakken** *March 18, 2016 09:26*

The physical limitation is the 512MB, a bigger ring buffer will not increase responsiveness, it is checked each path segment.


---
**Ante Vukorepa** *March 21, 2016 02:30*

Hm. Just witnessed the head stutter visibly (leaving a blob) on a random segment of an arc. Wasn't a very complicated print, print speed was slow (30 mm/s). Doesn't seem like a buffer underflow would be likely. Any ideas? 



Also, i've sometimes noticed Octoprint UI start lagging a LOT during a print. Can't figure out when exactly (nor why) it happens, it seems totally random. But it only happens during a print.


---
**Jon Charnas** *March 21, 2016 06:40*

I have the same problem. I thought it was the buffer underflow though. It would make sense since the master branch uses a python path planner. I'm out of ideas as to what else it could be... ﻿



The python planner is a bit of a CPU hog so everything else stutters.


---
**Elias Bakken** *March 21, 2016 07:33*

Can you do a 'top' when this happens? I have seen octoprint consume 80-90% cpu at times, so it might be related. Perhaps a simple workaround would be to set thread priority or something?


---
**Jon Charnas** *March 21, 2016 07:57*

I'll try it tonight. I was discussing the problem with Akex on IRC last night, I'll make a video of what's happening on the printer and I'll try to get a shot of the top window at the same time, so we see what's going on. I know for a fact that my octoprint issues got a lot better after I changed my wifi dongle (apparently some of them need a lot more CPU resources than others...)


---
*Imported from [Google+](https://plus.google.com/113335787317667734340/posts/WVXry6C2yvv) &mdash; content and formatting may not be reliable*
