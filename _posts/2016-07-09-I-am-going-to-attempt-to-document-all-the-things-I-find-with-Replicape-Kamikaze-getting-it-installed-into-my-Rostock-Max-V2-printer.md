---
layout: post
title: "I am going to attempt to document all the things I find with Replicape/Kamikaze getting it installed into my Rostock Max V2 printer"
date: July 09, 2016 18:17
category: "Discussion"
author: Alexander Borsi
---
I am going to attempt to document all the things I find with Replicape/Kamikaze getting it installed into my Rostock Max V2 printer. This will be as I find them, and remember to write them down. They might not be in any order. But I think that documentation being written down is a start of something.



First thing is that the instructions on the wiki for Kamikaze install are not clear as they could be. For instance, it is mentioned in one spot that having all 4 lights solid blue means that it is time to power down the board and remove the SD card. But before that, it is mentioned that it will 'power down' but having all 4 lights on doesn't really seem like a 'powered down' state to me.



Secondly, the 'ping kamikaze.local' command only works if you have a mDNS/SD-DNS installed, and Win10 does not have it installed by default for all applications. You need to download and install one, and the major one is (ironically) Apple based. It is called "Bonjour Print Services for Windows" even more confusingly since it works for everything and not just printers... Once that is installed, then it works as expected. It usually returns an IPV6 address, but sometimes a IPV4 address, so PuTTY kind of freaks out sometimes when connecting. Oh, and having a WiFi and wired connection also makes everything freak out--so pick one or the other. Not both. Wired is more reliable, but wireless is far more convenient if you are pulling the board in and out of places.





**Alexander Borsi**

---
---
**Jon Charnas** *July 09, 2016 18:37*

**+Alexander Borsi** Please feel free to email them to me, and I can then add your edits to the wiki. Or request access to the wiki from Elias to edit it directly. My email is goeland86@ gmail


---
**JaeHyung Lee** *July 09, 2016 21:29*

Totally agree with Alexander's opinions about wiki pages. It seems that the guides of Kamikaze/Redeem's wiki pages are based on LINUX environment. So it's very hard to follow it with Windows ONLY users. Actually until  I had installed Linux system on my old notebook for Replicape, I couldn't  understand the guides of wiki well. 



I wish that the guides of OS(Windows or Linux) environments  are written explicitly in wiki pages. Thanks.


---
**Jon Charnas** *July 09, 2016 21:33*

**+JaeHyung Lee** The problem is that as far as I know, Elias (and myself) use Linux as our primary OS, so we couldn't easily document things from a Windows perspective... You're right about needing to mark better from which platform it'll work, but at the same time, it's also difficult for non-Windows users to know what will and won't work.


---
**Jon Charnas** *July 10, 2016 08:50*

**+Alexander Borsi** I've updated the wiki to address the second point. I haven't flashed Kamikaze in a while so I won't deal with point #1 until my development BBB gets here and I can correct it.


---
**Jon Charnas** *July 11, 2016 06:59*

If either of you have any further comments about what doesn't work for WIndows users, or something windows-specific to add to the documentation, please let me or Elias know, we'll be happy to add it to the documentation.


---
*Imported from [Google+](https://plus.google.com/102644518510093053332/posts/11K8ppM5B5R) &mdash; content and formatting may not be reliable*
