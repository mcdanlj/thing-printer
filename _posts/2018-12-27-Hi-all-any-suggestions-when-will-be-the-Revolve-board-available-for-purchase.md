---
layout: post
title: "Hi all, any suggestions when will be the Revolve board available for purchase ?"
date: December 27, 2018 22:10
category: "Discussion"
author: Kvapil
---
Hi all,



any suggestions when will be the Revolve board available for purchase ? 



Lukas





**Kvapil**

---
---
**Jon Charnas** *December 28, 2018 07:09*

Hi Lukas, last I heard was the Kickstarter would start end of January or beginning of February. I'm meeting with Elias next week, and I'll push him to put out a bit more info about it.


---
**Kvapil** *December 29, 2018 09:42*

**+Jon Charnas** Thank you and I'm looking forward to your response.


---
**Jon Charnas** *January 10, 2019 12:52*

Hi Lukas, just checking to see if you saw the Revolve-related post I made a few days ago? I know it's not a date when you'd like it, but as far as I understand it, Elias is moving as fast as he can to provide the specs for a final board for manufacturing. Can't promise a date until the factory comes back and says they've started production though.


---
*Imported from [Google+](https://plus.google.com/112069338342087354177/posts/ZKKKXyHs18a) &mdash; content and formatting may not be reliable*
