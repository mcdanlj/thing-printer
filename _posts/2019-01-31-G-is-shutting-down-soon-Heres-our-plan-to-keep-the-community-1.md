---
layout: post
title: "G+ is shutting down soon. Here's our plan to keep the community: 1"
date: January 31, 2019 08:51
category: "Discussion"
author: Jon Charnas
---
G+ is shutting down soon. Here's our plan to keep the community:



1. As a moderator I will download the whole community's posts & info into an archive. This archive will be publicly available, and linked to from the Thing-Printer wiki.

2. We'll setup a phpBB forum instance (or something similar) that will be linked from the Thing-Printer homepage.

3. When it's ready for user account creation, I will use the emails your G+ accounts were linked to (if I have access to those in the archive) to send a mass email notification about where that forum will be located, so you can easily sign up and come join us. I will also link the downloaded G+ archive in that email.

4. I will have a "Community" section in the forum, where I would encourage any feedback on how the layout of the forum matches your expectations, or what you'd like to change in the categories etc. that will be organized there.











**Jon Charnas**

---
---
**Michael K Johnson** *January 31, 2019 11:42*

**+Jon Charnas** I have created an archive with the Friends+Me Google+ Exporter and have written code to convert it into a static site, which I used to build an archive for the HercuLien and Eustathios Builders community. Feel free to browse [eclsnowman.gitlab.io - HercuLien and Eustathios community archive](https://eclsnowman.gitlab.io/eustathios_herculien_ingentis_gplus/) — and if that's useful it would be really easy for me to re-use the work to build a similar archive from the contents. Let me know if you'd like that. Note: The exporter currently has a limitation that it does not include all comments; I have a bug report open about that.


---
**Karl Pitrich** *January 31, 2019 14:47*

Thanks for your efforts! I find phpBB (well, any PHP-based forum software) ugly at best. Please have a look into NodeBB ([https://nodebb.org/](https://nodebb.org/)) and Discourse ([https://www.discourse.org/](https://www.discourse.org/))  for more modern alternatives.

[discourse.org - Discourse - Civilized Discussion](https://www.discourse.org/)


---
**Jon Charnas** *January 31, 2019 15:06*

**+Michael K Johnson** Yes, actually, I would greatly appreciate that! Let me know if you manage to build something with the tool, and I will see what tools G+ puts up for community managers soon. Maybe we can convert from the Google-provided archive into a static site with the comments once the bug is fixed, instead of having to fetch it all while it's online.



**+Karl Pitrich** Thanks! I haven't kept up with the web-hosting world much, and will definitely look into what nodebb and discourse offer as tools instead of phpbb. I agree phpBB is simplistic and not particularly easy on the eyes.


---
**Jon Charnas** *January 31, 2019 15:19*

Looking at cost, though, both nodeBB and discourse are way over the budget we can dedicate for a community forum.  (G+ was free...) However I was having a look at Fruum ([https://fruum.github.io/forum.html](https://fruum.github.io/forum.html)) - thinking that may play better on the Thing-Printer website...

[fruum.github.io - Our dedicated forum page. A sandbox test, documentation, news and chat from fruum](https://fruum.github.io/forum.html)


---
**Michael K Johnson** *January 31, 2019 16:50*

**+Jon Charnas** ok, I'll create a repository on gitlab and build pages. The export is missing the first comments for long threads, the comments that don't show until you click "show more comments" or something like that. If I haven't given you a URL in a week, ask me. It's only about an hour of work for me, it's just that the next few days are extra-busy.



I have also done a conversion of my own posts from takeout JSON, so it's possible that code will be a useful start towards processing whatever Google deigns to give community owners in March.


---
**Michael K Johnson** *February 01, 2019 01:57*

Got a little time this evening after all. take a look at [mcdanlj.gitlab.io - Replicape / Revolve / Redeem G+ Community Archive](https://mcdanlj.gitlab.io/thing-printer/)


---
**Michael K Johnson** *February 01, 2019 02:03*

**+Jon Charnas** If you make me an owner or moderator of this community I can also compare the takeout JSON and see if it helps fill in any gaps.


---
**Michael K Johnson** *February 03, 2019 19:39*

**+Jon Charnas** take a look at the archive and see what you think. It's currently using a minimal theme to focus on content, but of course the theme can be replaced trivially, so if you find one you like that can be added.



I checked on another community and the moderator takeout is currently 99.75% useless at this point.



The new version of the exporter has a bug fix that fetches the comments that were previously missed, so the archive I created now has a lot more comments in it and should be effectively complete, except for 34 missing pictures to add by hand and 12 missing videos. If anyone wants to fix those up let me know and I can show you how.


---
**Jon Charnas** *February 03, 2019 20:03*

Oh this is really awesome **+Michael K Johnson** ! I've got to take a closer look at the details, but a quick overview looks fantastic. I'll try to work on finding the missing pictures and videos, we can work on some those in later. 👍👍👍


---
**Michael K Johnson** *February 03, 2019 20:13*

If the instructions in the README.md aren't clear, feel free to ask questions. :)


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/PcPnTaMiGQS) &mdash; content and formatting may not be reliable*
