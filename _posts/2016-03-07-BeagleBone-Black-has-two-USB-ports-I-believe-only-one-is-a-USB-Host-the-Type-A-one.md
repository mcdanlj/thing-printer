---
layout: post
title: "BeagleBone Black has two USB ports. I believe only one is a USB Host (the Type A one)"
date: March 07, 2016 22:39
category: "Discussion"
author: Ante Vukorepa
---
BeagleBone Black has two USB ports. I believe only one is a USB Host (the Type A one). Is there a way to get the USB mini one to behave as a Host as well?



I'd like to connect both a WiFi adapter and a Manga screen to it.





**Ante Vukorepa**

---
---
**Elias Bakken** *March 07, 2016 23:04*

Good question! I think the mini port has power input, so it might be difficult. Any reason why you cannot use a USB hub?


---
**Ante Vukorepa** *March 07, 2016 23:11*

Two reasons, none of them too serious: 1) nowhere to put it (printer has no enclosure); 2) powering it would require finding a hub i can power off 12V to avoid dealing with yet another PSU.



Can i get enough 5V amperage to power a hub off of Replicape, maybe?


---
**Elias Bakken** *March 07, 2016 23:21*

I use both Wifi and and a Manga, works fine, no extra PSU. I've tried the screen on max power without problems, but if it starts to blink, just decrease the backlight. 




---
**Ante Vukorepa** *March 07, 2016 23:30*

Ah, cool. Good to know, thanks!


---
*Imported from [Google+](https://plus.google.com/105486978634901788220/posts/HuzbvUJLXLK) &mdash; content and formatting may not be reliable*
