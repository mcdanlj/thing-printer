---
layout: post
title: "This is a homing problem but I have no idea what to do"
date: June 08, 2016 17:31
category: "Discussion"
author: Bob McDowell
---
This is a homing problem but I have no idea what to do. I need to edit something.


**Video content missing for image https://lh3.googleusercontent.com/-E9kEXPUayh0/V1hWek4zX4I/AAAAAAAAAC4/QIzHBjUc-HQMF8OALthyeWaLwOiNRJVPA/s0/MOV_0001.mp4.gif**
![images/95721b9facfa8f55f5178f89f47e32d4.gif](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/95721b9facfa8f55f5178f89f47e32d4.gif)



**Bob McDowell**

---
---
**Daniel Kruger** *June 08, 2016 19:44*

Try plugging x and Y steppers in  the other way.


---
**Elias Bakken** *June 08, 2016 19:47*

Indeed! In redeem all end stops are software configured. You can set it using either g-code m574 or directly in the config. An M574 without parameters should give you the current config.


---
**Bob McDowell** *June 08, 2016 21:14*

Z stop works. X and Y however...

Recv: ok X1 stops: x_neg , X2 stops: , Y1 stops: y_neg , Y2 stops: , Z1 stops: z_neg , Z2 stops:


---
**Elias Bakken** *June 08, 2016 21:16*

Did you see this vid? [http://www.thing-printer.com/end-stop-configuration-for-redeem/](http://www.thing-printer.com/end-stop-configuration-for-redeem/)


---
**Bob McDowell** *June 08, 2016 21:21*

Yes. And honestly I still haven't wrapped my puny brain around this concept. I will re-view the video and figure it out! Thanks.


---
**Bob McDowell** *June 13, 2016 10:49*

I am still having problems. The printer is working great except for this homing problem.


---
**Elias Bakken** *June 13, 2016 10:57*

Well, that is a problem worth fixing:) 

Do you have end stops by the end where the steppers are moving, or are they located on the opposite side? If you need to change direction for homing, you can use negative values for the travel_x and travel_y values. 


---
**Bob McDowell** *June 13, 2016 11:38*

I agree! I do not have physical end stops at the "far" ends, the Y stop is on the left end of the axis and the Y stop is at the aft end. I could put stops on but I assume that virtual or soft stops can be configured. I just can't figure out how.


---
**Elias Bakken** *June 13, 2016 11:45*

Ok, so when you press the end stop at the far right (in the video), I assume it says "X1 end stop hit!" In the OctoPrint terminal window. If so, you need to write "M574 X1 x_pos" to change the direction in which is stops the stepper. 


---
**Elias Bakken** *June 13, 2016 12:49*

This looks like the default config file and it should be the same for everyone. Do you get any writing in the console when you press the end stops?


---
**Bob McDowell** *June 13, 2016 12:50*

My current default.cfg


---
**Elias Bakken** *June 13, 2016 12:51*

Default.cfg should not be altered, it will be overwritten by any subsequent software upgrades. Please place your changes into local.cfg. 


---
**Bob McDowell** *June 13, 2016 13:36*

There is no stop on the right side, the x stop is on the left near the X stepper. It does say "X1 end stop hit" when I trigger the stop.



As to local.cfg, I should copy the entries from default I want to change and then save them as they will load in subsequent redeem starts. Correct?

So to stop the X axis at pos X 190 (right hand side in video)I need a soft stop at that position? How do I do that?



And thanks for helping me, somehow I am having trouble with this.



Bob


---
**Elias Bakken** *June 13, 2016 13:45*

If the end stop is on the left side, but your objects are printed right (not inverted) then you need to define travel_x = -0.18 in the [Geometry] section


---
**Bob McDowell** *June 14, 2016 15:43*

Ok, I have re-init'd the BBB to start from scratch. The printer still had the hoing problem but is printing pretty well. I have edited the local.cfg and will restart redeem after this print is done.





[Geometry]



travel_x = -0.18



[Steppers]



direction_e = -1



[Endstops]



invert_X1 = True

invert_Y1 = False

invert_Z1 = True


---
**Bob McDowell** *June 14, 2016 15:43*

homing not "hoing"!


---
**Bob McDowell** *June 14, 2016 15:46*

Another question, your calibration cube prints well but the size is .2-.5 mm too big. Should I adjust the steps?


---
**Bob McDowell** *June 15, 2016 07:50*

When I edit the local.cfg in redeem and try to save nothing happens. It just sits there. When I try to download the file I get "500: Internal Server Error".

I have edited the local.cfg file in the /etc/redeem folder (see above)


---
**Bob McDowell** *June 15, 2016 07:52*

But it is printing GREAT! The Replicape is a much better device than the SAV  board!


---
**Elias Bakken** *June 15, 2016 11:20*

**+Bob McDowell** I think it might be an issue with permissions in the /etc/redeem folder.


---
**Bob McDowell** *June 15, 2016 12:57*

Yep, you were right. I created a local.cfg using nano as root so redeem did not have rights to write to the file.  Corrected.So now after my print is finished I will reload redeem and see if the [Geometry]  travel_x = -0.18  entry works.


---
*Imported from [Google+](https://plus.google.com/107803003115083427930/posts/XCm6DmrdLQD) &mdash; content and formatting may not be reliable*
