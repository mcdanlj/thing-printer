---
layout: post
title: "This is a short update on the reach development and at the same time a request to hear the opinion of the community which will actually be using the board"
date: June 21, 2017 13:17
category: "Discussion"
author: Theodor Andersen
---
This is a short update on the reach development and at the same time a request to hear the opinion of the community which will actually be using the board.



The current feature set of the reach includes (this list is subject to change at any moment)

- 3x Tmc2130's

- 4x Mcp9600 thermocouple chips

- 2x End stops(which will mean 8 end stops in total for a replicape, reach, BBB package)

- 2x Thermistor inputs

- 4x Pwm controlled fan outputs

- rs-485/CAN bus



Me and Justin Mcleod(my codeveloper), have decided that due to gpio constraint we will move the 3 third stepper driver (step, dir, limit) to pins currently being used by the HDMI interface. This will mean that usage of the third stepper driver will require disabling the HDMI-cape. 

The HDMI-cape will come enabled by default and the third driver will be enabled by jumpers. 



what would you guys want on the reach as features to use along with the third stepper driver. because at the current moment the reach has 16 about gpio pins which are not in use.



(i talk of the HDMI interface as a cape because at the current moment it is implemented as a virtual cape on the Beaglebone Black)





**Theodor Andersen**

---
---
**Elias Bakken** *June 21, 2017 13:28*

Looks like a smorgasboard! I imagine most people will want to use the third driver for full color blending, but would be interesting to hear what the 8th axis can be used for : )




---
**Per Arne Kristiansen** *June 21, 2017 13:56*

Drivers that can handle more current ? 


---
**Grfive Store** *June 21, 2017 14:47*

Please add support for PT100 sensors


---
**Theodor Andersen** *June 21, 2017 16:04*

**+Grfive Store** we have considered it and it has been brought up in the past, with a poll on this very page. at this point, it won't because the demand wasnt high enough to justify it. things arent set in stone yet though, so it might happen.




---
**Andrew Dowling** *June 21, 2017 17:07*

I like all of the above, and wont really be needing more than 4 extruders (500C capable of course), with support for end stops being used as a filament detector, so it all adds up for me.


---
**Theodor Andersen** *June 21, 2017 17:09*

**+Per Arne Kristiansen** due to area restraints on the board I urge you to at options which utilize the can bus. That should help you out 


---
**Per Arne Kristiansen** *June 22, 2017 16:27*

**+Theodor Andersen** i didnt think about that 👍  with can bus we can use mdrive motors servo motors  etc Thats awesome 


---
**Radek Biskupski** *June 23, 2017 14:00*

I'm voting YES for these. CAN is very fast and RS-485 together with Modbus could control Emerson, Delta, Ell-BG and other's big drives used for Lathes and industrial CNC's. When will you be ready with selling it? :)


---
*Imported from [Google+](https://plus.google.com/117630610652060224381/posts/DXLWa49218z) &mdash; content and formatting may not be reliable*
