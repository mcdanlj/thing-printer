---
layout: post
title: "Ok, quick update! The Kamikaze bug causing kernel panics seems to be solved by adding a mutex on the ADCs: Also, I've started a plugin for Octoprint that handles choosing the printer config in Redeem: Finally, I've managed to"
date: June 15, 2015 11:35
category: "Discussion"
author: Elias Bakken
---
Ok, quick update! The Kamikaze bug causing kernel panics seems to be solved by adding a mutex on the ADCs: [https://bitbucket.org/intelligentagent/redeem/commits/2098b36bc2c6d7b522d0c7eb3a2ce7dc7febe8d7#Lredeem/Thermistor.pyT36](https://bitbucket.org/intelligentagent/redeem/commits/2098b36bc2c6d7b522d0c7eb3a2ce7dc7febe8d7#Lredeem/Thermistor.pyT36)
 Also, I've started a plugin for Octoprint that handles choosing the printer config in Redeem: [https://bitbucket.org/intelligentagent/operate/src/db7bf1a2248cb8403cd588c4c296bca75aa392b5/octoprint-plugin/?at=master](https://bitbucket.org/intelligentagent/operate/src/db7bf1a2248cb8403cd588c4c296bca75aa392b5/octoprint-plugin/?at=master)

Finally, I've managed to complete few prints with the first Rev B board, and it looks (and sounds) beautiful!

I'll clean up some hardware bugs and get a quote on the production of ten developer boards!





**Elias Bakken**

---
---
**Elias Bakken** *June 15, 2015 14:02*

Rev_b has now been merged into develop and with some added bug fixes. Yay!


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/Sr2nz6TFZEH) &mdash; content and formatting may not be reliable*
