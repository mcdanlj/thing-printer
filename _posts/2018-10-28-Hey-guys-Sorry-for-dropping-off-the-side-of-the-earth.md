---
layout: post
title: "Hey guys, Sorry for dropping off the side of the earth"
date: October 28, 2018 20:28
category: "Discussion"
author: Jon Charnas
---
Hey guys,



Sorry for dropping off the side of the earth. Still alive, still trying to push forward on the new image front... We're close, but not yet ready for a new release.



Also, life's gotten in the way of many of the devs, so I do apologize for the extra long delay. There will likely be a release-candidate image for testing before or around christmas time, but a new release will likely be next year.



Things are complicated on the developer side. So much to do, so little time... You know how that goes. We'll keep fighting to get you a newer and better version of Redeem, but we feel it's better to get it out when it's ready, and not before.





**Jon Charnas**

---


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/gUU3PR6r1wC) &mdash; content and formatting may not be reliable*
