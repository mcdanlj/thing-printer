---
layout: post
title: "Just in time for the Debian Jessie release party and the release of Kernel 4.0 I've finished a beta version of Kamikaze which is what I've named the Debian based distro for Replicape"
date: April 20, 2015 13:25
category: "Discussion"
author: Elias Bakken
---
Just in time for the Debian Jessie release party and the release of Kernel 4.0 I've finished a beta version of Kamikaze which is what I've named the Debian based distro for Replicape. The aim is to have a turn key BBB image with Redeem, Toggle and Operate which is the new name for the web interface incorporating Octoprint and stuff. There are still some quirks with this, but great if anyone wants to give it a shot and help with debugging the last issues :) 





**Elias Bakken**

---


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/38fwvKyZS6M) &mdash; content and formatting may not be reliable*
