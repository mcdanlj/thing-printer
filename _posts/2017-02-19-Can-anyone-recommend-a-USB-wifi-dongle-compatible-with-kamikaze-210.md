---
layout: post
title: "Can anyone recommend a USB wifi dongle compatible with kamikaze 2.1.0?"
date: February 19, 2017 03:41
category: "Discussion"
author: Benjamin Liedblad
---
Can anyone recommend a USB wifi dongle compatible with kamikaze 2.1.0?



I tried the TP-Link TL-WN725N, I ordered for my system last night - seems like there are issues with this particular model.



If I need a different module, so be it... but if anyone is curious as to the issues I'm experiencing, they seem to be similar to those described in the following link:

[https://github.com/lwfinger/rtl8188eu/issues/49](https://github.com/lwfinger/rtl8188eu/issues/49)





**Benjamin Liedblad**

---
---
**Jon Charnas** *February 19, 2017 07:09*

🤔 I'll try to see if there's something that can be done at the OS level about that. I would recommend a dongle compatible with a raspberry pi, it's basically the same Linux kernel and architecture we're using.


---
**Jon Charnas** *February 19, 2017 07:13*

What's really odd is that that thread is really old, we're on kernel 4.1 for kamikaze 2.1.0... Can you share what the output of dmesg is after you start getting the slow pings? And are there any errors showing up in the output of "journalctl -xe"?


---
**Klipper Pressure Advance** *February 20, 2017 21:14*

for simplicity I used this device and connected straight to the ethernet port .

[tp-link.com.au - 300Mbps Wireless N Nano Router - TP-Link](http://www.tp-link.com.au/products/details/cat-9_TL-WR802N.html)

It is small and easy connect.


---
**Klipper Pressure Advance** *February 20, 2017 21:17*

it basically makes the wr802n a client to my existing wifi  and it is small


---
**Dan Cook** *February 21, 2017 19:55*

I bought this from Amazon... dirt cheap, and works great so far... [https://www.amazon.com/gp/product/B003MTTJOY/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B003MTTJOY/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)


---
**Benjamin Liedblad** *April 25, 2017 02:54*

Thanks **+Dan Cook**, bought the Edimax and no problems since. 


---
*Imported from [Google+](https://plus.google.com/111192213763748051453/posts/EuqgAEHVpvD) &mdash; content and formatting may not be reliable*
