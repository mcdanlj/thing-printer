---
layout: post
title: "I've added a new version of the OctoPrint Redeem plugin, so you can now download current local, printer and default profiles"
date: April 25, 2016 01:52
category: "Discussion"
author: Elias Bakken
---
I've added a new version of the OctoPrint Redeem plugin, so you can now download current local, printer and default profiles. To update: 

apt-get update

apt-get install python-octoprint-redeem





**Elias Bakken**

---
---
**Jarek Szczepański** *April 25, 2016 08:50*

why not add this plugin to [plugins.octoprint.org](http://plugins.octoprint.org)?


---
**Elias Bakken** *April 25, 2016 13:33*

I'll add that to the Replicape roadmap: [https://trello.com/b/kG6Fx7Ib/replicape-roadmap](https://trello.com/b/kG6Fx7Ib/replicape-roadmap) 


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/GFVG92m4tqx) &mdash; content and formatting may not be reliable*
