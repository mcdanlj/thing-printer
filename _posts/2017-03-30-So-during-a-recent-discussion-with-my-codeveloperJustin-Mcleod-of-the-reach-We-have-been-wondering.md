---
layout: post
title: "So during a recent discussion with my codeveloper(Justin Mcleod) of the reach, We have been wondering;"
date: March 30, 2017 08:51
category: "Discussion"
author: Theodor Andersen
---
So during a recent discussion with my codeveloper(Justin Mcleod) of the reach, We have been wondering; what do people see in the PT100, that places it above a thermocouple?



Because we are seeing a vastly higher demand for the pt100´s than thermocouple when it should be evident that thermocouple are as accurate, and with the right type can reach higher temperatures



So what would people prefer?  regardless of what you choose please tell us why.



This a surprise poll, weigh in or forever hold your peace. :)

 





**Theodor Andersen**

---
---
**Andrew Dowling** *March 30, 2017 09:34*

The PT100 is available through e3d. Its a plug and play solution. Sure a thermocouple would also work, but it would require a lot of additional making - that some of us might not have tools  for. My best tools are a good set of crimpers for crimping connectors. I don't have a lathe. I don't have a CNC mill (yet). And I have no way of 3d printing a hot end. I need reliable plug and play solutions.


---
**Theodor Andersen** *March 30, 2017 09:44*

You can just as easily purchase a type k thermocouple through e3d**+Andrew Dowling** 


---
**Oyvind Dahl** *March 30, 2017 09:57*

Client wanted PT100. I really wanted to use thermocouple to simplify the connection, but couldn't find a satisfying option for the client. I don't remember exactly, but I think it was because I couldn't find a thermocouple that was plug and play and could reach high enough temperatures in the same price range as PT100.

 


---
**Andrew Dowling** *March 30, 2017 11:12*

I am using the new cartages. I kinda like them a lot. Better strain relief and less messing if something goes wrong. **+Theodor Andersen** 


---
**Theodor Andersen** *March 30, 2017 11:50*

**+Andrew Dowling**​ [e3d-online.com - Type K Thermocouple Cartridge](http://e3d-online.com/Type-K-thermocouple-cartridge) :D


---
**Ryan Carlyle** *March 30, 2017 13:31*

E3Dv6 blocks have accepted Makerbot-style (M3 thermowell) thermocouples since day 1. Their new-ish cartridge style is even easier. 



The awesome thing about thermocouples is that you can reterminate the sensing tip yourself if you need to. It's fantastic for field repairs (eg if your wires break from bend cycle fatigue). All you need to do is strip the insulation off the tips, twist them together, and wrap with a bit of kapton tape, and you've got a new thermocouple. Their function is such a straightforward bit of physics that they're basically bulletproof. 



I struggle to see any real benefit to the PT100 over the type-K TC... some people make arguments about resolution, but that's really a function of the amp and input (eg ADC or SPI) you're using. Control boards that only have 10bit ADCs reading a linearized output from an amp struggle to get acceptable resolution out of the wide sensing range that a TC amp is going to provide. (EG 2C per bit, which sucks.) You really ought to use SPI amps for TCs to take advantage of the wide sensing range without losing resolution.



Basically, they're both fine, but you have to implement them properly. 


---
**Justin McLeod** *March 30, 2017 17:49*

**+Ryan Carlyle** Nailed it. The benefits of TCs are plenty. We were using a 12-18 bit (depending on what update rate you want) I2C thermocouple IC by the way.


---
**Leo Södergren** *April 04, 2017 12:22*

If I'm not remembering​ incorrect: thermocouples design makes it much harder(for the printer) to recognize a faulty connector. If a pt100 gets loose the temperature drops to ~-100°C if a K -type thermocouple gets lose the temperature drops to ~0°C. 



Also the standard connector for the k-type is huge, on a small board that could be problematic


---
**Justin McLeod** *April 04, 2017 21:56*

**+Leo Södergren** The IC we are using to read the thermocouples has open circuit and closed circuit detection. We are using the MCP9600 for anyone interested. 



And yes, thermocouple connectors are huge. We chose a vertical connector to save as much room as possible. It is a PCC-SMP-V.


---
**Ryan Carlyle** *April 05, 2017 01:29*

At the board end, all you need is a screw terminal nearby the amp so the cold-junction compensation works properly. There's no need for TC connectors on the mainboard. You only need them when putting a disconnect point in the TC wire run between the hot end and mainboard.


---
**Justin McLeod** *April 05, 2017 07:38*

**+Ryan Carlyle** Hi Ryan, thank you so much for the tip! I had read about that but was not sure if cold junction compensation would be effected or not.


---
*Imported from [Google+](https://plus.google.com/117630610652060224381/posts/B4EpDQYgJ5e) &mdash; content and formatting may not be reliable*
