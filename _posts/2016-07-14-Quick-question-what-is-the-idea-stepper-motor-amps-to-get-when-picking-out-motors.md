---
layout: post
title: "Quick question, what is the idea stepper motor 'amps' to get when picking out motors?"
date: July 14, 2016 06:26
category: "Discussion"
author: Andrew A
---
Quick question, what is the idea stepper motor 'amps' to get when picking out motors?  I see 1.2A with 2.5 peak with the tmc2100 drivers.  Im guessing running 2.5 would be bad, and 1.2A im guessing would be on the weaker end of the motors, so not sure what to shoot for.



Secondly, same question for heatbed basically, im going for 24v system, i see it is limited by 20A fuse, so 20A x 24v = 480 watts, however thats for the entire board if im not mistaken, so hotend, motors, and everything else running, how much is actually left for the heatbed?  How close can we get to the max watts before that fuse starts blowing?



i was considering a 300x300mm or 400x400mm bed so a bit on the bigger end, im not even sure how many watts i need to heat it in reasonable times





**Andrew A**

---
---
**Jon Charnas** *July 14, 2016 06:39*

What kind of printer are you building? I run a delta which doesn't need much current on the steppers and at 0.7A it can print at insanely f fast speed without missing steps... 


---
**Andrew A** *July 14, 2016 06:54*

well i was wanting to get the best that i can since the prices are basically the same, so that basically means finding out what the board can use safely.  but to answer your question im building something similar to this [http://openbuilds.org/builds/voxel-ox-extendable-3d-printer-and-cnc-platform.2418/](http://openbuilds.org/builds/voxel-ox-extendable-3d-printer-and-cnc-platform.2418/)



i havn't settled on if i want to build it into a box type design for more support.  and im considering switching from belt to lead screw for the hotend axis.  mainly because i 'might' end up trying the router on it, but am worried about speed loss doing that so not sure yet.  So thats also partly why i wanted to get the best motors i could without having heat/lost steps/blown fuses ect.


---
**Andrew A** *July 14, 2016 06:57*

oh and another change i was planning to make was basically just using all 500mm extrusion so i won't have to make hardly any cuts, just the top beam and maybe the middle depending on how i angle the sides.  I was trying to think of a way to get around using those plate gantrys since that adds alot to the cost but the idea's i came up with would make it harder to get everything aligned straight and square


---
**Ryan Carlyle** *July 14, 2016 14:30*

500mm frame members is pretty big for a Mendel type Y-bed printer... you know it's going to need almost a meter of footprint front/back to print? Measure your desk :-)



Unless you do something to bind up the linear stages, 1.5A is way more than enough for any normal-sized 3D printer. The super-common Kysan 1124090 at 1.5A at 24v can hit 350mm/s with moderately heavy motion stages -- which is kind of uselessly fast considering your extruder can't keep up with half that. 



Most people building 3d printers end up using ~1.5A rated steppers and running them around 1-1.2A to keep the heat down, so they don't melt/warp plastic motor brackets. And lots of 3d printers sling around dual direct drive extruders using only 0.7-0.8A using a puny little short-stack NEMA 17.



Now, if you want to do milling/routing, you need to go as big and beefy as you can. NEMA 23s are ideal. But the TMC2100 probably doesn't have enough capacity for NEMA 23s. 


---
**Andrew A** *July 14, 2016 22:57*

the open build nema 17's are 1.7A, would a replicape handle those running at 1.2A or would it lose steps or something?  i looked up the kysans but its 10-$20 for shipping, and i need to buy some parts from openbuilds anyway so i can save money by just getting their motors if they will work that is.



also thanks for pointing out the meter Y to print, i did overlook that a tiny bit, but assumed because i planned to make it bigger that it would have fit within the 500mm's.  or at least i thought i had, looking closer the Y is actually 533mm.



anyways i planned to make a little spot just for the printer, so i never expected to fit in on my desk, plus i don't want to breathe the fumes so it might end up out in the garage.



originally i planned to make some toys with this machine, but i recently started looking at the data sheets on these filaments and they have LD50 ratings which have me very worried.  alot of them talking about not coming in contact with the dust, to avoid skin and eye contact......wasn't expecting all of that.  the interesting thing was the PLA which was supposed to be corn based and i assumed the most non toxic happens to be 'mildly' toxic.  throws a wrench in my plans a bit 


---
**Ryan Carlyle** *July 14, 2016 23:40*

For toys for young kids you'll want to use US-made or EU-made filament. There is some credible anecdotal evidence of terrible toxic contamination in large amounts of Chinese filament. Doesn't actually matter too much between ABS, PLA, PETG, etc as far as toxicity goes unless your kid is literally chewing on the plastic or sleeping in the room the printer is running in. Most baby toys are made out of ABS! 



The bigger issue is prints breaking apart and little bits becoming choking hazards -- in my opinion PLA is too brittle for toys, and ABS is probably also too brittle unless you have a very hot enclosure. PETG type filaments and nylon are durable enough and nice and chemically inert. I personally only give my kid (1yo) nylon printed stuff to play with. I'm not worried about him chewing on Taulman nylon products. I do worry about the pigments and additives in Chinese filaments. 



As for the motor -- torque is simply linear with current if you under-drive the motor. Running a big motor with low current will increase ringing because of the rotor inertia and make your microstep positions less accurate because of the detent torque, but as long as you're over maybe about half the rated current, it should perform pretty well.


---
**Andrew A** *July 15, 2016 02:58*

it was going to be for a bird, so lots of chewing ect.  i just did some more searching and found this article :(  [https://pinshape.com/blog/3d-printing-food-safe/](https://pinshape.com/blog/3d-printing-food-safe/)



anyways thanks for all the help, going to finish up ordering my parts i guess, at this point im not even sure ill be printing anything tho lol maybe i can find some food grade filiments somewhere


---
**Andrew A** *July 15, 2016 03:32*

found the food grade stuff and its about the same price and clear like glass, so that problems solved




---
*Imported from [Google+](https://plus.google.com/106001037169639681932/posts/D6463ASsXzV) &mdash; content and formatting may not be reliable*
