---
layout: post
title: "Finally printing something! The printer has never been so quiet!"
date: February 20, 2016 05:10
category: "Discussion"
author: Veridico Cholo
---
Finally printing something! 

The printer has never been so quiet!

![images/6f0d007fc6645ecd6b4b810b3cb54311.gif](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/6f0d007fc6645ecd6b4b810b3cb54311.gif)



**Veridico Cholo**

---


---
*Imported from [Google+](https://plus.google.com/109713994627754483735/posts/b84Kc1C8qUn) &mdash; content and formatting may not be reliable*
