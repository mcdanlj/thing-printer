---
layout: post
title: "can someone tell me why when i run a g29 command, the probe does not stop the bed from crashing into my probe?"
date: February 20, 2018 19:47
category: "Discussion"
author: Andrew A
---
can someone tell me why when i run a g29 command, the probe does not stop the bed from crashing into my probe?



my probe will stop the bed from moving closer to it during octoprint manual movements.  so it is setup correctly as a endstop based on that.



but during a g29 its just not working as it should.  the probe is on, the light turns on as the bed is hitting it.  strangely the x/y movements are super fast, yet the g32 is supposed to slow it down i thought with the s-60?



i suppose the problem could be the g31 commands at the end of each probe, then its homing z?  so its docking my probe then homing z?  i guess maybe thats fine on a normal printer but im on core xy so.....yah fun times.  maybe it has to do with where my hard endstop is located?   i have it at the min then use a offset to not smash my nozzle.  seemed the most logical because if i had it at max then it would take 5 mins every time it homes to get back and forth.  anyways how to i get this to work







[Macros]

g31 = 

	M106 P2 S0                        ; Turn off power to probe.

	M574 Z2  ; Probe up (Dock sled)

	M280 P0 S320 F3000              ; Probe up (dock sled)

g32 = 

	M106 P2 S255                    ; Turn on power to probe.

	M574 Z2 z_ccw, h_ccw  ; Probe down (Undock sled)

	M280 P0 S-60 F3000              ; Probe down (Undock sled)

g29 = 

	M561                ; Reset the bed level matrix

	M557 P0 X10 Y0     ; Set probe point 0

	M557 P1 X10 Y180    ; Set probe point 1

	M557 P2 X180 Y180   ; Set probe point 2

        M557 P3 X180 Y0   ; Set probe point 3

        M557 P4 X90 Y90   ; Set probe point 4



	G28 X0 Y0           ; Home X Y

	G28 Z0              ; Home Z

	G0 Z12              ; Move Z up to allow space for probe

	G32                 ; Undock probe

	G92 Z0              ; Reset Z height to 0

	G30 P0 S            ; Probe point 0

	G0 Z0               ; Move the Z up

	G31                 ; Dock probe



	G28 Z0              ; Home Z

	G0 Z12              ; Move Z up to allow space for probe

	G32                 ; Undock probe

	G92 Z0              ; Reset Z height to 0

	G30 P1 S            ; Probe point 1

	G0 Z0               ; Move the Z up

	G31                 ; Dock probe



	G28 Z0              ; Home Z

	G0 Z12              ; Move Z up to allow space for probe

	G32                 ; Undock probe

	G92 Z0              ; Reset Z height to 0

	G30 P2 S            ; Probe point 2

	G0 Z0               ; Move the Z up

	G31                 ; Dock probe



	G28 Z0              ; Home Z

	G0 Z12              ; Move Z up to allow space for probe

	G32                 ; Undock probe

	G92 Z0              ; Reset Z height to 0

	G30 P3 S            ; Probe point 3

	G0 Z0               ; Move the Z up

	G31                 ; Dock probe



	G28 Z0              ; Home Z

	G0 Z12              ; Move Z up to allow space for probe

	G32                 ; Undock probe

	G92 Z0              ; Reset Z height to 0

	G30 P4 S            ; Probe point 4

	G0 Z0               ; Move the Z up

	G31                 ; Dock probe



	G28 X0 Y0           ; Home X Y

	M561 U; (RFS) Update the matrix based on probe data

	M561 S; Show the current matrix

	M500; (RFS) Save data



[Steppers]

microstepping_x = 6

microstepping_y = 6

microstepping_z = 6

microstepping_e = 6

microstepping_h = 6

current_x = 0.5

current_y = 0.5

current_z = 0.5

current_e = 0.5

current_h = 0.5

steps_pr_mm_x = 10.0

steps_pr_mm_y = 10.0

steps_pr_mm_z = 50.0

steps_pr_mm_e = 26.15

steps_pr_mm_h = 50.0

in_use_h = True

slave_z = H

direction_z = -1

direction_h = -1

direction_x = -1

direction_y = -1



[Endstops]

invert_x1 = False

invert_x2 = True

invert_y1 = False

invert_y2 = True

invert_z1 = False

invert_z2 = True

end_stop_z1_stops = z_ccw, h_ccw

end_stop_z2_stops = z_ccw, h_ccw

end_stop_x1_stops = x_ccw, y_ccw

end_stop_y1_stops = x_ccw, y_cw



[Heaters]

sensor_e = SEMITEC-104GT-2

max_temp_e = 270.0



[Cold-ends]

connect-therm-e-fan-0 = True

add-fan-0-to-m106 = True



[Planner]

max_speed_x = 0.3

max_speed_y = 0.3

max_speed_z = 0.03

max_speed_e = 0.2

max_speed_h = 0.03

acceleration_x = 3.0

acceleration_y = 3.0

acceleration_z = 1.0

acceleration_e = 3.0

acceleration_h = 1.0



[Homing]

home_speed_x = 0.1

home_speed_y = 0.1

home_speed_z = 0.1

home_speed_e = 0.01

home_speed_h = 0.1

home_x = 0.00

home_y = 0.00

home_z = 0.00



[Geometry]

axis_config = 2

travel_x = 0.216

travel_y = 0.203

travel_z = 0.4

offset_x = 0.0

offset_y = 0.0

offset_z = 0.0025



[Probe]

length = 0.01

speed = 0.05

accel = 0.1

offset_x = 0.023

offset_y = -0.004





**Andrew A**

---
---
**William Burton** *February 21, 2018 02:38*

So i have been playing with the BLTouch tonight and it seems to be working with the default g29.  It goes to three points and takes measurements i think.  I have a Tronxy x5s corexy printer.  I had to change a few things from the instruction page. 



Also from the documentation, it looks like you should be able to create the macro with the G29S command.  But in all my tests it just ran the default 3 points in the default.cfg.  No matter what parameters i gave it.



I am no expert on the configuration files, but i included mine to look at.   right now my printer homes to the wrong side and then i offset it back to the top corner, so it kind of back assward, but it works great for me.







My end point config specific to the probe

[Endstops]

invert_z2 = False

end_stop_x2_stops =

end_stop_z2_stops = z_pos, h_pos





here is my full configuration

[System]



[Geometry]

axis_config = 2

travel_x = 0.32

travel_y = 0.32

travel_z = 0.41

travel_h = 0.0

offset_x = 0.32

offset_y = 0.32

offset_z = 0.0

offset_e = 0.0

offset_h = 0.0



[Steppers]

microstepping_x = 4

microstepping_y = 4

microstepping_z = 4

microstepping_e = 3

microstepping_h = 4

current_x = 0.55

current_y = 0.55

current_z = 0.75

current_e = 0.6

current_h = 0.75

steps_pr_mm_x = 5

steps_pr_mm_y = 5

steps_pr_mm_z = 25

steps_pr_mm_e = 6

steps_pr_mm_h = 25

in_use_x = True

in_use_y = True

in_use_z = True

in_use_e = True

in_use_h = True

in_use_a = False

in_use_b = False

in_use_c = False

direction_x = 1

direction_y = 1

direction_z = 1

direction_e = 1

# direction_h = 1

slow_decay_x = 1

slow_decay_y = 1

slow_decay_z = 1

slow_decay_e = 1

slow_decay_h = 1

slave_z = H



[Endstops]

has_x = True

has_y = True

has_z = True

has_e = False

has_h = False

invert_x1 = True

invert_y1 = True

invert_z1 = True

invert_z2 = False

end_stop_x1_stops = x_ccw, y_ccw

end_stop_y1_stops = x_ccw, y_cw

end_stop_z1_stops = z_cw, h_cw

end_stop_x2_stops =

end_stop_z2_stops = z_pos, h_pos



[Planner]



# size of the path planning cache

move_cache_size = 1536



max_speed_x = 0.3

max_speed_y = 0.3

max_speed_z = 0.03

max_speed_e = 0.2

max_speed_h = 0.03

acceleration_x = 3.0

acceleration_y = 3.0

acceleration_z = 0.5

acceleration_e = 3.0

acceleration_h = 0.5



[Homing]

home_speed_x = -0.1

home_speed_y = -0.1

home_speed_z = 0.01

home_speed_e = 0.01

home_speed_h = 0.01



[Cold-ends]

connect-therm-e-fan-1 = True

therm-e-fan-1-target_temp = 50

add-fan-0-to-m106=true



[Fans]

default-fan-2-value = 1.0

default-fan-3-value = 1.0



[Heaters]

sensor_E = SEMITEC-104GT-2

#pid_Kp_E = 0.0091

#pid_Ti_E =  78.8221

#pid_Td_E =5.6870

#max_rise_temp_E = 10.0

#max_fall_temp_E = 10.0

min_temp_E = 20.0

max_temp_E = 280.0



[Servos]

servo_0_enable = True

servo_0_channel = P9_14

servo_0_angle_init = 90

servo_0_angle_min = 0

servo_0_angle_max = 180

servo_0_pulse_min = 0.0006

servo_0_pulse_max = 0.0024



[Probe]

length = 0.01

speed = 0.05

accel = 0.1

offset_x = 0.023

offset_y = -0.004


---
**Andrew A** *February 22, 2018 20:18*

so i changed a few things up, and now its at least doing the motions as expected for the most part



[Macros]

g31 = 

	M106 P2 S0                        ; Turn off power to probe.

	;M574 Z2  ; Probe up (Dock sled)

	;M280 P0 S320 F3000              ; Probe up (dock sled)

g32 = 

	M106 P2 S255                    ; Turn on power to probe.

	;M574 Z2 z_ccw, h_ccw  ; Probe down (Undock sled)

	;M280 P0 S-60 F3000              ; Probe down (Undock sled)

g29 = 

	M561                ; Reset the bed level matrix

	M557 P0 X10 Y0     ; Set probe point 0

	M557 P1 X10 Y180    ; Set probe point 1

	M557 P2 X180 Y180   ; Set probe point 2

	M557 P3 X180 Y0   ; Set probe point 3

	M557 P4 X90 Y90   ; Set probe point 4

	G28 X0 Y0           ; Home X Y

	G28 Z0              ; Home Z

	G0 Z12              ; Move Z up to allow space for probe

	G32                 ; Undock probe

	G92 Z0              ; Reset Z height to 0

	G30 P0 S            ; Probe point 0

	G0 Z0               ; Move the Z up

	G28 Z0              ; Home Z

	G0 Z12              ; Move Z up to allow space for probe

	G92 Z0              ; Reset Z height to 0

	G30 P1 S            ; Probe point 1

	G0 Z0               ; Move the Z up

	G28 Z0              ; Home Z

	G0 Z12              ; Move Z up to allow space for probe

	G92 Z0              ; Reset Z height to 0

	G30 P2 S            ; Probe point 2

	G0 Z0               ; Move the Z up

	G28 Z0              ; Home Z

	G0 Z12              ; Move Z up to allow space for probe

	G92 Z0              ; Reset Z height to 0

	G30 P3 S            ; Probe point 3

	G0 Z0               ; Move the Z up

	G28 Z0              ; Home Z

	G0 Z12              ; Move Z up to allow space for probe

	G92 Z0              ; Reset Z height to 0

	G30 P4 S            ; Probe point 4

	G0 Z0               ; Move the Z up

	G31 ;

	G28 X0 Y0           ; Home X Y

	M561 U; (RFS) Update the matrix based on probe data

	M561 S; Show the current matrix

	M500; (RFS) Save data



i was having issues with one of my z motors not responding to the endstop for some strange reason.  commenting out the few things in the g31 and g32 is what got everything moving right.  however i don't think these macro's are working at all like intended.  the G31 is run at pretty much the end of the g29 command, which is 'supposed' to cut power to the probe right?  but when printing starts the prob clearly still has power.



anyways my main issue at this point is that it appears to be probing in the wrong direction or something. im getting -10mm for every probe point.  and their also appears to be no compensation happening on the z axis during printing.  but i guess if they are all -10 then it thinks everything is flat?



anyways what would be causing this?  the guide mentions setting up the soft endstops which i have not done.  thats the only thing i can think of that i havn't followed in the guide, but i fail to see how that would be causing the issue im having.  i have no use for soft endstops otherwise i would.


---
*Imported from [Google+](https://plus.google.com/106001037169639681932/posts/63TKyhJV3Wm) &mdash; content and formatting may not be reliable*
