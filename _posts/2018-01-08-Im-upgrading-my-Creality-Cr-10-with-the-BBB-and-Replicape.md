---
layout: post
title: "I'm upgrading my Creality Cr-10 with the BBB and Replicape"
date: January 08, 2018 09:27
category: "Discussion"
author: Kenneth Tan
---
I'm upgrading my Creality Cr-10 with the BBB and Replicape.

I've ordered some KF2510 3P 2.54mm Pitch connectors, but I'm unsure what pliers to buy to crunch the connectors. Could someone give me a search word for the correct ones? There are so many different molds to select.





**Kenneth Tan**

---
---
**Kenneth Tan Fotografie** *January 09, 2018 16:05*

Found one, I hope it works/fits.

[nl.aliexpress.com - JRD Krimptang Krimptang Voor Dupont XH2.54 KF2510 SM 2.54mm 3.96mm Plug Terminals AWG28-22/JRD1 + 011 Lengte 200mm](https://nl.aliexpress.com/item/Crimping-Tool-Crimping-pliers-For-Dupont-XH2-54-KF2510-SM-2-54mm-3-96mm-plug-Terminals/32805173349.html?spm=a2g0z.search0104.3.1.TfSCD4&ws_ab_test=searchweb0_0,searchweb201602_4_10152_10151_10065_10068_10344_10342_10343_10340_10341_10084_10083_10613_10304_10615_10307_10614_10302_10059_10314_10534_100031_10604_10103_10142,searchweb201603_1,ppcSwitch_5&algo_expid=21d0589d-17ab-4af2-b8de-c356b206ab98-0&algo_pvid=21d0589d-17ab-4af2-b8de-c356b206ab98&priceBeautifyAB=0)




---
*Imported from [Google+](https://plus.google.com/+KennethTanFotografie/posts/GgHK7RQu74T) &mdash; content and formatting may not be reliable*
