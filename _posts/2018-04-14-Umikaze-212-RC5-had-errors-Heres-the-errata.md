---
layout: post
title: "Umikaze 2.1.2 RC5 had errors... Here's the errata"
date: April 14, 2018 22:07
category: "Discussion"
author: Jon Charnas
---
Umikaze 2.1.2 RC5 had errors... Here's the errata.

Welcome 2.1.2 RC5c. Took me quite a while to figure out why we had issues.



[https://github.com/intelligent-agent/Umikaze/releases/tag/v2.1.2rc1](https://github.com/intelligent-agent/Umikaze/releases/tag/v2.1.2rc1)



However this new image will guarantee you are running Redeem 2.1.2.

New features in 2.1.2 RC5c compared to Umikaze 2.1.1:

- Upgrade in place (keep your Redeem, Toggle, Octoprint & network configs across the upgrade)

- Print from SD (Octoprint hands off a file handle to Redeem instead of streaming gcodes), which makes stutter on curves or small circles go away.

- M-Code synchronization: any non-path command is now executed in order (if you had large buffers you'd see fans turning on or off way before the tool arrived at where the fan change was wanted)



Also included:

- OctoPrint 1.3.8, 

- upgraded Ubuntu packages all around 

- kernel is now locked so you can run apt-get upgrade without fear



Beware of the dragons! One flash still resulted in losing wifi configuration, though Redeem and OctoPrint files were intact.



If you're printing with Umikaze 2.1.1, then at 99% Umikaze 2.1.2 RC5c will work better for you (at worse it'll seem the same), if you follow the usual precautions of backing up your files before relying on the upgrade in place. (Do prepare a USB cable to hook up to your BBB in case network config gets lost... this is why this is still a release candidate)



I really want to apologize for the long delay in getting 2.1.2 out. I wanted to get it done way sooner, but I think it's best for everyone that 2.1.2 brings out a solidly stable platform for everyone. We've added wiki documentation on how to try and run newer code straight from the git repository for those who are impatient to test out new features.



I aim to be able to release 2.1.2 soon, but not before all bugs with the new features have been fixed. So far there are no regressions, which is a sign of a solid setup, but I've been fighting with newer versions of the underlying system not cooperating with the previous way I was building Umikaze.



Next release will be 2.2.0 - and I'll be rethinking the image build completely, in a way that should allow for a much faster and regular release process. Thank you all for your patience,

Jon





**Jon Charnas**

---
---
**Jon Charnas** *April 14, 2018 22:13*

Oh, forgot: if you want to flash a fresh install, ignoring all files from a previous installation, simply remove the /etc/kamikaze-release from your beaglebone before flashing. For those not sure, SSH into your system, and run "rm /etc/kamikaze-release" 


---
**László Szekeres** *April 16, 2018 22:39*

I'd like to ask for some support on installing Umikaze 2.1.2 RC5c. I think, I miss some trivial thing, but I don't know what exactly...



I built a big delta (BBBlack + Replicape B3A) which was running great on Kamikaze 2.0.8 for a while, but I wanted to test the latest Image.



1.) I installed 2.1.2RC5. The installation went well, but I was not able to connect to Redeem. The connection was dropped immediately as far as I tried to connect to Redeem in Octoprint. I could see some missing parameters/parsing errors via SSH during the startup of Redeem. (e.g. pwm_freq parameter was missing). For testing purposes I added manually pwm_freq to the local.cfg but than another error was coming with some other missing parameter again.



2.) Than I installed 2.1.1 without any issue, connection OK, printer seems working fine.



3.) Than I installed the latest 2.1.2RC5c with clean install - by removing the "/etc/kamikaze-release" folder. I have again a connection issue to Redeem in Octoprint, it's not connecting.

(Sorry, I forgot to copy the log of Redeem startup, and I have no access right now to my printer.)



So my simple question would be: what do I miss to make 2.1.2RC5c running, while it is no problem to install and run 2.1.1 or 2.0.8?

Do you have any suggestion based on the mentioned symptoms?


---
**Jon Charnas** *April 17, 2018 05:19*

**+László Szekeres** I assume you're importing your local.cfg file into RC5c? If so there may be called whose names have changed. I would be happy to help you with figuring out what's different if you can share your config file? Also what are you selecting as printer.cfg?


---
**László Szekeres** *April 17, 2018 06:27*

**+Jon Charnas** I’ve tried both way: importing local.cfg from old version and using a totally empty config file. As for printer.cfg, I’m using an empty file because I planned to manage all my stuff in local.cfg (which I created from default.cfg and modified it). 

My assumption was, that default.cfg has all stuff inside as default, so upper layers only modify parameters which are necessary to be adapted. I’m very excited to make it run, but unfortunately I can share my config only today night (CEU time).


---
**Jon Charnas** *April 17, 2018 07:06*

**+László Szekeres** not a problem - I'm in GMT+1 timezone as well. If you have time tonight then we can debug it. Also maybe easier if you join the Slack and ask in #support - I'll be there and chat is sometimes faster than G+ posts. The logs will definitely be interesting to read to figure out what went wrong.


---
**Mark Loit (CanadianAvenger)** *April 24, 2018 01:45*

Just to capture this here as well, for those not on slack. To use rc5c a couple of minor fix-ups need to be performed. Both need to be performed by logging in as root on the BBB [ssh root@kamikaze.local]



1) Permissions: The permissions on /etc/redeem and the files within it are incorrect, as such you won't be able to configure, or upload a configuration. To fix this:

# chown -R octo:octo  /etc/redeem



2) Redeem startup: Redeem does not start automatically by default. As a result a restart through the web-interface, or systemctl start redeem needs to be issued from the command prompt. To fix this:

# systemctl enable redeem



reboot your device and you should be good to go :)

# reboot


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/UtiogYHFYr1) &mdash; content and formatting may not be reliable*
