---
layout: post
title: "This has been a long and frustrating process to deal with thus far"
date: February 15, 2016 12:55
category: "Discussion"
author: Tim Curtis
---
This has been a long and frustrating process to deal with thus far. I have my BBB flashed with the latest Kamikazi. In fact at least 4 times so far. I have managed to get my eeprom flashed with the latest (Replicape_00B3.eeprom). I am trying to get into Octoprint to start setting up my printer. I cannot get it to connect from my computer. Here is my current setup. I am using a Windows 7 pc. I have my BBB/Replicape powered up via a 12 volt source at the power in on the replicape. I can connect to my BBB though puTTy with the USB and with a ethernet cable.  I log in under "root" because that's the only way I know how to right now. When connect with google chrome using 192.168.7.2 I get the "It works" screen with the IP address, Including the ethernet IP address. I get the same screen when I log in using the ethernet IP address. The problem is when I click on "Octoprint" I get an unable to resolve host error. Where am I going wrong? I have followed the Kamikazi flash videos and directions to a tee yet can't get past having a black paperweight with flashing blue LEDs. I understand this is still a developing process but the lack of directions to get up and running are a real problem. Obviously these boards work great once you get them running, but getting to that point is very difficult for someone who lacks BBB experience. I'm sure I am doing something wrong but just can't figure it out. While connected with putty I issued a "top" command and can see redeem, octoprint, and toggle all seem to be running (see attached picture). As I said before I'm sure this will be a nice controller board, I just can't get it working. What am I missing here?

![images/3023553f9524eb5143555419d1e3b8a1.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/3023553f9524eb5143555419d1e3b8a1.jpeg)



**Tim Curtis**

---
---
**Elias Bakken** *February 15, 2016 13:13*

Hello Tim, the problem you are describing is indeed one that needs to be resolved, and in fact I have a (hopefully) working version flashing on my Kossel as we speak. In the new version, you are directed to OctoPrint right away. When connecting via an IP address, the "OctoPrint" link does not work, only when accessed with the more friendly "Bonjour" name, kamikaze.local. A workaround is to use the port directly, [http://192.168.7.2:5000](http://192.168.7.2:5000) in your case. 


---
**Tim Curtis** *February 15, 2016 13:15*

Thank you Elias! I will try that when I get home tonight.


---
**Christian Herget** *February 15, 2016 13:47*

At least for me [http://192.168.7.2:5000](http://192.168.7.2:5000) will not work, as the USB to Ethernet SW bridge thning isn't working, which is also listed on [http://wiki.thing-printer.com/index.php?title=Kamikaze#Known_problems](http://wiki.thing-printer.com/index.php?title=Kamikaze#Known_problems). So I'm actually wondering how the SSH connection through this interface is working for Tim.

Nevertheless, no big deal for me as I have connected the physical Ethernet port and this works like a charm. Elias thanks for your hard work!


---
**Tim Curtis** *February 15, 2016 14:03*

@Christain do you mean this will only work through an Ethernet port and not with the USB link? I haven't gotten home to try it yet?


---
**Christian Herget** *February 15, 2016 14:16*

Yes and no, this octoprint URL thing isn't working, but the USB Ethernet bridge isn't working either, these are two separate things.

I allways connect to the BBB via my normal wired ethernet, so for me the URL to access the OctoPrint is [http://192.168.178.45:5000](http://192.168.178.45:5000) but this for sure depends on your environment. As Elias wore the important thing is to add the :5000 after the IP address to connect to the right TCP port on the BBB where the OctoPrint HTTP server is listening.

However, for you [http://192.168.7.2:5000](http://192.168.7.2:5000) sould work, as you are able to connect to the BBB via USB, right? At least your putty screenshot is showing this ip address.

I than edit the printer config file with NANO via SSH (putty) and loaded it with the OctoPrint web interface (Redeem plugin). It tooked me some trial and error, but now I have Replicate Rev. B3 working with my Prusa-i3.

One more thing I noticed is, that the wiring instruction (only the picture) for the stepper motors seems to be wrong, or at least my steppers have different signals associated to different colored wires than shown in the thing wiki.Ah I zoomed into the picture and it is showing a B2 replicape, but the stepper connectors layout changed from B2 to B3 so that's why the picture is wrong!


---
**Elias Bakken** *February 15, 2016 16:29*

Is the Ethernet over USB not working for you guys? I know it says so on the wiki, but I think that has been fixed. I have it working now at least, so if **+Christian Herget** is having trouble with it, I think I'll update the wiki to say "unstable" instead of "not working". 

As for the stepper wires, those should be right, at least they correspond to my steppers, but the stepper manufacturers are often sloppy with the standards on that even changing wire colors from the same manufacturer, I've read. We should double check with one of the more reputable manufacturers, Kysan or Wantai, maybe. 


---
**Tim Curtis** *February 16, 2016 12:35*

Elias, I was able to get to octoprint and connect to the replicape board. I started doing some rough configuration. FYI I am doing this with USB over ethernet. It works great. I'll try to use my network connection later. 


---
**Jon Charnas** *February 17, 2016 10:11*

For those folks with the Ethernet over USB not working, one thing I noticed (if you're running Ubuntu) is that you need to actively enable the connection from the same menu where you would select the wifi connectivity... Can't help people with Windows about it, but just in case you thought something was broken - it's not. Just a bit of futzing around on Ubuntu's part.


---
*Imported from [Google+](https://plus.google.com/106115959585163180149/posts/HgdQL7iJghb) &mdash; content and formatting may not be reliable*
