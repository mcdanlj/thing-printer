---
layout: post
title: "Crazy idea time: I plan to have a heated build chamber, but I got an idea"
date: February 24, 2016 03:25
category: "Discussion"
author: Philip Acierto
---
Crazy idea time:



I plan to have a heated build chamber, but I got an idea.



Instead of using a stand alone temperature controller, could I use replicape and a thermistor located in the printer hooked up to extruder 2/heater h, plus a fan for circulating the air, and control the linked heating element and expect it to work?



At 24v and 20A I want to believe replicape can handle an output of 480w through heater h, but I am not sure I could just wire this heater directly as it says it is a 500W. Is that 20 watts going to trip the fuse - or - I could I just use a relay?



I have a 50A 24v power supply so I got some power to play with. My heated bed is 450W (18A), so I would certainly be using all the power supply can handle with everything pulling a draw, but once up to temp it shouldn't be too bad.



Anyways, trying to avoid more controllers, and waiting for the Reach Rev B to allow for more extruders!



Thoughts?



[http://www.amazon.com/dp/B016EBRRBI/ref=wl_it_dp_o_pC_S_ttl](http://www.amazon.com/dp/B016EBRRBI/ref=wl_it_dp_o_pC_S_ttl)





**Philip Acierto**

---
---
**Daryl Bond** *February 24, 2016 05:26*

I think you would be safest to go with a relay. I always like to leave a bit of wiggle room when it comes to fuses and the like. You never know when a little spike can upset things. Unfortunately you are then left with bang-bang control, which may be fine for a heated chamber as there is a lot of thermal mass that will smooth things out. Replicape + Redeem should be able to handle that setup without any problems.


---
**Philip Acierto** *February 24, 2016 05:28*

Thanks for the input!


---
**Jon Charnas** *February 24, 2016 08:03*

Also with a chamber, the heatbed will also significantly help keep temp up in the chamber, so your chamber heating won't be nearly used as often, and bang-bang should be ok in that use-case...


---
*Imported from [Google+](https://plus.google.com/111974602640488998735/posts/hb9KpS4kFKz) &mdash; content and formatting may not be reliable*
