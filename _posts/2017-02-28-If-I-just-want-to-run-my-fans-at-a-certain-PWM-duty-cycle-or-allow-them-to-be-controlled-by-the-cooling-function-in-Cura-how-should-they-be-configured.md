---
layout: post
title: "If I just want to run my fans at a certain PWM duty cycle, or allow them to be controlled by the cooling function in Cura, how should they be configured?"
date: February 28, 2017 14:52
category: "Discussion"
author: Henrik Söderholtz
---
If I just want to run my fans at a certain PWM duty cycle, or allow them to be controlled by the cooling function in Cura, how should they be configured? Any hints or a link to where it's documented much appreciated!





**Henrik Söderholtz**

---
---
**Jon Charnas** *February 28, 2017 15:05*

Here's the section you want in your local.cfg with examples. In this case fan-1 is controlled by Cura with M106, and fan-3 is started by the temperature rising on the extruder e reaching the threshold temp (default is 60 degrees):



[Cold-ends]

connect-therm-e-fan-1 = False

connect-therm-e-fan-3 = True

add-fan-1-to-m106 = True

add-fan-3-to-m106 = False


---
*Imported from [Google+](https://plus.google.com/103752299402481876381/posts/dSBefXXQHTF) &mdash; content and formatting may not be reliable*
