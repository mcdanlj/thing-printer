---
layout: post
title: "Hi everyone, Poll-time again. And yes, sorry, I know that a lot of you guys are wondering \"where's the next update?\""
date: February 08, 2018 15:49
category: "Discussion"
author: Jon Charnas
---
Hi everyone,



Poll-time again. And yes, sorry, I know that a lot of you guys are wondering "where's the next update?". Well, this kind of relates to that. I'll put another poll up for additional details on that topic.



So, who's actually using the embedded slicers we're shipping with Umikaze/Kamikaze, and which one? 



CuraEngine 15.4 is easy to install as it's a zip to extract and is there, but it's way outdated (like a couple years at least now). Slic3r prusa edition is a lot more recent, but takes upwards of 4 hours and manual intervention to properly compile and install on the images before I package them. Please let us know below so we can slim down and reduce build time according to what you guys use of the features we pack.









**Jon Charnas**

---
---
**James Armstrong** *February 08, 2018 15:54*

Slic3r prusa or off-board would be my choices.


---
**Tim Curtis** *February 08, 2018 15:56*

Off board, There are way to many settings to adjust based to the model being sliced.


---
**Jon Charnas** *February 08, 2018 15:56*

Thanks **+James Armstrong**. 



For those who know of a slicer that can run on a Raspberry Pi with Octoprint other than the two listed above, please let me know in the comments of what it is and if you would use it if we included it!


---
**James Armstrong** *February 08, 2018 16:01*

would be interesting to have a way to remote slice (not on the cloud). I have a DLP slicer that will check to see if you have the slicer also running on a computer on the network and will send the file to be sliced to it and get a zip back. I don't or haven't checked if anything like that is available but I know Slic3r can totally be run on a command line so all we would need is a way to configure and remotely execute it (mainly for speed I guess).



There is also the whole [polar3d.com - Polar Cloud](http://Polar3d.com) way, Dan Newman works for them and was very instrumental on the old sailfish / makerbot stuff. He may be at MRRF again this year.


---
**Jon Charnas** *February 08, 2018 16:44*

So juste so the people who like on board slicing don't despair; if the community votes to remove it, we'll provide scripts and instructions for how to reinstall them in later releases. At the time I'm writing this only 12 people have voted, and I'm sure there's a lot more users out there. So the decision isn't made yet.


---
**Dave Posey** *February 08, 2018 18:56*

I didn't know Slic3r was/would integrate with the Umikaze image...  



Otherwise, I'm with **+James Armstrong** in using Slic3r or Off board.  


---
**Jon Charnas** *February 08, 2018 19:33*

**+Dave Posey** It was added in Umikaze 2.1.1 I think, I'd need to check the 2.1.0 image to see if it's there... The files should be in /usr/src/slic3r/


---
**Philip Acierto** *February 09, 2018 21:05*

I used simplify 3d which I found superior to Cura and Slic3r, but even if it could integrate, as many have said, there are far too many variables you need to review that a drag and drop integrated solution just doesn't do.



Sure.. printing a cube would be fine with just the integrated slicer, but anything complex with manual supports, multi color, multiple printed parts during one print (sequential printing), or a dozen other reasons just makes it niche enough I don't ever use it.


---
**Jon Charnas** *February 09, 2018 23:31*

So in case most of you didn't look, the full featured slicer plugin in octoprint let's you adjust settings on the fly


---
**Heath Riewe** *February 11, 2018 19:57*

Jon, I'm interested in the outcome, but not familiar enough with the options to vote.  It looks like I should spend a little time with Slicer 3d.  The value of having a slicer bundled with the distribution is that it gives newbies less work to do.  Not a very good reason to keep it in the distribution. 

Rock On

Heath


---
**Jon Charnas** *February 14, 2018 09:50*

Well, it looks like the majority has spoken... On-board slicing seems to be a feature desired by a minority. As mentioned above, I will write documentation and scripts to make it easier to install those tools for those who want them, but removing them from the default base image. I'm hoping to be able to create .deb packages or maybe even snaps that will make it trivial to install the slicer(s) you want from among the Slic3r (Prusa) and CuraEngine choices. A bit longer to put in place for Umikaze 2.1.2, but I think it'll be worth it.


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/83vAxkbDw9s) &mdash; content and formatting may not be reliable*
