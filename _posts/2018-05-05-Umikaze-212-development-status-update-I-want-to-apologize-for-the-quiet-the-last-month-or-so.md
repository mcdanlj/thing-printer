---
layout: post
title: "Umikaze 2.1.2 development status update: I want to apologize for the quiet the last month or so"
date: May 05, 2018 11:12
category: "Discussion"
author: Jon Charnas
---
Umikaze 2.1.2 development status update:



I want to apologize for the quiet the last month or so. I haven't forgotten or abandoned the project. But I have encountered a blocking bug for the release. The newer kernel version seems to fail to load on a BBBW, not because of Replicape issues, but because of wifi issues. 



The upstream wifi issue is not trivial to resolve and it's affecting the ability to flash an image on a BBBW, as the problem is from one of the kernel overlays that tells Linux what to find where in terms of hardware. When the overlay fails to load, the Kernel basically starts panicking and keeps trying to find a way to learn how those pins work. So it's not going to flash and it won't run.



I'm seriously stumped as to how to fix the issue, so if anyone has experience dealing with these kinds of issues, please, I welcome the help!



This is, as far as I can tell, the last blocking issue to releasing Umikaze 2.1.2, aside from writing documentation on the new features.





**Jon Charnas**

---
---
**Daniel Kruger** *May 05, 2018 11:30*

**+Jason Kridner** Maybe Jason K can point you in the right direction.


---
**Jason Kridner** *May 05, 2018 11:33*

I’d need more specifics. What bootloader are you using and have you made the jump to u-boot overlays? **+Robert Nelson** Kernel tree, dev tree sources and boot log?


---
**Jon Charnas** *May 05, 2018 11:43*

**+Jason Kridner** I'm working from an RCN flasher image, with his 4.4 bone kernel update (currently 4.4.127) and yes, using uboot overlays, the bb.org-overlays repo fork from ThatWileyGuy on GitHub (on my phone out of the house or I'd link it). I get an fdt error when it tries to apply the bbbw dtbo (again, reference will be added when I get home)


---
**Jon Charnas** *May 05, 2018 18:32*

**+Jason Kridner** now that I'm home, here's the [bb.org](http://bb.org) overlay repo I'm using [https://github.com/ThatWileyGuy/bb.org-overlays](https://github.com/ThatWileyGuy/bb.org-overlays), and here's a link to the image I built: [https://drive.google.com/open?id=1LbHJPhdKzPhZB-mjZjYeGO8Wt2WcW6Jy](https://drive.google.com/open?id=1LbHJPhdKzPhZB-mjZjYeGO8Wt2WcW6Jy) - if you uncomment the flasher you should be able to bring it up fine on BBB and login with root:kamikaze - but you'll see the FTD error when trying to startup on a BBBW with the same SD card


---
**Jon Charnas** *May 10, 2018 10:53*

Managed to get a working image, but no HDMI on it... Kernel is 4.9 ti. HDMI is kind of important to anyone who has a screen plugged in.


---
**Tim Curtis** *May 23, 2018 12:10*

Hi, Is there any kind of release date yet for this release?


---
**Jon Charnas** *May 23, 2018 12:14*

I wish, but sadly not yet. Been swamped at work, worked 2 out of the 3 days of a supposed 3-day weekend, not to mention the weekend before that... I know a couple of other devs are trying to get up to speed on building images, but it will be a bit of time still, unfortunately.


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/Jnra3v7vE5n) &mdash; content and formatting may not be reliable*
