---
layout: post
title: "did 3 point bed leveling and this is the plot graph"
date: January 04, 2017 05:01
category: "Discussion"
author: Step Cia
---
did 3 point bed leveling and this is the plot graph. not really surprised considering I did not do any physical alignment to ensure the x is parallel with the bed surface. so my question, the graph only show area within that triangle. what happen to the area outside that triangle. my bed is square...

![images/4010eb3d3449c1328d01c8f631dc884e.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/4010eb3d3449c1328d01c8f631dc884e.png)



**Step Cia**

---
---
**Jon Charnas** *January 04, 2017 05:12*

Do you have more than 3 points defined in the probing? If not you'll only see those points showing up


---
**Step Cia** *January 04, 2017 05:18*

No it did its job correctly since I only define 3 points the hope is that all 3 point will define the plane surface. But I'm not sure if the calculation takes acount into areas outside the points


---
**Jon Charnas** *January 04, 2017 05:20*

It can't. You can't compute a surface outside the boundaries you give, which are those three points...


---
**Step Cia** *January 04, 2017 05:22*

Hmm makes sense. Are there a way to extrapolate the 3 points to create infinate flat surface? 



Since it only takes 3 points to define a face surface. Right?


---
**Jon Charnas** *January 04, 2017 05:24*

Yes, the plane is defined by those 3 points. It'll do a linear correction and extrapolate the correction to every point of the bed, that's the default. But it won't display more than the triangle in the graph, that's normal


---
**Step Cia** *January 04, 2017 05:27*

Ah got it so it should still print outside those "graphical" triangle. Good. 


---
**Andrew Dowling** *January 06, 2017 22:54*

My theory was use more points. It will average out the error in your bed leveling sensor. Maybe try  one with 9 points or more. Might show that you have a warped bed.


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/MnghHASrX92) &mdash; content and formatting may not be reliable*
