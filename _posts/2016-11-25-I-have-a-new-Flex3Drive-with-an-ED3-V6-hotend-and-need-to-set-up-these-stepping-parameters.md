---
layout: post
title: "I have a new Flex3Drive with an ED3 V6 hotend and need to set up these stepping parameters"
date: November 25, 2016 07:18
category: "Discussion"
author: Antaris Soren
---
I have a new Flex3Drive with an ED3 V6 hotend and need to set up these stepping parameters.

First, what are the setting I need and do they go in the local.cfg of Redeem?





E Axis Microstepping

We recommend that the stepper driver circuit used with Flex3Drive is set to x8

microstepping



E Axis steps per mm

The correct setting for E steps when using the Flex3Drive is 1900 steps per mm



E axis jerk should be set to 0.1mms



E Axis Acceleration

Set this to 50mms if you are running a system that is locked to x16 microstepping otherwise

set this to 100mms if running with x8 microstepping.



E Axis Feed Rate

This is the maximum speed the E axis can run, and whilst normal extrusion speeds are very

low, the biggest impact of this setting is the maximum permissible retact speed that can be

achieved. Set this to 25mms for x16 microstepping and 50mms when using x8

microstepping.





**Antaris Soren**

---
---
**Dan Cook** *November 25, 2016 15:03*

[local.cfg](http://local.cfg)



[Steppers]

steps_pr_mm_e = xxx


---
**Bob McDowell** *November 26, 2016 09:16*

**+Dan Cook** 



Dan



Thanks. So in the local.cfg 



[Steppers]

steps_pr_mm_e = 1900



So will the 1900 setting work?



How do I set microstepping to x8?



Jerk, accel and speed I have configured.



Bob and Erika






---
**Antaris Soren** *November 26, 2016 21:30*

My local.cfg

If I push up the steps_pr_mm_e = 120 any higher my stepper motor coughs.



[Homing]

home_x = 0

home_y = 0

home_z = 0



[Steppers]

direction_e = -1

steps_pr_mm_e = 120

current_z = 1.0

current_e = 1.1



[Cold-ends]

connect-therm-e-fan-0 = True


---
**Dan Cook** *November 26, 2016 22:02*

**+Bob McDowell**​ I'm not sure about the x8... plug in the 1900 and then calibrate a length of filament with a caliper or ruler.


---
**Bob McDowell** *November 29, 2016 06:57*

When I use any setting above steps_pr_mm_e = 120 the motor stalls. Even when I use a unattached stepper motor. Tried it on 3 different stepper motors. I tried turning down the current_e to various smaller values, no joy. HELP! By the way, the Flex3Drive is really cool and the support from Jason is supberb. 


---
**Daniel Kusz** *December 01, 2016 10:19*

Maybe try to make 'jerk' higher. 


---
**Daniel Kusz** *December 01, 2016 10:21*

What is your speed for stepper e?


---
*Imported from [Google+](https://plus.google.com/115412029632663838329/posts/EaLow7FNEDH) &mdash; content and formatting may not be reliable*
