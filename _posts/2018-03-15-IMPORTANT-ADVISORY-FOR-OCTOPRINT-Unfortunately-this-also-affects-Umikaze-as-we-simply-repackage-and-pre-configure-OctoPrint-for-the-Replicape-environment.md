---
layout: post
title: "IMPORTANT ADVISORY FOR OCTOPRINT Unfortunately this also affects Umikaze as we simply repackage and pre-configure OctoPrint for the Replicape environment:"
date: March 15, 2018 11:24
category: "Discussion"
author: Jon Charnas
---
IMPORTANT ADVISORY FOR OCTOPRINT



Unfortunately this also affects Umikaze as we simply repackage and pre-configure OctoPrint for the Replicape environment:

[https://octoprint.org/blog/2018/03/15/security-issue-update-to-1.3.6/](https://octoprint.org/blog/2018/03/15/security-issue-update-to-1.3.6/)





**Jon Charnas**

---
---
**Jon Charnas** *March 15, 2018 12:26*

Please note that if you have difficulty with the update system for OctoPrint, you should ask support in this community first - it may be a Kamikaze/Umikaze specific change giving you trouble.


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/gCVRMA4inFC) &mdash; content and formatting may not be reliable*
