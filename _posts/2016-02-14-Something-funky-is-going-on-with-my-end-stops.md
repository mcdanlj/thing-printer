---
layout: post
title: "Something funky is going on with my end-stops"
date: February 14, 2016 03:17
category: "Discussion"
author: Daryl Bond
---
Something funky is going on with my end-stops. I am trying to use X2, Y2 and Z2. X2 and Z2 work fine, Y2 is triggering an event that is picked up in python (I get a "End Stop Y2 hit!", i.e. event.code = 115) but if I inspect the PRU according to "state = PruInterface.get_shared_long(0)" I get "state =  0b0". This should be 0b10000. Inspecting all the other end stop pins with PruInterface gives me the expected values ranging from 0b1 to 0b100000.



The end result is that while python is telling me that I have hit an end stop, the corresponding axis is not being masked off by the step generator and so unhappy things happen. Any ideas what is going on?





**Daryl Bond**

---
---
**Elias Bakken** *February 14, 2016 12:47*

What versjon of redeem? Use M115 to see it. Make sure you have 1.1.7. 

There is also a tool that will show you the raw end stop outputs: [https://bitbucket.org/intelligentagent/redeem/src/25e850dbd1031281e399415428190337a37b6c5d/tools/endstop_test.py?at=develop&fileviewer=file-view-default](https://bitbucket.org/intelligentagent/redeem/src/25e850dbd1031281e399415428190337a37b6c5d/tools/endstop_test.py?at=develop&fileviewer=file-view-default)


---
**Daryl Bond** *February 14, 2016 18:37*

I am using the latest version of the develop branch. That tool was the first thing I used to inspect the raw end stop values. Then I hacked together some of the pru code from Endstop.py as another check and got the same thing. I've changed to using X1 Y1 Z1, which work fine, but it is something that is a bit of a worry.﻿


---
**Elias Bakken** *February 14, 2016 18:45*

I did a quick test now, and checked the output from Y2; it does report it in the end stop tool and also stops the right axis if configured with M574. Did you not see the change using the end stop tool?


---
**Daryl Bond** *February 15, 2016 00:32*

Thanks for checking on your hardware. I used the end stop tool and triggered all the end stops in sequence and get the following X1:0b1, Y1:0b10, Z1:0b100, X2:0b1000, Y2:0b0, Z2:0b100000. So it appears there is no response registered by the PRU. Yet there is still an event registered in /dev/input/event0 leading to a notification in the log that Y2 has been hit. I am seriously confused about what is happening!


---
**Elias Bakken** *February 15, 2016 00:34*

When the firmware is compiled, there is a file called /tmp/config.h that might be worth a look!


---
**Daryl Bond** *February 15, 2016 13:52*

Just had a look in /tmp/config.h. My endstop configuration is:

"""

invert_X1 = True

invert_Y1 = True

invert_Z1 = True



invert_X2 = False

invert_Y2 = False

invert_Z2 = False



end_stop_X1_stops =

end_stop_Y1_stops =

end_stop_Z1_stops = x_neg, y_neg, z_neg



end_stop_X2_stops = x_pos

end_stop_Y2_stops = y_pos

end_stop_Z2_stops = z_pos

"""



and the config.h file, after a restart of Redeem to make sure the firmware is being re-compiled, has the following:



"""

#define INVERSION_MASK          0b00000111

#define STEPPER_MASK_Y2         0b0000001000000000

#define STEPPER_MASK_Y1         0b0000000000000000

#define STEPPER_MASK_X2         0b0000000100000000

#define STEPPER_MASK_X1         0b0000000000000000

#define STEPPER_MASK_Z1         0b0000000000000111

#define STEPPER_MASK_Z2         0b0000010000000000

"""



So, to my eye, this all looks good.



I am wondering, however, why I have



#define STEPPER_Y2_END_PIN              17

#define STEPPER_Y1_END_PIN              17



should both of these be 17?


---
**Daryl Bond** *February 15, 2016 14:23*

I have also used the following python code to check the inputs via evdev:

"""

import evdev



device = evdev.InputDevice('/dev/input/event0')



print(device)



for event in device.read_loop():

    if event.type == evdev.ecodes.EV_KEY:

        print(event)

"""



Using this script the event codes are triggered correctly for all end stops as listed under keycodes in default.cfg.



I am thinking that /dev/input/event0 is not fed by the PRU and is therefore working? Any further things I should check?



By the way, big thanks Elias for the quick and helpful interaction, huge kudos!


---
**Elias Bakken** *February 15, 2016 14:57*

The thing is that there is no connection between the evdev and that of the PRU. One is simply used to notify the user/alarm. The other will block PRU 0 from doing anything, but it will not give an event back to "user space". Could you double check that the GPIO bank and pin  is right in the  config.h? If I'm not mistaken, Y2 has been problematic before... I have bank 3, pin 17 which is working.


---
**Daryl Bond** *February 16, 2016 11:00*

Yep, I also have bank 3 pin 17



#define STEPPER_Y2_END_PIN              17

#define STEPPER_Y2_END_BANK             GPIO_3_IN

#define STEPPER_Y1_END_PIN              17

#define STEPPER_Y1_END_BANK             GPIO_1_IN

#define STEPPER_X2_END_PIN              30

#define STEPPER_X2_END_BANK             GPIO_0_IN

#define STEPPER_X1_END_PIN              21

#define STEPPER_X1_END_BANK             GPIO_3_IN

#define STEPPER_Z1_END_PIN              31

#define STEPPER_Z1_END_BANK             GPIO_0_IN

#define STEPPER_Z2_END_PIN              4

#define STEPPER_Z2_END_BANK             GPIO_0_IN



Is there a way to check all GPIO pins so I can see what is triggered when I activate my end stop? 



My usual job is coding fluid simulation software so all this hardware interfacing stuff is a bit new...


---
**Elias Bakken** *February 16, 2016 11:18*

You could try looking at it from sysfs. /sys/class/gpio. You need to export the pin first.


---
**Daryl Bond** *February 16, 2016 12:19*

SUCCESS!!



If anyone else has this issue here is what I did.



I called the following command:



$ cat /sys/kernel/debug/gpio



and observed that  end stop Y2 was mapped to gpio-51. This corresponds to bank 1, pin 19 (banks increment by 32 so 51 - 32 = 19). I then added "pin_Y2 = GPIO1_19" to my .cfg file (changed from GPIO3_17), and re-started Redeem to re-compile the PRU firmware. I then ran the endstop_test.py script that is in the tools folder and checked all my end stops again. End stop Y2 (and all the others) are now registering correctly.



The following sites helped me once Elias pointed me in the direction of sysfs.



[http://falsinsoft.blogspot.com.au/2012/11/access-gpio-from-linux-user-space.html](http://falsinsoft.blogspot.com.au/2012/11/access-gpio-from-linux-user-space.html)



[http://www.armhf.com/using-beaglebone-black-gpios/](http://www.armhf.com/using-beaglebone-black-gpios/)



Many thanks Elias.




---
**Elias Bakken** *February 16, 2016 14:02*

Great! But how did it work for me? From the pinout, Y2 is definitely mapped to GPIO3_17. This is BeageBone Black, right?


---
**Daryl Bond** *February 17, 2016 01:58*

Yes, BeagleBone Black rev C with Replicape A4A. I am also running the latest Kamikaze image.



I can't remember if it was working with the default settings previously as I have only recently got back into printing.


---
**Daryl Bond** *February 17, 2016 02:19*

I just had a look in the device tree overlay and I am not sure how yours is working!



0x04C 0x37 // P9_16 = End stop Y2 = GPIO1_19



and



switch_y2 { label = "Replic:End-stop-Y2"; debounce_interval = <50>; linux,code = <115>; gpios = <&gpio1 19 0x5>; gpio-key,wakeup; };			





From: [https://bitbucket.org/intelligentagent/replicape/raw/bf08295bbb5e98ce6bff60097fe9b78d96002654/Device_tree/3.12/BB-BONE-REPLICAP-0A4A.dts](https://bitbucket.org/intelligentagent/replicape/raw/bf08295bbb5e98ce6bff60097fe9b78d96002654/Device_tree/3.12/BB-BONE-REPLICAP-0A4A.dts)


---
**Elias Bakken** *February 17, 2016 03:06*

Ah! Well that explains it rev A4A... 


---
*Imported from [Google+](https://plus.google.com/104347555452272558593/posts/49Gh1Y9HtAM) &mdash; content and formatting may not be reliable*
