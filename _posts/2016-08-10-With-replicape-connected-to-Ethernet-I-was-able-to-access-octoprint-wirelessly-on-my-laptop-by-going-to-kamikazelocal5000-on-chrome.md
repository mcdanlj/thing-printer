---
layout: post
title: "With replicape connected to Ethernet, I was able to access octoprint wirelessly on my laptop by going to kamikaze.local:5000 on chrome"
date: August 10, 2016 15:56
category: "Discussion"
author: Step Cia
---
With replicape connected to Ethernet, I was able to access octoprint wirelessly on my laptop by going to kamikaze.local:5000 on chrome. But I could not get the same result if I try on tablet or phone... Strange... It shouldn't matter what device I'm on as long as it's on the same network right?





**Step Cia**

---
---
**Jon Charnas** *August 10, 2016 16:12*

The .Local suffix is from the bonjour protocol, if your phone doesn't use it it won't work. But your router might have a DNS and give you a host name locally instead. Use that and no need for the :5000 port number


---
**Step Cia** *August 10, 2016 19:35*

When I tried without :5000 on my laptop it actually just gives me a Google search...


---
**Jon Charnas** *August 10, 2016 20:07*

Try putting http:// in front. Which version of kamikaze are you running?


---
**Step Cia** *August 15, 2016 04:36*

Thanks local host works also with http infront works as well :)


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/NeqQvkQwNPW) &mdash; content and formatting may not be reliable*
