---
layout: post
title: "I bought my replicape about a year ago, I now have the time to finish off my printer, I got to the point where i flashed the bbb with kamikaze"
date: January 07, 2017 01:34
category: "Discussion"
author: KairuFPV “Deltym”
---
I bought my replicape about a year ago, I now have the time to finish off my printer,  I got to the point where i flashed the bbb with kamikaze. whats the next step after that? Do i configure everything from octoprint? In octoprint i dont see anything for configuration. Do i need to have it powered up through 12v to communicate with it?







**KairuFPV “Deltym”**

---
---
**KairuFPV “Deltym”** *January 07, 2017 02:50*

I'm going to update kamikaze I think it's still on 1.0


---
**Step Cia** *January 07, 2017 03:57*

The general big steps for me has been:



1. Flash latest stable kamikaze

2. Configure your printer profile. Just dont change anything from default config

3. Ive had things go wrong and had to repeat step 1 many time...


---
**KairuFPV “Deltym”** *January 07, 2017 03:58*

Is 2.1 considered stable?


---
**Step Cia** *January 07, 2017 03:59*

I havent tried that. Im on 2.0.8


---
**Step Cia** *January 07, 2017 04:00*

I also find plenty of information that is not either in this g+ nor wiki. Try to join replicape slack the search function is better


---
**KairuFPV “Deltym”** *January 07, 2017 04:01*

Okay thanks. Do I have to supply 12v power to the replicape while configuring?


---
**Per Arne Kristiansen** *January 07, 2017 14:08*

5v on bb or 12-24 on replicape 


---
*Imported from [Google+](https://plus.google.com/108846290422025907415/posts/TKRGESzpuKm) &mdash; content and formatting may not be reliable*
