---
layout: post
title: "Question about SD cards, Can the beaglebone black Micro SD card slot be used by Octoprint to read and run print files?"
date: February 16, 2016 13:52
category: "Discussion"
author: Tim Curtis
---
Question about SD cards, Can the beaglebone black Micro SD card slot be used by Octoprint  to read and run print files? 





**Tim Curtis**

---
---
**Elias Bakken** *February 16, 2016 13:57*

If there is no bootloader on the SD card, it should be possible to use it as a regular SD card yes. I'm not certain, but it would surprise me if there is no event fired by interting the SD card that can be used to mount it as a partition. Another option is to have a USB dongle, that would also work. 


---
**Tim Curtis** *February 16, 2016 14:45*

Thanks Elias. If I use a USB dongle (Thumb Drive) do I have to do any pre-programming to the BBB or does it already know how to handle a USB drive?


---
**Jon Charnas** *February 16, 2016 21:04*

**+Tim Curtis** Kamikaze is a linux image, so it should be able to handle a USB disk by default. Octoprint may not, however, see the folder it mounts to by default. Just make sure your USB disk is in a standard file system format (FAT32 is your safest bet) and you should be ok. With the new octoprint you can set folders in Octoprint to save your uploads to. My suggestion would be to make a symlink using ln -s in the uploads folder of octoprint that points to the USB drive or SD card mount so you can then go into the folder in octoprint and read the files there. You just need to make sure that the permissions on the SD card/USB drive folder let the user octoprint runs access them...


---
**Tim Curtis** *February 17, 2016 12:24*

Jon Charnas Thanks for the info. I will give it a try.


---
**Jon Charnas** *February 18, 2016 10:16*

**+Tim Curtis** any luck with the symlink? I haven't had a chance to try yet.


---
**Tim Curtis** *February 18, 2016 15:59*

**+Jon Charnas** Sorry I didn't get a chance to try it. I'm still working on configuring redeem.


---
**Jon Charnas** *February 18, 2016 16:01*

**+Tim Curtis** No problem. I'm suffering from a bad episode of wrist strain this week so I'm keeping my "off hours" typing to a minimum until it resolves... Luckily swipping on a smartphone with the other hand works for G+ ;)


---
*Imported from [Google+](https://plus.google.com/106115959585163180149/posts/DpYYAA71PB8) &mdash; content and formatting may not be reliable*
