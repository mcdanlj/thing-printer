---
layout: post
title: "Quick couple questions on the Replicape. When the steppers are not moving but powered to keep position, I seem to get a bit of whining or hissing from the steppers"
date: April 09, 2016 20:47
category: "Discussion"
author: Pieter Koorts
---
Quick couple questions on the Replicape. When the steppers are not moving but powered to keep position, I seem to get a bit of whining or hissing from the steppers. It does slowly go away at lower currents of around 0.2 or less amps but then I get skipped steps because it can't reverse direction to fast. 



As you turn the current up, it becomes louder and clearly audible at 0.5 amps and above. I have turned micro stepping to 8 (max) and slow decay to 7 (max) and although it gets better it can still be heard quite clearly. However when the steppers are in constant motion it seems to have less of an effect. Is this normal or can it be mitigated some way? I currently use 0.9 degree steppers from RobotDigg.



Another thing, in an effort to reduce build height usage and add some cooling, is it possible to use ribbon cables to attach the Replicape to the BeagleBone? Something like having the BeagleBone and the Replicape sitting side by side and not on top of each other. Does anyone foresee any problems like EM noise causing print issues (myself not being an electrical engineer)?





**Pieter Koorts**

---
---
**Daryl Bond** *April 09, 2016 22:52*

Are you adjusting the steps and decay using the sliders in octoprint? I was having the same issue with hissing when I was adjusting in octoprint. Moving the sliders made minimal difference. I then changed the stepping in my config file and the hissing went away.


---
**Pieter Koorts** *April 09, 2016 22:55*

Originally I did use the OctoPrint interface as quick adjustment but I have since placed it into the Redeem config for my printer. This makes no difference. It's not like the default high pitched hiss or whine more but rather lower sounding when the micro steps and decay are on max. I will see if I can record it.


---
**Elias Bakken** *April 10, 2016 00:28*

Some steppers are more noisy than others, but play around with both current and decay, until you find something suitable. I find that the current settings is not linear with regards to noise.


---
*Imported from [Google+](https://plus.google.com/100077479073911242630/posts/Me2Lt9q2GNn) &mdash; content and formatting may not be reliable*
