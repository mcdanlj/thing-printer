---
layout: post
title: "I was next to John at MRRF and hooked him up with a Replicape"
date: May 22, 2017 15:41
category: "Discussion"
author: Elias Bakken
---
I was next to John at MRRF and hooked him up with a Replicape. He has some interesting comparison pictures between Replicape and Smoothieboard too. Fisrt time I've seen direct comparisons. Care to share them **+John Pickens**?



<b>Originally shared by John Pickens</b>



Woodstock Delta Printer with Replicape/Beaglebone Black







**Elias Bakken**

---
---
**John Pickens** *May 22, 2017 18:18*

Sure thing, Elias.

As I said in the video, the Replicape drives the steppers much quieter than either Ramps 1.4 or Panucatt Azteeg Mini Smoothiware board. Both of those controllers use Polulu-style stepstick stepper drivers, and they print with the typical "sing song" sounds coming from the steppers.



Now the Replicape is a completely different animal.  The steppers are nearly silent in operation, though I do get a high pitched "whisper" from them when they are active.  It makes the printer MUCH quieter in  operation, and, I believe, causes less harmonic vibration to be transferred from the "singing" steppers, through the printhead, to the layers of plastic in the print.



What all this means is that the Replicape gives a much smoother layer printed suface than the other two print systems.



Below are two 3DBenchy models printed with identical Slic3R settings and speeds.  The Smoothiebware print has Moire' patterns in the layers, and the Replicape does not.



First Photo: The Benchy on the Right is the Smoothieware print, Replicape on the Left

![images/a4b8c2a742023e3f4140733fc3834ad2.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/a4b8c2a742023e3f4140733fc3834ad2.jpeg)


---
**John Pickens** *May 22, 2017 18:19*

Second photo, the Benchy on the Left is the Smoothie, Right is the Replicape.  You can easily see the vibration effects.

![images/24dc0038b732a3c291aa920794e26e8c.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/24dc0038b732a3c291aa920794e26e8c.jpeg)


---
**Step Cia** *May 22, 2017 20:29*

I second the smoothness print I get with replicape. They all came out soo smooth that little defect or imperfection are now quite easy to spot...


---
**Klipper Pressure Advance** *May 23, 2017 00:47*

I am guessing that  the Panucatt Azteeg Mini Smoothiware board was not using TMC2100 drivers so really not a apple for apples comparison and if infact the Panucatt Azteeg Mini Smoothiware board was using the DRV8825 stepper drivers ....there is the moire pattern culprit. so it is more of a comparison of stepper drivers not boards imho  ...can you give details of the steppers drivers used please just for full disclosure sake?


---
**Step Cia** *May 23, 2017 00:53*

He actually did mentioned both ramp and azteeg run pololu style driver..... 


---
**Klipper Pressure Advance** *May 23, 2017 03:16*

yes he did mention pololu style drivers but that is just a pinned style module that can have a multitude of difference drivers in that pololu style driver board ie  A4988 , DRV8825 TMC2100, TMC2360, A5984 and the list goes on  ....an for the record I agree the TMC2100 produce quiet smooth results 


---
**John Pickens** *May 23, 2017 11:52*

I used the A5984 driver option. They have Digipot software current control.

From the Panucatt website:

   "SD5984 - Based on allegro's new A5984 chips. Quieter and smoother than most drivers, 1/32 microstepping, 2 Amps 40V max. With Adaptive Percent Fast Decay feature."



I did not see any noise reduction compared to inexpensive RAMPS 1.4 boards with whatever generic drivers they ship with. 


---
**John Pickens** *May 23, 2017 12:02*

But I should add that the Panucatt Azteeg X5 Mini V3 did alot of things very well. It is able to handle high print speeds with Delta kinematics without stuttering, like the RAMPS boards. It also is extremely easy to set up, with a single config file which autoloads off micro SD card. Easy to tell what your configuration is, easy to edit, and easy to copy between machines.  



Right now, I have to put a 10 second buffer in the Replicape config to prevent Delta stuttering, which throws off the synchronization of the parts cooling fan and layer temperature changes. But I understand that is being fixed in development, so I'm happy with the Replicape.


---
**Ryan Carlyle** *May 23, 2017 14:35*

The lines on the Azteeg print are most likely caused by the stepper drivers interacting with the delta kinematics. Although I would expect the 5984 to give good results, not like an 8825. This is a good discussion thread on the topic: [forums.reprap.org - Removing vertical lines on prints](http://forums.reprap.org/read.php?178,763881)



There has also been some proposals that the use of segmentation for delta kinematics can contribute to these ripples, but I don't know offhand if Replicape uses segmentation. **+Elias Bakken**?


---
**Elias Bakken** *May 23, 2017 17:54*

Replicape uses fixed length segments for the delta, standard is 0.1mm, but it can be set in config. Slower processors usually split less the faster they print, which almost certainly contributes to worse prints. 


---
**Klipper Pressure Advance** *May 23, 2017 22:44*

Replicape Develop software is not using segmentation ie smooth-delta merge Elias....I am also susprised that the 5984 has similar moire effect as DRV8825  


---
**John Pickens** *May 23, 2017 22:54*

**+Ryan Carlyle** 

Thanks for the link. More information than I can handle at once, but after reading it, I have an observation.



The 3DBenchys in my photos were printed at 0.15mm layer height. However, when I print with the Smoothie board at 0.2mm layers, I don't​ see anywhere near as much banding. I wonder if some of the issues in your link are at play here.


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/5VwC3sWZnm4) &mdash; content and formatting may not be reliable*
