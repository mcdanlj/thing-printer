---
layout: post
title: "Saw this and got me thinking. Is there a 3D model for replicape?"
date: November 24, 2016 07:34
category: "Discussion"
author: Step Cia
---
Saw this and got me thinking. Is there a 3D model for replicape?



<b>Originally shared by Eric Lien</b>



I have been working with **+Ray Kholodovsky** over the last few weeks making him 3D models and 3D renders of his two new Smoothieware based controllers (Cohesion3D Mini, and the Cohesion3D ReMix). It has been a fun project.



While we were working on it I also tested one of the Mini controllers in my Talos3D Tria Delta printer. The controller has been running great, and I am excited to have a controller with socketed drivers on the Delta so I can try different drivers. 



Ray just released his ReMix, and I wish him the best of luck with sales of the boards. He has worked long hours and very hard on both of these designs. 



In the pictures tied to this post you can see the Mini installed on my Delta, as well as renders of the 3D models, and the beginning work on a compact enclosure for the ReMix (it's a work in progress and not finished, I am going back to screw on lid and ditching the tool-less tab locking design).



![images/5176d62c7aae543c22c468c11d0584f0.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/5176d62c7aae543c22c468c11d0584f0.png)
![images/2a81bc03521d3e197e76648e85075a21.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/2a81bc03521d3e197e76648e85075a21.png)
![images/2847b2aa858b35cb51f299a5a51cbe6d.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/2847b2aa858b35cb51f299a5a51cbe6d.jpeg)
![images/60a30ff8ac6ad978b1cd54c243202d06.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/60a30ff8ac6ad978b1cd54c243202d06.png)
![images/fe8c8c4ca2ea89dc2523c4b2266f8389.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/fe8c8c4ca2ea89dc2523c4b2266f8389.png)
![images/c2462698a471dfd15d5214b848f04901.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/c2462698a471dfd15d5214b848f04901.png)
![images/a61d9a02b84583ae7825b6a4a06c8491.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/a61d9a02b84583ae7825b6a4a06c8491.png)

**Step Cia**

---
---
**Jon Charnas** *November 24, 2016 07:52*

Not sure, but there's already a few enclosures out...


---
**Christian Herget** *November 24, 2016 09:27*

There is SketchUp model in the GIT for RevB2: [bitbucket.org - intelligentagent / Replicape 
  / source  / Models 
 — Bitbucket](https://bitbucket.org/intelligentagent/replicape/src/73a64999a7299aede141204b4abcf289d56f63d8/Models/?at=Rev-A4)


---
**Step Cia** *November 24, 2016 09:47*

Thanks. I have something different in mind than just enclosure :)


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/ZK1hUC4ooKg) &mdash; content and formatting may not be reliable*
