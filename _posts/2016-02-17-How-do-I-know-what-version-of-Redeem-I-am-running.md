---
layout: post
title: "How do I know what version of Redeem I am running?"
date: February 17, 2016 21:03
category: "Discussion"
author: Veridico Cholo
---
How do I know what version of Redeem I am running?

M115 outputs this:



Send: M115

Recv: ok PROTOCOL_VERSION:0.1 FIRMWARE_NAME:Redeem FIRMWARE_URL:http%3A//[wiki.thing-printer.com/index.php?title=Replicape](http://wiki.thing-printer.com/index.php?title=Replicape) MACHINE_TYPE:Mendel EXTRUDER_COUNT:2





**Veridico Cholo**

---
---
**Hitbox Alpha** *February 17, 2016 21:34*

Can you ssh into the BBB? 


---
**Veridico Cholo** *February 17, 2016 21:36*

yes I can


---
**Hitbox Alpha** *February 17, 2016 21:49*

Systemctl status redeem


---
**Elias Bakken** *February 17, 2016 22:12*

It's most likely an older version then, 1.1.5 perhaps?


---
**Veridico Cholo** *February 18, 2016 00:09*

Here is what I get:

root@kamikaze:~# systemctl status redeem

* redeem.service - The Replicape Dameon

   Loaded: loaded (/lib/systemd/system/redeem.service; enabled)

   Active: active (running) since Thu 2016-02-18 00:04:55 UTC; 3min 48s ago

 Main PID: 278 (redeem)

   CGroup: /system.slice/redeem.service

           |-278 /usr/bin/python /usr/bin/redeem

           |-522 socat -d -d -lf /var/log/redeem2octoprint pty,mode=777,raw,echo=0,link=/dev/octoprint_0 pty,mode=777,raw,echo=0,link=/dev/octoprint_1

           |-524 socat -d -d -lf /var/log/redeem2toggle pty,mode=777,raw,echo=0,link=/dev/toggle_0 pty,mode=777,raw,echo=0,link=/dev/toggle_1

           |-526 socat -d -d -lf /var/log/redeem2testing pty,mode=777,raw,echo=0,link=/dev/testing_0 pty,mode=777,raw,echo=0,link=/dev/testing_1

           `-528 socat -d -d -lf /var/log/redeem2testing_noret pty,mode=777,raw,echo=0,link=/dev/testing_noret_0 pty,mode=777,raw,echo=0,link=/dev/testing_noret_1



Feb 18 00:05:08 kamikaze redeem[278]: 02-18 00:05 root         INFO     Added fan 0 to M106/M107

Feb 18 00:05:08 kamikaze redeem[278]: 02-18 00:05 root         INFO     Ethernet bound to port 50000

Feb 18 00:05:08 kamikaze redeem[278]: 02-18 00:05 root         INFO     Pipe octoprint open. Use '/dev/octoprint_1' to communicate with it

Feb 18 00:05:09 kamikaze redeem[278]: 02-18 00:05 root         INFO     Pipe toggle open. Use '/dev/toggle_1' to communicate with it

Feb 18 00:05:09 kamikaze redeem[278]: 02-18 00:05 root         INFO     Pipe testing open. Use '/dev/testing_1' to communicate with it

Feb 18 00:05:09 kamikaze redeem[278]: 02-18 00:05 root         INFO     Pipe testing_noret open. Use '/dev/testing_noret_1' to communicate with it

Feb 18 00:05:09 kamikaze redeem[278]: 02-18 00:05 root         INFO     Redeem ready

Feb 18 00:07:16 kamikaze redeem[278]: 02-18 00:07 root         WARNING  Heater time update large: H temp: 0.0 time delta: 110.6438241

Feb 18 00:07:16 kamikaze redeem[278]: 02-18 00:07 root         WARNING  Heater time update large: E temp: 21.4333 time delta: 110.643247128

Feb 18 00:07:16 kamikaze redeem[278]: 02-18 00:07 root         WARNING  Heater time update large: HBP temp: 21.4333 time delta: 110.893690109


---
**Hitbox Alpha** *February 18, 2016 00:10*

Apt-get update Apt-get install redeem


---
**Elias Bakken** *February 18, 2016 00:12*

Needs moar lines! Add "-n100" to see the whole output. Should be the first log line.


---
**Veridico Cholo** *February 18, 2016 00:48*

Yes, it was old. Thanks Zachary Jeffrey. Updated to 1.1.8 and I couldn't start redeem because of an error due to the motor decay values in my printer config file that were set to "False". This version of redeem was expecting "0" instead. 


---
*Imported from [Google+](https://plus.google.com/109713994627754483735/posts/8vrj2dH6uuh) &mdash; content and formatting may not be reliable*
