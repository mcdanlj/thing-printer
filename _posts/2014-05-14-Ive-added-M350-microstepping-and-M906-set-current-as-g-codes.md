---
layout: post
title: "I've added M350 (microstepping) and M906 (set current) as g-codes"
date: May 14, 2014 19:50
category: "Discussion"
author: Elias Bakken
---
I've added M350 (microstepping) and M906 (set current) as g-codes. Also, three virtual TTY pipes are created from Redeem when it starts up, one for Octoprint, one for Toggle and one for Testing. The latter does not send a response through the channel and so it can actually be used to pipe output directly from CuraEngine, like this:

CuraEngine treefrog.stl > /dev/testing_1



Due to a version name change, the latest redeem has to be installed in a special manner. The only way I've found is to download the file locally and then install it. Details here:

[http://wiki.thing-printer.com/index.php?title=Replicape#Upgrading_to_the_latest_Redeem](http://wiki.thing-printer.com/index.php?title=Replicape#Upgrading_to_the_latest_Redeem)



Finally, I've added the testing-script for Replicape to the package feed so if anyone wants to test their board, its available.





**Elias Bakken**

---
---
**James Armstrong** *May 14, 2014 19:52*

I got redeem working a while ago (just test loading) on angstrom and now that beagle bones are shipped with Debian will the packages or instructions work for that also?


---
**Elias Bakken** *May 14, 2014 20:00*

James, I think it would be a good idea to get Redeem working on the new Debian image! I have not had a chance to look at it at all, but I know Jason and Robert have been doing a lot of work on it, so It's probably way more up to date than Angstrom. My only concern is the build system. I have not done any research into it yet, but one of the things I love about Angstrom is that it's built on Yocto. I'll have to search around to find out : )


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/QSaxhhqaFWx) &mdash; content and formatting may not be reliable*
