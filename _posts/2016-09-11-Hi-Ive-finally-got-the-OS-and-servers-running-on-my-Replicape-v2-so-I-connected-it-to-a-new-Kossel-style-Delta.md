---
layout: post
title: "Hi, I've finally got the OS and servers running on my Replicape v2, so I connected it to a new Kossel-style Delta"
date: September 11, 2016 17:31
category: "Discussion"
author: Dana Basken
---
Hi, I've finally got the OS and servers running on my Replicape v2, so I connected it to a new Kossel-style Delta. I was able to get motion, but after many many hours of testing different settings, it appears the X driver is faulty. If I perform a home (for example), the X stepper usually makes it to the top, but sometimes doesn't. If it makes it to the top, sometimes it doesn't move after that (along with the Y and Z). I thought there was something wrong with my linear rails, so I bought new ones and tested them. Then I thought there was something wrong my belt setup, but after adjusting that I see the same issue (and the other axis work perfectly). So finally I figured it must be my stepper motor, but I tried switching the board's X driver to a different motor, and now that motor is showing the same issue as the X, but the X motor works fine. In other words, it appears that the X driver on the board has a fault.



Any ideas?





**Dana Basken**

---
---
**Elias Bakken** *September 11, 2016 17:47*

That sounds like a faulty X-driver, yes. Could you connect the X-stepper to the H-driver and set it up in slave mode? That should work for now, at least for one extruder. If that works, I can ship a new cape once the new ones come in! Sorry you had to go through all that trouble!


---
**Dana Basken** *September 11, 2016 17:51*

I actually started to do that, but wasn't sure how to make H be slave to X? In the config, i mean.


---
**Elias Bakken** *September 11, 2016 17:53*

There is some documentation on that in the default.cfg file: [bitbucket.org - intelligentagent / Redeem 
  / source  / configs / default.cfg
 — Bitbucket](https://bitbucket.org/intelligentagent/redeem/src/938649ebb4dd77a476b9016f767476dfc9cbbe72/configs/default.cfg?at=master&fileviewer=file-view-default#default.cfg-146)




---
**Dana Basken** *September 11, 2016 18:48*

I plugged the X stepper motor into the H driver on the board, and copied all the *_x settings to *_h, then added slave_x=H as suggested in the comments in default.cfg. No motion from the X stepper motor. Do I need to enable slave mode somewhere else?


---
**Dana Basken** *September 11, 2016 19:04*

Actually, it looks like it's trying to do some kind of motion using H, but I'm getting "Error accessing 0x70: Check your I2C address" when I try to do any kind of move with the slave setup.


---
**Edwardo Garabito** *September 27, 2016 23:07*

Im wondering if this is the same problem ive been having. For me every time I try to send the command t move my z stepper the motor wont respond as well as the x, and y.  These are the exact same brand of motors, running the same amount of current and decay. Yet the other 2 work perfectly. Ive even tried different currents and settings. Even a different motor, as well as switching out motor, into the z axis drive, and still the same. 


---
**Edwardo Garabito** *September 27, 2016 23:08*

Check this out it shows the problem ive been having and leaving me frustrated. 

![images/6dfac6422c8431ed69a1007c9ae14031](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/6dfac6422c8431ed69a1007c9ae14031)


---
**Dana Basken** *September 29, 2016 00:10*

**+Edwardo Garabito**: that doesn't seem like the same issue I'm having. If I had to guess, I'd say your Z speed is just set lower than your X and Y speed.



In my case, the X axis would just stop working, and I confirmed the X driver is faulty by switching it to another, known-good, stepper motor and it showed the same faulty behavior. **+Elias Bakken** told me to switch to one of the other axis drives, but I haven't been able to get the configs working, so I'm dead in the water.


---
**Elias Bakken** *September 29, 2016 11:03*

**+Edwardo Garabito** that is a setting in Octoprint itself! Go into settings and then printer settings in OctoPrint to change it. You might want to switch to having the origo in the center of the bed as well while you are in there!


---
*Imported from [Google+](https://plus.google.com/114035164852620518539/posts/ddHTMhuwFZp) &mdash; content and formatting may not be reliable*
