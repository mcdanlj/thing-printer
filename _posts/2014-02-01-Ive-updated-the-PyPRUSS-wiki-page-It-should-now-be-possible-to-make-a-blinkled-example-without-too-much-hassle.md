---
layout: post
title: "I've updated the PyPRUSS wiki page. It should now be possible to make a blinkled example without too much hassle:"
date: February 01, 2014 22:36
category: "Discussion"
author: Elias Bakken
---
I've updated the PyPRUSS wiki page. It should now be possible to make a blinkled example without too much hassle: [http://wiki.thing-printer.com/index.php?title=PyPRUSS_on_BeagleBone](http://wiki.thing-printer.com/index.php?title=PyPRUSS_on_BeagleBone)





**Elias Bakken**

---


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/PhfCYcZM72D) &mdash; content and formatting may not be reliable*
