---
layout: post
title: "I was installed the new Toggle and python-octoprint-redeem package"
date: May 14, 2016 18:27
category: "Discussion"
author: Daniel Kusz
---
I was installed the new Toggle and python-octoprint-redeem package. Now I can download .cfg files and edit them. But how to do it online? The second problem is that the toggle does not see .stl models (empty platform on screen) and I can't control axes. When you attempt to change the model is still loading. I currently have only one engine hooked to the Z axis and control does not work with screen. I typed API key in Octoprint and at the moment reads the temperature of the thermistor. I can't also download the file local.cfg for Toggle - 404 error page. Any ideas or suggestions?





**Daniel Kusz**

---
---
**Elias Bakken** *May 15, 2016 01:03*

Check the permissions on the file /etc/toggle/local.cfg. The user octo needs to have write permissions.


---
**Curt Page** *May 16, 2016 14:46*

I am having the same problem with toggle not displaying the models. 


---
**Daniel Kusz** *May 16, 2016 19:25*

**+Curt Page** What about controlling axes? About models, when I upload gcode in Octoprint for printing I see model on screen but cannot switch to another. It only see uploaded model.


---
**Curt Page** *May 16, 2016 20:22*

I haven't hooked it up to my printer yet.  I've just been configuring the beaglebone and printing up parts to mount it and the screen. Once I changed the permissions for /etc/toggle Octoprint was able to save the toggle settings. 


---
**Daniel Kusz** *May 16, 2016 20:35*

**+Curt Page** How do I change the permissions for /etc/toggle/local.cfg?


---
**Elias Bakken** *May 16, 2016 20:37*

Quick and dirty: chmod 777 /etc/toggle/local.cfg


---
**Daniel Kusz** *May 16, 2016 20:49*

Thanks and sorry for so many questions. These things are new to me :-)


---
**Curt Page** *May 16, 2016 22:41*

Toggle still doesn't display the models though. 


---
**Elias Bakken** *May 16, 2016 22:44*

**+Curt Page** have you verified that the API key is right? Can you move the print head from the jog menu?


---
**Curt Page** *May 16, 2016 23:25*

+ Elias Bakken I copy and pasted the code but it's not connected to the printer yet. I'll verify the code again. 


---
**Elias Bakken** *May 16, 2016 23:27*

Ok, good! Temperature data is separate from the REST API, so that does not need a key.


---
**Veridico Cholo** *May 17, 2016 03:56*

I used to see the models inthe previous version of Toggle. Now, it just sits there thinking. There are a few STL files that I have in there, but most of the files are gcode files that won't get displayed. Is toggle trying to read and display the gcode files? Maybe a mod would be to skip files that are not STL?


---
**Veridico Cholo** *May 17, 2016 04:11*

With the API key, jog works great, temp display and control work great too. 

Besides the issue with the STL files nos displaying, I really like and use Toggle a lot.



Somehow, I think that Toggle should be able to work independent of a user loading Octoprint on a browser and connecting. Can toggle automatically connect to Octoprint so that the printer can be controlled standalone? or can it at least have a "connect" button so that the connection to Octoprint happens directly by touching the screen?


---
**Daniel Kusz** *May 17, 2016 20:44*

I also think that the Toggle should allow independent operation of the device in a certain minimalist range, for example: quick and easy start printing models from memory or easy preparing to printing process. 


---
**Veridico Cholo** *May 18, 2016 02:06*

Exactly @Daniel Kusz. Checking the status of prints, setting up for prints, starting prints, leveling the bed, etc: That's what it is most useful for.

If I pre-load GCODE files It would be useful to at least see the files listed and be able to select them to start the print from the touch screen.


---
**Elias Bakken** *May 18, 2016 09:13*

You can enable automatic connection in octoprint so that will make it connect on startup!


---
*Imported from [Google+](https://plus.google.com/103041373788524358316/posts/QT9Kge2oHmx) &mdash; content and formatting may not be reliable*
