---
layout: post
title: "Rewiring my kossel mini: the beginning. It's ugly"
date: February 22, 2016 19:18
category: "Discussion"
author: Jon Charnas
---
Rewiring my kossel mini: the beginning. It's ugly.

![images/61e06a562d19d9aaf65b5bbac49103e0.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/61e06a562d19d9aaf65b5bbac49103e0.jpeg)



**Jon Charnas**

---
---
**Elias Bakken** *February 22, 2016 19:19*

Haha! I've seen better, yes! Definitely room for improvement : )


---
**Jon Charnas** *February 22, 2016 19:48*

Note that the Replicape and BBB are seated at the top, with enough clearance that I can fit a spool holder flush against the top :)


---
**Elias Bakken** *February 22, 2016 20:07*

I noticed! I might copy that myself!


---
**Jon Charnas** *February 22, 2016 20:39*

I'll post the stl files when I'm done.


---
**Jon Charnas** *February 22, 2016 22:04*

Well, it's better looking now...


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/bCrMLtcetTq) &mdash; content and formatting may not be reliable*
