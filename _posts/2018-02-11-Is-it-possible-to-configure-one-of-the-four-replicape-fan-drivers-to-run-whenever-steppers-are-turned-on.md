---
layout: post
title: "Is it possible to configure one of the four replicape fan drivers to run whenever steppers are turned on?"
date: February 11, 2018 22:22
category: "Discussion"
author: Michael K Johnson
---
Is it possible to configure one of the four replicape fan drivers to run whenever steppers are turned on? I have an 80mm 12V 2W fan I'd like to use to cool the replicape when the steppers are on (because I want to exceed 0.5A), but it would be nice to turn the fan off when the steppers aren't being driven. (If that's in the configuration docs, my apologies for missing it.)





**Michael K Johnson**

---
---
**James Armstrong** *February 11, 2018 22:24*

Can’t you just put a gcode in your start and end sections of the slicer to turn them on? Either that or make it temp controlled (which it can be done) and put a DS1820(?) on one of the driver heat sinks. 


---
**Michael K Johnson** *February 11, 2018 22:35*

**+James Armstrong** The idea of breaking my replicape because I switched slicers and forgot to copy a gcode command doesn't sound awesome. A DS18B20 is an interesting idea.



My replicape didn't include heat sinks. Is that a normal add now?


---
**James Armstrong** *February 11, 2018 22:36*

**+Michael K Johnson** it is optional. I put heatsinks on all my drivers. Replicape supports the DS temp chips as cold end or other temperature monitors and can Slave a fan to them 


---
**Jon Charnas** *February 12, 2018 06:39*

Yeah slaving a temperature sensor to the cooling fan is probably the easiest thing. Heatsinks are not provided, but a good idea if you're wanting extra effective cooling


---
**Michael K Johnson** *February 26, 2018 02:23*

Heatsinks arrived and installed. Should I use silicone adhesive to stick the 18B20s to the stepper driver heatsinks? Can I sync 5 18B20s to a single fan so that it turns on if any stepper driver is warm?


---
**Jon Charnas** *February 26, 2018 10:33*

I think so yes.


---
**Michael K Johnson** *March 04, 2018 03:17*

Not enough space to glue the sensors to heat sinks on drivers, I find. I think gluing just one sensor to the heat pad on the bottom of the board under the Y stepper driver makes sense.



Then I think I'll solder it directly to the Dallas pins on the bottom of the board, rather than run it out to the header, to leave the header free to monitor the cold end temperature.


---
**Michael K Johnson** *March 13, 2018 01:28*

After I get a successful print of the Replicase, I can test this... ☺ Ugly soldering job, but it's not bridged.

![images/43525508321b0af08115138a003c6bbc.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/43525508321b0af08115138a003c6bbc.jpeg)


---
*Imported from [Google+](https://plus.google.com/+MichaelKJohnson/posts/b1U2oQLQZBb) &mdash; content and formatting may not be reliable*
