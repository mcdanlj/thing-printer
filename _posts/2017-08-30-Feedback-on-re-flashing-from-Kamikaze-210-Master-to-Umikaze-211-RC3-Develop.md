---
layout: post
title: "Feedback on re-flashing from Kamikaze 2.1.0 Master to Umikaze 2.1.1 RC3 Develop"
date: August 30, 2017 10:26
category: "Discussion"
author: Tim Curtis
---
Feedback on re-flashing from Kamikaze  2.1.0 Master to Umikaze 2.1.1 RC3 Develop.



The flash went fine and after a while of letting BBB do its thing Octoprint started running and I was able to set up my new connection. Then I got a message saying there was an update available for Replicape. I went ahead and chose to install it. After the install said it was successful I couldn't re-connect. I restarted my printer and let it sit for about 20 minutes wondering if it had "things" to do with the new updated files but all the while I knew it wasn't starting right because the motors were making the start-up ringing the whole 20 minutes instead of the usual 30-40 seconds. I tried restarting a few more times but never got anywhere with it.



So I re-flashed it again and ignored the update available. Now it is working fine but I'm not sure if I have the latest update? The update was called A8#### blah Blah Blah?? (just a bunch of weird characters). I saw many gcode files were being updated so I guess I don't have any of them with my flash.



So what did I do wrong and what am I missing?



Also I ran a test print and I don't have the large blobs anymore but still have tons of small blobs all over the part. I  will be changing to a Titan Aero extruder this weekend hoping this will correct my issues...





**Tim Curtis**

---
---
**Jon Charnas** *August 30, 2017 10:37*

You didn't do anything wrong. The new update to redeem requires an additional dependency which isn't shipped with RC 3


---
**Jon Charnas** *August 30, 2017 10:38*

I'm building the final 2.1.1 now which will include it. However if you want the update with rc3, you need to manually execute "pip install sympy" to get redeem to restart properly.


---
**Tim Curtis** *August 30, 2017 10:53*

**+Jon Charnas** do you mean execute "pip install sympy" in putty ssh session? I haven't been able to login to my BBB with putty since  I upgraded to kazikaze 2.1.0 a while ago. i'm sure its something wrong that I am doing. I have very very limited experience with Linux.


---
**Jon Charnas** *August 30, 2017 10:55*

**+Tim Curtis** yes - that's what I meant. For your SSH issues try using 192.168.6.2 if trying to connect to 192.168.7.2 if you're going through the USB? 2.1.1 should fix your issues.


---
**Tim Curtis** *August 30, 2017 11:09*

**+Jon Charnas** Yeah I was going thru USB. 192.168.6.2 is always the one I used before. I will try again tonight. 



If I can't get in that way can I login through my current network connection since I know the IP address? I never tried it that way? 


---
**Jon Charnas** *August 30, 2017 11:10*

yes, that'd work more reliably even.


---
**Tim Curtis** *August 30, 2017 11:21*

**+Jon Charnas** Okay Thanks for the help.



Just to be clear, Once I log into my BBB I just type in pip install sympy and it should  finish the install and get redeem up and running again?

I only ever logged in as root before, do I log in as root or kamikaze?



Sorry for all the questions...


---
**Jon Charnas** *August 30, 2017 11:25*

**+Tim Curtis** not a problem - if you're having to ask, there's likely another dozen or so folks wondering. So, yes. Login (as root!) through ssh, just execute the "pip install sympy" and restart the BBB. (Or, to get redeem going faster, execute the Restart Redeem from the OctoPrint menu, or execute "systemctl restart redeem" from the command line before closing down the SSH connection).



I hope this helps?


---
**Tim Curtis** *August 30, 2017 11:40*

Thank you **+Jon Charnas**! I will do that tonight and provide feedback tomorrow for others that may encounter this. 


---
**Tim Curtis** *August 31, 2017 12:55*

Another update. With the helpful info from **+Jon Charnas**, Everything went smooth last night with the update. I was able to correctly use putty to ssh directly into my BBB through my IP address instead of using the USB cable. All I have left to do is change the password on my BBB to a new one.



I think I just issue a passwd command to reset it but I think I remember reading somewhere that I have to log in as a different user? Or can I just change the password while logged in as root?



Thanks again.


---
**Jon Charnas** *August 31, 2017 13:04*

You can run "passwd" as root - normally ubuntu doesn't have a root user, but we changed that for Umikaze/Kamikaze. It's a philosophical discussion among many linux distributions that doesn't have a correct answer with regards to security. For practicality though, it really helps to login as root directly ;)


---
**Tim Curtis** *August 31, 2017 13:11*

Okay, thanks again!


---
*Imported from [Google+](https://plus.google.com/106115959585163180149/posts/2Bb9pfs7VtY) &mdash; content and formatting may not be reliable*
