---
layout: post
title: "Helo, I need some help, what unit use for blacklash on reedem ?"
date: February 27, 2016 12:07
category: "Discussion"
author: Francois Muelle
---
Helo, 

I need some help,

 what unit use for blacklash on reedem ? %,step, mm, or other?

Thanks





**Francois Muelle**

---
---
**Daryl Bond** *February 27, 2016 13:48*

all the length settings in the config are in metres


---
**Elias Bakken** *February 27, 2016 22:36*

Yes, except for steps_pr_mm :)


---
**Veridico Cholo** *February 29, 2016 20:21*

How do the backlash settings in replicape work exactly? I don't recall dealing with backlash settings in other firmware. 


---
**Elias Bakken** *February 29, 2016 20:42*

Not sure, not my work, and my Kossel mini is ok on backlash. From what I've seen in the code it just adds some extra distance to the paths.


---
**Daryl Bond** *March 01, 2016 00:25*

It keeps track of the direction of motion for each axis and whenever there is a change in direction it inserts a little extra distance to the path.



I have never used it. I imagine that measuring the backlash for which you are trying to compensate would be relatively difficult for any reasonably built machine. The uncertainty in extrusion dynamics would probably negate any backlash compensation anyway.


---
**Veridico Cholo** *March 01, 2016 00:29*

Ohhh, I see. That makes sense. So Far I don't have any backlash issues in my printer but It's good to know it's there. Thanks!


---
**Jon Charnas** *March 02, 2016 10:02*

Aha, I need to look at those. My kossel mini's got a backlash problem (calibration cube corners are a little overinflated)


---
**Tim Curtis** *March 02, 2016 20:13*

Jon, That problem could also be an extrusion related issue. Backlash should be check with a dial indicator to verify the actual movement is what the programmed movement is when you move back and forth.. Once you verify that a 1mm move is truly a 1mm move no matter which way you move then you can dial in your extruder/extrusion rate. At least that is how I went about it with my printer.


---
**Jon Charnas** *March 02, 2016 20:31*

Tim, that was exactly what the calibration cube print was for. I'm going to try and print it again but slower on the perimeters and see if it resolves it. If it does, then I know it's backlash :)


---
*Imported from [Google+](https://plus.google.com/110879606286009280583/posts/8TQgGeRGxDs) &mdash; content and formatting may not be reliable*
