---
layout: post
title: "Checked the wiki it seems end stop has internal pull up resistance of 47k"
date: September 19, 2016 16:32
category: "Discussion"
author: Step Cia
---
Checked the wiki it seems end stop has internal pull up resistance of 47k. I'm planning to straight hook up FSR as z limit switch. Does this means that to get a somewhat quick response, I need to preload the FSR to slightly higher than 47k and then when force is applied to FSR it dips below 47k and my end stop is triggered?





**Step Cia**

---
---
**Elias Bakken** *September 20, 2016 14:24*

What FSRs are you using? 




---
**Step Cia** *September 20, 2016 15:26*

This kind of FSR [http://www.ultibots.com/fsr-kit](http://www.ultibots.com/fsr-kit)/



I ended up ordering the FSR board... It seems to require certain amount of preloading to make the reading reliable. I did a dry test and basically without preloading, the reading was erratic and jumping all over the place

[ultibots.com - FSR Kit](http://www.ultibots.com/fsr-kit/)


---
**Elias Bakken** *September 20, 2016 15:32*

It seems you can connect it NC or NO. I would connect it NO. It should not be a problem with the pull-up resistor in that case. 




---
**Step Cia** *September 20, 2016 19:42*

Thx I also think it need to be NO but still unsure how hard I need to crash my hot end to the bed in order you trigger the FSR. I read it has to do with lowering the internal FSR resistance to match internal pull up resistor in this case 47k and then it will trigger.



Btw will the new version of replicape change the internal pull up resistance from 47k?


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/dPhx4tEjkK7) &mdash; content and formatting may not be reliable*
