---
layout: post
title: "My team and I are having some issues with our printer"
date: June 18, 2016 19:14
category: "Discussion"
author: Sinchita Siddiquee
---
My team and I are having some issues with our printer. We are still trying to configure it and running some test prints. I would really appreciate some insights on the following:

1. During the print, the machine suddenly stops and  we get an alert message saying "stepper fault - stepper z". 

2. The bed reaches the position we want when we press Z home from octoprint but once the print starts the bed goes down and there is a huge gap between the extruder and the bed.





**Sinchita Siddiquee**

---
---
**Elias Bakken** *June 18, 2016 19:26*

1) that is an over temperature alarm, try to lover the current to 0.7

2) Use the M206 g-code to configure the offset for that axis. M206 X1 will increase the offset with 1mm.


---
**Sinchita Siddiquee** *June 18, 2016 20:21*

0.7 seems to be a very small current for our printer and it affects the movement. Another concern we have is that changing values on the printer.cfg file is not always causing changes on the printer. Any idea what that the reason for that could be?


---
**Elias Bakken** *June 18, 2016 20:41*

0.8 max without cooling. You can play around with it, see what works. If you cannot make it work, you need to add cooling fins. 

Make sure you restart redeem every time you make a change. Hopefully you are making the changes to the local.cfg


---
**Sinchita Siddiquee** *June 18, 2016 20:53*

Yes I restart redeem after making any changes, but I will still check it again. Thanks a lot for your help :)


---
**Jon Charnas** *June 20, 2016 14:48*

Also, make sure that you define the home position properly. "Endstops" and "home" positions are not the same. So if you setup your home position to be z=0 while endstops are at z=200, it'll first bounce up against the endstops and then shift down to it's "home" of 0. I hope this makes sense, **+Elias Bakken** can you confirm?


---
**Sinchita Siddiquee** *July 04, 2016 17:02*

I tried restarting redeem after making changes in the cfg file but it's still showing any changes in the printer. Might the local.cfg file have something to do with it?


---
**Elias Bakken** *July 04, 2016 18:55*

Do you mean that it is NOT showing any changes? The local.cfg has the highest priority of the config files. What change did you make? 


---
*Imported from [Google+](https://plus.google.com/108772268407141748394/posts/jhMtf4Cv1FU) &mdash; content and formatting may not be reliable*
