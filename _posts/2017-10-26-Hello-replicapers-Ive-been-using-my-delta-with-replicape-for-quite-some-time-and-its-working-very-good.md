---
layout: post
title: "Hello replicapers :) I've been using my delta with replicape for quite some time and it's working very good"
date: October 26, 2017 08:34
category: "Discussion"
author: Olexandr Gudozhnik
---
Hello replicapers :)



I've been using my delta with replicape for quite some time and it's working very good. Thank you for this nice board!

Recently I changed my endstops to magnetic switches with no fine adjustment option, so I need to mess now with that "end stop offset" thing which isn't very clear to me in spite of lot of reading on the subject.

So, my main question is: which parameter in the config files is affected by M206? Changing values with M206 seem to generate NO differences in redeem behavior, nor they appear in the config files after I issue the  M500 to save changes.

Before I was manually adjusting my endstops until the distances from the bed at the (or close to) columns locations were the same as in the center (with dial indicator in the effector). Now I was hoping that M206 would allow me to do similar thing but with the software correction.

Could someone guide me or just share a hint on this calibration, please?)



Here are some relevant sections of my current config file:



[Geometry]

axis_config = 3

travel_x = -0.65

travel_y = -0.65

travel_z = -0.65

offset_x = 0.0

offset_y = 0.0

offset_z = 0.0



[Delta]

hez = 0.05

l = 0.291

r = 0.183

ae = 0.026

be = 0.026

ce = 0.026

a_radial = 0.0

b_radial = 0.0

c_radial = 0.0



[Endstops]

end_stop_x1_stops = x_cw 

end_stop_y1_stops = y_cw

end_stop_z1_stops = z_cw

soft_end_stop_min_x = -0.5

soft_end_stop_min_y = -0.5

soft_end_stop_min_z = -0.5

soft_end_stop_max_x = 0.5

soft_end_stop_max_y = 0.5

soft_end_stop_max_z = 0.5

has_x = True

has_y = True

has_z = True

invert_x1 = True

invert_y1 = True

invert_z1 = True

invert_x2 = False

invert_y2 = False

invert_z2 = False



[Homing]

home_x = 0.0

home_y = 0.0

home_z = 0.4015

home_e = 0.0

home_speed_x = 0.15

home_speed_y = 0.15

home_speed_z = 0.15

home_backoff_speed_x = 0.01

home_backoff_speed_y = 0.01

home_backoff_speed_z = 0.01

home_backoff_offset_x = 0.01

home_backoff_offset_y = 0.01

home_backoff_offset_z = 0.01









**Olexandr Gudozhnik**

---
---
**Jon Charnas** *October 26, 2017 08:50*

Hi Olexandr, It's odd that I don't see those settings in your config now, but the values that M206 adjusts are normally

[Endstops]

end_stop_offset_x = - <distance to bed>

end_stop_offset_y = - <distance to bed>

end_stop_offset_z = - <distance to bed>



I'm honestly surprised that those settings did not appear in the Documentation at all - I'll make a note to have that added ASAP. If you're in doubt about the syntax, double-check what the variables are called in the default.cfg file, and re-use that.



As to M206, you need to run M206, home, then try to see if you're centered again. You can save the values modified to the local.cfg file using M500 afterwards.


---
**Olexandr Gudozhnik** *October 26, 2017 09:08*

Hi Jon!

My default.cfg doesn't have those params either. But my installation is more then 1 year old and redeem version is 0.9 so I think it's time to update :) 

Thank you for hint!


---
**Jon Charnas** *October 26, 2017 09:13*

Ah, hehe, then yes, probably ;) Be careful, lots of things have changed in Redeem's [Delta] section too. You'll need to subtract the value in hez from r and remove hez (it's gone). If you have any probe at all you can probe with G33 to get all the values for your printer (endstop offsets, delta radius, tower offsets...). Come and ask if you need help, there's always someone ready to help, either here or on the Slack.


---
**Olexandr Gudozhnik** *October 26, 2017 12:16*

Ok, I flashed 2.1.0. In default.cfg there is still no mention of "end_stop_offset_*" parameters...Looks like I'll be looking into redeem's source code.

By the way, is it safe to click on "Update Now" when Octoprint's "Update available" pops up?


---
**Jon Charnas** *October 26, 2017 13:22*

Starting with 2.0.8 it was safe indeed to update octoprint through the popups.



Why not 2.1.1 to get the latest and greatest path planner for Delta printers? 😁


---
**Olexandr Gudozhnik** *October 26, 2017 13:55*

Well... Installed 2.1.1 :)

There still no "offset" params in the [Endstops] section...

The values changed with M206 somehow end up written (with "-" sign) to "offset_*" parameters from [Geometry] section. And these parameters, contrary to the documentation, do NOT change anything in my homing or any other movement... Now I'm really stuck...


---
**Dan Cook** *October 26, 2017 15:13*

Aye, you need to have some kind of negative number for your X/Y/Z offsets in [Geometry]... have you run any G33 calibrations? Here's what I have for example:

offset_x = -0.31629893673

offset_y = -0.317463940269

offset_z = -0.316830120087



Then my "home_z = 0.300" which pulls my carriages off of the endstop switches. In the [Homing] section...


---
**Dan Cook** *October 26, 2017 16:11*

If you have a working Z2 endstop, I suggest setting up a proper G29.1 routine: [https://replicape.github.io/replicape/gcodes.html#g29-1](https://replicape.github.io/replicape/gcodes.html#g29-1)



and run some G33's, beginning with "G33 N3"... that'll properly set your XYZ offsets: [https://replicape.github.io/replicape/gcodes.html#g33](https://replicape.github.io/replicape/gcodes.html#g33)


---
**Olexandr Gudozhnik** *October 26, 2017 18:27*

Thank you Dan )



I do not have any bed probe and was perfectly printing like this for 2 years. I have plans to install one, but I still want to understand how to calibrate it manually.

I'll be trying your suggestions and post my results.

Cheers!


---
**Dan Cook** *October 26, 2017 18:33*

**+Olexandr Gudozhnik** Gotcha... well, either way, you'll need to figure the offsets for your XYZ endstops and have them entered in your [Geometry] section. As mentioned they should be entered as negative numbers.


---
**Olexandr Gudozhnik** *October 26, 2017 21:02*

Ok, got it right. My offset_* are now equal approximately -max_height, and I can make adjustments to them with M206. Apparently, "home_z" had nothing to do with actual calibration :)

Thank you all for your help!


---
**Dan Cook** *October 26, 2017 21:55*

**+Olexandr Gudozhnik** excellent! As far as home Z, the guys tell me to make sure my carriages don't sit on the endstops when it's homed...that's why I pull mine down about 15mm...


---
*Imported from [Google+](https://plus.google.com/117872216631045673950/posts/FsRUdswdtNm) &mdash; content and formatting may not be reliable*
