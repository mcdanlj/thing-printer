---
layout: post
title: "I am yet to see redeem in the logs "
date: April 02, 2016 22:13
category: "Discussion"
author: Jonathan Olesik
---
I am yet to see redeem in the logs 😕

![images/cb63b9a645eb34c4a552a1de9d91daa2.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/cb63b9a645eb34c4a552a1de9d91daa2.jpeg)



**Jonathan Olesik**

---
---
**Jon Charnas** *April 03, 2016 20:49*

Can you double check which version of octoprint you're using, the redeem plugin version and the redeem version itself? I found it useful to do a git clone of the official repositories of both and install both from the git. There needs to be a few changes in the /etc/systemd/system/ files to accomodate for the changes (i.e. point to /usr/local/bin/octoprint instead of /usr/bin/octoprint for instance), but they're simple and minor. Also, thumbs up to Redeem's development branch!


---
*Imported from [Google+](https://plus.google.com/+JonathanOlesik/posts/HRdp97ofYDF) &mdash; content and formatting may not be reliable*
