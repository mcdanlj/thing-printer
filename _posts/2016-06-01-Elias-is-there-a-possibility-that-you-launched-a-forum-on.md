---
layout: post
title: "Elias, is there a possibility that you launched a forum on ?"
date: June 01, 2016 20:14
category: "Discussion"
author: Daniel Kusz
---
Elias, is there a possibility that you launched a forum on [http://www.thing-printer.com/forums/](http://www.thing-printer.com/forums/)? I think that has allowed to us for easier troubleshooting and exchange of knowledge about replicape, software, etc.. Alternatively, I also thought about running a free forum for our needs, but for now I have little time: / Of course I want that the community continue in parallel acted on G + and She showed their achievements and ideas. What do you think about that?





**Daniel Kusz**

---
---
**Marcos Scriven** *June 02, 2016 06:09*

I'd definitely like this too. Google+ seems to be more suited to social networking. The layout is one of casual browsing, with a single 'story' or photo in the first post. The comments section is too small and limited to provide answers to technical questions.


---
**Elias Bakken** *June 02, 2016 10:17*

Yes, ill get right on this!


---
*Imported from [Google+](https://plus.google.com/103041373788524358316/posts/DnbmiEqt9SH) &mdash; content and formatting may not be reliable*
