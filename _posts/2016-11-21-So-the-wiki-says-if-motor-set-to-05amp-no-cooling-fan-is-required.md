---
layout: post
title: "So the wiki says if motor set to 0.5amp no cooling fan is required"
date: November 21, 2016 20:38
category: "Discussion"
author: Step Cia
---
So the wiki says if motor set to 0.5amp no cooling fan is required. I had mine set around 0.75 amp and use cooling fan. Could a geared motor say 3:1 or 7:1 be used to reduce current back to 0.5amp eliminate cooling fan and at the same time maintain higher torque one would get with 0.75amp. is this possible? Thx





**Step Cia**

---
---
**Jon Charnas** *November 21, 2016 20:46*

Yes, assuming the geared motor moves without skipping steps at 0.5A


---
**Elias Bakken** *November 21, 2016 20:50*

I don't think the limits are that strict. I've ran my steppers at 1.0 without a fan, just an open lid. But to answer your question, a geared reduction will limit the necessary torque!


---
**Step Cia** *November 21, 2016 21:01*

wait I thought a geared reduction will give you more torque?


---
**Jon Charnas** *November 21, 2016 21:04*

Yes for the same current. Lower current means lower torque. And gearing adds drag


---
**Jon Charnas** *November 21, 2016 21:04*

In your use case though, I'll expect you to be just fine


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/GrhAHgJTpND) &mdash; content and formatting may not be reliable*
