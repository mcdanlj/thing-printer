---
layout: post
title: "I'm having trouble getting Kamikaze to flash onto a BBB wireless and I'm hoping someone would be kind enough to help"
date: August 08, 2017 05:46
category: "Discussion"
author: Marty Vowles
---
I'm having trouble getting Kamikaze to flash onto a BBB wireless and I'm hoping someone would be kind enough to help. I'm about 8 hours in and I really feel like this should be easier.

I've got a drive running the bone-debian-8.7 image. It seems to work well, wireless connects and everything seems on the up-and-up.

I'm using Etcher to flash images onto a number of SD cards. I've tried both 2.0.8 and 2.1 images. I've verified the MD5 on both, and Etcher verifies the flash. I'm inserting the SD card with the BBB powered off. The replicape isn't installed. I depress the S2 switch and hold it as I connect a 5V, 2.4A power source. The device powers on, initially showing only the power LED. A while later the four user LEDS illuminate one by one, fairly quickly from 0 to 3, then they blank out. Then 0 and 1 illuminate again and nothing changes from that point. No cylon lights. I never see anything out on the HDMI connection. I've tried flashing with that cable disconnected. I've tried different power sources. I've probably re-burned the images a couple dozen times each. I'm just pretty well stumped. I'd think there was a problem with the BBB, but it runs the Debian image without a hitch. Can anyone point me the right direction?





**Marty Vowles**

---
---
**Janne Grunau** *August 08, 2017 09:15*

At least Kamikaze 2.1 is known not to work with the beaglebone black wireless out of the box, see [github.com - Umikaze2](https://github.com/goeland86/Umikaze2/releases) 


---
**Dan Cook** *August 08, 2017 11:30*

Aye, you'll need the Umikaze image in order for the BBBW to work. 


---
**Marty Vowles** *August 09, 2017 21:24*

That did the trick. Thanks, guys!


---
**Jon Charnas** *August 13, 2017 20:19*

I suspect 2.1.1 with BBBW should be ready for release within a month or so, and it should include "easy upgrade scripts" so it keeps your configs as it goes through the upgrade, meaning you won't have to re-do the wifi/redeem/octoprint setups ;)


---
**Marty Vowles** *August 24, 2017 03:31*

Many things are working well for me now. The last trouble spot is that /dev/octoprint_0 isn't showing up regularly for connection. I hope the official 2.1.1 helps with that.


---
**Marty Vowles** *August 24, 2017 03:41*

Ah, but "pip install sympy" did the trick. Strange that it worked some of the time.


---
**Jon Charnas** *August 24, 2017 05:38*

Yes, very strange. Thanks for letting me know, I'll double check what's going on.


---
*Imported from [Google+](https://plus.google.com/107862738482261175444/posts/bKeppxZUtmH) &mdash; content and formatting may not be reliable*
