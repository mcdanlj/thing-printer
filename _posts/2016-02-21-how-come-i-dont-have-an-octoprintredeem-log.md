---
layout: post
title: "how come i don't have an octoprint.redeem log?"
date: February 21, 2016 23:36
category: "Discussion"
author: Hauvert Pacheco
---
how come i don't have an octoprint.redeem log?





**Hauvert Pacheco**

---
---
**Veridico Cholo** *February 21, 2016 23:51*

I wasn't getting one either until I updated to the latest redeem version: 1.1.8 Raw Deal



If you send an M115 Gcode on Octoprint, you should see the redeem version. if you don't see it, then you need to update it:



Send: M115

Recv: ok PROTOCOL_VERSION:0.1 FIRMWARE_NAME:Redeem FIRMWARE_VERSION:1.1.8~Raw Deal FIRMWARE_URL:http%3A//[wiki.thing-printer.com/index.php?title=Redeem](http://wiki.thing-printer.com/index.php?title=Redeem) MACHINE_TYPE:Rapide_Lite_200XL EXTRUDER_COUNT: 1



To update via ssh:



apt-get update

apt-get install redeem


---
*Imported from [Google+](https://plus.google.com/116125412087866821266/posts/X887Zpfxuhb) &mdash; content and formatting may not be reliable*
