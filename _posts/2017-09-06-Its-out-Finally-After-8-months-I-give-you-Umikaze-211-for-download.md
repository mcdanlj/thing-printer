---
layout: post
title: "It's out! Finally! After 8 months, I give you Umikaze 2.1.1 for download!"
date: September 06, 2017 07:05
category: "Discussion"
author: Jon Charnas
---
It's out! Finally!



After 8 months, I give you Umikaze 2.1.1 for download!



If you're upgrading, please please please make sure you read the release notes!

[http://wiki.thing-printer.com/index.php?title=Release_notes_Umikaze_2.1.1](http://wiki.thing-printer.com/index.php?title=Release_notes_Umikaze_2.1.1)





**Jon Charnas**

---
---
**Jon Charnas** *September 07, 2017 12:42*

Oh, one slight new feature that went under reported in 2.1.1 is the presence of Slic3r (CLI only) so you should be able to use the Slic3r plugin in OctoPrint now. It's not the prusa branch as it wouldn't compile, but rather the master of the "standard" Slic3r.


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/digJkj3Cor1) &mdash; content and formatting may not be reliable*
