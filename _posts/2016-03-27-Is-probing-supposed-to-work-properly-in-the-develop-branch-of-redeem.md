---
layout: post
title: "Is probing supposed to work properly in the develop branch of redeem?"
date: March 27, 2016 19:20
category: "Discussion"
author: Matti Airas
---
Is probing supposed to work properly in the develop branch of redeem? I'm trying to finish my delta printer least squares auto-calibration code ([https://bitbucket.org/mairas/redeem/branch/autocal](https://bitbucket.org/mairas/redeem/branch/autocal), if anyone's interested), but G30 (and the respective PathPlanner.probe() method) gives me a headache.



It would seem to me that PruInterface.get_steps_remaining() doesn't work quite reliably. In particular, if I move the print head mid-air and issue "G30", I believe I should always get a bed distance of -10.0mm. However, I get something like -7.69690200129mm with debug log stating there are 235 steps remaining. This also results in print head not returning to the starting position correctly.



What am I missing?





**Matti Airas**

---
---
**Matti Airas** *April 03, 2016 14:58*

Hi,



I’m glad my code port is (potentially) useful. 😃



As my original post suggested, I’ve indeed been having various issues with probing, especially when initiating the probe directly from Python with PathPlanner.probe(). The print head wouldn’t return to same height after the probe resulting in lost calibration. I only saw that happen with PathPlanner.probe(), never with G30, even though the latter calls the former internally.



Probing began working for me soon before my latest pull request on March 30 and I assumed that some bug related to it had been fixed. Looking at the commit log, I can’t find anything related, however. My bet is there are some issues in the real-time code causing the occasional problems, but I haven’t yet tried to really read the assembler code to figure it out.



My G29 macro can be found below. It was created with the get_g29_macro() function in the DeltaAutoCalibration module. I intend to modify the G33 code to automatically generate the probing macro for the delta auto-calibration.



[http://pastebin.com/h4FjYqrd](http://pastebin.com/h4FjYqrd)



I didn’t quite get your question re G34. You should first move the print head to just touch the bed (with a paper in between) and then run G34. The resulting value should be immediately available, or if not, then at least after running M500. Once you’ve acquired the value, you can make micro adjustments to it by running M206. For example, “M206 X0.05 Y0.05 Z0.05” raises the print head uniformly by 0.05 mm.



The ported auto-calibration code supports 6 or even 7 factor calibration - it just modifies the tower positions by bumping the tower X or Y coordinate (depending on the tower). It’s not difficult to convert that back to Redeem’s tower angle and radius changes. I can make the code changes but I’d need some help testing because I wouldn’t want to intentionally worsen my printer’s geometry.



Cheers,



Matti


---
*Imported from [Google+](https://plus.google.com/106643081727071984016/posts/YXGRKFQVai2) &mdash; content and formatting may not be reliable*
