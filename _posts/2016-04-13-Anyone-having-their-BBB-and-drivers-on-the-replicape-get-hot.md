---
layout: post
title: "Anyone having their BBB and drivers on the replicape get hot?"
date: April 13, 2016 22:20
category: "Discussion"
author: Hitbox Alpha
---
Anyone having their BBB and drivers on the replicape get hot?





**Hitbox Alpha**

---
---
**Pieter Koorts** *April 14, 2016 05:57*

Mine do get warm even when no stepper are plugged in but not overly hot.


---
**Jon Charnas** *April 15, 2016 19:04*

Yes. I used some thermal paste to stick small radiators on them, but also lower the current as low as you can go without skipping steps while printing.


---
*Imported from [Google+](https://plus.google.com/110719820264542745151/posts/E2fkCDS3EvN) &mdash; content and formatting may not be reliable*
