---
layout: post
title: "So my replicape no longer powers my BBB"
date: April 02, 2016 23:55
category: "Discussion"
author: Hitbox Alpha
---
So my replicape no longer powers my BBB. I was getting some odd behavior (x motor reverising direction, and x endstop not working) and now the BBB will not power up using the PSU power. I have the power Led coming on the replicape but the BBB lights are dead. I can use usb power to power the BBB but not PSU. what gives?





**Hitbox Alpha**

---
---
**Hitbox Alpha** *April 03, 2016 00:02*

well i'm getting this error "Apr 03 00:01:53 kamikaze redeem[323]: Error accessing 0x70: Check your I2C addressError accessing 0x70: Check your I2C address

Apr 03 00:01:53 kamikaze redeem[323]: Error accessing 0x70: Check your I2C address


---
**Elias Bakken** *April 03, 2016 02:11*

Yes, if the BBB is not powered by the cape, that happens. Sounds like a return perhaps. Send me an email and I'll look into that. Meanwhile, you should be able to power the cape by connecting 5v sys with 5v in. Pin 7 and 9 I think, but I need to check the schematics.


---
*Imported from [Google+](https://plus.google.com/110719820264542745151/posts/JTGGSmnuFpG) &mdash; content and formatting may not be reliable*
