---
layout: post
title: "For the last couple of weeks I've been pulling my hair out wondering what could be the cause of so many blobs in my prints, running the Kossel Mini at ~100mm/s..."
date: June 27, 2016 20:21
category: "Discussion"
author: Jon Charnas
---
For the last couple of weeks I've been pulling my hair out wondering what could be the cause of so many blobs in my prints, running the Kossel Mini at ~100mm/s... Then today I noticed it had "hiccups" in its movement. Notably at layer change. 



I've been running the master branch for a while now. When I slow to 80mm/s (set in Cura Engine with Repetier before exporting G-code), it prints perfectly fine.



If I had to guess, I'd say something's slowed down the path planner from one of the previous master versions... Any other ideas why this may have changed? (my slicing settings have stayed the same, and I used them as a reference for another Kossel mini on ramps, just slowing down, and had 0 issues).





**Jon Charnas**

---
---
**Elias Bakken** *June 27, 2016 20:34*

Hm... That is way above what I usually print at. I have a print going now that should be finished in 1:40, I can give it a shot with the latest develop branch and see if I can reproduce your problem. 

You could try to play around with the settings in the path planner as well. Increasing  move_cache_size and max_length will most certainly help!


---
**Jon Charnas** *June 27, 2016 21:15*

Ok, thanks, I'll take a peek at those and see if it will help. Could any of the changes you did for the auto-leveling also affect the print settings? (i.e. matrix compensation path computation taking a while longer? I haven't looked at that code at all)


---
**Elias Bakken** *June 28, 2016 12:22*

I tried a print speed of 100 without problems, but the object was quite small. Do you have an object that you know causes blobs? Perhaps also with your slier settings? 


---
**Jon Charnas** *June 28, 2016 12:25*

Sure. I tried printing 3D benchy: [http://www.thingiverse.com/thing:763622](http://www.thingiverse.com/thing:763622)

Curaengine printer settings in repetierHost settings: [https://www.dropbox.com/s/8gd77e5znqy665w/kosselmini_replicape.rcp?dl=0](https://www.dropbox.com/s/8gd77e5znqy665w/kosselmini_replicape.rcp?dl=0)

Curaengine filament settings in repetierHost: [https://www.dropbox.com/s/zyys1nmx5fvnir1/pla_esun.rcf?dl=0](https://www.dropbox.com/s/zyys1nmx5fvnir1/pla_esun.rcf?dl=0)



I'd be glad to hear any feedback from you on that matter - If it's not redeem, then it's my slicer settings, and I need to understand what I'm doing wrong with them.


---
**Elias Bakken** *June 28, 2016 12:32*

Could you perhaps upload the gcode file directly? I don't have repetier host and it takes too long to get into it...




---
**Jon Charnas** *June 28, 2016 12:33*

Sure. But my printer's at home offline, won't be able to upload it until later this evening.


---
**Elias Bakken** *June 28, 2016 12:34*

Ok. It might be that I have a very slow outer perimeter, or it might be that we have different accelleration paramters. 




---
**Jon Charnas** *June 28, 2016 12:36*

Probably. Could you share your slicer settings? (I've been thinking about ditching Repetier/Cura for something else, but don't know what to try out) We'll have a double-blind study this way.


---
**Jon Charnas** *June 28, 2016 18:43*

Elias, here's the G-code file: [https://www.dropbox.com/s/6psws7hwk30sxq1/3DBenchy.gcode?dl=0](https://www.dropbox.com/s/6psws7hwk30sxq1/3DBenchy.gcode?dl=0)


---
**Jon Charnas** *June 30, 2016 14:44*

**+Elias Bakken** any update with the gcode I sent you?


---
**Elias Bakken** *June 30, 2016 16:39*

I'm printing it now, not seeing any stops at any point actually...


---
**Elias Bakken** *June 30, 2016 16:58*

It prints fine as far as I can tell. Can you upload a video with the problem perhaps? 


---
**Jon Charnas** *June 30, 2016 17:34*

Yes, sure, I'll restart the print now and film it. Give me about 45 minutes (with upload time).


---
**Jon Charnas** *June 30, 2016 17:44*

Ugh. Friggin demo effect. :S


---
**Jon Charnas** *June 30, 2016 17:54*

Of course, when I try to reproduce the issue, I can't. But, I did modify my local.cfg slightly - I had an identity leveling compensation matrix (no idea why, just that Redeem inserted it when I did an M500 at some point), so I took it out. Also changed the timelapse settings from every 20s to Z-layer change... Could timelapse really affect performance that much?


---
**Elias Bakken** *June 30, 2016 17:57*

I haven't tried time lapse at all, but I imagine it could pull a lot of CPU if it needs to do a software inversion of some sort... Check "top" while running to get a glimpse of what is using resources!




---
**Elias Bakken** *June 30, 2016 17:57*

*conversion




---
**Jon Charnas** *June 30, 2016 17:59*

Doesn't seem like that would do it... I see Octoprint at 10%, redeem at 10% and mjpg_streamer at 10%... I hate crying wolf when I know I had an issue. But I did update redeem from git and kamikaze updates as well since, so... I dunno, maybe I just got stuck in one bad combo?


---
**Elias Bakken** *June 30, 2016 18:00*

That might very well have been it! Most likely something using a lot of CPU!


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/BZ6vchB6jGo) &mdash; content and formatting may not be reliable*
