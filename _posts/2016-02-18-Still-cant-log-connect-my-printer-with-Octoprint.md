---
layout: post
title: "Still can't log connect my printer with Octoprint"
date: February 18, 2016 23:14
category: "Discussion"
author: Hauvert Pacheco
---
Still can't log connect my printer with Octoprint. I can't flash my board either. When I try it doesn't the lights don't blink like it should. It can log into octoprint but cant connect my printer. Can anyone help? I already tried flashing it so please avoid that :P although I am on a mac if that means anything. Btw when I download the bbb emmc flasher kamikaze  the extension is .xz and I need to convert it to img with another application. Not sure if that's messing up the flashing. Although I did do it this way the first time and it worked. 





**Hauvert Pacheco**

---
---
**Andy Tomascak** *February 19, 2016 02:45*

First things first! Make sure you are using the latest Kamikaze image (2016-02-16). Then make sure your cape is recognized by typing "dmesg | grep bone" and making sure one of your slots says "Replicape 3D printer cape, ...". If all that is good, you should be able to connect to you printer with the default settings. You will still need to make a config for your specific printer layout, but it should be able to connect regardless. Check out #replicape on freenode on IRC if you are still having issues.


---
**Hauvert Pacheco** *February 19, 2016 19:33*

I was using the 192.168.1.11 in the Web browser. I typed kamikaze.local:5000 like in elias new video and I was able to connect the printer! As well as log in. 


---
*Imported from [Google+](https://plus.google.com/116125412087866821266/posts/PufW7fXHEE3) &mdash; content and formatting may not be reliable*
