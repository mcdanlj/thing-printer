---
layout: post
title: "Ripple/vibration test with StealthChop on. Happy to see it eliminates a LOT of vibration coming from abrupt stepper direction changes (like the inset letters and dimples on the sample in the photos)"
date: March 18, 2016 18:57
category: "Discussion"
author: Ante Vukorepa
---
Ripple/vibration test with StealthChop on. Happy to see it eliminates a LOT of vibration coming from abrupt stepper direction changes (like the inset letters and dimples on the sample in the photos). You'd normally see multiple "shadows" of the letter with normal microstepping due to stepper acting like a spring in a spring+mass system.



You can see the corners aren't 100% perfect though, due to excess extrusion / blobbing i've mentioned before.



![images/dbcc4447eb86fad7fea590f41e93b57b.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/dbcc4447eb86fad7fea590f41e93b57b.jpeg)
![images/1ba39c293944651b93e76d813efc3716.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/1ba39c293944651b93e76d813efc3716.jpeg)
![images/2b942f736c79d98e40fb59f8a1822c93.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/2b942f736c79d98e40fb59f8a1822c93.jpeg)

**Ante Vukorepa**

---
---
**Ante Vukorepa** *March 18, 2016 18:59*

Download the pics to inspect them at 100% zoom. The layers are very very neat and orderly, but they look a little weird in the downsized images and thumbs due to moire.


---
**Chris Romey** *March 18, 2016 19:22*

I get the funky corners as well.



[https://drive.google.com/file/d/0B4vlYd5QDwgBZUlJWmdnOGZvNzQ/view?usp=sharing](https://drive.google.com/file/d/0B4vlYd5QDwgBZUlJWmdnOGZvNzQ/view?usp=sharing)


---
**Ante Vukorepa** *March 18, 2016 19:31*

Cool, so i'm not crazy :)


---
**Chris Romey** *March 18, 2016 19:38*

Nope! Haha


---
**James Armstrong** *March 18, 2016 19:43*

Me too, that is why I created the issue on this a month or two ago.


---
**Ante Vukorepa** *March 18, 2016 19:54*

While you're right in that having the Advance algorithm built in would be very very nice, i still think that's treating the symptom instead of the disease in this case.



Even without Advance, i think there should be less corner overextrusion than there currently is.


---
**James Armstrong** *March 18, 2016 19:56*

Maybe, but that is the whole point of advance algorithm. Without it, redeem will only extrude how much and when it is told. It won't turn down the extrusion amount when it gets to a corner or before a corner since it is only interpreting the gcode extrude command (again, that is the whole reason Advance or any sort of code like it, as far as I know). 




---
**Ante Vukorepa** *March 18, 2016 19:59*

True.


---
**Jon Charnas** *March 22, 2016 08:46*

I've noticed that I get a better finish and less blob around corners when I turn print speed down, too, but I can't seem to run the develop branch of redeem. I printed the test cube at my regular speed (60mm/s outer perimeter, 100mm/s infill) and at a very slow speed (25mm/s outer, 40mm/s infill) and the results are clear. Also, stuttering issue goes away when printing slower, so it's a confirmation of print buffer/path planner performance issue.


---
*Imported from [Google+](https://plus.google.com/105486978634901788220/posts/KFrU1y4beM5) &mdash; content and formatting may not be reliable*
