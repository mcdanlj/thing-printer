---
layout: post
title: "Hey Elias Bakken any way to get a purchase a Manga screen v1 or going to have to wait till v2?"
date: January 13, 2017 03:28
category: "Discussion"
author: Amit Patel
---
Hey **+Elias Bakken** any way to get a purchase a Manga screen v1 or going to have to wait till v2?





**Amit Patel**

---
---
**Jon Charnas** *January 13, 2017 07:03*

Afaik the v1 is sold out... He's building v2 now.


---
**Elias Bakken** *January 13, 2017 11:47*

Waiting for the V2 Rev b1 pcb, which should arrive on Monday. I'll know more by the end of that week I hope!


---
**Ратибор Конжин** *January 17, 2017 05:18*

А какие размеры будут у нового дисплея?


---
**Ратибор Конжин** *January 17, 2017 05:55*

I use a USB-flash in conjunction with the new display possible?


---
**Jon Charnas** *February 01, 2017 10:33*

So you can try other touch-screens, yes. One thing to remember, whether it's a Manga or a Raspi LCD though: use a powered USB hub - the BBB can only provide 500mA and will cause faults if you try to draw too much current.


---
*Imported from [Google+](https://plus.google.com/100854251935781152705/posts/E8HmThToW5p) &mdash; content and formatting may not be reliable*
