---
layout: post
title: "Can someone confirm how to install and setup a z probe?"
date: February 16, 2017 17:39
category: "Discussion"
author: Andrew A
---
Can someone confirm how to install and setup a z probe?



My current understanding is:



lower nozzle to bed, with a buisness card between nozzle and bed for spacing.



Then lower my z probe until the light turns on, on the probe, indicating it has detected the bed.  lock into place.





Then i need to add this to my local.cfg:



[Probe]

length = 0.01

speed = 0.05

accel = 0.1

offset_x = 0.0

offset_y = 0.0



i assume the first 3 lines need no editing.  



I understand the offsets do need to be set, and thinking about it a while i determined the easiest way would be to drop the nozzle to the bed, mark where my probe is on the bed with a pencil.  Then send a 



M114: Get current printer head position



then use the manual controls to move my nozzle to where the zprobe marking on the bed is and hit another



M114: Get current printer head position



then the difference between those numbers are my offsets, assuming i get the + and - correct i should be good.  would be nice to know of a way to double check that the offsets are currect and functioning as expected?



Anyways after this point is where i start to get a little confused.







Now i need to tell the printer to do the bed leveling, and i see 2 or 3 different things in the redeem wiki.



First i see a macro setting, and i see alot of sled docking stuff in it, and im thinking/hoping this is just for fancy setups that want to use a servo to raise and lower their probe, ontop of this it also says editing it is beyond the guide, so at that point im basically assuming i should just ignore that all together as it sounds beyond the simple thing im trying to do.



So, im assuming those macro's have to be 'added', and arn't already in the default.cfg file.  Because it looks like the macro overrides the G29 command when enabled?



In otherwords, ignoring the macro, leads me to the G29 command to do this.  So at this point im thinking i just need to add G29 to my start code in octoprint?



However it gets slightly more complicated because it says to add an "S" to it to 'not' update the bed matrix, which seems very counter productive and strange to even have as an option but whatever.



The confusing part is i think look down and see a G29S command?  How is that possible?  I mean adding an S is supposed to just not update the matrix, however its listed as 



G29S: Generate a square probe pattern for G29



which sounds like what i need to use, except it may or may not update my matrix because of the S????



Anyways long story short, to get my probe working, do i just need to add this



G29S W200 D200 P16 S6.0 K3000



to my start code in octoprint?





**Andrew A**

---
---
**Simon-Gabriel Gervais** *February 17, 2017 06:50*

you can have redeeem get the probe offset whit a G34.

g29s will save macro in local.cgf once the macro is set-up you just need to do a G29 to probe the bed.



but i am still not 100% on how to disable z2 as an endstop have have the offset apply correctly before printing. you can look at macro set-up on the other post on the subjet it will give you an idea on how it should look like (keep in mind that my printer have a 500x600 bed)


---
*Imported from [Google+](https://plus.google.com/106001037169639681932/posts/emXExkm7VvF) &mdash; content and formatting may not be reliable*
