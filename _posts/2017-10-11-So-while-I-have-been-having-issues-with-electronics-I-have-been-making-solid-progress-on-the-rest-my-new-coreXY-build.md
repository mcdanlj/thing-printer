---
layout: post
title: "So while I have been having issues with electronics, I have been making solid progress on the rest my new coreXY build"
date: October 11, 2017 03:19
category: "Discussion"
author: Daryl Bond
---
So while I have been having issues with electronics, I have been making solid progress on the rest my new coreXY build. So I thought I'd share some pics here. Note that this design is heavily inspired by the DICE printer by **+René Jurack**, just enlarged a bit. The water cooling block I have done up for the replicape may be of interest to some and is the main reason I am posting this here. I can't vouch for its efficiency, as I haven't been able to test it with a live replicape yet, but I think it should be complete overkill!



![images/797988d10c482f453bfff3376a4f5c58.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/797988d10c482f453bfff3376a4f5c58.jpeg)
![images/1481d9eacef90b8955171a072e7abc44.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/1481d9eacef90b8955171a072e7abc44.jpeg)
![images/a666f1c49d7f4a4b228896c1bde5971e.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/a666f1c49d7f4a4b228896c1bde5971e.jpeg)
![images/c144a53dae310dbc8ffa7f958eca7c1f.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/c144a53dae310dbc8ffa7f958eca7c1f.jpeg)
![images/eb3a01f88bd7a073e96c22c1a9b5b9f4.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/eb3a01f88bd7a073e96c22c1a9b5b9f4.jpeg)
![images/b7ebef12debd28b9324424f9583903c9.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/b7ebef12debd28b9324424f9583903c9.jpeg)
![images/3b0e9eaa709d0764f1780d1630b95122.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/3b0e9eaa709d0764f1780d1630b95122.jpeg)

**Daryl Bond**

---
---
**Dan Cook** *October 11, 2017 03:31*

That's a nice setup! Yeah, the water cooling block for the Replicape may be overkill... I'm able to cool mine with ease using a 30mm fan. But it definitely gets cool points! 😎


---
**Daryl Bond** *October 11, 2017 03:35*

**+Dan Cook** When I was running my old printer I had an 80mm fan ducted straight through the BBB-replicape gap, and I was still getting overheating. I do have to deal with sub-tropical ambient temperatures though...


---
**Tim Curtis** *October 11, 2017 13:14*

Now that's a cool set-up :-) .... Sorry, couldn't resist.



Nice solid looking printer, good job.


---
**Step Cia** *October 11, 2017 15:25*

Solid build. Did you start from Rene's files and modify it to make it bigger?


---
**Daryl Bond** *October 11, 2017 22:11*

**+Step Cia** I started from scratch with images of the DICE for inspiration, so the SolidWorks model is all mine. I am quite impressed with the DICE design because while I tried many different variations in my design I always seemed to end up with something very similar to what Rene had done. Some differences were necessitated by the fact that I currently don't have a 3d printer, so everything had to be milled rather than printed.


---
*Imported from [Google+](https://plus.google.com/104347555452272558593/posts/YqZX9xkbBK7) &mdash; content and formatting may not be reliable*
