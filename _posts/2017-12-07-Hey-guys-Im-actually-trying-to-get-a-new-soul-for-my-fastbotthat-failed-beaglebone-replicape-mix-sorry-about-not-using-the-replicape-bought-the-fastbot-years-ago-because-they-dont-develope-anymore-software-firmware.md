---
layout: post
title: "Hey guys, I'm actually trying to get a new soul for my fastbot(that failed beaglebone replicape mix, sorry about not using the replicape bought the fastbot years ago) because they dont develope anymore software/firmware"
date: December 07, 2017 13:54
category: "Discussion"
author: Alex Lienert
---
Hey guys,



I'm actually trying to get a new soul for my fastbot(that failed beaglebone replicape mix,  sorry about not using the replicape bought the fastbot years ago) because they dont develope anymore software/firmware updates.



After hours I was able to understand how the fastbot team got things to work but I'm not satisfied about their solution(hard coded octoprint fork) because there is no way to update octoprint without modifying a lot of core functions.

They use a unicorn server and send the octoprint GCodes by a pipe(op plugin) and the GCodes in the unicorn server are also hardcoded whats a really bad thing for upgrades. 

 

Your redeem solution is a much better system in a lot of ways and I'm not able to build my one firmware for that fastbot. 

My idea is to fork your redeem by adding another board to the configs with the right parameters. 

The only differences between replicape and fastbot I actually found out:

+GPIOs for DIR and Step

+Other EEPROM

+Other Stepper Drivers



Some help would be very nice, because I'm actually not sure which files I have to modify for the right hardware parameters.





**Alex Lienert**

---
---
**Elias Bakken** *December 07, 2017 15:15*

Hi Alex! You will have to put quite a bit of elbow grease in this in order to make it work, but if you do, a patch is welcome and I'm sure a lot of of other people with Fastbot boards would be happy as well. 

You should probably start with a BeagleBone black device tree and get that running with the latest kernel from RCN. Then change each of the parts of the DT which is relevant. You can have a look at my Device tree for Revolve: [github.com - revolve](https://github.com/intelligent-agent/revolve/blob/master/DT/am335x-revolve.dts) A static DT should be OK for this board. 



Once you have the DT in order, you need to poke around the [Redeem.py](http://Redeem.py) startup script to branch based on the board you have, but I think most of the bases are there, so it's mostly about configuration. 


---
**Alex Lienert** *December 07, 2017 22:28*

Hi Elias, thanks for your fast reply!

Getting a Debian or Ubuntu running (I recognized that you switched from Debian to Ubuntu but don't know why?) on this board will be the first step because they also modified some RAM and EEPROM settings so I'm talking about a milstone for myself :D. 

I tried to boot a normal BBB image from SD card but received some errors in the serial console.



Your device tree is a really good checkpoint to get some light in the dark for what I've to do - thanks for that information. 

I think I can handle that project but it surley needs lots of coffee cups, browser tabs and of course time.


---
*Imported from [Google+](https://plus.google.com/112031588868602620352/posts/4KRfNdqiiW8) &mdash; content and formatting may not be reliable*
