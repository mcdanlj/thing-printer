---
layout: post
title: "I'm trying my first vase-style prints. The printer pauses for ~3s once or twice every layer"
date: March 12, 2017 13:22
category: "Discussion"
author: Henrik Söderholtz
---
I'm trying my first vase-style prints. The printer pauses for ~3s once or twice every layer. Any thought as to why this might happen? No retraction or anything, so it oozes out the filament and makes a big blob. It really feels like it's experiencing some type of buffer overrun or similar?





**Henrik Söderholtz**

---
---
**Daniel Kusz** *March 12, 2017 13:45*

Try this in local.cfg:



[Planner]

# size of the path planning cache

move_cache_size = 1024



# time to wait for buffer to fill, (ms)

print_move_buffer_wait = 500



# if total buffered time gets below (min_buffered_move_time) then wait for (print_move_buffer_wait) before moving again, (ms)

min_buffered_move_time = 1000



# total buffered move time should not exceed this much (ms)

max_buffered_move_time = 10000



# max segment length

max_length = 0.001


---
**Henrik Söderholtz** *March 12, 2017 14:09*

Thanks! I'll try that. It seems at low speed (15mm/s) AND shutting down the PC showing OctoPrint I also got rid of the blobs. With OctoPrint open the blobs continued. Priority issues with path planner/web i-face?


---
**Daniel Kusz** *March 12, 2017 14:24*

I don't know that. I only know that the configuration lines from above solve this problem in my and in similar cases. This is information from Elias and other Replicape users.


---
**Henrik Söderholtz** *March 12, 2017 18:11*

**+Daniel Kusz** fair enough, thanks for supporting. Perhaps **+Elias Bakken** will pitch in when he has a moment. Would love to know the reason behind the parameter values as well, because making such short segments looks to me like it would make it work hard as hell, but that's just a completely uneducated guess/thought/question. 😊


---
**Jon Charnas** *March 14, 2017 14:59*

There is a minor bug in Kamikaze 2.1.0 I need to fix urgently, which may help, related to CPU frequency switching... It's slower on the BBB than on an Intel, and impacts complex perimeters with lots of short tool paths


---
**Jon Charnas** *March 14, 2017 15:04*

The short answer is to check GitHub for the issue and the solution, it's a simple fix to install cpufrequtils, adding a config file in the system and rebooting


---
**Tim Curtis** *August 29, 2017 11:47*

**+Jon Charnas** I have been chasing this blob issue thinking it was my extruder. But now I see this has been a issue with others. Does the new Umikazi 2.1.1 correct some of these problems? I have it to upgrade but I been waiting to finish a large print job. 


---
**Jon Charnas** *August 29, 2017 12:16*

It should - for one thing CPU settings are better, and the new redeem develop branch is in it, instead of the master version. 



I'm still writing up the release notes for the upcoming 2.1.1 - there are a number of parameters that are different or removed from the [Planner] section in the new Redeem. 



Also if you've read up in the #development channel on Slack, you'll see that 2.1.1 when it's finally released will also have a fresh merge of Redeem that's changing version from 1.3.1 to 2.0.0 :) One of the largest change sets ever made to Redeem is getting merged in. Most of those changes are included in the 2.1.1 RC3, but we're hoping to enter final testing of 2.1.1 this week, and have a public release this weekend.


---
*Imported from [Google+](https://plus.google.com/103752299402481876381/posts/CpmUJn1W2NB) &mdash; content and formatting may not be reliable*
