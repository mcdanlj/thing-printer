---
layout: post
title: "Any updates on the Revolve launch? Is it on kickstarter?"
date: June 05, 2018 17:11
category: "Discussion"
author: Jeff Erenstone
---
Any updates on the Revolve launch?   Is it on kickstarter?  



if so, can you give me a link.



Thanks





**Jeff Erenstone**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 05, 2018 17:56*

I am not Elias but there was the one time I hung out with him at a trade show. 



My understanding is that they still need to do prototypes of the final production board before launching a campaign. 


---
**Elias Bakken** *June 05, 2018 17:58*

I've just finished making a trailer for the Kickstarter laying out the timeline up til it is launched. It should be released as soon as the papers are signed!




---
**Jeff Erenstone** *June 05, 2018 17:59*

Cool, please post the link when you have it.


---
**adrian sinclair** *June 14, 2018 13:46*

awaiting revolve with eager anticipation :) 


---
**xyz122** *June 30, 2018 05:04*

**+Elias Bakken** will  manga screen 2 be opensource? I cannot buy manga 2 because of the price (its almost the cost of an android phone in India) but if i can get schematic or gerber files i can try to make my own


---
**Tim** *August 08, 2018 04:23*

Any eta on the production launch date for the Revolve?  


---
*Imported from [Google+](https://plus.google.com/104408054195666724585/posts/2iW3uVueP3r) &mdash; content and formatting may not be reliable*
