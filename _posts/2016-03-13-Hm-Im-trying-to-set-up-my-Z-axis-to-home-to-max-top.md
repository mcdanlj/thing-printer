---
layout: post
title: "Hm. I'm trying to set up my Z axis to home to max (top)"
date: March 13, 2016 16:35
category: "Discussion"
author: Ante Vukorepa
---
Hm. I'm trying to set up my Z axis to home to max (top). I can't seem to get it to detect the endstop while homing, which is probably my fault (incorrectly set up endstops directions), but never mind that...



After homing times out, Redeem crashes. I get this error:



Unexpected error while reading serial port, please consult octoprint.log for details: SerialException: 'read failed: [Errno 5] Input/output error' @ comm.py:_readline:1373

Changing monitoring state from 'Operational' to 'Error: SerialException: 'read failed: [Errno 5] Input/output error' @ comm.py:_readline:1373'

Connection closed, closing down monitor



... which doesn't really tell much (besides the fact Redeem crashed). Some seconds later, the watchdog seems to kick in and reboots the BBB. Any clues?





**Ante Vukorepa**

---
---
**Ante Vukorepa** *March 13, 2016 19:04*

Yeah, i know. Was about to and post an update, but fell asleep, lol.


---
**Ante Vukorepa** *March 13, 2016 19:47*

Nothing useful there.


---
**Jon Charnas** *March 14, 2016 13:57*

try "systemctl -n 100 -l status redeem"


---
**Ante Vukorepa** *March 14, 2016 14:37*

Will do when i'm back home. If i can repro it, that is, because the issue is gone now the endstops are doing their job.


---
**Ante Vukorepa** *March 14, 2016 14:44*

It looks like it's a known issue: [https://bitbucket.org/intelligentagent/redeem/issues/85/homing-for-z-doesnt-seem-to-work-right](https://bitbucket.org/intelligentagent/redeem/issues/85/homing-for-z-doesnt-seem-to-work-right)



In short, if the axis never reaches the home offset in time, it times out, tries to cancel the move, and crashes.



Not a big deal if everything works, but will cause a crash if an endstop is misconfigured or not working.


---
*Imported from [Google+](https://plus.google.com/105486978634901788220/posts/MqX1Ywdo9MG) &mdash; content and formatting may not be reliable*
