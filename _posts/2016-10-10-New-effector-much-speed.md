---
layout: post
title: "New effector, much speed"
date: October 10, 2016 20:18
category: "Discussion"
author: Elias Bakken
---
New effector, much speed



![images/b1737522093ada14141c5604dc6cfa00.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/b1737522093ada14141c5604dc6cfa00.jpeg)
![images/38e5686f348f42dd6c557497d32d6f58.gif](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/38e5686f348f42dd6c557497d32d6f58.gif)
![images/1f54565ada74280025a6852622fbff2c.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/1f54565ada74280025a6852622fbff2c.jpeg)
![images/31fa21f0186a866e6705dd5f16919ba1.gif](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/31fa21f0186a866e6705dd5f16919ba1.gif)

**Elias Bakken**

---
---
**Daniel Kusz** *October 10, 2016 21:13*

Good work :-) 


---
**Daniel Kusz** *October 11, 2016 20:49*

What do you print on the video? Any problems with adhesion in this big element?


---
**Elias Bakken** *October 11, 2016 20:53*

I'm printing an LCD and button panel for a Hydroponics system. Usig G29 and G33, I'm not having any trouble with adhesion. Well, I do see there are some spots that are crapping out, but I think that is due to finger marks, not errors in the bed height. Here is my latest probe data. It's within 0.2 mm across the bed: [http://review.thing-printer.com/?key=WBN0CIE9TK98XMNPYST0](http://review.thing-printer.com/?key=WBN0CIE9TK98XMNPYST0)

[review.thing-printer.com - Tools Bed probing Config Key Store Wiki Review is a collection of ...](http://review.thing-printer.com/?key=WBN0CIE9TK98XMNPYST0)


---
**Daniel Kusz** *October 11, 2016 21:02*

My last print was element 165mm width and two corners were not stick properly to the bed. Now I test primafix and it makes adhesion much better. But I must change design of heated bed and implememt bed probing. 


---
**Alexander Borsi** *October 12, 2016 16:53*

What is your probe setup? Can we get BLTouch support added?


---
**Elias Bakken** *October 14, 2016 15:26*

Wiley on [replicape.slack.com](http://replicape.slack.com) has BLTouch working. I can possibly make it into a plug-in if need, but it should work with just the right setup.. 


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/NdYJwjy4qWZ) &mdash; content and formatting may not be reliable*
