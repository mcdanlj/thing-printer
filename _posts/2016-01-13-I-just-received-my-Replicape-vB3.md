---
layout: post
title: "I just received my Replicape vB3 ;)"
date: January 13, 2016 17:26
category: "Discussion"
author: James Armstrong
---
I just received my Replicape vB3 ;)





**James Armstrong**

---
---
**Elias Bakken** *January 13, 2016 17:28*

So soon? Wow! Awesome : )


---
**James Armstrong** *January 14, 2016 16:38*

I just posted a new question but will put it here also. What is the best and latest image to download for this board? Is there a newer kernel image? I see the Kamikaze images seem newer than the Jessie images on you feeds. Also, after installing what ever image you recommend, I assume I should checkout the latest replicape (Redeem) repository.


---
*Imported from [Google+](https://plus.google.com/+JamesArmstrong3/posts/D9xpE9H3dok) &mdash; content and formatting may not be reliable*
