---
layout: post
title: "Hello does anyone have a functioning g29 macro for a core xy they could post"
date: August 04, 2016 01:34
category: "Discussion"
author: Olahf84
---
Hello does anyone have a functioning g29 macro for a core xy  they could post. I have my machine all up and running except for a working g29. I am not very familiar with g codes to get it working the way I would like . Right now I have it probing the bed at 8 points around the board but it does not lower the z axis between each move unfortunately dragging the nozzle across my plate and fouling up the bed leveling. Does anyone know the codes to lower the bed a set amount between moves. I would like it to move to each position and then probe location then lower the bed 5mm and then move to the next spot probe and so on. 



Also what slicer is everyone using because for some reason my copy of simplify3d is refusing to print on an inverted bed.





**Olahf84**

---
---
**James Armstrong** *August 04, 2016 02:07*

You need to go join the slack channel, there have been a few others recently that have been messing with bed leveling and one has a preliminary write up that will eventually be put on the wiki. 


---
**Olahf84** *August 04, 2016 02:14*

Cool thanks James I will check that.


---
**Olahf84** *August 04, 2016 02:32*

Sorry James dumb question how do I join the slack channel downloaded the app and put in the address and it is saying I can not access it. Slack does not seem user friendly.


---
**James Armstrong** *August 04, 2016 02:34*

[http://www.thing-printer.com/wp-login.php?action=slack-invitation](http://www.thing-printer.com/wp-login.php?action=slack-invitation)


---
**James Armstrong** *August 04, 2016 02:34*

Slack groupsa are usually invite only 


---
**Olahf84** *August 04, 2016 02:38*

Never mind I am on my phone and the mobile site for thing printer is being a bit wonky. Got the invite to pop up.


---
**James Armstrong** *August 04, 2016 02:40*

You can send me you email address and I can invite manually


---
*Imported from [Google+](https://plus.google.com/101872482691679577480/posts/R7N6iUFD8wr) &mdash; content and formatting may not be reliable*
