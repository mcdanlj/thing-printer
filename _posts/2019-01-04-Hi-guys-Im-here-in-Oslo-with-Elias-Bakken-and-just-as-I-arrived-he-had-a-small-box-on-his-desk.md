---
layout: post
title: "Hi guys, I'm here in Oslo with Elias Bakken , and just as I arrived, he had a small box on his desk"
date: January 04, 2019 20:46
category: "Discussion"
author: Jon Charnas
---
Hi guys,



I'm here in Oslo with **+Elias Bakken**, and just as I arrived, he had a small box on his desk.



What was inside, you ask?



The first developer prototypes of Revolve. The pre-series meant to be testing the Revolve design in depth more than the proof of concept demoed at MRRF.



I can already hear the "When can I pre-order one?" questions coming. The answer is: we can't really tell at the moment. First, the software running on it right now needs a whole lot of work. Developers will be getting their boards in the coming week(s?) to get started, but there aren't many of them, and most of us have day jobs that take up our time.



Then there's the fact that until we've managed to really run through long prints with the board, and validate that the components and PCB actually hold up under stress, it'd be inconsiderate to manufacture more. You don't want a board that's not going to work as you expect, do you?



So in short, yes, the project is moving forward, and we'll try to keep you up to date when we have important milestones to share about this development. The big news today is, yes, some Revolve boards exist. They boot, and they basically move motors and react to endstops as they should. But there may be some issues that will come up to be resolved before we can push for a full-on production run.



And for those of you with Replicapes, don't worry, we're still pushing for a new release of the software before we shift our efforts to Revolve-centric development.



![images/6ec69a42f24c8717f11419c25ac69ece.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/6ec69a42f24c8717f11419c25ac69ece.jpeg)
![images/107b5f5131fc775f25776afb53a867fe.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/107b5f5131fc775f25776afb53a867fe.jpeg)

**Jon Charnas**

---
---
**ThantiK** *January 05, 2019 01:45*

Holy freakin' hell I've been waiting like mad for this thing to finally arrive...


---
**John Pickens** *January 05, 2019 02:52*

Yes, I've been hoping to see these in production since I saw the prototype at MRRF.  It's been tough showing off my printers and having to tell people that they can't buy a Replicape for love or money.


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/Q2wrtrB2UFD) &mdash; content and formatting may not be reliable*
