---
layout: post
title: "Hi All, I'm having a problem connecting to the BBB using either USB or ethernet cable"
date: April 30, 2017 12:26
category: "Discussion"
author: tizzledawg
---
Hi All,



I'm having a problem connecting to the BBB using either USB or ethernet cable.



To confirm, I downloaded an image of a normal ubuntu flasher and that was fine. I could connect to [192.168.7.2:22](http://192.168.7.2:22)  using Putty no problems.



I've tried 2.0.8 and 2.1.0b and had no luck connecting on either image. I had been using this BBB before and only re-flashed because I'm a lazy idiot.

Any ideas?



I flash the image to sd using Etcher, hold down boot switch before plugging into usb. Wait till I see activity on the leds and then release. The back-forth pattern goes on for 5 mins, then solid, then out. Remove power and SD card and re-power with USB. Can't connect - even after 10 mins.



Has the local IP changed?



Thanks for any help.





**tizzledawg**

---
---
**tizzledawg** *April 30, 2017 13:50*

Don't worry - I found out it was missing Beaglebone Drivers for Windows 7.


---
*Imported from [Google+](https://plus.google.com/108496831286068938419/posts/LWvrv1ADhUN) &mdash; content and formatting may not be reliable*
