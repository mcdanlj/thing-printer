---
layout: post
title: "So it's been a bit of time since I published the Umikaze 2.1.1 image"
date: September 15, 2017 11:21
category: "Discussion"
author: Jon Charnas
---
So it's been a bit of time since I published the Umikaze 2.1.1 image.



Many on Slack have reported good things, some have reported a few issues which we've tried hard to fix and have turned out to not be a problem in the image per se.



That said, the development team takes the feedback of the Replicape community seriously, and I thought it'd be interesting to get a feel for how the community feels about the development team in general, and whether things are working fine, or do we need to spend more effort getting the message out there.



The form below is 6 questions and completely anonymous, so if you can take the time, we'd appreciate hearing back from you.



Sincerely, the Replicape development team





**Jon Charnas**

---
---
**Daniel Kusz** *September 27, 2017 19:44*

Hi Jon!

Is it possible to know the results of the feedback from community?


---
**Jon Charnas** *September 27, 2017 20:06*

Sure! Though <b>community feedback</b> feels like a bit of a stretch at this point, we only have 8 or 9 answers in the sheet as of today's... I'll post the read only link here as soon as my self inflicted fresh hot glue injury stops throbbing ;)


---
**Jon Charnas** *September 27, 2017 20:10*

Here it is. Since answers are anonymous I think it's ok to make them public?

[docs.google.com - Replicape User Feedback (Responses)](https://docs.google.com/spreadsheets/d/1ELc-lhZWHWQ6hQycn1ZyJIH8RLazUzXNacxKsmqGqT4/edit?usp=sharing)


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/hdN7KYDk9Fo) &mdash; content and formatting may not be reliable*
