---
layout: post
title: "Hi All , So I bought a IR probe from Aus3D and it works fine"
date: February 11, 2017 10:03
category: "Discussion"
author: tizzledawg
---
Hi All ,



So I bought a IR probe from Aus3D and it works fine. Their selling point was it triggers from 3 mm above the nozzle so no need for a servo.



So it homes the Z with about 2 mm to the surface but I'm trying all the offset workarounds like G92 Z2 but it doesn't seem to move it past the trigger point. I suspect the probe is disabling the Z motor because it is an endstop after all.



Should I comment out the endstop mask for the Z and will redeem still know what input to look for when probing?









**tizzledawg**

---
---
**Elias Bakken** *February 11, 2017 14:37*

If you want it to be a z-probe for bed calibration, you can activate and deactivate it as a part of the g31/g32 routine!


---
**Simon-Gabriel Gervais** *February 15, 2017 16:48*

I have the same issue. I'm not sure if my probe type is set correctly the M558 description in the wiki is not very clear. whit this macro it works fine for the probing. but when I start the print it act as an end stop and don't apply the probe offset.

my macro have surely something missing.

my g29 macro looks like this:



[Macros]

g29 = M561; (RFS) Reset bed matrix

	M557 P0 X+0.00 Y+5.00 Z6.0

	M557 P1 X+0.00 Y+166.67 Z6.0

	M557 P2 X+0.00 Y+333.33 Z6.0

	M557 P3 X+0.00 Y+480.00 Z6.0

	M557 P4 X+200.00 Y+05.00 Z6.0

	M557 P5 X+200.00 Y+166.67 Z6.0

	M557 P6 X+200.00 Y+333.33 Z6.0

	M557 P7 X+200.00 Y+480.00 Z6.0

	M557 P8 X+350.00 Y+5.00 Z6.0

	M557 P9 X+350.00 Y+166.67 Z6.0

	M557 P10 X+350.00 Y+333.33 Z6.0

	M557 P11 X+350.00 Y+480.00 Z6.0

	M557 P12 X+500.00 Y+5.00 Z6.0

	M557 P13 X+500.00 Y+166.67 Z6.0

	M557 P14 X+500.00 Y+333.33 Z6.0

	M557 P15 X+500.00 Y+480.00 Z6.0

	G32 ; Undock probe

	G28 ; Home steppers

	G30 P0 S F3000; Probe point 0

	G30 P1 S F3000; Probe point 1

	G30 P2 S F3000; Probe point 2

	G30 P3 S F3000; Probe point 3

	G30 P4 S F3000; Probe point 4

	G30 P5 S F3000; Probe point 5

	G30 P6 S F3000; Probe point 6

	G30 P7 S F3000; Probe point 7

	G30 P8 S F3000; Probe point 8

	G30 P9 S F3000; Probe point 9

	G30 P10 S F3000; Probe point 10

	G30 P11 S F3000; Probe point 11

	G30 P12 S F3000; Probe point 12

	G30 P13 S F3000; Probe point 13

	G30 P14 S F3000; Probe point 14

	G30 P15 S F3000; Probe point 15

	G31 ; Dock probe

	M561 U; (RFS) Update the matrix based on probe data

	M561 S; Show the current matrix

	M500; (RFS) Save data


---
**Klipper Pressure Advance** *February 18, 2017 12:34*

I have a IR and use the G32 to enable and G31 to disable the Z2 enstop

g32 =

       M574 Z2 x_ccw,y_ccw,z_ccw    ;   activate endstop

        

g31 = 

       M574 Z2                      ; deactivate endstop 


---
**Klipper Pressure Advance** *February 18, 2017 12:35*

you need to make sure the motor direction is right for you ie ccw or cw


---
**Jonathan Cohen** *April 30, 2018 20:37*

Are there working examples of config files using an IR probe for the bed calibration and z-min endstop ?


---
*Imported from [Google+](https://plus.google.com/108496831286068938419/posts/Z6gAxeKPxSx) &mdash; content and formatting may not be reliable*
