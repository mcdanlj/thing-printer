---
layout: post
title: "I don't get this. I've got an end stop on the maximum end of the Z axis"
date: March 13, 2016 20:33
category: "Discussion"
author: Ante Vukorepa
---
I don't get this. I've got an end stop on the maximum end of the Z axis. It's connected as Z1.



No matter what i do, end_stop_Z1_stops = z_pos, z_neg, z_cw or z_ccw, the behaviour is <b>always</b> exactly the same - the endstop stops <b>negative</b> motion only (i.e. movement towards 0). M574 behaves in exactly the same way.



What is going on? Am i going nuts?





**Ante Vukorepa**

---
---
**Elias Bakken** *March 13, 2016 20:36*

Hehe, let's hope not! A clean M574 should return the current config, how does that look? Are you editing local.cfg?


---
**Ante Vukorepa** *March 13, 2016 20:49*

M574 by itself returns the correct setup (regardless of whether i set it up via cfg or override via gcode). But the actual behaviour is always the same, no matter what it's set to.



I'm using a custom .cfg i edited for my machine, but i've tried editing local.cfg as well. I've currently have it empty and am using just the printer-specific .cfg (sanity check).


---
**Ante Vukorepa** *March 13, 2016 20:50*

I've tried Z2 as well, same behaviour.


---
**Ante Vukorepa** *March 13, 2016 20:52*

I think i might be going crazy. Now it's suddenly working.


---
**Ante Vukorepa** *March 13, 2016 20:55*

Maybe i've had it overriden in local.cfg and forgot to restart after clearing it. No idea :/


---
**Elias Bakken** *March 13, 2016 21:25*

That might be it. But the M574 should give you the current config no matter what is in the config files.


---
**Ante Vukorepa** *March 13, 2016 21:34*

Then i've got no idea what happened. I was moving the Z axis up and down and triggering the endstop with my finger. It would only stop the negative movement, regardless of what M574 was returning. Weird.


---
*Imported from [Google+](https://plus.google.com/105486978634901788220/posts/ijW5QGXjygK) &mdash; content and formatting may not be reliable*
