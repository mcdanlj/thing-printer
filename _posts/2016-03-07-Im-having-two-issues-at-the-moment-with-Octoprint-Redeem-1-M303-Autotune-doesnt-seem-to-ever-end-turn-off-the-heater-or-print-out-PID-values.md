---
layout: post
title: "I'm having two issues at the moment with Octoprint/Redeem: 1) M303 (Autotune) doesn't seem to ever end (turn off the heater) or print out PID values"
date: March 07, 2016 23:01
category: "Discussion"
author: Andy Tomascak
---
I'm having two issues at the moment with Octoprint/Redeem:

1) M303 (Autotune) doesn't seem to ever end (turn off the heater) or print out PID values. Or maybe my own values are so far off that it can't converge. Either way, this command never completes. I don't even know how to cancel it other than to turn off the BBB.

2) My Manga screen doesn't seem to be reflecting the temp value changes. It does at first, but at some point it loses connection (or something) and the temp values shown become static (and incorrect).



Anyone else experiencing this or have thoughts?





**Andy Tomascak**

---
---
**Elias Bakken** *March 07, 2016 23:07*

1) M303 is not ideal, I just closed an issue on that in the redeem respository tracker, but I guess that will have to be re-opened. 

2) Toggle loses the connection with Redeem some times. Not sure why, I think the client SockJS implementation is a bit off, I could not find a good sockJS client for Python, so it's a hacked together version of something. 


---
**Ante Vukorepa** *March 08, 2016 00:26*

Tried M303 just now, worked fine for me.


---
**Jon Charnas** *March 08, 2016 10:29*

**+Elias Bakken** I think it's a more general connection problem you've seen, because I noticed that sometimes Octoprint also kind of "hiccups" in its update status with redeem.


---
**Jon Charnas** *March 11, 2016 21:47*

Nevermind, my "hiccup" issues were due to a bad wifi usb dongle...


---
**Ante Vukorepa** *March 13, 2016 21:22*

Reproduced the M303 issue just now. I have a hotend with a very very high "momentum" (it continues to heat up and overshoot for quite a while after cutting the power to the heater). FWIW, it confuses the hell out of Marlin's autotune too, i ended up having to reduce its power significantly, and even then, i had to manually tune it because the autotune results weren't very good.


---
**Ante Vukorepa** *March 15, 2016 02:47*

Manual PID tuning FTW. [http://i.imgur.com/GYvQgbZ.png](http://i.imgur.com/GYvQgbZ.png)


---
**Chris Romey** *March 24, 2016 23:31*

**+Ante Vukorepa** Out of curiosity, what hot end are you using and what were your final PID values?


---
**Ante Vukorepa** *March 25, 2016 01:06*

I'm using a Dglass3D (D3D) auto-lift hotend. Semitec 104GT-2 thermistor. P = 0.05, I = 0.05, D = 10.00. It has a slight initial overshoot, but settles quickly, so i didn't bother with further tweaking.


---
*Imported from [Google+](https://plus.google.com/110080975040288455209/posts/g8P7PLaHnMH) &mdash; content and formatting may not be reliable*
