---
layout: post
title: "I made a video on end stop configuration, think that should be useful for a lot of people!"
date: January 25, 2016 22:55
category: "Discussion"
author: Elias Bakken
---
I made a video on end stop configuration, think that should be useful for a lot of people! 
{% include youtubePlayer.html id=5LEjdQtIYe4 %}
[https://youtu.be/5LEjdQtIYe4](https://youtu.be/5LEjdQtIYe4)





**Elias Bakken**

---
---
**Jon Charnas** *January 27, 2016 14:31*

Thanks for that!


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/1W7q8FozZ6D) &mdash; content and formatting may not be reliable*
