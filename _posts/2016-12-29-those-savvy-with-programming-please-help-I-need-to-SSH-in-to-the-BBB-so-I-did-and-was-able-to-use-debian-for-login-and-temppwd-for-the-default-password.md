---
layout: post
title: "those savvy with programming please help. I need to SSH in to the BBB so I did and was able to use debian for login and temppwd for the default password"
date: December 29, 2016 07:03
category: "Discussion"
author: Step Cia
---
those savvy with programming please help. I need to SSH in to the BBB so I did and was able to use debian for login and temppwd for the default password. is there anything else I need to type in before I can do "updatedb" ? how do I locate redeem.py?

![images/2f7c4c0d3a1bcaba30727dc716b38e78.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/2f7c4c0d3a1bcaba30727dc716b38e78.jpeg)



**Step Cia**

---
---
**Step Cia** *December 29, 2016 07:23*

ok found it. I think this is where I need to be 

![images/94c84e4ed43023adf27a7e37320cd317.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/94c84e4ed43023adf27a7e37320cd317.jpeg)


---
**Jon Charnas** *December 29, 2016 07:41*

yes, but remember to do make install if you modify the sources there. The code isn't run from that location.


---
**Step Cia** *December 29, 2016 07:43*

ok did nano and added a new line I call it "DELTAPRINTR" all c values taken from steinhart calc.



so I'm not quite there yet? what do I need to do "make install"? sorry I'm complete noob here

![images/8bff047890525d3b8f84dfdc69bbffbf.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/8bff047890525d3b8f84dfdc69bbffbf.jpeg)


---
**Jon Charnas** *December 29, 2016 08:03*

you only added it to the original source files, not the actual code that is run. execute 'python [setup.py](http://setup.py) clean install' in the redeem folder


---
**Step Cia** *December 29, 2016 08:07*

Which redeem? The top one?


---
**Jon Charnas** *December 29, 2016 08:08*

yes top one


---
**Step Cia** *December 29, 2016 08:40*

done. did exactly that line and I can see the new thermistor line I put in. Thanks!

![images/31fb20f031b8daf98a638ac5a652b279.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/31fb20f031b8daf98a638ac5a652b279.jpeg)


---
**Step Cia** *December 31, 2016 06:09*

haha Semitec-104NT4 found the steinhart coefficient here



[smoothieware.org - Temperaturecontrol - Smoothie Project](http://smoothieware.org/temperaturecontrol#toc5)



I think I will change my values to follow the number shown there.


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/4keNDJ5R8q9) &mdash; content and formatting may not be reliable*
