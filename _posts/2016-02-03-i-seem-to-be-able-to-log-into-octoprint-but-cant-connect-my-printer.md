---
layout: post
title: "i seem to be able to log into octoprint but can't connect my printer"
date: February 03, 2016 22:31
category: "Discussion"
author: Hauvert Pacheco
---
i seem to be able to log into octoprint but can't connect my printer. any input would be nice. i already updated the erom





**Hauvert Pacheco**

---
---
**Elias Bakken** *February 03, 2016 22:33*

It's a good sign that you are seeing the /dev/octoprint_1 in the list of serial ports, but could you have a look at the redeem output log? 

systemctl status redeem -n 200


---
**Hauvert Pacheco** *February 03, 2016 22:35*

i typed in redeem in the terminal and the only warning i got was WARNING  Missing config file /etc/redeem/printer.cfg

could this be what I'm missing?


---
**Elias Bakken** *February 03, 2016 22:50*

No, not for running and connecting. That warning will go away once you choose a printer profile from the octoprint settings menu.


---
**Hauvert Pacheco** *February 03, 2016 22:54*

anyway i can reset the bbb? i messed around with it yesterday trying to flash it. perhaps i did not flash it correctly and erased certain files?


---
**Hauvert Pacheco** *February 04, 2016 02:43*

I can flash the regular beagle bone black fimage and again and then flask the kamikaze image after right? And see if it works then. 


---
**Jon Charnas** *February 04, 2016 09:24*

You should be able to just reflash the Kamikaze image over itself with no difficulties if that's what you want to do... Check on the wiki that you have the latest version, too (Jan 22nd)


---
**Hauvert Pacheco** *February 04, 2016 23:09*

Thats the first thing i tried. was reflashing it with Kamikaze but it still did nothing. i can log into octoprint but my connect the printer. no idea why. Serial Port has /dev/octoprint_1

and Baudrate at auto

and printer profile to default. 


---
**Jon Charnas** *February 05, 2016 10:55*

have you tried specifying a baud rate? I noticed sometimes octoprint's a bit tricky about that...


---
**Hauvert Pacheco** *February 05, 2016 18:55*

Yeah I selected each baud rate. I also tried to flash the bbb image on the bbb but it won't flash? When I remove the SD card and reboot it it goes back to kamikaze 


---
**Elias Bakken** *February 05, 2016 18:57*

After flashing, remove power, and then remove the card and apply power. The first time it will reboot after some config.


---
**Andy Tomascak** *February 05, 2016 21:32*

I have a similar issue. Octoprint appears to work/load and can be found at <BBB IP>:5000. However there is no serial port option other than "AUTOMATIC" and my printer is not recognized. Octoprint doesn't load from Operate either. Ideas?


---
**Andy Tomascak** *February 05, 2016 23:34*

I reflashed the Kamikaze image and it now connects after a second reboot. I didn't do ANY updates at all.


---
**Hauvert Pacheco** *February 06, 2016 18:35*

Elias, I already tried that. 



Andy, thats what happened to me so I reflashed it with kamikaze. It kept doing the same thing. so I tried flashing the bbb with the updated bbb firmware but won't. It does if I leave the SD card inside. But once I remove it and reboot it goes back to kamikaze. 


---
**Hitbox Alpha** *February 06, 2016 18:40*

Plus one to having this problem. i can get the screen to come up and display. but everytime i try to connect octo to it it just gives me a "failed to autodetect serial port. please manually enter port" in both the octo screen and a long the bottom of the BBB screen 


---
**Andy Tomascak** *February 06, 2016 18:43*

I only got the printer to connect with the fresh kamikaze image. My other issues are still present (Octoprint doesn't work from Operate, and Operate doesn't show any other options besides Octoprint.) I'm thinking the current Kamikaze image needs a bit of tuning. I'll be in #replicape on IRC (freenode) if anyone needs help or another head to bounce things off.


---
**Hauvert Pacheco** *February 06, 2016 18:44*

Mine can connect(log in) to octoprint but cant connect my printer to it. 



Zachary did you try reflashing as well? What about flashing it with the beagle board black updated firmware? 


---
**Andy Tomascak** *February 06, 2016 18:55*

If the BBB has Kamikaze flashed, and the Replicape has updated firmware and is properly recognized, you should see something like "/dev/octoprint_1" in the serial port dropdown. I can walk you though some of this in the irc channel if you need help.


---
**Hitbox Alpha** *February 06, 2016 19:02*

i reflashed kamikaze it still will not connect. no dev port found in the drop down. odd thing happened when i reflashed. one the flashing finished the usual scripts came up. but it failed to restart by itself. 

What is the IRC channel?


---
**Andy Tomascak** *February 06, 2016 19:03*

Freenode server, #replicape channel.


---
**Hauvert Pacheco** *February 06, 2016 19:04*

Andy Tomoscak mine shows dev/octoprint _1 but it still can't connect. 


---
**Andy Tomascak** *February 06, 2016 20:31*

Jump into the irc channel and I'll try to help. We just got Zac's trouble a little further along. :)


---
**Hauvert Pacheco** *February 06, 2016 21:33*

Is that like another group page? 


---
**Andy Tomascak** *February 06, 2016 21:36*

[https://en.m.wikipedia.org/wiki/Internet_Relay_Chat](https://en.m.wikipedia.org/wiki/Internet_Relay_Chat)

[https://webchat.freenode.net](https://webchat.freenode.net)


---
**Tim Curtis** *February 08, 2016 15:42*

With this issue coming up so much, maybe a better instruction page could be created to help everyone get there boards up and running? There are too many web pages to jump around on in my opinion.


---
**Andy Tomascak** *February 08, 2016 17:21*

It's a software issue with Redeem (or at least was for me). Typing the following should get you updated and on track:



Apt-get update

Apt-get install redeem



(Should say something about updating to version 1.7)



(Restart your BBB)



You should now be able to connect to your printer through Octoprint.




---
**Hauvert Pacheco** *February 10, 2016 17:11*

i still haven't been able to connect my printer to octoprint...i can log in but can't connect the printer.


---
*Imported from [Google+](https://plus.google.com/116125412087866821266/posts/7VLKXm36eqH) &mdash; content and formatting may not be reliable*
