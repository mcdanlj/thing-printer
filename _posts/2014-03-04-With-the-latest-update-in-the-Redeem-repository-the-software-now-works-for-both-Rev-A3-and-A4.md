---
layout: post
title: "With the latest update in the Redeem repository, the software now works for both Rev A3 and A4"
date: March 04, 2014 18:19
category: "Discussion"
author: Elias Bakken
---
With the latest update in the Redeem repository, the software now works for both Rev A3 and A4.  [https://bitbucket.org/intelligentagent/redeem](https://bitbucket.org/intelligentagent/redeem)





**Elias Bakken**

---
---
**James Armstrong** *March 04, 2014 18:21*

Thanks. I just saw that about 30 minutes ago. Much appreciated as it took some time to find the right flasher before (version). Went back and forth with someone yesterday trying to get it installed. 


---
**Elias Bakken** *March 04, 2014 19:16*

Glad you like it! You are one of the 3-4 people with a Rev A3A board, so this is one third to a quarter for you : )


---
**James Armstrong** *March 04, 2014 19:22*

Actually I think I ordered the A4 so I am (and two others) are basically just preparing for when we get the boards. Can't wait. 


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/ecvTncfx3Ln) &mdash; content and formatting may not be reliable*
