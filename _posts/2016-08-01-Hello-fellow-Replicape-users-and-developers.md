---
layout: post
title: "Hello fellow Replicape users and developers !"
date: August 01, 2016 17:38
category: "Discussion"
author: Max
---
Hello fellow Replicape users and developers !



Since everyone seems to hangout here and not in the forum I repost my problem here in Google+



I have just recieved my replicape Rev B3 but i haven’t been able to flash the Kamikaze image correctly to sd. Most of the time the flashing procedure runs trough without a problem. But after the validation i get error messages like this. 

![images/dcdeb5940e8c29cc7d7367b0d83dd16c.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/dcdeb5940e8c29cc7d7367b0d83dd16c.png)



**Max**

---
---
**Max** *August 01, 2016 17:39*

Things that i have checked/tried so far:



Etcher is running as admin.



I have tried 4 different sd cards, all have 4GB to 8GB. After every attempth to flash the image the sd card is corrupted and i have to rewrite the disk partition using diskpart.



i have tried flashing the image on two PC’s one running Win 10 and Win 7.



I have tried the “experimantal” an the “normal” kamikaze image.



I am using 7-zip to unpack the image.



I can flash the BeagleBoneBlack_Debian#Flashing_eMMC without a problem and also connect to the BBB afterwards.



Any help would be much apreciated.



Cheers!

Max


---
**Daniel Kusz** *August 01, 2016 20:01*

Use Win32diskimager. It works fine. I cannot burn image with Etcher too, but it doesn't want to even start burning process.


---
**Olahf84** *August 01, 2016 20:12*

I agree with Daniel I cant get etcher to work but win32 works great.




---
**Daniel Kusz** *August 01, 2016 20:14*

Elias, I think it will be good to put back info about win32 in wiki together with Etcher.


---
**Max** *August 01, 2016 22:26*

Yep, i've tried using win32diskimager to...just forgot to mention it. I can write the image to the sd and flash the Beagle bone. The LED's do their knight rider thing and light up at the end. I disconnect the usb, eject the sd card and reconnect the usb. The LED's start to flash and the Beagle bone is recognised as an external device briefly but dissapears again from the list of external devices. I can't connect over USB or Ethernet cable. And the LED's are flasching in a recurring pattern.


---
**Jon Charnas** *August 01, 2016 23:08*

Weird though, I've used Etcher several times without issue... But it may not be the very latest version. I've used 1.0.0-beta.10 on Windows 10 Pro and Ubuntu with no issues.


---
**Max** *August 02, 2016 18:13*

This is getting realy frustrating.



I can write the BeagleBoneBlack_Debian#Flashing_eMMC image to sd using Win32diskimager. Flash the BBB and connect afterwards.

With the Kamikaze image the flashing works well until the point where all LED's light up. after disconnecting and ejecting the SD, I reconnect the power and the LED's start to flash seemingly random for a few seconds. Shortly after it changes to a recurring pattern and the Beagle bone doesn't reboot as it should. The BBB is also not recognised as an USB device. I have pulled the replicape from the BBB, checked and cleaned every pin. Still nothing. 



Am I missing something or is there something wrong with my Replicape ?



Ideas and suggestions are much appreciated !



Cheers !



Max






---
**Daniel Kusz** *August 02, 2016 18:50*

Wait 2-3 minutes after flashing before you turn off the power and remove SD card. Do you have Manga screen? 


---
**Daniel Kusz** *August 02, 2016 19:37*

One more thing. After flashing and waiting some time try to connect with ethernet - it should works. To connect with USB needs some additional BBB configuration after flashing but I haven't tried it.


---
**Max** *August 05, 2016 10:29*

Hey Daniel,



I am now able to connect via Ethernet after a few tries. For some reason the connection was getting interrupded after a few minutes. I guess the same thing happend when I tried connecting via Ethernet the first time, wich left me thinking the board wasn't flashed correctly. After updating the packages over SSH the connection appears stable.



Thank you!


---
*Imported from [Google+](https://plus.google.com/115636610276456924336/posts/CKjZxRTmqca) &mdash; content and formatting may not be reliable*
