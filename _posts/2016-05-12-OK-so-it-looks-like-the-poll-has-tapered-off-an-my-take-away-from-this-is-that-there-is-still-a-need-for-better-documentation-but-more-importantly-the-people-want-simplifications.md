---
layout: post
title: "OK, so it looks like the poll has tapered off an my take away from this is that there is still a need for better documentation, but more importantly, the people want simplifications!"
date: May 12, 2016 17:04
category: "Discussion"
author: Elias Bakken
---
OK, so it looks like the poll has tapered off an my take away from this is that there is still a need for better documentation, but more importantly, the people want simplifications! 



Not too long ago, a big pull request was added to the Redeem mainline with improved algorithms for bed levelling. I have not tried these extensively, but I will go through them and try to document thoroughly how it works. Documentation will be provided in the form of a tutorial video and an updated wiki. 



The PID auto-tune could do with some more testing, some people are saying it works, while others are saying it is not working. An auto-tune will never be better than a manually well tuned PID, but it should be possible to get it to a point where it performs satisfactory at least for the most common hot ends. Once working, it needs some proper documentation as well! I'm thinking a video tutorial plus a good wiki section. 



After that, I'd like to spend some time moving to the new IoT image from BB.org as a basis for Kamikaze. It has hostAP for setting up wifi without cables now, so that is an interesting next step! Also, I'm hoping to install Redeem, Toggle and OctoPrint directly from git witch allows for simplified updating via OctoPrint and easier switch back and forth between develop and master branch. 



Other than that, a new minor revision of Replicape is under way, with some minor hardware updates. Hopefully we will see a new batch be ready in a few weeks. 



I'm also working on a new Manga Screen, bigger, better and only slightly more expensive : )



Follow along on the Replicape roadmap, I think even it's possible to vote now: [https://trello.com/b/kG6Fx7Ib/replicape-roadmap](https://trello.com/b/kG6Fx7Ib/replicape-roadmap)





**Elias Bakken**

---
---
**Pieter Koorts** *May 12, 2016 17:55*

Will the smaller Manga screen still be available when the bigger one is done? I have been watching your store but the current one seems to be out of stock. What would be estimated size of the redeveloped Manga screen since I am looking for a size in the 4 to 5 inch range which is what the current screen is?


---
**Elias Bakken** *May 12, 2016 19:21*

**+Pieter Koorts** I haven't decided exactly on the size of the new Manga, there might also be two different sizes, but the development screen is 5.9". A lot of people seem to like the 4.3"-ish size though, so hopefully there will be a replacement in that size range. 


---
**Step Cia** *December 30, 2016 18:30*

Can't +1 enough for better documentation :)


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/Lec7hd8fymT) &mdash; content and formatting may not be reliable*
