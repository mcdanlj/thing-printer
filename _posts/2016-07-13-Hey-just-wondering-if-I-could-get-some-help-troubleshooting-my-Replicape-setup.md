---
layout: post
title: "Hey, just wondering if I could get some help troubleshooting my Replicape setup"
date: July 13, 2016 19:13
category: "Discussion"
author: Samuel Kvarnbrink
---
Hey, just wondering if I could get some help troubleshooting my Replicape setup. The last two days, I've come back to failed prints, print head moving around mid-air, with bed and extruder heaters at room temp. Today, I watched it the whole time to see where the heck it went wrong. The OctoPrint log looks normal, but at the time point it failed the redeem log reads like this:



2016-07-13 18:55:35,083 root         WARNING  Disabling heaters

2016-07-13 18:55:35,094 root         ERROR    Alarm: Temperature falling too quickly (-11.0) for E



which seems totally bizarre to me. Is there any other log where I can find some logical explanation as to why the heaters are killed?





**Samuel Kvarnbrink**

---
---
**Jon Charnas** *July 13, 2016 19:24*

It's the temperature alarm. It does this because you had a sudden temp drop of 11C in a short time interval... If you have powerful fans it can easily happen. 2 things you can do: set your alarm threshold to 20C step in a 1s interval, run M303 with your cooling fans full on. Either one should help, both together will guarantee the problem won't repeat. But before you do that, double and triple check that your thermistor wires don't have a bad contact somewhere, that could lead to exactly this problem.


---
**Jon Charnas** *July 13, 2016 19:25*

Specifically, the temperature alarm was designed to prevent your printer from setting something afire if the thermistor were to fail... So verify this before changing the alarm threshold.


---
**Jon Charnas** *July 13, 2016 19:45*

It disables all heaters, but perhaps it's a flaw that needs to be fixed in redeem that it simply stops all motor movement as well until redeem is reset/told to continue. **+Elias Bakken**, one more issue for redeem I fear ;)


---
**Elias Bakken** *July 13, 2016 19:49*

It should send a "pause print" command to OctoPrint. You are running the latest Redeem? This happened to me the other day, when I had a blower aimed directly at a 25W hexagon hot end. The print did some sort of hickup, but did pause the print. 


---
**Elias Bakken** *July 13, 2016 19:50*

And yes, it is to prevent fires. Imagine if the thermistor cable breaks, that would be disasterous...




---
**Samuel Kvarnbrink** *July 13, 2016 19:58*

Ah, that makes sense! The bad news for me is that the fan wasn't running… time for a post mortem on the thermistor wire :) Thanks for the help!


---
*Imported from [Google+](https://plus.google.com/116914284093198004627/posts/HXGn8rMYWay) &mdash; content and formatting may not be reliable*
