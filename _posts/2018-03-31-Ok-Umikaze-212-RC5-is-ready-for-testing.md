---
layout: post
title: "Ok, Umikaze 2.1.2-RC5 is ready for testing"
date: March 31, 2018 01:55
category: "Discussion"
author: Jon Charnas
---
Ok, Umikaze 2.1.2-RC5 is ready for testing.



Fixed the backup script such that it will flash no matter what. However this means that you should <b>definitely</b> backup config files before, as they will be erased if they can't be read/backed up.



Bug reports here, on Slack or on github please!





**Jon Charnas**

---
---
**Elias Bakken** *March 31, 2018 02:58*

Nice! What kernel version is it? 




---
**Jon Charnas** *March 31, 2018 07:33*

Still 4.4.69. I couldn't get both BBB versions to play nice with a newer 4.4. next release will jump to 4.14


---
**Jon Charnas** *April 10, 2018 22:12*

For those who want to test the print-from-SD to avoid stutter, read this page please: [wiki.thing-printer.com - Print from sd - Thing-Printer](http://wiki.thing-printer.com/index.php?title=Print_from_sd)


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/WFH7sibvy2f) &mdash; content and formatting may not be reliable*
