---
layout: post
title: "I recently got my replicape in the mail and the power supply that came with my makergear m2 has positive, negative and ground"
date: February 04, 2016 22:06
category: "Discussion"
author: Matthew Palmore
---
I recently got my replicape in the mail and the power supply that came with my makergear m2 has positive, negative and ground. Dumb question but is ground necessary for the replicape? Also on the PCB board how can i tell which port is positive and which is negative? Thank you!





**Matthew Palmore**

---
---
**Elias Bakken** *February 04, 2016 22:09*

Hi! Does it have a negative voltage? What is that needed for? I would use the Ground + positive and leave the negative unconnected. There are markings on the PCB, just next to the fuse on what is + and -.




---
**Matthew Palmore** *February 05, 2016 01:01*

Ok, I found out there are 6 ports that output 3 positive and 3 ground terminals (all at 24V). So i just need to use 2 of them?


---
**Elias Bakken** *February 05, 2016 08:33*

Yeah, that sounds more normal. Use 2  yes. 


---
**Dave Posey** *February 05, 2016 13:56*

3 Power ports would agree with the in WikiDot site for the M2 ([http://makergear.wikidot.com/m2-electronics](http://makergear.wikidot.com/m2-electronics)). However, it shows that the power is a +12VDC port for the Heat Bed and Two +24V ports for Motors & everything else.  



In the case of the Replicape, +12V or +24V will work, but if you use parallel wiring (Replicape has parallel VIN) for current, use a Voltmeter and make sure that you are grabbing 24VDC for both ports.


---
*Imported from [Google+](https://plus.google.com/103569827835970105061/posts/h6d4v7CujTh) &mdash; content and formatting may not be reliable*
