---
layout: post
title: "I am working on homing my CoreXY printer, with a dual Z and H axis, with a properly setup slave"
date: February 20, 2016 03:28
category: "Discussion"
author: Philip Acierto
---
I am working on homing my CoreXY printer, with a dual Z and H axis, with a properly setup slave. Pressing the up and down arrows on octoprint correctly moves both the Z and H together.



When I press home, for the Z axis, only the Z moves, which tilts the bed because for some reason H on slave no longer follows it. Anyone got any ideas?



Also( I would imagine this is an issue for most printers) when I home Z, I need it to not home unless first it finds X and Y correctly zeroed, and then move to a specified X and Y to begin the Z home, so that my bed is actually under the inductive sensor. This issue is compounded by the fact I have a glass bed, so for homing and for auto bed leveling, I need it to target specific points that I will have covered with aluminum tape.



Homing X and Y does happen correctly when I home with octoprint for those two axis, verified. I just need to see if I can ensure any Z homing takes place at a particlar place on the print bed to avoid it bottoming out on the hot end.





**Philip Acierto**

---
---
**Elias Bakken** *February 20, 2016 04:20*

I'm not at my computer, but this was a tricky one. Steppers move right both up and down? And they both stop when you press the endstop switch?


---
**Philip Acierto** *February 20, 2016 04:41*

Yes. They move up and down together. H is set as a slave to Z.



I use octoprint to move Z up and down, and the H set as slave follows it correctly. When I use home command for Z via octoprint however, only the Z stepper moves.


---
**Philip Acierto** *February 20, 2016 04:43*

and, tbh, I had to set the following for the inductive end stop:





end_stop_Z2_stops = z_neg, h_neg



otherwise H would continue to move even if Z stopped. Seems there might be some slave functionality not fully working, but didnt want to start digging into a rabbit hole if there was something simple I was missing.




---
**Philip Acierto** *February 20, 2016 09:59*

Alright, played around with G29 customizing, but it's not doing things quite right, and I am curious still about making a way that "G28 Z0 ; Home Z" only is able to do so at particular locations on the X and Y.



So it seems like I could use some help.


---
**Elias Bakken** *February 20, 2016 14:26*

Which printer is this? You can only home z when x and y are at certain (known) locations? Is it an option to install a second endstop for z at the top maybe? There is not support for homing z given certain criterias, no... 


---
**Philip Acierto** *February 20, 2016 17:54*

This is a custom corexy. It has a glass heatbed, so in order to Home Z on it with the inductive sensor, it needs to do so over areas I have placed aluminum tape on the glass top (corners).



I could just have custom macro I suppose for homing, that moves the x and y first, but I was trying to see if I could overwrite the g code that homes z so I could insure I never have crashing issues, in theory.



How about just the homing in general though? How do I get z and h to move together while issueing "home z" G28 z0?


---
*Imported from [Google+](https://plus.google.com/111974602640488998735/posts/MCopmXRdAZr) &mdash; content and formatting may not be reliable*
