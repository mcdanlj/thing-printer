---
layout: post
title: "My first fast insights from Kamikaze 2.0.8: - File printer.cfg not read clearly"
date: October 23, 2016 18:14
category: "Discussion"
author: Daniel Kusz
---
My first fast insights from Kamikaze 2.0.8:

- File printer.cfg not read clearly. First, I have swapped axes X and Y. When I changed the direction of one of the motors I have properly axes, but the X-axis direction was the opposite. In the previous Kamikaze my file printer.cfg work properly.

- After a few restarts of Redeem I lose my connection and can't restore it again. After turning off/on the printer can be heard known sound of the engines, which does not stop. In Octoprint I get information: There was an unhandled error while talking to the printer. Due to That OctoPrint disconnected. Error: Failed to autodetect serial port, please set it manually. It happened twice (I have reinstaled Kamikaze one time after this situation).  And one time after one restart on screen was this picture:



![images/6d954d012e5ed9aa252cbd2b6b02be4d.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/6d954d012e5ed9aa252cbd2b6b02be4d.jpeg)



**Daniel Kusz**

---
---
**Jon Charnas** *October 23, 2016 18:26*

I'm going to guess that some configuration parameters that you were using changed... Normally your changes go in local.cfg and you always use the printer.cfg that comes with the redeem version you're running. If you have redeem 1.2.8 (default on kamikaze 2.0.8) running with a config file for an older version of redeem, that would be your problem - redeem just crashes.


---
**Jon Charnas** *October 23, 2016 18:27*

local.cfg values always override any other values... But usually a number of defaults are set in printer.cfg for redeem.


---
**Daniel Kusz** *October 23, 2016 19:26*

OK. One more question: how to change direction only for X axis in H-bot? I want only change direction of 2 motors rotating together in X axis for H-bot.


---
**Daniel Kusz** *October 23, 2016 19:41*

Again: There was an unhandled error while talking to the printer. Due to that OctoPrint disconnected. Error: Failed to autodetect serial port, please set it manually.



How to restore connection?


---
**Elias Bakken** *October 24, 2016 01:45*

The error is probably due to redeem crashing. Could you check the log to see if there is maybe a spelling error in the config file or something?


---
**Daniel Kusz** *October 24, 2016 06:03*

You're right. Reversing of the X axis in Octoprint did not work so I tried to change settings and changed probably microsteping to inadequate. I just need information how to reverse the direction of the X axis for h-bot in the configuration files?


---
**Elias Bakken** *October 24, 2016 07:45*

Assuming your x-axis is the boom in the center, you need to change the direction of stepper x and y. 

[Steppers]

direction_x = -1

direction_y = -1



Right?


---
**Elias Bakken** *October 24, 2016 12:57*

Also, if you update Kamikaze through the OctoPrint web interface, the u-boot should now disable the steppers early on. 




---
**Daniel Kusz** *October 24, 2016 19:14*

**+Elias Bakken** 

The situation looks like this:

1. direction_x = 1; direction_y = 1 - push right carriage go left and vice versa, push up the carriage go up and vice versa

2. direction_x = -1; direction_y = -1 - push right carriage go right and vice versa, push up the carriage go down and vice versa

3. direction_x = 1; direction_y = -1 - push right carriage go down and vice versa, push up the carriage go right and vice versa

4. direction_x = -1; direction_y = 1 - push right carriage go up and vice versa, push up the carriage go left and vice versa



Configurations 1 and 2 are good and I need only to change direction of one of the axis in this two configurations. Invert control in Octoprint does not work. Have I missed something?


---
**Daniel Kusz** *October 24, 2016 19:34*

**+Elias Bakken** Regarding the Kamikaze update every time you start Octoprint he asks about update again. When turning on the printer still hear noise of the engine probably Z (not so loud). Probably because of the loss of an update.


---
**Daniel Kusz** *October 24, 2016 19:46*

Hmmm very strange. I tried again to reverse the X-axis and now works :O


---
**Elias Bakken** *October 24, 2016 20:33*

You need to run the update manually I'm afraid. Go to /usr/src/Kamikaze2 and run the update script.


---
**Daniel Kusz** *October 24, 2016 20:55*

First calibration cube and printing speeds are much too high than normal and printer prints in lower left corner instead of center. Tomorrow I will do more tests.


---
**Daniel Kusz** *October 25, 2016 16:25*

Elias, when my home_speed_x = -0.1 then

home_x can be = -0.01??? I have inverted X axis.


---
**Elias Bakken** *October 25, 2016 17:56*

I'm sure that's fine, but normally the speed is kept positive and the travel_* is changed.


---
**Daniel Kusz** *October 25, 2016 19:20*

For home_speed_x = -0.1 and home_x = -0.01homing work properly (0 point i there where i want) but printing is not i center of bed but in lower left corner. I think that this is due to the reversal of the x-axis. And printing speed is 50% faster than from slicer I think (desktop cura) = low quality prints.



Is it so that Redeem in Kamikaze 2.0 otherwise interpret some of the settings from old Kamikaze (at least for me)? I understand that due to the recent changes and not result of errors?



In old Kamikaze was direction_x =  -1

direction_y =  1 and everything were fine. Now direction_x = 1; direction_y = 1 - push right carriage go left and vice versa, push up the carriage go up and vice versa. X axis need to be inverted, so home speed is should be inverted with home_x = -0.01, but this is searching for maximum end what I think is wrong.



I see lot of improvements in new Kamikaze but it would be great to solve the problem of axles for HBOT. I dont want install old Kamikaze ;)






---
**Daniel Kusz** *October 26, 2016 16:02*

1. Is endstop X1 always the minimum and at the same time X2 is the maximum or it depends only on home_speed? If home_speed is with '+' sign encountered endstop always is minimal? It does not matter X1 or X2?

2. Your The Thing was able to correctly configure the Kamikaze 2.0.8?


---
*Imported from [Google+](https://plus.google.com/103041373788524358316/posts/eNyJZRuNinj) &mdash; content and formatting may not be reliable*
