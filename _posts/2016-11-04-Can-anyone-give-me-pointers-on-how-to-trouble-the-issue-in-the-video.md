---
layout: post
title: "Can anyone give me pointers on how to trouble the issue in the video?"
date: November 04, 2016 21:26
category: "Discussion"
author: Jim Nofsinger
---
Can anyone give me pointers on how to trouble the issue in the video?  basically movements to Y are slopped pretty badly (up).  Here is what I checked so far,



Homing XYZ works good.  Printer defaults .01 above bed

I can move the effector across the entire bed manually and it works without crashing into the bed (it is flat)



Checked belt tension and it is good



Checked all towers for proper level/height and it is good



Checked soft min and max endstops (all movement works) although it could be fine tuned



steps are tuned 100mm moves 100mm



This behavior is present when I print, or go to level the bed.  I am well aware I messed something up, but can't figure where to look next.  Any help would be greatly appreciated!  thanks



[Geometry]

axis_config = 3

travel_x = -0.6

travel_y = -0.6

travel_z = -0.6

offset_x = -0.329

offset_y = -0.329

offset_z = -0.329



[Delta]

hez = 0.055

l = 0.27

r = 0.14575

ae = 0.023

be = 0.023

ce = 0.023



soft_end_stop_min_x = -0.06

soft_end_stop_min_y = -0.06

soft_end_stop_min_z = -0.001

soft_end_stop_max_x = 0.06

soft_end_stop_max_y = 0.06

soft_end_stop_max_z = 0.329



[Homing]

home_x = 0

home_y = 0

home_z = .01



[Probe]

length = 0.01

speed = 0.05

accel = 0.1

offset_x = 0.0

offset_y = 0.0

![images/39b5196135b0b0c017e1143823ce3819.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/39b5196135b0b0c017e1143823ce3819.jpeg)



**Jim Nofsinger**

---
---
**Jon Charnas** *November 04, 2016 21:34*

Try adjusting your Delta radius and rod lengths


---
**Jon Charnas** *November 04, 2016 21:35*

See the Delta calibration guide on redeem's wiki page


---
**Jon Charnas** *November 04, 2016 21:36*

And if you get a proper move without rubbing a calibration routine, try printing without launching one... If it's trying to calibrate and mucks up the values for Delta radius and length in memory you'd see that


---
**Jim Nofsinger** *November 07, 2016 17:00*

Indeed Branden!  trying to get this thing dialed in.  I think the nozzle was dragging a bit at the start of the print, which made it skippish.  Right now I have the power settings at .5 had them at .750 also. 



update I added .010 to the radius (only) and tried to print the cube calibration.  it printed a flat triangle instead...

do not fully understand that.



Today I will try to adjust the rod length and radius.


---
**Jim Nofsinger** *November 07, 2016 17:00*

oops forgot to mention, thanks for the suggestions Jon


---
*Imported from [Google+](https://plus.google.com/118269177478098269592/posts/NniV4h9jQCy) &mdash; content and formatting may not be reliable*
