---
layout: post
title: "Kamikze 2.1.0 is out! Remember to read up on the Kamikaze 2.1.0 wiki page, and fire away"
date: January 26, 2017 09:07
category: "Discussion"
author: Jon Charnas
---
Kamikze 2.1.0 is out!



Remember to read up on the Kamikaze 2.1.0 wiki page, and fire away.

Release notes also have instructions on how to upgrade from a previous version if you want to (or just download your local.cfg and re-initialize octoprint, that's cool too)





**Jon Charnas**

---
---
**Step Cia** *January 26, 2017 16:51*

Is this weekend already? :)


---
**Jon Charnas** *January 26, 2017 18:42*

It must be somewhere, 😎


---
**Jon Charnas** *January 26, 2017 20:33*

Apologies, but it seems the image is bad - a bug slipped through and the image won't flash! I'm working on a fix now.


---
**Jon Charnas** *January 26, 2017 23:09*

Uploaded a fixed version! Please make sure that you use the file ending in version 2.1.0a. My apologies for the problem!


---
**Jon Charnas** *January 27, 2017 17:50*

So I've added a section on how to backup confusions to the release notes.


---
**Jon Charnas** *January 27, 2017 17:51*

Those will work on 2.1.0 as well


---
**Jon Charnas** *January 27, 2017 18:29*

unfortunately quite a few. Nothing you can't do by hand if you're really pressed for time. Did you have the original 2.1.0 image then? or was it 2.1.0a? For 2.1.0a the fixes are straightforward and involve just copying a couple files into place again (octoprint config.yaml if you don't have yours, and the octoprint.service for systemd are two that we know were corrupted). Perhaps we can help you faster in the  #suppor  channel on slack?


---
**Jon Charnas** *January 28, 2017 09:50*

Not yet. I've got a working SD card with the new image on it, but I'm having difficulties extracting it into a file. At 4 am I decided I needed rest before I messed up again... I'll try to upload an image today


---
**Jon Charnas** *January 29, 2017 00:20*

I just finished confirming the image I built this time is good, it should be uploaded on github in a few more minutes. Sorry for the trouble, but I hope the effort devoted to fixing it right this time will lead to not running into the same problem again next time. I've spun off the SD extraction software into its own little project, because I saw nothing to help on Google when I was looking for indications on how to do it... Cheers guys!


---
**Jon Charnas** *January 29, 2017 10:39*

It's confirmed - the new image is printing and working for others :) Sorry for the troublesome release process!


---
**Jon Charnas** *January 29, 2017 11:21*

Correct - if you only want to run from SD, commenting that line will boot from SD instead of flashing.


---
**Step Cia** *January 29, 2017 18:50*

Is that a good way to test new version by booting to SD?


---
**Jon Charnas** *January 29, 2017 18:53*

It is, but it won't be as fast as running from the eMMC. If you back up the files as per the release notes you'll be better off flashing it and then you can always restore 2.0.8 if that's not to your liking 


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/QySQ63W42A7) &mdash; content and formatting may not be reliable*
