---
layout: post
title: "Replicape case with forced air cooling. Hi All, I've made a Replicape case design, which has a little \"wind tunnel\" inside - to ensure forced air cooling all around the Replicape board"
date: April 16, 2018 23:12
category: "Discussion"
author: László Szekeres
---
Replicape case with forced air cooling.



Hi All, 



I've made a Replicape case design, which has a little "wind tunnel" inside - to ensure forced air cooling all around the Replicape board. The air is pushed from the top and leaving the box through the space in-between Replicape and BBB. This way all stepper drivers are cooled from both side, which is especially effective if some heatsinks are attached on the bottom side of the PCB (as in my case). Take a look and use if you like it.



The top part is actually fixed by screws to the bottom (I prefer stability...), but can be easily modified to click onto it.



Link: [https://www.thingiverse.com/thing:2855558](https://www.thingiverse.com/thing:2855558)







![images/349ae58042e3260113a616020df9694b.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/349ae58042e3260113a616020df9694b.jpeg)
![images/340c7b128683ed1c8f847ab3f622a7c5.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/340c7b128683ed1c8f847ab3f622a7c5.jpeg)

**László Szekeres**

---
---
**Mark Loit (CanadianAvenger)** *April 17, 2018 04:36*

Nice work! I was going to laser cut something from acrylic along the same lines. I'm seriously considering printing your design instead now.


---
**Michael K Johnson** *April 17, 2018 09:55*

I like your idea of putting labels on the top instead of the side like I did with replicase.


---
**Chris Deister** *May 13, 2018 00:34*

Great design. Well thought out. Thanks!


---
**Jeremiah Coley** *June 17, 2018 11:26*

Love it ... What is that large capacitor for?


---
**László Szekeres** *June 17, 2018 12:42*

The big capacitor is just for additional filtering/buffering for the power supply. I just feel better in this way (based on my experience), actually no big scientific calculations behind... I had two empty terminal on the power connector - as I don't need high currents (using SSR for heated bed), so I've just used it for the capacitor.


---
*Imported from [Google+](https://plus.google.com/101242932785846122477/posts/PuRBfUnGGHC) &mdash; content and formatting may not be reliable*
