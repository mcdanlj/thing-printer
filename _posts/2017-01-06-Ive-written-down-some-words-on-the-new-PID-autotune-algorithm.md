---
layout: post
title: "I've written down some words on the \"new\" PID autotune algorithm"
date: January 06, 2017 15:49
category: "Discussion"
author: Elias Bakken
---
I've written down some words on the "new" PID autotune algorithm. I've had this post in my backlog for a while, but at least it is finally out now. 

[http://www.thing-printer.com/pid-autotune-algorithm-improvements/](http://www.thing-printer.com/pid-autotune-algorithm-improvements/)





**Elias Bakken**

---
---
**Elias Bakken** *January 06, 2017 15:51*

**+Step Cia**, the bug with only four cycles should be pushed into develop now, so you can test that and see if you get a better temperature curve with more cycles on your aggressive heater!




---
**Step Cia** *January 06, 2017 16:38*

👍thanks 


---
**Jonathan Cohen** *July 03, 2017 14:01*

Is that patch in the main branch now ?


---
**Elias Bakken** *July 03, 2017 15:26*

Master branch is still very outdated! I think it's still only in develop, but we should be getting close to a new release soon though.


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/jb7pJn7RLMD) &mdash; content and formatting may not be reliable*
