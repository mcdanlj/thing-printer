---
layout: post
title: "So I'm having difficulties connecting to the /dev/octoprint_1 because it only shows AUTO in the serial port option"
date: August 21, 2017 03:19
category: "Discussion"
author: Matthew Palmore
---
So I'm having difficulties connecting to the /dev/octoprint_1 because it only shows AUTO in the serial port option. I've tried reinstalling kamikaze and still nothing. The plugin redeem log reads as follows:



 2016-10-17 18:34:49,887 root         INFO     Stepper watchdog started, timeout 500 s

 2016-10-17 18:34:49,892 root         WARNING  USB gadget serial not available as /dev/ttyGS0

 2016-10-17 18:34:49,896 root         INFO     Ethernet bound to port 50000

 2016-10-17 18:34:50,074 root         INFO     Pipe octoprint open. Use '/dev/octoprint_1' to communicate with it

 2016-10-17 18:34:50,234 root         INFO     Pipe toggle open. Use '/dev/toggle_1' to communicate with it

 2016-10-17 18:34:50,394 root         INFO     Pipe testing open. Use '/dev/testing_1' to communicate with it

 2016-10-17 18:34:50,564 root         INFO     Pipe testing_noret open. Use '/dev/testing_noret_1' to communicate with it

 2016-10-17 18:34:50,588 root         INFO     Alarm: Operational

 2016-10-17 18:34:50,596 root         INFO     Watchdog started, refresh 30 s

 2016-10-17 18:34:50,600 root         INFO     Redeem ready

 2016-10-17 18:36:26,592 root         INFO     -- Logfile configured --

 2016-10-17 18:36:27,233 root         INFO     Found Replicape rev. 0B3A

 2016-10-17 18:36:27,431 root         INFO     End Stop Y2 hit!

 2016-10-17 18:36:28,336 root         INFO     Stepper watchdog started, timeout 500 s

 2016-10-17 18:36:28,350 root         WARNING  USB gadget serial not available as /dev/ttyGS0

 2016-10-17 18:36:28,355 root         INFO     Ethernet bound to port 50000

 2016-10-17 18:36:28,571 root         INFO     Pipe octoprint open. Use '/dev/octoprint_1' to communicate with it

 2016-10-17 18:36:28,774 root         INFO     Pipe toggle open. Use '/dev/toggle_1' to communicate with it

 2016-10-17 18:36:28,995 root         INFO     Pipe testing open. Use '/dev/testing_1' to communicate with it

 2016-10-17 18:36:29,193 root         INFO     Pipe testing_noret open. Use '/dev/testing_noret_1' to communicate with it

 2016-10-17 18:36:29,214 root         INFO     Alarm: Operational

 2016-10-17 18:36:29,243 root         INFO     Watchdog started, refresh 30 s

 2016-10-17 18:36:29,248 root         INFO     Redeem ready

 2016-10-17 18:45:08,257 root         INFO     -- Logfile configured --

 2016-10-17 18:45:08,883 root         INFO     Found Replicape rev. 0B3A

 2016-10-17 18:45:09,085 root         INFO     End Stop Y2 hit!

 2016-10-17 18:45:09,947 root         INFO     Stepper watchdog started, timeout 500 s

 2016-10-17 18:45:09,963 root         WARNING  USB gadget serial not available as /dev/ttyGS0

 2016-10-17 18:45:09,970 root         INFO     Ethernet bound to port 50000

 2016-10-17 18:45:10,178 root         INFO     Pipe octoprint open. Use '/dev/octoprint_1' to communicate with it

 2016-10-17 18:45:10,400 root         INFO     Pipe toggle open. Use '/dev/toggle_1' to communicate with it

 2016-10-17 18:45:10,592 root         INFO     Pipe testing open. Use '/dev/testing_1' to communicate with it

 2016-10-17 18:45:10,779 root         INFO     Pipe testing_noret open. Use '/dev/testing_noret_1' to communicate with it

 2016-10-17 18:45:10,822 root         INFO     Alarm: Operational

 2016-10-17 18:45:10,845 root         INFO     Watchdog started, refresh 30 s

 2016-10-17 18:45:10,864 root         INFO     Redeem ready



Please help!





**Matthew Palmore**

---
---
**Jon Charnas** *August 21, 2017 04:48*

It looks like redeem isn't started ever.... Look at the date of the log, that's left over from the image creation. Can you try looking at the output of "systemctl status redeem -l -n 100"?


---
**Jon Charnas** *August 21, 2017 14:44*

**+Nathan Reichenberger** lastest version of redeem in the master or develop branches? If develop it won't start because it's missing one of the new library dependencies... you can fix it by running "pip install sympy" first.


---
**Matthew Palmore** *August 21, 2017 14:56*

How do I give it terminal commands if I can't get to a terminal window via BBB? I've got a Mac, so is there any way I can control it via the terminal window on my Mac?


---
**Jon Charnas** *August 21, 2017 15:02*

Uh, yes, sorry I forget to say it nowadays - usually we remote-login with SSH. Instructions and login info are here: [wiki.thing-printer.com - Redeem - Thing-Printer](http://wiki.thing-printer.com/index.php?title=Redeem#Troubleshooting)


---
**Matthew Palmore** *August 21, 2017 15:06*

Awesome! Thank you! Just tagging onto my comment about changing the code for the screen, would I use the same method?


---
**Jon Charnas** *August 21, 2017 15:08*

Yep! Pretty much all interactions go through SSH or Octoprint's web UI, I think I've never actually connected a keyboard to my BBBs (and I build the Kamikaze/Umikaze images ;) )


---
**Matthew Palmore** *August 22, 2017 01:13*

root@kamikaze:~# systemctl status redeem -l -n 100

● redeem.service - The Replicape Daemon

   Loaded: loaded (/lib/systemd/system/redeem.service; enabled; vendor preset: enabled)

   Active: inactive (dead)



Aug 22 01:00:41 kamikaze systemd[1]: Dependency failed for The Replicape Daemon.

Aug 22 01:00:41 kamikaze systemd[1]: redeem.service: Job redeem.service/start failed with result 'dependency'.



So it looks like you were correct that redeeem didn't even start. I ran "pip install sympy" which installed version 8.1.2 and then upgraded it to version 9.0.1 and both showed the same message as shown above. Where would I start to try and find/fix the problem?


---
**Matthew Palmore** *August 22, 2017 02:51*

● redeem.service - The Replicape Dameon

   Loaded: loaded (/lib/systemd/system/redeem.service; enabled; vendor preset: enabled)

   Active: failed (Result: exit-code) since Tue 2017-08-22 02:48:14 UTC; 1min 10s ago

  Process: 1238 ExecStart=/usr/bin/redeem (code=exited, status=1/FAILURE)

 Main PID: 1238 (code=exited, status=1/FAILURE)



Aug 22 02:48:10 kamikaze systemd[1]: Started The Replicape Dameon.

Aug 22 02:48:14 kamikaze redeem[1238]: Traceback (most recent call last):

Aug 22 02:48:14 kamikaze redeem[1238]:   File "/usr/bin/redeem", line 9, in <module>

Aug 22 02:48:14 kamikaze redeem[1238]:     load_entry_point('Redeem==1.2.2', 'console_scripts', 'redeem')()

Aug 22 02:48:14 kamikaze redeem[1238]:   File "/usr/lib/python2.7/dist-packages/pkg_resources/__init__.py", line 567, in load_entry_point

Aug 22 02:48:14 kamikaze redeem[1238]:     return get_distribution(dist).load_entry_point(group, name)

Aug 22 02:48:14 kamikaze redeem[1238]:   File "/usr/lib/python2.7/dist-packages/pkg_resources/__init__.py", line 2604, in load_entry_point

Aug 22 02:48:14 kamikaze redeem[1238]:     return ep.load()

Aug 22 02:48:14 kamikaze redeem[1238]:   File "/usr/lib/python2.7/dist-packages/pkg_resources/__init__.py", line 2264, in load

Aug 22 02:48:14 kamikaze redeem[1238]:     return self.resolve()

Aug 22 02:48:14 kamikaze redeem[1238]:   File "/usr/lib/python2.7/dist-packages/pkg_resources/__init__.py", line 2270, in resolve

Aug 22 02:48:14 kamikaze redeem[1238]:     module = <i>_import_</i>(self.module_name, fromlist=['__name__'], level=0)

Aug 22 02:48:14 kamikaze redeem[1238]:   File "/usr/lib/python2.7/dist-packages/redeem/Redeem.py", line 40, in <module>

Aug 22 02:48:14 kamikaze redeem[1238]:     from Stepper import *

Aug 22 02:48:14 kamikaze redeem[1238]:   File "/usr/lib/python2.7/dist-packages/redeem/Stepper.py", line 28, in <module>

Aug 22 02:48:14 kamikaze redeem[1238]:     from ShiftRegister import ShiftRegister

Aug 22 02:48:14 kamikaze redeem[1238]:   File "/usr/lib/python2.7/dist-packages/redeem/ShiftRegister.py", line 40, in <module>

Aug 22 02:48:14 kamikaze redeem[1238]:     spi.bpw = 8

Aug 22 02:48:14 kamikaze redeem[1238]: AttributeError: 'NoneType' object has no attribute 'bpw'

Aug 22 02:48:14 kamikaze systemd[1]: redeem.service: Main process exited, code=exited, status=1/FAILURE

Aug 22 02:48:14 kamikaze systemd[1]: redeem.service: Unit entered failed state.

Aug 22 02:48:14 kamikaze systemd[1]: redeem.service: Failed with result 'exit-code'.





after trying to manually install the package feed twice, this is now what is happening


---
**Jon Charnas** *August 22, 2017 05:02*

Uh which package? The Debian package is completely outdated, redeem is install from git directly, with the sources in /use/src/redeem.



Can you clarify which version of the image you're installing and on what type of beagle one (black Ethernet or black wireless?)


---
**Matthew Palmore** *August 22, 2017 18:41*

Wait, when downloading the ~1GB size kamikaze image on the microSD card does that have the version of linux I need? Or do I need to install something onto the BBB before doing the kamikaze flash? The BBB I'm using is the Ethernet version, should I be using the wireless version?



I flashed the Kamikaze-2.0.8-2016-10-17.img.xz file onto my BBB


---
*Imported from [Google+](https://plus.google.com/103569827835970105061/posts/Rgcqgs3rq52) &mdash; content and formatting may not be reliable*
