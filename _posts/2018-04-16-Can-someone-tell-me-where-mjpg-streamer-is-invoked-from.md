---
layout: post
title: "Can someone tell me where mjpg-streamer is invoked from?"
date: April 16, 2018 04:52
category: "Discussion"
author: Mark Loit (CanadianAvenger)
---
Can someone tell me where mjpg-streamer is invoked from? I'm trying to locate it so I can add some customized parameters to suit my webcam. Currently I get an image, but at the wrong aspect/resolution. All the documents I've been able to locate seem to be centered around  the pi, and specifically "octopi", but I haven't been able to locate any corresponding files under Umikaze yet. [I'm currently running 2.1.2rc4]





**Mark Loit (CanadianAvenger)**

---
---
**Jon Charnas** *April 16, 2018 05:29*

It's called by systemd, edit the file /lib/systemd/system/mjpg.service to add extra options. Note that the version in 2.1.1 and before is old. I updated it in 2.1.2 RC5c.


---
**Mark Loit (CanadianAvenger)** *April 16, 2018 05:54*

Awesome, worked perfectly... thanks!


---
**Mark Loit (CanadianAvenger)** *April 17, 2018 04:29*

Just to follow up, for any future travelers in this area. I was able to get my HP HD 4310 webcam running in 1920x1080 @15fps by modifying the mjpg-streamer invocation as follows:

ExecStart=/usr/local/bin/mjpg_streamer -i "/usr/local/lib/mjpg-streamer/input_uvc.so -r 1920x1080 -f 15" -o "/usr/local/lib/mjpg-streamer/output_http.so"



Note that the "-f 15" is required, I was unable to get the camera to work at full resolution at other rates, or via the default/auto rate [by omitting the rate]. Also note that setting it to less [like 5] will result in a message that says "FPS coerced ......: from 5 to 15", but the picture will still not work for some reason.


---
*Imported from [Google+](https://plus.google.com/114349838982694099342/posts/1LdPGGGU75q) &mdash; content and formatting may not be reliable*
