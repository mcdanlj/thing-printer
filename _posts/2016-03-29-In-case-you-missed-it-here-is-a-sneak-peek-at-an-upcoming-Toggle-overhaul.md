---
layout: post
title: "In case you missed it, here is a sneak peek at an upcoming Toggle overhaul"
date: March 29, 2016 23:55
category: "Discussion"
author: Elias Bakken
---
In case you missed it, here is a sneak peek at an upcoming Toggle overhaul.

![images/ca35d3ca0cad2fde367b27c567f63f59.gif](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/ca35d3ca0cad2fde367b27c567f63f59.gif)



**Elias Bakken**

---
---
**Ante Vukorepa** *March 30, 2016 01:13*

I like the new built-in sound effects.


---
**Veridico Cholo** *April 01, 2016 15:50*

Yes, very nice sound effects! 😀

Is there motion control capability via Manga Screen yet?


---
**Elias Bakken** *April 01, 2016 15:53*

The sound effects were the hardest part!

Yes, Jog is implemented as well.


---
**Veridico Cholo** *April 01, 2016 15:55*

Fantastic!!!! 

When is the release?


---
**Elias Bakken** *April 01, 2016 15:56*

You can run it from git right away if you want, or I'm planning a release some time the coming week.


---
**Veridico Cholo** *April 03, 2016 12:39*

Downloaded it from git and:



1. Setup.py points at a couple of png files that no longer exist. I just removed them.



2. Toggle seems to require the cairo python module and I don't have it installed


---
**Elias Bakken** *April 03, 2016 12:59*

apt-get install python-gi-cairo python-cairo


---
**Veridico Cholo** *April 03, 2016 13:02*

Cool! Thanks Elias. I will get to it later today. I saw cairo missing and then I stopped


---
**Elias Bakken** *April 03, 2016 13:05*

I added some more icons as well, so just do a git pull and let me know what else is missing. It's probably some more things!


---
**Veridico Cholo** *April 03, 2016 15:43*

It looks good! It is up and running. It shows up on the manga screen. A few comments:

1. In the temperature screen, the temp graph and the temp values display update great but only when the temperatures are set up in Octoprint. I can't initiate or stop the heat-up of the bed or hotend via touch screen. When you do touch the icons, they briefly change to yellow but nothing happens. 



2. The jog screen looks great as well but nothing happens when the icons are touched. I am thinking I need to update redeem as well for the touch funcionality to work? I only updated Toggle.



3. The screen that shows the model on the bed also renders fine but the model does not load. It seems that it it trying to load the models but it can't. 



Is any of this expected?

Let me know if you want to see the log


---
**Elias Bakken** *April 04, 2016 01:28*

Do you have an icon in the bottom left corner saying disconnected? If so that would explain a lot. Right now you need the web interface for connecting to the printer. You can enable autoconnect in octoprint from the web interface, but not yet from toggle.


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/YPffmDPvTf1) &mdash; content and formatting may not be reliable*
