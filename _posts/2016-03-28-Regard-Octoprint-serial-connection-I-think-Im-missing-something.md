---
layout: post
title: "Regard Octoprint serial connection. I think I'm missing something"
date: March 28, 2016 13:35
category: "Discussion"
author: Jonathan Olesik
---
Regard Octoprint serial connection. I think I'm missing something. Since the replicape/BBB is the  motor driver and octoprint host how am I supposed to make a serial connection between the two? Octoprint does not have any COM ports available for connection. Auto detect doesn't work either...  

Any suggestions? 





**Jonathan Olesik**

---
---
**Elias Bakken** *March 28, 2016 14:02*

If you are not seeing a serial connection called octoprint_1, it is most likely that redeem has encountered an error. Check the log.


---
**Jonathan Olesik** *March 28, 2016 14:08*

dmesg logs?

I don't  see /dev/octoprint_1

Watched the video, found some logs, not the ones you mentioned. See the log below:


---
**Elias Bakken** *March 28, 2016 19:17*

Watch the into video, around 4 mins: 
{% include youtubePlayer.html id=BKb28fJx26I %}
[https://www.youtube.com/watch?v=BKb28fJx26I](https://www.youtube.com/watch?v=BKb28fJx26I)


---
**Jonathan Olesik** *March 28, 2016 20:29*

2016-03-27 04:39:44,555 - octoprint.server - INFO - Starting OctoPrint 1.2.8.post0.dev0+gdedadbc (heads/1.2.8 branch)

2016-03-27 04:39:44,563 - octoprint.plugin.core - INFO - Loading plugins from /home/octo/.octoprint/plugins, /usr/lib/python2.7/dist-packages/octoprint/plugins and installed plugin packages...

2016-03-27 04:39:45,020 - octoprint.plugins.discovery - INFO - pybonjour is not installed, Zeroconf Discovery won't be available

2016-03-27 04:39:45,024 - octoprint.plugin.core - INFO - Found 6 plugin(s) providing 6 mixin implementations, 2 hook handlers

2016-03-27 04:39:45,033 - octoprint.filemanager.storage - INFO - Initializing the file metadata for /usr/share/models...

2016-03-27 04:39:45,057 - octoprint.filemanager.storage - INFO - ... file metadata for /usr/share/models initialized successfully.

2016-03-27 04:39:45,100 - octoprint.plugins.softwareupdate - INFO - Loaded version cache from disk

2016-03-27 04:39:46,487 - octoprint.util.pip - WARNING - Error while trying to run pip --version: Traceback (most recent call last):

  File "/usr/bin/pip", line 9, in <module>

    load_entry_point('pip==1.5.6', 'console_scripts', 'pip')()

  File "/usr/lib/python2.7/dist-packages/pkg_resources.py", line 356, in load_entry_point

    return get_distribution(dist).load_entry_point(group, name)

  File "/usr/lib/python2.7/dist-packages/pkg_resources.py", line 2476, in load_entry_point

    return ep.load()

  File "/usr/lib/python2.7/dist-packages/pkg_resources.py", line 2190, in load

    ['__name__'])

  File "/usr/lib/python2.7/dist-packages/pip/__init__.py", line 74, in <module>

    from pip.vcs import git, mercurial, subversion, bazaar  # noqa

  File "/usr/lib/python2.7/dist-packages/pip/vcs/mercurial.py", line 9, in <module>

    from pip.download import path_to_url

  File "/usr/lib/python2.7/dist-packages/pip/download.py", line 25, in <module>

    from requests.compat import IncompleteRead

ImportError: cannot import name IncompleteRead



2016-03-27 04:39:46,495 - octoprint.util.pip - WARNING - pip command returned unparseable output, can't determine version: 

2016-03-27 04:39:46,496 - octoprint.util.pip - WARNING - pip command returned unparseable output, can't determine version: 

2016-03-27 04:39:46,497 - octoprint.util.pip - ERROR - Error while discovering pip command

Traceback (most recent call last):

  File "/usr/lib/python2.7/dist-packages/octoprint/util/pip.py", line 72, in trigger_refresh

    self._command, self._version, self._version_string, self._use_sudo = self._find_pip()

  File "/usr/lib/python2.7/dist-packages/octoprint/util/pip.py", line 199, in _find_pip

    version_segment = split_output[1]

IndexError: list index out of range

2016-03-27 04:39:46,502 - octoprint.plugin.core - INFO - Initialized 6 plugin implementation(s)

2016-03-27 04:39:47,089 - octoprint.plugin.core - INFO - 6 plugin(s) registered with the system:

|  CuraEngine (<= 15.04) (bundled) = /usr/lib/python2.7/dist-packages/octoprint/plugins/cura

|  Discovery (bundled) = /usr/lib/python2.7/dist-packages/octoprint/plugins/discovery

|  Plugin Manager (bundled) = /usr/lib/python2.7/dist-packages/octoprint/plugins/pluginmanager

|  Redeem Plugin (0.4) = /usr/lib/python2.7/dist-packages/octoprint_redeem

|  Software Update (bundled) = /usr/lib/python2.7/dist-packages/octoprint/plugins/softwareupdate

|  Virtual Printer (bundled) = /usr/lib/python2.7/dist-packages/octoprint/plugins/virtual_printer

2016-03-27 04:39:47,106 - octoprint.filemanager - INFO - Adding backlog items from all storage types to analysis queue...

2016-03-27 04:39:47,123 - octoprint.server - INFO - Reset webasset folder /home/octo/.octoprint/generated/webassets...

2016-03-27 04:39:47,129 - octoprint.filemanager - INFO - Added 2 items from storage type "local" to analysis queue

2016-03-27 04:39:47,146 - octoprint.server - INFO - Reset webasset folder /home/octo/.octoprint/generated/.webassets-cache...

2016-03-27 04:39:48,048 - octoprint.plugins.pluginmanager - INFO - Loaded plugin repository data from [http://plugins.octoprint.org/plugins.json
2016-03-27](http://plugins.octoprint.org/plugins.json%0A2016-03-27) 04:39:48,133 - octoprint.server - INFO - Listening on [http://0.0.0.0:5000](http://0.0.0.0:5000)

2016-03-27 04:39:48,157 - octoprint.plugins.discovery - INFO - Registered OctoPrint instance on kamikaze for SSDP

2016-03-27 04:39:48,287 - octoprint.server.util.sockjs - INFO - New connection from client: 127.0.0.1

2016-03-27 04:40:02,887 - octoprint.server.util.sockjs - INFO - New connection from client: 192.168.0.107

2016-03-27 13:32:13,845 - tornado.general - WARNING - error on read

Traceback (most recent call last):

  File "/usr/lib/python2.7/dist-packages/tornado/iostream.py", line 601, in _handle_read

    pos = self._read_to_buffer_loop()

  File "/usr/lib/python2.7/dist-packages/tornado/iostream.py", line 571, in _read_to_buffer_loop

    if self._read_to_buffer() == 0:

  File "/usr/lib/python2.7/dist-packages/tornado/iostream.py", line 683, in _read_to_buffer

    chunk = self.read_from_fd()

  File "/usr/lib/python2.7/dist-packages/tornado/iostream.py", line 947, in read_from_fd

    chunk = self.socket.recv(self.read_chunk_size)

error: [Errno 113] No route to host

2016-03-27 13:32:13,869 - octoprint.server.util.sockjs - INFO - Client connection closed: 192.168.0.107

2016-03-27 15:30:26,972 - octoprint.server.util.sockjs - INFO - New connection from client: 192.168.0.107

2016-03-27 21:13:45,046 - octoprint.server.util.sockjs - INFO - Client connection closed: 127.0.0.1

2016-03-27 21:13:45,484 - octoprint.server - INFO - Shutting down...

2016-03-27 21:13:45,992 - octoprint.server - INFO - Goodbye!


---
**Elias Bakken** *March 28, 2016 21:46*

That's the OctoPrint log. Looks like an error on read there, but I'm not sure what was done to provoke that. Do you have the redeem log? Perhaps in a pastebin? ::)


---
**Jonathan Olesik** *April 02, 2016 16:05*

unfortunately no redeem log...


---
**Terabyte Johnson** *April 02, 2016 18:05*

I had a similar problem, when octoprint1 did not exist. It happened because I've started Kamikaze before connecting Replicape. I have no problem connecting after reinstalling and powering up with Replicape in place.


---
**Jon Charnas** *April 03, 2016 20:52*

To see some of the errors from redeem, try "systemctl status redeem -l -n 100" - but sometimes it's not clear that there's a typo in the config file. One more thing on my todo list is to fix that ;)


---
*Imported from [Google+](https://plus.google.com/+JonathanOlesik/posts/eUWLLXFZVVP) &mdash; content and formatting may not be reliable*
