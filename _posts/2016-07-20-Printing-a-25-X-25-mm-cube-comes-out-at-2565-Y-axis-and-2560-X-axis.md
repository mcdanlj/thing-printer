---
layout: post
title: "Printing a 25 X 25 mm cube comes out at 25.65 Y axis and 25.60 X axis"
date: July 20, 2016 13:53
category: "Discussion"
author: Bob McDowell
---
Printing a 25 X 25 mm cube comes out at 25.65 Y axis and 25.60 X axis. What settngs do I have to change and how. Do I put the changes in the Redeem local.cfg?





**Bob McDowell**

---
---
**Jon Charnas** *July 20, 2016 14:26*

Changes go in local.cfg for sure. What changes, that I don't know. First maybe check that your jerk and acceleration values aren't too high (slow down your print?), then if it's really a problem with too many steps, you need to recompute your x/y steps_per_mm


---
**James Armstrong** *July 20, 2016 15:42*

Could be x and y steps but they should usually be spot on with belt and gear calculations. Could also be a slicer setting. The slicer defines the path and changing the nozzle size, flow multiplier, filament diameter and extrusion width can all come into play here. 


---
*Imported from [Google+](https://plus.google.com/107803003115083427930/posts/DS4LnBMXSV1) &mdash; content and formatting may not be reliable*
