---
layout: post
title: "Hi guys, I am a fresher on replicape and redeem.Some common questions : 1.what touch screen is suitable for relicape ?some links?HDMI connection?"
date: January 18, 2018 00:24
category: "Discussion"
author: vincent Do
---
Hi guys,

     I am a fresher on replicape and redeem.Some common questions :

     1.what touch screen is suitable for relicape ?some links?HDMI connection?

      2.The redeem is derived from what firmware ?Marlin or repetier?

Thanks .





**vincent Do**

---
---
**Daryl Bond** *January 18, 2018 01:48*

Addressing point 2, Redeem is not based on either Marlin or Repetier but rather follows the Marlin style when it comes to gcode compatibility. This means that while the code for Redeem and Marlin are completely different, if you supply the same gcode command to either one, you should get essentially the same response.


---
**Jon Charnas** *January 18, 2018 05:39*

Touch screen wise, the manga screen 2 is coming out of Kickstarter in the next month or two, but there are a number of USB+HDMI touch screens you can use. Just beware you'll need to tweak the toggle configuration of the resolution doesn't match the manga screen's


---
**vincent Do** *January 18, 2018 06:49*

On the page:[https://www.thing-printer.com/product/replicape/,I](https://www.thing-printer.com/product/replicape/,I) cannot see the HDMI interface on the board.How can I link the screen to replicape board?



[thing-printer.com - Replicape Rev B](https://www.thing-printer.com/product/replicape/)


---
**Jon Charnas** *January 18, 2018 06:59*

The HDMI is on the beaglebone black, micro HDMI plug


---
**vincent Do** *January 18, 2018 07:40*

So,I must buy  "beaglebone black" ?How about  REV.C4 version?


---
**Jon Charnas** *January 18, 2018 07:45*

Rev C isn't even in pre production yet. Rev B3 is like a ramps, you need the Arduino underneath... Here it's a beaglebone black


---
**vincent Do** *January 18, 2018 08:07*

I see.Thank you very much .




---
**vincent Do** *January 18, 2018 08:10*

Does it support pt100 or pt1000 temperature sensors?


---
**Dave Posey** *January 18, 2018 15:42*

**+vincent Do** I use a BeagleBone Black (BBB) Rev C (4 GB) with a Replicape Rev B3.


---
**vincent Do** *January 20, 2018 01:18*

ok,thanks.I will try one.


---
*Imported from [Google+](https://plus.google.com/110726710776841383619/posts/ZgECz84mBK5) &mdash; content and formatting may not be reliable*
