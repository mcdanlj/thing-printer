---
layout: post
title: "I've recently update the OctoPrint Redeem plugin, so people can now see the contents of the printer config files as well as default.cfg"
date: October 14, 2016 15:54
category: "Discussion"
author: Elias Bakken
---
I've recently update the OctoPrint Redeem plugin, so people can now see the contents of the printer config files as well as default.cfg. Especially for Windows users this is welcome, since the newline is different! 

![images/b5d1b5f375fc8345e71ce578e02eadfd.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/b5d1b5f375fc8345e71ce578e02eadfd.png)



**Elias Bakken**

---
---
**Daniel Kusz** *October 14, 2016 16:18*

What do you think about button "save and restart" in this window?


---
**Elias Bakken** *October 14, 2016 16:20*

This window only has a view of the file, but the local.cfg also has a save button, which could also have a save and restart!


---
**Philip Acierto** *October 14, 2016 18:35*

save + restart on that window would speed things up a bit for sure! would be a good "nice to have"




---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/7aw3zis4b8p) &mdash; content and formatting may not be reliable*
