---
layout: post
title: "Yep, Replicape is definitely the quietest printer board I've ever heard!"
date: March 02, 2016 22:07
category: "Discussion"
author: Jon Charnas
---
Yep, Replicape is definitely the quietest printer board I've ever heard!

Now just need to get me quieter fans...





**Jon Charnas**

---
---
**Andy Tomascak** *March 03, 2016 22:26*

Do you have a config you can share? I'm still trying to work out some "wonkiness" that I'm seeing with my delta.


---
**Andy Tomascak** *March 03, 2016 23:44*

Also, do you have the .cht you were working on? I'm seeing something close to what you were last time we chatted. (Hotend goes to stable ~25 degrees colder than it's target.)


---
**Andy Tomascak** *March 04, 2016 01:18*

(Sorry, was an idiot and missed your PID settings section in your wiki entry)


---
**Jon Charnas** *March 04, 2016 08:21*

**+Andy Tomascak** no problem, I only added the PID section recently. Like I said, I'm still building up the page... As far as my configuration file, well, I can post it, sure. If you can wait until I'm home tonight. But I didn't change any of the geometry settings from Elias' Kossel config file. I re-defined a few settings for the stepper drivers, the endstops and the home position (I'm not sure why his home position is (0,0,0), it should be (0,0,zmax) probably). I also re-computed the steps_per_mm because my GT2 pulleys are 18 teeth and not 20. What delta is it that you have, and what "wonkiness" are you running into?


---
**Jon Charnas** *March 04, 2016 20:01*

**+Andy Tomascak**: config file I have running now on my kossel: [https://www.dropbox.com/s/me2hxqros85wwq1/local.cfg?dl=0](https://www.dropbox.com/s/me2hxqros85wwq1/local.cfg?dl=0)


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/Gi3pWJxeEwc) &mdash; content and formatting may not be reliable*
