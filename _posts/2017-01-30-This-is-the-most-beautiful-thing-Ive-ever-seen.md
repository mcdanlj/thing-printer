---
layout: post
title: "This is the most beautiful thing I've ever seen!"
date: January 30, 2017 19:49
category: "Discussion"
author: Elias Bakken
---
This is the most beautiful thing I've ever seen! First image appearing on the new Manga Screen! 1080p:)

![images/f7e9c0510c0ecc61fd4c7b9904a3605d.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/f7e9c0510c0ecc61fd4c7b9904a3605d.jpeg)



**Elias Bakken**

---
---
**Daniel Kusz** *January 30, 2017 19:55*

It's big!!!


---
**Amit Patel** *January 31, 2017 02:10*

**+Elias Bakken** This looks awesome, can't wait to get one, Great job




---
**Daniel Kruger** *January 31, 2017 04:54*

Is there some way to use a cheap tablet or old smartphone to do the same thing?


---
**Elias Bakken** *January 31, 2017 11:17*

There is an app for iPhone at least, probably for android as well. 


---
**Daniel Kusz** *January 31, 2017 11:21*

Printoid for android. Works very good.


---
**Jon Charnas** *February 01, 2017 10:45*

**+Elias Bakken** how soon 'til the final product is available? :) I'm gonna get me an upgrade!


---
**Elias Bakken** *February 01, 2017 15:00*

Jon, the deadline is the end of Q1, still needs the touch panel and the injection molded back plane.


---
**Jon Charnas** *February 01, 2017 15:02*

Cool! In the meantime, 2.1.0 is out and getting pretty good feedback (a few minor issues), and we're looking at this weekend for a 2.1.1 RC (adding BBB-W support)


---
**Dan Cook** *February 01, 2017 16:12*

I'll take two! I tried Printoid (for Android) last night... not bad, but I still like having a nice screen attached to my printer. Looking forward to the release!


---
**Elias Bakken** *February 01, 2017 17:47*

Sweet! I've got to get my hands on a BBBW myself!


---
**Jon Charnas** *February 01, 2017 21:30*

You should try to pop into slack every once in a while. I think gearheadred and I stumbled on a fantastic idea for the next redeem feature set, but it'd be good to get your take on it.


---
**James Armstrong** *February 02, 2017 07:06*

Looks great. What is the size and res?


---
**Per Arne Kristiansen** *February 02, 2017 19:39*

I tried to Google bbbw 😂😂😂 **+Elias Bakken**  you have to Google IT and search in pictures


---
**Per Arne Kristiansen** *February 02, 2017 19:39*

But What is bbbw by the way 


---
**Per Arne Kristiansen** *February 02, 2017 19:44*

Beagleboneblackwireless 👍😄


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/VhQiH7ccUdQ) &mdash; content and formatting may not be reliable*
