---
layout: post
title: "BBB Dev help needed! I've got a potential RC version of Kamikaze 2.1 ready on my uSD card, I can boot my BBB from it fine, it's got all the bells and whistles we want in it"
date: December 16, 2016 13:03
category: "Discussion"
author: Jon Charnas
---
BBB Dev help needed!



I've got a potential RC version of Kamikaze 2.1 ready on my uSD card, I can boot my BBB from it fine, it's got all the bells and whistles we want in it. However when I tried extracting the image and distributing to a few willing beta-testers, their BBB wasn't flashed as expected. I don't yet have a sparse uSD card to try and push the image I created on it to test out the flashing procedure myself.



I've extracted the image by using dd if=/dev/mmcblk0p1 bs=16M of=Kamikaze-2.1.0-rc1.img (where mmcblk0p1 is the 2GB partition on my 8GB uSD card). Does anyone know if that is inherently wrong?





**Jon Charnas**

---


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/DpnfiWN4L7G) &mdash; content and formatting may not be reliable*
