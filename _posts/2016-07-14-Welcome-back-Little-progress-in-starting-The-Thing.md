---
layout: post
title: "Welcome back, Little progress in starting The Thing"
date: July 14, 2016 16:10
category: "Discussion"
author: Daniel Kusz
---
Welcome back,



Little progress in starting The Thing. Cheap version E3D V6 hotend proved to be defective and filament block between the teflon tube and the heatbrake - to high temp. in this place. After changing to v5 I have no problems with extrusion. The printer starts printing but:

1. At the beginning after homing Z axis printer printed, but does not lower the table for next layer

2. I changed the parameters in the file printer.cfg and local.cfg - unfortunately I do not remember exactly what and how much :/ At the moment, after the homing axis Z and start printing, the printer lowers the table by approx. 15 mm and the next layer increases the table instead of lower (I think something is configured vice versa).

3. Even a question about hotends: I have yet one hotend but to start printing you need to have a second, which also must warm up to printing has begun. The setting in the printer profiles one hotend does nothing. How to set the printer configuration to the printer to start printing after only the first heating hotend?﻿





**Daniel Kusz**

---
---
**Daniel Kusz** *July 14, 2016 18:11*

Elias, installing new Kamikaze image will solve this problem?


---
**Elias Bakken** *July 14, 2016 18:55*

If you have the latest version of redeem, that is the most important change in comparison with the old Kamikaze. I'm not sure what is happening with the z-axis.. It is not hit by the end stop?


---
**Daniel Kusz** *July 14, 2016 19:17*

1. You know that I'm rookie in linux :) Easier for me is installing new image with latest redeem. This will be a good occasion to test it. So what do you recommend? So this should I use in SSH?:



cd /usr/src

git clone [https://bitbucket.org/intelligentagent/redeem.git](https://bitbucket.org/intelligentagent/redeem.git)

git checkout develop

make install_py

cp systemd/redeem.service /lib/systemd/system/

systemctl daemon-reload 

systemctl restart redeem



and



cp /usr/src/redeem/configs/default.cfg /etc/redeem 

systemctl restart redeem

journalctl -f -n 100﻿



2. Yes, z-axis is hit by the end stop because this is position after homing (no offset). When start printing bed goes down approx. 15 mm, printing in air and goes up with each layer. When I change line in g-code (G1 Z0.0 F{travel_speed}) - bed doesn't go up with each layer because it is in 0 position and holded by end stop. This is the same situation with replicape: [https://plus.google.com/114125966923153934677/posts/WaR8Gn4SgYt](https://plus.google.com/114125966923153934677/posts/WaR8Gn4SgYt)


---
**Jon Charnas** *July 15, 2016 13:34*

**+Daniel Kusz** minor changes needed also in /etc/systemd/ files - use 

sed -i -e "s/bin\/redeem/local\/bin\/redeem/g" /etc/systemd/system/base.target.wants (or whatever other systemd file it's in, can't remember off the top of my head)


---
**Daniel Kusz** *July 15, 2016 18:08*

Can you write exactly what I need to write in console? No such file or directory after: sed -i -e "s/bin\/redeem/local\/bin\/redeem/g" /etc/systemd/system/base.target.wants. Sorry about all this questions, but only linux upgrades and problems are difficult for me.



PS. After updating redeem, the X axis is changed with the axis Y. Also, the axes do not move in accordance with the distances. I have the impression that the .cfg files are not reading properly.


---
**Jon Charnas** *July 15, 2016 22:40*

right, because the last argument is a file that probably doesn't exist - if you remember DOS, basically ls is "dir" and cd is the same. except C: becomes / in Linux. use those two to find out what the right name is for the redeem.service file


---
**Daniel Kusz** *July 16, 2016 12:38*

OK thanks, I think i can do this after learning some linux basics. Now I'm on the Kamikaze 1.1.0.


---
*Imported from [Google+](https://plus.google.com/103041373788524358316/posts/R8qHSNAA4nS) &mdash; content and formatting may not be reliable*
