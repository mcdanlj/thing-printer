---
layout: post
title: "Hi all, I've ran into some trouble with my setup"
date: June 02, 2016 19:29
category: "Discussion"
author: Samuel Kvarnbrink
---
Hi all,

I've ran into some trouble with my setup. Using a Prusa i3 with an E3D Chimera hotend, in which a single fan cools a shared heatsink. So, if either extruder 0 or extruder 1 goes above 60°, the fan should turn on. I've configured it like this:



connect-therm-E-fan-0 = True

connect-therm-H-fan-0 = True



and it basically works, except for that the fan randomly stops and stays off for scarily long periods of time. If i comment out one of the lines the fan at least doesn't stop, but of course, the cooling won't activate at all if the other hotend is used. If I keep both hotends above 60° the fan will also stay active, but that's also too much of a kludge.



Does anybody know if there is a way to fix this in the configuration? Or do I need to fiddle with the Redeem code itself? Very thankful for any help :)





**Samuel Kvarnbrink**

---
---
**Jon Charnas** *June 03, 2016 14:25*

Umm... That's something only **+Elias Bakken** can answer completely, but I think part of your problem is that you've got a conflict of configuration. I think the best solution would be to have your fan powered by 2 outputs, and have one output for each hotend sensor. Then you guarantee that you'll have the fan always on. But you'll have to make sure you don't get a PWM sync issue...


---
**Elias Bakken** *June 03, 2016 18:08*

You have two threads competing to set the output, that's why you will see it intermittently on and off. This would require a software patch I think...


---
**Samuel Kvarnbrink** *June 06, 2016 16:54*

Thanks, makes sense. I'm thinking of fixing the problem by adding a Dallas 1W cold end monitor to control the fan instead – are there any examples of how I configure that?


---
**Elias Bakken** *June 06, 2016 17:20*

It's not well documented, but there is some info on the wiki. [http://wiki.thing-printer.com/index.php?title=Redeem#Cold_ends](http://wiki.thing-printer.com/index.php?title=Redeem#Cold_ends)



If you connect a DS18B20 to the Dallas W1 interface, it should get an index during Redeem startup. If you have 1, it will get index 0. Looks like the wiki is a bit outdated on that topic. 



Then you have to use:

connect-ds18b20-0-fan-0 = True

in your local.cfg to connect fan 0 to that temp sensor. 



Make sure you restart the BBB, since the temp sensor is discovered during kernel start up. 


---
**Samuel Kvarnbrink** *June 06, 2016 18:20*

Splendid, thanks! Have to say – despite the fan issue, setting up the Replicape has been a much easier (and nicer) process than with any Marlin board I've ever worked with. Kudos! :)


---
*Imported from [Google+](https://plus.google.com/116914284093198004627/posts/1jNrjQAGdKo) &mdash; content and formatting may not be reliable*
