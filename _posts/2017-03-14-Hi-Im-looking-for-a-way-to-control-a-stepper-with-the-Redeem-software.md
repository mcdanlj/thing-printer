---
layout: post
title: "Hi, I'm looking for a way to control a stepper with the Redeem software"
date: March 14, 2017 11:10
category: "Discussion"
author: Fröhlich Jan
---
Hi, 

I'm looking for a way to control a stepper with the Redeem software. I'd like to do it without any g-codes, just with a short .py-script. Right now we use the detour via the g-codes. It works an looks like this:



	    if heaterState:

		# Turn motor forward

		os.system("echo G1 E-3.5 F210>/dev/testing_1")



The problem is that I have to use this command every second. Therefore during longer prints the system starts jerking.



Does anybody knows how to move a stepper with a certain pace directly in Redeem?



Kind regards Jan





**Fröhlich Jan**

---
---
**Jon Charnas** *March 14, 2017 14:52*

The gcode is how redeem expects commands to be queued up... If you want to execute the script every second you can wrap it with a 'watch -n 1 python [script.py](http://script.py)'


---
**Jon Charnas** *March 14, 2017 14:53*

Or you can add a custom gcode to your slicer to add the gcode into it.


---
**Fröhlich Jan** *March 16, 2017 13:49*

Thank you for your answers. I already execute it every second which causes the trouble. The custom g-code is an idea and I'll try it. Nevertheless I'd like to avoid the g-code and use a .py-class for the control of the steppers.


---
*Imported from [Google+](https://plus.google.com/114252554493278211650/posts/7UBTBxw9oDh) &mdash; content and formatting may not be reliable*
