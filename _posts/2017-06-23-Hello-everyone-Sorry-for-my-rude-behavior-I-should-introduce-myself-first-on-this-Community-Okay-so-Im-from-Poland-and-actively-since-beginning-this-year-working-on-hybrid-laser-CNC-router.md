---
layout: post
title: "Hello everyone, Sorry for my rude behavior - I should introduce myself first on this Community :) Okay, so I'm from Poland and actively, since beginning this year, working on hybrid laser/CNC router"
date: June 23, 2017 13:50
category: "Discussion"
author: Radek Biskupski
---
Hello everyone,

Sorry for my rude behavior - I should introduce myself first on this Community :)

Okay, so I'm from Poland and actively, since beginning this year, working on hybrid laser/CNC router. Hybrid because platform will be adopted from Lasersaur, but controller will be probably Replicape and laser modules as of ND:YAG (have three of them - one is very big) and swap-able diode lasers (both ND:YAG and diodes are almost 200W CW) instead of CO2 tube laser in orginal Lasersaur project. For prototype it would be tested Kamikaze with Replicape board and in second shot BeagleG with LasersaurApp or Octoprint after some tweaks.

At this specific moment Replicape and Kamikaze looks best promisingly, but I'm not sure how and whether  Kamikaze uses PRUs to control steppers directly.

If anyone of you could share yours experiences it would be very welcome.

Greetings from Poland.





**Radek Biskupski**

---
---
**Daniel Kusz** *June 23, 2017 14:03*

Cześć. Fajnie, że ktoś z Polski :) Używam Replicape w mojej drukarce 3D. Jeśli będziesz potrzebowałem porad w konfiguracji/uruchomieniu to pisz. Nie wszystko wiem, ale trochę przy tym sprzęcie posiedziałem od strony uruchomienia pod drukarkę 3D. Jestem inżynierem mechanikiem i z programowania na Linuxie mało co wiem ;) 


---
**Jon Charnas** *June 23, 2017 14:06*

Hi Radek,

Yes, the replicape uses PRUs to control the steppers :) For your application I think the Replicape + Kamikaze could work quite well. Kamikaze computes the g-code moves on the normal CPU and then buffers the moves in memory for the PRUs to consume - I hope this helps clarify how the stack works?


---
**Radek Biskupski** *June 23, 2017 14:33*

@Jon, 

Absolutely :) Good news for me. So time to invest in Replicape and test it.



@Daniel,

Dzięki. Miło Cię poznać. Na pewno będę miał pytania, bo Replicape to dla mnie nowa płytka. Do tej pory testowałem różne inne capes ale też pod inny projekt.

Prede mną jeszcze dużo pracy z chłodzeniem laserów itp. ale w te wakacje po urlopie chcę trochę przyspieszyć aby na Boże Narodzenie mieć już zbudowaną maszynę.

Do usłyszenia zatem :)


---
**Roel Jaspers** *July 05, 2017 11:15*

Hi Radek,

I'm using the replicaped and Kamikaze for a CNC hotwire foam cutter.

Redeem works very fine with that machine.

Of coarse Kamikaze+Octoprint+redeem has an interface specially for printers but the only thing I need is some manual functions (homing testing etc) and and a G-code translator (Redeem)

The terminal for entering manual g-codes is great also.



I've tried to put MachineKit on the BBB but never got te damned motors to run.

So I'm back to Kamikaze and that works great.

I use a BBB wireless and I'm able to follow the process on my phone.



Keep in touch to see how things workout for you



Roel 


---
*Imported from [Google+](https://plus.google.com/100171647175055988434/posts/iMnB9zDJ1XF) &mdash; content and formatting may not be reliable*
