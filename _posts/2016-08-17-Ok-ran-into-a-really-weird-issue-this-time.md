---
layout: post
title: "Ok, ran into a really weird issue this time"
date: August 17, 2016 14:15
category: "Discussion"
author: Samuel Kvarnbrink
---
Ok, ran into a really weird issue this time. I'm trying to print a large part, but once the printer finishes the first skirt outline, the printer just stops and sits there. I've previously noticed that it randomly stops and waits between movements when doing smaller parts (most noticeably, random pauses between every stroke on a solid infill), but it just never starts moving again. The only thing I'm getting in the OctoPrint terminal is a communication timeout once the printer has been standing still for 30 secs. I tried "systemctl -n 100 -l status redeem" but it gives no info.



I think the stopping and random waiting started after upgrading to Kamikaze 1.1.0, but I'm not quite sure. Also, I'm not even sure if this is the same issue since the printer reproducibly stops at the same exact spot every time, and never starts moving again. I updated to the latest Redeem using git, but no difference.



Any clues on how to proceed with troubleshooting this? Or has anyone seen anything like this after upgrading? Any help is extremely appreciated…





**Samuel Kvarnbrink**

---
---
**Samuel Kvarnbrink** *August 17, 2016 14:18*

By the way, the OctoPrint output looks like this up to the point of failure. Can't see much wrong except for, ehm, that part where everything silently dies.



Send: N36 G1 X114.424 Y168.280 E20.0170*80

Recv: ok

Send: N37 G1 X77.220 Y168.280 E21.4277*102

Recv: ok

Send: N38 G1 X77.220 Y31.820 E26.6018*90

Recv: ok

Send: N39 M105*29

Recv: ok

Send: N40 G1 X77.220 Y31.720 F546*75

[...]

Send: N41 G1 X76.740 Y31.240 F6000*122

Recv: ok

Send: N42 G1 X114.701 Y31.240 E28.0411 F504*59

Communication timeout while printing, trying to trigger response from printer. Configure long running commands or increase communication timeout if that happens regularly on specific commands or long moves.


---
**Step Cia** *August 17, 2016 15:20*

I read a couple iteration of octoprint back had issues with communication. You might want to check that it has since been solved


---
**James Brassill** *August 17, 2016 16:17*

Try a different slicer. I had this issue on some objects and switching to a different slicer solved it for that print, no matter what I changed it would just freeze at the same spot. The gcode looked just fine to me.


---
**Samuel Kvarnbrink** *August 17, 2016 18:48*

**+James Brassill** Thanks, could get the print to run after slicing in Cura 2. Wanted to keep working in S3D so I tried every possible change in slicing (different layer sizes, disabled coasting, disabled z lift, rotated the model differently and so on) but it just stopped at different points.


---
**James Brassill** *August 17, 2016 18:49*

I don't know what is going on with this. My issues are from slic3r, but if I use cura it prints fine.


---
**Samuel Kvarnbrink** *August 17, 2016 18:56*

I also went through the G-code and it looks perfectly normal, no shenanigans. It fails before executing the second of these commands:

G1 X114.701 Y31.240 E42.0367 F504

G1 X123.260 Y46.064 E43.0103



So, just normal moves and extrusions. What bugs me even more is that it fails silently, no clue in any logs as to what goes wrong. Devs, any ideas?


---
**Andrew Dowling** *August 18, 2016 05:41*

I have the same issue with simplify 3d. Try printing faster. above 4000mm/min.


---
**Samuel Kvarnbrink** *August 18, 2016 06:13*

Thanks Andrew, I might try that on another print. This was a large XT-CF20 print I needed to get done, all attempts at more than 2400 mm/min have turned out awful. Tried going from 1800 to 2400, though. It printed a bit more, but froze in the middle of the 1st layer solid infill instead.


---
*Imported from [Google+](https://plus.google.com/116914284093198004627/posts/RZkMBMD3HQS) &mdash; content and formatting may not be reliable*
