---
layout: post
title: "I have everything on my cartisian wired up and now its time to calibrate"
date: July 23, 2016 20:05
category: "Discussion"
author: James Brassill
---
I have everything on my cartisian wired up and now its time to calibrate. One question about that though. I had homing working on Kamikaze 1.0, but I noticed there was 1.1.0 so I upgraded to that and now I can't get my printer to home the right direction with the same local.cfg. I want X to home negative, and Y to home positive(endstop at front of bed) yet it does the exact opposite for homing. For regular moves the axis go the commanded direction. I have tried flipping the signs and X still goes positive and Y still goes negative regardless of whats there. Am I missing something elsewhere in the configs?



My geometry section looks like this.



[Geometry]

offset_x = -0.19

offset_y = 0.19

travel_y = 0.25

travel_x = -0.25

offset_z = 0.0001





**James Brassill**

---
---
**James Brassill** *July 24, 2016 03:08*

I realize I had the signs wrong on the offsets but now it is homing right. The new issue is I get alarm stepper fault while heating the bed, no steppers are on but it trips all four for some reason.




---
**James Brassill** *July 27, 2016 04:50*

So with the new PSU I am not having any issues. There must have been something in there causing current spikes.


---
*Imported from [Google+](https://plus.google.com/+JamesBrassill/posts/XFbWVAJz2dM) &mdash; content and formatting may not be reliable*
