---
layout: post
title: "How do I rotate my display, I know I can rotate Toggle but I want to rotate the hdmi signal"
date: December 26, 2016 14:05
category: "Discussion"
author: Leo Södergren
---
How do I rotate my display, I know I can rotate Toggle but I want to rotate the hdmi signal.





**Leo Södergren**

---
---
**Elias Bakken** *December 26, 2016 19:58*

There is no way to rotate the signal on a hardware level, but you can rotate the console image by changing the kernel command line. Look in /boot/uEnv.txt and look for the rotation param.


---
*Imported from [Google+](https://plus.google.com/107257105792966201860/posts/AtWNNBwDQaz) &mdash; content and formatting may not be reliable*
