---
layout: post
title: "Have a question while setting up the delta endstops axis movements, I continue to hit some walls"
date: August 08, 2016 02:44
category: "Discussion"
author: Edwardo Garabito
---
Have a question while setting up the delta endstops  axis movements, I continue to hit some walls. Every time I try to move what I think is the z axis, i notice that it moves significantly slower then a the x, and y.



On top of that when I cant seem to figure out how to switch from Cartesian mode to Delta. I use the M270 mcode that Elias states in his guide. But when I send "M270 S3" in the terminal, the printer still acts as if  its Cartesian. Not sure whats going on here one bit﻿



Heres a video showing whats going on.





My printers settings Its a modified Rostock max V2



[System]



machine_type = DingaLang2000



[Geometry]

# Delta

axis_config = 3



# The total length each axis can travel

#   This affects the homing endstop searching length.

#   travel_* can be left undefined.

#   It will be determined by soft_end_stop_min/max_

<b># travel_x =</b>

<b># ...</b>



<b># Define the origin in relation to the endstops</b>

<b>#   offset_</b> can be left undefined.

#   It will be determined by home_speed and soft_end_stop_min/max_

<b># offset_x = 0.0</b>

<b># ...</b>



<b>        </b>

<b>[Delta]</b>

<b># Distance head extends below the effector.</b>

<b>Hez = 29</b>

<b># Length of the rod</b>

<b>L   = 300</b>

<b># Radius of the columns (distance from column to the center of the build plate)</b>

<b>r   = 204.0  </b>

<b># Effector offset (distance between the joints to the rods to the center of the effector)</b>

<b>Ae  = 30.22</b>

<b>Be  = 30.22</b>

<b>Ce  = 30.22</b>

<b># Carriage offset (the distance from the column to the carriage's center of the rods' joints)</b>

<b>A_radial = 27.075</b>

<b>B_radial = 27.075</b>

<b>C_radial = 27.075</b>



<b># Compensation for positional error of the columns</b>

<b># (For details, read: </b>[https://github.com/hercek/Marlin/blob/Marlin_v1/calibration.wxm](https://github.com/hercek/Marlin/blob/Marlin_v1/calibration.wxm)<b>)</b>

<b># Positive values move the tower to the right, in the +X direction, tangent to it's radius</b>

<b>A_tangential = 0.0</b>

<b>B_tangential = 0.0</b>

<b>C_tangential = 0.0    </b>



<b># Stepper e is ext 1, h is ext 2</b>

<b>[Steppers]</b>

<b>current_x = 0.713</b>

<b>current_y = 0.713</b>

<b>current_z = 0.713</b>

<b>current_e = 0.7</b>

<b>steps_pr_mm_x = 6.25</b>

<b>steps_pr_mm_y = 6.25</b>

<b>steps_pr_mm_z = 6.25</b>

<b>steps_pr_mm_e = 31.0</b>

<b>microstepping_x = 8</b>

<b>microstepping_y = 8</b>

<b>microstepping_z = 8</b>

<b>microstepping_e = 8</b>

<b>slow_decay_x = 4</b>

<b>slow_decay_y = 4</b>

<b>slow_decay_z = 4</b>



<b># Which steppers are enabled</b>

<b>in_use_x = True</b>

<b>in_use_y = True</b>

<b>in_use_z = True</b>

<b>in_use_e = True</b>



<b>[Endstops]</b>

<b>end_stop_x1_stops = x_cw</b>

<b>end_stop_y1_stops = y_cw</b>

<b>end_stop_z1_stops = z_cw</b>

<b>invert_x1 = false</b>

<b>invert_y1 = false</b>

<b>invert_z1 = false</b>

<b>soft_end_stop_max_x = 0.085</b>

<b>soft_end_stop_max_y = 0.085</b>

<b>soft_end_stop_max_z = 0.253</b>

<b>soft_end_stop_min_x = -0.085</b>

<b>soft_end_stop_min_y = -0.085</b>

<b>soft_end_stop_min_z = -0.001</b>



<b>[Heaters]</b>

<b># For list of available temp charts, look in temp_chart.py</b>



<b># E3D v6 Hot end</b>

<b>temp_chart_E = SEMITEC-104GT-2</b>

<b>pid_p_E = 0.1</b>

<b>pid_i_E = 0.01</b>

<b>pid_d_E = 0.3</b>

<b>ok_range_E = 4.0</b>



#Heated<b> Bed</b>

<b>temp_chart_e = SEMITEC-104GT-2</b>

<b>max_rise_temp_e = 20.0</b>

<b>max_fall_temp_e = 20.0</b>

<b>min_temp_e = 10.0</b>

<b>max_temp_e = 300.0</b>

<b>max_temp_bed = 120.0</b>

<b>pid_p_e = 0.7811</b>

<b>pid_i_e = 0.0322</b>

<b>pid_d_e = 4.7358</b>

<b>pid_p_hbp = 0.275698141115</b>

<b>pid_i_hbp = 0.00250634673741</b>

<b>pid_d_hbp = 7.58169888066</b>

<b>pid_kp_e = 0.0572957795131</b>

<b>pid_ti_e = 54.7340133667</b>

<b>pid_td_e = 3.94906301347</b>

<b>pid_kp_hbp = 0.0423315066574</b>

<b>pid_ti_hbp = 239.42792902</b>

<b>pid_td_hbp = 17.2747423535</b>





<b>[Homing]</b>



<b># Homing speed for the steppers in m/s</b>

<b>#   Search to minimum ends by default. Negative value for searching to maximum ends.</b>

<b>home_speed_x = 0.1</b>



<b># homing backoff speed                </b>

<b>home_backoff_speed_x = 0.01</b>

<b>                                      </b>

<b># homing backoff dist                             </b>

<b>home_backoff_offset_x = 0.01                                                          </b>



<b># Where should the printer goes after homing</b>

<b>#   home_</b> can be left undefined. It will stay at the end stop.

# home_x = 0.0

# ...





[Watchdog]

enable_watchdog = True



[Macros]

G29 =

    M561                ; Reset the bed level matrix

    M558 P0             ; Set probe type to Servo with switch

    M557 P0 X10 Y20     ; Set probe point 0

    M557 P1 X10 Y180    ; Set probe point 1

    M557 P2 X180 Y100   ; Set probe point 2

    G28 X0 Y0           ; Home X Y



    G28 Z0              ; Home Z

    G0 Z12              ; Move Z up to allow space for probe

    G32                 ; Undock probe

    G92 Z0              ; Reset Z height to 0

    G30 P0 S            ; Probe point 0

    G0 Z0               ; Move the Z up

    G31                 ; Dock probe



    G28 Z0              ; Home Z

    G0 Z12              ; Move Z up to allow space for probe

    G32                 ; Undock probe

    G92 Z0              ; Reset Z height to 0

    G30 P1 S            ; Probe point 1

    G0 Z0               ; Move the Z up

    G31                 ; Dock probe



    G28 Z0              ; Home Z

    G0 Z12              ; Move Z up to allow space for probe

    G32                 ; Undock probe

    G92 Z0              ; Reset Z height to 0

    G30 P2 S            ; Probe point 2

    G0 Z0               ; Move the Z up

    G31                 ; Dock probe



    G28 X0 Y0           ; Home X Y



G31 =

    M280 P0 S320 F3000  ; Probe up (Dock sled)



G32 =

    M280 P0 S-60 F3000  ; Probe down (Undock sled)

    

   



 


**Video content missing for image https://lh3.googleusercontent.com/-ZrxXCTuAxH4/V6fxfVPUIXI/AAAAAAAAAJQ/W_kCPbAwyJ4l2370pQOecvCSbUFtN_u8wCJoC/s0/20160807_223203.mp4**
![images/cf2695ac99fd7f78a29ced75e65be349.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/cf2695ac99fd7f78a29ced75e65be349.jpeg)



**Edwardo Garabito**

---
---
**Edwardo Garabito** *August 12, 2016 03:57*

Does anyone have any incite

?


---
*Imported from [Google+](https://plus.google.com/115118943136595777855/posts/GVPtzAaiLPw) &mdash; content and formatting may not be reliable*
