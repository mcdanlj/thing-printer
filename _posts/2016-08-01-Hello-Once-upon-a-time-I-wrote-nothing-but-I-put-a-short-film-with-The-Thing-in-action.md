---
layout: post
title: "Hello, Once upon a time I wrote nothing but I put a short film with The Thing in action"
date: August 01, 2016 15:56
category: "Discussion"
author: Daniel Kusz
---
Hello,



Once upon a time I wrote nothing but I put a short film with The Thing in action. After changing the linear bearings I have eliminated backlash and the print quality is satisfactory for me. I am at the stage of printing several elements to the printer (layer cooling, endstop brackets etc.) and then I would close the electronics and bury cables. There is still adding a second hotend and an inductive sensor for the Z axis. This will be the next stage :)




{% include youtubePlayer.html id=8drjTJ_zr5Y %}
[https://youtu.be/8drjTJ_zr5Y](https://youtu.be/8drjTJ_zr5Y)





**Daniel Kusz**

---
---
**Elias Bakken** *August 01, 2016 16:11*

Awesome!




---
*Imported from [Google+](https://plus.google.com/103041373788524358316/posts/WAAVugzRVX2) &mdash; content and formatting may not be reliable*
