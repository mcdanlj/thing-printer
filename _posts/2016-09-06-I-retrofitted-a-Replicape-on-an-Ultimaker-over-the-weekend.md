---
layout: post
title: "I retrofitted a Replicape on an Ultimaker over the weekend"
date: September 06, 2016 13:38
category: "Discussion"
author: Elias Bakken
---
I retrofitted a Replicape on an Ultimaker over the weekend. Noticeably more quiet and also prints from OctoPrint. **+Erik de Bruijn** let me know if you want to change controller boards, I now have a config file for you:)

![images/ce14df6d23ffcb4964372b5479c376ad.gif](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/ce14df6d23ffcb4964372b5479c376ad.gif)



**Elias Bakken**

---
---
**Koen Kooi** *September 06, 2016 13:47*

I really need to finish my makerbot replicape conversion and get printing again!


---
**Elias Bakken** *September 06, 2016 13:47*

Yeah, where did it stop for you?


---
**Daniel Kusz** *September 06, 2016 14:23*

Equalizer on Manga Screen :-D 


---
**Elias Bakken** *September 06, 2016 14:25*

**+Daniel Kusz** spectrum analyzer on my diy spotify box:)


---
**Dan Cook** *September 06, 2016 14:25*

This is very similar to my printer, so I'll be checking this out. I have everything to get started on mine, I've just been procrastinating...



Elias, is that a Manga stereo in the background?


---
**Koen Kooi** *September 06, 2016 14:46*

**+Elias Bakken** everything is hooked up, then we moved house and had a baby


---
**Elias Bakken** *September 06, 2016 14:49*

**+Dan Cook** I'll add the config to the repo. Yes, a mopidy player


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/4sD1NYmiwJa) &mdash; content and formatting may not be reliable*
