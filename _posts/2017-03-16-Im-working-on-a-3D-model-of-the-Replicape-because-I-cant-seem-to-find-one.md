---
layout: post
title: "I'm working on a 3D-model of the Replicape, because I can't seem to find one"
date: March 16, 2017 09:32
category: "Discussion"
author: Henrik Söderholtz
---


I'm working on a 3D-model of the Replicape, because I can't seem to find one. Is there any drawing with board dimensions specified available other than the .pls file?





**Henrik Söderholtz**

---
---
**Jon Charnas** *March 16, 2017 09:47*

Hi Henrik, I believe there was a model floating around on the Replicape Slack just a couple of days ago. There might be a 3D model of some kind (STEP?) on the replicape bitbucket repository.


---
**Klipper Pressure Advance** *March 16, 2017 12:30*

[bitbucket.org - intelligentagent / Replicape 
  / source  / Models / Replicape_B2.stl
 — Bitbucket](https://bitbucket.org/intelligentagent/replicape/src/2717a4b54c66b592cc67d7cf700adc2b5288bc23/Models/Replicape_B2.stl?at=Rev-A4&fileviewer=file-view-default)


---
**Henrik Söderholtz** *March 16, 2017 14:31*

Ah yes!


---
**Elias Bakken** *March 16, 2017 15:30*

Should be a STEP file there as well : )




---
**Henrik Söderholtz** *March 16, 2017 16:28*

Just a .skp model, unfortunately. If you would line to upload a step file that would be 😗👌


---
**Daniel Kusz** *March 16, 2017 17:35*

I sent you link to my files. There is .ipt Replicape model.


---
**Daniel Kusz** *March 16, 2017 17:38*

**+Elias Bakken** Very good model, but is there possibility to get .stp or IGES?


---
**Elias Bakken** *March 17, 2017 10:22*

Sorry, no step, but colladae is a good format for exchange and it is there. 


---
*Imported from [Google+](https://plus.google.com/103752299402481876381/posts/3y9bpMdoNtP) &mdash; content and formatting may not be reliable*
