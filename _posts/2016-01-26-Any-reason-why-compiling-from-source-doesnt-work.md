---
layout: post
title: "Any reason why compiling from source doesn't work?"
date: January 26, 2016 14:25
category: "Discussion"
author: James Armstrong
---
Any reason why compiling from source doesn't work? Followed the instructions but when I run the /usr/local/bin/redeem I get:



ERROR:root:You have to compile the native path planner before running Redeem. Make sure you have swig installed (apt-get install swig) and run cd ../../PathPlanner/PathPlanner && python setup.py install

Traceback (most recent call last):

  File "/usr/local/bin/redeem", line 9, in <module>

    load_entry_point('Redeem==1.1.5', 'console_scripts', 'redeem')()

  File "/usr/lib/python2.7/dist-packages/pkg_resources.py", line 356, in load_entry_point

    return get_distribution(dist).load_entry_point(group, name)

  File "/usr/lib/python2.7/dist-packages/pkg_resources.py", line 2476, in load_entry_point

    return ep.load()

  File "/usr/lib/python2.7/dist-packages/pkg_resources.py", line 2190, in load

    ['__name__'])

  File "/usr/local/lib/python2.7/dist-packages/Redeem-1.1.5-py2.7-linux-armv7l.egg/redeem/Redeem.py", line 50, in <module>

    from PathPlanner import PathPlanner

  File "/usr/local/lib/python2.7/dist-packages/Redeem-1.1.5-py2.7-linux-armv7l.egg/redeem/PathPlanner.py", line 40, in <module>

    raise e

ImportError: No module named PathPlannerNative



I see the PathPlannerNative.py file but it has a _ in the front of the name. I did the sudo python setup.py install



/usr/local/lib/python2.7/dist-packages/Redeem-1.1.5-py2.7-linux-armv7l.egg/_PathPlannerNative.py

/usr/local/lib/python2.7/dist-packages/Redeem-1.1.5-py2.7-linux-armv7l.egg/_PathPlannerNative.pyc

/usr/local/lib/python2.7/dist-packages/Redeem-1.1.5-py2.7-linux-armv7l.egg/_PathPlannerNative.so





**James Armstrong**

---
---
**Elias Bakken** *January 26, 2016 14:31*

Yes, for some reason when installing from the repo, there is a "_" added. I usually just change the import statement to reflect this, until I can figure out what is happening.


---
**James Armstrong** *January 26, 2016 14:33*

Ok. After the main setup.py is run, apparently you have to go into the python package for redeem into the path planner directory and do another setup.py and that then creates the pathplannernative.egg where if you just run the main setup.py you see it compile but it doesn't create the egg.



cd /usr/local/lib/python2.7/dist-packages/Redeem-1.1.5-py2.7-linux-armv7l.egg/redeem/path_planner



python setup.py install


---
*Imported from [Google+](https://plus.google.com/+JamesArmstrong3/posts/DHqbLJduwv8) &mdash; content and formatting may not be reliable*
