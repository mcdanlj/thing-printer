---
layout: post
title: "Elias Bakken when is Manga screen coming in stock?"
date: April 02, 2016 02:19
category: "Discussion"
author: varun s
---
**+Elias Bakken**​ when is Manga screen coming in stock? Need one for development. 





**varun s**

---
---
**Elias Bakken** *April 02, 2016 02:31*

New version coming this summer. 


---
**Jonathan Olesik** *April 02, 2016 15:57*

doh! I should have waited 


---
**Tim Orling** *April 02, 2016 23:51*

Did you finally get a display supplier? Yay!


---
**Elias Bakken** *April 02, 2016 23:52*

**+Tim Orling**, no, new revision!


---
**Tim Orling** *April 02, 2016 23:55*

Looking forward to it.


---
**Jozef Török** *April 10, 2016 09:27*

**+Elias Bakken**​ when will be available/in stock BBB boards? 


---
**Elias Bakken** *April 10, 2016 12:32*

BBBs are in stock now!


---
*Imported from [Google+](https://plus.google.com/116786516372429379291/posts/UwAXCAp5obH) &mdash; content and formatting may not be reliable*
