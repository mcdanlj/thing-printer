---
layout: post
title: "Connectivity Problems... I am still having problems establishing a stable connection with the BBC/Replicape"
date: December 01, 2016 14:15
category: "Discussion"
author: Max
---
Connectivity Problems...



I am still having problems establishing a stable connection with the BBC/Replicape. After a realy frustrating first experience with the replicape and Kamikaze 1.0  I waited a few Month for the Kamikaze 2.0 and after that the Kamikaze 2.0.8 Update, hoping it would fix my issues.



So today i downloaded the Kamikaze 2.0.8 release and flashed it to the BBC. After flashing the LEDs in the  the knight rider pattern for a few minutes, the BBC shuts down like discribed in the wiki (this is something my board would not do with Kamikaze 1.0 and 2.0, so Yaaaay!)



I disconnect the power, eject the SD and reapplied power. For me there is no way of knowing if all the scripts ran sucsessful, or if the board rebooted like discribed in the wiki. The LED`s just continue flashing in the same pattern after a while. At this point I cant establisch a connection with kamikaze.local in my browser or SSH with the 192.168.7.2 IP under Port 22. (Bonjour is installed, the board is connected to a USB port and also to my router over an Ethernet Powerline adapter )



 After waiting 20minutes I press the power butten briefly to restart the board. Still nothing. After restarting the board a couple of times I sometimes get a connection to kamikaze.local and the octoprint interface. Everything appears to be working. I have not been able to setup the config file for my ptinter, so I only got to test the motors and turn some fans on and off. But even if the connection is established I get random disconnects (Octroptint tells me that the server is currently not running) Also since Kamikaze 2.0.8 i can't connect via SSH at all.



Any help would be much apreciated



Cheers !

Max





**Max**

---
---
**Jon Charnas** *December 01, 2016 14:40*

Hi Max, I would recommend trying only to connect through the ethernet if you can for now. Also there's a problem (that I've also experienced) with Kamikaze 2.0.8 with regards to USB drivers. I can't get my wifi dongle or webcam to work on it reliably, and hooked it up to ethernet until I can get a better kernel version to work.



With regards to kamikaze 1.1 - you need to actually hold the "boot from SD" button to get the knight rider pattern, after editing the /boot/uEnv.txt file from the SD card itself (need a linux/OS X machine to do this though, Windows won't recognize the file system).


---
**Daniel Kusz** *December 01, 2016 14:51*

Like John writes try Ethernet cable and switch unit to connect with printer. 


---
**Max** *December 01, 2016 15:40*

I have already tried powering the Board with 12V and using only the ethernet connection. The result is the same. Sometimes it works for a short time, but after a while i get disconnected or can`t connect at all.



Daniel, what do you mean with "switch unit to connect with printer" ?


---
**Step Cia** *December 01, 2016 17:22*

I echo OP frustration especially the part where after flashing is performed there's no real way of knowing whether it was successful or not. Checked the wiki, youtube and bbb they all seem to have different way of achieveing the same thing... Perhaps the novice knows this by heart already but for someone like me who has limited experience with flashing firmware, It gets confusing very fast.



I feel the wiki could use some more detailed step by step instruction and not leaving it to interpretation or assumption that one need to have linux background to enjoy this board.



Having said that I understand that this whole thing if very much in the early development cycle. And if **+Elias Bakken**​ ever need opinnion from complete newbie to review instruction update :) I volunteer myself :)


---
**Daniel Kusz** *December 01, 2016 21:45*

**+Max Borhof** I mean internet switch. I'm using this kind of connection. 


---
*Imported from [Google+](https://plus.google.com/115636610276456924336/posts/g8dZe4oVmqe) &mdash; content and formatting may not be reliable*
