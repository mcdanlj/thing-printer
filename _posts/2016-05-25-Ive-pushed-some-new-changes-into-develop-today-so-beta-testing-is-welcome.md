---
layout: post
title: "I've pushed some new changes into develop today, so beta testing is welcome"
date: May 25, 2016 01:38
category: "Discussion"
author: Elias Bakken
---
I've pushed some new changes into develop today, so beta testing is welcome. A better auto-tune, (still WIP), set slave mode by G-code (M608) and ability to save the bed compensation matrix, apparently that was brokeh.

Note that this new push changes a few things in the config. pid_e_Kp  instead of pid_e_P etc., sensor_e instead of temp_chart_e, and the bed matrix has a new format. Check the default.cfg after installing. Would be great to run the config checker during start-up and get warnings on deprecated variable names etc. 





**Elias Bakken**

---


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/aUS4B5ivyAt) &mdash; content and formatting may not be reliable*
