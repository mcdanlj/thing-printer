---
layout: post
title: "Elias Bakken - Need to modify the BBB-Kernel included in the image for enabling the touch interface on my new Display [1]"
date: September 22, 2014 09:08
category: "Discussion"
author: Henrik Peiss
---
**+Elias Bakken**  - Need to modify the BBB-Kernel included in the image for enabling the touch interface on my new Display [1].



What is the best way to compile a new Kernel for the BBB with attached #replicape?

Is there is a modified kernel running BBB+Replicape? Do you have a  list of changes from a standard BBB kernel to the modified version as a starting point?



I aim  to modify the kernel for enabling the touch interface according to the manual [2] I have to 

* patch the kernel module

* patch the kernel source code

* blacklist existing modules



While doing this I have to decide if I have a non-X or X-Server-SetUp. AFAIK this is non-X and toggle is running directly via the framebuffer. My last experince with compiling kernels is 5 years old ...



Is there any change to include my mods in future thing images?



[1]: ebay (In German): [http://www.ebay.de/itm/7-17-78-cm-Display-Set-mit-Touchscreen-LS-7T-HDMI-DVI-VGA-CVBS-/131257118549?pt=LH_DefaultDomain_77&hash=item1e8f88af55](http://www.ebay.de/itm/7-17-78-cm-Display-Set-mit-Touchscreen-LS-7T-HDMI-DVI-VGA-CVBS-/131257118549?pt=LH_DefaultDomain_77&hash=item1e8f88af55)

[2]: EETI eGTouch Linux Programming Guide v2.5f

[https://drive.google.com/file/d/0B2YY4lgs9_ztNmJEMWpLZldpM2c/edit?usp=sharing](https://drive.google.com/file/d/0B2YY4lgs9_ztNmJEMWpLZldpM2c/edit?usp=sharing)﻿





**Henrik Peiss**

---
---
**Elias Bakken** *September 22, 2014 09:14*

**+Henrik Peiss** Here is the preferred way to rebuild the kernel or any of the other packages in the feed: [http://wiki.thing-printer.com/index.php?title=Rebuilding_the_packages_for_Angstrom](http://wiki.thing-printer.com/index.php?title=Rebuilding_the_packages_for_Angstrom)



It is written for the old image, so the latest uses v2014.06-yocto1.6



The package you want to build is virtual/kernel. Reproduce that first and then get back to me :)


---
**Elias Bakken** *September 22, 2014 09:23*

Ok, I've updated the instructions, trying them now...


---
**Elias Bakken** *September 22, 2014 12:15*

 Ok, looks like i've used a different commit of the TI repository for building 3.14..


---
**Elias Bakken** *September 22, 2014 12:35*

This might be the commit I'm using: [http://git.yoctoproject.org/cgit/cgit.cgi/meta-ti/commit/?id=1d4407e7f67a1a1955a7debb336020de304adc00](http://git.yoctoproject.org/cgit/cgit.cgi/meta-ti/commit/?id=1d4407e7f67a1a1955a7debb336020de304adc00)



I'll double check once I get beck to the office, but that might not be until tomorrow since I'm working on my PhD today...


---
**Elias Bakken** *September 23, 2014 09:03*

Ok, here is the meta-ti repository version I use. Make sure it looks like this in your layers.txt: meta-ti,git://[git.yoctoproject.org/meta-ti,master,1d4407e7f67a1a1955a7debb336020de304adc00](http://git.yoctoproject.org/meta-ti,master,1d4407e7f67a1a1955a7debb336020de304adc00)


---
**Elias Bakken** *January 06, 2015 15:15*

**+Henrik Peiss** How is it going with your project now? I've done a lot of work on Toggle lately, and I've added the option to rotate the display to any orientation, which should be good news for you if you want to use this display : )


---
*Imported from [Google+](https://plus.google.com/+HenrikPeiss/posts/EGsy5qNQfbt) &mdash; content and formatting may not be reliable*
