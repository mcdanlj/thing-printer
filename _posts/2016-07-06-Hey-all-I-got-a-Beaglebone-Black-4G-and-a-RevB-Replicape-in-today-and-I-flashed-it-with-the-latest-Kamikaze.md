---
layout: post
title: "Hey, all. I got a Beaglebone Black 4G and a RevB Replicape in today, and I flashed it with the latest Kamikaze"
date: July 06, 2016 00:17
category: "Discussion"
author: Blake Nolastname
---
Hey, all.



I got a Beaglebone Black 4G and a RevB Replicape in today, and I flashed it with the latest Kamikaze. It's tethered to my computer right now.



But I still can't access kamikaze.local. I have no idea what's wrong. Any ideas?





**Blake Nolastname**

---
---
**Philip Acierto** *July 06, 2016 00:25*

I access mine through Network, and it's listed under "Other Devices"


---
**Mark Walker** *July 06, 2016 04:06*

If you're trying to get there from windows, your probably missing Apple Bonjour Printer Services.


---
**Blake Nolastname** *July 06, 2016 05:22*

Why is an Apple product needed for a Windows OS?



And Philip, it's not there.


---
**Jon Charnas** *July 06, 2016 05:49*

Can you try using ethernet instead of usb? Windows is a known difficult system to connect to USB from... See the getting started wiki page as well ﻿[http://wiki.thing-printer.com/index.php?title=Replicape:_Getting_Started#I_can.27t_connect_the_BBB_to_my_network.2C_how_do_I_get_updates_from_online.3F](http://wiki.thing-printer.com/index.php?title=Replicape:_Getting_Started#I_can.27t_connect_the_BBB_to_my_network.2C_how_do_I_get_updates_from_online.3F)


---
**Mark Walker** *July 06, 2016 06:09*

Because Windows took a different path for peer name discovery and open source generally supported apple's approach to mdns/bonjour


---
**Mark Walker** *July 06, 2016 06:10*

So unless you set up a DNS server or your router does name resolution, bonjour is the method for associating the name with the address.


---
**Blake Nolastname** *July 06, 2016 07:41*

John I have an ethernet-to-USB cable and a standard USB cord. 



I installed Bonjour but I'm getting an error starting it up.


---
**Jon Charnas** *July 06, 2016 07:49*

Have you had a chance to watch the youtube video linked to from the wiki page? Also, which version of windows are you using? That could also make a difference.


---
**Blake Nolastname** *July 06, 2016 07:50*

I'm stuck on Win10 and the best I can get is the BBB being seen on COM4.


---
**Jon Charnas** *July 06, 2016 07:52*

Ok, have you tried the Beaglebone steps installing drivers for connectivity? [http://beagleboard.org/getting-started](http://beagleboard.org/getting-started)


---
**Blake Nolastname** *July 06, 2016 08:02*

Drivers are installed, and my system sees the Beaglebone on COM4. But it won't 'connect to beaglebone' in the getting-started package.


---
**Elias Bakken** *July 06, 2016 09:22*

On the USB, the BBB gets a static ip: 192.168.7.2 I think. Could you try that in your browser?


---
**Elias Bakken** *July 06, 2016 09:30*

There is a list of four things to double check in case something has gone wrong: [http://wiki.thing-printer.com/index.php?title=Kamikaze#Troubleshooting](http://wiki.thing-printer.com/index.php?title=Kamikaze#Troubleshooting)


---
**Blake Nolastname** *July 06, 2016 09:30*

Hey, Elias. I tried it, but no dice.



Apparently, I need an RNDIS driver for my OS. This is making me want to install some Linux distro on my laptop, at this point. Any recommendations??


---
**Elias Bakken** *July 06, 2016 09:33*

**+Blake Nolastname** changing OS over this sounds a bit extreme, but if that is what you want, I'm happy with Ubuntu on my laptop:) Mint is also popular!


---
**Jon Charnas** *July 06, 2016 09:35*

I'd recommend installing ubuntu on a USB key and not dumping windows entirely just for this. At least get your feet wet with Linux first. Once you've got Kamikaze setup with wifi/ethernet you'll be able to ssh in from windows just fine


---
**Blake Nolastname** *July 06, 2016 09:38*

My main tower has a 500GB SSD and 2TB HDD, and a 1TB hard disk in my laptop I use for various things.



But that's actually a good idea. I just have to get my wifi set up first on this blasted thing.


---
*Imported from [Google+](https://plus.google.com/115183749019601270463/posts/g1H54NbLNyY) &mdash; content and formatting may not be reliable*
