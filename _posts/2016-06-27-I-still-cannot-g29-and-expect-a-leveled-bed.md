---
layout: post
title: "I still cannot g29 and expect a leveled bed"
date: June 27, 2016 23:03
category: "Discussion"
author: Philip Acierto
---
I still cannot g29 and expect a leveled bed. I am sure its something I have set wonky in my config or end stops.. but I am about to rip my hair out trying to figure this out.



I have attached my current config, a log, and a video in multiple angles. Please help me solve this homing issue.



Note there is an inductive sensor (that works when hit over the aluminum tape if you look closely, the red light flashes). It is hooked up to Z2 end stop. It should also be noted that when I tell the printer manually to go to points 0,1,2 it goes correctly and over the tape, but when the g29 runs, it doesn't go to the points as expected. Z is inverted because up is closer to the nozzle.



Lastly, I tried this on the master, and am now currently on the dev branch. Trying anything to work. I've had this controller since launch and still have yet to do a single print...



                                                                     

Send: G29

Recv: End stop X1 hit!

Recv: End stop X1 hit!

Recv: End stop Y1 hit!

Recv: End stop Y1 hit!

Recv: Homing done.

Recv: End stop Z2 hit!

Recv: Homing done.

Recv: Found Z probe distance 0.00 mm at (X, Y) = (88.00, 10.00)

Recv: // action:bed_probe_point@[88.0, 10.0, 0.0]

Recv: Found Z probe distance 0.00 mm at (X, Y) = (88.00, 310.00)

Recv: // action:bed_probe_point@[88.0, 310.0, 0.0]

Recv: Found Z probe distance 0.00 mm at (X, Y) = (390.00, 150.00)

Recv: // action:bed_probe_point@[390.0, 150.0, 0.0]

Recv: End stop X1 hit!

Recv: End stop X1 hit!

Recv: End stop Y1 hit!

Recv: End stop Y1 hit!

Recv: Homing done.

Recv: // action:bed_probe_data@{"probe_type":"probe", "probe_data":

{"y": [10.0, 310.0, 150.0], "x": [88.0, 88.0, 390.0], "z": [0.0, 0.0,

0.0]}}

Recv: ok







[System]

machine_type = DinCore_XY

loglevel = 20



[Geometry]

axis_config = 2

bed_compensation_matrix =

	1.0, 0.0, 0.0,

	0.0, 1.0, 0.0,

	0.0, 0.0, 1.0

travel_x = 0.45

travel_y = 0.4

travel_z = 0.3

offset_x = 0.005

offset_y = 0.005

offset_z = 0.0

offset_e = 0.0



[Steppers]

microstepping_x = 6

microstepping_y = 6

microstepping_z = 8

microstepping_e = 6

current_x = 0.4

current_y = 0.4

current_z = 0.9

current_e = 0.4

steps_pr_mm_x = 8.0

steps_pr_mm_y = 8.0

steps_pr_mm_z = 20.0

steps_pr_mm_e = 8.0

direction_z = -1

direction_e = -1



[Cold-ends]

connect-therm-e-fan-0 = True



[Heaters]

pid_p_e = 0.142762682705

pid_i_e = 0.002279643636

pid_d_e = 2.23512825109

pid_p_hbp = 1.58068224052

pid_i_hbp = 0.0329308800107

pid_d_hbp = 18.9681868862



[Endstops]

invert_x1 = False

invert_x2 = False

invert_y1 = False

invert_y2 = False

invert_z1 = False

invert_z2 = False

end_stop_x2_stops =

end_stop_y2_stops =

end_stop_z1_stops =

end_stop_x1_stops = x_cw, y_cw

end_stop_y1_stops = x_cw, y_ccw

end_stop_z2_stops = z_ccw

soft_end_stop_min_x = -0.5

soft_end_stop_min_y = -0.5

soft_end_stop_min_z = -0.5

soft_end_stop_max_x = 0.45

soft_end_stop_max_y = 0.39

soft_end_stop_max_z = 0.3



[Planner]

max_speed_x = 3.0

max_speed_y = 3.0

max_speed_z = 0.025

max_speed_e = 3.0

acceleration_x = 2.0

acceleration_y = 2.0

acceleration_z = 0.6

acceleration_e = 2.0



[Homing]

home_speed_x = 0.2

home_speed_y = 0.2

home_speed_z = 0.01

home_backoff_speed_x = 0.05

home_backoff_speed_y = 0.05

home_backoff_speed_z = 0.005

home_backoff_offset_x = 0.01

home_backoff_offset_y = 0.01

home_backoff_offset_z = 0.01



[Macros]

g29 =

	M561                	                ; Reset the bed level matrix

	M558 P3                              ; Set probe type to Servo with switch

	M557 P0 X88 Y10			; Set probe point 0 (Bottom Left)

	M557 P1 X88 Y310		; Set probe point 1 (Top Left)

	M557 P2 X390 Y150		; Set probe point 2 (Middle Right)

	M574 Z2 z_neg ; Enable endstop Z2

	G28 X0 Y0          		; Home X Y

	G0 X88 Y10			; Move the X Y to safe place to Home Z

	G28 Z0              	        ; Home Z

	G0 Z8   	             	; Move Z up to allow space for probe

	G92 Z0   	          	; Reset Z height to 0

	G30 P0 S              	; Probe point 0

	G0 Z0	                	; Move the Z up

	G30 P1 S              	; Probe point 1

	G0 Z0	                	; Move the Z up

	G30 P2 S              	; Probe point 2

	G0 Z0	                	; Move the Z up

	G28 X0 Y0                   ; Home X Y

	M574 Z2 ; Disable endstop Z2

	G92 Z0.3 ; Compensate probe offset

	G0 Z0 ; Move nozzle to the lowest position





**Philip Acierto**

---
---
**Elias Bakken** *June 28, 2016 09:18*

This is very interesting! First of all, use the latest commit from Develop to do bed compensation. But it also looks like the you have some unneeded G92 commands in the G29. 

Also from the log, it looks like Z2 is hit after the homing and is kept there, so all distances are 0.0. What is the cause of that?


---
**Philip Acierto** *June 28, 2016 18:38*

I based my g29 macro off of the help from the last time I tried to get this board to work and from the wiki:



[http://wiki.thing-printer.com/index.php?title=Redeem](http://wiki.thing-printer.com/index.php?title=Redeem)



Right there it shows the zeroing out of the Z after each move. It looks like you were raising the nozzle, swinging an arm out, and then resetting to zero, but I am not sure. I am guessing best I can.



I am on the latest development branch.



Also, the red light flashes on the top of the inductive sensor when it is triggered, I can physically see when the end stop is hit. It triggers as expected the first time G28 Z0 is run, but then after that it never works as expected again in software. Physically it is not triggered again, so it makes me think there is something software still wonky.



If you watch the video you can see after it homes Z onces, the bed actually moves away from the nozzle more when I believe it should be trying to probe the point closer.


---
**Philip Acierto** *June 30, 2016 22:13*

There seems to be a bug with the end stops. Once Z is triggered, either by homing or by lowering the bed til it triggers, once the Z2 inductive sensor is triggered, it never untriggers in code. Physically its not triggered, this is only in software and it doesn't change from this state until I restart redeem. I have tried set soft end stops min and max to extremes that should never trigger, and no matter what nothing changes. I actually just found a bug where is you set the soft end stops to blank, Replicape crashes and redeem/octoprint does not boot. I am now having to SSH to manually fix the file and reinsert the values so that I can hopefully get back into octoprint to try and figure out some hack to make this thing print.



I also noticed, I have no X2, Y2, or Z1 end stops plugged in, but replicape still shows them as trigger regardless of me setting them as connected inverted false or true. I think this is also probably related to w/e end stop bug is forcing these to trigger.



6 months not a single print, pretty frustrating..



@Elias, you don't have any other ideas for me? Want to setup irc time to meet and do some debugging step by step with me? I am really out of ideas. I can't believe I am the only one having issues, or is CoreXY just not a printer other people do?


---
**Elias Bakken** *June 30, 2016 22:28*

I do have some ideas! Just logged on to #replicape on Freenode, but it's 
{% include youtubePlayer.html id=EqyB7jAt888 %}
[12:25](http://www.youtube.com/watch?v=EqyB7jAt888&t=12m25s) here so tomorrow might be a better time. If you are there we can chat a little bit perhaps!




---
**Philip Acierto** *July 01, 2016 20:47*

Ok, Elias helped figure out the issue with my Z2 probe getting stuck triggered, was a hardware issue with the inductive sensor, which he updated the wiki on:



[http://wiki.thing-printer.com/index.php?title=Replicape_rev_B#Inductive_sensors](http://wiki.thing-printer.com/index.php?title=Replicape_rev_B#Inductive_sensors)



I am still having issues with g29 with the carriage not going to the probe points everytime. For this test, every time this is from a fresh reboot of redeem with no changes to the config, and nothing entered into the terminal but "G29". One of two things happen: 



A)The carriage probes the 3 points correctly.



B)The carriage probes first two points correctly, and on its way to the 3rd point stops half way, moves a bit y-, then probes.. which with my inductive sensor and glass bed, will jam the head into the bed. Because my Z axis is driven by ball screws and dual Nema 23s.. it literally will break the machine in half, and has one already.. after replacing my machine's broken parts, I don't home without the power cable plus in my hand so I can disconnect the power if it malfunctions.



It is still super terrifying to home my bed, and I hope to resolve this probe point issue (and maybe use a mechanical homing switch as preventative...)



I will try to get a video this weekend showing what I Just described, but octoprint log and redeem log show nothing out of the ordinary when it does this, about 50% of the time; what can I log to help solve this intermittent issue?


---
**Philip Acierto** *July 01, 2016 21:48*

**+Elias Bakken** Actually, am I even looking at the right logs for this? Do I need to change the logging level perhaps on the redeem log?


---
**Elias Bakken** *July 01, 2016 21:52*

To look at the logs, set logging to 10. There are several ways to do that, but if you are using SSH to your board: 

echo M111 S10 > /dev/testing_noret_1

Then to see the log: 

journalctl -f -n 200




---
**Philip Acierto** *July 01, 2016 22:26*

I just realized the video above shows the printer doing the weird movement when it doesn't go to the 3rd probe point, at about the 10 second mark in the video. The 3rd probe point the printer is supposed to be hovering over you can see marked with aluminum tape on the opposite side of the print bed.



Since we fixed the Z probe, it does a z probe after the weird movement captured in the video above. In the video, it didn't do the z probe because of the problem with the inductive sensor that we have now fixed.



What would cause it to do that movement instead of going to the 3rd probe point.


---
*Imported from [Google+](https://plus.google.com/111974602640488998735/posts/gzgvWkckiN2) &mdash; content and formatting may not be reliable*
