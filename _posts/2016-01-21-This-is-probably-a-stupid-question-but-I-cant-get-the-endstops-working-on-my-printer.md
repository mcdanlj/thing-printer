---
layout: post
title: "This is probably a stupid question, but I can't get the endstops working on my printer"
date: January 21, 2016 02:28
category: "Discussion"
author: Chris Dagher
---
This is probably a stupid question, but I can't get the endstops working on my printer. I've looked at the config files, and created a new Local.cfg, but it hasn't worked. And ideas?





**Chris Dagher**

---
---
**Elias Bakken** *January 21, 2016 03:33*

It's not a stupid question! Look at the section with endstop_X1_stops = x_neg and similar. Copy that into local.cfg, remember the section header. You might also look at inverting the end stops. There is a lot of work done with this in the latest develop branch, I'll try to push a new deb package with the changes tomorrow!


---
**Chris Dagher** *January 21, 2016 16:23*

Is there a debugger view to see if endstops are triggered? I would prefer to check them manually instead of running the motors against them after I change the code.


---
**Elias Bakken** *January 21, 2016 16:37*

M119 i think it is? If not, just write M to get a list of implemented codes.


---
**Chris Dagher** *January 21, 2016 17:12*

Ok. I'll try it when I get back from school. Thanks!


---
**Chris Dagher** *January 22, 2016 02:13*

I've tried with the different settings and configurations, but haven't gotten it to work. I noticed it's going to event0, so I opened it up, and noticed it's empty. Is there supposed to be something in it?


---
**Jon Charnas** *January 23, 2016 08:37*

event0 is typically a linux input device - it won't show you anything if you just open it. If you want to see what comes out of event0, open a terminal and type 'tail -f /dev/event0' - it'll print whatever comes back in to the console for you to see. Then you can try to see if you get anything back when triggering the endstops.


---
*Imported from [Google+](https://plus.google.com/101836314773516090545/posts/AGTBkJucCZq) &mdash; content and formatting may not be reliable*
