---
layout: post
title: "What is the best/easiest way to control print speed?"
date: November 17, 2016 09:03
category: "Discussion"
author: Antaris Soren
---
What is the best/easiest way to control print speed? I want to slow down the printer speeds for testing.





**Antaris Soren**

---
---
**Daniel Kusz** *November 17, 2016 09:40*

In Octoprint - speed rate. 


---
**Antaris Soren** *November 17, 2016 11:14*

Do you mean "Feed Rate"?


---
**Daniel Kusz** *November 17, 2016 11:24*

Yes. Sorry for mistake. 


---
**Antaris Soren** *November 17, 2016 14:36*

I tried that and it didn't seem to effect the speed.


---
**Antaris Soren** *November 17, 2016 14:38*

Just tried it again and it worked! Go figure. Thanks


---
**Daniel Kusz** *November 17, 2016 14:42*

Yes. Click button with percentage value. 


---
*Imported from [Google+](https://plus.google.com/115412029632663838329/posts/28TMcfVXyCy) &mdash; content and formatting may not be reliable*
