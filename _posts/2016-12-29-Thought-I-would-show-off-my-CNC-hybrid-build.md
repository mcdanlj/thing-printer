---
layout: post
title: "Thought I would show off my CNC hybrid build"
date: December 29, 2016 06:33
category: "Discussion"
author: IonicBasher
---
Thought I would show off my CNC hybrid build. I am using a Rev A Replicape which has Machinekit support allowing me to use a 4th axis.



Planning on putting a huge aluminum bed for 3d printing at some point, which is why I went with the Replicape instead of just using Beaglebone and drivers. 



Hopefully Machinekit support is added on the Rev.B.



 It is nice to pop in the sd card for Machinekit and pop it out for Redeem.



![images/f85e385b91c891d52febbde1cb7adb1d.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/f85e385b91c891d52febbde1cb7adb1d.jpeg)
![images/d86823b9a0ec25a3f0c1c1423a9f7ab3.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/d86823b9a0ec25a3f0c1c1423a9f7ab3.jpeg)
![images/fd08a4080782c351773b1c04a3fe46ba.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/fd08a4080782c351773b1c04a3fe46ba.jpeg)

**IonicBasher**

---
---
**Philip Acierto** *December 29, 2016 19:14*

I knew someone out there had to be using Replicape with a CNC. Looks like this machine has already been put through its paces; very col.


---
**Sam Wong** *April 11, 2017 20:26*

I have portted B3 with machinekit for 3D printing. 

Hope that helps.  Hopefully not too hard to convert it to CNC. You might need to write some code to hook up the spindle though.



[github.com - sam0737/machinekit-replicape](https://github.com/sam0737/machinekit-replicape)


---
**IonicBasher** *April 12, 2017 23:26*

Nice! Its only thanks to you that I can use machinekit!




---
*Imported from [Google+](https://plus.google.com/100874739223986876085/posts/ddgPzZ7sfpe) &mdash; content and formatting may not be reliable*
