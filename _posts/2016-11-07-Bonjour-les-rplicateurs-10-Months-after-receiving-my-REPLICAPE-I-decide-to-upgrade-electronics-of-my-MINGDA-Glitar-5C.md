---
layout: post
title: "Bonjour les rplicateurs. 10 Months after receiving my REPLICAPE, I decide to upgrade electronics of my MINGDA Glitar 5C"
date: November 07, 2016 13:20
category: "Discussion"
author: Furax Gromov
---
Bonjour les réplicateurs.



10 Months after receiving my REPLICAPE, I decide to upgrade electronics of my MINGDA Glitar 5C.



 After successfully test my 2 BullDog extruder with a Chimera, I open the back of Chinese and begins with the cabler Y EndStop and stepper.

 At power-up, the 2 extruder grumble, normal Firmware 2.08, but not the Y motor. 

 After 2 hours of control and reconfiguration in REDEEM, I to connect the Y motor on the stepper X.  And it works ... 

 I remove the REPLICAPE and examines the pins, I see nothing , but the Y TMC2100 seem's have a short-circuit of track right to the exit. Did you already see that?

 While waiting for my new REPLICAPE, I will continue integration and  playing with slave_y=h



![images/88343f723b7b3e819610356620b3795d.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/88343f723b7b3e819610356620b3795d.jpeg)
![images/d191856d576636a38446f848a045d59a.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/d191856d576636a38446f848a045d59a.jpeg)

**Furax Gromov**

---
---
**Jon Charnas** *November 07, 2016 13:31*

Ouch. That's unfortunate. All I can say is, I've had my B3 for over a year and I've blown the HDMI on the BBB but nothing has been damaged on the replicape itself


---
*Imported from [Google+](https://plus.google.com/101492236644373072590/posts/Tu7ccPn6D3Y) &mdash; content and formatting may not be reliable*
