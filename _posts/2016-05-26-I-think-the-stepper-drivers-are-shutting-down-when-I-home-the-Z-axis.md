---
layout: post
title: "I think the stepper drivers are shutting down when I home the Z axis"
date: May 26, 2016 16:09
category: "Discussion"
author: Marcos Scriven
---
I think the stepper drivers are shutting down when I home the Z axis.



I have a Mendel90 which has two NEMA17 steppers (x16 microstepping) in series (E.g. to one single connection on the 'cape) for the Z axis. The fabulous folks on the IRC channel tactfully pointed out I had a too low current to drive two z motors, and a much too high speed. Having sorted that out, homing Z seemed to work.



Now however I'm trying to get '0' to mean the bed, so set home_z = -0.190 - hoping that would be roughly 1cm off the bed (E.g 1cm less than the roughly 20cm build height), so I could measure it more precisely.



Here's what happens:



1) The z-axis moves up towards my z endstop

2) Hits the end stop then pulls back

3) Moves back up to the end stop (usually this is slower, but I'm already going slow)

4) Then starts moving down towards the bed (as I wanted). However, it only moves about 6cm before stopping, and Octoprint just stops responding. I see no Redeem errors in /var/log/syslog. Without restarting redeem or octoprint, it does after a few minutes begin responding again.



The z driver is very hot to the touch, so wondering if this is what's happening? Certainly the steps/mm for z is right (Easy with an M6 rod - 1mm pitch, 200 rotations).



Question is what do I do? I've taken the current down to 1.0 (remember that's for two motors), and when it was too low, one of them didn't turn, causing the other to attempt to bend the x-carriage!



Here's my full config:



[System]



[Geometry]

# Cartesian XY

axis_config = 0



# Set the total length each axis can travel  [meters]

travel_x = 0.20

travel_y = 0.20

travel_z = 0.20



# Define the origin in relation to the endstops [meters]

offset_x = 0.00

offset_y = 0.00

offset_z = 0.00



# Stepper e is ext 1, h is ext 2 [In powers of 2, so '4' is 2^4 = 16]

[Steppers]

microstepping_x = 4

microstepping_y = 4

microstepping_z = 4

microstepping_e = 4



# Drivers are rather 1.5 max. Z needs more as two in series.

current_x = 0.5

current_y = 0.5

current_z = 1.0

current_e = 0.6



steps_pr_mm_x = 4

steps_pr_mm_y = 4

steps_pr_mm_z = 200

steps_pr_mm_e = 33.4



# Only one extruder

in_use_h = False



[Heaters]

# Epcos 100 K

temp_chart_E = B57560G104F



# Epcos 100 K

temp_chart_HBP = B57560G104F



[Endstops]

end_stop_X1_stops = x_ccw

# Normally you'd use Y1 - but seems to be broken on mine

end_stop_Y2_stops = y_ccw

end_stop_Z1_stops = z_ccw



[Homing]

# Homing speed for the steppers in m/s

home_speed_x = -0.02

home_speed_y = -0.02

home_speed_z = -0.001

home_speed_e = 0.02



home_x = -0.10

home_y = -0.10

home_z = -0.190



[Planner]

# Max speed for the steppers in m/s

max_speed_x = 0.1

max_speed_y = 0.1

max_speed_z = 0.001

max_speed_e = 0.1









**Marcos Scriven**

---
---
**Elias Bakken** *May 26, 2016 16:16*

Set your home_* values to 0 until you are done with the end stop config. 

Also, not related to this, the microstepping table is no longer 2^n like with Rev A. [http://wiki.thing-printer.com/index.php?title=Redeem#Steppers](http://wiki.thing-printer.com/index.php?title=Redeem#Steppers)


---
**Elias Bakken** *May 26, 2016 16:17*

Also of interest is the note on homing: [http://wiki.thing-printer.com/index.php?title=Redeem#Geometry](http://wiki.thing-printer.com/index.php?title=Redeem#Geometry)




---
**Marcos Scriven** *May 26, 2016 16:24*

Thanks **+Elias Bakken** - Believe it or not, I did RTFM :) In fact, that exact section!

Upon second reading though I see that 'travel_*' is important no only for its magnitude but sign. It seems to be a mixture of two concepts to me. On the one has it specifies how large the build area is, and implies something has gone wrong if you've travelled more than this. To me that just seems like a safety feature?



But its sign seems to specify what direction is up/down, left/right etc.


---
**Elias Bakken** *May 26, 2016 16:36*

Yes, no need to search further than necessary in case it has been configured wrong. 



It seems this is unintuitive for many people, though, so perhaps it would need some more attention. 



Seems to me you have it almost right except for the home_z stuff. 




---
**Marcos Scriven** *May 26, 2016 16:44*

Hah - so now I'm really confused :) Your first response said to leave the home_ values, but now you seem to say I've got it right except these? Sorry if I'm being dim. I do really think there's something not working, as even when I don't touch home_z, if the z axis is too far  from the top, it doesn't reach the end stop and stops without any messages, and stops responding for a few minutes.


---
**Elias Bakken** *May 26, 2016 16:52*

Depending on your redeem version, the home_* values are either set to 0 in default.cfg or implicitly defined by the soft end stops. Either way, set them to 0 explicitly and then work on them only after homing is working right. 


---
**Marcos Scriven** *May 26, 2016 17:41*

I'm using this version: Redeem initializing 1.2.2~Predator



When I import my config I see:



May 26 17:38:08 kamikaze octoprint[319]: 2016-05-26 17:38:08,977 - root - WARNING - Unknown option in file: travel_x in Geometry

May 26 17:38:08 kamikaze octoprint[319]: 2016-05-26 17:38:08,983 - root - WARNING - Unknown option in file: travel_y in Geometry

May 26 17:38:08 kamikaze octoprint[319]: 2016-05-26 17:38:08,988 - root - WARNING - Unknown option in file: travel_z in Geometry

May 26 17:38:09 kamikaze octoprint[319]: 2016-05-26 17:38:09,000 - octoprint.plugins.redeem - INFO - Renaming /tmp/octoprint-file-upload-hIVpTt.tmp to /etc/redeem/mendel90.cfg

May 26 17:38:09 kamikaze octoprint[319]: 2016-05-26 17:38:09,058 - octoprint.plugins.redeem - ERROR - get_profiles



But in every config example I see the travel_* options are in [Geometry]


---
**Elias Bakken** *May 26, 2016 17:44*

Yes, that is the reason why I advocate having explicit values defined in default.cfg instead of letting them be defined by the soft end stops as is currently the implementation. So automatic checking can be performed.


---
**Marcos Scriven** *May 26, 2016 17:46*

here's my exact config, pls can you tell me where I'm going wrong? 



[System]



[Geometry]

# Cartesian XY

axis_config = 0



# Set the total length each axis can travel  [meters]

travel_x = 0.20

travel_y = 0.20

travel_z = 0.20



[Steppers]

microstepping_x = 8

microstepping_y = 8

microstepping_z = 8

microstepping_e = 8



# Drivers are rather 1.5 max. Z needs more as two in series.

current_x = 0.5

current_y = 0.5

current_z = 1.0

current_e = 0.6



steps_pr_mm_x = 4

steps_pr_mm_y = 4

steps_pr_mm_z = 200

steps_pr_mm_e = 33.4



# Only one extruder

in_use_h = False



[Endstops]

end_stop_X1_stops = x_ccw

# Normally you'd use Y1 - but seems to be broken on mine

end_stop_Y2_stops = y_ccw

end_stop_Z1_stops = z_ccw



[Homing]

# Homing speed for the steppers in m/s

home_speed_x = -0.02

home_speed_y = -0.02

home_speed_z = -0.001

home_speed_e = 0.02



home_x = 0

home_y = 0

home_z = 0



[Probe]

# Define the origin in relation to the endstops [meters]

offset_x = 0.00

offset_y = 0.00

offset_z = 0.00



[Planner]

# Max speed for the steppers in m/s

max_speed_x = 0.1

max_speed_y = 0.1

max_speed_z = 0.001

max_speed_e = 0.1



[Heaters]

# Epcos 100 K

temp_chart_E = B57560G104F



# Epcos 100 K

temp_chart_HBP = B57560G104F




---
**Chris Romey** *May 26, 2016 18:33*

Hi Marcos, here's my local config for my prusa i3 which looks to be very similar to the mendel90. May be useful for reference. 



[http://files.mrzood.com/2020-i3-local.cfg](http://files.mrzood.com/2020-i3-local.cfg)


---
**Marcos Scriven** *May 26, 2016 18:36*

Thanks **+Chris Romey** - I downloaded and imported your config (not to use it, but just to check the Redeem logs), and I still see warnings:



May 26 18:20:02 kamikaze octoprint[328]: 2016-05-26 18:20:02,094 - root - WARNING - Unknown option in file: offset_x in Geometry

May 26 18:20:02 kamikaze octoprint[328]: 2016-05-26 18:20:02,099 - root - WARNING - Unknown option in file: offset_y in Geometry

May 26 18:20:02 kamikaze octoprint[328]: 2016-05-26 18:20:02,103 - root - WARNING - Unknown option in file: offset_z in Geometry

May 26 18:20:02 kamikaze octoprint[328]: 2016-05-26 18:20:02,107 - root - WARNING - Unknown option in file: travel_y in Geometry

May 26 18:20:02 kamikaze octoprint[328]: 2016-05-26 18:20:02,110 - root - WARNING - Unknown option in file: travel_x in Geometry

May 26 18:20:02 kamikaze octoprint[328]: 2016-05-26 18:20:02,115 - root - WARNING - Unknown option in file: travel_z in Geometry



That's with your config!



Are you not seeing issues when you import your config, in /var/log/syslog ?


---
**Marcos Scriven** *May 26, 2016 18:55*

So I think the log errors are a red herring (I only started looking anyway, because of issues). I've put my config into GitHub: 



[https://github.com/marcosscriven/3dmodels/blob/4e4de568de522c786c64ee6141ed2d8acadeefa2/redeem/mendel90.cfg](https://github.com/marcosscriven/3dmodels/blob/4e4de568de522c786c64ee6141ed2d8acadeefa2/redeem/mendel90.cfg)



With that config - x and y homing works great. Z has some VERY WEIRD behaviour. If it's my fault, please tell me why.



If (and only if) the z axis is within 60mm of the end stop, homing works. If not, it seems to only travel the 60mm before stopping. Then, a few minutes later, without any further input, starts again, going away from the z axis.



Another quirk. If I've successfully homed z (and as you can see, I'm explicitly, for now, setting home_* and offeset_* to 0), and I issue G1 Z-70.0, it will only travel 60mm before stopping.



There's a 60mm (or very near) limit in my z-axis somewhere. Nothing in the logs to suggest anything's gone wrong, but Redeem stops responding for a few minutes.


---
**Chris Romey** *May 26, 2016 19:11*

Marcos, I do not get those errors.


---
**Chris Romey** *May 26, 2016 19:13*

Did you symlink one of the printer profiles as printer.cfg?


---
**Marcos Scriven** *May 26, 2016 19:39*

**+Chris Romey** **+Elias Bakken** - Here's a video, because I just can't see what in my config would explain this behaviour. Sorry in advance it's a bit rambling: 
{% include youtubePlayer.html id=GynoB4rfrm0 %}
[https://www.youtube.com/watch?v=GynoB4rfrm0](https://www.youtube.com/watch?v=GynoB4rfrm0)


---
**Marcos Scriven** *May 26, 2016 20:29*

Goeland86 on IRC has suggested trying Redeem develop branch directly from the Bitbucket repo, which I'll try when I get a chance.


---
**Elias Bakken** *May 26, 2016 21:17*

First of all, I would say that z=0 should be close to the bed, so add some offset to the z. My best guess for why it stops at 60 is perhaps a soft end stop preventing it from going further down. 


---
**Chris Romey** *May 26, 2016 21:19*

Hrm.. Shouldn't need the dev branch. The thing that's throwing me off is your homing position values.  I would consider where your endstops are as home 200,200,200 rather than 0,0,0.  But I digress.


---
**Marcos Scriven** *May 26, 2016 21:24*

I agree with you both - the aim is for z=0 to be the bed. But I thought the guidance was to get the end stops / homing working first. I'm clear on how to use the offset_z - but not even getting back to the z stop if more than 60mm away seems bizarre. Also, the same setup for x and y is fine - with z its not. 


---
**Chris Romey** *May 26, 2016 21:54*

Is the Z axis driver getting extremely hot? perhaps it's triggering temp shutoff.  Especially at 1amp.  Grasping at straws, really.  But, it may need active cooling at that amperage.  I use H as a Z slave so I don't have to power both steppers off of one driver.


---
**Elias Bakken** *May 27, 2016 00:24*

I noticed you pressed down a bunch of times after hitting the mark, that might cause it to be spending time trying to move beyond the point. I think the current implementation needs you to go back up that same amount, which 'seems' like a block.


---
**Marcos Scriven** *May 27, 2016 06:15*

So this morning I've just tried this config. It's different in two ways.



1) I flipped the sign of the travel to negative, and the 'home_speed_' values  to positive.



2) I set the 'offset_' values to 18cm (2cm less than the size of the printer), in case it was hitting a 'soft' end stop.



I'm seeing the same thing. x and y home work fine and as described in the wiki:



- They go to the end stop

- They back off a bit and go back slower

- They go to the offset - which with my latest config is a couple of centimetres short, as configured and expected



For the z homing the first two happen. But while it's on its way to the offset_z (I.e back towards the bed, away from the z1 end stop), it again stops at this strange magic number of 60mm. Because I've set the offset_z to nearly the full height, I don't think its's the issue **+Elias Bakken** suggested.



Because it's always that exact distance from the end stop I'm skeptical it's the driver overheating, as even if I've moved the z-axis up and down a bit first (heating up the driver), it always still stops after that distance.



What's the next step here please? Do I have a broken cape or board? 



Git hash: [https://github.com/marcosscriven/3dmodels/blob/61df324853892b15c3969f1f2371157200a20fc9/redeem/mendel90.cfg](https://github.com/marcosscriven/3dmodels/blob/61df324853892b15c3969f1f2371157200a20fc9/redeem/mendel90.cfg)



Pasted here for ease of ref:



[System]



[Geometry]

# Cartesian XY

axis_config = 0



# Set the total length each axis can travel  [meters]

travel_x = -0.20

travel_y = -0.20

travel_z = -0.20



# Define the origin in relation to the endstops [meters]

offset_x = -0.18

offset_y = -0.18

offset_z = -0.18



[Steppers]

microstepping_x = 8

microstepping_y = 8

microstepping_z = 8

microstepping_e = 8



# Drivers are rated 1.5 max. Z needs more as two in series.

current_x = 0.5

current_y = 0.5

current_z = 1.0

current_e = 0.6



steps_pr_mm_x = 4

steps_pr_mm_y = 4

steps_pr_mm_z = 200

steps_pr_mm_e = 33.4



# Only one extruder

in_use_h = False



[Heaters]

# Epcos 100 K

temp_chart_E = B57560G104F



# Epcos 100 K

temp_chart_HBP = B57560G104F



[Endstops]

end_stop_X1_stops = x_ccw

# Normally you'd use Y1 - but seems to be broken on mine

end_stop_Y2_stops = y_ccw

end_stop_Z1_stops = z_ccw



[Homing]

# Homing speed for the steppers in m/s

home_speed_x = 0.02

home_speed_y = 0.02

home_speed_z = 0.001



home_x = 0.00

home_y = 0.00

home_z = 0.00



[Planner]

# Max speed for the steppers in m/s

max_speed_x = 0.1

max_speed_y = 0.1

max_speed_z = 0.001

max_speed_e = 0.1

﻿


---
**Marcos Scriven** *May 27, 2016 07:13*

I've also just been experimenting with G1 Z movements - and it's always this 60mm limit, in either direction. 



Here's an example:



1) Homed z (when it was just a few cm from the end stop). That again stopped on its way to offset_z at the 60mm mark



2) Checking the printer position, it thinks it's at z=0 (I.e that it's at home). But in reality it still has another 12cm to go to match the configured 18cm offset_z



3) Next I issue G1 Z-70, and it moves towards the bed 60mm before again stopping. Getting the position shows the software thinks z=-0.07, even though it again has only moved 60mm.



I've tried this multiple times (E.g while the z-driver is still warm from previous tries), and it's always the same. It seems it's not really a z homing issue, but an issue moving more than 60mm in either direction on the z axis. 




---
**Marcos Scriven** *May 27, 2016 07:38*

Here's debug log while homing z. Looks like a problem with 'Stepper watchdog timeout'



(Also copied to [http://paste.ubuntu.com/16727240/](http://paste.ubuntu.com/16727240/) )



May 27 07:34:49 kamikaze redeem[311]: 05-27 07:34 root         DEBUG    Executing G28 from buffered G28 Z0

May 27 07:34:49 kamikaze redeem[311]: 05-27 07:34 root         DEBUG    homing ['Z']

May 27 07:34:49 kamikaze redeem[311]: 05-27 07:34 root         DEBUG    endstop active mask = 0b111111

May 27 07:34:49 kamikaze redeem[311]: 05-27 07:34 root         DEBUG    homing internal ['Z']

May 27 07:34:49 kamikaze redeem[311]: 05-27 07:34 root         DEBUG    Doing homing for Z

May 27 07:34:49 kamikaze redeem[311]: 05-27 07:34 root         DEBUG    Search: {'Z': 0.2} at 0.001 m/s, 0.5 m/s^2

May 27 07:34:49 kamikaze redeem[311]: 05-27 07:34 root         DEBUG    Backoff to: {'Z': -0.01}

May 27 07:34:49 kamikaze redeem[311]: 05-27 07:34 root         DEBUG    Fine search: {'Z': 0.012}

May 27 07:34:49 kamikaze redeem[311]: 05-27 07:34 root         DEBUG    Center: {'Z': 0.18}

May 27 07:34:49 kamikaze redeem[311]: 05-27 07:34 root         DEBUG    Enabling stepper Y

May 27 07:34:49 kamikaze redeem[311]: 05-27 07:34 root         DEBUG    Enabling stepper X

May 27 07:34:49 kamikaze redeem[311]: 05-27 07:34 root         DEBUG    Enabling stepper Z

May 27 07:34:49 kamikaze redeem[311]: 05-27 07:34 root         DEBUG    Enabling stepper E

May 27 07:34:58 kamikaze systemd-timesyncd[198]: interval/delta/delay/jitter/drift 64s/-0.003s/0.048s/0.001s/+0ppm

May 27 07:35:12 kamikaze redeem[311]: 05-27 07:35 root         DEBUG    Poking watchdog

May 27 07:35:42 kamikaze redeem[311]: 05-27 07:35 root         DEBUG    Poking watchdog

May 27 07:35:42 kamikaze rsyslogd-2007: action 'action 17' suspended, next retry is Fri May 27 07:36:12 2016 [try [http://www.rsyslog.com/e/2007](http://www.rsyslog.com/e/2007) ]

May 27 07:35:48 kamikaze redeem[311]: 05-27 07:35 root         INFO     End Stop Z1 hit!

May 27 07:35:48 kamikaze redeem[311]: 05-27 07:35 root         DEBUG    Coarse search done!

May 27 07:36:02 kamikaze systemd-timesyncd[198]: interval/delta/delay/jitter/drift 128s/-0.003s/0.046s/0.003s/-12ppm

May 27 07:36:08 kamikaze redeem[311]: 05-27 07:36 root         INFO     End Stop Z1 hit!

May 27 07:36:08 kamikaze redeem[311]: 05-27 07:36 root         DEBUG    endstop active mask = 0b111111

May 27 07:36:08 kamikaze redeem[311]: 05-27 07:36 root         DEBUG    Home: {'Z': 0.0}

May 27 07:36:12 kamikaze redeem[311]: 05-27 07:36 root         DEBUG    Poking watchdog

May 27 07:36:12 kamikaze rsyslogd-2007: action 'action 17' suspended, next retry is Fri May 27 07:36:42 2016 [try [http://www.rsyslog.com/e/2007](http://www.rsyslog.com/e/2007) ]

May 27 07:36:42 kamikaze redeem[311]: 05-27 07:36 root         DEBUG    Poking watchdog

May 27 07:36:42 kamikaze rsyslogd-2007: action 'action 17' suspended, next retry is Fri May 27 07:37:12 2016 [try [http://www.rsyslog.com/e/2007](http://www.rsyslog.com/e/2007) ]

May 27 07:37:08 kamikaze redeem[311]: 05-27 07:37 root         DEBUG    Stepper watchdog timeout

May 27 07:37:08 kamikaze redeem[311]: 05-27 07:37 root         DEBUG    Disabling stepper Y

May 27 07:37:08 kamikaze redeem[311]: 05-27 07:37 root         DEBUG    Disabling stepper X

May 27 07:37:08 kamikaze redeem[311]: 05-27 07:37 root         DEBUG    Disabling stepper Z

May 27 07:37:08 kamikaze redeem[311]: 05-27 07:37 root         DEBUG    Disabling stepper E

May 27 07:37:12 kamikaze redeem[311]: 05-27 07:37 root         DEBUG    Poking watchdog

May 27 07:37:12 kamikaze rsyslogd-2007: action 'action 17' suspended, next retry is Fri May 27 07:37:42 2016 [try [http://www.rsyslog.com/e/2007](http://www.rsyslog.com/e/2007) ]


---
**Marcos Scriven** *May 27, 2016 07:46*

Poking around here - I see the timeout stops the motors: [https://bitbucket.org/intelligentagent/redeem/src/de9d0728f1c3c0a331c3b8134ea6e1921a3025cd/redeem/StepperWatchdog.py?at=master&fileviewer=file-view-default#StepperWatchdog.py-80](https://bitbucket.org/intelligentagent/redeem/src/de9d0728f1c3c0a331c3b8134ea6e1921a3025cd/redeem/StepperWatchdog.py?at=master&fileviewer=file-view-default#StepperWatchdog.py-80)


---
**Marcos Scriven** *May 27, 2016 07:53*

Ok - nearly fixed I think. Just to confirm I set my home_speed_x to just 0.01 as well, and homing x shows the same timeout issue. So it looks like I'm just the first replicape owner with a z axis that's too slow :)






---
**Marcos Scriven** *May 27, 2016 08:07*

Doesn't seem to be documented here: [http://wiki.thing-printer.com/index.php?title=Redeem](http://wiki.thing-printer.com/index.php?title=Redeem)



But I see the timeout is configurable here: [https://bitbucket.org/intelligentagent/redeem/src/de9d0728f1c3c0a331c3b8134ea6e1921a3025cd/redeem/Redeem.py?at=master&fileviewer=file-view-default#Redeem.py-515](https://bitbucket.org/intelligentagent/redeem/src/de9d0728f1c3c0a331c3b8134ea6e1921a3025cd/redeem/Redeem.py?at=master&fileviewer=file-view-default#Redeem.py-515)


---
**Marcos Scriven** *May 27, 2016 08:16*

I see here this is where the stepper watchdog should be reset: [https://bitbucket.org/intelligentagent/redeem/src/de9d0728f1c3c0a331c3b8134ea6e1921a3025cd/redeem/Printer.py?at=master&fileviewer=file-view-default#Printer.py-131](https://bitbucket.org/intelligentagent/redeem/src/de9d0728f1c3c0a331c3b8134ea6e1921a3025cd/redeem/Printer.py?at=master&fileviewer=file-view-default#Printer.py-131)



Maybe timeout could be calculated as a function of travel_ and home or max speed, instead of fixed? EDIT Or at least a warning in the logs if your timeout is less than the biggest move possible on that axis. I'll add an issue to the repo.


---
**Marcos Scriven** *May 27, 2016 08:32*

So I just watched in relief as my z axis glided gracefully out of its 60mm cage: [https://github.com/marcosscriven/3dmodels/commit/2cf121a5a691a7523340cc6947682b40d3410bee](https://github.com/marcosscriven/3dmodels/commit/2cf121a5a691a7523340cc6947682b40d3410bee)


---
**Marcos Scriven** *May 27, 2016 08:43*

[https://bitbucket.org/intelligentagent/redeem/issues/105/stepperwatchdog-timeout-should-be-info-and](https://bitbucket.org/intelligentagent/redeem/issues/105/stepperwatchdog-timeout-should-be-info-and)




---
*Imported from [Google+](https://plus.google.com/117910047317867058880/posts/FcArUNaV13C) &mdash; content and formatting may not be reliable*
