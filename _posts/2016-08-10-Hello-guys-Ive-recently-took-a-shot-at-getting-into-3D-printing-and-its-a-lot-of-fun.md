---
layout: post
title: "Hello guys! I've recently took a shot at getting into 3D printing, and it's a lot of fun!"
date: August 10, 2016 17:56
category: "Discussion"
author: Dan Cook
---
Hello guys! I've recently took a shot at getting into 3D printing, and it's a lot of fun! I bought a Cubex Duo mainly because of how the printer is designed. I know the software and proprietary accessories are crap. I've started replacing basically all of the electronics except for the stepper motors, and I'm considering the E3D Cyclops hot end with a modified direct feed filament system. Last week I ordered a Replicape and Manga Screen for my printer... I'll get the BeagleBone either this week or next since the Replicape is on backorder until later this month.



Anyways, I just want to know how complicated it is to get started with the Replicape system. I've watched all of the videos I could find, and it seems that there is quite a bit of info online. This is my first printer, and I may have been better off getting a Ramps or Rambo, but I'm notorious for taking big challenges. I may have a million questions in the future, and from the looks of it, there are quite a few intelligent members here... so let me thank you all in advance!



Cheers,



Dan Cook

Greenville, SC USA





**Dan Cook**

---
---
**Edwardo Garabito** *August 11, 2016 04:22*

Good luck. I'm in the same boat as you. From what I see the info on setting up the replicape is spread out all over the internet. From figuring out what currents your motors will need. Too figuring out what your effector-arm offset is. You'll be scratching your head in no time.



My best recommendation. Read the configuration.h file for your current set up. It'll give you a lot of useful info that you'll need to plug in, (ie build dimensions etc). After that its trial and error.  



The documentiaon isn't all there. But that's what this group is trying to fix. 




---
**Dan Cook** *August 11, 2016 14:39*

Thanks for the reply! It's weird... I posted this yesterday and it took a couple of hours to show up on the community page.



I'm trying to learn as much as possible about everything before my Replicape arrives. Fortunately, this is just a hobby, so trial and error is perfectly fine, as long as I don't fry anything of course, lol...



The one thing I'm kinda curious about right now is if my current reed switches will work as limit switches with the Replicape. They are nicely implemented into the design and I'm one for not only performance and capability, but also a clean layout.



Here's a look for example:

[http://2.bp.blogspot.com/-jdDuiiDiH1w/UfaRjmXwUJI/AAAAAAAAAeM/H2w2agXB4KM/s1600/SAM_3561.JPG](http://2.bp.blogspot.com/-jdDuiiDiH1w/UfaRjmXwUJI/AAAAAAAAAeM/H2w2agXB4KM/s1600/SAM_3561.JPG)


---
**Daniel Kusz** *August 11, 2016 20:46*

Great, that you bought Replicape and Manga screen. I also have them. I think that Replicape a really good piece of electronics, although I have no experience with others. I decided to build a 3D printer The Thing and use Replicape without any experience and knowledge of the construction of printers. I'm an engineer, so I was not afraid of challenges. After setting up my printer now most things are easy for me. My questions answered Elias and people from the community helped solve other problems. If you need help, write here - sooner or later we will respond if it is possible. Good luck!


---
**Dan Cook** *August 11, 2016 23:03*

Thanks, Daniel! I'm an engineer myself, so I love taking on these projects. Unfortunately, I'm a civil engineer, so the software side always creates a learning curve for me. It's great to have a community of skilled people in my case! I'm looking forward to getting my Replicate and getting started!


---
*Imported from [Google+](https://plus.google.com/105892126556694282818/posts/Sw3US9YYDLf) &mdash; content and formatting may not be reliable*
