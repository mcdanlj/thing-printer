---
layout: post
title: "G28 isn't reliably homing all axis on my delta printer"
date: September 17, 2017 18:19
category: "Discussion"
author: Chad Hinton
---
G28 isn't reliably homing all axis on my delta printer.  I can immediately do another G28 and it will find home on all three towers just fine.  What should I look for?



I just flashed Umikaze last night and brought forward my Redeem settings from the prior Kamikaze 2.0.8.



Sample Log:

Send: G28

Recv: End stop Z2 hit!

Recv: End stop Z2 hit!

Recv: End stop Z1 hit!

Recv: Homing done.

Recv: ok

[...]

Send: G28

Recv: End stop Z1 hit!

Recv: End stop Y1 hit!

Recv: Homing done.

Recv: End stop X1 hit!

Recv: ok



Even here the X1 hit was late.





**Chad Hinton**

---
---
**Alex Carlson** *September 17, 2017 19:45*

Can you post your local.cfg? 


---
**Alex Carlson** *September 17, 2017 19:46*

It concerns me that Z2 is firing off, I assume that's a probe?


---
**Chad Hinton** *September 17, 2017 21:26*

**+Alex Carlson** Here's my config.

[System]

machine_type = Folger Tech Kossel 2020



[Geometry]

axis_config = 3

offset_x = 0.0

offset_y = 0.0

offset_z = 0.0



[Steppers]

microstepping_x = 3

microstepping_y = 3

microstepping_z = 3

microstepping_e = 3

current_x = 0.5

current_y = 0.5

current_z = 0.5

current_e = 0.5

steps_pr_mm_x = 5.0

steps_pr_mm_y = 5.0

steps_pr_mm_z = 5.0

steps_pr_mm_e = 6.0

backlash_x = 0.0

backlash_y = 0.0

backlash_z = 0.0

backlash_e = 0.0

in_use_x = True

in_use_y = True

in_use_z = True

in_use_e = True

in_use_h = False

in_use_a = False

in_use_b = False

in_use_c = False

direction_x = 1

direction_y = 1

direction_z = 1

direction_e = -1

direction_h = 1

direction_a = 1

direction_b = 1

direction_c = 1

slow_decay_x = 0

slow_decay_y = 0

slow_decay_z = 0

slow_decay_e = 0

use_timeout = True

timeout_seconds = 500



[Endstops]

has_x = True

has_y = True

has_z = True

invert_x1 = True

invert_y1 = True

invert_z1 = True

invert_z2 = True

end_stop_x1_stops = x_pos

end_stop_y1_stops = y_pos

end_stop_z1_stops = z_pos

end_stop_x2_stops = 

end_stop_y2_stops = 

end_stop_z2_stops = x_neg, y_neg, z_neg

soft_end_stop_min_x = -0.210

soft_end_stop_min_y = -0.220

soft_end_stop_min_z = -0.5

soft_end_stop_max_x = 0.210

soft_end_stop_max_y = 0.220

soft_end_stop_max_z = 0.5



[Homing]

home_x = 0.0

home_y = 0.0

home_z = 0.3055

home_e = 0.0

home_speed_e = 0.01



[Macros]

g32 = 

	M574 Z2 x_neg,y_neg,z_neg  ; Enable the Z2 endstop (z-min)

g31 = 

	M574 Z2 x_neg,y_neg,z_neg  ; Disable the Z2 endstop (z-min).  Don't jack with them for now.



[Delta]

l = 0.24

r = 0.15

a_radial = 0.0

b_radial = 0.0

c_radial = 0.0



[Cold-ends]

connect-therm-e-fan-0 = False

connect-therm-e-fan-1 = True

add-fan-0-to-m106 = False

add-fan-1-to-m106 = True

cooler_0_target_temp = 60



[Heaters]

pid_kp_e = 0.0172202991985

pid_ti_e = 88.0257707596

pid_td_e = 6.35106571137






---
**Chad Hinton** *September 17, 2017 21:31*

And yes, I've got a z-min probe that I leave enabled until I'm sure G28 doesn't go slamming the extruder into the bed.  It likes to go about 100 - 150 mm in the negative direction before going up to try to find the endstops.


---
**Chad Hinton** *September 17, 2017 21:41*

**+Nathan Reichenberger** I've just added



# The total length each axis can travel

# This affects the homing endstop searching length.

travel_x = 0.5

travel_y = 0.5

travel_z = 0.5



to the geometry section.  Now it travels all the way down to the bed and heads back up, falling short.  Putting those back to a smaller value.


---
**Chad Hinton** *September 17, 2017 21:42*

As a side-note, it'd be cool if the local.cfg editor could do git commits.  I don't have the discipline to do it by hand yet and it'd be cool to be able to roll back.




---
**Chad Hinton** *September 17, 2017 22:13*

Changing the offset parameters in the geometry section fixed that issue.



offset_x = 0.3

offset_y = 0.3

offset_z = 0.3




---
**Alex Carlson** *September 17, 2017 22:42*

Ok so it should never travel down for a home, only up


---
**Alex Carlson** *September 17, 2017 22:43*

Make your travel negative, and your offset negative, have the offsets be close to the height of the nozzle from your bed.


---
**Alex Carlson** *September 17, 2017 22:46*

Home z will need to be a bit less than the offsets, but positive, otherwise the carriages will hit the end stops and stay there, not do the second homing 


---
**Chad Hinton** *September 18, 2017 00:53*

This seems to have done the trick.  With my config set as:



[Geometry]

axis_config = 3

offset_x = -0.3055

offset_y = -0.3055

offset_z = -0.3055

travel_x = -0.320

travel_y = -0.320

travel_z = -0.320



...snip...

[Homing]

home_x = 0.0

home_y = 0.0

home_z = 0.300

home_e = 0.0



It now only moves upward and settles down just shy of the endstops and still goes to the print bed with G1 Z0.



Thanks!




---
**Alex Carlson** *September 18, 2017 03:53*

Cool :) I suggest increasing your travel though. If you were to run g28 when your nozzle is off to the side, it wouldn't home right the first time. (Travel is basically the actual length of the possible travel on a tower.) total travel length is roughly home_z height +sin(60) x rod length , or -0.503


---
**Chad Hinton** *September 18, 2017 03:58*

I found that out when checking level and adjusting my R value.  I measured my towers to be 600 and used that value.


---
*Imported from [Google+](https://plus.google.com/109766677766216016353/posts/bwpyMzFd9gK) &mdash; content and formatting may not be reliable*
