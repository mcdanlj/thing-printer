---
layout: post
title: "And I thought I'd cleaned up the printer..."
date: April 01, 2016 15:48
category: "Discussion"
author: Jon Charnas
---
And I thought I'd cleaned up the printer... Now I've got the extra cables for the radial fan cooling the Cape, the z probe clamped onto a support next to it with its wires dangling down... Looks messy again. Sigh. 

![images/cefab36762c1612943f68d4118b95d98.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/cefab36762c1612943f68d4118b95d98.jpeg)



**Jon Charnas**

---


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/X5sqyZVJGFw) &mdash; content and formatting may not be reliable*
