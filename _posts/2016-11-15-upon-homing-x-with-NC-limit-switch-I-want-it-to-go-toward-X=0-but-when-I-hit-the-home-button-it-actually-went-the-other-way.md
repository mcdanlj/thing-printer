---
layout: post
title: "upon homing x with NC limit switch I want it to go toward X=0 but when I hit the home button it actually went the other way..."
date: November 15, 2016 07:59
category: "Discussion"
author: Step Cia
---
upon homing x with NC limit switch I want it to go toward X=0 but when I hit the home button it actually went the other way... How do tell it to home toward x = 0?



I see this option "invert_X1 = True" but this looks like just for toggling NC to NO





**Step Cia**

---
---
**Daniel Kusz** *November 15, 2016 08:08*

I dont remember exactly but change travel Sign (direction) X in Geometry. 


---
**Step Cia** *November 15, 2016 15:15*

The travel sign direction is already how I want it; right is positive and left is negative and I didn't want to flip that


---
**Daniel Kusz** *November 15, 2016 15:24*

Why? I think there is no other way, this is homing travel direction. Another possibility is to change homing speed but this means searching max endstop. 


---
**Step Cia** *November 15, 2016 16:11*

I didn't see " homing travel direction" option I might have missed that. Will check on that.

I could flip the travel direction but I don't want to do x-10 to go right side..


---
**Daniel Kusz** *November 15, 2016 16:18*

I mean travel_X is homing travel direction in Geometry section. When you change this you don't need to put "-" to travel in the right direction in normal moves. This is only direction for homing I think. Check this. 


---
**Step Cia** *November 16, 2016 06:50*

[solved] thanks. changing +/- on travel_x did it :)

loving the silence at 10mm/s speed basically 0 noise...


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/31g6j9DMPLN) &mdash; content and formatting may not be reliable*
