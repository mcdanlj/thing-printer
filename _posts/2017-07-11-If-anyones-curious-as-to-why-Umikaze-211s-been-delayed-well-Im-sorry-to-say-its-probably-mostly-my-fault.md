---
layout: post
title: "If anyone's curious as to why Umikaze 2.1.1's been delayed, well, I'm sorry to say it's probably mostly my fault"
date: July 11, 2017 21:16
category: "Discussion"
author: Jon Charnas
---
If anyone's curious as to why Umikaze 2.1.1's been delayed, well, I'm sorry to say it's probably mostly my fault.



I've started a new job in March in a startup, and as you all know, startups are rumored to eat your time. Sadly, in my case it's not a rumor. I've had to make it clear I needed enough sleep to be productive, and luckily management is agreeable to the suggestion that they'd rather have a smart programmer than a dummy code monkey.



Also, my Kossel Mini wasn't fitting the bill for my printing project anymore, so I decided I wanted something bigger... And the re-design and assembly took for ever because, well, see paragraph above. However I'm now happily printing with what I've dubbed "the Monster Kossel", which is in essence a Kossel XXL. The whole Monster Kossel files for the printed parts are online at [https://www.thingiverse.com/goeland86/collections/monster-kossel](https://www.thingiverse.com/goeland86/collections/monster-kossel)



I'm still documenting things and I believe the effector I tweaked still needs to be uploaded somewhere as well (it's a remix though, nothing fancy yet), so, I'll try to get to Umikaze 2.1.1 soon - in the meantime I'm still using 2.1.0 with minor tweaks and Redeem/develop branch on the Monster.



Cheers,

Jon





**Jon Charnas**

---


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/YsrCXY7kSuC) &mdash; content and formatting may not be reliable*
