---
layout: post
title: "Hi all, We've been quiet on the dev side for a while"
date: December 13, 2018 12:54
category: "Discussion"
author: Jon Charnas
---
Hi all,



We've been quiet on the dev side for a while.



However as of today one dev managed to build an image with Toggle support for Manga Screen 2, "Print from SD", "upgrade in place", OctoPrint 1.3.10 and a whole lot of path planner updates/fixes.



I'm hoping we can issue an official RC image out in time for the dragon and bug hunters among you to play with it over Christmas.



Plan for a release sometime in the first quarter of 2019.



This release will likely serve as the building block for the next phase of development which is to include support for the Revolve and the Reach in Redeem.





**Jon Charnas**

---


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/R5X4XhLffXV) &mdash; content and formatting may not be reliable*
