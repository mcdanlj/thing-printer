---
layout: post
title: "Hello everyone, im at the point of wiring my motors"
date: January 01, 2017 10:24
category: "Discussion"
author: Andrew A
---
Hello everyone, im at the point of wiring my motors.  can someone explain this to me?



Rev B3: 1, 2, 3, 4 = OA2, OA1, OB1, OB2<- Pretty standard



i have no idea what OA2, OA1, ect are......i find no indication in the data sheet for motors as to what wires those are, only the wire color for each end of the phase wire.  Even then, there's no indication as to which one would be '1' or '2'.



[http://www.omc-stepperonline.com/download/pdf/17HM15-0904S.pdf](http://www.omc-stepperonline.com/download/pdf/17HM15-0904S.pdf)



for example i get black and green for a phase, red and blue for a phase, thats it.



i tried looking in the replicape pictures and on the board, searched google+, there is nothing anywhere about what OA2 ect are.....tried searching youtube and google, nothing.....



oh and i might as well ask now, what about all the other connections as im not seeing a positive or negative for any of the connections anywhere, except for the 12v/24v input, so does it matter if the wires get swapped......







**Andrew A**

---
---
**Jon Charnas** *January 01, 2017 11:01*

Basically oa1 and oa2 correspond to coil a, and ob1 and ob2 correspond to the second. Motors generally have color coded wires defining which is which, so if your motor wires are the same color as in the wiki's diagram you can just wire them by color.


---
**Andrew Dowling** *January 01, 2017 14:36*

Use a voltage meter in continuity mode (or ohm mode if your meter is basic). One pair that has continuity (meter beeps or shows 0 ohms) will go on pin 1 and pin 2. The other pair that has continuity will go on pin 3 and pin 4. If the motor moves in the wrong direction, change the config file by adding a line "direction_x =  -1" to the axis that isn't moving in the right direction.


---
*Imported from [Google+](https://plus.google.com/106001037169639681932/posts/AmQscvG17Zw) &mdash; content and formatting may not be reliable*
