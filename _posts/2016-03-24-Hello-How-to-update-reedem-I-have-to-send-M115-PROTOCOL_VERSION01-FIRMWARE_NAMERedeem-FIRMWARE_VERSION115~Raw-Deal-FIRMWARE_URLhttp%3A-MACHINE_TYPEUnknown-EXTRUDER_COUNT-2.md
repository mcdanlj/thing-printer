---
layout: post
title: "Hello, How to update reedem? I have to send M115 PROTOCOL_VERSION:0.1 FIRMWARE_NAME:Redeem FIRMWARE_VERSION:1.1.5~Raw Deal FIRMWARE_URL:http%3A// MACHINE_TYPE:Unknown EXTRUDER_COUNT: 2"
date: March 24, 2016 17:00
category: "Discussion"
author: Francois Muelle
---
Hello,

 How to update reedem? 

I have to send M115

PROTOCOL_VERSION:0.1

FIRMWARE_NAME:Redeem

FIRMWARE_VERSION:1.1.5~Raw Deal

FIRMWARE_URL:http%3A//[wiki.thing-printer.com/index.php?title=Redeem](http://wiki.thing-printer.com/index.php?title=Redeem)

MACHINE_TYPE:Unknown

EXTRUDER_COUNT: 2





**Francois Muelle**

---
---
**Elias Bakken** *March 24, 2016 17:50*

Right now you have to do it manually. Log in via ssh, then write:

sudo apt-get install redeem


---
**James Armstrong** *March 24, 2016 17:51*

Doing the apt-get update then apt-get install redeem should work. Going by memory and assuming you are running from a downloaded image and not from source. Correct me if I am wrong since I don't have computer access right now. 


---
**Francois Muelle** *March 24, 2016 20:45*

Thank you very much, it 's ok.

Now redeem is 1.8.0 


---
*Imported from [Google+](https://plus.google.com/110879606286009280583/posts/HDsYy93EPpU) &mdash; content and formatting may not be reliable*
