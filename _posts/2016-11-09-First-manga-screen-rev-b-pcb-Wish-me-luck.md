---
layout: post
title: "First manga screen rev b pcb. Wish me luck!"
date: November 09, 2016 19:16
category: "Discussion"
author: Elias Bakken
---
First manga screen rev b pcb. Wish me luck!

![images/569ab0999a5c40a992a7127218c756aa.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/569ab0999a5c40a992a7127218c756aa.jpeg)



**Elias Bakken**

---
---
**Daniel Kusz** *November 09, 2016 19:25*

So good Luck! Size?


---
**Elias Bakken** *November 09, 2016 20:04*

First prototype lcd is 5.9", but we'll see what it ends up being:)


---
**Simon-Gabriel Gervais** *November 09, 2016 20:08*

Good luck!! cant wait to install this bad boy on my printer.


---
**Daniel Kusz** *November 09, 2016 20:09*

AIO Robotics has 7" :-D 


---
**Oyvind Dahl** *November 09, 2016 23:18*

nice!




---
**Jarek Szczepański** *November 10, 2016 08:36*

maybe higher res lcd? 4K? ;)


---
**Elias Bakken** *November 10, 2016 10:04*

Only 1K, Jarek, 1080p! 


---
**Mathieu Paquin** *March 20, 2017 14:28*

5.9" that would be perfect for my project, just saying ;)




---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/U3ShsRsFzAG) &mdash; content and formatting may not be reliable*
