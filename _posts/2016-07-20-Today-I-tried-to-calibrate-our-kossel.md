---
layout: post
title: "Today I tried to calibrate our kossel"
date: July 20, 2016 18:51
category: "Discussion"
author: Mathias Giacomuzzi
---
Today I tried to calibrate our kossel. So I jog in the Terminal. But my problem is that I can only move 50 mm in each axis as a maximum. How can I change that limit?



Example G0 X50 Y50 Z0 works

But G0 X51 Y50 Z0 doesn´t work => so there must be anywhere a limit of 50 mm but where?



thankx





**Mathias Giacomuzzi**

---
---
**Jon Charnas** *July 20, 2016 19:03*

Hi Mathias, check your soft_endstop values. Those are the "virtual" limits past which you won't be able to move the effector.


---
**Mathias Giacomuzzi** *July 20, 2016 20:28*



Ups that I've overlooked. Many Thanks




---
*Imported from [Google+](https://plus.google.com/104869072343043179738/posts/Ysa4aPQGHDM) &mdash; content and formatting may not be reliable*
