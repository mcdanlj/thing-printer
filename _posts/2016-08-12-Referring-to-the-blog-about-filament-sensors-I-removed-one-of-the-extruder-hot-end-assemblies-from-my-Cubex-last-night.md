---
layout: post
title: "Referring to the blog about filament sensors, I removed one of the extruder/hot end assemblies from my Cubex last night"
date: August 12, 2016 14:36
category: "Discussion"
author: Dan Cook
---
Referring to the blog about filament sensors, I removed one of the extruder/hot end assemblies from my Cubex last night. Is this worth toying with?



![images/85078a0153823c443885d7713246ee7b.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/85078a0153823c443885d7713246ee7b.jpeg)
![images/d527165baebd0b01fda3456bb841b715.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/d527165baebd0b01fda3456bb841b715.jpeg)

**Dan Cook**

---
---
**James Armstrong** *August 12, 2016 20:32*

We had found the same. Never did anything with it. [https://flic.kr/p/j76jxV](https://flic.kr/p/j76jxV)


---
**_-KaT-_ “KaTZWorld”** *August 12, 2016 20:54*

I took mine out if my cube looks the same ...was going to pull the gear off to reuse the step on the mod rebuild but was easier to just put a new steppers in place 


---
**Dan Cook** *August 15, 2016 15:16*

I looked around but couldn't find any info on the pinouts for the sensors. I could probably figure it out, but it might be easier just to use external sensors. Ken are you using Bowden or Direct on your Cubex? I'm starting to lean towards Bowden, or maybe one of each to give more material options.


---
**_-KaT-_ “KaTZWorld”** *August 15, 2016 15:21*

I have a old cube that hacked up. I have a Titan mounted on it but working on redoing the  mounting to make it compact and lightweight.    [https://www.instagram.com/p/BJIhwn9BVBi/](https://www.instagram.com/p/BJIhwn9BVBi/) if the one you have uses the same stuff rail system it will work once I post it to thingiverse will let you know.


---
**Dan Cook** *August 16, 2016 13:27*

That's pretty cool, Ken. Have you devised a way of keeping up with the flow of filament? I'm not really worried about keeping up with the amount of filament I'm using more than just having a way of forcing a pause to the printer if the filament stops flowing.


---
**_-KaT-_ “KaTZWorld”** *August 16, 2016 15:06*

I did away with that all together went with a normal hotend and didn't look back,  [http://www.thingiverse.com/thing:1722599](http://www.thingiverse.com/thing:1722599) is the mgn9 mount adapter I made to make this work. The hacked up cubify cube never seems to be done even though I use it for small prints on a regular basis.

one day I should get around to writing up the process that I used to put the ramps board into mine.


---
**Dan Cook** *August 16, 2016 15:29*

I'm doing the same thing... going to try the Chimera, but I've got to figure out how I'll do the extruding. I just thought CubeX's extruder design including the sensors was pretty cool.


---
**Jan Vik** *December 18, 2016 21:40*

Interesting "strip down" Dan :)  Could you explain if  the black sectors one the wheels are magnetic (triggering a reed switch) or just a non reflection for a optic sensor ?  I guess the Cube is actually counting drive wheel ws idler wheel continuously to detect clogging or slipping. Does it automatically pause and try to solve errors like the pro FDM machines (or the new Makerbot software) ? I would really like to get a setup with filament detected error engaging   - Pause/go to "tool change position"/retract/heat/purge filament, if OK/resume job - if error/retry (retraction- "overheating" - purge)  :)  


---
**Dan Cook** *December 19, 2016 03:16*

They're for an optic sensor... I binned those extruders and went with E3D Titan and v6...


---
*Imported from [Google+](https://plus.google.com/105892126556694282818/posts/7JjGB6xW3Ha) &mdash; content and formatting may not be reliable*
