---
layout: post
title: "on Rev B3A flashing procedure to 2.0.8 after the cylon LEDs done it powered down and never wake up anymore to show the all solid LED..."
date: December 27, 2016 20:11
category: "Discussion"
author: Step Cia
---
on Rev B3A flashing procedure to 2.0.8 after the cylon LEDs done it powered down and never wake up anymore to show the all solid LED... is this normal? I was waiting for the solid LEDs before unplugging USB





**Step Cia**

---
---
**Daniel Kusz** *December 27, 2016 20:20*

I think yes. Remove memory card and turn it on :-) 


---
**Step Cia** *December 27, 2016 20:23*

The troubleshoot on wiki says after done flashing, solid leds should appear but it never appear.... So I just unplugged USB and remove the card and applu power back. It semms to be thinking for a looong time...


---
**Daniel Kusz** *December 27, 2016 20:30*

OK. I remeber my board was sleeped after flashing and remember solid LED lights, but not sure if LEDs was still solid or only for moment. I read too fast your post and miss, that your leds do not appear after flashing :/


---
**Step Cia** *December 27, 2016 20:32*

That wes with etcher. I may need to try win32 if this goes on for longer than 1 hr


---
**Daniel Kusz** *December 27, 2016 20:35*

One time I had problems with the etcher. But with the Kamikaze 2.0.8 unless I used it and it worked. Win32 was always effective. Give him a chance.


---
**Step Cia** *December 27, 2016 22:55*

hmm d2 and d4 kept blinking for hours after sd card removal power up...


---
**Step Cia** *December 27, 2016 23:30*

well out of curiosity since the USB is plugged to my laptop, I tried [http://192.168.7.2/](http://192.168.7.2/) in my browser and it open octoprint right away! it appear everything is working?... how do I check which kamikaze version is really installed?


---
**Elias Bakken** *December 28, 2016 15:38*

If you do not get solid leds on, it means something has gone wrong, most likely not a 4GB sdcard. It might still work, though as you have discovered. Hmm, where does the version number go again? /etc/dogtag perhaps?


---
**Step Cia** *December 28, 2016 17:25*

I know the wiki says something must be wrong but I don't know what it is. This is the same 4gb card used to flash kamikaze version 1. I was able to navigate to octoprint reupload my printer profile cfg and check for xyz movement they all move smoothly as expected including extruder motor ( thanks for the cape replacement btw) 



At this point I'm not sure if I should go back with "proper" flashing to find that solid leds... I might just continue with setting everything up to get to print something 


---
**Andrew A** *January 12, 2017 02:22*

i just had the same problem basically, im on Rev B3, i did a flash back in last febuary i think and i got the 4 solid lights everything ok.  just updated to 2.0.8 and couldn't get the 4 solid lights, after repowering without the card in, it is showing activity and not stopping.  i don't have a mini hdmi cable so i have no idea what it is actually doing, so i think im going to try reflashing.  I did check my MD5 sum and that matched, but etcher shows a CRC32 Sum and the website i used to compare showed a different CRC, maybe i goofed up checking it, or maybe the flash really is bad.



One thing i want to mention as well, is that this etcher program has given me so many problems.  it constantly freezes and i have to reburn the image.  Im using a 32gb card so maybe thats why i dunno.


---
**Andrew A** *January 12, 2017 03:14*

hmm ok so i used this [esrg.sourceforge.net - MD5 And CRC-32 Calculation Utilities For Windows (Similar To Unix "md5sum -b")](http://esrg.sourceforge.net/utils_win_up/md5sum/)



and the md5 matches what is on the wiki page, but the crc32 does NOT match what etcher says.  this says 3DBC00E1, etcher says 2f98339f.  



i reflashed and im not sure if i got the 4 lights, but it did turn itself off this time.  i was able to get into octoprint but it says version 1.2 something, but isn't the newest version 1.3?  i tried updating but it says newest version, so i don't know what to think




---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/VerPhZtfYDF) &mdash; content and formatting may not be reliable*
