---
layout: post
title: "Is G29C broken on the Redeem that ships with Umikaze?"
date: September 18, 2017 04:06
category: "Discussion"
author: Chad Hinton
---
Is G29C broken on the Redeem that ships with Umikaze?  



It acts like it's going to do a great huge amount of work, homing and probing then dies attempting the second probe.  



I'm using this command:

G29C D140 C2 P8 S15 Z1 K1500



and G29C doesn't display any help, it just takes off doing stuff and then dies.



If I don't restart Redeem quickly then the Watchdog timer counts down and goes through its stuff.



Here's the syslog messages from one of the attempts:

Sep 18 02:14:19 kamikaze redeem[2158]: 09-18 02:14 root         ERROR    No GCode processor for No-Gcode. Message:

Sep 18 02:14:19 kamikaze redeem[2158]: 09-18 02:14 root         WARNING  M557: Missing Z-parameter

Sep 18 02:14:19 kamikaze redeem[2158]: message repeated 2 times: [ 09-18 02:14 root         WARNING  M557: Missing Z-parameter]

Sep 18 02:14:20 kamikaze redeem[2158]: 09-18 02:14 root         INFO     End Stop Y1 hit!

Sep 18 02:14:20 kamikaze redeem[2158]: 09-18 02:14 root         INFO     End Stop Z1 hit!

Sep 18 02:14:20 kamikaze redeem[2158]: 09-18 02:14 root         INFO     End Stop X1 hit!

Sep 18 02:14:22 kamikaze redeem[2158]: 09-18 02:14 root         INFO     End Stop X1 hit!

Sep 18 02:14:22 kamikaze redeem[2158]: 09-18 02:14 root         INFO     End Stop Y1 hit!

Sep 18 02:14:22 kamikaze redeem[2158]: 09-18 02:14 root         INFO     End Stop Z1 hit!

Sep 18 02:14:23 kamikaze redeem[2158]: 09-18 02:14 root         INFO     Homing done.

Sep 18 02:14:23 kamikaze redeem[2158]: 09-18 02:14 root         INFO     End Stop X1 hit!

Sep 18 02:14:23 kamikaze redeem[2158]: 09-18 02:14 root         INFO     End Stop Y1 hit!

Sep 18 02:14:23 kamikaze redeem[2158]: 09-18 02:14 root         INFO     End Stop Z1 hit!

Sep 18 02:14:26 kamikaze redeem[2158]: 09-18 02:14 root         INFO     End Stop Z1 hit!

Sep 18 02:14:26 kamikaze redeem[2158]: 09-18 02:14 root         INFO     End Stop Y1 hit!

Sep 18 02:14:26 kamikaze redeem[2158]: 09-18 02:14 root         INFO     End Stop X1 hit!

Sep 18 02:14:26 kamikaze redeem[2158]: 09-18 02:14 root         INFO     Homing done.

Sep 18 02:14:28 kamikaze redeem[2158]: 09-18 02:14 root         ERROR    No GCode processor for No-Gcode. Message:

Sep 18 02:14:28 kamikaze redeem[2158]: 09-18 02:14 root         INFO     Using config file /etc/redeem/default.cfg

Sep 18 02:14:28 kamikaze redeem[2158]: 09-18 02:14 root         WARNING  Missing config file /etc/redeem/printer.cfg

Sep 18 02:14:28 kamikaze redeem[2158]: 09-18 02:14 root         INFO     Using config file /etc/redeem/local.cfg

Sep 18 02:14:34 kamikaze redeem[2158]: 09-18 02:14 root         ERROR    No GCode processor for No-Gcode. Message:

Sep 18 02:14:34 kamikaze redeem[2158]: 09-18 02:14 root         INFO     Using config file /etc/redeem/default.cfg

Sep 18 02:14:35 kamikaze redeem[2158]: 09-18 02:14 root         WARNING  Missing config file /etc/redeem/printer.cfg

Sep 18 02:14:35 kamikaze redeem[2158]: 09-18 02:14 root         INFO     Using config file /etc/redeem/local.cfg

Sep 18 02:14:53 kamikaze redeem[2158]: 09-18 02:14 root         INFO     End Stop X1 hit!

Sep 18 02:14:53 kamikaze redeem[2158]: 09-18 02:14 root         INFO     End Stop Z1 hit!

Sep 18 02:14:54 kamikaze redeem[2158]: 09-18 02:14 root         INFO     End Stop Y1 hit!

Sep 18 02:14:56 kamikaze redeem[2158]: 09-18 02:14 root         INFO     End Stop Y1 hit!

Sep 18 02:14:56 kamikaze redeem[2158]: 09-18 02:14 root         INFO     End Stop Z1 hit!

Sep 18 02:14:56 kamikaze redeem[2158]: 09-18 02:14 root         INFO     End Stop X1 hit!

Sep 18 02:14:57 kamikaze redeem[2158]: 09-18 02:14 root         INFO     Homing done.

Sep 18 02:14:59 kamikaze redeem[2158]: 09-18 02:14 root         ERROR    No GCode processor for No-Gcode. Message:

Sep 18 02:14:59 kamikaze redeem[2158]: 09-18 02:14 root         INFO     Using config file /etc/redeem/default.cfg

Sep 18 02:14:59 kamikaze redeem[2158]: 09-18 02:14 root         WARNING  Missing config file /etc/redeem/printer.cfg

Sep 18 02:14:59 kamikaze redeem[2158]: 09-18 02:14 root         INFO     Using config file /etc/redeem/local.cfg

Sep 18 02:15:03 kamikaze redeem[2158]: [ 1505700903325 ]#011queueMove FAILED: axis 1 end position outside of soft limit (end = 0.18, max limit = 0.113)

Sep 18 02:15:04 kamikaze redeem[2158]: python: redeem/path_planner/Delta.cpp:408: double Delta::calculateCriticalPointTimeForAxis(int, const DeltaPathConstants&) const: Assertion `!std::isnan(firstT) && !std::isnan(secondT)' failed.

Sep 18 02:15:04 kamikaze octoprint[448]: 2017-09-18 02:15:04,189 - octoprint.util.comm - ERROR - Unexpected error while reading from serial port

Sep 18 02:15:04 kamikaze octoprint[448]: Traceback (most recent call last):

Sep 18 02:15:04 kamikaze octoprint[448]:   File "/usr/local/lib/python2.7/dist-packages/OctoPrint-1.3.4-py2.7.egg/octoprint/util/comm.py", line 1827, in _readline

Sep 18 02:15:04 kamikaze octoprint[448]:     ret = self._serial.readline()

Sep 18 02:15:04 kamikaze kernel: [ 6555.893231] watchdog watchdog0: watchdog did not stop!

Sep 18 02:15:04 kamikaze octoprint[448]:   File "build/bdist.linux-armv7l/egg/serial/serialposix.py", line 485, in read

Sep 18 02:15:04 kamikaze octoprint[448]:     raise SerialException('read failed: %s' % (e,))

Sep 18 02:15:04 kamikaze octoprint[448]: SerialException: read failed: [Errno 5] Input/output error

Sep 18 02:15:04 kamikaze systemd[1]: redeem.service: Main process exited, code=killed, status=6/ABRT

Sep 18 02:15:04 kamikaze octoprint[448]: 2017-09-18 02:15:04,257 - octoprint.util.comm - ERROR - Please see [https://bit.ly/octoserial](https://bit.ly/octoserial) for possible reasons of this.

Sep 18 02:15:04 kamikaze systemd[1]: redeem.service: Unit entered failed state.

Sep 18 02:15:04 kamikaze systemd[1]: redeem.service: Failed with result 'signal'.





**Chad Hinton**

---
---
**Alex Carlson** *September 18, 2017 05:20*

G29C was replaced with G29.2 to be more compliant with GCode standards, G29.2 should spit out code for you, and once you hit M500, it'll be saved into local.cfg, then for a delta, G33 uses this to do auto calibration




---
**Alex Carlson** *September 18, 2017 05:24*

also a quick heads up, before you use G33, type G33? to see what options there are. (if your version says you would type something like G33 F4, it would actually be G33 N4, that was changed to be more compliant as well.) N4 is endstops and radius, N6 is endstops, radius, 2 angular sets, N8 is endstops, radius, 2 angular, and 2 radial.


---
*Imported from [Google+](https://plus.google.com/109766677766216016353/posts/jPQzCNLdDZA) &mdash; content and formatting may not be reliable*
