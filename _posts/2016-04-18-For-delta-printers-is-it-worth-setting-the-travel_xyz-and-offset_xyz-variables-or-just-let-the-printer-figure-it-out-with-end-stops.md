---
layout: post
title: "For delta printers, is it worth setting the travel_(xyz) and offset_(xyz) variables or just let the printer figure it out with end stops?"
date: April 18, 2016 17:45
category: "Discussion"
author: Pieter Koorts
---
For delta printers, is it worth setting the travel_(xyz) and offset_(xyz) variables or just let the printer figure it out with end stops?



If setting them, is this the length of the rod entirely or the amount the carriage can travel in unison with other carriages and the effector centred?





**Pieter Koorts**

---
---
**Elias Bakken** *April 18, 2016 18:11*

I would say set them explicitly. The travel  is not important as long as it covers at least the longest travel for each axis. Offset however needs to be exact, and remember that homing on deltas happen in Cartesian space, so xyz need to be the same.


---
**Pieter Koorts** *April 18, 2016 18:52*

When you say xyz needs to be the same do you mean all values on offset_(xyz) should be the same? Or just that carriage movements should be?


---
**Elias Bakken** *April 18, 2016 18:55*

On a delta, if you want the nozzle to be centered on the bed, offset_(xyz) should be the same. 


---
**Daryl Bond** *April 18, 2016 22:13*

If your endstops aren't equally placed (like mine) then your offset_* values may be unequal. But for a well built printer they should be equal.


---
*Imported from [Google+](https://plus.google.com/100077479073911242630/posts/gsVzhTXHZ5r) &mdash; content and formatting may not be reliable*
