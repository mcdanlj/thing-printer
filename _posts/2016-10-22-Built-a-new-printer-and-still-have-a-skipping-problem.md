---
layout: post
title: "Built a new printer and still have a skipping problem"
date: October 22, 2016 01:13
category: "Discussion"
author: tizzledawg
---
Built a new printer and still have a skipping problem.



So I fitted a replicape to a prusa I3 and it works fine except very occasionally it skips steps in the Y axis. I lowered jerk and its better but still happens - chalked it up to a moving bed in the Y having too much weight.



So I built  a CoreXY and damn it's snappy however now it's skipping in the diagonal...swapped motors -no change. raised current - no change. Dropped accel to 1m/s^2 and jerk to 0.01 - no change. I thought maybe the current sense resistors on the Y driver were playing up., so I swapped them both from the H channel drive - no change.



I'm ordering some small heatsinks to glue under the board and run a fan across them but it doesn't explain why only the Y driver is having issues...



I should note that I can't hear the motor skipping steps., the print just shifts.



Any ideas?





**tizzledawg**

---
---
**Elias Bakken** *October 22, 2016 01:38*

Is it the same place every time on the model or is it random? If the temperature gets too high, you will get an alarm in OctoPrint, and the print will stop (in more recent versions of Redeem and the OctoPrint redeem plugin). 


---
**tizzledawg** *October 22, 2016 02:50*

It seems to be random. A few days ago it happened about 4 hrs into a print (95% done, quite upset) but lately it happens after maybe 2 or 3 layers so it's pretty much not working now.


---
**Daniel Kusz** *October 22, 2016 06:24*

What slicer do you use?


---
**tizzledawg** *October 22, 2016 08:17*

I've used Cura within octoprint and also tried Cura from desktop.



I've found it has something to do with object size.  Small objects are fine but something say 100mm across will skip.



I'm going to try a few more things - it might be the printer itself?


---
**Jon Charnas** *October 22, 2016 08:54*

I don't suppose you have a ramps board lying around to test the physical hardware with? That would tell you if it's the setup or the cape


---
**Daniel Kusz** *October 22, 2016 09:01*

I have the same problem with shifting layers from time to time with gcodes from cura and slic3r but last 2 prints with Craftware were very fine. Try this one and give some feedback. 


---
**tizzledawg** *October 22, 2016 10:21*

Derp....It was the belts.



I tightened one by a single tooth and now it's fine...

I haven't done a long print yet but a 1hr 15min one turned out alright at speed.



Thanks for everyone's input :D


---
**Daniel Kusz** *October 22, 2016 12:38*

Today the two approaches to a print from cura 2.3.0 twice switched layer in the same place. For any driver stuck radiator, two fans air blowing. I do not know for what it happens. I have to install kamikaze 2.0 and see if there will be some improvement.


---
**Jon Charnas** *October 23, 2016 18:28*

If the drivers were overheating the printer would stop and you would see a stepper alert in the octoprint console... Heat probably isn't your problem.


---
**tizzledawg** *October 25, 2016 08:57*

Ok I've found exactly what it was. I'm using 20T GT2 pulleys (stolen from the Prusa I3) and with a steel reinforced belt it can't bend around that radius too easily and what was happening was the belt teeth would ride over the teeth on the pulley and eventually skip. 



I've tightened them way up and it stopped it but i think there is still a chance on long prints so I've bought some 30T pulleys but will have to make new mounts to allow for the radius change.



When they come I'll let you guys know how it went :D


---
*Imported from [Google+](https://plus.google.com/108496831286068938419/posts/LsB4RiAVakF) &mdash; content and formatting may not be reliable*
