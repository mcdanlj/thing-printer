---
layout: post
title: "Announcement regarding kamikaze 2.1.0 development, please read and contact me if you have questions"
date: December 31, 2016 22:36
category: "Discussion"
author: Jon Charnas
---
Announcement regarding kamikaze 2.1.0 development, please read and contact me if you have questions. And join us on slack!



Happy New year!





**Jon Charnas**

---
---
**Jon Charnas** *January 01, 2017 19:43*

Slack is not a forum. It's a messaging system, and it's been up for a good 6 months. The purpose of slack is different than that of G+


---
**Jon Charnas** *January 01, 2017 20:12*

I agree it's not an ideal system. I've not used the forum at all myself. Google+ I think is better suited for multiple ongoing things, but it isn't easy to search for information in it. Slack is there as an alternative to IRC, (which also exists but is linked to slack, so forget IRC and just get on slack directly). It's meant for instant discussion and is more conversation oriented than Google+. Both slack and Google+ are there for community building and exchanges. The information we try to keep on the wiki, which is where it belongs, so you can browse it easier, instead of having to search through thousands of G+ posts and comments or slack messages. Slack doesn't replace G+, it complements it, in my view. They have a similar aim but different methods. The wiki is different, being a documentation repository instead of an exchange platform.


---
**Elias Bakken** *January 05, 2017 02:41*

Absolutely Jon! G+ is a social media channel, more suited for announcements and show and tell. Slack is for instant feedback. The forum (which is not as much used as it should) is better for structuring topics. All have their strengths and weaknesses:) 


---
**Jon Charnas** *January 05, 2017 09:10*

True. Slack was basically a replacement for the IRC channel, though it still exists and there's a two way link between #replicape and the#general channel on slack. There's also an open archive of the slack discussions somewhere, managed by yet another slack bot. At this time I think we can mostly ignore the forum and maybe try to add categories within our community on G+ to better orient the posts?


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/7vWHBRqsd1L) &mdash; content and formatting may not be reliable*
