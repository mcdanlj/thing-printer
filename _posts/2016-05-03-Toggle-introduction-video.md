---
layout: post
title: "Toggle introduction video!"
date: May 03, 2016 23:14
category: "Discussion"
author: Elias Bakken
---
Toggle introduction video! [http://www.thing-printer.com/toggle-1-0/](http://www.thing-printer.com/toggle-1-0/)





**Elias Bakken**

---
---
**Step Cia** *May 04, 2016 00:17*

Thanks would love to see more on these kind of "intro" video. 


---
**Tim Curtis** *May 04, 2016 10:47*

Can you set the heater temps through toggle?


---
**Elias Bakken** *May 04, 2016 11:46*

**+Tim Curtis** the preheat values are taken from the settings file.


---
**Tim Curtis** *May 04, 2016 11:49*

Ok thanks. So you can't manually enter heat values?


---
**Elias Bakken** *May 04, 2016 11:50*

No, unfortunately not.


---
**Daniel Kusz** *May 04, 2016 15:13*

I'd like to update but i'm not programmer. Will I find some tutorial about installing packages on BBB?


---
**Veridico Cholo** *May 04, 2016 15:46*

Thanks for the video Elias! 

So toggle is working fine with the octoprint plugin and for jog, pre-heat of the bed and hot-end, but the loading of models on the display does not. It just sits there "thinking". I have only three STL files in there and a bunch of gcode files. Is it getting confused trying to display the gcode files? As I understand, toggle displays STL files only. Could this be the issue?


---
**Daryl Bond** *May 05, 2016 00:21*

**+Daniel Kusz** from memory you should be able to update by calling "apt-get install toggle" from the command line. This assumes your BBB is connected to the Internet.


---
**Daniel Kusz** *May 05, 2016 19:58*

**+Daryl Bond** should I use puTTy to do this?


---
**Daryl Bond** *May 05, 2016 22:00*

Yep. Connect through to the BBB as user  root and you should then be able to use the command line interface to do everything you need.


---
**Jan M. Simonsen** *May 06, 2016 12:07*

How do i invert the gecode picture on manga screen? Becouse on my screen it's mirroed. 


---
**Daniel Kusz** *May 06, 2016 15:05*

**+Daryl Bond**​ Thanks! I have the last question. Package will download automatically?﻿


---
**Daryl Bond** *May 07, 2016 01:01*

If you log into the BBB as root with puTTy then call 'apt-get update' then 'apt-get install toggle'. That will first update the list of available software packages and then install the toggle package. You may also want to install the octoprint plugin for toggle 'apt-get install python-octoprint-toggle'. You may need to look up the name for the plugin as I am going from memory here!


---
**Elias Bakken** *May 07, 2016 04:46*

**+Jan M. Simonsen**, I have not not found a good way to solve this yet unfortunately, It is a bug listed in issues! Sorry about that!


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/QEWUkM14qe1) &mdash; content and formatting may not be reliable*
