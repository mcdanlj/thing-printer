---
layout: post
title: "What would be the Linux commands to switch to the development branch of redeem, say from a fresh flash of the sd card?"
date: June 17, 2016 11:07
category: "Discussion"
author: Andrew Dowling
---
What would be the Linux commands to switch to the development branch of redeem, say from a fresh flash of the sd card?

I am running windows and using an ssh client to get in - so I am that far.

I just cant find any straightforward "here is how you switch to the development branch of redeem" tutorials anywhere. Can I get a walkthrough?





**Andrew Dowling**

---
---
**Elias Bakken** *June 17, 2016 11:18*

You are right, it should be in the read me of the repo. Outline: 

cd /usr/src

git clone [https://bitbucket.org/intelligentagent/redeem.git](https://bitbucket.org/intelligentagent/redeem.git)

git checkout develop

make install_py

cp systemd/redeem.service /lib/systemd/system/

systemctl daemon-reload 

systemctl restart redeem




---
**Andrew Dowling** *June 17, 2016 11:41*

I did these exact commands.

cd /usr/srccd /usr/src

git clone [https://bitbucket.org/intelligentagent/redeem.git](https://bitbucket.org/intelligentagent/redeem.git)

cd redeem

git checkout develop

make install_py

cp systemd/redeem.service /lib/systemd/system/

systemctl daemon-reload

systemctl restart redeem

and now I get the "Machine State: Error: Failed to autodetect serial port, please set it manually."

the same type of thing I get when I upload a bad config file.


---
**Elias Bakken** *June 17, 2016 11:43*

Ok, sorry, you also need the new config fallback: 

cp /usr/src/redeem/configs/default.cfg /etc/redeem 

systemctl restart redeem

journalctl -f -n 100


---
**Andrew Dowling** *June 17, 2016 12:11*

ah ah sokka. it needs a .cfg file. I am starting to see how the system works a little with this, but I am not at the level that I can visualize those Linux commands. That got me connected.


---
**Elias Bakken** *June 17, 2016 12:23*

Good! Make sure you run M303 to get new values for the PID


---
**Andrew Dowling** *June 17, 2016 12:28*

you re-did the pid system, correct? I see it bobbing a little now, but I don't even care - the printrbot play is printing brilliantly. Thanks for the help. You should really wrap up your loose ends on things and get a good enough master version of things onto the emmc flasher soon. The latest version of the dev branch fixed like everything that was wonky with my machine.


---
**Elias Bakken** *June 17, 2016 13:36*

I will! I really feel the software is coming together now with the PID auto tune and the bed probe routine. I'll spruce up my Prusa I3 and make sure everything is working good there as well, make a couple of videos for documentation and then wrap it all up in a tight little package. 




---
**Jon Charnas** *June 20, 2016 14:44*

**+Elias Bakken** if you need help with the doc, get in touch - I can help you put it all neatly tidied up in the wiki.


---
**Elias Bakken** *June 20, 2016 15:24*

I'd love some help with the wiki! I'm working on a sick tool for visualizing the results of the G29 probing online, so the wiki has suffered by it. Any chance you could add some stuff on how to update to the latest software? Might be some stuff needed on homing still as well, seems to be questions related to that still as well. How to check if the end stops are working, how to set travel direction, how to set up end stops etc. :)


---
**Jon Charnas** *June 20, 2016 15:32*

Gotcha. I'll see what I can update tonight already. What do you think of a bi-monthly hangouts with the community to know what needs the most doc or fixing?


---
*Imported from [Google+](https://plus.google.com/114125966923153934677/posts/KZ67kpTgb7A) &mdash; content and formatting may not be reliable*
