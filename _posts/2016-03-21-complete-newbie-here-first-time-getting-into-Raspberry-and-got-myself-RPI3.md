---
layout: post
title: "complete newbie here :) first time getting into Raspberry and got myself RPI3"
date: March 21, 2016 03:53
category: "Discussion"
author: Step Cia
---
complete newbie here :) first time getting into Raspberry and got myself RPI3. with the manga screen I was able to change the config so the layout is taking full screen. but the touch screen isn't working. upon reading more into the wiki it looks like I have to do something with EDID to make the touch enable? looks like a lot of steps... just want to make sure if I'm on the right direction here. Thx

![images/b506a6b23f10521922af70215ff32f02.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/b506a6b23f10521922af70215ff32f02.jpeg)



**Step Cia**

---
---
**Step Cia** *March 23, 2016 20:36*

bump. is the multi touch feature is not enoble by default? do I need to update EDID?


---
**Algoritmi Lab** *August 01, 2016 00:22*

Hello Dear Step,



When plugging the Manga Screen to Raspberry Pi 3, and previously editing the two files/boot/config.txt and /usr/share/X11/xorg.conf.d/99-calibration.conf (as explained on the wiki documentation at [http://wiki.thing-printer.com/index.php?title=Manga_Screen](http://wiki.thing-printer.com/index.php?title=Manga_Screen)), I get a pink line on the left/down edge. I thought that is some problem with the display, but as its new and previously unused I tried to plug it to the Raspberry Pi 3 without modifying the two files, so the pink line disappeared from display screen, but there was some noise with small flickering parallel lines in the bottom section in portrait mode of the screen. Probably this is because of the inappropriate default resolution/refresh that the RRi 3 sends (or because I'm using the RPi with bigger standard PC monitor while not attached to the Manga Screen)



On the following link I uploaded a photo of the screen, the results when following instructions available at official Manga Screen wiki pages.



[https://postimg.org/image/f3odv8tp3/](https://postimg.org/image/f3odv8tp3/)



I also notice that the display image is zoomed out, so I cannot use the full resolution which the screen offers. Can you please help me how to avoid the pink line and zoom the image to the maximal resolution available?



Also I wanted to ask you what is the maximal refresh rate that Manga Screen can support, because with 60hz I get the dark zones while recording images/videos. Also please tell me if I can damage the display in any way by plugin it to the Raspberry Pi 3 without the editing of the two files as presented in the wiki documentation.



Thank you endlessly in advance, and sorry for the exhaustive question :-)




---
**Algoritmi Lab** *August 03, 2016 14:21*

Can you help me, please? :-)


---
**Step Cia** *August 03, 2016 16:58*

same boat as you here. I'm also a newbie :)


---
**Algoritmi Lab** *August 05, 2016 15:00*

Please tell me how to edit the config to make the image from RPi go fullscreen

Thank you very much!!!


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/3smbxKio6u6) &mdash; content and formatting may not be reliable*
