---
layout: post
title: "I found a solution how to config latest Raspbian Stretch so it works with Manga Screen 2 (in landscape) or HDMI Monitor without reconfig"
date: July 06, 2018 16:31
category: "Discussion"
author: Claudio Prezzi
---
I found a solution how to config latest Raspbian Stretch so it works with Manga Screen 2 (in landscape) or HDMI Monitor without reconfig.



First <b>sudo nano /boot/config.txt</b> and insert the following at the beginning: 



[EDID=IAG-MangaScreen2] 

# Settings for Manga Screen 2

hdmi_timings=1080 1 100 10 60 1920 1 4 2 4 0 0 0 60 0 144000000 3

max_framebuffer_width=1080

max_framebuffer_height=1920

display_rotate=1



Then <b>sudo nano /etc/udev/rules.d/99-manga.rules</b> an add:



ACTION=="add", ATTRS{name}=="Intelligent Agent AS Manga Screen 2", ENV{LIBINPUT_CALIBRATION_MATRIX}="0 1 0 -1 0 1" #rotate 270 degree



Thats it! At least for me ;)





**Claudio Prezzi**

---
---
**Tomáš Vít** *July 06, 2018 17:26*

Thanx for the tip, time to try it... 


---
**Elias Bakken** *July 06, 2018 18:34*

Oh that is awesome! Great catch!


---
**Andy Williams** *July 09, 2018 16:04*

Doesn't work for me Claudio.. I get 3 Pi desktops split across my screen in portrait. 


---
**Claudio Prezzi** *July 10, 2018 10:23*

My config is only valid for devices  with an EDID of "IAG-MangaScreen2". You can check your EDID with the command "tvservice -n"


---
**Andy Williams** *July 10, 2018 10:24*

**+Claudio Prezzi** I managed to sort it last night, i got confused with { and ( - all done and happy.. My only issue is I want the orientation to be where the HDMI cables are coming from the bottom of the screen, not the top of the screen as per your config... any advice to rotate even further ?




---
**Claudio Prezzi** *July 10, 2018 12:17*

**+Andy Williams** This could be done by changing display_rotate to 3 in config.txt and change the ACTION string above to "ACTION=="add", ATTRS{name}=="Intelligent Agent AS Manga Screen 2", ENV{LIBINPUT_CALIBRATION_MATRIX}="0 -1 1 1 0 0" # 90 degree clockwise".


---
**Andy Williams** *July 10, 2018 15:00*

**+Claudio Prezzi** Man, thank you ! Next up, orientation of the mouse is off now ! slide right on the screen and the mouse goes left - I'll work it out ! Thanks so much for your advice


---
**Tomáš Vít** *July 10, 2018 15:25*

**+Andy Williams** I am precisely at this state. Thanx in advance for a note if you find a solution of both layers (picture, touch response). ;)


---
**Andy Williams** *July 10, 2018 15:36*

**+Tomáš Vít** Hi Tomas, how do you mean both layers ? I'm working fine now, just need to reverse the mouse movements




---
**Tomáš Vít** *July 10, 2018 15:52*

**+Andy Williams** Yes, I have meant proper picture layer and sensitive layer justified and with cables heading down. ;) 


---
**Andy Williams** *July 10, 2018 16:08*

**+Tomáš Vít** Ah.. I'll cut and paste my config later. What are you using your screen for ?


---
**Claudio Prezzi** *July 11, 2018 07:31*

**+Andy Williams** Is your touch screen just left/right reversed or also top/down?


---
**Claudio Prezzi** *July 11, 2018 09:48*

Ok, got it working. For Landscape with cables down, you need the following settings:



In config.txt: 

<b>display_rotation=3</b>



In 99-manga.rules:

<b>ACTION=="add", ATTRS{name}=="Intelligent Agent AS Manga Screen 2", ENV{LIBINPUT_CALIBRATION_MATRIX}="0 -1 1 1 0 0" </b>#rotate<b> 90 degree</b>


---
**Tomáš Vít** *July 13, 2018 15:12*

**+Claudio Prezzi** It works. Lama question: Is there a possibility to use touch layer under Raspbian at all? ;)


---
**Claudio Prezzi** *July 14, 2018 12:58*

**+Tomáš Vít** The touch is simulating mouse moves/klicks, but real touch gestures like pinch/zoom are not supported, as far as I know.


---
**Tomáš Vít** *July 14, 2018 16:15*

**+Claudio Prezzi** I know, but can't get any touch response under Raspbian yet.


---
**Claudio Prezzi** *July 14, 2018 23:20*

**+Tomáš Vít** Did you try with a normal USB cable?


---
**Tomáš Vít** *July 15, 2018 06:32*

**+Claudio Prezzi** Have tried two, will dig for more. Thank you for a tip.


---
**Claudio Prezzi** *July 15, 2018 08:25*

I tried first with a very short cable of a power bank, but it sems this cable only has power wires but no data wires connected.


---
*Imported from [Google+](https://plus.google.com/+ClaudioPrezzi/posts/dJB7mkQoL2U) &mdash; content and formatting may not be reliable*
