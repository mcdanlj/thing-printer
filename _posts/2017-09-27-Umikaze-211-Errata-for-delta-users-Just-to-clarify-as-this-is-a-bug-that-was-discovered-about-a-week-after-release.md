---
layout: post
title: "Umikaze 2.1.1 Errata for delta users: Just to clarify, as this is a bug that was discovered about a week after release"
date: September 27, 2017 14:56
category: "Discussion"
author: Jon Charnas
---
Umikaze 2.1.1 Errata for delta users:



Just to clarify, as this is a bug that was discovered about a week after release. Well, not so much bug as misdocumentation.



G33 Fn is not the expected syntax. Even if G33? says it is. The documentation wasn't updated properly in Redeem. Use G33 Nn instead.



Apologies for the confusion for all delta users affected by this issue... G33 Fn will work, but will only apply 4-factor calibration (endstops + radius) to your delta printer.





**Jon Charnas**

---


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/LJwDfNVqriC) &mdash; content and formatting may not be reliable*
