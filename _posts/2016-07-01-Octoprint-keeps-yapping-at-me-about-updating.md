---
layout: post
title: "Octoprint keeps yapping at me about updating"
date: July 01, 2016 18:32
category: "Discussion"
author: Philip Acierto
---
Octoprint keeps yapping at me about updating.



When looking into updating, it needs to know the directory it is installed at, and then I thought:



Should we be updating octoprint? Can we? If not, can we turn off the update messages?





**Philip Acierto**

---
---
**Elias Bakken** *July 01, 2016 18:41*

There is a script that will update to the latest version: [https://github.com/eliasbakken/kamikaze/issues/1](https://github.com/eliasbakken/kamikaze/issues/1)






---
**Philip Acierto** *July 01, 2016 18:44*

Oh, was hoping it be as simple as clicking update in octoprint. Guess it's time to ssh :)


---
**Elias Bakken** *July 01, 2016 18:45*

It's coming in Kamikaze2! :)




---
**Jon Charnas** *July 02, 2016 09:47*

Yes, it will be in kamikaze 2. As to whether you should update **+Philip Acierto**, I highly recommend it. The latest versions of octoprint are more reliable and use less CPU. However I need to complete the comment on the kamikaze script as there are a few minor steps missing in how to set it up right.


---
**Philip Acierto** *July 02, 2016 16:04*

Let me know when it's ready and I'll give it a go. Thanks.


---
**Jon Charnas** *July 03, 2016 07:36*

**+Philip Acierto** give it a go - let me know if you run into any issues. I just had a minor syntax error in the sed command that's been fixed.


---
**Philip Acierto** *July 03, 2016 19:15*

**+Jon Charnas**  Alright, I ran the script posted line by line, no errors that i could see. I actually rebooted replicape afterwards to be after seeing the below and no change. Octoprint still wants me to update saying:



OctoPrint: 1.2.8.post0.dev0+gdedadbc 

Installed: 1.2.8.post0.dev0+gdedadbc

Available: 1.2.13 (stable)



On plugin tab of octoprint - The pip command could not be found. Please configure it manually. No installation and uninstallation of plugin packages is possible while pip is unavailable.



Octoprint Software Update: Please configure the checkout folder of OctoPrint, otherwise this plugin won't be able to update it. Click on the  button to do this. Also refer to the Documentation.



While installing I saw it say it installed 1.2.13, but my old version of octoprint is still there it seems? Any ideas?


---
**Jon Charnas** *July 03, 2016 20:49*

Huh. ok, that's not good. I'll backup everything and reflash kamikaze from scratch to keep it updated. About the pip not found issue, that's a setting in octoprint, when you open the plugin manager, click on the wrench icon (top right), and specify "/usr/local/bin/pip" in the pip path...


---
**Chris Romey** *July 04, 2016 03:11*

I'm getting the same issue on a fresh kamikaze re-flash as well.  Trying to figure it out but I'm not the best with Linux :)


---
**Philip Acierto** *July 06, 2016 00:49*

any ideas **+Jon Charnas** ?


---
**Jon Charnas** *July 06, 2016 05:52*

Working on it 


---
**Philip Acierto** *July 06, 2016 07:06*

**+Jon Charnas** [https://github.com/eliasbakken/Kamikaze2/blob/master/make-from-kamikaze1.sh](https://github.com/eliasbakken/Kamikaze2/blob/master/make-from-kamikaze1.sh)


---
**Jon Charnas** *July 06, 2016 07:18*

**+Philip Acierto** You can try it, but the script is still being developped, there may be bugs ahead. I'll try to get a solid fix on the original script for Kamikaze 1 later today. Need to setup a VM with Kamikaze installed so I can snapshot it and try various things.


---
*Imported from [Google+](https://plus.google.com/111974602640488998735/posts/R6kpwoPtiMA) &mdash; content and formatting may not be reliable*
