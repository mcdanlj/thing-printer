---
layout: post
title: "I just got my Replicape! Still doing the basic setup, but every time I move a stepper for testing ."
date: August 20, 2017 17:43
category: "Discussion"
author: Aditya Gandhi
---
I just got my Replicape!



Still doing the basic setup, but every time I move a stepper for testing .



I hear a high pitched humming noise for a while after the steppers have stopped moving? Why is this happening? I read about boot-up noise, but not this. 



Please help





**Aditya Gandhi**

---
---
**Jon Charnas** *August 20, 2017 18:04*

This is related to the driver's stepper mode and decay settings. 



If you set it to stealthchop (mode 7 or 8) do you still hear the noise? My guess is that you probably won't.



If not, try going back to the original mode you were using and play around with the decay settings until you get rid of the noise. Decay has an effect and is related to whether or not your motors have low or high inductance, and the hissing is then the white noise of the microstepping that you're hearing.


---
**Aditya Gandhi** *August 20, 2017 19:18*

Thanks Jon! I suspect that is the issue. That helped reduce the noise. 



I am now facing another issue , I configure the z-axis as per this website and the wiki, I have a Prusa i3 2020 clone, M8 lead screw. 



I see an issue, I try to move 1 mm on z-axis but it moves a lot more. 



[Steppers]

microstepping_x = 7

microstepping_y = 7

microstepping_z = 7

microstepping_h = 7

microstepping_e = 7



#Mirror z on H

in_use_h = True



slave_z = H



steps_pr_mm_x = 6.25

steps_pr_mm_y = 6.25

steps_pr_mm_z = 160.0

steps_pr_mm_h = 160.0

steps_pr_mm_e = 8.0



current_z = 0.7

current_h = 0.7

current_e = 0.7

current_y = 0.7

current_x = 0.7



slow_decay_x = 1

slow_decay_y = 1

slow_decay_z = 1

slow_decay_h = 1

slow_decay_e = 1





[Geometry]

# 0 - Cartesian

# 1 - H-belt

# 2 - Core XY

# 3 - Delta

axis_config = 0



offset_x =  0.0005

offset_y =  0.0005

travel_y =  0.190

travel_x =  0.185

travel_z =  0.20

travel_h =  0.20

offset_h =  0.0001

offset_z =  0.0001



[Planner]

max_speed_z = 0.003

max_speed_h = 0.003



[Homing]

home_x = 0

home_y = 0

home_z = 0

home_h = 0

home_speed_x = 0.05

home_speed_y = 0.05 

home_speed_z = 0.05

home_speed_e = 0.05

home_speed_h = 0.05

![images/0b85fea022cc0aec96c170169d675f9b.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/0b85fea022cc0aec96c170169d675f9b.png)


---
**Jon Charnas** *August 20, 2017 19:44*

hmm, that is indeed troublesome. But I'm assuming that H and Z are moving synchronously though, right? If so, then I'd suggest that you try the calculator for different M8 pitches if you can - it could be an 8mm diameter with a different pitch, meaning different steps per millimeter. That's the only real explanation for moving more distance than you should when using a cartesian.


---
**Aditya Gandhi** *August 20, 2017 20:25*

Resolved it from a previous post. 



Please update wiki to say 25 steps, or not use the website linked.. There is a mistake on the website , if it says 160 steps/mm for 1 step config.. 


---
**Jon Charnas** *August 20, 2017 20:28*

We'll update the wiki soon. Thanks for spotting the issue.


---
**Phil Turner** *August 20, 2017 22:06*

If you're using a standard M8 leadscrew, it's most likely a TR8*8, which actually has a pitch of 2mm and 4 starts, which all translates to, it's actually moving 8mm per turn, not 1.25.



The M8 leadscrew the site is referring to, is actually M8 threaded rod, not an 8mm lead screw.



At 8mm per turn, your steps per mm should be 25.



[http://wiki.thing-printer.com/index.php?title=Faq#Q:_My_steps_per_mm_are_wrong.2Fmy_printer_is_moving_way_too_far_per_mm.21](http://wiki.thing-printer.com/index.php?title=Faq#Q:_My_steps_per_mm_are_wrong.2Fmy_printer_is_moving_way_too_far_per_mm.21)



Has a deeper explanation


---
**Aditya Gandhi** *August 23, 2017 00:47*

Thanks this also helped a lot. * was the actual setting, I was hoping I have the 1.25 screw, but you were right I have the 2mm pitch.. 




---
*Imported from [Google+](https://plus.google.com/116302896773894703093/posts/joYvfsWxhSG) &mdash; content and formatting may not be reliable*
