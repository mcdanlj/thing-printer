---
layout: post
title: "Hello are all the fan pins connection on replicape 12v?"
date: November 13, 2016 09:38
category: "Discussion"
author: Step Cia
---
Hello are all the fan pins connection on replicape 12v? I'm referring to fan0,1,2 and3 the documentation says something about 12v but not very clear. I asked because I found cooling fans that I really like buy it's 5v





**Step Cia**

---
---
**Pieter Koorts** *November 13, 2016 10:00*

As far as I know all fans are 12v even with higher voltage power supplies. If they are ground switched you could connect the ground of the fans and then the positive wire to 5 volt else where.


---
**Elias Bakken** *November 13, 2016 10:59*

They are ground switched, yes!


---
**Step Cia** *November 13, 2016 16:35*

So just to be clear two wires coming out from the fan, one goes to ground of say Fan0 and the other wire goes to somewhere else with 5v on the replicape? Which oin for example can I connect to 5v? 



Would I still be able to control fan speed through slicer setting? Thx!


---
**Elias Bakken** *November 14, 2016 14:52*

Hi! You can use the 5V coming from either of the end stop connectors. To switch on and off, you can then use the ground terminal from the fan. It will not look pretty, but you should be able to make it work. Look at the connection diagram to figure out which pin does what on the board.


---
**Step Cia** *December 03, 2016 01:35*

Thanks that did it!


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/ZirvWjRgjJX) &mdash; content and formatting may not be reliable*
