---
layout: post
title: "Does 'shutdown -P now' actually power anybody's BBB off?"
date: September 23, 2017 21:38
category: "Discussion"
author: Chad Hinton
---
Does 'shutdown -P now' actually power anybody's BBB off?  I've left mine set for upwards of 15 minutes waiting for it to power down and it hasn't.  poweroff has the same lack of effect.  Is there something in  Umikaze that's blocking it?





**Chad Hinton**

---
---
**Benjamin Liedblad** *September 23, 2017 21:50*

Isn't the command 'shutdown -h now'?

"-h" being for "halt"...

I'm no guru, but never heard of "-P" option and would like to know what it does if it is legit.


---
**Dan Cook** *September 23, 2017 21:50*

'sudo shutdown' ? 😜


---
**Chad Hinton** *September 23, 2017 21:54*

from my BBB:

root@kamikaze:~# shutdown --help

shutdown [OPTIONS...] [TIME] [WALL...]



Shut down the system.



     --help      Show this help

  -H --halt      Halt the machine

  -P --poweroff  Power-off the machine

  -r --reboot    Reboot the machine

  -h             Equivalent to --poweroff, overridden by --halt

  -k             Don't halt/power-off/reboot, just send warnings

     --no-wall   Don't send wall message before halt/power-off/reboot

  -c             Cancel a pending shutdown

root@kamikaze:~# 



so -h and -P should be power down.  I've not tried -h.  I can't test right now as I've got a print going.


---
**Benjamin Liedblad** *September 23, 2017 22:00*

I'm afk, so can't peek. 

I'm not on the latest Kamikazi yet, but I know I use -h. I'm not sure if my entry in Octoprint's settings uses "sudo", but that is worth a try with your -P.


---
**Dan Cook** *September 23, 2017 22:16*

I just tried 'shutdown' ... works fine as root or sudo...


---
**Phil Turner** *September 23, 2017 23:29*

shutdown -H only halts the system, it doesn't actually power it down.


---
**Jon Charnas** *September 27, 2017 14:58*

**+Chad Hinton** it's curious that it's not shutting down at all. I've never had one of my BBBs refuse to shutdown, even the one hit by a bad short-circuit on the USB... How are you seeing the BBB still up? Console/SSH session still open, LEDs..?


---
**Chad Hinton** *September 28, 2017 00:30*

**+Jon Charnas** I believe I figured out what I was seeing.  I've reviewed the syslogs a couple times and think it is actually powering down the BBB but at least one power light is illuminated because of course it cannot physically shut off the power supply.  I just don't recall seeing that when I was using it as a weather station.




---
**Jon Charnas** *September 28, 2017 04:59*

Huh. Interesting. On mine it shuts off all LEDs on the BBB proper, only the power LED on the replicape stays on


---
*Imported from [Google+](https://plus.google.com/109766677766216016353/posts/UW4Nsn1zvF7) &mdash; content and formatting may not be reliable*
