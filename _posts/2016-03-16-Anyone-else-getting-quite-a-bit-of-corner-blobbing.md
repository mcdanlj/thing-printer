---
layout: post
title: "Anyone else getting quite a bit of corner-blobbing?"
date: March 16, 2016 03:06
category: "Discussion"
author: Ante Vukorepa
---
Anyone else getting quite a bit of corner-blobbing? I had to crank up the acceleration quite a bit and am still not completely rid of it. As a consequence, i had to increase the current. No overheating yet, but the drivers are nice and toasty (to a finger at least).



I know Redeem doesn't have the "Advance" algorithm (yet?), but i didn't have it on in Marlin either (it was broken for quite a while, no idea if they ever patched it up).



Not sure what gives, maybe my temps are too high. Using the correct table, but will double-check the temps with a thermocouple probe tomorrow.





**Ante Vukorepa**

---
---
**James Armstrong** *March 16, 2016 12:05*

Not yet. I posted a lot of info on an issue I opened on the bitbucket with comments and I formation from Dan Newman when they added the JKN advance algorithm to sailfish but it is all above my head. 


---
**Ante Vukorepa** *March 16, 2016 15:56*

Yeah, i've seen that issue. But even without Advance, it seems like there's way way too much blobbing, compared to Marlin or Repetier (also without Advance/JKN). It's almost like the planner isn't cornering quite correctly or something.


---
**Ante Vukorepa** *March 17, 2016 03:17*

**+Dan Ghiciulescu** It looks like this:

[https://dl.dropboxusercontent.com/u/1702513/Photo%2017-03-16%2004%2003%2029.jpg](https://dl.dropboxusercontent.com/u/1702513/Photo%2017-03-16%2004%2003%2029.jpg)

[https://dl.dropboxusercontent.com/u/1702513/Photo%2017-03-16%2004%2003%2015.jpg](https://dl.dropboxusercontent.com/u/1702513/Photo%2017-03-16%2004%2003%2015.jpg)

[https://dl.dropboxusercontent.com/u/1702513/Photo%2017-03-16%2004%2004%2026.jpg](https://dl.dropboxusercontent.com/u/1702513/Photo%2017-03-16%2004%2004%2026.jpg)




---
**Jon Charnas** *March 17, 2016 08:32*

**+Ante Vukorepa**, I've noticed some cornering difficulties as well, but not as severe as yours. Delta printer's light print head apparently helps there. Perhaps the backlash settings is what really needs looking into?


---
**Ante Vukorepa** *March 17, 2016 22:06*

**+Jon Charnas** It's not backlash, nor related to backlash. There's no measurable backlash on my machine, plus, if it were backlash, it wouldn't just be noticeable in the corners. There's a visible excess extrudate in the corners.



That's a typical symptom of the print head stopping briefly at the corners and is normal (without Advance), but what i'm noticing is way more pronounced than on Marlin.



My best guess is - the planner has a tiny stutter between the segments, causing the blobbing. It's not noticable on flat angles and rounded corners, because the head has enough inertia to continue moving "through" the stutter. However, on a right angle, sharp corner, there's a slight pronounced pause and blobbing.


---
**Ante Vukorepa** *March 17, 2016 22:08*

**+Dan Ghiciulescu** I can share the G-code if you want to take a look and use it for "dry" debugging, but you won't be able to do much with it on your printer, as our printers are not mechanically the same.



I've tried speeds ranging from 15-40 (or was it 50). There's not much change. Increasing acceleration and jerk do improve things, as expected, but i quickly start overheating the drivers and steppers that way. In short - i need to bump up the acceleration and jerk WAY way high for the issue to go away. Higher than i should be needing to.


---
**James Armstrong** *March 18, 2016 19:39*

It is. Redeem is responsible for calculating the actual steps and any jerk / accel / deaccel (JKN) advance while printing.


---
**Ante Vukorepa** *March 18, 2016 19:49*

Yes, but that has nothing to do with the issue at hand. G1 E1 on my machine does not necessarily produce the same effect as G1 E1 on your machine. What if i'm using 3mm filament and you're using 1.75? What if we both use nominal 3mm, but mine is 2.85 while yours is 2.95? What if i'm compensating in-slicer for underextrusion, and you're not? Or what if my retraction settings cause your extruder to skip?


---
**Ante Vukorepa** *March 18, 2016 19:50*

My generated g-code <b>might</b> work with your printer, but it also might not. It might produce a similar result, but might produce something completely different. Bottom line is - using someone else's G-code to compare print results isn't a really good idea :)


---
**Ante Vukorepa** *March 18, 2016 19:52*

On that subject, though, there used to be an initiative a few years ago of switching to using volumetric extruder commands instead of treating it like a 4th axis in G0/G1. The idea being that if slicers are always outputting desired extrusion volume, and the filament dia etc. are moved to firmware parameters, same G-code can (almost) always be guaranteed to work on any printer. Wonder what happened to that.


---
**Ante Vukorepa** *March 19, 2016 22:34*

The inward curling you seem to be getting on the corners (visible in some parts of the vid) is likely due to overextrusion in the corners. The warping from infill is likely lack of print cooling. The perimeter should be cold by the time infill is being laid down and should not pull on it.


---
*Imported from [Google+](https://plus.google.com/105486978634901788220/posts/4X19JB64hhQ) &mdash; content and formatting may not be reliable*
