---
layout: post
title: "Hi, I try to choose the stepper motors to my 3D printer, and I'm in this topic beginner"
date: April 23, 2016 10:04
category: "Discussion"
author: Daniel Kusz
---
Hi,

I try to choose the stepper motors to my 3D printer, and I'm in this topic beginner. Do they need to be on the 12V? From Replicape specifications I know that the current per phase is 1.2A and 12V. How to choose the optimal engines by current-voltage? Can I connect to Replicape engines 3V / 1.2A?



Regards





**Daniel Kusz**

---
---
**Javier Murcia** *April 26, 2016 18:12*

Usually, stepper motors have a low voltage rating (1-4v). That's the voltage needed to archive the nominal current. But since we are using stepper drivers (and the replicape has pretty good ones) the driver will modulate the current supplied to the stepper, so it stays under the programed limit, so you could supply your 3v motor with 12 or even 24v! (if the drivers supports it). 



See this for more info [http://reprap.org/wiki/NEMA_17_Stepper_motor](http://reprap.org/wiki/NEMA_17_Stepper_motor)


---
*Imported from [Google+](https://plus.google.com/103041373788524358316/posts/fx3NnkQRu4W) &mdash; content and formatting may not be reliable*
