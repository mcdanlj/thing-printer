---
layout: post
title: "Anyone know how if the BeagleBone black has it's own temperature monitor?"
date: February 20, 2016 21:00
category: "Discussion"
author: Veridico Cholo
---
Anyone know how if the BeagleBone black has it's own temperature monitor? I have found some stuff online but it doesn't work on my current board.





**Veridico Cholo**

---
---
**Andy Tomascak** *February 21, 2016 00:34*

What are you attempting to do? The BBB has no sensors but the Replicape allows for 3 thermistors.


---
**Veridico Cholo** *February 21, 2016 02:32*

The beaglebone black has a Texas Instruments SOC (System on a Chip) that runs the whole thing. Usually these chips have hardware that monitors the chip's temperature. There are ways to query this hardware to get this temperature.



This is totally unrelated to the Replicape itself.



I just wanted to see how hot was the CPU on the BBB getting while printing for 6 hours to see of the fan I have cooling the boards is sufficient or not. And this seemed like an easy and quick way to do it.


---
**James Armstrong** *February 21, 2016 03:18*

Not sure. The pi has a built in temperature sensor because my octoprint navtemp shows the pi temp. 


---
**Andy Tomascak** *February 21, 2016 03:23*

Found this, but haven't tried it: [http://inspire.logicsupply.com/2014/12/beaglebone-cpu-temperature-monitor.html](http://inspire.logicsupply.com/2014/12/beaglebone-cpu-temperature-monitor.html)


---
**Veridico Cholo** *February 21, 2016 04:17*

**+Andy Tomascak** Thanks for looking! I had found this same thing and tried it but it doesn't work because the file that contains the temp is not there:



CPU Temp [Celsius]: cat: /sys/class/hwmon/hwmon0/device/temp1_input: No such file or directory


---
**Veridico Cholo** *February 21, 2016 04:19*

**+James Armstrong** Exactly. I have Octoprint with navbar temp plugin on a Raspberry Pi  for another printer and it displays the Pi temperature as well. I was expecting the same for the BeagleBone Black after I installed the same plugin but it's not there.


---
**Andy Tomascak** *February 21, 2016 04:28*

Sounds like TI doesn't want end users using that sensor anymore. Ref: [http://stackoverflow.com/questions/28099822/reading-cpu-temperature-on-beaglebone-black](http://stackoverflow.com/questions/28099822/reading-cpu-temperature-on-beaglebone-black)


---
**Veridico Cholo** *February 21, 2016 04:38*

**+Andy Tomascak** I appreciate you digging further and clearing up the mystery!


---
*Imported from [Google+](https://plus.google.com/109713994627754483735/posts/W2VV29zyhPe) &mdash; content and formatting may not be reliable*
