---
layout: post
title: "Status update on Umikaze 2.1.2 Hi folks, just letting you know that no, I haven't forgotten you guys"
date: March 08, 2018 21:33
category: "Discussion"
author: Jon Charnas
---
Status update on Umikaze 2.1.2



Hi folks, just letting you know that no, I haven't forgotten you guys. I've been banging my head against the wall quite a bit lately. The linux kernel that was used in 2.1.1 is no longer available in the repositories, and the ones that are seem to have a bizarre way of handling the eMMC and SD device numbering. Meaning it can get tricky to make sure the BBB boots after it has been flashed!



Redeem has a new merge of code in the staging branch as of last week that includes the "print to SD" feature I've been trying to get out for a while. The 'in-place-upgrade' scripts also function near perfectly.



BUT. There's a significant issue with the latest image I built: the USB network connection doesn't work anymore.



So to recap: I have everything that I wanted working, but I'm missing a couple of critical features to be able to push the image out for testing and then release.



I hope you'll forgive me while I work on the update. If you're already running Umikaze 2.1.1 though, you can manually upgrade your Redeem to the latest staging branch code to take advantage of it! However it will not work for Kamikaze 2.1.0 or earlier - a few things have been re-written and require extra libraries to be installed onboard for Redeem to be able to compile the PRU firmware properly.



Thanks for your patience guys! It has never felt more like 2 steps forward 1 step back than right now.





**Jon Charnas**

---


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/1Fu7UHzn1Aw) &mdash; content and formatting may not be reliable*
