---
layout: post
title: "Hi, I got hit by a badly heating extruder stepper motor"
date: March 09, 2016 19:58
category: "Discussion"
author: Matti Airas
---
Hi,



I got hit by a badly heating extruder stepper motor. On the default current of 500 mA the temperature rose to over 90°C,  and even when reducing the current to 100 mA, the temperature remained high. Since I have to run the axis steppers in microstepping mode 8 to avoid noise, I might as well switch all of them to more compatible ones.



Question: which steppers should I get? My only specs would be NEMA 17, probably 1.8° per step. Should be suitably powerful to run a delta printer with decent speed and acceleration. Maybe we could compile a list of recommended steppers in the wiki?





**Matti Airas**

---
---
**Tim Curtis** *March 09, 2016 20:04*

I use these steppers on my printer. Plenty of power.



[http://openbuildspartstore.com/nema-17-stepper-motor/](http://openbuildspartstore.com/nema-17-stepper-motor/)


---
**Jon Charnas** *March 10, 2016 09:53*

Depending on where you are, I got my delta kit from [www.builda3dprinter.eu](http://www.builda3dprinter.eu) and his motors don't heat up at all. My drivers heat up more than the motors, running at .5 or even .7A on my reduced stepper for the extruder.


---
*Imported from [Google+](https://plus.google.com/106643081727071984016/posts/REMqRf7hP3e) &mdash; content and formatting may not be reliable*
