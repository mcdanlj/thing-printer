---
layout: post
title: "Hi everyone, I was just wondering if there's any way to know the optimum current for the z-axis of your printer?"
date: July 04, 2016 16:54
category: "Discussion"
author: Sinchita Siddiquee
---
Hi everyone, 



I was just wondering if there's any way to know the optimum current for the z-axis of your printer? Our printer seems to working fine with current of about 0.9 while the bed is moving downwards but it's really struggling on it's way up. Thanks





**Sinchita Siddiquee**

---
---
**Ryan Carlyle** *July 04, 2016 20:49*

What's the current rating of the motor? You would usually want to target about 80% of that, depending on the kind of mount the motor is on. 


---
*Imported from [Google+](https://plus.google.com/108772268407141748394/posts/VmNb9VhVDMh) &mdash; content and formatting may not be reliable*
