---
layout: post
title: "Hi all, I am a new member, and have been searching through past posts and information, reading documentation, and trying virtually anything I can think of or find"
date: February 19, 2018 09:22
category: "Discussion"
author: Chris Thompson
---
Hi all,



I am a new member, and have been searching through past posts and information, reading documentation, and trying virtually anything I can think of or find.



I just got a new Beaglebone Black Wireless, and have flashed it successfully as far as I can tell with Umikaze 2.1.1. It did its Night Rider thing, rebooted as I was supposed to and it ran its script and reset. 



I am running Windows 10 Pro x64.



I have been trying to connect to it through PuTTy using 192.168.7.2 and 192.168.6.2, and neither are working. Before I flashed it, I saw it pop up on my PC as a wireless connection, now I don't see that when I connect using USB. I don't have a microHDMI cable at the moment, so I can't connect it to a monitor until I get one. I also cannot seem to get it to show up as a USB Storage Device, so I can't try the Start.htm method. [http://kamikaze.local](http://kamikaze.local) does not work either. Tried for 2 and port 5000. I have a Replicape, but I am not at that point yet if I can't even SSH in, as that is how I plan to use this.



I have seen that perhaps there is not a proper build yet for BBBW, or maybe there is and I just have the wrong image flashed?



Any help here would be fantastic, as I am quite literally out of ideas at the moment.





**Chris Thompson**

---
---
**Chris Thompson** *February 19, 2018 09:26*

Oh a bit more info, I am having a driver issue as I have seen in another post. I am missing the CDC ECM and RNDIS drivers in my "Other devices" section of Device Manager. I have tried downloading the Acer version, and I have even tried using a driver finder tool that typically works great for other drivers. It does not work.




---
**Chris Thompson** *February 19, 2018 09:59*

Screenshot of my Device Manager

![images/5270bc8c2be5e78df587fb70fb6675cb.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/5270bc8c2be5e78df587fb70fb6675cb.png)


---
**Jon Charnas** *February 19, 2018 12:15*

Hi Chris, I would focus on getting the RNDIS interface working more than the CDC one. The BBB shows two network interfaces over USB, one is the RNDIS which is standard for Linux & Windows, and the other one is for Macs. The RNDIS should be a standard driver load, and I'm unclear why it's not working.


---
**Chris Thompson** *February 19, 2018 20:37*

I haven't been able to figure it out either. I have tried my laptop, and my PC. PC running windows 10 pro and laptop running windows 10 home. Both are displaying the same issue with the RNDIS driver.



The best I have come to find is that windows 10 has problems with RNDIS driver support. I don't know if that is still valid though. I can't seem to find a fix that works for it.




---
**Jon Charnas** *February 19, 2018 21:35*

agh! that's a general beaglebone/windows issue though, not Umikaze specific. We build Umikaze based on the normal  Beagleboard Ubuntu distros from RCN. If you google for normal BBB problems with network on windows any solution should apply here also. Sorry I can't help more, I'm rarely seeing this problem myself as I work mostly on ubuntu.


---
**Chris Thompson** *February 19, 2018 23:16*

That's okay. I appreciate you replying.



I got a microHDMI cable now, and I am trying to flash the new 2.1.2rc3 but it only gets as far as preparing to flash and then it freezes at "Tearing Down script environment" unmounts /tmp and stops the cylon LEDs. Then it just sits there. No issues with the 2.1.1 when flashing. Going to try rc2 maybe.


---
**Chris Thompson** *February 19, 2018 23:28*

[media.discordapp.net](https://media.discordapp.net/attachments/291982836196442112/415284736408420354/20180219_151305.jpg?width=683&height=910)


---
**Chris Thompson** *February 20, 2018 00:21*

I ended up kind of solving my problem. I reinstalled the 2.1.1 stable image, and I just rooted into the system and configured a new wireless connection on the device using nmtui and it works now. I can SSH through PuTTy and I can hit the [http://kamikaze.local](http://kamikaze.local)



Pain in the arse, but not umikaze fault. Thanks Microsoft. :P


---
**Jon Charnas** *February 20, 2018 03:06*

Yeah RC 3 has a known problem with the flash procedure of the underlying filesystem can't be read. I should really take that image down. 2.1.1 has worked flawlessly for most people do I'm really stumped by what could be the issue there


---
**Chris Thompson** *February 20, 2018 03:56*

It seems like RC3 is attempting to force a flash, instead of utilizing the onboard manual flash procedure. When I try to use any other distro and use the manual procedure it works great. 



Might chek to see if you can force RC3 back to manual flash. Don't know if it will fix it, but it seems to work best that way on other distros.


---
*Imported from [Google+](https://plus.google.com/106349563371745145727/posts/fgweVYJLZ4q) &mdash; content and formatting may not be reliable*
