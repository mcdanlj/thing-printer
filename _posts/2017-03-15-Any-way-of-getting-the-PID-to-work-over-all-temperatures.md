---
layout: post
title: "Any way of getting the PID to work over all temperatures?"
date: March 15, 2017 18:17
category: "Discussion"
author: Leo Södergren
---
Any way of getting the PID to work over all temperatures?  I have to run M303 for all new materials, and then I loose my precision for the old one. Also how do i get the M303 values to stick? I seem to loose the setting each time restarting redeem.





**Leo Södergren**

---
---
**Daniel Kusz** *March 15, 2017 18:27*

Use M500

[reprap.org - PID Tuning - RepRapWiki](http://reprap.org/wiki/PID_Tuning)


---
**Leo Södergren** *March 15, 2017 18:34*

**+Daniel Kusz** Is it correctly implimented in redeem? Here's from the wiki:

"M500: Store parameters to file

Save all changed parameters to file."


---
**Daniel Kusz** *March 15, 2017 18:37*

I think parameters are save in [local.cfg](http://local.cfg). I did it a few times. Write M500 after PID and check your [local.cfg](http://local.cfg). 


---
**Leo Södergren** *March 15, 2017 18:39*

**+Daniel Kusz** Okay thanks, I'll try after this print.


---
*Imported from [Google+](https://plus.google.com/107257105792966201860/posts/ZwaoJziarHz) &mdash; content and formatting may not be reliable*
