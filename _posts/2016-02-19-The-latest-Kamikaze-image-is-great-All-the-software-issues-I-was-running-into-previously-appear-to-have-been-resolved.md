---
layout: post
title: "The latest Kamikaze image is great! All the software issues I was running into previously appear to have been resolved"
date: February 19, 2016 03:28
category: "Discussion"
author: Andy Tomascak
---
The latest Kamikaze image is great! All the software issues I was running into previously appear to have been resolved. However, my steppers no longer move (or even make noise). I've double checked the endstops, and they are functioning correctly. I had the steppers moving correctly in the last image, and the wiring hasn't been modified, so I'm stumped at where I went wrong. Anyone have any ideas?





**Andy Tomascak**

---
---
**Elias Bakken** *February 19, 2016 03:29*

Hehe, just a slight problem then:) 


---
**Elias Bakken** *February 19, 2016 03:31*

Can you hear the steppers enable when you try to move? Also, check the log under "logs" in octoprint. 


---
**Andy Tomascak** *February 19, 2016 03:34*

No noise whatsoever. Logs don't show anything out of the ordinary. Maybe a short of some kind?


---
**Elias Bakken** *February 19, 2016 03:41*

Increase log level to debug (m111 s10) and see if you get an enable message when moving.


---
**Andy Tomascak** *February 19, 2016 04:33*

Nope, no additional messages.


---
**Andy Tomascak** *February 19, 2016 04:41*

After homing,  the effector position updates in OctoPrint as if it was correctly moving. The extruder and HBP both heat up fine. It like the steppers aren't getting power at all, they aren't energized and can be rotated still.


---
**Elias Bakken** *February 19, 2016 12:20*

I'll get right on this, not dilute what has happened!


---
**Elias Bakken** *February 19, 2016 14:35*

Hm... I just tried this now, and I had no problems with the steppers not moving... Just worked out of the box as expected. I have to ask, did you have this working with a previous image or is this the first time you are trying to move?


---
**Andy Tomascak** *February 19, 2016 14:55*

**+Flo Ri Han** Are you using a rev A or B cape? I'm working with a rev A.﻿ Although it sounds like you have power to your steppers which I don't think I'm getting.


---
**Elias Bakken** *February 19, 2016 14:56*

**+Andy Tomascak** That is interesting! What image did you use before?


---
**Andy Tomascak** *February 19, 2016 14:59*

Yesterday I was using the 2-16 image. I previously had the steppers working with the 1-22 image.


---
**Andy Tomascak** *February 19, 2016 18:01*

Even if I wire up the steppers incorrectly/differently, I'm not seeing any changes/errors in the logs. ???


---
**Elias Bakken** *February 19, 2016 18:04*

Andy, let me try to update a printer with rev A so I can see if I have the same problem!


---
**Andy Tomascak** *February 19, 2016 18:07*

**+Elias Bakken** Sounds good! It looks like it must either be a config issue, or at the hardware driver level. I don't know how else I could lose all of the steppers at the same time.


---
**Andy Tomascak** *February 19, 2016 18:08*

I'll post my logs on the IRC channel as well.


---
**Elias Bakken** *February 20, 2016 03:37*

**+Andy Tomascak**, it looks like the signature on rev A4A was not recognized, so the board EEPROM was updated to Rev B3 on mine. Did you have the cape installed on first boot after flashing? It's not a big deal, just update to Rev A4A if is wrong.  If you were able to connect, that was probably not it. 

My steppers are working good now. The alarm on the Rev A4A board is not implemented correctly I think, needs an update in the init sequence. 


---
**Andy Tomascak** *February 20, 2016 04:08*

**+Elias Bakken** That was indeed it! I don't know how the thing connected, but it was somehow partially working with the wrong drivers! I reinstalled the A4A drivers and now everything works like a charm! I feel like an idiot that I didn't find that earlier. :P



Thank you so much for finding that for me! I can't tell you how excited I am to be at the "fine-tuning" stage. :D Off to do a happy dance!



(You might want to put out a warning that the current image assumes a Rev B Replicape.)


---
**Elias Bakken** *February 20, 2016 04:13*

Haha! I will push out a new image tomorrow with a patch! We cannot have old capes overwritten! :)


---
*Imported from [Google+](https://plus.google.com/110080975040288455209/posts/Jbkg2DXX3Wp) &mdash; content and formatting may not be reliable*
