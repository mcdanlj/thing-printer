---
layout: post
title: "So here is the issue I am having now when heating the bed"
date: July 26, 2016 02:54
category: "Discussion"
author: James Brassill
---
So here is the issue I am having now when heating the bed. The bed will get up to temp then suddenly I have stepper faults and everything shuts down. It doesn't seem to matter if I am going to 35c or 100c, when it gets to temp everything crashes. Below is what I can get from the logs when this happens. I tried turning logging up to debug but the steppers just start vibrating when I turn everything on and I can't ever access Octoprint, but I can SSH in.



WARNING  Heater time update large: E temp: 30.265003748 time delta: 0.253293991089

WARNING  Heater time update large: H temp: 0.0 time delta: 0.253234863281

WARNING  Heater time update large: HBP temp: 32.9619144239 time delta: 0.505464792252

INFO     End Stop Z2 hit!

ERROR    Alarm: Stepper X

INFO     End Stop Y2 hit!

INFO     End Stop X2 hit!

INFO     End Stop Z1 hit!

ERROR    Alarm: Stepper E

ERROR    Alarm: Stepper 

ERROR    Alarm: Stepper Z

ERROR    Alarm: Stepper Y





**James Brassill**

---
---
**Jon Charnas** *July 26, 2016 07:12*

What current are you setting your steppers to run at? Are there any radiators to cool them in place? Typically stepper driver alarms are due to the drivers overheating. I have a small fan blowing air across the top of my cape with heatsinks on each driver chip.


---
**James Brassill** *July 26, 2016 07:16*

My current is set to 0.5 right now, but just in case I also set them to 0.1 and I get the same issue. I have a fan blowing across the steppers as well. The printer will print without the bed but that part seems to be the key to the issue. Could this be a bad power supply causing replicape to misinterpret the problem? It is a 500w single rail PC power supply but it might be bad. 


---
**Jon Charnas** *July 26, 2016 07:19*

That could be. If your bed is drawing too many amperes, the voltage to the steppers will drop. What PSU do you use?


---
**Elias Bakken** *July 26, 2016 10:09*

That is a weird issue! The "delta time" for E means that redeem was not able to get temperatures for 30 seconds. I've seen this happen during start-up sometimes, but only with ~1 second. And the steppers are working fine without the heated bed? Can you post a picture of your board with connections? 


---
**Jon Charnas** *July 26, 2016 10:16*

Elias, I seem to remember someone else had a similar delta time issue because they were using a D1W thermal sensor, no?


---
**James Brassill** *July 26, 2016 16:41*

I just realized I pulled the timing out of those log files so its impossible to see if the deltas are related to this issue. I will double check to see if they are related time wise. Without the heated bed I can print just fine for at least 30 minutes as thats the longest print I have had. I will grab a picture of the board this evening. And here is the PSU.



[http://www.frys.com/product/6893426](http://www.frys.com/product/6893426)



It powered the bed fine under RAMPs but I realize that likely has little bearing now.


---
**James Brassill** *July 26, 2016 18:39*

So I bought a MeanWell NES-350-12. Since my bed can't run 24v this should do fine until I build the D-Bot I want and end up needing a new bed.


---
**James Brassill** *July 28, 2016 18:22*

So the new PS runs the bed fine. I am not sure why the PSU did not have issues doing a bed PID tune, but caused a bunch of issues when reaching print temp regardless of actual set temp.


---
**Elias Bakken** *July 28, 2016 18:27*

**+James Brassill** that is interesting! I'd be  interested in seeing the data for the actual PID tuning and I've thought of implementing a publishing function for it similar to the bed leveling algorithm, but I have not gotten around to it yet. If you have changed PSU, I won't ask you for a screen dump though:)


---
**James Brassill** *July 28, 2016 19:20*

I should still have the logs from the old PSU and can dig through them and find that data for the PID. I can also run one on the new PSU. I am thinking maybe holding it at temp caused some problem in the PSU and I had a current spike that caused replicape to shut down to protect itself. At this point I am just spit balling though.


---
*Imported from [Google+](https://plus.google.com/+JamesBrassill/posts/UoaJAoSkgcp) &mdash; content and formatting may not be reliable*
