---
layout: post
title: "External stepper driver on Replicape:"
date: August 29, 2016 19:59
category: "Discussion"
author: Elias Bakken
---
External stepper driver on Replicape:



![images/13652e463124c498cbdffb0ee1a72d7e.gif](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/13652e463124c498cbdffb0ee1a72d7e.gif)



**Elias Bakken**

---
---
**Daniel Kusz** *August 29, 2016 20:22*

Great! I need two extruders and one extra stepper for some feature in future.


---
**JaeHyung Lee** *August 31, 2016 10:52*

Good. I have a question about micro stepping of this external driver. Is it 1/16 or 1/32, 1/128 ??? Actually I want to drive 1/128 micro stepping with external driver. 




---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/22v5MsVoXcD) &mdash; content and formatting may not be reliable*
