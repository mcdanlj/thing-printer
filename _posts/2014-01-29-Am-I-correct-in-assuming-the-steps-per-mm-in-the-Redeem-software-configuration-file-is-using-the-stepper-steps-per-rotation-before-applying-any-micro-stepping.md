---
layout: post
title: "Am I correct in assuming the steps per mm in the Redeem software configuration file is using the stepper steps per rotation before applying any micro stepping?"
date: January 29, 2014 17:43
category: "Discussion"
author: James Armstrong
---
Am I correct in assuming the steps per mm in the Redeem software configuration file is using the stepper steps per rotation before applying any micro stepping? The default values are a lot lower than I am use to seeing. We usually specify the steps per mm calculated after the micro stepping has been applied (200steps/mm * 1/16 micro steps = 3200 / 40 (20 tooth pulley) = 80 steps per mm where the cupcake config is showing 5. Just need to change my way of thinking (makerbot and repetier firmware) if so.





**James Armstrong**

---
---
**Elias Bakken** *January 29, 2014 17:58*

**+James Armstrong** yes, the steps_pr_mm settings are before any microstepping is applied. 


---
*Imported from [Google+](https://plus.google.com/+JamesArmstrong3/posts/guaosephmp9) &mdash; content and formatting may not be reliable*
