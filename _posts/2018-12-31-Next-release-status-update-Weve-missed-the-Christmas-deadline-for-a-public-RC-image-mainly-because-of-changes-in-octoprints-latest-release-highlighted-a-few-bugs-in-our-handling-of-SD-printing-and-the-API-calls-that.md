---
layout: post
title: "Next release status update: We've missed the Christmas deadline for a public RC image, mainly because of changes in octoprint's latest release highlighted a few bugs in our handling of SD printing, and the API calls that"
date: December 31, 2018 15:41
category: "Discussion"
author: Jon Charnas
---
Next release status update:



We've missed the Christmas deadline for a public RC image, mainly because of changes in octoprint's latest release highlighted a few bugs in our handling of SD printing, and the API calls that toggle made to fetch information on the printer status.



Redeem and octoprint have been tested and work reliably to the best of or developers printer pool testing. However the image should be functional in all aspects.



If you're not using toggle at all, then feel free to jump in the #development channel on slack and looking up the "boxing day" image created by Richard.



I will be in Oslo with **+Elias Bakken** for a week starting on the 3rd, and hopefully that will result in us hammering out an image for release. Not to mention more news about the revolve.



Happy New year and happy printing!





**Jon Charnas**

---


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/iQe8VGqsWc3) &mdash; content and formatting may not be reliable*
