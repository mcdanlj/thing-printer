---
layout: post
title: "Hi can bed mesh levelling be done manually like for instance on 3x3 grid can I use paper method on all 9 points and manually save the value?"
date: November 07, 2016 22:33
category: "Discussion"
author: Step Cia
---
Hi can bed mesh levelling be done manually like for instance on 3x3 grid can I use paper method on all 9 points and manually save the value?





**Step Cia**

---
---
**Jeremiah Coley** *November 08, 2016 15:05*

Yes, run bed leveling then m500 to save to eeprom or in Marlin from the endcoder save config.


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/FuDbp4WpUza) &mdash; content and formatting may not be reliable*
