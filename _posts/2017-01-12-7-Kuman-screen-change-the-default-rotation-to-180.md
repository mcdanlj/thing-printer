---
layout: post
title: "7\" Kuman screen. change the default rotation to 180"
date: January 12, 2017 18:46
category: "Discussion"
author: Step Cia
---
7" Kuman screen. change the default rotation to 180. plug and play.. I can move the box around and rotate it. upper right control and upper left temp view is un tappable ...



![images/370e4451f8ea0ca264788be40464152d.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/370e4451f8ea0ca264788be40464152d.jpeg)
![images/5ff5613c5c0478f0358b79c460ddc7ab.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/5ff5613c5c0478f0358b79c460ddc7ab.jpeg)

**Step Cia**

---
---
**Daniel Kusz** *January 14, 2017 12:44*

Toggle concept is great but now I think its only for monitoring temperatures. Rest is useless for me. Better solution is Printoid on some 7 inch or bigger tablet. It has  a lot of Octoprint features to use printer without PC. 


---
**matt chadsey** *January 23, 2017 16:43*

Was this plug and play? Or did you have to perform a config file exorcism...



I want to put a screen on, this might be a good placeholder for the manga rev2, but depending on the complexity I might wait...


---
**Step Cia** *January 23, 2017 16:54*

Sort of... 

I had to rotate the screen in local config of toggle



It probably is my setting but both manga screen and this one isn't responding to button presses for now..


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/cRbPwtqUVcc) &mdash; content and formatting may not be reliable*
