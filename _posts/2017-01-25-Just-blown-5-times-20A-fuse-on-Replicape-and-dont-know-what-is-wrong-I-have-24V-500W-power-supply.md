---
layout: post
title: "Just blown 5 times 20A fuse on Replicape and don't know what is wrong :/ I have 24V/500W power supply"
date: January 25, 2017 18:57
category: "Discussion"
author: Michal Tůma
---
Just blown 5 times 20A fuse on Replicape and don't know what is wrong :/ I have 24V/500W power supply. Last time i was testing if everything is ready on printer. Had heatbed (450W/24V) connected directly to replicape (together with 4x motors 1,7A run on 0,85A). For testing everything separately it was ok and i forgot to connect heatbed through SSR so i blow a fuse on replicape. Till today i was searching for new fuses. Last fuse was blown today when i had connected only power cables to replicape when i switched power on (nothing else connected to power supply or replicape). 





**Michal Tůma**

---
---
**Ryan Carlyle** *January 26, 2017 02:12*

If you're blowing fuses with nothing hooked up, you've probably fried and shorted something. Maybe the heatbed FET. 


---
**Michal Tůma** *January 26, 2017 08:04*

Original fuse on replicape was also 20A. I connected heatbed directly only for test of heatbed. When i switched only heatbed on, through replicape, it was working. Afterwards when i got SolidStateRelay i forgot to rewire it. During test i realized i need better heatbed (it took me more than 20mins to heat up to 100°C), something like 800W/220V or more will be better.



Nothing looks like that is fried. How can i check if something is shorted out?


---
**Dan Cook** *February 21, 2017 19:59*

You need an SSR or coil with that heated bed...


---
*Imported from [Google+](https://plus.google.com/116142004932985250467/posts/2Wjx1BYW4fj) &mdash; content and formatting may not be reliable*
