---
layout: post
title: "Why would by system all of a sudden become completely unreachable after getting a temperature change alarm when connecting (I'll admit hot plugging) a thermistor?"
date: December 29, 2016 23:39
category: "Discussion"
author: Henrik Söderholtz
---
Why would by system all of a sudden become completely unreachable after getting a temperature change alarm when connecting (I'll admit hot plugging) a thermistor? I have re-flashed but I am still unable to even ping the kamikaze.local over ethernet which was working fine just prior to this. Have I broken it? =(





**Henrik Söderholtz**

---
---
**Henrik Söderholtz** *December 29, 2016 23:41*

The heartbeat is there, though...


---
**Step Cia** *December 30, 2016 00:07*

I had to do more than 3 time flashing kamikaze to be able to finally make connection to display octoprint. I'd say try flashing again with different mehod perhaps? Etcher or win32? Burn new kamikaze?


---
**Henrik Söderholtz** *December 30, 2016 10:34*

Thanks Step. I refreshed a couple of times, but it all seems to have boiled down to network issues, unclear whether it was from the BBB or network or Mac... Thanks though!


---
*Imported from [Google+](https://plus.google.com/103752299402481876381/posts/Gr6CS3ngWqU) &mdash; content and formatting may not be reliable*
