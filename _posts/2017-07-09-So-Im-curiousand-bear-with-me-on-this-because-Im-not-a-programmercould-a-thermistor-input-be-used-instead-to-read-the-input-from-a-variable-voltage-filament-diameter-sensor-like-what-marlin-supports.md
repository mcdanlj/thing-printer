---
layout: post
title: "So I'm curious...and bear with me on this because I'm not a programmer...could a thermistor input be used instead to read the input from a variable voltage filament diameter sensor like what marlin supports?"
date: July 09, 2017 00:57
category: "Discussion"
author: seekerofthetruths
---
So I'm curious...and bear with me on this because I'm not a programmer...could a thermistor input be used instead to read the input from a variable voltage filament diameter sensor like what marlin supports? The sensor outputs a voltage equivalent of the filament diameter (1.75v = 1.75mm) I assume the hardware is capable with no problems but not sure what the limits would be as far as software.





**seekerofthetruths**

---
---
**Jon Charnas** *July 09, 2017 11:38*

so at the moment Redeem would probably just ignore that... But if you get hardware working and want to get "automagic" flow adjustment into Redeem, come talk on Slack in the #development channel :) (self-invite link on the thing-printer page)


---
**seekerofthetruths** *July 10, 2017 18:26*

I will definitely do that. I'm still waiting on some parts to arrive before I switch my printer over to the replicape. 


---
*Imported from [Google+](https://plus.google.com/111487005800269310962/posts/2KPw63JPXJ3) &mdash; content and formatting may not be reliable*
