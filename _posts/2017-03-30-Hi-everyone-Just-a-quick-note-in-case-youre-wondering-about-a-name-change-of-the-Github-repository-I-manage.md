---
layout: post
title: "Hi everyone, Just a quick note, in case you're wondering about a name change of the Github repository I manage"
date: March 30, 2017 11:59
category: "Discussion"
author: Jon Charnas
---
Hi everyone,



Just a quick note, in case you're wondering about a name change of the Github repository I manage. Last month, **+Elias Bakken** was suggesting that we keep the Kamikaze name for Debian IoT based images, as you can read here: [https://replicape.slackarchive.io/general/page-69/ts-1487613657007804](https://replicape.slackarchive.io/general/page-69/ts-1487613657007804)



The conclusion is that as the Kamikaze 2.1.0 currently released is already too well anchored in the mind of the community as it is, so it'll keep that name.



However starting with 2.1.1, I will try to push forth a pair of images simultaneously:

Kamikaze which will be built off a Debian Stretch base (github: [https://github.com/eliasbakken/Kamikaze2](https://github.com/eliasbakken/Kamikaze2)), and

Umikaze, built off an Ubuntu base (github: [https://github.com/goeland86/Umikaze2](https://github.com/goeland86/Umikaze2))



I hope this won't confuse too many of you, but essentially the idea is that both will have a similar end-user feature set with the same Redeem/Toggle/Octoprint versions and functionalities. But for the few Linux geeks around that like to play under the hood, the differences are there. I'm actually going to start working on isolating the distribution specific parts into separate scripts such that I can send a pull-request to the Kamikaze2 github to keep things in sync and properly attributed (see [https://github.com/goeland86/Umikaze2/issues/33](https://github.com/goeland86/Umikaze2/issues/33))



The original reason I was pushing to move away from Debian was a matter of the release cycle and upgrade process between versions. Specifically I was running into USB wifi driver issues on 2.0.8 and created 2.1.0 off Ubuntu to address that. However I recognize that Debian can also have an advantage over Ubuntu in other cases.



I'm hoping that the multiple versions becoming available will not cause confusion among the community, and that it is understood that both branches at present will be supported. The code generating the images will be kept as centralized as possible for both - however there are distinctions that can have an impact on the user. 



For instance regarding the Linux kernel versions, Debian has historically been slower to upgrade to newer package version. The direct impact there would be that Ubuntu is more likely to support a later-generation USB wifi dongle than Debian, or the newer BBB-Wireless platform. But the Debian procedures for hacking into the drivers and uBoot settings are better known and more consistent, offering other advantages there.



I still need to equip myself with extra BBB and BBB-Wireless cards to have enough "dev" machines to be able to work on both in parallel but this should happen shortly. Though any help is welcome on that end of course.



TL;DR summary:

Ubuntu based images renamed to Umikaze in the future, Kamikaze back to Debian. If you're likely to hack behind the hood to make things better, you'll know what this means. If you don't get it, it'll be a difference equivalent to "color" vs "colour" and you can pick either one to be able to print.





**Jon Charnas**

---
---
**Elias Bakken** *March 30, 2017 19:42*

Great update Jon! Kamikaze 2.2 (I guess it will be, since 2.1 is taken:) will come out when the official [BB.org](http://BB.org) Debian Stretch becomes stable. We can try to have a captive wiki page with links to both versions if you want! 


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/BJS7juLpKBf) &mdash; content and formatting may not be reliable*
