---
layout: post
title: "flashed the latest kamikaze image but can't log on, tried all the usernames and passwords that were suggested on the web page but nothing works not even root, any suggestions ?"
date: January 26, 2017 01:10
category: "Discussion"
author: Gary burke
---
flashed the latest kamikaze image but can't log on, tried all the usernames and passwords that were suggested on the web page but nothing works not even root, any suggestions ?.







**Gary burke**

---
---
**Dan Cook** *January 26, 2017 01:29*

Have you tried "kamikaze" as the password?


---
**Per Arne Kristiansen** *January 26, 2017 05:34*

Isnt the username ubuntu ? 


---
**Jon Charnas** *January 26, 2017 09:07*

Ok - need some help here, which version is "latest"? 2.0.8 or one of the 2.1.0 RC images?


---
**Jon Charnas** *January 26, 2017 09:08*

FYI I just released 2.1.0, and the only image with password problems was 2.1.0-RC3. That one you had to login with user ubuntu and password "temppwd", then run sudo su to set the root password.


---
**Gary burke** *January 26, 2017 19:17*

ok thanks it seems to work with ubuntu first time I have flashed it and have seen at least 3 different username/passwords for it but seems ok now thanks


---
*Imported from [Google+](https://plus.google.com/108707710390956734418/posts/1HkSMa6DcPM) &mdash; content and formatting may not be reliable*
