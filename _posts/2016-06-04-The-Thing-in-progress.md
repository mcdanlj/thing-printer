---
layout: post
title: "The Thing in progress... :-)"
date: June 04, 2016 19:30
category: "Discussion"
author: Daniel Kusz
---
The Thing in progress... :-) 

![images/2ba528cb4cf9d8252698b8132ca75d03.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/2ba528cb4cf9d8252698b8132ca75d03.jpeg)



**Daniel Kusz**

---
---
**Elias Bakken** *June 06, 2016 16:44*

Are you making it in aluminium?


---
**Daniel Kusz** *June 06, 2016 16:44*

Yes, 4mm.


---
*Imported from [Google+](https://plus.google.com/103041373788524358316/posts/8t3RMFQSFVd) &mdash; content and formatting may not be reliable*
