---
layout: post
title: "I am having a weird problem...I have no shutdown or reboot command from root!"
date: October 31, 2016 10:20
category: "Discussion"
author: Bob McDowell
---
I am having a weird problem...I have no shutdown or reboot command from root! Any ideas?

root@kamikaze:~# reboot

-bash: reboot: command not found

root@kamikaze:~# shutdown

-bash: shutdown: command not found







**Bob McDowell**

---
---
**Elias Bakken** *October 31, 2016 11:05*

Is this on Kamikaze 2.0.7? There have been a lot of problems with that version.


---
**Erika McDowell** *November 04, 2016 14:46*

No this was on 1.1.0


---
**Jon Charnas** *November 12, 2016 08:38*

It sounds like your system was corrupted, even on 1.1.0 I had those commands. Try doing an ls /sbin and ls /usr/sbin to see if the commands exist? If they do is a problem of your PATH variable being incomplete


---
**Bob McDowell** *November 16, 2016 15:17*

System was corrupted, reflash fixed! Thanks


---
*Imported from [Google+](https://plus.google.com/107803003115083427930/posts/EDiB9JJ5pxc) &mdash; content and formatting may not be reliable*
