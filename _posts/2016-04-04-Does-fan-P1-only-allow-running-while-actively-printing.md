---
layout: post
title: "Does fan P1 only allow running while actively printing?"
date: April 04, 2016 23:30
category: "Discussion"
author: Pieter Koorts
---
Does fan P1 only allow running while actively printing?



To test the fans I have been manually turning them on and off however when trying to turn on fan P1 with "M106 S255 P1" (so full speed), the fan only turns on briefly and then turns off again. If I repeat the command the same thing happens. Both fans P0 and P2 seem to allow manual control without any issues and stay running until I send the M107 command to each.





**Pieter Koorts**

---
---
**Elias Bakken** *April 05, 2016 00:31*

It's probably connected to a thermistor. You can configure that.


---
**Pieter Koorts** *April 05, 2016 07:32*

Ah that explains it. Thought it was just disobeying my orders. Still going through your Wiki, Elias.


---
**tizzledawg** *April 24, 2016 02:06*

Related to fans, in Cura you can set a cooling fan to enable at "X" layer height. Does redeem handle this and if so, which fan header does it send this PWM to?


---
**Elias Bakken** *April 24, 2016 02:14*

**+tizzledawg** Most likely Cura will just send an M106 with no params in that case. There is an option in Redeem to specify which fans to start in the case of a blank M106


---
**tizzledawg** *April 24, 2016 06:07*

Cheers. It looks like the default was fan0 so I assigned hotend thermistor  to fan1 instead and hooked up cooling fan to fan0. Works perfectly. Thanks :)


---
*Imported from [Google+](https://plus.google.com/100077479073911242630/posts/B5fc1Q5QiMH) &mdash; content and formatting may not be reliable*
