---
layout: post
title: "I'y trying to get my z-probe working right when I run G29"
date: October 28, 2016 20:53
category: "Discussion"
author: Matt F
---
I'y trying to get my z-probe working right when I run G29. My configuration is as follows:

Z1 - switch

Z2 - capacitive sensor



When I run G29 it seems to be using the Z1 input and not the Z2 input. Looking at the configuration I don't see where I would tell replicape to use Z2 (maybe modify the macro??) when probing only and use Z1 all other times.





**Matt F**

---
---
**Elias Bakken** *October 29, 2016 00:07*

Hi Matt! The best way to handle this is to use the G31/G32 macros. You can then enable and disable any end stop you want, for instance: 

[Macros]

g32 = 

	M574 Z2 x_cw,y_cw,z_cw

g31 = 

	M574 Z2

This will enable and disable the Z2 end stop for my Delta printer. 


---
**Matt F** *November 25, 2016 15:38*

G31/G32 works great. I have change my hot end and sensor holder. Before the sensor was to the right of the hot end and was close to the hot end, so I had the following:

[Probe]

length = 0.01

speed = 0.1

accel = 0.1

offset_x = 41

offset_y = 3

offset_z = 3.35



When I ran G29 it always seemed to use the hot end as the point of testing instead of the sensor, meaning that instead of the sensor being at 0,0 on the bed the hot end was, it would never move the x axis so the sensor was at 0,0. It seems that it would not take the offset setting for x,y. So I just modified the G29 macro to make things work. Now with the setup in picture, the sensor being off to the left of the hot end I tried setting the offsets to these:



offset_x = -36.50

offset_y = -19.44

offset_z = 3.17



When I run G29 it does not move the x axis over so that the sensor is at 0,0. It just starts to probe so the hot end hits the plate.



It still seems to ignore the offset setting for the x,y.  It should with the newer offset settings move the x axis over 36.50mm  then start probing the bed which it is not doing.



Am I doing this right?



Thanks

![images/bb238cdaa6c39806aa7e4cf3710f730a.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/bb238cdaa6c39806aa7e4cf3710f730a.jpeg)


---
*Imported from [Google+](https://plus.google.com/101706555333858781444/posts/6mcE2Ak3Qtr) &mdash; content and formatting may not be reliable*
