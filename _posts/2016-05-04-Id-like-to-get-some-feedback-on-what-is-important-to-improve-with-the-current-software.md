---
layout: post
title: "I'd like to get some feedback on what is important to improve with the current software"
date: May 04, 2016 22:45
category: "Discussion"
author: Elias Bakken
---
I'd like to get some feedback on what is important to improve with the current software. What should core developers focus on?





**Elias Bakken**

---
---
**Philip Acierto** *May 05, 2016 02:52*

I had problems bed leveling (Z probing) my CoreXY and was never able to print. The Replicape was shelved ever since, about a month or two now.



What I would consider "basic functionality" compared to the much more common Marlin would be expected, in my opinion, from this control board plus more, otherwise I question what am I "upgrading to" by using Replicape.



I realize it's possible the Z probe may have been patched by now and I could have, even at that time, made the changes to my own files to fix the problem, but as a new user to Replicape I wanted to be programming my own custom features, not fixing the "basic functionality" I had that with a 20 dollar ramps board.



So my .02: simpler usage should be a priority so new users don't get discouraged. Have it work and have it be easy to manage.


---
**Pieter Koorts** *May 05, 2016 10:15*

I am on the fence with this one. Firstly, the replicape hardware. Rev B is awesome and I really do like it and the idea of the BeagleBone driving it. This is one of the reasons I have stuck with it even though I am spamming the Google group with issues and questions (some being my own stupidity).



If I could vote for two I would say "simpler usage" and "better docs". The reason being that the way the settings are done is not to much of an issue but understanding clearly what all the settings do is important. Making settings management simpler although helps, would be minimal impact if the user of the replicape does not understand what all settings do and how they operate (including quirks maybe). I think this gets amplified a little for Delta users as they are notoriously a love/hate/calibration nightmare initially.



My vote would be priority on "simpler usage" and get to a good working feature stage like Marlin. Then as a lower priority but still in the loop, get documentation more detailed and clear. Documentation is also something the community can help with even if they don't fully understand the code behind the redeem software. 



Personally I have been noting down my endeavours with my Delta setup and will splurge it out to a wiki maintainer after working out my issues which should be soon as I am almost done. 



With good understandable documentation, simpler settings management could be shoved down the queue quite a bit since the profile uploading I find works fine for me.


---
**Tim Curtis** *May 05, 2016 16:49*

I second Philip Acierto comments.


---
**Daryl Bond** *May 05, 2016 21:55*

I would just like to make a comment that this is an open source project and it is partly the communities responsibility to improve the project. So when you think about <b>what</b> you want improved also think about <b>how</b> it should be improved. Then do something about it! If you aren't a programmer, now might be the time to learn! Or make your thoughts be known either here or in the software repository itself. The more people working on the replicape+redeem+toggle+... system the faster it will become what we want it to be.


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/4dkBbzysKRe) &mdash; content and formatting may not be reliable*
