---
layout: post
title: "Trying to get everything setup before wiring..."
date: March 26, 2016 23:17
category: "Discussion"
author: Jonathan Olesik
---
Trying to get everything setup before wiring...

When I try to do an update i get this error:

W: GPG error: [http://feeds.thing-printer.com](http://feeds.thing-printer.com) jessie InRelease: The following signatures couldn't be verified because the public key is not available: NO_PUBKEY 4C7E98C0315373BE

W: Size of file /var/lib/apt/lists/ftp.us.debian.org_debian_dists_jessie-updates_main_binary-armhf_Packages.gz is not what the server reported 4932 4936







**Jonathan Olesik**

---
---
**Elias Bakken** *March 26, 2016 23:31*

[http://feeds.thing-printer.com](http://feeds.thing-printer.com) is the old debian feed. The new is [http://kamikaze.thing-printer.com](http://kamikaze.thing-printer.com) 


---
**Jonathan Olesik** *March 26, 2016 23:41*

fixed!

"sudo wget -O - [http://feeds.thing-printer.com/apt/debian/conf/replicape.gpg.key](http://feeds.thing-printer.com/apt/debian/conf/replicape.gpg.key) | sudo apt-key add -"

worked for me




---
**Jonathan Olesik** *March 26, 2016 23:49*

hmm, I ran sudo apt-get upgrade first then apt-get update, how should I properly get my system up to date?


---
**Jon Charnas** *April 03, 2016 20:50*

update first <b>then</b> upgrade. It's counter-intuitive, but apt-get update only updates the catalog of software versions. The upgrade command is what installs them.


---
*Imported from [Google+](https://plus.google.com/+JonathanOlesik/posts/cw2CEzc2iPN) &mdash; content and formatting may not be reliable*
