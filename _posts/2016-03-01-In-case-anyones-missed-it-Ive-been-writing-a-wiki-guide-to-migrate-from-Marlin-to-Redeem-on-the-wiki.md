---
layout: post
title: "In case anyone's missed it, I've been writing a wiki guide to migrate from Marlin to Redeem on the wiki"
date: March 01, 2016 08:35
category: "Discussion"
author: Jon Charnas
---
In case anyone's missed it, I've been writing a wiki guide to migrate from Marlin to Redeem on the wiki. I think having this migration is going to be helpful to at least some of you now, and possibly to many more people buying the replicape in the future after they hit the limitations with Marlin (or their ears decide they want a quiet printer).



If you have any additions to make, please add them (or if you don't have a wiki account, pass them on to me, I'll be happy to add those for you). Same with comments or questions if you think something needs to be detailed a bit more, send them my way. (I'm Goeland86 on #replicape on freenode)





**Jon Charnas**

---
---
**Tim Curtis** *March 01, 2016 12:07*

Jon, excellent work so far on your page. I am finishing up converting my printer from marlin/ramps 1.4 to replicape. Thank you!


---
**Jon Charnas** *March 01, 2016 12:17*

Great to hear Tim! Let me know if there are some specifics that you think are missing.


---
**Step Cia** *March 01, 2016 16:11*

Bookmarked! Thanks I have 1.4 and very new to replicape and beaglebone that have no clue where to start


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/DofgGh9DzA8) &mdash; content and formatting may not be reliable*
