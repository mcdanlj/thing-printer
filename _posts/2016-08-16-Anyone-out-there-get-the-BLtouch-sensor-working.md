---
layout: post
title: "Anyone out there get the BLtouch sensor working?"
date: August 16, 2016 04:52
category: "Discussion"
author: Andrew Dowling
---
Anyone out there get the BLtouch sensor working? I will literally pay money to someone who will get a workaround for that bed leveling sensor. Elias, I will find a way to send you at least 2 of these sensors if you are willing to take on the project. Also, Ellias, why aren't you on Patreon? I would pay some small amount (5-10$/month, with things being more to the 10$ end if I could put in a request for like "make BLtouch work" or "push out a newer image for the SD cards" or "help me identify why printing below 1000mm/sec can sometimes cause my prints to freeze and become unresponsive") of money just for you to commit more time to the whole replicape / redeem / magenta thing. Anyways, keep up the good work. I think I am running the latest master branch and its working pretty good overall.





**Andrew Dowling**

---
---
**Dan Cook** *August 16, 2016 13:15*

I agree. I almost pulled the trigger on a BLtouch (sort of) assuming something as simple as bed leveling was already covered by the Replicape. Since I'm no software guru, I'm willing to throw in 5-10$/month as well if it can help.


---
**Andrew Dowling** *August 16, 2016 15:25*

only reason I mentioned the patreon thing is I noticed the octoprint guy was making 5000$ a month on it. I mean he might be living somewhere where that just barely pays the rent, but I am jealous be cause he is flat out making more money than me. Personally I would support the replicape/redeem more than octoprint (at east until they make octoprint update easily!) ELIAS!!! 20$ a month right there!!! AND THIS IS JUST THE TIP AND ONLY FOR A MOMENT!


---
**James Armstrong** *August 16, 2016 16:07*

I think snaffu over on the slack channel was working with the BLTouch and redeem and I thought he had it working. 


---
**Dan Cook** *August 16, 2016 19:39*

Andrew, I just checked out the Patreon site you mentioned. I imagine the site is getting a nice chunk of your profits, but it's leaving money on the table if you're doing any kind of ongoing video tutorials and not taking advantage of it. To me it makes more sense for free software such as Octoprint. If I had a product like the Replicate for example, I'd always keep my customers in the loop without charging them to watch my videos since they've already invested in my product.


---
**Andrew Dowling** *August 18, 2016 06:31*

Can I get a link to the working BLtouch? I hit the slack but couldn't find it. Still trying to figure out the purpose behind the whole slack thing, so I am not that good at searching it.


---
**Andy Tomascak** *August 29, 2016 01:56*

Hey Andrew, I'm working on this as well. I'll let you know if I make any progress.


---
**Matt Bivona** *September 04, 2016 01:47*

I have my BLtouch hooked up but cant even get it to recognize the commands. I have the 3 Pin servo Control hooked up to Y2 and the 2 Pin end stop connector hooked up to Z2. Would love to help get this working but cant even get the servo commands to work for it. 


---
*Imported from [Google+](https://plus.google.com/114125966923153934677/posts/YqEG5A5nLKM) &mdash; content and formatting may not be reliable*
