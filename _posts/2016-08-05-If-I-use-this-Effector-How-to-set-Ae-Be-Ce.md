---
layout: post
title: "If I use this Effector . How to set Ae, Be, Ce?"
date: August 05, 2016 19:31
category: "Discussion"
author: Mathias Giacomuzzi
---
If I use this Effector [http://www.thingiverse.com/thing:824932](http://www.thingiverse.com/thing:824932). How to set Ae, Be, Ce? Should I use the mid point and then in the slicer I use the x and y offset for booth of them?







**Mathias Giacomuzzi**

---
---
**Jon Charnas** *August 05, 2016 21:41*

That's probably what I would do, too, but I'm not interested in dual extrusion... yet.


---
**Mathias Giacomuzzi** *August 06, 2016 09:27*

Okay thanks. 




---
*Imported from [Google+](https://plus.google.com/104869072343043179738/posts/RXZN4YMtQgy) &mdash; content and formatting may not be reliable*
