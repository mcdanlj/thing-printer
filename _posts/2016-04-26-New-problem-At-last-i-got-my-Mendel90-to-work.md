---
layout: post
title: "New problem. At last i got my Mendel90 to work"
date: April 26, 2016 19:20
category: "Discussion"
author: Jan M. Simonsen
---
New problem.  At last i got my Mendel90 to work. I had a test print of the frog yesterday, all went nice. But when i should start a new print today i have notice when bed starting to warm up it stop heat after about 4 sec and then i can't get any warm back on ether the Hotend and Bed. The axis goes as normal. I haven't done any changes sens yesterday.





**Jan M. Simonsen**

---
---
**Elias Bakken** *April 26, 2016 19:52*

Perhaps an alarm has been triggered. Could you try M562 and see if that does anything with the heaters? You could also check the terminal log for any alarm info.


---
**Jan M. Simonsen** *April 26, 2016 19:59*

Ok will check it tomorrow ﻿


---
**Jan M. Simonsen** *April 28, 2016 20:38*

When i try to set heat on the bed  it goes 1 sec and power is cut to the bed and temp falls to 0 for 4sec and the same happening when i use the M562 command and terminal log says this "Send: M105

Recv: ok T:20 T1:0 T0:20 B:21 @:255.0

Recv: Alarm: Temperature falling too quickly (-21.4333) for HBP"


---
**Elias Bakken** *April 28, 2016 23:50*

What would make the temperature fall that quickly for the heat bed? Are you sure the thermistor is OK on the heat bed?


---
**Jan M. Simonsen** *April 30, 2016 08:49*

Yes it seams so. When I put my finger on the termistor it goes to rock bottom. :-(


---
**Elias Bakken** *April 30, 2016 11:12*

If the temperature falls quickly when you touch the thermistor, it sounds like you might have a broken wire, or am I misunderstanding you?


---
**Jan M. Simonsen** *April 30, 2016 11:13*

Yes. I have found the problem. Thanks for the tips. 


---
**Marcos Scriven** *May 26, 2016 09:07*

**+Jan M. Simonsen** - I have the same printer, any chance you could pass on your config please, would save me repeating your efforts.


---
**Jan M. Simonsen** *May 26, 2016 19:28*

My mendel90 config file

[https://www.dropbox.com/s/8lpj7uf5uxgqgky/Mendel90.cfg?dl=0](https://www.dropbox.com/s/8lpj7uf5uxgqgky/Mendel90.cfg?dl=0)


---
**Marcos Scriven** *May 26, 2016 20:58*

Just an aside **+Jan M. Simonsen** - Your z steps/mm is just 25. I have a Mendel90 and the z-axis is directly driving a 1mm M6 threaded rod, so for me its 200 steps/mm. Just curious how your Mendel90 is so different to mine?


---
**Jan M. Simonsen** *May 26, 2016 21:12*

i use these for my z axis

[http://www.ebay.com/itm/NEW-300mm-3D-Printer-8mm-Lead-Screw-Rod-Z-Axis-Linear-Rail-Bar-Nut-/331723399096?hash=item4d3c415fb8](http://www.ebay.com/itm/NEW-300mm-3D-Printer-8mm-Lead-Screw-Rod-Z-Axis-Linear-Rail-Bar-Nut-/331723399096?hash=item4d3c415fb8)


---
*Imported from [Google+](https://plus.google.com/114134914189900792065/posts/hfMvwh824qq) &mdash; content and formatting may not be reliable*
