---
layout: post
title: "ok silly question (but maybe the wiki could be more clear): How do you set the Z offset of the nozzle from the probe?"
date: July 03, 2016 20:37
category: "Discussion"
author: Philip Acierto
---
ok silly question (but maybe the wiki could be more clear): How do you set the Z offset of the nozzle from the probe? How does the X and Y offset affect the position of the nozzle? When setting a probe point, is that X/Y position supposed to be equal to the location of the nozzle or the location of the probe tip (which then I would assume the offset would be applied before a probe takes place).



When you probe, how ever you do it for your application, the height of the nozzle is usually different then probe. I see there is an X/Y offset on the probe section of the wiki (though that is not clear how that translates to the printer nozzle when a print starts), but I do not see any mention of Z height, nor does adding an "offset_z" seem to affect the Z axis after a probe.



I have just a little bit of an offset here :)



Speaking of offsets, This printer will be utilizing a dual print heads soon as this single extruder can print itself better replacement parts. How do you set print offsets for the dual heads E1 and E2 so they are also printing on one spot.

![images/e128b8aa3492c3cc93f320d588753397.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/e128b8aa3492c3cc93f320d588753397.jpeg)



**Philip Acierto**

---
---
**Step Cia** *July 03, 2016 20:49*

Hmm this is first time I've seen probe located quite a distance away from the hot end... 



I'm also curious to find out about your offset question


---
**Jon Charnas** *July 03, 2016 22:08*

normally you have values like x_probe_offset, which is the offset of the probe with the print head as a reference point... So if your probe is 10mm to the left of your nozzle, your line should read x_probe_offset=-0.01



At least that's what I remember it being supposed to do. I need to re-check that as I haven't had a need for a Z-probe in a while.


---
**Philip Acierto** *July 03, 2016 22:13*

in the default.cfg it shows this:



[Probe]

length = 0.01

speed = 0.05

accel = 0.1

offset_x = 0.0

offset_y = 0.0

offset_z = 0.0



The wiki shows this as well (sans offset_z) but doesn't really say what each setting does. 



I don't see any information on x_probe_offset, but i'll test and see if it does anything.



If i understand this correctly though, once I get a successful z probe, the nozzle when placed right above the bed would be read as a position of 0, right?


---
**Philip Acierto** *July 03, 2016 22:23*

Perhaps delta is the only one given offset values?



# Effector offset

Ae  = 0.026  

Be  = 0.026

Ce  = 0.026

# Carriage offset

A_radial = 0.0

B_radial = 0.0

C_radial = 0.0



Where is the Cartesian love? (coreXY) I know There are printing Cartesian printers, what did you guys do?!


---
**Jon Charnas** *July 04, 2016 07:55*

**+Philip Acierto** I'm not sure what the probe offset value name looks like - it may be just the offset_x in the [probe] section. Like I said, I haven't used this at all, since I fine tuned my delta manually.


---
**Philip Acierto** *July 04, 2016 07:58*

There is an offset_z user variable, but after I home Z, no matter how big positive or negative I set that value, the printer still reads the probes trigger point as z=0, meaning no offset was applied.


---
**Elias Bakken** *July 04, 2016 19:03*

If I remember correctly, the offset_x and offset_y is added to the location of the probe point, but the nozzle still follows the position in the G30 code. That is because an offset that compensated the actual nozzle on a delta would cause errors in the calculations. Offset_z does not have any effect in the current G29 and friends gcodes as far as I remember...


---
**Philip Acierto** *July 04, 2016 19:26*

**+Elias Bakken** ok I have some ideas for you, i'm on irc atm


---
**Philip Acierto** *July 05, 2016 06:06*

For anyone in the future that might find this, to set z offset:



G28 Z0          ; Home Z

G92 Z8          ; Compensate probe offset

G0 Z0             ; Move nozzle to the lowest position



1) What this is doing is finding the probed z point, which when finished sets the current position to z=0



2) We tell the printer that where you are isn't really 0, it is really z=8 (or w/e your offset is from the probe).



3) Then we tell the printer to put the nozzle to the print bed, where it is really Z height of zero.



Note: you may need to add other bits of gcode to the above, but long as you do at least those 3 steps, on a Cartesian printer, that will get your offset set. I do this at the end of my g29 macro, which is ran before every print.




---
*Imported from [Google+](https://plus.google.com/111974602640488998735/posts/EyKBUJwHjgh) &mdash; content and formatting may not be reliable*
