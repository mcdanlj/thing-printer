---
layout: post
title: "Hi all, I'm looking at a decent way of building a cht file for thermistors, since I've found that the default one doesn't seem to work for me for the version that comes with the E3D Lite6, and I'd also like to build one for"
date: March 08, 2016 13:47
category: "Discussion"
author: Jon Charnas
---
Hi all, I'm looking at a decent way of building a cht file for thermistors, since I've found that the default one doesn't seem to work for me for the version that comes with the E3D Lite6, and I'd also like to build one for the heatbed thermistor, which is a different one (Marlin #5 and #0 respectively).



I found this class on the redeem repository, but I'm wondering from where the Tk value should be set? T0, B and R0 I found in the thermistor's documentation, but Tk has me stumped.



Thanks for the help guys.





**Jon Charnas**

---
---
**Elias Bakken** *March 08, 2016 13:54*

Tk is the zero degrees Kelvin: [https://en.wikipedia.org/wiki/Absolute_zero](https://en.wikipedia.org/wiki/Absolute_zero)




---
**Jon Charnas** *March 08, 2016 13:57*

Thanks Elias. But I'm still having issues with the cht file - the thermistor documentation for 104GT-2 (link found in Marlin source here: [http://shop.arcol.hu/static/datasheets/thermistors.pdf](http://shop.arcol.hu/static/datasheets/thermistors.pdf)) shows values for every 10 degrees that don't line up with what the python script produces... Sometimes off by 20 ohms (at 300C)


---
**Elias Bakken** *March 08, 2016 14:00*

You could try the pending pull request from Parate, that eliminates the tables all together. I think he also uses the same formula though: [https://bitbucket.org/intelligentagent/redeem/pull-requests/41/rewrite-and-extension-of-temperature/diff](https://bitbucket.org/intelligentagent/redeem/pull-requests/41/rewrite-and-extension-of-temperature/diff)


---
**Jon Charnas** *March 08, 2016 14:06*

I'll try it tonight. But if it's the same function, it's going to be the same problem. I think I'd rather manually create the cht file by hand for the thermistor. But I want to understand what's mistaken in the formula so the fix will propagate to every other file we have, and be valid for any possible thermistor we can get documentation of the B value for.


---
**Elias Bakken** *March 08, 2016 14:13*

I'm not sure how the Marlin tables are created, but I think the formula I have is pretty standard. If there is a 20 ohm difference at 300 degrees, that is significant indeed (20%)!




---
**Jon Charnas** *March 08, 2016 14:18*

That's the problem I'm facing. I was trying to print with a reported indication of 220 degrees last night, but backpressure kept making the bowden tube slide back out of the fitting. I jumped the temp to 240C reported and that was back to what I used to be able to extrude with Marlin. The thing that bothers me is that the values built with the formula don't match with the values reported in the documentation for that thermistor... So clearly it's not matching the model all that well. I'll see if there's some indication I can look at. But the Marlin table is built using this: [https://github.com/MarlinFirmware/Marlin/blob/RC/Marlin/thermistortables.h](https://github.com/MarlinFirmware/Marlin/blob/RC/Marlin/thermistortables.h) (see line 173 for the 104GT-2 thermistor)


---
**Jon Charnas** *March 08, 2016 14:20*

By adding the 2% of error to the B value I was able to get somewhat closer (8 ohm difference from documented table at 300C), which might help somewhat. At 210C the difference looks to be about 3-4 ohms only, which would be acceptable.


---
**Jon Charnas** *March 08, 2016 16:35*

Yes sure but at the moment I thought they were at 1 degree intervals. At least the python snippet to generate then does that for every degree. 


---
**Jon Charnas** *March 08, 2016 16:36*

I'll try your code tonight. Doc of the thermistor is good for lots of temps up to 300C


---
**Jon Charnas** *March 08, 2016 18:43*

**+Florian** see the pdf link in my 2nd comment for the doc about the 104GT-2 thermistor documentation


---
**Jon Charnas** *March 08, 2016 19:41*

**+Florian** I can't seem to compile the path planner properly - isn't it just a simple python setup.py install to get redeem installed?


---
**Jon Charnas** *March 08, 2016 20:27*

Ah, maybe not the devel branch. But I've been having way too many weird things with Kamikaze the last week. I've backed up my local.cfg and I'm going to completely reflash kamikaze from scratch. I am tired of all those reboots whenever it feels like it.


---
**Jon Charnas** *March 08, 2016 22:44*

Ok figured out the reason I was so off. The local.cfg was specifying the wrong .cht file, and I was looking at the kosselmini.cfg. woops. :/


---
**Elias Bakken** *March 08, 2016 23:15*

Ai... Brainfart, it happens:)


---
**Jon Charnas** *March 09, 2016 07:27*

I still want to test **+Florian**​ 's branch, but as said on irc I am missing movement on my printer (x and y don't go beyond the -50/+50mm range).


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/ZGw6mzzRDH4) &mdash; content and formatting may not be reliable*
