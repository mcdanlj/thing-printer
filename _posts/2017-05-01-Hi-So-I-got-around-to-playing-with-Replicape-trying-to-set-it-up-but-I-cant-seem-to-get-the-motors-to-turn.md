---
layout: post
title: "Hi. So I got around to playing with Replicape, trying to set it up, but I can't seem to get the motors to turn"
date: May 01, 2017 13:25
category: "Discussion"
author: Matouš Vrba
---
Hi. So I got around to playing with Replicape, trying to set it up, but I can't seem to get the motors to turn. For example when I press Extrude in Octoprint, the motor just starts to make a noise and the current from PSU rises, so the driver is clearly doing something, but the motor does not turn. When I press Motors off the noise stops and current drops. It seems like no step signals are being generated. I even tried to reflash the image and try it without altering the default configs, but it didn't help.



I am using about two months old Replicape Rev B3A on a BeagleBone Green Wireless and the latest Kamikaze pre-release image (Umikaze 2.1.1-rc1). Thanks for any help, I am quite clueless here and without turning motors the pretty Replicape I have sitting on my desk is just useless :(





**Matouš Vrba**

---
---
**Jon Charnas** *May 01, 2017 13:45*

Can you double check your wiring with the diagram from the fritzing example on the thing-printer wiki? It's pretty common for steppers to mix up the wiring loops. If current' s rising something is happening, but if the two wire loops aren't connected properly the steppers won't move.


---
**Jon Charnas** *May 01, 2017 13:46*

Also, shouldn't be a problem, but be aware that we have heard very little of people running with the green edition of the beaglebone - we recommend the black instead, though on paper they should be interchangeable.


---
**Matouš Vrba** *May 01, 2017 14:11*

Yes, I tried even multiple steppers and switching the wires of the stepper in the connector, but when I switch them then there is not even any noise, so that is probably not the problem.

Also if you mean the wiring diagram on this page: [http://wiki.thing-printer.com/index.php?title=Replicape_rev_B](http://wiki.thing-printer.com/index.php?title=Replicape_rev_B), then it is not really apparent how the wires correspond to the coils inside the steppers from that image, so it is not very helpful in this case...


---
**Jon Charnas** *May 01, 2017 14:15*

**+Elias Bakken** any idea about what could be wrong there?



**+Matouš Vrba** maybe double check the drivers are well aligned with the traces? Despite everything there can sometimes be a few QA issues... Otherwise wait for Elias to get back to you because I can't really help you in this case - I build Umikaze, not the 'cape.


---
**Matouš Vrba** *May 01, 2017 15:29*

I checked the board, but visually I don't see any problems. I could check with an oscilloscope for the signals, but I don't know which traces to check and the chip is a bit too tiny for that... Anyways thanks for your response.


---
**Elias Bakken** *May 01, 2017 23:19*

The current rising is probably the steppers being enabled. The green wireless is probably the biggest unknown in this setup, so if you have an oscilloscope, it would be good to check that steps are coming out of the right pins. But it would be strange if none of the step pins were right...


---
**Matouš Vrba** *May 02, 2017 08:15*

Oh yeah, it did not occur to me that I could simply plug a wire into the female header and measure the pin signal directly, I was looking for some testpoints on the PCB :) I'll try that when I get home and I'll see then, thanks!


---
**Matouš Vrba** *May 03, 2017 19:18*

OK, I measured some STEP and DIR signals and they do not change and stay at their respective levels. Interestingly some of them are 0V and some 3V3. I also tried measuring the ENABLE signal to verify, but it does not correspond to the motor sound and higher current so I guess it is not a motor enable signal as I thought.

I also noticed that the "Fan on" option in Octoprint makes the Fan 0 LED light up, so that seems to be working.



So what now? What to check or try next... any ideas? I do not have access to a BBB or BBBW and it is quite hard to source here in ČR. So if the problem is indeed caused by incompatibility with BBGW then I am willing to try make it work and share my solutions, but I will probably need your help as I have no experience in working with PRUs - only microcontrollers or embedded Linux a bit.



Thanks!


---
**Elias Bakken** *May 03, 2017 19:24*

Could it be related to the pins being muxed right? If the Device tree overlay is not loaded, that might be the case. I couldn't find the pinout for the BBGW, but I cannot imagine that the PRU pins are that different. Could you check dmesg and see if the Replicape DTC is loaded?




---
**Jon Charnas** *May 03, 2017 19:24*

What current are you setting for your motors? Likely 0.5A is enough to get them spinning without any load on them... If they're under-powered you'd get in a state where they don't switch to the next step correctly...


---
**Matouš Vrba** *May 03, 2017 19:52*

**+Elias Bakken** Not sure what I'm looking for, but



$ dmesg | grep REPLICAP



gives



[    4.232714] bone_capemgr bone_capemgr: slot #0: 'Replicape 3D printer cape,0B3A,Intelligent Agen,BB-BONE-REPLICAP'

[    4.430519] bone_capemgr bone_capemgr: slot #0: dtbo 'BB-BONE-REPLICAP-0B3A.dtbo' loaded; overlay id #0



so I guess it is loaded. But I noticed that with the Replicape plugged in the builtin wifi stops working and dmesg output is filled with messages like this: [https://pastebin.com/eBQHZfLg](https://pastebin.com/eBQHZfLg), so maybe that has something to do with the problem.



**+Jon Charnas** I saw a warning in the wiki Redeem page to never run steppers over 0,5A without cooling, so I didn't really want to go over 0,5A, but I doubt this is the problem as there are no step signals being generated and the motor does not even make a "trying to turn, but skipping" sound... I have a few other CNCs so I know that sound :)


---
**Matouš Vrba** *May 03, 2017 20:07*

I found BBGW pinout on this page: [http://wiki.seeed.cc/BeagleBone_Green_Wireless/](http://wiki.seeed.cc/BeagleBone_Green_Wireless/) (link to picture: [https://github.com/SeeedDocument/BeagleBone_Green_Wireless/blob/master/images/BeagleBoneGreenWirelessPins.jpg?raw=true](https://github.com/SeeedDocument/BeagleBone_Green_Wireless/blob/master/images/BeagleBoneGreenWirelessPins.jpg?raw=true)), but it seems to use different GPIO numbering than in the BBB documentation I found - the BBB doc has GPIOX_XX, but BBGW uses GPIO_XX... So I can't really tell if they match or not. The special functions (like "ehrpwm2B", "AIN1", etc.) seem to match though.



EDIT: Here is a more informative PDF with schematics and pinout with the same numbering as BBB uses: [https://github.com/SeeedDocument/BeagleBone_Green_Wireless/blob/master/resources/BeagleBone_Green%20Wireless_V1.0_SCH_20160314.pdf](https://github.com/SeeedDocument/BeagleBone_Green_Wireless/blob/master/resources/BeagleBone_Green%20Wireless_V1.0_SCH_20160314.pdf)

The pins match as far as I can tell... but there is "Caution: Used on board" near some of that pins - I don't know what that means.



EDIT2: So I found in the schematic that the pin GPIO1_12, which is assigned to STEP_E is used on the BBGW for communication with the WiFi chip... So I guess that's it. Unless I'm going to desolder some tiny SMD parts off the board it will probably not work :(



If you have any ideas please let me know, but I'm afraid that it is simply not compatible with Replicape. What pisses me off though is that I had to dig into the schematics to find out that these pins are used for something else... otherwise the board is advertised as "fully compatible" with the BBB!


---
**Matouš Vrba** *May 05, 2017 18:23*

OK, I have managed to disable the WiFi after blacklisting all wifi related modules and renaming all wifi related overlays, but now I am getting this error: [https://pastebin.com/pk9HG3G5.](https://pastebin.com/pk9HG3G5.).. I don't think I will be able to disable the MMC as the BB boots from it, right?


---
**Matouš Vrba** *May 18, 2017 20:19*

So I think I can conclude that the BBGW is certainly not compatible with Replicape (and probably many other capes). But I have exchanged it with my friend for a BBG and now it is working like a charm! I must say that the motors really spin silently as hell on the microstepping set to 7 in Octoprint! Anyways I will link this thread here for reference (there is a list of pins which are occupied on the headers): [https://groups.google.com/forum/#!category-topic/beaglebone/seeedstudio-beaglebone-green-wireless/xYwrzSEXs_U](https://groups.google.com/forum/#!category-topic/beaglebone/seeedstudio-beaglebone-green-wireless/xYwrzSEXs_U)


---
**Uday Kumar Adusumilli** *January 17, 2018 18:31*

Hi Matouš Vrba.. I am trying out Replicape B3 on a BBG board but not able to start redeem on it owing to some serial interface errors. Can you please tell me of any modifications that you made for yours to bring to working condition




---
**Matouš Vrba** *January 18, 2018 18:43*

Hmm its been some time and I didn't really have time to play with Replicape since then because of school... So unfortunately I no longer remember if I had to change anything, but I believe that when I switched from BBGW to BBG, it was working OK. I was using the Kamikaze image (I think version 2.0.8 or what was current at that time), so maybe if you are using something different or a newer version, then that could be the problem.


---
*Imported from [Google+](https://plus.google.com/114713703406364073556/posts/PFuLCCMfkDc) &mdash; content and formatting may not be reliable*
