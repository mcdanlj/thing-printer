---
layout: post
title: "Hi guys, Looking forward to the revolve coming out, great work"
date: January 29, 2019 23:28
category: "Discussion"
author: Dara Ó hEidhin
---
Hi guys,

Looking forward to the revolve coming out, great work. With Redeem does anyone know if it supports pressure/linear advance? Having played with linear advance and Klipper I noticed great results when configured.

Cheers

D





**Dara Ó hEidhin**

---
---
**Jon Charnas** *January 29, 2019 23:39*

Hi Dara, yes, there has been work to setup linear advance on redeem. It hasn't yet made it to the main code base because of firefighting other more urgent issues. However by the time the revolve becomes available I believe it will be ready for beta testing.


---
*Imported from [Google+](https://plus.google.com/112580875076752096166/posts/ftC5jumvPFj) &mdash; content and formatting may not be reliable*
