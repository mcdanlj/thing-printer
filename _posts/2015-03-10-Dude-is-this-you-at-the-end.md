---
layout: post
title: "Dude is this you at the end???"
date: March 10, 2015 20:14
category: "Discussion"
author: Jimmy Edwards
---
[http://www.ti.com/lit/wp/spry264/spry264.pdf](http://www.ti.com/lit/wp/spry264/spry264.pdf)

Dude is this you at the end??? How cool is that!!!!





**Jimmy Edwards**

---
---
**Jimmy Edwards** *March 10, 2015 21:53*

OK I get it, this looks like a phishing attempt.

The link goes to a Ti white paper about PRU's and real time response in a .pdf that has a picture of Hipster Bot in it.  It's super cool.


---
**Elias Bakken** *March 11, 2015 09:13*

Haha, awesome! :)


---
*Imported from [Google+](https://plus.google.com/+JamesEdwards780/posts/e3UDpgUrwvB) &mdash; content and formatting may not be reliable*
