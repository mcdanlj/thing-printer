---
layout: post
title: "A few hours later, and I'm still missing extensions for the z stepper motor and the z belt is making life difficult for me"
date: February 22, 2016 22:08
category: "Discussion"
author: Jon Charnas
---
A few hours later, and I'm still missing extensions for the z stepper motor and the z belt is making life difficult for me. But otherwise, it cleaned up nicely...

![images/eaffcebd99943ca4b2173c2450fb5257.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/eaffcebd99943ca4b2173c2450fb5257.jpeg)



**Jon Charnas**

---
---
**Jimmy Edwards** *February 22, 2016 23:42*

Looking Good


---
**Daryl Bond** *February 23, 2016 00:19*

Much nicer! I designed my delta to have all the electronics in the base under the heated bed. Unfortunately this does not make accessing the BBB+replicape easy as I have to re-level the bed whenever I do have to change something. So your placement is much better!


---
**Jon Charnas** *February 23, 2016 06:51*

One reason I didn't want mine under the bed is also because of ventilation, being at the top the 'cape can dissipate heat better. I'll upload the stl files for the holder when I'm at the computer again. Also I'll put up a link for the spool holder I've printed that fits over the 'cape. The only thing I'm not entirely satisfied with is the extruder placement, the Bowden tube has a tendency to get in the way of one of the towers.﻿


---
**Jon Charnas** *February 23, 2016 07:52*

Aaaand the files are not synced on my dropbox. Of course, would've been too easy. <b>sigh</b> Anyways, the spool holder I'm using is this guy: [http://www.thingiverse.com/thing:454808](http://www.thingiverse.com/thing:454808) but I need to upload the extended supports I made, so the brackets can fit next to where the replicape holder's attached on the top frame. (you can edit the stl yourself in Blender and just stretch it a good 15mm or so too)


---
**Jon Charnas** *February 23, 2016 21:33*

Aaand finally here are the files for the replicape holder. Note that these will be printable on a kossel mini, which Elias' original design wasn't ;) [https://www.youmagine.com/designs/minikossel-replicape-holder](https://www.youmagine.com/designs/minikossel-replicape-holder)


---
**Jon Charnas** *March 01, 2016 13:18*

Finally the complete file designs I use on my Kossel mIni w/ Replicape can be found in only one place: [http://www.thingiverse.com/goeland86/collections/kossel-mini-e3d-v6-parts](http://www.thingiverse.com/goeland86/collections/kossel-mini-e3d-v6-parts)


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/Bnaon4m4bDV) &mdash; content and formatting may not be reliable*
