---
layout: post
title: "Getting the Titan from E3D. Strange they don't post the step per mm"
date: October 03, 2016 07:52
category: "Discussion"
author: Step Cia
---
Getting the Titan from E3D. Strange they don't post the step per mm. is anyone using this extruder what should I input the step per mm for 1.8deg nema 17? thx





**Step Cia**

---
---
**Pieter Koorts** *October 03, 2016 12:36*

May be worth posting this one to the 3D printing community. Quite a few titan users in there. 


---
**Chris Romey** *October 03, 2016 15:40*

Steps are roughly 3x what a normal direct drive is since it's 3:1 reduction.


---
**Chris Romey** *October 03, 2016 17:38*

Here are my e steps for what it's worth:



steps_pr_mm_e = 25.60




---
**Step Cia** *October 03, 2016 19:18*

Did you start from (200*3)/(7.3*π)?


---
**Elias Bakken** *October 14, 2016 15:24*

Love the Titan, just ordered three more! 


---
**Step Cia** *October 19, 2016 19:02*

Will you be doing 3 MISO? That would be cool. Can redeem do this?


---
**Chris Romey** *October 19, 2016 20:17*

**+Step Cia** I didn't use any formulas.  Simply ballparked 3x direct drive steps then fine tuned until requesting 100mm of filament was accurate.


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/bT6fDapY8Xa) &mdash; content and formatting may not be reliable*
