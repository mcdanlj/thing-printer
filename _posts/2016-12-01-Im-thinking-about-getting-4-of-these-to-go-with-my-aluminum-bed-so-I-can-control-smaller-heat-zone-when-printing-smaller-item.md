---
layout: post
title: "I'm thinking about getting 4 of these to go with my aluminum bed so I can control smaller heat zone when printing smaller item"
date: December 01, 2016 21:22
category: "Discussion"
author: Step Cia
---
I'm thinking about getting 4 of these to go with my aluminum bed so I can control smaller heat zone when printing smaller item. question is, is replicape or redeem capable of handling 4 different heatbed input?





**Step Cia**

---
---
**Simon-Gabriel Gervais** *December 02, 2016 04:43*

my guess is that you can use fans outputs to trigger a relay to switch each heat bed from its paralle grid.hope it make sense to you




---
**Jon Charnas** *December 03, 2016 16:32*

I don't know that this is going to be that easy... You're basically needing to add a whole lot of inputs - remember that each area needs its own PID loop for temperature control to stay at the targeted temp! Otherwise you're going to have your thermistor at the center of all 4 mini HB meaning you'll get 4 extra-hot spots at the center of each of those beds...



The replicape as it stands does not have enough thermistor inputs to do this. You'd be better off wiring in an arduino or something that has the thermistor/relays for power management of each area separately that then receives targeted temp from the replicape over SPI or some other port... But that's going to involve lots of code hacking in redeem and wiring on your end, plus the extra arduino/whatever controller managing the HBs.


---
**Step Cia** *December 03, 2016 16:38*

Thx all it sounds like a lot of work to add these plates... I'll save it for furture project :)


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/88nZyrTcG1C) &mdash; content and formatting may not be reliable*
