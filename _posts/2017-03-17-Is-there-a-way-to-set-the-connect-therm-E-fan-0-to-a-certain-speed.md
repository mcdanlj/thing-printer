---
layout: post
title: "Is there a way to set the connect-therm-E-fan-0 to a certain speed?"
date: March 17, 2017 19:03
category: "Discussion"
author: Eric Trombly
---
Is there a way to set the connect-therm-E-fan-0 to a certain speed? I only need the fan to blow about 50%. Also related to that when I slow the speed on the fan I get a pwm whine, is there any way to change the frequency of the fan pwm to get rid of it?





**Eric Trombly**

---
---
**Dan Cook** *March 17, 2017 20:36*

What kind of hot end are you using?


---
**Mark Walker** *March 17, 2017 20:45*

M141 P0 F1000 S0.5



P is the fan number

F is the PWM frequency (I think 1,000 Hz is the default maybe?)

S is the on-time from 0..1


---
**Eric Trombly** *March 18, 2017 03:43*

I'm using an e3d v6, but swapped out the stock fan for a blower fan. It has a lot more air flow, that's why I'm trying to slow it down. Mark, 141 is for heated enclosure, 106 is for fans. Thanks for pointing out the frequency parameter though, I hadn't noticed it. Tried several different values, and it doesn't sound like they are having any effect.


---
**Mark Walker** *March 18, 2017 04:57*

Yes M141 on the g-code wiki page says heated enclosure, but that's not what Redeem's source code says: 



[bitbucket.org - intelligentagent / Redeem 
  / source  / redeem / gcodes / M141.py
 — Bitbucket](https://bitbucket.org/intelligentagent/redeem/src/0dc7929320eec5f4573579aab4fb31ddaab8bbbd/redeem/gcodes/M141.py?at=master&fileviewer=file-view-default)


---
**Mark Walker** *March 18, 2017 04:58*

And M106 doesn't take a frequency parameter. [https://bitbucket.org/intelligentagent/redeem/src/0dc7929320eec5f4573579aab4fb31ddaab8bbbd/redeem/gcodes/M106_M107.py?at=master&fileviewer=file-view-default](https://bitbucket.org/intelligentagent/redeem/src/0dc7929320eec5f4573579aab4fb31ddaab8bbbd/redeem/gcodes/M106_M107.py?at=master&fileviewer=file-view-default)


---
**Eric Trombly** *March 18, 2017 16:37*

Thanks for showing me where the code was, it helped a ton. I tried out M141, but that wasn't working either. Turns out set_PWM_frequency() isn't implemented in Fan.py. I ended up just setting PWM.set_frequency(100) in the fan init. I also hard coded the fan to go on at half speed in Cooler.py, but it would be nice to have a way to modify it in the config.


---
*Imported from [Google+](https://plus.google.com/101455866316142068094/posts/8A4xS45J1Y1) &mdash; content and formatting may not be reliable*
