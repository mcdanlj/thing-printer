---
layout: post
title: "Is there any easy way to link the acceleration of the head(relative the bed) to the acceleration of the extruder"
date: November 15, 2016 15:44
category: "Discussion"
author: Leo Södergren
---
Is there any easy way to link the acceleration of the head(relative the bed) to the acceleration of the extruder. 



It would be the same as linking the extrusion-rate to the movement of the head.





**Leo Södergren**

---
---
**Step Cia** *November 15, 2016 16:24*

Isn't that automatically calculated already? The extrusion rate should match either the slowest or fastest movement of any motor. This is my assumption btw but otherwise without it, things could go wrong very easily 


---
**Leo Södergren** *November 15, 2016 16:27*

I don't think it is. Try raising the extrusion acceleration and yo'u'll see over-extrusion on both ends of the movement.


---
**Jon Charnas** *November 22, 2016 15:36*

There are different algorithms that manage this, and there's active discussion on the subject on the development channel on slack


---
**Leo Södergren** *December 03, 2016 14:54*

**+Frederic M.**  It was a long time ago, but i think i was running 0.01 on everything. Now i've moved on to lower acceleration for the extruder.



A quick calculation says 0.2mm layer height and 0.5mm layer width means the extruder will move 3-4% of the other axies, so try having the extruder acceleration around 5-10% of the other accelerations and see if your results improve.




---
**Leo Södergren** *December 04, 2016 12:36*

**+Frederic M.** Yeah got the same results this morning. Might be (in my case) a slight over extrusion

 


---
*Imported from [Google+](https://plus.google.com/107257105792966201860/posts/Nmy6J8PV9hd) &mdash; content and formatting may not be reliable*
