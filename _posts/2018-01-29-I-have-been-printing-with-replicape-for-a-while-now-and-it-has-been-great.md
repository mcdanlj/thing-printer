---
layout: post
title: "I have been printing with replicape for a while now and it has been great"
date: January 29, 2018 18:08
category: "Discussion"
author: William Burton
---
I have been printing with replicape for a while now and it has been great.  However last week it stopped working. I thought it might be a power supply issue, but swapped that out and the beaglebone black but still have the same issue. It can not find the replicape. Like would happen if no power was connected to it.  When I turn it on the stepper motors jitter like always and I checked and there is 5 volts to the end stops but replicape is never found by octoprint. I have even re installed the operating system. But still same result. I have already ordered a new replicape but was wondering if there is something I might be able to do to fix this one. 





**William Burton**

---
---
**Jon Charnas** *January 29, 2018 18:30*

There may be, but... What makes you say it can not find the replicape? Is it an i2c error in the Kamikaze or redeem logs?


---
**Jon Charnas** *January 29, 2018 18:31*

Does the stepper jittering ever stop?


---
**William Burton** *January 29, 2018 18:37*

So the jittering  never stops.  I'll have to look at the logs when I get home. I was thinking it didn't even get to the point where it would write anything in those logs though so I didn't think to check. 


---
**William Burton** *January 29, 2018 18:40*

The error is octoprint says it failed to auto detect the serial port please manually select it or something to that effect


---
**Jon Charnas** *January 29, 2018 19:24*

Right that means redeem doesn't start to create the port. Can you look at the output of "systemctl status redeem -l -n 1000"? From an SSH prompt


---
**William Burton** *January 29, 2018 21:11*

Thanks. I will try that when I get home and let you know


---
**William Burton** *January 30, 2018 01:24*

Ok this is the results of the command



● redeem.service - The Replicape Dameon

   Loaded: loaded (/lib/systemd/system/redeem.service; enabled; vendor preset: enabled)

   Active: failed (Result: exit-code) since Mon 2018-01-29 03:35:23 UTC; 21h ago

  Process: 455 ExecStart=/usr/bin/redeem (code=exited, status=1/FAILURE)

 Main PID: 455 (code=exited, status=1/FAILURE)



Jan 29 03:35:03 kamikaze systemd[1]: Started The Replicape Dameon.

Jan 29 03:35:20 kamikaze python[455]: libadafruit-bbio version <unknown> initialized

Jan 29 03:35:22 kamikaze redeem[455]: 01-29 03:35 root         INFO     Redeem initializing 1.2.2~Predator

Jan 29 03:35:22 kamikaze redeem[455]: 01-29 03:35 root         INFO     Using config file /etc/redeem/default.cfg

Jan 29 03:35:23 kamikaze redeem[455]: 01-29 03:35 root         INFO     Using config file /etc/redeem/maxcorexy.cfg

Jan 29 03:35:23 kamikaze redeem[455]: 01-29 03:35 root         INFO     Using config file /etc/redeem/local.cfg

Jan 29 03:35:23 kamikaze redeem[455]: Traceback (most recent call last):

Jan 29 03:35:23 kamikaze redeem[455]:   File "/usr/bin/redeem", line 9, in <module>

Jan 29 03:35:23 kamikaze redeem[455]:     load_entry_point('Redeem==1.2.2', 'console_scripts', 'redeem')()

Jan 29 03:35:23 kamikaze redeem[455]:   File "/usr/lib/python2.7/dist-packages/redeem/Redeem.py", line 650, in main

Jan 29 03:35:23 kamikaze redeem[455]:     r = Redeem(config_location)

Jan 29 03:35:23 kamikaze redeem[455]:   File "/usr/lib/python2.7/dist-packages/redeem/Redeem.py", line 109, in <i>_init_</i>

Jan 29 03:35:23 kamikaze redeem[455]:     os.path.join(config_location,'local.cfg')])

Jan 29 03:35:23 kamikaze redeem[455]:   File "/usr/lib/python2.7/dist-packages/redeem/CascadingConfigParser.py", line 43, in <i>_init_</i>

Jan 29 03:35:23 kamikaze redeem[455]:     self.readfp(open(config_file))

Jan 29 03:35:23 kamikaze redeem[455]:   File "/usr/lib/python2.7/ConfigParser.py", line 324, in readfp

Jan 29 03:35:23 kamikaze redeem[455]:     self._read(fp, filename)

Jan 29 03:35:23 kamikaze redeem[455]:   File "/usr/lib/python2.7/ConfigParser.py", line 546, in _read

Jan 29 03:35:23 kamikaze redeem[455]:     raise e

Jan 29 03:35:23 kamikaze redeem[455]: ConfigParser.ParsingError: File contains parsing errors: /etc/redeem/local.cfg

Jan 29 03:35:23 kamikaze redeem[455]:         [line 64]: 'Planner]\n'

Jan 29 03:35:23 kamikaze systemd[1]: redeem.service: Main process exited, code=exited, status=1/FAILURE

Jan 29 03:35:23 kamikaze systemd[1]: redeem.service: Unit entered failed state.

Jan 29 03:35:23 kamikaze systemd[1]: redeem.service: Failed with result 'exit-code'.


---
**Jon Charnas** *January 30, 2018 06:50*

Ok so.. good news and bad news? Good news is your cape is just fine. The bad news is you have a typo on your [local.cfg](http://local.cfg), so your second cape isn't going to fix your problem..


---
**Jon Charnas** *January 30, 2018 06:51*

I'm not certain what exactly is the problem yet, but if you can post your [local.cfg](http://local.cfg) file somewhere, I can ping some of the devs to take a look and give you a hand. I'm assuming you're running the redeem packaged in umikaze 2.1.1?


---
**William Burton** *January 30, 2018 14:38*

Well that's good news. I need another anyways so no worries. I'll get the local config when I get home tonight. It's weird it stopped working in the middle of a print but maybe it was corrupted somehow. Thanks again for your assistance 


---
**William Burton** *January 31, 2018 00:51*

You are the man!  So i cleared out the local config and it came up.  Then i started looking thru it and found a [ was missing in front of Planner.  Added it back and all is good.   The power supply was bad though.  It blew a fuse and new fuse was no better.  It was a cheep one from China.  But anyways your help was greatly appreciated.




---
**Jon Charnas** *January 31, 2018 06:42*

Cool! I'm glad you fixed the redeem problem. And yeah, power supply problems always suck. One of the reasons why, despite some people saying they're not great for it, I stick to atx units... Always find in local shops quickly if I blow one out


---
*Imported from [Google+](https://plus.google.com/110903600885855713529/posts/6fVZ1LEczXm) &mdash; content and formatting may not be reliable*
