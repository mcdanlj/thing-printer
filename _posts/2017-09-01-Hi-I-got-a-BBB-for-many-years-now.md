---
layout: post
title: "Hi, I got a BBB for many years now"
date: September 01, 2017 20:32
category: "Discussion"
author: Vinh Dam Nguyen
---
Hi, I got a BBB for many years now. 



I try to load it with softwares starting with Kamikaze before I buy the replicape. 



My BBB is of version A5C which has only 2GB of eMMC, I think that is why failed to flash Kamikaze to it. I try to run direct from SD card but not successful yet, I don't know that's because the small eMMC or it's my errors editing the uEnv.txt



Do you think I could and should try to use all the softwares for Replicape booting from the SD card? or the old BBB is just brick?





**Vinh Dam Nguyen**

---
---
**Dan Cook** *September 01, 2017 20:45*

And you edited the uEnv.txt correctly? I'm still using my older BBB on a daily basis, and I'm pretty sure they designed the latest image to fit within the 2GB limitations.


---
**Jon Charnas** *September 01, 2017 20:52*

2.1.0 should indeed fit on a 2GB edition - however A5C is quite old and according to [elinux.org - Beagleboard:BeagleBoneBlack - eLinux.org](http://elinux.org/Beagleboard:BeagleBoneBlack#Revision_C_.28Production_Version.29) the main processor is a different chip from Rev B and Rev C. **+Vinh Dam Nguyen** the only change to make in uEnv.txt to boot from SD is to insert a # in front of the last line of the file (the one that flashes to eMMC). 



You'll also need to boot the BBB while pressing down on the "boot" button (the one on the opposite side of the ethernet port)



For more debugging help you'd probably need to use a JTAG console to see what the kernel errors are on boot. Let us know if you manage to boot Kamikaze 2.1.0? Umikaze 2.1.1 will be too heavy unfortunately, but I can try to make a "light" edition for folks with 2GB BBBs.


---
**Vinh Dam Nguyen** *September 01, 2017 21:19*

Dan and Jon, many thanks for your quick responses. 



Actually I have debian in eMMC (old version that fits) that I want to keep. 



I edited the uEnv.txt on Kamikaze image to comment out the last line so to boot from the SD but it boot from the eMMC debian instead. 



Next try, I pressed the boot button and apply power, to my surprise after a minute or so the BBB start the rolling the LED sequentially. Eventually all leds turned off.



Rebooting without the SD, it ends up with 2 of the leds light up solid.  I suppose Kamikaze shows something on  the monitor, I connect to the monitor but there is nothing. The keyboard and the mouse do nothing.



Does Kamikaze show something on the monitor without the replicape? Should I try again editing the uEnv.txt and try again? is there more than one uEnv.txt file in the image?




---
**Jon Charnas** *September 01, 2017 22:20*

oh... there might be. You'd need to edit the boot/uEnv.txt file and press the boot button to boot from SD... You should have been seeing the Toggle interface on the screen if it had booted normally.


---
**Vinh Dam Nguyen** *September 03, 2017 17:35*

I try one more time flashing the eMMC but the Kamikaze got stuck there with 2 LEDs on. I think that has to do with the small eMMC.



I edit the boot/uEnv.txt and now it boot from the SD card,  no need to push the boot button. I see Toggle on the monitor but in sideway portrait. The mouse and keyboard are of no use. Does this mean I have to have the Manga screen?


---
*Imported from [Google+](https://plus.google.com/102902839402668522825/posts/LrwrgtwU716) &mdash; content and formatting may not be reliable*
