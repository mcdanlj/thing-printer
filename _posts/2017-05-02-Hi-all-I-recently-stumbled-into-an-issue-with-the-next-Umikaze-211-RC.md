---
layout: post
title: "Hi all, I recently stumbled into an issue with the next Umikaze 2.1.1 RC"
date: May 02, 2017 14:24
category: "Discussion"
author: Jon Charnas
---
Hi all,



I recently stumbled into an issue with the next Umikaze 2.1.1 RC. I was seeing 2 USB networks... I finally have the explanation thanks to RobertCNelson (the dev who makes Ubuntu for the BBB): [https://github.com/RobertCNelson/linux-dev/issues/44](https://github.com/RobertCNelson/linux-dev/issues/44)



In short, the reason there are two interfaces is to facilitate access from Windows which sees one interface, or Mac OSX which will see the other one. Linux will see both.



So, as usual, I'd like your feedback concerning future developments.



Can you please indicate how you prefer that I proceed?







**Jon Charnas**

---
---
**Matouš Vrba** *May 03, 2017 19:19*

I see only one connection with OpenSUSE 42.2 and the last release of Umikaze from Github, but I would vote to leave both and document it.


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/ey21LA4qLfi) &mdash; content and formatting may not be reliable*
