---
layout: post
title: "On Friday a french replicape user and I had a chat"
date: April 23, 2017 09:49
category: "Discussion"
author: Jon Charnas
---
On Friday a french replicape user and I had a chat. Turns out that for Core XY and H-bot printers he and members of the fablab nearby found a significant speed difference between the Replicape and other boards of a similar level (Smoothie, Duet). The same speed difference did not exist for the fablab's delta printers.



This is truly unfortunate and points to potential implementation problems of the path planner for those motion kinematics.



I've created a "beta-test" image of Kamikaze 2.1.0 with the development branch of redeem installed instead of the stable version. In addition there is the CPU frequency fixed to maximum speed, something that can cause issues for kinematics with lots of calculations for stepper motion calculations (especially on a Delta printer).



I am posting the link to this image here, so that anyone who thinks their printer is too slow can give it a try and let us know if this fixes the speed.



What I would ask is that you give feedback on whether or not this helps your printer! Either post here, comment below or join us on Slack and say something in the #general or #support channels - this is critical information for us to be able to fix the issue once and for all.



Thanks guys, and here's to hoping we can get you guys printing better and faster than before!





**Jon Charnas**

---
---
**Ryan Carlyle** *April 23, 2017 14:08*

How far is it off? A multiple of 70.7% (sqrt(2)/2) or 50% is pretty easy to inadvertently get with CoreXY/HBot kinematics because the rotation transform is also a compound pulley mechanism. Sailfish and Repetier both had issues with that in the past. 


---
**David Slade** *April 23, 2017 16:02*

Hello Ryan, I wrote a post to explain the problem  but I think it need to ne moderated. It's about 40 to 100% slower than estimation, and real  print time on same  printer  it's another firmware ils really 30 to 100% slower ( may be  a little more on complexe peint)


---
**Tan Tuan TRAN** *April 23, 2017 18:55*

Same printing time for me, i have printed cat-3x-M10, i took me 33mn, almost same time after update.


---
**Jon Charnas** *April 23, 2017 19:11*

**+Tan Tuan TRAN** can you share a picture or link of the part? One of the things we suspect is a slowdown due to very complex geometries. A simple square part would be slow due to the default jerk/acceleration values


---
**Tan Tuan TRAN** *April 23, 2017 19:15*

but some thing strange, with the same gcode, with the new image, i have the 2 first layer shaft


---
**Tan Tuan TRAN** *April 23, 2017 19:16*

try to set xy acceleration back to 0.5


---
**Tan Tuan TRAN** *April 23, 2017 19:27*

i have left all by default, but today, i have try to tweak a little x-y acceleration an djerk, but, i didnt remember the time i have done before tweaking them. I'll publish the time it take with the new image and set this acceleration to default


---
**Jon Charnas** *April 23, 2017 19:36*

Can you elaborate on what you meant with the first two layers?I didn't understand


---
**Tan Tuan TRAN** *April 23, 2017 20:01*

36mm at default acceleration (0.5) and djerk 0.01


---
**Tan Tuan TRAN** *April 23, 2017 20:17*

djerk 0.02 make layer shift on new image, not and the old version


---
**Tan Tuan TRAN** *April 23, 2017 20:53*

35 mm with acceleration at 0.8 and djerk at 0.01


---
**Tan Tuan TRAN** *April 23, 2017 20:54*

so better with old version, i can do with acceleration at 0.8 and djerk at 0.02, but now with djerk at 0.02 i have layer shift at 3rd ou 4th layer


---
**Tan Tuan TRAN** *April 23, 2017 22:21*

**+Jon Charnas** the printer do 2 or 3 layer, and after that, it print with a shift and continue as nothing happend


---
**Tan Tuan TRAN** *April 23, 2017 22:24*

i also notice that the fan didn't work, even i push 'fan on' on octoprint


---
**Jon Charnas** *April 24, 2017 05:29*

That's probably due to a bad or conflicting configuration in the cold end settings


---
**Jon Charnas** *April 24, 2017 05:31*

The layer shift sounds like skipped steps, and it can be difficult to find out the exact amount to go in current... I'll get Wiley to look at your reports here, since he's writing the path planner


---
**David Slade** *April 24, 2017 07:25*

First test: the printer seems more nervous, but print time don't change. I have layer shift too.


---
**Tan Tuan TRAN** *May 01, 2017 11:32*

**+Jon Charnas** i have tune up the current, but same. but i seen to be more than a layer shift. with old version, i have acc at 2 and djerk at 0.02, i dont notice this issue.

![images/507b7d3dc8fa0f00591b7b613f4a4ddf.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/507b7d3dc8fa0f00591b7b613f4a4ddf.jpeg)


---
**Jon Charnas** *May 01, 2017 11:34*

Ok, noted. David reported much the same to me on Slack. I'll communicate with Wiley (the path planner wizard) and we'll see what we can fix there.


---
**Tan Tuan TRAN** *May 01, 2017 11:35*

**+Jon Charnas** ok didn't know that david already tell to you, i just tell to him yesterday.


---
**Jon Charnas** *May 01, 2017 11:38*

It's good to make sure the message gets through, it's an important problem. :)


---
**Nicolas Seac'h** *May 02, 2017 16:25*

+1, i've got the same pb. print time is twice more than octoprint estimation


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/cRt7JK9ksju) &mdash; content and formatting may not be reliable*
