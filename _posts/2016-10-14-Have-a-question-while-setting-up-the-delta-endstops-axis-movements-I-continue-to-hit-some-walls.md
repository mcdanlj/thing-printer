---
layout: post
title: "Have a question while setting up the delta endstops axis movements, I continue to hit some walls"
date: October 14, 2016 21:37
category: "Discussion"
author: Edwardo Garabito
---
Have a question while setting up the delta endstops  axis movements, I continue to hit some walls. I cant seem to figure out how to switch from Cartesian mode to Delta.



 I use the M270 mcode that Elias states in his guide. But when I send "M270 S3" in the terminal, the printer still acts as if  its Cartesian. Not sure whats going on here one bit﻿





**Edwardo Garabito**

---
---
**Elias Bakken** *October 14, 2016 22:12*

Hm, that is strange. Could you write "M270?" You should get a description of how the command works!


---
**Edwardo Garabito** *October 14, 2016 22:20*

Yeah it does that for me but even so ive tried so many different variations i still cant get it to change into delta mode.



It was re flashed last night with the latest kamikaze image 



Here are the different variation ive tried. 



M270 S3

m270 s3

M270 S 3

M270 s 3

M270 S.3

M270 (then i would press enter  and send S3)



Ive had no luck after trying so many things it only wants to stay in Cartesian mode. 




---
**Elias Bakken** *October 14, 2016 22:25*

It should be "M270 S3", but I cannot think of what could be wrong. Could you check the log and see? It should be in the settings->log


---
**Edwardo Garabito** *October 14, 2016 22:27*

Sure what should i look for?




---
**Elias Bakken** *October 14, 2016 22:29*

Try to set the log level to debug: M111 S10, then try the M270 command and look at the end of the log. I'm not by a computer, so I can't check, but u think that should be right.


---
**Edwardo Garabito** *October 14, 2016 22:36*

Nothing so Far. 



The log seems to set the setting too 3 which is good.

But the machine its self only moves the one z stepper every time i try to move up and down.



here a copy of the log after i set log level.



2016-10-14 22:32:14,752 root         INFO     Debug level set to 10

2016-10-14 22:32:27,772 root         DEBUG    Poking watchdog

2016-10-14 22:32:29,136 root         INFO     Coordinate system set to 3

2016-10-14 22:32:34,081 root         DEBUG    Enabling stepper Y

2016-10-14 22:32:34,102 root         DEBUG    Enabling stepper X

2016-10-14 22:32:34,122 root         DEBUG    Enabling stepper Z

2016-10-14 22:32:34,141 root         DEBUG    Enabling stepper E

2016-10-14 22:32:34,165 root         DEBUG    Enabling stepper H

2016-10-14 22:32:46,883 root         DEBUG    Execute M18

2016-10-14 22:32:46,906 root         DEBUG    Disabling stepper Y

2016-10-14 22:32:46,931 root         DEBUG    Disabling stepper X

2016-10-14 22:32:46,946 root         DEBUG    Disabling stepper Z

2016-10-14 22:32:46,971 root         DEBUG    Disabling stepper E

2016-10-14 22:32:46,987 root         DEBUG    Disabling stepper H

2016-10-14 22:32:57,816 root         DEBUG    Poking watchdog

2016-10-14 22:33:27,865 root         DEBUG    Poking watchdog

2016-10-14 22:33:57,910 root         DEBUG    Poking watchdog

2016-10-14 22:34:27,953 root         DEBUG    Poking watchdog




---
**Elias Bakken** *October 14, 2016 22:42*

What about if you try to move the X and Y? Do they more as in a cartesian setup then? 




---
**Edwardo Garabito** *October 14, 2016 22:47*

its strange now only the z axis moves. If i send the command to move the x or y the z moves instead. Before in Cartesian mode each motor moved independently 


---
**Edwardo Garabito** *October 14, 2016 22:50*

So i figured out that it is switching back and forth from profiles. But now in delta mode the Z axis seems to control all motor functions. So its the only axis that moves when a movement command is sent. 




---
**Elias Bakken** *October 14, 2016 23:21*

Could it be that your end stops are stopping any movement for the other axes? What does your end stop configuration look like? 


---
**Edwardo Garabito** *October 15, 2016 01:10*

Every thing seems normal for endstops



Send: M574

Recv: ok X1 stops: x_ccw , X2 stops:  , Y1 stops: y_ccw , Y2 stops:  , Z1 stops: z_ccw , Z2 stops:



New issue i have it working a little bit better. The new issue has too do still with the motion of the z axis. X, and Y are not acting quite as well  either but that a different story. 



The z axis if i send the command for it to move  up only the X axis moves. But if i send the command for the z axis too move down, all axis move together down wards. 


---
**Elias Bakken** *October 15, 2016 02:12*

I think the problem you are seeing has to do with your end stops being inverted. Try to invert them and see if that helps.


---
**Edwardo Garabito** *October 15, 2016 02:28*

Yeah same problem, Im using the Rostock max printer profile supplied with redeem. 



My steppers on mounted on top of the printer vs the bottom, thats the only difference. Ive already inverted the motion control of the steppers too compensate for that. 



And have tried to invert the endstops but still same problem. For some reason if i try to move up only the x axis moves. while the others stay put. all 3 motors do move if i try to move the z axis down. Im very confused on this one.




---
**Edwardo Garabito** *October 15, 2016 02:37*

I finally have it figured out! For some reason, In the end stop config, the x axis was set too true, alone. While the Y, and z where set too false. 



After setting all the axis too false i was able too get the motion of the axis working perfectly. Thanks soooooooooo much for the help!!

 


---
**Edwardo Garabito** *October 15, 2016 02:52*

New problem haha



What could this mean? It happens only after ive homed all the axis.



Send: G91

Recv: ok

Send: G28 Z0

Recv: End stop X1 hit!

Recv: End stop Z1 hit!

Recv: End stop Y1 hit!

Unexpected error while reading serial port, please consult octoprint.log for details: SerialException: 'read failed: [Errno 5] Input/output error' @ comm.py:_readline:1618

Changing monitoring state from 'Operational' to 'Offline: SerialException: 'read failed: [Errno 5] Input/output error' @ comm.py:_readline:1618'

Connection closed, closing down monitor


---
**Edwardo Garabito** *October 15, 2016 02:52*

I get disconnected and can no longer control the printer




---
**Elias Bakken** *October 15, 2016 13:19*

Does it turn off? Sounds like you are getting a shirt circuit with one of your end stops.


---
**Edwardo Garabito** *October 15, 2016 17:33*

The printer will just disconnect from the server, and after about 5 mins, im allowed to re connect.


---
**Edwardo Garabito** *October 15, 2016 17:34*

But as soon as everything is homed again. It does the same thing. I get disconnected the printer is still on,but stay idle, and im able to reconnect after 5 mins.


---
*Imported from [Google+](https://plus.google.com/115118943136595777855/posts/a6iEj277Dma) &mdash; content and formatting may not be reliable*
