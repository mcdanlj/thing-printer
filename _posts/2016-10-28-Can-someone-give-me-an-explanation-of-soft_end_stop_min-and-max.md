---
layout: post
title: "Can someone give me an explanation of soft_end_stop_min and max"
date: October 28, 2016 14:22
category: "Discussion"
author: Jim Nofsinger
---
Can someone give me an explanation of soft_end_stop_min and max.  I noticed on a the rostock_max the settings are



soft_end_stop_min_x = -0.05

soft_end_stop_min_y = -0.05

soft_end_stop_min_z = -0.001



soft_end_stop_max_x = 0.05

soft_end_stop_max_y = 0.05

soft_end_stop_max_z = 0.6



how are those numbers figured out?  Are the negative min values there because of the hardware endstops?  how is max measured in relation?  how does Z fit into the picture?



I think this might be keeping me from having a leveled print bed, as my printable area on my printer is 254x254mm by 304mm tall.  My settings are



soft_end_stop_min_x = -0.128

soft_end_stop_min_y = -0.128

soft_end_stop_min_z = -0.001

soft_end_stop_max_x = 0.128

soft_end_stop_max_y = 0.128

soft_end_stop_max_z = 0.340



which seems incorrect to me.





Thanks,

Jim





**Jim Nofsinger**

---


---
*Imported from [Google+](https://plus.google.com/118269177478098269592/posts/CUREcoUn4ny) &mdash; content and formatting may not be reliable*
