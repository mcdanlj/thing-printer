---
layout: post
title: "I have a problem where a fan output only moves the fan very shortly when sending M106 P1 S255 using the Replicape Rev B3"
date: March 06, 2017 12:04
category: "Discussion"
author: Henrik Söderholtz
---
I have a problem where a fan output only moves the fan very shortly when sending M106 P1 S255 using the Replicape Rev B3. The other three outputs work just fine with the same fans.



What could cause this?





**Henrik Söderholtz**

---
---
**Henrik Söderholtz** *March 06, 2017 12:09*

Note: This was discovered when using the Fan connectors for the first time. My hotend fan is hard wired to 24V.


---
**Elias Bakken** *March 06, 2017 12:37*

P1 is connected to the temperature of thermistor e in most configurations. Sending M106 P1 S255 will then override it momentarily, but on the next loop cycle the temperature of the thermistor will govern its speed. You can change this behavior by explicitly setting the P1 to not be dependent on Thermistor E


---
**Henrik Söderholtz** *March 06, 2017 13:11*

**+Frederic M.** No, fans are 12V. 

**+Elias Bakken** Thank you, that would explain it.


---
*Imported from [Google+](https://plus.google.com/103752299402481876381/posts/TrnU4w1XgPY) &mdash; content and formatting may not be reliable*
