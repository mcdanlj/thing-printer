---
layout: post
title: "What temp chart would I use for a Semitec 100kOhm NTC Thermistor I use for my hotends?"
date: February 25, 2016 14:31
category: "Discussion"
author: Tim Curtis
---
What temp chart would I use for a Semitec 100kOhm NTC Thermistor I use for my hotends? And for a EPCOS thermistor I use for my heated bed?





**Tim Curtis**

---
---
**Tim Curtis** *February 25, 2016 14:40*

I think I found my answer for the Semitec. I think I would use Semitec-1004GT-2.cht? Still not sure about the EPCOS one though?


---
**Elias Bakken** *February 25, 2016 14:56*

Try and find the product name for the epcos thermistor, you might find it in the folder.


---
**Tim Curtis** *February 25, 2016 15:06*

Ok thanks I'll look up the one that came in the bed heater. Thanks again.


---
**Jon Charnas** *February 25, 2016 16:04*

Yes I think perhaps that's a section we need to expand in the wiki... I'll write it up as soon as I've gotten around to getting my printer moving the right way. (and documented that, as well)


---
**Elias Bakken** *February 25, 2016 16:05*

Would you like my Fritzing files as a starting point?


---
**Jon Charnas** *February 25, 2016 16:08*

**+Elias Bakken** Uh, I'm not sure I know enough to actually work with those... I'll read the documentation already in redeem first (I think I also saw somewhere a basic guide to create your own .cht file)


---
**Elias Bakken** *February 25, 2016 16:10*

There is a tool for it in the "tools" folder, but there is also a pending pull request for beta value system eliminating the lookup tables all together.


---
*Imported from [Google+](https://plus.google.com/106115959585163180149/posts/BPsUYtJLS3M) &mdash; content and formatting may not be reliable*
