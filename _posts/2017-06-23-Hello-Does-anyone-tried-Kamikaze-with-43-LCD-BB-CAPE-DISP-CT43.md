---
layout: post
title: "Hello, Does anyone tried Kamikaze with 4.3\" LCD (BB-CAPE-DISP-CT43)?"
date: June 23, 2017 13:22
category: "Discussion"
author: Radek Biskupski
---
Hello,

Does anyone tried Kamikaze with 4.3" LCD (BB-CAPE-DISP-CT43)?

On my LCD only left half of original view is presented and rotated 90 degree.

Probably because of half resolution of that cape than regular Manga has.

Where is a good place to edit and recompile files with new resolution settings?

Greetings from Poland





**Radek Biskupski**

---
---
**Jon Charnas** *June 23, 2017 13:29*

hi Radek, you can look in /etc/toggle/default.cfg and see what the rotation parameter name is, then set it to 0 in /etc/toggle/local.cfg - much like Redeem :)


---
**Jon Charnas** *June 23, 2017 13:29*

The resolution is probably ok, if your screen has the same resolution as the original Manga Screen


---
**Radek Biskupski** *June 23, 2017 13:34*

Thank you Jon, I check this file this afternoon, but it isn't unfortunately - it's 480X272 :( Screen seems working well and reacting on touches.


---
**Radek Biskupski** *July 05, 2017 12:27*

Finally I made this working.

Short solution below, this probably should works with other LCDs too:

1. /etc/toggle/default.cfg - rotation to 0

2. .../style/ui_800x480.json - every width 800 and height 400 need to be replaced with resolution of new LCD

3. .../toggle/Graph.py and .../toggle/[FillamentGraph.py](http://FillamentGraph.py) all instances of 800 & 500 should be replaced with resolution of new LCD

4. resize proportionally down/up all PNG files

5. the most hard to do point - again, in ui_800x480.json, replace sizes and positions of all children actors like PNGs. Many tries possible up to get satisfied effect.

6. do make build/install

7. restart Toggle

8. Check effect on new LCD - eventually repeat points from 4 to 6



Good Luck :)

Radek


---
**Matthew Palmore** *August 21, 2017 00:55*

do you modify the code before installing it to the BBB or do you modify it within?


---
**Jon Charnas** *August 21, 2017 04:44*

Usually modify it on the BBB, it's easier to do it through SSH if you're on Windows (can't read the SD card for system from Windows)


---
**Radek Biskupski** *August 22, 2017 07:35*

Yes, as Jon said, SSH is easier and faster. You don't have to copy, modify and copy again every time when you fix settings. All sources are included in folders, on BBB, after regular Kamikaze installation. So you are able to do all modifications just straight on BBB - you don't have to copy anything more from outside BBB. 


---
*Imported from [Google+](https://plus.google.com/100171647175055988434/posts/AwfVwkj1Jhd) &mdash; content and formatting may not be reliable*
