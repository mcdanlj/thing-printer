---
layout: post
title: "There will be a new revision of Replicape coming out in a few months"
date: March 21, 2015 09:28
category: "Discussion"
author: Elias Bakken
---
There will be a new revision of Replicape coming out in a few months. One of the most important features is a lower price. Because of that we have decided to sell the remaining replicapes at the price we hope to achieve for the new revision. There are only two left in stock, so be quick if you want to get in on that. Here are the suggested changes for the new revision, but feel free to pile on if there is anything you feel is missing :)



[http://www.thing-printer.com/product/replicape/](http://www.thing-printer.com/product/replicape/)



– Lower audible noise (more and better capacitors + Vref filtering + 4 layer PCB)

– Lower EMI noise, hopefully CE/FCC markings

– Enable pin on PWMs

– Decay mode can be in High Z mode on steppers enabling mixed decay.

– 3-pin end stop connectors instead of 2

– 2-pin thermistor inputs to be more compatible with other electronics.

– Polarity protection on VCC input.

– Two of the end stop inputs can function as 5V servo outputs.

– 15 V tolerable end stop inputs enable the use of inductive sensors without voltage divider.



We are also looking for a cheap and effective solution on programmable slew rate on the power mosfets,

but it looks difficult to get this handled.



The lower price will be possible by opting for a 500 unit batch, using some different component suppliers,

lower margin, a cooperation with a Norwegian company helping with PCB production and logistics, and finally handling

RMA in house (cuts about $20 off the price).



We are hoping to have the Kickstarter live some time in May, add to that 30 days for the campaign and some 45 days for

production and delivery. 





**Elias Bakken**

---


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/4CipPnA6TMi) &mdash; content and formatting may not be reliable*
