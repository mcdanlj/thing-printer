---
layout: post
title: "Alright.. well after 5 .1mm layers of printing, I have proved the auto bed leveling matrix doesn't seem to be working with my printer"
date: July 05, 2016 23:08
category: "Discussion"
author: Philip Acierto
---
Alright.. well after 5 .1mm layers of printing, I have proved the auto bed leveling matrix doesn't seem to be working with my printer.



Now I kind of expected this as during my test dry prints the nozzle was driven into the bed but I let this go for a few layers to see if it corrected itself. At the bottom left corner, my origin, I can set the nozzle offset with:



G92 Z14.5                               ; Compensate probe offset 

G0 Z0 F800                             ; Move nozzle to the lowest position



I do this directly after probing with a g30 in my g29 macro as seen in my previous post.



That would put my nozzle directly over the bed where I want it at the origin. When my print starts though, by the time my nozzle gets to the center of the bed to begin its first pass its shoved into the bed. If I compensate and add extra height, then anywhere near my origin and other edges of the bed are way high.



I have 8 probe points as seen in my other videos on this page. Any ideas as to why the matrix wouldn't work with my 8 points?

![images/4387f50882d54fff5290620cd1f52c06.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/4387f50882d54fff5290620cd1f52c06.jpeg)



**Philip Acierto**

---
---
**Elias Bakken** *July 05, 2016 23:49*

Hm... 8 points should not be a problem. Have you updated to the latest octoprint_redeem plugin? I don't think I have made a package for it yet, but if you install that, you will get a notfication in OctoPrint and a way to view the probe data. 


---
**Philip Acierto** *July 05, 2016 23:52*

I actually haven't seen any notification or visual representation on toggle, so I was kinda curious if maybe I wasn't full updated. I'll check and see.



I'll have to find the walk through on updating that specific plugin.., I've just been doing the general apt update and install after switching to the dev branch. I swear I program every day for my day job, but linux was just something I never did outside a course of redhat way back when, but that too was a visual OS.


---
**Philip Acierto** *July 06, 2016 00:04*

Is this the format to use to install it?



git clone "[https://github.com/eliasbakken/octoprint_redeem](https://github.com/eliasbakken/octoprint_redeem)"



cd "OctoprintPlugins" <- i dont have a plugin folder, so justmake one anywhere?



python setup.py install



systemctl restart octoprint.service




---
**Elias Bakken** *July 06, 2016 01:13*

I'm working on an updated Kamikaze1, so you might want to use this for reference as well. Found that using virtualenv helped. Her is my work so far: [https://github.com/eliasbakken/Kamikaze2/blob/master/make-from-kamikaze1.sh](https://github.com/eliasbakken/Kamikaze2/blob/master/make-from-kamikaze1.sh)


---
**Philip Acierto** *July 06, 2016 01:14*

that is handy!


---
**Philip Acierto** *July 06, 2016 01:22*

**+Elias Bakken** 



root@kamikaze:/usr/src/OctoPrint# sudo -u octo /usr/src/venv/bin/python setup.py install

sudo: /usr/src/venv/bin/python: command not found





not sure if you are looking for feedback, but first i was trying to update my octoprint and this came up.


---
**Elias Bakken** *July 06, 2016 01:26*

It does not run as a clean script for your need, but if you enable the prerequisites and the venv command + the octoprint part it might have a chance.


---
**Philip Acierto** *July 06, 2016 21:39*

running everything on that script with the prereqs did get everything updated and working. It didn't solve my bed leveling issues so I swapped my inductive sensor to a servo controlled swinging arm with a mechanical z end stop switch.



I haven't done much testing with the new probe after I got it wired up, but I'll report back once I get some time to play with it.


---
*Imported from [Google+](https://plus.google.com/111974602640488998735/posts/32XmdqjJCqD) &mdash; content and formatting may not be reliable*
