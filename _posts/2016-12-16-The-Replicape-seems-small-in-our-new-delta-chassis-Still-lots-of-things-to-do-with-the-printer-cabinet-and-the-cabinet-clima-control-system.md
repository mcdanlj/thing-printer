---
layout: post
title: "The Replicape seems small in our new delta chassis :) Still lots of things to do with the printer cabinet, and the cabinet clima control system"
date: December 16, 2016 11:08
category: "Discussion"
author: janbvik
---
The Replicape seems small in our new delta chassis :) 

Still lots of things to do with the printer cabinet, and the cabinet clima control system.



![images/39889f58d200b98b4f3ef5192240b3eb.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/39889f58d200b98b4f3ef5192240b3eb.jpeg)
![images/35a6914cdaa47e5c8bd6141f62f03106.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/35a6914cdaa47e5c8bd6141f62f03106.jpeg)
![images/46552fa8e02ff6a8e61a3faa8953bec9.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/46552fa8e02ff6a8e61a3faa8953bec9.jpeg)

**janbvik**

---
---
**Jon Charnas** *December 16, 2016 11:59*

Holy mother of delta! Can you share details of your build? (size, what frame parts you use, motors, hotend and nozzle size, etc?) I'm thinking of building or upsizing my Kossel Mini, but this is a lot bigger than I was thinking...


---
**janbvik** *December 16, 2016 12:20*

Hi Jon, it`s my own design, 80x40(60deg) and 40x40 Heavy version alu profiles. All other parts custom water jet cut or machined in stainless steel or aluminium.  2,35 meters high, buildplate 10mm thick 0,8 meter, glass Ø0,86m will be ordered later. The design is made to demount and assembly from 5 parts, because the printer is to wide to get thru a 0,9 meter door, and to tall to raise in most office rooms. 

German high quality 2 meter linear tracks and blocks. Arms made from Ø 12 carbon fiber tubes. Curved polycarbonate build chamber walls with controlled clima. Flying extruder, feeding from 30kg filament reels. BBB and Replicape, 24V external stepper drivers,  3Amp Nema23 motors, 0,9deg and 1,8 deg. 


---
**Jon Charnas** *December 16, 2016 12:27*

Woah! Very very very nice! Are you going to run it through ethernet or wifi? (And damn do I need to get that Kamikaze 2.1RC out <b>now</b> ;) )


---
**janbvik** *December 16, 2016 12:44*

About Kamikaze, I`v been to busy to get time to work much with controls yet. Did a set up when Elias kindly shipped me a pre release Replicape this summer. But most likely based on my limited Linux experience, I have not got the Kamikaze image and so to work as expected yet. I have done a plasma cutter based on Jessy before, but exept from conversions and maintenance of Z-Corp printers I have not worked much with Linux. Bought a simple chinese delta 1,1meter high recently, just to use it for Replicape control testing from my home office. My "my better half" have made new rules for bringing more cnc machines to the house, so the delta is at my workplace :)  




---
**Jon Charnas** *December 16, 2016 12:46*

Haha, well, I would suggest you use Kamikaze 2.0.8 for now, and if you can use Ethernet connection until 2.1 is ready (2.0.8 sometimes has wifi/USB issues depending on too many factors to pinpoint)


---
**janbvik** *January 16, 2017 10:50*

Replicape setup getting closer to "hook up"  :)

![images/dbd578846523e831336b114f0e1d8868.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/dbd578846523e831336b114f0e1d8868.jpeg)


---
*Imported from [Google+](https://plus.google.com/100134923545560313587/posts/9mZ8DsWmLq6) &mdash; content and formatting may not be reliable*
