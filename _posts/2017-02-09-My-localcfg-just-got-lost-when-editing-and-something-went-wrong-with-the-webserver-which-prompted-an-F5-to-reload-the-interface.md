---
layout: post
title: "My local.cfg just got lost when editing and something went wrong with the webserver which prompted an F5 to reload the interface"
date: February 09, 2017 10:20
category: "Discussion"
author: Henrik Söderholtz
---
My local.cfg just got lost when editing and something went wrong with the webserver which prompted an F5 to reload the interface. WHERE can it have gone!!!? This is a disaster!





**Henrik Söderholtz**

---
---
**Jon Charnas** *February 09, 2017 11:06*

Can you try logging in with SSH and look at the file /etc/redeem/local.cfg ?


---
**Jon Charnas** *February 09, 2017 11:07*

(I'm assuming your editing was in the Octoprint interface, otherwise I'm afraid there is no backup currently being down in kamikaze)


---
**Henrik Söderholtz** *February 09, 2017 12:57*

I unfortunately started editing the file again. I guess I shouldn't have, because now, of course, it reflects the new contents...


---
**Jon Charnas** *February 09, 2017 12:59*

Can you help me understand what you mean? You started editing the first time through Octoprint? Then your changes were lost because the page had to be reloaded, and you wanted to go back to the file version before where you had made your edits?


---
**Henrik Söderholtz** *February 09, 2017 13:37*

Yes, that's correct. I edited in Octoprint, then all data in the local.cfg dissappeared when the web interface stopped responding. I reloaded the browser window with F5. After that, the local.cfg was  blank in Octoprint. Before you made your suggestion to look through SSH, I had already started putting data back in Octoprint, and that new data is what I found when connecting through SSH.




---
**Daniel Kusz** *February 09, 2017 15:58*

Yestetday my [printer.cfg](http://printer.cfg)'s and Toggle cfg's are dissapeard. [Local.cfg](http://Local.cfg) is ok. I think I need to load empty file again. 


---
**Henrik Söderholtz** *February 10, 2017 08:26*

Daniel, try just reloading the interface with F5, mine dissappeared briefly from the interface too, but were back upon reloading.


---
*Imported from [Google+](https://plus.google.com/103752299402481876381/posts/PXq5JaiuQxK) &mdash; content and formatting may not be reliable*
