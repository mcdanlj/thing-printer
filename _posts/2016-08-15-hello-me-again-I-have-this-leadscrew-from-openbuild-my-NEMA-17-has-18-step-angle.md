---
layout: post
title: "hello me again. I have this leadscrew from openbuild my NEMA 17 has 1.8 step angle"
date: August 15, 2016 05:47
category: "Discussion"
author: Step Cia
---
hello me again. I have this leadscrew from openbuild 



my NEMA 17 has 1.8 step angle. 



should my steps_pr_mm_z = 200 or 8?



thx









**Step Cia**

---
---
**Daniel Kusz** *August 15, 2016 06:50*

Measure the pitch of the screw and share the number of steps per revolution (200) for this number.


---
**Step Cia** *August 15, 2016 08:32*

The pitch of the screw is 8mm per rev


---
**Daniel Kusz** *August 15, 2016 08:35*

So 200/8=25  steps_pr_mm_z


---
**Step Cia** *August 15, 2016 15:09*

Ok I'll try that number but I'm pretty sure the wiki didn't mention that... Or perhaps it could be more clearer... 


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/SyrTyYoyfGC) &mdash; content and formatting may not be reliable*
