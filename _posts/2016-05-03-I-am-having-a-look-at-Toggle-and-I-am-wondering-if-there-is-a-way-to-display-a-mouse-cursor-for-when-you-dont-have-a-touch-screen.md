---
layout: post
title: "I am having a look at Toggle and I am wondering if there is a way to display a mouse cursor for when you don't have a touch screen?"
date: May 03, 2016 11:09
category: "Discussion"
author: Daryl Bond
---
I am having a look at Toggle and I am wondering if there is a way to display a mouse cursor for when you don't have a touch screen?





**Daryl Bond**

---
---
**James Armstrong** *May 03, 2016 15:01*

I did see a recent video Elias posted showing some Tabs in Toggle he was testing and looked like he was on a desktop using a mouse for testing. 


---
**Jon Charnas** *May 03, 2016 19:52*

You can apparently run Toggle on a standard desktop edition of ubuntu. Maybe try plugging in a simple USB mouse into the BBB and see if you can move a pointer around?


---
**Elias Bakken** *May 03, 2016 23:13*

The problem is that there is no hardware cursor when running from the framebuffer. The Clutter people recommend making a software implementation for it!


---
**Daryl Bond** *May 04, 2016 00:46*

**+Elias Bakken** I thought that was the case. Do you have a link for how to go about implementing it? I did a quick search but nothing popped up.


---
**Elias Bakken** *May 04, 2016 00:49*

I think the best way is to use a png, and fix it in the application. A simple mouse move event should suffice. 


---
**Daryl Bond** *May 05, 2016 13:02*

Well, I have a mouse pointer now. Thanks for the suggestions **+Elias Bakken** Do you want me to raise a pull request? Or do you want to keep the screen free because it is intended for use with a touch interface?


---
**Elias Bakken** *May 05, 2016 13:04*

Ill accept a pull if you make it configurable:) 


---
**Daryl Bond** *May 05, 2016 13:59*

Done :)


---
*Imported from [Google+](https://plus.google.com/104347555452272558593/posts/T9UdhceFDPF) &mdash; content and formatting may not be reliable*
