---
layout: post
title: "Following up on my build with the replicape, homing for X and Y looks okey but there is something strange with the Z axis"
date: January 27, 2016 23:32
category: "Discussion"
author: Lars Dunemark
---
Following up on my build with the replicape, homing for X and Y looks okey but there is something strange with the Z axis.


{% include youtubePlayer.html id=Wtdk0BTpZak %}
[https://youtu.be/Wtdk0BTpZak](https://youtu.be/Wtdk0BTpZak)





**Lars Dunemark**

---
---
**Elias Bakken** *January 28, 2016 13:54*

Are you using a splitter cable or slave mode for the two Z-axis motors?

If you are using slave mode, make sure all settings for Z equal the settings for H (which I am then assuming is your slave) - microstepping, steps_pr_mm, decay and direction should be the same, finally make sure end stop Z1 stops both steppers, z_pos,h_pos or whatever you have there.


---
**Elias Bakken** *January 28, 2016 13:54*

Keep posting videos! Great way to respond to issues : )


---
**Lars Dunemark** *January 28, 2016 14:00*

I'm using the slave mode on H to mirror Z but I haven't copying all setting. After posting the video I found that by setting same microstepps on H that everything got a lot quieter. Will look thru the config later tonight to see so I haven't missed anything. I noticed that there is some configuration for min_speed and max_speed that was different for H and Z as default. I'm using prusa_i3 as my printer.cfg


---
**Lars Dunemark** *January 29, 2016 12:59*

The problem was fixed by increasing the current on both Z and H and making sure that both had the same settings.


---
*Imported from [Google+](https://plus.google.com/+LarsDunemark/posts/bunbK2QZ2Ev) &mdash; content and formatting may not be reliable*
