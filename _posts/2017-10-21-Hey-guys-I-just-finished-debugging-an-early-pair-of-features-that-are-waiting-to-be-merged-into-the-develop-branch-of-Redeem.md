---
layout: post
title: "Hey guys, I just finished debugging an early pair of features that are waiting to be merged into the develop branch of Redeem"
date: October 21, 2017 22:19
category: "Discussion"
author: Jon Charnas
---
Hey guys,



I just finished debugging an early pair of features that are waiting to be merged into the develop branch of Redeem.



If you've been having stutters on prints before, tell them goodbye! Huuuuge thanks to new redeem dev AndrewJ for having built the functionality to read the gcode files from within Redeem, I went ahead and add compatibility for Marlin's SD controls.



Which means, Redeem now lets OctoPrint think there's an SD card to print from. So Redeem reads the file directly without Octoprint's latency being an issue. The result? See the screenshot below.



We'll try to get this update packaged properly before Thanksgiving for certain, and include the Umikaze in-place upgrade functionality that's been long-overdue if it's ready. Of course documentation is scarce at the moment, but we have been working on a fix to your problems.



![images/d64cb92f1978e43f870bdc686f145edd.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/d64cb92f1978e43f870bdc686f145edd.png)



**Jon Charnas**

---
---
**Elias Bakken** *October 22, 2017 04:42*

This is incredible guys, looking forward to testing :)


---
**Step Cia** *October 22, 2017 05:29*

👍


---
**Tim Curtis** *November 01, 2017 14:04*

Are you still on schedule to have this update packaged properly? Will there be any documentation to support how to use it?


---
**Jon Charnas** *November 01, 2017 14:11*

We've had a slightly slow review process since that was my first patch for Redeem... But it'll be in the develop branch pretty soon. The in-place-upgrade may have to wait a bit longer however. But as soon as it's ready I'll post instructions on how to pull the changes and get the goods running on your current Umikaze 2.1.1 :)


---
**Jon Charnas** *November 02, 2017 08:26*

Hurray, it's been merged in the develop branch on github!


---
**Tim Curtis** *November 02, 2017 15:38*

So should I switch my updates to pull from the develop branch to get this upgrade? Or do I have to re-flash with the development image? I would rather just upgrade if I can. 


---
**Jon Charnas** *November 02, 2017 15:58*

You can pull from the develop branch without an update. However I suggest you wait a bit as a few extra packages have been added as a dependency to the latest redeem develop, and I need to list the commands to be able to install those missing packages. Those should be up before the weekend, and I'll post that in a new G+ post.


---
**Tim Curtis** *November 02, 2017 16:00*

Ok, thanks I'll wait till I see the new post. Thanks for your hard work on all of this stuff Jon.


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/BkevDTCerwH) &mdash; content and formatting may not be reliable*
