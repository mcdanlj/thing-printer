---
layout: post
title: "Print Problems"
date: September 01, 2016 11:08
category: "Discussion"
author: Mathias Giacomuzzi
---
Print Problems.





![images/de09e462c051f238894eed3aad78f319.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/de09e462c051f238894eed3aad78f319.jpeg)
![images/fb18bdcc3f27545810c7cae96bb979d9.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/fb18bdcc3f27545810c7cae96bb979d9.jpeg)
![images/1d4436df0354b9fafa8858b0e0067bed.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/1d4436df0354b9fafa8858b0e0067bed.jpeg)
![images/7e2fc4eb8f2ff361fdfc9204a79e0d32.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/7e2fc4eb8f2ff361fdfc9204a79e0d32.jpeg)
![images/c0305813850f1cc623c23788e347ee88.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/c0305813850f1cc623c23788e347ee88.jpeg)
![images/9574b80b4b541486fa42272d46add23a.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/9574b80b4b541486fa42272d46add23a.jpeg)
![images/1269f03b8bbff995257573d12d99be7b.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/1269f03b8bbff995257573d12d99be7b.jpeg)

**Mathias Giacomuzzi**

---
---
**Daniel Kusz** *September 01, 2016 11:16*

First printing with Replicape? 


---
**Mathias Giacomuzzi** *September 01, 2016 11:16*

I have some print problems with the first setup. First problem print pulls threads. Second problem I do have gaps between the infill and the perimeters but I do not know why.



At one picture you can see the first layer => is it possible that I have a little bit over extrusion here?

  


---
**Mathias Giacomuzzi** *September 01, 2016 11:18*

Yes with replicape and slic3r but I do not know what´s wrong.


---
**Daniel Kusz** *September 01, 2016 11:19*

Steps per mm are correctly for steppers?


---
**Joan Sparky** *September 06, 2016 21:50*

You should try to hit up [https://groups.google.com/forum/#!forum/deltabot](https://groups.google.com/forum/#!forum/deltabot) with that and see what they say - a lot of knowledgeable people over there.

[groups.google.com - Google Groups](http://googlegroups.com)


---
*Imported from [Google+](https://plus.google.com/104869072343043179738/posts/MyHBhx6cQHd) &mdash; content and formatting may not be reliable*
