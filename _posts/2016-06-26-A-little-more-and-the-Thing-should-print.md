---
layout: post
title: "A little more and the Thing should print"
date: June 26, 2016 11:01
category: "Discussion"
author: Daniel Kusz
---
A little more and the Thing should print. I am waiting for aluminum carriages X and Y and I will mount limit switches. There remains little solder and launch full functionality :-) Of course, expect small issues, as always, but at this stage everything is ok :-)





**Daniel Kusz**

---
---
**Elias Bakken** *June 26, 2016 12:01*

Awesome! What hot ends are you going for? I want it in alu now:)


---
**Daniel Kusz** *June 26, 2016 12:22*

For now I designed a X carriage for 2x E3D v5 hot end. The aluminum housing is robust enough and stiff :-)


---
**Elias Bakken** *June 26, 2016 13:24*

Great! I would go for a V6 if you have not got two V5s lying around, and also E3D Titan as extruder in case you don't want to make that for yourself as well!


---
**Daniel Kusz** *June 26, 2016 13:54*

Fortunately for the moment I have one piece of v5, so I can later test v6. I would like to make an inductive sensor but not all at once - the current target is the first effective print :) Tomorrow I need to make new mounting for table from thicker angle - now the front of the table falls under its own weight.


---
*Imported from [Google+](https://plus.google.com/103041373788524358316/posts/SZYZUFWygzA) &mdash; content and formatting may not be reliable*
