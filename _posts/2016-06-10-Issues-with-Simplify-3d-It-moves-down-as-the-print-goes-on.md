---
layout: post
title: "Issues with Simplify 3d. It moves down as the print goes on"
date: June 10, 2016 12:16
category: "Discussion"
author: Andrew Dowling
---
Issues with Simplify 3d. It moves down as the print goes on. I tried editing gcode to more closely resemble the formatting of cura with no luck. I tried replacing g1 with g0. I tried all the simple solutions. So now I am here asking for help


**Video content missing for image https://lh3.googleusercontent.com/-ImAnbwa5JBY/V1qs8gDAlGI/AAAAAAAAADk/WetFdRM3vHYmhLlQ1qTEtVeJOxAxOf7ig/s0/WIN_20160610_19_27_33_Pro.mp4.gif**
![images/681a6f48874863c9f6d313bbf96ea76d.gif](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/681a6f48874863c9f6d313bbf96ea76d.gif)



**Andrew Dowling**

---
---
**Elias Bakken** *June 10, 2016 12:21*

Could you try to install the latest redeem from develop branch? I've made an important change that I hope will solve this.


---
**Elias Bakken** *June 10, 2016 13:27*

There is also a start gcode section in OctoPrint, you might be able to add something there also.


---
**Andrew Dowling** *June 10, 2016 13:29*

I tried to get the development branch installed once already, but it just locked me out of connecting to the machine in octoprint. I will be the first to admit my linux skills are level 1 at best. What would the terminal commands through ssh look like to move to the development branch look like? And to branden - I have tried everything, including trying to trick the machine into flipping the z axis. I have most of what would be my "starting script" in the g29 macro. That way all I have to do is say g29 and everything happens. Again - my g29 works fine in cura. But moves down with simplify 3d. If you would post the entire gcode for your starting script here - Maybe I might get some inspiration for a solution. I think mr. elias is correct in thinking that moving to the development branch will solve the bug that I have now. So please give this poor linux ignorant person the terminal commands to move to that development branch successfully.


---
**Andrew Dowling** *June 10, 2016 13:39*

Is there some gcode out there for "make the z axis move in the opposite direction?" I know there is an M569. do you suppose simply using that to temporarally flip the z axis would work?


---
**James Armstrong** *June 10, 2016 23:30*

Yeah, I had issues with s3d also. Would choke on tool selections and the heater selections and I had to modify the codes to get it to work. 


---
**Klipper Pressure Advance** *June 11, 2016 02:26*

I might be way off but have you checked the direction of homing for Z in the option settings of S3D


---
**Andrew Dowling** *June 11, 2016 10:22*

It occurred to me today that the M569 wont work. It will invert the bed leveling compensation. One thing I plan on trying soon is using the post processing commands in s3d to put a "-" on the g0 command. Ie {REPLACE "g0 Z" "g0 Z-"} I hope this works.


---
*Imported from [Google+](https://plus.google.com/114125966923153934677/posts/WaR8Gn4SgYt) &mdash; content and formatting may not be reliable*
