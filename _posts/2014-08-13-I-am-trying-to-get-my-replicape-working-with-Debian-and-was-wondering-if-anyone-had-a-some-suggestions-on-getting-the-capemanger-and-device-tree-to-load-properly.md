---
layout: post
title: "I am trying to get my replicape working with Debian, and was wondering if anyone had a some suggestions on getting the capemanger and device tree to load properly"
date: August 13, 2014 01:45
category: "Discussion"
author: Sam Nay
---
I am trying to get my replicape working with Debian, and was wondering if anyone had a some suggestions on getting the capemanger and device tree to load properly.  I have copied placed the dts for the replicape in /lib/firmware/ and complied it and it still does not load





**Sam Nay**

---
---
**Elias Bakken** *August 13, 2014 08:03*

Hello Sam! In Debian, look at "custom capes": [http://elinux.org/Beagleboard:BeagleBoneBlack_Debian#Loading_custom_capes](http://elinux.org/Beagleboard:BeagleBoneBlack_Debian#Loading_custom_capes)


---
**Sam Nay** *August 20, 2014 19:17*

Thanks, That took care of the problem


---
*Imported from [Google+](https://plus.google.com/104273704681528962565/posts/ATpnyRWJDgw) &mdash; content and formatting may not be reliable*
