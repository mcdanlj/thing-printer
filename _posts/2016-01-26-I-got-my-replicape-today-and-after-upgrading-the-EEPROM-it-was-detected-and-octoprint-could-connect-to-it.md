---
layout: post
title: "I got my replicape today and after upgrading the EEPROM it was detected and octoprint could connect to it"
date: January 26, 2016 20:54
category: "Discussion"
author: Lars Dunemark
---
I got my replicape today and after upgrading the EEPROM it was detected and octoprint could connect to it.

Here is a first video of the only one connected stepper. The build is still in progress and I still haven't wire anything up yet. And missing endstops.




{% include youtubePlayer.html id=qQMry7UlG0g %}
[https://youtu.be/qQMry7UlG0g](https://youtu.be/qQMry7UlG0g)





**Lars Dunemark**

---


---
*Imported from [Google+](https://plus.google.com/+LarsDunemark/posts/8hVaaroqtsX) &mdash; content and formatting may not be reliable*
