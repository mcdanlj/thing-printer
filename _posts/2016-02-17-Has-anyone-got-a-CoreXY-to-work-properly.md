---
layout: post
title: "Has anyone got a CoreXY to work properly?"
date: February 17, 2016 22:08
category: "Discussion"
author: Philip Acierto
---
Has anyone got a CoreXY to work properly?



I am trying to troubleshoot my gantry movement issues. When I try to move left and right along the X axis, I am also getting some unexpected Y movement forward and backwards.



I can post a video later, but curious if anyone has one working properly without having to do an custom work. I have found I needed to change the end stop behavior from the default CW and CCW of the X and Y, so I am not positive the default CoreXY config is solidified.



Am I alone in this endeavor?





**Philip Acierto**

---
---
**Hitbox Alpha** *February 17, 2016 22:10*

Flip the motor plug?


---
**Philip Acierto** *February 17, 2016 22:12*

I haven't actually. I wired them both the same, and they are both the same model stepper, so I would assume I wouldn't need to invert one but not the other, but I'll give that a shot.


---
**James Armstrong** *February 17, 2016 23:22*

I have. It was fun. 


---
**Philip Acierto** *February 17, 2016 23:23*

Thanks for the comment James. Glad to hear someone got it working.



Did it take any modifications to the config for you?


---
**James Armstrong** *February 17, 2016 23:27*

I had to make changes to the config. I'll boot it up and get a copy.  It would appear my stepper motor wires are on the same direction. 


---
**James Armstrong** *February 17, 2016 23:47*

[https://www.dropbox.com/s/qisbjknyfxwybv4/local.cfg?dl=0](https://www.dropbox.com/s/qisbjknyfxwybv4/local.cfg?dl=0)


---
**Philip Acierto** *February 17, 2016 23:53*

Thanks for the super fast response. I'll take a look at this and compare to what I have.



I haven't updated my redeem since the day I got it, and I have been editing the corexy.cfg file, so I plan to reflash everything back to original and leave my editing to the specified file (local.cfg).



So just to clarify, you have both X and Y stepper motors wired the same to the board, changes all included in this config, and you have working gantry movement?



If that is the case than anything else would be specific to my physical printer, and I'll direct my questions specific to any issues I can identify there. I appreciate the help in eliminating the issues from the software side of things.


---
**Philip Acierto** *February 19, 2016 18:55*

Alright, my movement is working now! Thanks a lot.



I reflashed my hardware, started a fresh local.cfg and started copying things over one by one.



Hope to be printing this weekend now!


---
**James Armstrong** *February 20, 2016 03:12*

Great. One thing about redeem that I like but can cause minor issues is that it is really flexible and highly configurable. 


---
*Imported from [Google+](https://plus.google.com/111974602640488998735/posts/FfgLd7HQHg5) &mdash; content and formatting may not be reliable*
