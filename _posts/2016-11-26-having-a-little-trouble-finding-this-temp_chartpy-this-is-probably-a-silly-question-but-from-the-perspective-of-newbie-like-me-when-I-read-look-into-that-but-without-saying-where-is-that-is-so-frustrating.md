---
layout: post
title: "having a little trouble finding this temp_chart.py this is probably a silly question but from the perspective of newbie like me when I read look into \"that\" but without saying where is \"that\" is so frustrating..."
date: November 26, 2016 23:49
category: "Discussion"
author: Step Cia
---
having a little trouble finding this temp_chart.py this is probably a silly question but from the perspective of newbie like me when I read look into "that" but without saying where is "that" is so frustrating...



naturally I went to wiki and bitbucket but still can't find this temp_chart.py can anyone point me to the right direction. Thx

![images/c61c6ca5721b3914d2f0c58ca174e43a.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/c61c6ca5721b3914d2f0c58ca174e43a.jpeg)



**Step Cia**

---
---
**Elias Bakken** *November 27, 2016 09:51*

Temp charts are outdated, look in the default.cfg for an explanation of how to use the Steihart-Hart algorithm!


---
**Step Cia** *November 27, 2016 10:02*

Yeah saw that algorithm. Was hoping I can avoid that route...﻿ but ok will give it a shot


---
**Step Cia** *November 28, 2016 05:20*

is there a short video about setting up thermistor and PID tuning guide?


---
**Step Cia** *November 28, 2016 06:12*

did the algorithm as best as I could. this is for Semitec 104NT-4-R025H42G thermistor. found the data sheet and plug in the R1T1 to R3T3. it spits the following calculated number. However, where do I go from here?

![images/dbf200638fa718747f0755b785ed9c95.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/dbf200638fa718747f0755b785ed9c95.jpeg)


---
**Elias Bakken** *November 28, 2016 13:57*

Step, is this a new thermistor that has not been used before? If so, we need to get it into mainline. The small hot end you are experimenting with looks like it is light weight, which is something that might be good for my Delta. Take a look at the temperature sensor file and see if you can decifer how to add your own sensor: [https://bitbucket.org/intelligentagent/redeem/src/8d6135603d203732658f239b4fa2304f1c4e73de/redeem/TemperatureSensorConfigs.py?at=master&fileviewer=file-view-default](https://bitbucket.org/intelligentagent/redeem/src/8d6135603d203732658f239b4fa2304f1c4e73de/redeem/TemperatureSensorConfigs.py?at=master&fileviewer=file-view-default)


---
**Step Cia** *November 29, 2016 07:16*

so Basically c1,c2 and c3 are A,B and C values respectively from the calculated steinhart model coefficient I don't know what is "r" value of 4700.



I tested the calculator with 104GT-2 data and the all 3 c values are accurately reproduced by the calculator so the A B C snap shot calculated above should be accurate 



can this Semitec 104NT-4-R025H42G thermistor be added to the list with above A B Cs number? or do you need anything else? thx


---
**Step Cia** *December 09, 2016 07:33*

Is this something that can be added to printer config?


---
**Elias Bakken** *December 09, 2016 07:35*

Yes, you should definitely add this to your printer.cfg. Do you mean to add the C values directly? There is no support for that yet, but there should be, at least for testing.


---
**Step Cia** *December 09, 2016 07:43*

Is there a guide on how to add this to printer config? Since this one is not listed, I assumed i will need to pick any listed one and modify its values in [default.cfg](http://default.cfg)?


---
**Elias Bakken** *December 09, 2016 18:53*

Since you are using a new thermistor that has not been added yet, you need to add it yourself. The only way to do that right now is to edit the file directly. The easiest is perhaps to use SSH/putty to log in to your BBB and edit the file directly. 


---
**Step Cia** *December 29, 2016 01:48*

So basically I need to SSH into BBB find  [TemperatureSensorConfigs.py](http://TemperatureSensorConfigs.py) and add a new line there give it a new name and the corresponding C1,2,3 values. Save and use this new name in my printer config? 


---
**Step Cia** *December 29, 2016 14:31*

I think I figured it out with some help of course :) [https://plus.google.com/+StephenCia/posts/4keNDJ5R8q9](https://plus.google.com/+StephenCia/posts/4keNDJ5R8q9)


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/hw2FVSMqMBy) &mdash; content and formatting may not be reliable*
