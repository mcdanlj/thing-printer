---
layout: post
title: "I wonder the further malfunctioning of the Z axis in my printer"
date: July 16, 2016 12:36
category: "Discussion"
author: Daniel Kusz
---
I wonder the further malfunctioning of the Z axis in my printer. I'll describe my setup Z axis:

1. Geometry is H-belt

2. Z axis is inverted (-1)

3. Z1 endstop is 'True' connected to the input endstop Z1 and is upper endstop

4. home_speed_z = -0.005 (homing goes to max. Endstop)



Is not that in this configuration endstop Z1 should be lower and the upper connected to Z2? I suggest a diagram, where the Z2 is connected to an inductive sensor instead of the normal upper endstop. Then it will be home_speed_z = 0.005. Is my configuration, the printer performs homing to last endstop instead of closer hotend and while printing moves the Z axis in the opposite direction?





**Daniel Kusz**

---
---
**Jon Charnas** *July 16, 2016 18:22*

normally you don't want a negative homing speed - if you have that you should invert the axis inversion and put a positive speed... Maybe that'll fix your problem?


---
**Jon Charnas** *July 16, 2016 18:23*

it's a matter of coordinate system, if you're going upside down but negative to your normal homing, you'll still go in the right direction, but Redeem will think you're going down to an endstop while you're going "up negatively", which isn't the same.


---
**Daniel Kusz** *July 16, 2016 22:48*

After my thoughts also came to the same conclusions. I think it was my fault for what deceived program. Tomorrow I go back to the Kamikaze 16.02, because the new Redeem or Kamikaze 1.1.0 has a problem with axes X and Y as previously written. It is a pity because the preheating works very cool, but wait for a stable version or Kamikaze 2. I would finally make the first printing and fine-tune the printer :-)


---
**Daniel Kusz** *July 18, 2016 07:17*

I set the Z axis not inverted (1) and home_speed_z = 0.005 and the printer behaves normally and simultaneously prints :) Only the buttons opposite effect so I wonder about reversing operation axis and setting homing to Redeem knew that directs the axis in the correct direction. I will put some movie form printing later. Thanks for help :)


---
*Imported from [Google+](https://plus.google.com/103041373788524358316/posts/eMRKpC4RcPc) &mdash; content and formatting may not be reliable*
