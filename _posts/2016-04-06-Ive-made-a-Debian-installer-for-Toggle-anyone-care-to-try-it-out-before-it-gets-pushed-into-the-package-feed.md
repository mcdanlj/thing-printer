---
layout: post
title: "I've made a Debian installer for Toggle, anyone care to try it out before it gets pushed into the package feed?"
date: April 06, 2016 16:59
category: "Discussion"
author: Elias Bakken
---
I've made a Debian installer for Toggle, anyone care to try it out before it gets pushed into the package feed? It exists as a downloadable package now: [https://bitbucket.org/intelligentagent/toggle/downloads](https://bitbucket.org/intelligentagent/toggle/downloads)





**Elias Bakken**

---
---
**Curt Page** *April 20, 2016 23:36*

I installed from Debian packages and now Toggle will not run.

error message:

04-20 23:30 root         CRITICAL   File "/usr/bin/toggle", line 9, in <module>



04-20 23:30 root         CRITICAL     

04-20 23:30 root         CRITICAL load_entry_point('Toggle==1.0.0', 'console_scripts', 'toggle')()



04-20 23:30 root         CRITICAL   File "build/bdist.linux-armv7l/egg/toggle/Toggle.py", line 197, in main



04-20 23:30 root         CRITICAL   File "build/bdist.linux-armv7l/egg/toggle/Toggle.py", line 123, in <i>_init_</i>



04-20 23:30 root         CRITICAL   File "build/bdist.linux-armv7l/egg/toggle/ModelLoader.py", line 33, in <i>_init_</i>



04-20 23:30 root         CRITICAL   File "build/bdist.linux-armv7l/egg/toggle/ModelLoader.py", line 61, in sync_models



04-20 23:30 root         CRITICAL   File "build/bdist.linux-armv7l/egg/toggle/RestClient.py", line 137, in get_list_of_files



04-20 23:30 root         CRITICAL TypeError

04-20 23:30 root         CRITICAL : 

04-20 23:30 root         CRITICAL get() takes exactly 1 argument (3 given)



Am I missing something?






---
**Elias Bakken** *April 21, 2016 01:17*

I might have forgotten to submit the latest package then. I'll do that tomorrow, sorry. Meanwhile you can add "params=data" to the functions in RestClient if you want a quickfix.


---
**Elias Bakken** *April 21, 2016 22:08*

**+Curt Page** could you try to update and install the latest? 1.0.1


---
**Curt Page** *April 22, 2016 00:12*

Thanks that did the trick.


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/MspeLBqihpt) &mdash; content and formatting may not be reliable*
