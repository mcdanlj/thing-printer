---
layout: post
title: "Hello, After ages, I am trying now to set up my Replicape Rev A on my beaglebone black"
date: July 03, 2016 19:59
category: "Discussion"
author: Franz Kainz
---
Hello,

After ages, I am trying now to set up my Replicape Rev A on my beaglebone black. Anyone experience, if the current Kamikaze image is compatible with Rev A, or does it only work with the newer Rev B version?

I flashed the BB with the current Kamikaze image (BBB-eMMC-flasher-kamikaze-2016-02-16-4gb.img.xz), and after installing and re-starting, the LEDs are flashing. However, I can't connect to the BB, so I wonder, if the startup script does not work on the Rev A version.

Sorry for the stupid question, I am really a beginner with the Replicape..

Any advise would be great!

thanks, Franz





**Franz Kainz**

---
---
**Daniel Kusz** *July 03, 2016 20:13*

Do you mean USB connection? Try Ethernet! On my Replicape rev B I cannot connect via USB :/ Only Ethernet and it works.


---
**Franz Kainz** *July 03, 2016 20:14*

Many thanks, I give it a try, currently, I am connected through USB!


---
**Daniel Kusz** *July 03, 2016 20:19*

Go to Windows net folder and there should be Octoprint Kamikaze Instance. It's nice to have the possibility to connect by USB.


---
**Franz Kainz** *July 03, 2016 20:30*

Hi Daniel, yes, connecting by USB would be great, since I currently do not have access to the router, and direct connection by Ethernet probably does not work. So quick question on your proposal: is it a specific software, I need to download? Thanks!


---
**Franz Kainz** *July 03, 2016 20:32*

not sure, where to find Octoprint Kamikaze


---
**Franz Kainz** *July 03, 2016 20:39*

oh I see, need to install Octoprint first..


---
**Daniel Kusz** *July 03, 2016 20:40*

I only bought a cheap Ethernet switch and put ethernet cable form switch to Replicape - this suggests Elias on his wiki page. Probably direct connection doesn't work. Maybe you can connect ethernet cable from router and have a PC in the same net but i I have not tested this.


---
**Daniel Kusz** *July 03, 2016 20:43*

**+Franz Kainz** Look into windows net folder where you have other network devices...


---
**Franz Kainz** *July 03, 2016 20:44*

Many thanks, I'll give it a try!


---
**Elias Bakken** *July 04, 2016 19:06*

If you flash a rev A4 with latest Kamikaze, it overwrites the EEPROM with that of rev B3. You have to manually flash it back to A4. Instructions are on the rev a wiki page.


---
**Franz Kainz** *August 20, 2017 10:36*

Hi Elias,

A year later, I give it a try again. Quick question on your advice, can I flash the BBB with the Rev A4 attached and then, once everything is done, I need to log in through ssh and use the following commands?



Debian

On the Debian/kamikaze image:



 sudo apt-get install nodejs-legacy

 wget [https://bitbucket.org/intelligentagent/replicape/raw/f623a0304134b3effcc427a82de1ebbf7cee82bb/eeprom/replicape_0A4A.json](https://bitbucket.org/intelligentagent/replicape/raw/f623a0304134b3effcc427a82de1ebbf7cee82bb/eeprom/replicape_0A4A.json)

 wget [https://bitbucket.org/intelligentagent/replicape/raw/a2b195880014c7c27aabd7c0a8d140c5564007ad/eeprom/eeprom.js](https://bitbucket.org/intelligentagent/replicape/raw/a2b195880014c7c27aabd7c0a8d140c5564007ad/eeprom/eeprom.js)

 wget [bitbucket.org - if(typeof exports === 'undefined') exports = {}; exports.bone = { "P8_1 ...](https://bitbucket.org/intelligentagent/replicape/raw/a2b195880014c7c27aabd7c0a8d140c5564007ad/eeprom/bone.js)

 node ./eeprom.js -w replicape_0A4A.json

 cat Replicape.eeprom > /sys/bus/i2c/drivers/at24/2-0054/nvmem/at24-1/nvmem



I am asking since I cannot login through ssh after the flash, and I believe the board gets stuck. Do I need to have the Replicape Rev A4 attached during the flashing, or is this only optional?



many thanks,

Franz


---
**Franz Kainz** *August 20, 2017 10:40*

So some more information, the flashing seems to work, but then when I remove the SD card and plug it in again, only the first 2 LEDs (1 & 2) light up and nothing happens any more. I feel it might have to do that the Rev A4 is no longer compatible with the Replicape, could this be the case? so the question is, when shall I install the old version. thanks!


---
**Franz Kainz** *August 20, 2017 10:51*

when I try to connect to the board through SSH or ping (through Ethernet and the IP address), I do not get a response. So I feel the startup procedure of the latest image does not work with the "old" RevA4. Any ideas of a workaround, or shall I get started with a pure Debian version, flash the BBB and then install the packages manually?


---
**Elias Bakken** *August 29, 2017 17:39*

Franz, any chance I can get you to go on [Replicape.slack.com](http://Replicape.slack.com)?  We see more people hanging out there and should be able to help out more easily, invitation here: [https://www.thing-printer.com/wp-login.php?action=slack-invitation](https://www.thing-printer.com/wp-login.php?action=slack-invitation)[replicape.slack.com - Slack](http://Replicape.slack.com)


---
**Franz Kainz** *August 30, 2017 10:10*

Hi Elias,

Sure, I signed up for Slack! 

My challenge related that my BeagleBone Black was that I am using an older version with 2GB, and therefore the flashing did not work with the older Kamikaze image. With the newest Kamikaze image, the 2GB is actually fine and sufficient, and therefore I got it to work. So in principle, the board is installed now correctly, and the next step is the wiring, so let's see..

Frankly, I am just getting into Linux, and therefore I am pretty slow progressing.. but, it's fun!

best, Franz


---
**Elias Bakken** *August 30, 2017 10:12*

Hehe, well, you have your work cut out for you then! Linux is the way of the future though, so you are on your way to greatness : )


---
*Imported from [Google+](https://plus.google.com/+FranzKainz/posts/74xQkjwQVzk) &mdash; content and formatting may not be reliable*
