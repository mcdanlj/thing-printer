---
layout: post
title: "I have a question related to cura 2.3.1"
date: December 07, 2016 21:05
category: "Discussion"
author: Daniel Kusz
---
I have a question related to cura 2.3.1. Do you use the slicer for printing on Replicape? If so, did you happen to be when printing larger models motors lose steps and layers are shifted? For me, this often happens when they reach high travel speed (130mm / sec) and I suppose that this is related to the settings of speed, acceleration and jerk. What qualities do you use in your Replicape and from slicer when printing? Are you setting these values in Cura? Engines have approx. 0.5Nm 1.5A - in Redeem set 1A. I would use Cura, because it has good for me, interface and capabilities. My settings below:



max_speed_x = 0.135

max_speed_y = 0.135

max_speed_z = 0.004

acceleration_x = 1.5

acceleration_y = 1.5

max_jerk_x = 0.015

max_jerk_y = 0.015

max_jerk_z = 0.04

max_speed_e = 0.2

max_speed_h = 0.2





**Daniel Kusz**

---
---
**Jon Charnas** *December 07, 2016 21:15*

You mean you upload the sliced gcode to redeem, right? I've not noticed that very much no. The only times it happened to me was because the clamps holding down my build plate slipped... Tightened the fit since and never seen it again, but I use a delta and I've tuned it over two years now


---
**Daniel Kusz** *December 07, 2016 21:22*

Yes, I upload the gcode from Cura 2.3.1. Sometimes I have big shifts so I think that is not happened because of clamps. Small prints are very good.


---
**Step Cia** *December 07, 2016 22:31*

Did you point a fan to cool the TMC2100?


---
**Daniel Kusz** *December 08, 2016 04:21*

**+Step Cia** I have two 40x40 fans and heatsinks on TMC's but under the printer... 

**+Frederic M.** Ok, as you know 1.2A is maksimum and I will try it. Can you recommend some steppers? What steppers do you have? 


---
**Daniel Kusz** *December 08, 2016 05:09*

Are you using silent mode/stealthchop? If not what steppers do you have? 


---
**Daniel Kusz** *December 08, 2016 14:24*

I need to try printing without stealthchop. Maybe that is the key. 


---
**Jon Charnas** *December 08, 2016 14:35*

It really depends on how much torque you really need! I've been using stealthchop on my delta and never had an issue with it. There is one other factor that could influence the torque you get, which is the stepper delay. There was some discussion about this in the #support channel on the Replicape slack earlier this week (or last weekend?).


---
**Daniel Kusz** *December 08, 2016 16:58*


{% include youtubePlayer.html id=g6Bxoqr8QlY %}
[youtube.com - Honest pre-review: The all-new Trinamic TMC2100 stepper drivers!](https://www.youtube.com/watch?v=g6Bxoqr8QlY)



Thomas has similar results like me when tested these drivers. Unfortunately I have this pitchy noise. About torque I need stepper with min 0,5Nm on 1.2A. In most cases 05Nm is enough for small 3d printer.


---
**Daniel Kusz** *December 09, 2016 17:33*

**+Jon Charnas** 24V PSU puts 24V on steppers?


---
**Daniel Kusz** *December 09, 2016 17:48*

Thanks a lot! I wanted to confirm this. So solution for me should be 24v PSU for better stepper performance.


---
**Step Cia** *December 09, 2016 18:02*

can one draw a generalization that 12V power supply is more prone to cause loose steps than 24v?


---
**Daniel Kusz** *December 16, 2016 19:54*

My mistake was using 12V PSU, but I've done many good small and medium size prints on this set up (bowden on Hbot, 20x20 heated bed). Problems are with big, long prints and fast travels. Now I've bought 24V PSU and waiting only for 24V hotend heater.


---
**Daniel Kusz** *December 20, 2016 17:39*

Today I ran the printer to 24V and on spread cycle mode engines still produce sounds of high frequency. 24V should address this sound, but unfortunately that did not happen. I'll do a few test prints still in the mode Stealthchop. Do you have any more ideas?


---
**Daniel Kusz** *December 20, 2016 18:28*

I have three different types of motors: 1.3A for Z, 1.5A for X and Y, 1.2A for extruders. My stepper current settings are  1.1A for X and Y, 0.9 for Z and extruders. I tried 0.3A with no positive effect on X and Y.


---
*Imported from [Google+](https://plus.google.com/103041373788524358316/posts/4w5cYmLRV1s) &mdash; content and formatting may not be reliable*
