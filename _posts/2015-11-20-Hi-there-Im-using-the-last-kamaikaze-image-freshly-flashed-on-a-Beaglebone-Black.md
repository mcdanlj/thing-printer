---
layout: post
title: "Hi there! I'm using the last kamaikaze image freshly flashed on a Beaglebone Black"
date: November 20, 2015 19:24
category: "Discussion"
author: Francois Muelle
---
Hi there!



I'm using the last kamaikaze image freshly flashed on a Beaglebone Black. I'm encountering two problems, maybe related, the biggest being redeem won't start:



<i>debian@kamikaze:/home/octo$/usr/bin/redeem</i>

<i>Traceback (most recent call last):</i>

  <i>File "/usr/bin/redeem", line 9, in <module></i>

    <i>load_entry_point('Redeem==1.0.4', 'console_scripts', 'redeem')()</i>

  <i>File "/usr/lib/python2.7/dist-packages/pkg_resources.py", line 356, in load_entry_point</i>

    <i>return get_distribution(dist).load_entry_point(group, name)</i>

  <i>File "/usr/lib/python2.7/dist-packages/pkg_resources.py", line 2476, in load_entry_point</i>

    <i>return ep.load()</i>

  <i>File "/usr/lib/python2.7/dist-packages/pkg_resources.py", line 2190, in load</i>

    <i>['__name_</i>'])_

  <i>File "/usr/lib/python2.7/dist-packages/redeem/Redeem.py", line 40, in <module></i>

    <i>from Stepper import Stepper, Stepper_00A3, Stepper_00A4, Stepper_00B1, Stepper_00B2</i>

  <i>File "/usr/lib/python2.7/dist-packages/redeem/Stepper.py", line 28, in <module></i>

    <i>from ShiftRegister import ShiftRegister</i>

  <i>File "/usr/lib/python2.7/dist-packages/redeem/ShiftRegister.py", line 38, in <module></i>

    <i>spi.bpw = 8</i>

<i>AttributeError: 'NoneType' object has no attribute 'bpw'</i>



I googled this error with no luck, and... I'm stuck.



Any help would much appreciated. Thanks in advance.





**Francois Muelle**

---
---
**Elias Bakken** *November 20, 2015 19:30*

Hi, this happens if you do not have Replicape installed. If you want to get further with this, you should have a look at faking the SPI part i think.


---
*Imported from [Google+](https://plus.google.com/110879606286009280583/posts/eUmxnXNxZPN) &mdash; content and formatting may not be reliable*
