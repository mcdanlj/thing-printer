---
layout: post
title: "Hi guys, I've got a question that I think might be of interest to a few others out there"
date: May 04, 2016 21:28
category: "Discussion"
author: Jon Charnas
---
Hi guys, I've got a question that I think might be of interest to a few others out there. I've got a Mini kossel (standard openbeam size, 17cm build plate), and I don't want to lose build volume by mounting an extra fan on the effector. So I've got those 120mm fans blowing air at the plate from the side. Works really well to maintain temps for PLA.



I'm now trying to print Colorfabb XT (at 260C, up from 205C for PLA). My hotend can't seem to hold the additional temp properly. I was wondering if there's a way to limit the strength of the cooling fan set with M106 based on hotend temp? (kind of a negative feedback like you'd see on the cold-end) The idea being this only becomes active once M106 is issued...



Is this possible or do I need to go tweak the code a bit to allow for a dynamic part fan speed?





**Jon Charnas**

---
---
**Elias Bakken** *May 04, 2016 21:34*

You can connect a thermistor to a fan and have it try to maintain a temperature. Typically this is used to start a fan one a hit end goes above 60 degrees or similar. You can also connect a fan to a cold end monitor, but they only go up to 125 deg.


---
**Jon Charnas** *May 04, 2016 21:48*

That would sound perfectly feasible if I was always printing at the same temp - but the idea I had was that it would always adapt around the targeted M109 temperature... So if it sees that after enabling or increasing fan power through M106 it automatically reduces the fan power as long as temp is decreasing while the extruder is on continuous power to heat up (this is assuming that no alarm trips the heater of course)


---
**Elias Bakken** *May 04, 2016 21:51*

Ah, ok, that functionality is not implemented no. Would it help to be able to set the cold end temperature with a G-code?


---
**Jon Charnas** *May 04, 2016 21:57*

Uh, not in this case. The thing is that my hotend temp drops (while the cold end is plenty cooled, E3D v6 fan takes care of it) - it's the part cooling fan I'd like to be able to throttle down dynamically. I'll try by tweaking slicer settings on the cooling fan parameters, but the idea was that if the hotend can't maintain the temp within a reasonable bracket (say 5 degrees of target temp) the fans can either spin up or down to account for the cooling/heating ability of the hotend. This might be particularly useful if a printer has a weak power supply.


---
**Jon Charnas** *May 04, 2016 21:57*

(and a cold-end sensor, to avoid damage)


---
**Daryl Bond** *May 05, 2016 00:36*

How about the following?



while (printing):

    accumulated_time = 0.0

    while (hotend at max power) & (temp. < allowed range):

        sleep(0.1)

        accumulated_time += 0.1

        if (accumulated_time >= threshold):

            fan_pwm *= scale_factor

            accumulated_time = 0.0

    

It's pretty rough but the general concept is there.


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/1uZvvZKqoeD) &mdash; content and formatting may not be reliable*
