---
layout: post
title: "On flashing procedure, I didn't hit the \"boot\" button but after applying power it continue to do the \"knight rider\" light..."
date: August 09, 2016 15:55
category: "Discussion"
author: Step Cia
---
On flashing procedure, I didn't hit the "boot" button but after applying power it continue to do the "knight rider" light... Is holding boot really necessary?





**Step Cia**

---
---
**Elias Bakken** *August 09, 2016 16:07*

No


---
**Elias Bakken** *August 09, 2016 16:08*

It's just for loading the boot loader on the ad card which disables the steppes during flashing, so that you do not have to hear that annoying sound from the steppers.


---
**Step Cia** *August 09, 2016 17:32*

Thx I don't mind the stepper sounds :)


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/3cLDwdWcNTv) &mdash; content and formatting may not be reliable*
