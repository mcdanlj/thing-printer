---
layout: post
title: "Hello I made an update with apt-get update and apt-get upgrade"
date: June 15, 2016 19:32
category: "Discussion"
author: Francois Muelle
---
Hello I made an update with apt-get update and apt-get upgrade. Now I can't run octoprint.

I need some help . thanks

![images/6069afbaee0ebeb7fc1d83735bdb7420.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/6069afbaee0ebeb7fc1d83735bdb7420.jpeg)



**Francois Muelle**

---
---
**Elias Bakken** *June 15, 2016 20:22*

Are you running a very old version of Kamikaze? What does 

cat /etc/dogtag

say? 




---
**Francois Muelle** *June 15, 2016 20:36*



Kamikaze Debian Jessie Image 2016-01-22




---
**Elias Bakken** *June 16, 2016 00:52*

Ok, you should probably upgrade to the latest version which has some of the packages related to upgrading OctoPrint freezed so this does not happen! Take a backup of your local.cfg and flash again. The alternative is a really long explanation:)


---
**James Armstrong** *June 16, 2016 14:35*

Also, I found when updating that if any new configuration options are added but not added to the default.cfg or local.cfg then that is one of the main reasons for crashing. It will show on the output if you run redeem from the prompt.


---
*Imported from [Google+](https://plus.google.com/110879606286009280583/posts/KG35D4FS6b2) &mdash; content and formatting may not be reliable*
