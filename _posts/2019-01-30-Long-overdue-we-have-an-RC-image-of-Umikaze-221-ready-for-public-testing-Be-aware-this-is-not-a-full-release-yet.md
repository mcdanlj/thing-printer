---
layout: post
title: "Long overdue, we have an RC image of Umikaze 2.2.1 ready for public testing: Be aware, this is not a full release yet"
date: January 30, 2019 20:52
category: "Discussion"
author: Jon Charnas
---
Long overdue, we have an RC image of Umikaze 2.2.1 ready for public testing: [https://github.com/intelligent-agent/Umikaze/releases/tag/2.2.1-RC2](https://github.com/intelligent-agent/Umikaze/releases/tag/2.2.1-RC2)



Be aware, this is not a full release yet. We're close, but we're hoping that a few fearless dragon hunters amongst you will find the few bugs we haven't squashed yet. 



If you need your printer for production - please consider disabling the flasher, and testing from onboard the SD card so you have a stable image to go back to with little hassle. Instructions on how to do this are on the release page.



IMPORTANT: Toggle is still undergoing changes to function with OctoPrint 1.3.10+ - if you use the touch screen interface, do not upgrade OctoPrint past 1.3.9 yet. We should have Toggle fixed by the time RC3 drops in a couple of weeks.



As usual, please submit any bug reports directly on Github!





**Jon Charnas**

---
---
**ThantiK** *January 31, 2019 19:57*

Great work! 


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/i1vGfM7euGD) &mdash; content and formatting may not be reliable*
