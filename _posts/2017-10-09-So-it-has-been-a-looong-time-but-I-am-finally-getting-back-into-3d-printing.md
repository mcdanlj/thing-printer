---
layout: post
title: "So it has been a looong time, but I am finally getting back into 3d printing"
date: October 09, 2017 13:17
category: "Discussion"
author: Daryl Bond
---
So it has been a looong time, but I am finally getting back into 3d printing. Unfortunately I've hit a problem. Powering up the Replicape for the first time in well over a year and I am getting "Error accessing 0x70: Check your I2C address". Note that I have power to the replicape so it isn't that. I also have all heaters plus three of the fans permanently powered on as shown in the image. I have nothing connected to the Replicape except for power (24V) and I am running the latest Umikaze image. Please tell me I don't have a bricked board....

![images/d77710fef1407b53644408d58adc9d6f.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/d77710fef1407b53644408d58adc9d6f.jpeg)



**Daryl Bond**

---
---
**Jon Charnas** *October 09, 2017 13:26*

Hi Daryl, from what I'm making out on your board you have one of the B3 boards, correct? The I2C address problem can come from 2 main problems. One is one of the pins isn't seated properly and actually folded during insert (it's annoying to check and fix, but nothing major, no damage done).



The other thing that's possible is that you need to reflash the EEPROM, follow the instructions here: [wiki.thing-printer.com - Replicape - Thing-Printer](http://wiki.thing-printer.com/index.php?title=Replicape#Update_EEPROM) (following the Debian instructions)


---
**Jon Charnas** *October 09, 2017 13:28*

If neither one of the actions above fix the issue, we'll have to dig a bit further with logs etc. Joining us on the Replicape slack will help cut down on the feedback time :) (self-invite link is on [thing-printer.com - Front page - Thing-Printer.com](http://thing-printer.com) under the Support menu) 


---
**Daryl Bond** *October 09, 2017 13:43*

Thanks, I've joined up to slack so I'll communicate my progress there...


---
*Imported from [Google+](https://plus.google.com/104347555452272558593/posts/eUnkr6mx51S) &mdash; content and formatting may not be reliable*
