---
layout: post
title: "More documentation of things I have noticed"
date: July 09, 2016 23:57
category: "Discussion"
author: Alexander Borsi
---
More documentation of things I have noticed.



In the section of setting up Octoprint, there is a part where the commands to be typed, and the physical keys to be pressed are not clearly defined.



[http://wiki.thing-printer.com/index.php?title=Kamikaze#System_commands](http://wiki.thing-printer.com/index.php?title=Kamikaze#System_commands)



The CTL+D looks like it should just be pasted into the shell, when it means to exit/quit the cat command.



And the whitespace is slightly wrong, leading to the command not working. The one that works for me is:



system:

  actions:

  - action: Reboot Octorint

    command: sudo systemctl restart octoprint.service

    name: Reboot Octorint

  - action: Reboot

    command: sudo shutdown -r now

    name: Reboot

  - action: Shutdown

    command: sudo shutdown -h now

    name: Shutdown



and the one on the wiki is



 system:

 actions:

 - action: Reboot Octorint

   command: sudo systemctl restart octoprint.service

   name: Reboot Octorint

 - action: Reboot

   command: sudo shutdown -r now

   name: Reboot

 - action: Shutdown

   command: sudo shutdown -h now

   name: Shutdown



And it even locked up my octoprint for a while. I edited this all in nano, which worked just fine.





**Alexander Borsi**

---
---
**Jon Charnas** *July 10, 2016 08:35*

Ok I fixed the whitespace issue on the wiki page. It's a bit tricky as the space indentations actually make a difference.


---
*Imported from [Google+](https://plus.google.com/102644518510093053332/posts/XApdkAy2iWZ) &mdash; content and formatting may not be reliable*
