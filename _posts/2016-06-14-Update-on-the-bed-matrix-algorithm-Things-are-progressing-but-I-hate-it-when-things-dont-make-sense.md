---
layout: post
title: "Update on the bed matrix algorithm: Things are progressing, but I hate it when things don't make sense!"
date: June 14, 2016 01:21
category: "Discussion"
author: Elias Bakken
---
Update on the bed matrix algorithm: Things are progressing, but I hate it when things don't make sense! Such as getting pretty decent results using a quarter of the bed level matrix... Can anyone help? 





![images/2017c37ed5b9f38dbdb047911e5dbcbe.png](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/2017c37ed5b9f38dbdb047911e5dbcbe.png)



**Elias Bakken**

---
---
**Ryan Carlyle** *June 14, 2016 04:50*

What kind of leveling are you trying to do?


---
**Elias Bakken** *June 14, 2016 12:39*

First off just to adjust the Z-coordinates so that the object is built with the Z-coordinates on the bed but with X and Y staying like the ordiginal. This is to verify that the probe and the probing routine is 

working properly. 



What I have now is a way to probe (G29) which gives consistent results and shows a bed at a slight angle with around 3mm difference between highest and lowest point. Plotting that data (red circles in the picture) shows that the data is in fact on a plane. 



Secondly I calculate the rotation matrix using this formula: [http://math.stackexchange.com/a/476311](http://math.stackexchange.com/a/476311) 

and dot the same (X, Y, 0) coordinates with the found matrix. That gives the orange stars in the picture. 



Then, to verify the results, I run the same G29, only without resetting the rotation matrix and plot those results in the same diagram. I would then expect that the new probe data would be very close to 0 across the bed (blue circles). Instead, without adjusting the rotation matrix, the verification probe data shows the data lying more or less in exactly the opposite plane to the probe data. 



The green points in the diagram is the expected position of the verification data by using R*0.25 + I*0.75, IE cutting the values in R by 4. 


---
**Ryan Carlyle** *June 14, 2016 13:12*

Are you just wanting to do bed tilt correction, or also auto-calibrate? The state of the art right now is the dc42 autocalibration routine in RepRapFirmware. It's pretty close to magical. Gets endstop offsets, tower offsets, arm length, etc. Don't know how hard it would be to port though. 


---
**Elias Bakken** *June 14, 2016 13:47*

Yeah, someone actually did implement something similar to that, but without having the basic stuff nailed down, it difficult to move forward. Looks like the probes distance might be doubled in the current implementation. That would explain a lot!


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/h69t52jtBLY) &mdash; content and formatting may not be reliable*
