---
layout: post
title: "If anyone is interested, I have hacked together a quick and dirty python script to generate scaled ui layouts for toggle based on an existing layout"
date: May 04, 2016 10:49
category: "Discussion"
author: Daryl Bond
---
If anyone is interested, I have hacked together a quick and dirty python script to generate scaled ui layouts for toggle based on an existing layout. You can find it at:



 [http://pastebin.com/raw/AwChX0Mk](http://pastebin.com/raw/AwChX0Mk)



I'm not sure if any of the other the tabs are good (I am unable to navigate without a touch screen) but I thought it may be useful to someone.



I found that a size of 1320x1080 worked well for me on a non-rotated 1920x1080 screen where the BBB is cropping the screen to some other resolution.





![images/149df2667787f26e4ed7cdb9e8c23e29.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/149df2667787f26e4ed7cdb9e8c23e29.jpeg)



**Daryl Bond**

---
---
**Elias Bakken** *May 04, 2016 11:41*

Haha! Awesome!


---
**Jon Charnas** *May 04, 2016 21:04*

Nice! Are you going to have a mouse or get an apple bluetooth touchpad to interact with Toggle?


---
**Elias Bakken** *May 04, 2016 21:16*

Needs a bigger platform though!


---
**Daryl Bond** *May 04, 2016 22:26*

I have a bigger platform that I use for PLA. I use this platform for ABS because it heats up quicker. 24V x 26A = 624W! 


---
**Daryl Bond** *May 04, 2016 22:27*

I have a wireless mouse so I'm looking into using that.


---
**Elias Bakken** *May 04, 2016 22:33*

I was talking about the platform on the screen, but now that you mention it, your physical platform is a bit small as well! Did you try implementing a soft mouse yet? I would say try a stage.connect("motion-event", self.mouse_move) and take it from there. Just start with a red square as a MVP:)


---
**Daryl Bond** *May 04, 2016 23:57*

Haha! Yes, the platform on the screen is too small as well! Another thing on the to-do list. I haven't gotten around to the soft mouse implementation yet either. Having a 5 month old child takes up a lot of my time :) Thanks for the suggestion on where to start though.


---
*Imported from [Google+](https://plus.google.com/104347555452272558593/posts/RMycbNR6VEL) &mdash; content and formatting may not be reliable*
