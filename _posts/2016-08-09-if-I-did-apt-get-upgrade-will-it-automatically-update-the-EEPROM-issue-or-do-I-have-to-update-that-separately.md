---
layout: post
title: "if I did \"apt-get upgrade\" will it automatically update the EEPROM issue or do I have to update that separately?"
date: August 09, 2016 17:57
category: "Discussion"
author: Step Cia
---
if I did "apt-get upgrade" will it automatically update the EEPROM issue or do I have to update that separately?





**Step Cia**

---
---
**Elias Bakken** *August 09, 2016 18:04*

You need to fix that separately, but if you had your cape on during flashing, it should have been fixed


---
**Step Cia** *August 09, 2016 19:47*

Thanks I had the cape on the whole time. 


---
*Imported from [Google+](https://plus.google.com/+StephenCia/posts/ECtfGYTZHKM) &mdash; content and formatting may not be reliable*
