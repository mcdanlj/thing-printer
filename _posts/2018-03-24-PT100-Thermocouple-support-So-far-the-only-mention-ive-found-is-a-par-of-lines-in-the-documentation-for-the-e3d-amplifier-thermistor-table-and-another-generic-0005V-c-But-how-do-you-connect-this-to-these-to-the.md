---
layout: post
title: "PT100/Thermocouple support? So far the only mention i've found is a par of lines in the documentation for the e3d amplifier \"thermistor\" table, and another generic 0.005V/c But, how do you connect this to these to the"
date: March 24, 2018 04:19
category: "Discussion"
author: Javier Murcia
---
PT100/Thermocouple support?



So far the only mention i've found is a par of lines in the documentation for the e3d amplifier "thermistor" table, and another generic 0.005V/cº



But, how do you connect this to these to the replicape? Certanily not as a thermistor, and if i'm not mistaken, beaglebone ADC inputs only support up to 1.8v, so also its a no-go.



Any ideas?





**Javier Murcia**

---
---
**Jon Charnas** *March 24, 2018 09:37*

There was a discussion I didn't really follow in the slack, about installing a voltage divider to not fry the BBB underneath. Take a look at the Replicape slack archives maybe?


---
*Imported from [Google+](https://plus.google.com/+JavierMurciaDiaz/posts/R68WDnHDykk) &mdash; content and formatting may not be reliable*
