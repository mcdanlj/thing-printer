---
layout: post
title: "I've spent the weekend working with Mathieu and integrating his work on the PRU into the main branch"
date: March 09, 2014 19:26
category: "Discussion"
author: Elias Bakken
---
I've spent the weekend working with Mathieu and integrating his work on the PRU into the main branch. The firmware now gets compiled on demand which is great for development and allows endstop inversion to be changed in the config file. The G28 command is also updated so homing should work better now. yay!

![images/b791cf74acbb5a21b62214b3b3e97ca2.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/b791cf74acbb5a21b62214b3b3e97ca2.jpeg)



**Elias Bakken**

---


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/HXYWLimEWgy) &mdash; content and formatting may not be reliable*
