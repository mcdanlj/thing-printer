---
layout: post
title: "note quite the 20mm cube I was hoping for, but it was my printer's first extrusion"
date: July 06, 2016 02:26
category: "Discussion"
author: Philip Acierto
---
note quite the 20mm cube I was hoping for, but it was my printer's first extrusion.

![images/2b455a9ebfe697e79c1870c05431218b.jpeg](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/2b455a9ebfe697e79c1870c05431218b.jpeg)



**Philip Acierto**

---
---
**Daniel Kruger** *July 06, 2016 05:04*

It's progress


---
**Elias Bakken** *July 06, 2016 09:12*

Hehe, room for improvement:) Check your extruder steps_pr_mm_e, looks like under extrusion.


---
**Philip Acierto** *July 06, 2016 09:20*

extruder steps per mm was off by ~10mm, I also switched to a mechanical Z and servo arm old school style, see if that fixes my auto bed leveling.


---
*Imported from [Google+](https://plus.google.com/111974602640488998735/posts/imWbheSvSW8) &mdash; content and formatting may not be reliable*
