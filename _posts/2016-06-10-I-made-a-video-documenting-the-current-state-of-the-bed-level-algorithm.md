---
layout: post
title: "I made a video documenting the current state of the bed level algorithm"
date: June 10, 2016 23:42
category: "Discussion"
author: Elias Bakken
---
I made a video documenting the current state of the bed level algorithm. It appears broken to me, not sure when that happened or if it has ever worked exactly as it should. Either way this should serve as a starting point! 

![images/432a5f858187fd124c59d8c3ff7d852c.gif](https://gitlab.com/mcdanlj/thing-printer/raw/master/images/432a5f858187fd124c59d8c3ff7d852c.gif)



**Elias Bakken**

---
---
**Elias Bakken** *June 12, 2016 08:16*

Turns out the probing and matrix are both ok, only the testing that has the error:)


---
**Elias Bakken** *June 12, 2016 08:29*

No audio?


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/TdH9oyCXmDj) &mdash; content and formatting may not be reliable*
