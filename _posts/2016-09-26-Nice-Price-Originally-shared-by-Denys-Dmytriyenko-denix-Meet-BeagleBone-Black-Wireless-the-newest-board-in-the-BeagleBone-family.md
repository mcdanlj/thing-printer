---
layout: post
title: "Nice! Price? Originally shared by Denys Dmytriyenko (denix) Meet BeagleBone Black Wireless, the newest board in the BeagleBone family"
date: September 26, 2016 22:40
category: "Discussion"
author: Elias Bakken
---
Nice! Price?



<b>Originally shared by Denys Dmytriyenko (denix)</b>



Meet BeagleBone™ Black Wireless, the newest board in the BeagleBone™ family





**Elias Bakken**

---
---
**Philip Acierto** *September 27, 2016 04:41*

$68.75 USD apparently



[http://www.mouser.com/search/ProductDetail.aspx?R=0virtualkey0virtualkeyBBBWL-SC-562](http://www.mouser.com/search/ProductDetail.aspx?R=0virtualkey0virtualkeyBBBWL-SC-562)


---
**Jon Charnas** *September 27, 2016 05:57*

That's a bit pricey, no?


---
**Elias Bakken** *September 27, 2016 12:19*

Yeah it's pricey. That MIMO Wi-Fi chip can be fun to play around with for an access point! 


---
**Pieter Koorts** *September 27, 2016 22:21*

For $68 they could have at least put in 1GB ram within that SOC. That extra buffer would help a lot sometimes.


---
**Jon Charnas** *January 09, 2017 19:09*

At the moment there are difficulties getting kamikaze to run on it. I know of at least one person who has one and ran into issues with the Linux kernel and overlay not working with the wifi as expected


---
**Sabin Iacob** *January 14, 2017 22:44*

am I the only one who got it fully working so far? :D 



I spammed the heck out of^W^W^W^W^W reported progress on

[https://github.com/goeland86/Kamikaze2/issues/20](https://github.com/goeland86/Kamikaze2/issues/20); I guess it will warrant a device-specific image eventually: won't boot with 4.1 and the proper dtb (needs 4.4 kernel), SPI2 is called SPI1 (needs systemd and udev changes), some boards - like mine, which arrived 2 days ago - are missing the board revision from the EEPROM so they don't get autodetected as BBBW, etc.

[github.com - BBB Wireless not supported · Issue #20 · goeland86/Kamikaze2](https://github.com/goeland86/Kamikaze2/issues/20)


---
**Jon Charnas** *January 16, 2017 08:05*

As far as I know you are, **+Sabin Iacob**. But thanks to your testing we'll hopefully have BBBW supported soon. I'd argue that the fix for SPI devices should make it into the main redeem code - it doesn't break anything and avoids having to mess with the linux image too much. **+Elias Bakken** thoughts on that?


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/fJ6NtDu2WEk) &mdash; content and formatting may not be reliable*
