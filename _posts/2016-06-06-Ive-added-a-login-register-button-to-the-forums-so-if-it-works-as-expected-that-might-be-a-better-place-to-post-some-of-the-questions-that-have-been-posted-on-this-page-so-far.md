---
layout: post
title: "I've added a login/register button to the forums, so if it works as expected, that might be a better place to post some of the questions that have been posted on this page so far"
date: June 06, 2016 16:34
category: "Discussion"
author: Elias Bakken
---
I've added a login/register button to the forums, so if it works as expected, that might be a better place to post some of the questions that have been posted on this page so far. If you created an account during the checkout, you can use that account, or you can create a new account if you have none. 

[https://www.thing-printer.com/forums/](https://www.thing-printer.com/forums/)





**Elias Bakken**

---
---
**Daniel Kusz** *June 06, 2016 16:36*

Great info. Thanks!


---
**Andy Tomascak** *June 19, 2016 01:09*

Awesome, thanks Elias! :)


---
*Imported from [Google+](https://plus.google.com/+EliasBakken/posts/9C3KVsDeGwZ) &mdash; content and formatting may not be reliable*
