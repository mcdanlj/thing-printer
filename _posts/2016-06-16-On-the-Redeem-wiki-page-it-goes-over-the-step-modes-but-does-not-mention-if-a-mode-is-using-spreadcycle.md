---
layout: post
title: "On the Redeem wiki page it goes over the step modes but does not mention if a mode is using spreadcycle"
date: June 16, 2016 23:38
category: "Discussion"
author: Chris Romey
---
On the Redeem wiki page it goes over the step modes but does not mention if a mode is using spreadcycle.  Do any of the modes use this? 





**Chris Romey**

---
---
**Elias Bakken** *June 17, 2016 11:21*

As far as I remember all modes except the last two use spreadcycle. The last two use stealthchop.


---
**Chris Romey** *June 17, 2016 15:55*

Excellent. Thank you!


---
*Imported from [Google+](https://plus.google.com/113335787317667734340/posts/MeX45tY7oeJ) &mdash; content and formatting may not be reliable*
