---
layout: post
title: "Public Service Announcement: If you're using Slic3r (specifically the Prusa edition) and you're seeing very slow prints..."
date: June 27, 2017 14:08
category: "Discussion"
author: Jon Charnas
---
Public Service Announcement:



If you're using Slic3r (specifically the Prusa edition) and you're seeing very slow prints... Check your redeem logs, you'll see them full of exceptions.



The fault lies with M204 - Redeem does not yet support it.



If you can, disable the default setting using M204 to control print speeds, and your print times should be closer to the estimates parsed by Octoprint. One user reported his print times being 3-4 times what they should've been, if not more, due to this.



Developers are hard at work fixing the situation for you, starting with a simple pass-through to ignore the M204 without throwing an error until it's properly implemented.





**Jon Charnas**

---
---
**John Pickens** *June 29, 2017 00:25*

Is M204 usually just invoked once at the beginning?


---
**Jon Charnas** *June 29, 2017 04:46*

Unfortunately not, it seems to be used throughout the prints, resulting in repeat slowdowns


---
**Step Cia** *June 29, 2017 14:38*

Does this has anything to do with the random stuttering issue while printing? 


---
**Jon Charnas** *June 29, 2017 14:46*

Maybe. The other cause could also be movement buffers too small. However the best way to know for sure is to test it - and unfortunately I don't use slic3r myself.


---
**Step Cia** *June 29, 2017 14:51*

I used all 3 slic3r, cura and s3d. But has recently default to S3D becuase the very reason you posted; silc3r yields slower...


---
*Imported from [Google+](https://plus.google.com/115366526841976572064/posts/MhxhQFxrY5M) &mdash; content and formatting may not be reliable*
