---
layout: post
title: "Some questions about Replicape, Toggle with Manga Screen: 1"
date: February 18, 2016 16:47
category: "Discussion"
author: Veridico Cholo
---
Some questions about Replicape, Toggle with Manga Screen:



1. Dumb one: aside from Octoprint, which comes with Kamikaze, can Replicape be connected to a PC via USB to use software like Simplify 3D, Repetier Host or 3DprinterOS?

I tried connecting via Simplify3D on a Mac and it could not connect. Only the BBB disk volume was mounted but that's about it.



2. Is there a version of Toggle that allows control of the printer (motion, etc) via the Manga screen touch interface? This is needed! 



Thanks!







**Veridico Cholo**

---
---
**Elias Bakken** *February 18, 2016 18:28*

1) No a dumb question! I've only tried Pronterface, that works via USB. Looks like my Ethernet patch is not a part of mainline any more.. Repetier Host is built for Windows it looks like, so I'm not too happy with the 50MB of compatibility packages I need to install to get that running. Simplify 3D is $149, not a bad price but too much just for testing. 3DprinterOS does have a debian package for armhf is looks like, but requires pythonwx-2.8 which does not exist for Debian Jessie, so I'm compiling that from source now and we'll see how far that goes. 

2) No, but is high in the Toggle roadmap list: [https://trello.com/b/kG6Fx7Ib/replicape-roadmap](https://trello.com/b/kG6Fx7Ib/replicape-roadmap)


---
**Elias Bakken** *February 18, 2016 18:48*

3DprinterOS client pulls in a lot of stuff man. I've not tried the client, I was unable to install it either on my Ubuntu box or the BBB, so I cannot say what the GUI is for. I think a version with command line configurations would be idel for the BBB, but without having tested it, I cannot say for certain! A working 3DprinterOS client would be great! 


---
**Veridico Cholo** *February 18, 2016 18:53*

The client is installed on the PC that the printer is connected to. I don't know if you want it installed on the BBB. As long as the BBB can connect to the client on the PC you should be OK. No?


---
**Veridico Cholo** *February 18, 2016 19:00*

Thanks for the reply Elias!



1. I tried using Simplify3D, Repetier, 3DprinterOS, and Pronterface on Mac OS but couldn't connect with any of them. I haven't tried connecting from Windows or Linux versions yet. 



It's great using Octoprint and uploading pre-sliced Gcode but I think the more software and connectivity the board supports the better because there are many use cases. 



2. Thanks for the roadmap. All good features to add. Under toggle, what is "jog"?




---
**Elias Bakken** *February 18, 2016 19:10*

Jog is move in X, Y, Z directions!


---
**Elias Bakken** *February 18, 2016 19:11*

Why did not pronterface work for OsX?


---
**Veridico Cholo** *February 18, 2016 19:16*

Thanks for the "Jog"



Pronterface just sat there saying: "Connecting..."

And didn't return from that.

It was late at night and I was watching the Champion's League so I may have not gave it 100% attention. I will try again tonight when I get home from work.


---
**Veridico Cholo** *February 19, 2016 02:45*

Elias, I tried connecting via USB again (I also have Ethernet connected but I didn't even go to Octroprint). I rebooted the Mac, restarted the printer, Toggle loaded on the Manga Screen, the Mac sees the USB port and it mounts the BBB disk (called kamikaze), I open Pronterface, select the correct USB port, hit Connect and nothing happens. it says "Connecting" and keeps issuing M105 commands but no return.

I tried with Repetier Host for Mac and that one actually says that it connected, but then it sits there: Waiting for temperatures.... It also says: Unknown printer firmware. 

So until these issues are firmed up, I will just continue with Octoprint. I just know a number of people that don't like Octoprint and will complain and won't buy the board if there is no reliable USB connectivity to a host PC with host 3D printing software. I think getting the Replicape and redeem to connect via USB is very necessary to get more people in!


---
**Elias Bakken** *February 19, 2016 02:53*

Which tty port shows up on Mac when connected? Does it have the right privileges? I'll see if I can try and get a Mac connected tomorrow. I'm sure there is a way to get this working, since I got it working on my Ubuntu box. Not questioning your Mac fu, just I don't think there is anything I can do on the "Beaglebone" side to fix it:)


---
**Veridico Cholo** *February 19, 2016 03:00*

OK, I tried the two ports that showed up in Pronterface:

/dev/cu.usbmodem144133:

crw-rw-rw-  1 root  wheel   18,  66 Feb 18 21:59 /dev/tty.usbmodem144133



and

/dev/tty.usbmodem144133:

crw-rw-rw-  1 root  wheel   18,  66 Feb 18 21:59 /dev/tty.usbmodem144133



Thanks for trying!


---
*Imported from [Google+](https://plus.google.com/109713994627754483735/posts/NwDohdUhBVm) &mdash; content and formatting may not be reliable*
