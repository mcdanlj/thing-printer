---
layout: post
title: "Hey, Im writing an essay on 3d-printing for my graduation"
date: November 03, 2016 13:15
category: "Discussion"
author: Erika McDowell
---
Hey,

I´m writing an essay on 3d-printing for my graduation. I´m using the BeagleBone Black and Kamikaze for a Prusia_i3 and creating a persistent Kali Linux USB-Drive for accessing my Printer over a web-address from any computer at any point in time. I was wondering, why use the BeagleBone Black instead of for example the RaspberryPi? 

Thanks in advance,

Erika





**Erika McDowell**

---
---
**Ryan Carlyle** *November 03, 2016 14:37*

A big factor is the BBB having the two PRU coprocessors to handle time-sensitive tasks like step pulse generation. The RasPi has issues toggling pins fast enough and with low enough latency/jitter to run a bunch of steppers in sync. There are tricks you can do with DMA, but it's not an optimal board for 3D printing or CNC machines. 


---
**Elias Bakken** *November 03, 2016 15:39*

Ryan is exactly right! I also want to add that the BBB also has more pins broken out on the headers, does not suffer from memory card corruption issues like the rpi, and is more open source than the pi, something that becomes very significant when you want to build a complete business and community around it! 


---
**Erika McDowell** *November 04, 2016 11:11*

Thanks a lot! Do you mind if I use you as a source?


---
**Ryan Carlyle** *November 05, 2016 00:38*

Elias is probably citable since he's a firmware author. I have no specific credentials in this case aside from "guy who posts on the Internet."


---
**Elias Bakken** *November 05, 2016 00:56*

No problem for me! I can be cited.


---
**Erika McDowell** *November 07, 2016 15:58*

Thank you!


---
*Imported from [Google+](https://plus.google.com/110157224788432521018/posts/2VUir7rRLLN) &mdash; content and formatting may not be reliable*
