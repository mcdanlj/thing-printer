---
layout: page
title: About
permalink: /about/
---

This content now lives at [MakerForums](https://forum.makerforums.info/c/3d-printing/thing-printer)
and is continuing on there. Please go log in there with the same google
account you used for Google+ and you will still own all the content you
wrote.

This static site is out of date and not maintained now that
the content is available at makerforums.

This repository was originally created to back up community contributions, development and knowledge on Replicape, Revolve, and Redeem. The group was active on Google+ but with google discontinuing the G+ service this repository will act as a historic log of that active and knowledgeable community.

* [Replicape](https://www.thing-printer.com/product/replicape/)
* [Revolve](https://www.thing-printer.com/revolve/)
* [Redeem](https://www.thing-printer.com/redeem/)
